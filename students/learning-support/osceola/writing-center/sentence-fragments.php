<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/sentence-fragments.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Sentence Fragments</h2>
                           
                           <p>Besides being a grammar problem, fragments can  interrupt your reader's flow as he/she
                              encounters something "broken." Once you know what is missing, they can be fairly easy
                              to fix. 
                           </p>
                           
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Fragments</font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p><strong>What is it? </strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>A fragment "sentence," like the name implies, is only a <em>piece</em> of a sentence; it is not complete because it is not an independent clause, but a
                                             period has been placed after it as if it were a complete sentence. 
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                       </blockquote>                  
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   <div>When I go to school.</div>
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                       <blockquote>&nbsp;</blockquote>                      
                                       
                                       <p><strong>Independent Clauses</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Before you fix a fragment, you have to know what a sentence, or independent clause,
                                             requires. An independent clause, in its most basic form, is  a <strong>subject</strong> + a <strong>verb</strong> that can stand alone (if you put a period at the end, it's a complete sentence).
                                          </p>
                                          
                                       </blockquote>                      
                                       
                                       <blockquote>
                                          
                                          <blockquote>
                                             
                                             
                                          </blockquote>  
                                          
                                          
                                       </blockquote>
                                       <p><strong>How to Fix</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Now that you know a sentence needs to be an independent clause, meaning you need a
                                             main subject and a verb, you'll have to find out what your incomplete sentence is
                                             missing. 
                                          </p>
                                          
                                       </blockquote>                      
                                       
                                       <blockquote>
                                          
                                          <p><strong>1) If your fragment is a <a href="http://www.chompchomp.com/terms/subordinateclause.htm">dependent clause</a>, like the example above, you are missing the whole independent clause. </strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><span><strong>When I go to school</strong></span>....<strong>what</strong>? I need more info. 
                                             </p>
                                             
                                             <p>When I go to school, I always take my backpack.</p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>2) If your sentence fragment is some other kind of clause/phrase, you might just need
                                                to attach the fragment to a complete sentence before or after.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>Joe fell down the stairs. <strong>Breaking his leg in the process. </strong></p>
                                             
                                             <p>Joe fell down the stairs, breaking his leg in the process. </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>3) Sometimes a fragment is simply missing a subject; add it back in and you have a
                                                complete sentence (you may need some other words to make the sentence fit well). </strong></p>
                                          
                                          <blockquote>
                                             
                                             <p>The principal at the school let the students go home early. <span><strong>And decided to give them the rest of the week off.</strong></span> (who decided?)
                                             </p>
                                             
                                             <p>The principal at the school let the students go home early. <strong>He also</strong> decided to give them the rest of the week off. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p><strong>4) At other times, a fragment sentence may be missing a verb or part of a verb.</strong></p>
                                          
                                          <p>Missing main verb: </p>
                                          
                                          <blockquote>
                                             
                                             <p><span><strong>The owl on the branch of the swaying willow tree. </strong></span>(the owl did what?) 
                                             </p>
                                             
                                             <p>The owl on the branch of the swaying willow tree <strong>watched the mice foraging in the grass</strong>. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>Missing helping verb: </p>
                                          
                                          <blockquote>
                                             
                                             <p><span><strong>The people at the county fair watching the tractor pulls</strong></span>.
                                             </p>
                                             
                                             <p>The people at the county fair<strong> are</strong> watching the tractor pulls. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <p><strong>5) Finally, a fragment might be the result of an incomplete thought, even if a subject
                                                and verb are present.</strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><span><strong>The dog is</strong></span>. (unless we're having an existential discussion, this is an incomplete thought- the
                                                dog is what?)
                                             </p>
                                             
                                             <p>The dog is<strong> in the backyard.</strong></p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://www.chompchomp.com/terms/fragment.htm">Chompchomp</a></p>
                                          
                                          <p><a href="https://www.butte.edu/departments/cas/tipsheets/grammar/fragments.html">Butte College </a></p>
                                          
                                          <p><a href="http://www.english-grammar-revolution.com/sentence-fragments.html">Grammar Revolution</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Exercises</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="https://owl.english.purdue.edu/exercises/5/18/38">Practice 1</a></p>
                                          
                                          <p><a href="http://grammar.ccc.commnet.edu/grammar/quiz2/quizzes-to-fix/fragments_add1.html">Practice 2</a></p>
                                          
                                          <p><a href="http://depts.dyc.edu/learningcenter/owl/exercises/fragments_ex1.htm">Practice 3</a></p>
                                          
                                          <p><a href="http://www.mhhe.com/socscience/english/langan/sentence_skills/exercises/ch05/p4exb.htm">Practice 4</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/sentence-fragments.pcf">©</a>
      </div>
   </body>
</html>