<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/qualifying.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Qualified vs. Absolute Statements </h2>
                           
                           
                           
                           
                           <p>Knowing when to qualify a statement or when it's OK to use an absolute statement will
                              only help to make your academic writing more accurate and your explanations more logical.
                              
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Absolute Statements </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p><strong>Defined</strong>: If something is absolute, it is 100%  one way or another; it is not relative or
                                          comparative.
                                       </p>
                                       
                                       <blockquote>
                                          
                                          <p> <strong>For example</strong>:
                                          </p>
                                          
                                          <p> If someone says, "Grass is green," it suggests that all grass, in any place and at
                                             any time, is green. It doesn't leave room for variation, and consequently it is an
                                             absolute statement. This is different than a qualified statement, which would suggest
                                             something like "<em>Sometimes</em> grass is green." 
                                          </p>
                                          
                                       </blockquote>                  
                                       <p>Now, it's possible for an absolute statement to be true, but more often than not you
                                          will usually need some kind of<strong> evidence or support</strong> to back up your claim. If you don't provide support, it can create problems with
                                          the logic of your paper.
                                       </p>
                                       
                                       <p><strong>Real World Example </strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Let's say you are writing a paper about business management, and somewhere in your
                                             paper you say, "Supervisors and managers do not care about employees; all they care
                                             about is making money." This is an absolute statement- it leaves no room for a different
                                             option. Thus, you are defining all supervisors and managers as being one way- money
                                             hungry and devoid of care for employee well-being. Of course, common sense tells us
                                             that there are supervisors and managers that do actually care about their employees,
                                             not just making money, so such a statement would cause your reader to question your
                                             logic in this matter (and probably put doubt in the logic within other areas of your
                                             paper).
                                          </p>
                                          
                                       </blockquote>                  
                                       <p>Words like <em>always</em> and <em>never</em>, and the use of "be" verbs (<em>is</em>, <em>are</em>, etc) often indicate an absolute statement. 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Qualified Statements </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><strong>Defined</strong>: Unlike absolute statements, qualified statements leave room for other options, opinions,
                                             and situations.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>For example: </strong></p>
                                             
                                             <p>"I usually play volleyball on Sundays," lets your reader know that while you may often
                                                play volleyball on Sundays, it's likely that there are Sundays when you don't play
                                                volleyball. As a result, your statement is qualified because you leave room for variation
                                                - you don't play volleyball on Sundays 100% of the time.
                                             </p>
                                             
                                          </blockquote>
                                          
                                          <p>Even though absolute statements, apart from historical statements or common knowledge,
                                             should almost always be accompanied by some kind of evidence or support (typically
                                             by citing research), qualified statements may or may not require evidence.<strong> It depends on the <em>degree</em> of qualification. </strong></p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>For example:</strong></p>
                                             
                                             <p>Saying "Managers are <strong><em>usually</em></strong> men," is a qualified statement because of the use of "usually." Still, this statement
                                                would probably need a citation because even though it's qualified, "usually" is pretty
                                                close to absolute (let's say 85-95%). If you don't provide a citation for this information,
                                                your reader could still question your logic because how do you know that managers
                                                are usually men? Did you read it somewhere? Did you see a statistic? Or are you just
                                                assuming?
                                             </p>
                                             
                                             <p>On the other hand, if you say, "Managers <strong><em>can</em></strong> be men," your statement is qualified in such a way that you don't really need any
                                                kind of citation. Since you allow "enough room" for variation, your logic can't really
                                                be questioned- it's more of a common sense statement rather than one that needs supported.
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources</h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>For a table of common absolute/qualified words, click <a href="http://writingcenter.unc.edu/handouts/qualifiers/">HERE</a>.
                                          </p>
                                          
                                          <p><a href="documents/qualifiers1.pdf">Qualifiers Handout</a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/qualifying.pcf">©</a>
      </div>
   </body>
</html>