<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/basic-mla-formatting.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Basic MLA Formatting </h2>
                           
                           
                           
                           
                           <p>MLA style is probably the most used style in  undergraduate classes. This page explains
                              the basics, but if you need more detailed information, check out the links on our
                              <a href="style-guides.php">styles guides</a> page. Remember that MLA formatting relates to both how the paper is formatted (margins,
                              font, etc) as well as how citations are formatted. Even if you don't do any research
                              or use sources, which would require citations, you may still be required to use MLA
                              formatting for how your paper <em>looks</em>. 
                           </p>
                           
                           <p>For a visual guide to basic MLA formatting, click <a href="documents/essayformathandout_000.pdf">HERE</a>. For more information about formatting MLA papers, visit <a href="https://style.mla.org/formatting-papers/">The MLA Style Center</a>. 
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">General MLA Requirements </font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p><strong>Here are the basics:</strong></p>
                                       
                                       <ul>
                                          
                                          <li>
                                             
                                             <div>Name &amp; page number on the right side of the header</div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>Include your name, the professor's name, the class name, and the date on the top left
                                                of the 1st page only (not in the header) 
                                             </div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>Have a title (use correct <a href="http://grammar.yourdictionary.com/capitalization/rules-for-capitalization-in-titles.html">capitalization</a>, don't bold or make a larger font) 
                                             </div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>Size 12 font (including the header info)</div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>Font family: Times New Roman (or similar easy to read font)</div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>Double-spaced</div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>1 inch margins all around </div>
                                             
                                          </li>
                                          
                                          <li>
                                             
                                             <div>Indent each paragraph (use the tab key)</div>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       
                                       
                                       <p><strong>Tip</strong>: By default, word usually adds extra space any time you hit the enter key. You want
                                          to turn this off because once you double space your document, the extra space also
                                          gets doubled, so it looks like you have extra space between your paragraphs. Download
                                          our step-by-step <a href="documents/essayformathandout_000.pdf">MLA formatting guide</a> and see <strong>step 2</strong> for how to fix this issue. If you have already completed your paper, make sure to
                                          highlight everything before completing step two. 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Basic MLA  In-text Citations </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>The word "citation" is often used for both types of citations in MLA: those citations
                                             in your text (in-text citations) and those that are listed in your works cited (works
                                             cited entries).
                                          </p>
                                          
                                          <p>Keep in mind that you will need to include both in a paper. If you have a works cited
                                             page, you will also need in-text citations (and vice versa). See our page about <a href="citing-and-plagiarism.php">citing and plagiarism</a> for more info. 
                                          </p>
                                          
                                          <p><strong>In-text Citations</strong></p>
                                          
                                          <p>In-text citations are those that occur in the sentences of your paper. Sometimes they
                                             are called "parenthetical citations" because they are put between parentheses.
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p><strong>1) For MLA, the author's last name and the page number are what are put in the parentheses.</strong></p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>ex. </strong>Most frogs can see in the dark (Smith 25).
                                                </p>
                                                
                                                <blockquote>
                                                   
                                                   <p>Make sure you put the period <em><strong>after</strong></em> the citation.
                                                   </p>
                                                   
                                                   <p>Notice that there is <strong>no comma</strong> between the name and page number. 
                                                   </p>
                                                   
                                                </blockquote>
                                                
                                             </blockquote>
                                             
                                             <p><strong>2) If you don't have a name, a title or shortened title is most often used.</strong></p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>ex. </strong>Cell radiation plays a key role in healthy cell function ("Healthy Cells" 52).
                                                </p>
                                                
                                                <blockquote>
                                                   
                                                   <p>For this example, notice that the title is put between <strong>quotation marks</strong>.
                                                   </p>
                                                   
                                                </blockquote>
                                                
                                             </blockquote>
                                             
                                             <p><strong>3) If you don't have a page number (such as for websites), then you don't put anything
                                                   (but you could put a paragraph number if you want).</strong></p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>ex. </strong>Twenty-three percent of Twitter followers check Twitter more than 3 times a day (Davidson).
                                                   
                                                </p>
                                                
                                             </blockquote>
                                             
                                             <p><strong>4) You can include the name/title of your source in the text of your sentence rather
                                                   than at the end, but the page number will still go in parentheses at the end.</strong></p>
                                             
                                             <blockquote>
                                                
                                                <p><strong>ex. </strong>According to Tim West, mowing your grass more than twice a week can cause lawn disease
                                                   (34). 
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>For more information about MLA in-text citations, or for information on creating entries
                                          for your works cited, see our <a href="style-guides.php">style guides</a> page. 
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/basic-mla-formatting.pcf">©</a>
      </div>
   </body>
</html>