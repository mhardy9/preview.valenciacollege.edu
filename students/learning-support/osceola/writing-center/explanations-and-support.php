<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Osceola Writing Center | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/writing-center/explanations-and-support.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Writing Center</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/writing-center/">Osceola Writing Center</a></li>
               <li></li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  <div class="row">
                     <div class="col-md-9">
                        <a name="content"></a>
                        <a href="/osceola/writing-center/"></a>
                        
                        
                        <section>
                           
                           <h2>Explanations and Support </h2>
                           
                           
                           
                           
                           <p>When writing your paper, it's best to provide some form of support or at least an
                              explanation when you make a statement about something. If you don't provide at least
                              one of these, especially for major statements, it can make your paper's argument less
                              strong because you haven't built a case for what you're saying. 
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <h3><font color="#000000">Types of Evidence*</font></h3>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>One of the ways to support a point you are making is by providing evidence. Here are
                                          4 types of evidence that you could use in your paper:
                                       </p>
                                       
                                       <p><strong>"Einstein" Evidence</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>This kind of evidence relates to information you get from scholars or experts (Einstein
                                             was an expert in physics). When you make a point in your paper, if you can show that
                                             an expert believes the same thing as you, your point will be more strongly supported.
                                             When using this type of evidence, it is a good idea to provide information about the
                                             expert to show your reader that they are credentialed and their opinion is important.
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <blockquote>
                                             
                                             <p>People with bad time-management skills may be more susceptible to anxiety problems.
                                                <strong>According to Dr. Gerard, a psychologist who has been working at John Hopkins University
                                                   Medical Center for 20 years</strong>, <strong>people who cannot manage their time tend to become anxious when they are late for
                                                   an appointment or have a deadline approaching</strong> (2001, p. 99). 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>                  
                                       <p><strong>Example Evidence</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Example evidence is evidence that uses specific examples to help your reader understand
                                             a point. For an academic paper, it will be better to provide an example that you found
                                             through research rather than simply coming up with an example off the top of your
                                             head.
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <blockquote>
                                             
                                             <p>One's country of origin usually dictates how many languages a person speaks.<strong> Citizens of Switzerland tend to speak 3 languages, while people from the Netherlands
                                                   most often speak four</strong> (Smith 144). 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>                  
                                       <p><strong>Fact Evidence</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>This evidence is pretty self-explanatory: your support is in the form of facts or
                                             statistics.
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                       </blockquote>
                                       
                                       <blockquote>
                                          
                                          <blockquote>
                                             
                                             <p>Children with parents who are involved in their schoolwork are usually better prepared
                                                for college. In his book, <em>The Prepared Child</em>, John Crane notes that <strong>when children have parents that spend at least 2 hours a week talking to their child
                                                   about school and assignments, it increases college graduation rates by 50 percent</strong> (322). 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>                  
                                       <p><strong>Undocumented Evidence:</strong></p>
                                       
                                       <blockquote>
                                          
                                          <p>Evidence or support that is considered "undocumented" would be anything that you use
                                             for support that was not found in a document (online or print). It can include personal
                                             experience, anecdotes, or things that fall more into common knowledge. Since undocumented
                                             evidence doesn't come from a specific source, you would not have anything to cite.
                                             While you can use this kind of evidence in your paper, it is best to make sure that
                                             most of your paper includes other forms of evidence (unless you are writing about
                                             a personal experience, etc). 
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>It's very important for people to remember to use their seat belts. Without them,
                                                drivers or passengers could be hurt in the event of an accident. <strong>Last year, my brother got into a car accident, and he wasn't wearing a seatbelt. He
                                                   broke several bones and was hospitalized for 2 months</strong>. 
                                             </p>
                                             
                                          </blockquote>
                                          
                                       </blockquote>
                                       
                                       
                                       <p>*Adapted from <em>Sourcework: Academic Writing From Sources</em> by Nancy Dollahite and Julie Haun 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>Be Specific </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>Also, when you are writing an essay, make sure that you don't leave your reader hanging.
                                             Depending on the context, if you write, "<span>I have learned a lot in my two years at college</span>," but stop right there, your reader will probably wonder what it is that you learned.
                                             It would be better to add another sentence or two explaining what those things were.
                                          </p>
                                          
                                          <p>Relatedly, you'll often want to answer the "Why?" question for your reader. If you
                                             write, "<span>I prefer to travel to Asia rather than Europe</span>," but don't continue, it leaves out information that would help complete the thought.
                                             Why do you prefer Asia? Here's a better sentence: I prefer to travel to Asia rather
                                             than Europe <strong>because I will be able to visit some family members there</strong>.
                                          </p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>So what? </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p>No matter what the point is that you're trying to make, you always want to make sure
                                             your reader can answer the "So what?" question, or the "Why do I need to know this?"
                                             question. Being able to answer these questions regarding information in your paper
                                             will help to make sure that you adequately explain your point or that you use appropriate
                                             supporting details. 
                                          </p>
                                          
                                          <p><strong>Example</strong>:
                                          </p>
                                          
                                          <blockquote>
                                             
                                             <p>Cheetahs are excellent predators. They are able to crouch very low to the ground,
                                                providing them opportunity to get close to their prey without the prey noticing. These
                                                wild cats also have large incisors and strong jaw muscles. With these features, Cheetahs
                                                can quickly strangle their victims, thereby limiting the other animal's suffering.
                                                <strong>Cheetahs have spots too</strong>. Finally, with a top speed of 70mph, these cats can outpace most creatures on their
                                                menu. 
                                             </p>
                                             
                                             <p>Consider the bolded text. After reading this sentence, ask yourself the 2 questions
                                                above: So what? Why do I need to know this? Based on what is provided, we can't answer
                                                these questions. Consequently, it would be better to provide more information showing
                                                how this bolded text relates to or supports the main idea (that Cheetahs are excellent
                                                predators).
                                             </p>
                                             
                                             <p><strong>Fixed</strong>:
                                             </p>
                                             
                                             <blockquote>
                                                
                                                <p>Cheetahs have spots too. These spots provide them a sort of camouflage that prevents
                                                   prey from noticing  these hunters in the African habitat. 
                                                </p>
                                                
                                             </blockquote>
                                             
                                          </blockquote>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <h3>More Resources </h3>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <div>
                                          
                                          <p><a href="http://writesite.elearn.usyd.edu.au/m3/m3u1/m3u1s7/m3u1s7_1.htm">University of Sydney - Evaluating Evidence</a></p>
                                          
                                          <p><a href="http://writingcenter.unc.edu/handouts/evidence/">UNC Writing Center - Evidence</a></p>
                                          
                                          <p><a href="http://www.groundsforargument.org/drupal/evidence/sidebar/types">Grounds for Argument - Types of Evidence </a></p>
                                          
                                          <p><a href="http://www.indiana.edu/~wts/pamphlets/using_evidence.shtml">Indiana University - Using Evidence </a></p>
                                          
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                           
                        </section>
                        <section>
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     <aside class="col-md-3">
                        	
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           
                           		   
                           
                           
                           <!-- website, email, phone, address, location, hours -->
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 3, Rm 100</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4250</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                       <br></div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                           </div>
                           
                           
                        </div>
                        
                        <a href="/students/learning-support/osceola/writing-center/documents/learningcentermap.pdf" target="_blank">
                           Directions</a>
                        			
                        <a href="/locations/map/osceola-campus.php" target="_blank" title="Osceola Campus Map">
                           Campus Map</a>
                        
                        		 
                        		 
                        		 
                        
                        
                        
                        
                        
                        
                     </aside>
                  </div>
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/writing-center/explanations-and-support.pcf">©</a>
      </div>
   </body>
</html>