<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Learning Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/tutoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus Learning Centers</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Osceola Campus Learning Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/learningcenter/index.html"></a>
                        
                        
                        <p><strong>Tutoring Services </strong></p>
                        
                        <p>The Osceola Campus provides tutoring services in a wide variety of subjects to enhance
                           learning via  peer-to-peer instruction.
                        </p>
                        
                        
                        <p><strong>Tutors' Responsibilities: </strong></p>
                        
                        <ul>
                           
                           <li>Provide one-on-one or small-group tutoring to currently enrolled students in a particular
                              discipline.<br>
                              
                           </li>
                           
                           <li>Assess academic ability of students being tutored using provided measurement instruments.
                              <br>
                              
                           </li>
                           
                           <li>Meet with supervisory staff on a regularly scheduled basis to discuss academic progress
                              of students being tutored. <br>
                              
                           </li>
                           
                           <li>Keep accurate and timely records of tutoring and/or lab time on the daily sign-in
                              log. <br>
                              
                           </li>
                           
                           <li>Assist supervisor with academic needs assessment. <br>
                              
                           </li>
                           
                           <li>Document student progress for each session using provided instruments<br>
                              
                           </li>
                           
                           <li>Participate in staff development training. <br>
                              
                           </li>
                           
                           <li>Perform other duties as assigned.<br>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Required skills for all Tutors:</strong></p>
                        
                        <ul>
                           
                           <li>Must have evidence of successful completion of college-level academic work. <br>
                              
                           </li>
                           
                           <li>Acceptable eyesight (with/without corrective aids).<br>
                              
                           </li>
                           
                           <li> Ability to follow instructions from supervisor and other faculty.<br>
                              
                           </li>
                           
                           <li>Ability to work a variety of hours, which includes nights and weekends.<br>
                              
                           </li>
                           
                           <li>Ability to use computer hardware and software programs related to discipline area.<br>
                              
                           </li>
                           
                           <li>Ability to tutor individuals or small groups of students.<br>
                              
                           </li>
                           
                           <li>Ability to use appropriate learning techniques/strategies to achieve specific learning
                              outcomes. <br>
                              
                           </li>
                           
                           <li>Ability to communicate and work effectively in a diverse community and meet the needs
                              of diverse student populations.
                           </li>
                           
                        </ul>
                        
                        <p>If you are interested in applying for a position as a tutor at the Osceola Campus,
                           please fill out an application <a href="http://form.jotform.us/form/40093847150149">HERE</a>.
                        </p>
                        
                        <h2>&nbsp;</h2>
                        
                        <p><a href="../../../locations/osceola/learningcenter/tutoring.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/tutoring.pcf">©</a>
      </div>
   </body>
</html>