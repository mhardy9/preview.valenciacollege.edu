<ul>
	<li><a href="/students/learning-support/index.php">Learning Support</a></li>	
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/osceola/index.php">Osceola Learning Centers <span class="caret"></span></a>
		<ul>	
			<li><a href="/students/learning-support/osceola/applications_LC.php">Available Software</a></li>
			<li><a href="/students/learning-support/osceola/hours.php">Hours of Operation</a></li>	
			<li><a href="/students/learning-support/osceola/resources.php">Learning Resources</a></li>	
		</ul>
	</li>
	<li><a href="/students/learning-support/osceola/tutoring.php">Academic Tutoring</a></li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/osceola/labs/">Labs <span class="caret"></span></a>
		<ul>
			<li><a href="/students/learning-support/osceola/labs/academicSystems.php">Academic Systems Lab</a></li>
			<li><a href="/students/learning-support/osceola/labs/graphics.php">Graphics Lab</a></li>
			<li><a href="/students/learning-support/osceola/labs/language.php">Language Lab</a></li>
		</ul>
	</li>
	<li><a href="/students/learning-support/osceola/mathdepot.php">Math Depot</a></li>
	<li><a href="/students/learning-support/osceola/student-services/">Student Services</a></li>
	<li><a href="/students/learning-support/osceola/testing.php">Testing</a></li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/osceola/writing-center/">Writing Center <span class="caret"></span></a>
		<ul>
			<li><a href="/students/learning-support/osceola/writing-center/services.php">Services</a></li>
			<li><a href="/students/learning-support/osceola/writing-center/staff.php">Staff</a></li>
			<li><a href="/students/learning-support/osceola/writing-center/resources.php">Resources</a></li>
			<li><a href="/students/learning-support/osceola/writing-center/online-consultation.php">Online Consultation</a>	</li>
		</ul>
	</li>
</ul>