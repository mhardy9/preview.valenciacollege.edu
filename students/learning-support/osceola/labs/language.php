<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/labs/language.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/labs/">Mainlab</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/mainlab/index.html"></a>
                        
                        <p><font size="3"> <span><a href="../../../locations/osceola/mainlab/language.html"><font size="3">The Writing Center and Language Lab</font></a> </span>| <a href="../../../locations/osceola/mainlab/EAP_Overview.html">EAP Course
                                 Overview</a> | <a href="../../../locations/osceola/mainlab/EAP_Links.html">EAP Links</a> | <a href="../../../locations/osceola/mainlab/SpanishPortuguese.html">World Language Links</a> | <a href="../../../locations/osceola/mainlab/Grammar-Bootcamp.html">Grammar Boot Camp</a> </font></p>
                        
                        <div>
                           
                           <h2>Writing Center and Language Lab </h2>
                           
                        </div> 
                        <div>&amp;lt;-- <a href="javascript:javascript:history.go(-1)">Back</a>
                           
                        </div>
                        
                        <div>
                           <br>
                           <br>
                           <br>
                           
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <strong>Mission:</strong> 
                                 </div>
                                 
                                 <div>
                                    <p>Our mission is to provide students of English, Spanish, and French with the tools
                                       they need to be successful in their courses. 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong><font size="2">Location:</font></strong></p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><font size="2">Building 3, Room 100 - 407-582-4250 </font></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><font size="2">Hours:</font></p>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><strong> Summer 2016 Hours</strong></div>
                                             
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Mon-Thur:</div>
                                             
                                             <div>8:00 am - 8:00 pm</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Fri: </div>
                                             
                                             <div>8:00 am - 12:00 pm </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Sat: </div>
                                             
                                             <div>8:00 am - 12:00 pm (in The Depot) </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>Sun: </div>
                                             
                                             <div>Closed</div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Dean</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="mailto:lshephard@valenciacollege.edu">Landon Shephard </a> <br>
                                       <strong><font size="2">Location:</font></strong> 4 - 214-B 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <h2>The  Writing Center and Language Lab </h2>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><font size="2"><strong>Associate Professors </strong></font></p>
                                    
                                    
                                    <p>Language Tutors Available </p>
                                    
                                    <p>Accademic assistance is available on a walk-in basis.</p>
                                    
                                    <p> Please make sure that you read our <a href="http://valenciacollege.edu/osceola/mainlab/Tutoring.docx">policies and tips</a> before you meet with an instructor.<font size="2"><strong> </strong></font></p>
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><font size="2"><a href="mailto:khafford@valenciacollege.edu">Keith Hafford</a>, Senior Instructional Assistant, Professor of English for Academic Purposes, ESL</font></p>
                                    
                                    <p><font size="2">(407) 582-4830</font></p>
                                    
                                    <p><a href="mailto:randmiller@valenciacollege.edu">Rand Miller</a>, Senior Instructional Assistant, Professor of English, Freshman Composition, and
                                       Liasion to the Associate of Science Programs. 
                                    </p>
                                    
                                    <p><br>
                                       <a href="mailto:ahenry19@valenciacollege.edu">Abbie Potter Henry</a>, Senior Instructional Assistant, Instructor of Developmental Education, Reading and
                                       Wrtiting, and Grammar Boot Camps 
                                    </p>
                                    
                                    <p>(407) 582-4259 </p>
                                    
                                    
                                    <p><img alt="The Learning Center east view" height="434" src="../../../locations/osceola/mainlab/learningcenterflickr1.jpg" width="618"></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="../../../locations/osceola/mainlab/language.html#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/labs/language.pcf">©</a>
      </div>
   </body>
</html>