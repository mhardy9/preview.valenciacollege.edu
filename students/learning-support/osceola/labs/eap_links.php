<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/labs/eap_links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/labs/">Mainlab</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/mainlab/index.html"></a>
                        
                        <p><font size="3"><span><a href="../../../locations/osceola/mainlab/language.html"><font size="3">Language Lab</font></a> </span>| <a href="../../../locations/osceola/mainlab/EAP_Overview.html"><strong>EAP Overview</strong></a> | <a href="../../../locations/osceola/mainlab/EAP_Links.html">EAP Links</a> | <a href="../../../locations/osceola/mainlab/SpanishPortuguese.html">Foreign Languages</a></font></p>
                        
                        <div>
                           
                           <h2>EAP Links</h2>
                           
                        </div> 
                        <div>&amp;lt;-- <a href="javascript:javascript:history.go(-1)">Back</a>
                           
                        </div>
                        <br>
                        <br>
                        <br>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Dictionaries</div>
                                 
                                 <div>Grammar</div>
                                 
                                 <div>Speech</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.merriam-webster.com/">merriam-webster</a>  
                                    </p>
                                    
                                    <p><a href="http://www.dictionary.com">Dictionary.com</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://a4esl.org/q/h/grammar.html">Activities 
                                          for ESL</a></p>
                                    
                                    <p><a href="http://www.dailygrammar.com">Daily 
                                          Grammar</a></p>
                                    
                                    <p><a href="http://www.englishgrammarsecrets.com/">English 
                                          Grammar Secrets</a></p>
                                    
                                    <p><a href="http://englishpage.com/index.html">English 
                                          Page</a></p>
                                    
                                    <p><a href="http://ww2.college-em.qc.ca/prof/epritchard/trouindx.htm">ESL Blues</a></p>
                                    
                                    
                                    <p><a href="http://www.eslcafe.com/">ESL 
                                          Cafï¿½</a></p>
                                    
                                    <p><a href="http://chompchomp.com/">Grammar 
                                          Bites</a></p>
                                    
                                    <p><a href="http://educator.com/language/english/english-grammar/hendershot/">Grammar 
                                          in Context</a>                    
                                    </p>
                                    
                                    <p><a href="http://www.manythings.org/">Manythings.com</a></p>
                                    
                                    <p><a href="http://staff.washington.edu/marynell/grammar/grammar.html">Mary Nell's</a></p>
                                    
                                    <p><a href="http://www.edufind.com/english/grammar/TOC.CFM">Online English</a></p>
                                    
                                    
                                    <p><a href="http://www.smic.be/smic5022/Onlineexercises.htm">Online Exercises</a></p>
                                    
                                    <p><a href="http://web2.uvcs.uvic.ca/elc/studyzone/">Study Zone</a></p>
                                    
                                    <p><a href="http://www.usingenglish.com/quizzes/">Using English</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.carolinebrownlisteninglessons.com/">Listening Exercises</a></p>
                                    
                                    <p><a href="http://www.elllo.org/">Elllo-Listening Exercises</a></p>
                                    
                                    <p><a href="http://www.esl-lab.com/">ESL-Lab.com</a></p>
                                    
                                    <p><a href="http://npr.org">NPR News</a></p>
                                    
                                    <p><a href="http://www.english-trailers.com/index.php">Movie Trailers</a></p>
                                    
                                    <p><a href="http://www.effective-public-speaking.com/">Oral Presentations</a></p>
                                    
                                    <p><a href="http://www.uiowa.edu/%7Eacadtech/phonetics/#%20">Pronunciation</a></p>
                                    
                                    <p><a href="http://www.manythings.org/pp/">Sound Discrimination</a></p>
                                    
                                    <p><a href="http://www.oupchina.com.hk/dict/phonetic/home.html">Phonetics</a></p>
                                    
                                    <p><a href="http://www.celan.droit.univ-cezanne.fr/anglais/L1/bigask3.htm">Syllable Stress</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Computers</div>
                                 
                                 <div>Writing</div>
                                 
                                 <div>Reading</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://home.earthlink.net/%7Eeslstudent/ez.html#computer">How to Use the Computer</a></p>
                                    
                                    <p>Help with <a href="http://office.microsoft.com/en-us/training/CR100654561033.aspx">Microsoft Office</a></p>
                                    
                                    <p><a href="http://wordprocessing.about.com/od/microsoftword2007/Microsoft_Word_2007_Tips_Tricks_and_Tutorials.htm">Tips on Word 2007</a></p>
                                    
                                    <p><a href="http://www.ehow.com/topic_6772_word-2007-basics.html">More help on MS Word </a></p>
                                    
                                    <p><a href="http://www.ehow.com/how_5699764_make-effective-powerpoint-presentations.html">HelpNow - </a></p>
                                    
                                    <p><a href="../../../locations/library/west/cal/documents/QR-Powerpoint-2012.pdf">PowerPoint</a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://grammar.ccc.commnet.edu/grammar">Guide to Grammar and Writing</a></p>
                                    
                                    <p><a href="http://owl.english.purdue.edu/owl/resource/678/01/">Online Writing Lab </a></p>
                                    
                                    <p><a href="http://mywritinglab.com">MyWritingLab</a></p>
                                    
                                    <p><a href="http://bcs.bedfordstmartins.com/exercisecentral/player_clp/HomeMain.aspx?rendering=screenreader&amp;ra=false&amp;scorecard=true&amp;leftnav=true&amp;rightnav=true&amp;top=true&amp;diag=true&amp;search=true&amp;launchdiag=false&amp;launchfirstex=false&amp;launchex=&amp;logout=true&amp;clpid=9b440edb-64b0-4eab-9c55-ba78240e5e4a">St. Martin's Guide to Writing</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.cdlponline.org/">Adult Learning Activities</a></p>
                                    
                                    <p><a href="http://turnerlearning.com">CNN Newsroom</a></p>
                                    
                                    <p><a href="http://home.earthlink.net/%7Eeslhome/sitemap.html">ESL Home</a></p>
                                    
                                    <p><a href="http://www.townsendpress.net/">Townsend Press</a>     
                                    </p>
                                    
                                    <p><a href="http://wps.ablongman.com/long_henry_er_1">The Effective Reader</a></p>
                                    
                                    <p><a href="http://home.earthlink.net/~ruthpett/safari/index.htm">Word Safari</a></p>
                                    
                                    <p><a href="http://myreadinglab.com">MyReadingLab</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <p><a href="../../../locations/osceola/mainlab/EAP_Links.html#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/labs/eap_links.pcf">©</a>
      </div>
   </body>
</html>