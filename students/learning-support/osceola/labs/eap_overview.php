<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/labs/eap_overview.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/labs/">Mainlab</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/mainlab/index.html"></a>
                        
                        <h2>EAP Overview - Osceola Campus </h2>
                        
                        
                        <p><font size="3"><a href="../../../locations/osceola/mainlab/language.html"><font size="3">Writing Center / Language Lab</font></a> | <a href="../../../locations/osceola/mainlab/EAP_Overview.html"><strong>EAP Overview</strong></a> | <a href="../../../locations/osceola/mainlab/EAP_Links.html">EAP Links</a> | <a href="../../../locations/osceola/mainlab/SpanishPortuguese.html">Foreign Languages</a> | <a href="../../../locations/osceola/mainlab/Grammar-Bootcamp.html">Grammar Boot Camp</a></font></p>
                        
                        
                        <p>EAP 0281 / 0381: High Beginning Combined Skills</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>1) Pathways Foundations: Listening, Speaking, and Critical Thinking</p>
                                    
                                    <p><img alt="Image result for pathways foundations listening speaking and critical thinking" height="159" src="../../../locations/osceola/mainlab/51e68DX8P8L._SX258_BO1204203200_.jpg" width="125"></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>2) Pathways Foundations: Reading, Writing, and Critical Thinking</p>
                                    
                                    <p><img alt="Image result for pathways foundations reading writing and critical thinking" height="165" src="../../../locations/osceola/mainlab/9781285450575-us-300.jpg" width="129"></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>3) Grammar in Context Basic</p>
                                    
                                    <p><img alt="Grammar in Context Book Cover" height="167" src="../../../locations/osceola/mainlab/book-Grammar-in-Context-Basic.jpg" width="130"></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li>
                              <a href="mailto:myelt.support@cengage.com">MyELT Support Email</a>                  
                           </li>
                           
                        </ul>
                        
                        
                        <p>EAP 0400: Intermediate Speech </p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Pathways 2: Listening, Speaking and Critical Thinking</p>
                                    
                                    <p><img alt="Image result for pathways 2 listening speaking and critical thinking" height="157" src="../../../locations/osceola/mainlab/51edqfkBPgL._SX258_BO1204203200_.jpg" width="125"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 0420: Intermediate Reading</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Ten Steps to Improving College Reading Skills (6th ed.)</p>
                                    
                                    <p><img alt="Image result for ten steps to improving college reading skills 6th edition" height="156" src="../../../locations/osceola/mainlab/51qkiLjz3L._SX258_BO1204203200_.jpg" width="125"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 0440: Intermediate Composition</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Great Writing 2 - Great Paragraphs w/ Online Workbook </p>
                                    
                                    <p><img alt="Great Writing 2 book cover" height="165" src="../../../locations/osceola/mainlab/book-Great-Writing-2.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 0460: Intermediate Structure                  </p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Grammar in Context 2 (6th ed)</p>
                                    
                                    <p> <img alt="Image result for grammar in context 2 6th edition" height="156" src="../../../locations/osceola/mainlab/imageServlet.jpg" width="125"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 1500: High Intermediate Speech</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>21st Century Communication 3: Listening, Speaking, &amp; Critical Thinking w/ Online Workbook</p>
                                    
                                    <p> <img alt="21st Century Communication 3 Book cover" height="167" src="../../../locations/osceola/mainlab/book-21st-Century-Communication-3.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 1520: High Intermediate Reading</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>The Effective Reader (4th ed)</p>
                                    
                                    <p> <img alt="Image result for the effective reader 4th edition" height="155" src="../../../locations/osceola/mainlab/51amRHvM4VL._SX258_BO1204203200_.jpg" width="125"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.pearsonmylabandmastering.com/northamerica/myreadinglab/" target="_blank">MyReadingLab - Pearson</a>
                              
                              <ul>
                                 
                                 <li>*<strong>Important</strong>: If you buy a standalone access code anywhere other than Pearson's website, you must
                                    make sure the code <span><strong>includes the eText</strong></span><strong>. </strong>Otherwise, you won't be able to access the lab program for your class.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <a href="https://support.pearson.com/getsupport/s/?tabset-dd12d=1" target="_blank">Support</a>
                              
                              <ul>
                                 
                                 <li>Scroll to the bottom then, click "Contact Support."                    </li>
                                 
                                 <li>You will be asked to sign in or continue as guest, and then                    they
                                    will ask for information. 
                                 </li>
                                 
                                 <li>After you fill out the form(s), you                    will be able to call or chat
                                    with someone.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p>EAP 1540: High Intermediate Composition</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Great Writing 3 - From Great Paragraphs to Great Essays, w/ Online Workbook</p>
                                    
                                    <p> <img alt="Great Writing 3 Book cover" height="167" src="../../../locations/osceola/mainlab/book-Great-Writing-3.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 1560: High Intermediate Structure</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Great Writing 3 - From Great Paragraphs to Great Essays, w/ Online Workbook</p>
                                    
                                    <p> <img alt="Grammar In Context 3 Book cover" height="167" src="../../../locations/osceola/mainlab/book-Grammar-In-Context-3.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 1585: High Intermediate Writing and Grammar</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Great Writing 3 - From Great Paragraphs to Great Essays, w/ Online Workbook</p>
                                    
                                    <p> <img alt="Great Writing 3 Book cover" height="167" src="../../../locations/osceola/mainlab/book-Great-Writing-3.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Great Writing 3 - From Great Paragraphs to Great Essays, w/ Online Workbook</p>
                                    
                                    <p> <img alt="Grammar In Context 3 Book cover" height="167" src="../../../locations/osceola/mainlab/book-Grammar-In-Context-3.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li><a href="mailto:myelt.support@cengage.com">MyELT Support Email</a></li>
                           
                        </ul>
                        
                        
                        <p>EAP 1620: Advanced Reading</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Reading Across the Disciplines (6th ed)                        </p>
                                    
                                    <p><img alt="Image result for reading across the disciplines 6th edition" height="154" src="../../../locations/osceola/mainlab/0321921488.jpg" width="125"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li> <a href="http://www.pearsonmylabandmastering.com/northamerica/myreadinglab/" target="_blank">MyReadingLab - Pearson</a>
                              
                              <ul>
                                 
                                 <li>*<strong>Important</strong>: If you buy a standalone access code anywhere other than Pearson's website, you must
                                    make sure the code <span><strong>includes the eText</strong></span><strong>. </strong>Otherwise, you won't be able to access the lab program for your class.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <a href="https://support.pearson.com/getsupport/s/?tabset-dd12d=1" target="_blank">Support</a>
                              
                              <ul>
                                 
                                 <li>Scroll to the bottom then, click "Contact Support." </li>
                                 
                                 <li>You will be asked to sign in or continue as guest, and then                    they
                                    will ask for information. 
                                 </li>
                                 
                                 <li>After you fill out the form(s), you                    will be able to call or chat
                                    with someone.                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <p>EAP 1640: Advanced Composition</p>
                        
                        <h3>Textbooks</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Great Writing 4 –Great Essays, w/ Online Workbook                        </p>
                                    
                                    <p><img alt="Great Writing 4 book cover" height="167" src="../../../locations/osceola/mainlab/book-Great-Writing-4.jpg" width="130"></p>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Lab</h3>
                        
                        <ul>
                           
                           <li>For a video on how to sign up,use <a href="https://myelt.heinle.com" target="_blank">MyELT</a>, and choose <strong>"How to Create a Student Account"</strong>
                              
                           </li>
                           
                           <li><a href="https://myelt.heinle.com/ilrn/global/myeltHelp.do" target="_blank">Additional tutorials and guides</a></li>
                           
                           <li>
                              <a href="mailto:myelt.support@cengage.com">MyELT Support Email</a>                  
                           </li>
                           
                        </ul>
                        
                        
                        
                        <p><a href="../../../locations/osceola/mainlab/EAP_Overview.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/labs/eap_overview.pcf">©</a>
      </div>
   </body>
</html>