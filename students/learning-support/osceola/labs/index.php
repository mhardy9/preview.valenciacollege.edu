<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/labs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Mainlab</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     
                     
                     				
                     <h2>Introduction to the Osceola Campus Labs</h2>
                     
                     				
                     <p>The Osceola Campus  labs are here to help you with your learning goals.  Thanks  for
                        visiting and if you have any comments or questions about our labs, please feel free
                        to email us with your concerns.
                     </p>
                     				
                     <h3>Osceola Learning Center </h3>
                     				
                     <p>The Learning Center is the primary site for non-classroom based 
                        					learning at the Osceola Campus and includes peer tutoring, communication 
                        					labs, the Writing Center, IT support, and and open computer lab. It is a place
                        where students can both access technology and receive personal 
                        					assistance. The Learning Center's mission is to provide an engaging 
                        					and supportive environment where students can pursue a wide range 
                        					of digital and traditional learning opportunities.
                     </p>
                     				
                     <h3>Academic Systems Lab</h3>
                     				
                     <p>Welcome to the future of mathematics! If you are already
                        					enrolled in an Academic Systems course, you'll feel right at home here.
                     </p>
                     				
                     <h3>Graphics Lab</h3>
                     				
                     <p>Enrolled in the latest layout and design courses?
                        					If so, our graphics lab is for you.&nbsp; The Graphics Lab will provide
                        					you with all of the software you need to let your creativity loose&nbsp; with the
                        platform of choice for the graphics professional:
                        					the Macintosh!
                     </p>
                     				
                     <h3>Language Lab</h3>
                     				
                     <p>Are you in an EAP course? If so, this specialized
                        					lab is for you. This lab provides you with all your EAP tools as well
                        					as assistance to help you succeed.
                     </p>
                     				
                     <h3>Math Depot</h3>
                     				
                     <p>The Math Depot, located in Building 4, is the place for math and science tutoring.
                        Stop by for face-to-face tutoring and support!
                     </p>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/labs/index.pcf">©</a>
      </div>
   </body>
</html>