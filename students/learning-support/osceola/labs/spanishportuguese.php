<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/labs/spanishportuguese.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/labs/">Mainlab</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/mainlab/index.html"></a>
                        
                        <p><font size="3"><span><a href="../../../locations/osceola/mainlab/language.html"><font size="3">Language Lab</font></a></span>&nbsp;| <a href="../../../locations/osceola/mainlab/EAP_Overview.html"><strong>EAP Overview</strong></a> | <a href="../../../locations/osceola/mainlab/EAP_Links.html">EAP Links</a> | <a href="../../../locations/osceola/mainlab/SpanishPortuguese.html">Foreign Languages</a></font></p>
                        
                        <div>
                           <h2>Foreign Languages</h2>
                        </div> 
                        <div>&amp;lt;-- <a href="javascript:javascript:history.go(-1)">Back</a>
                           
                        </div>
                        
                        <p><font size="3"><a href="../../../locations/osceola/learningcenter/tutoring.asp.html"><br>
                                 <br>
                                 <br>
                                 Learning Center Tutoring Schedule</a></font></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>Textbooks</div>
                                 
                                 <div>Labs</div>
                                 
                                 <div>Additional Links</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Spanish</div>
                                 
                                 <div>
                                    
                                    <p><strong>SPN 1120- Elem. Spanish I </strong></p>
                                    
                                    <p><strong>SPN 1121- Elem. Spanish II </strong></p>
                                    
                                    <p><em>Text: <strong>Aventuras 4th</strong> Edition (with access code) </em></p>
                                    
                                    
                                    <p><strong>SPN 1340- Spanish for Heritage Speakers</strong></p>
                                    
                                    <p><em>Text: Nuestro Idioma, Nuestra Herencia (with activities manual) </em></p>
                                    
                                    <p><strong>Important Notice:</strong> Starting in the Spring 2012 term, both SPN1120 and SPN1121 will be using the <em>Aventuras </em>textbook and lab component.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="https://www.vhlcentral.com/">Vista Higher Learning </a></p>
                                    
                                    <p> <a href="http://vistahigherlearning.com/students/store/">VHL Store</a> 
                                    </p>
                                    
                                    <p>Activities Manual</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.donquijote.org/spanishlanguage/alphabet/"> Alphabet</a> 
                                    </p>
                                    
                                    <p><a href="http://www.bbc.co.uk/languages/spanish/">BBC Languages Learn Spanish </a></p>
                                    
                                    <p><a href="http://www.byki.com/user/storres">BYKI (Vocabulary Practice) </a></p>
                                    
                                    <p><a href="http://duolingo.com/">DuoLingo</a> 
                                    </p>
                                    
                                    <p> <a href="http://faculty.valenciacollege.edu/sguevara/"></a><a href="http://eleaston.com/sp-qz.html#t"></a><a href="http://umichspanish.yabla.com/">Listening Exercises</a></p>
                                    
                                    <p><a href="http://www.fonetiks.org/indexother.html">Pronunciation </a>1
                                    </p>
                                    
                                    <p><a href="http://www.uiowa.edu/~acadtech/phonetics/#%20%23">Pronunciation </a>2
                                    </p>
                                    
                                    <p><a href="http://www.spanishdict.com/answers/100808/how-to-type-spanish-letters-and-accents-">Spanish Accents </a>1
                                    </p>
                                    
                                    <p><a href="http://www.spanishnewyork.com/spanish-characters.html">Spanish Accents </a>2
                                    </p>
                                    
                                    <p><a href="http://spanish.typeit.org/">Spanish Keyboard </a></p>
                                    
                                    <p><a href="http://www.colby.edu/~bknelson/SLC/index.php">Spanish Language &amp; Culture</a></p>
                                    
                                    <p><a href="http://www.multilingualbooks.com/online-newspapers-spanish.html#about">SpanishNewspapers Online</a></p>
                                    
                                    <p><a href="http://conjuguemos.com/home/index.html">Verb Drills</a></p>
                                    
                                    <p><a href="http://www.studyspanish.com/tutorial.htm">Study Spanish</a></p>
                                    
                                    <p><a href="http://www.wordreference.com/">Word Reference</a></p>
                                    
                                    
                                    <p><a href="http://www.donquijote.org/spanishlanguage/alphabet/"> </a>Dictionaries
                                    </p>
                                    
                                    <p><a href="http://www.freedict.com/onldict/spa.html">Dictionary</a></p>
                                    
                                    <p><a href="http://www.freedict.com/onldict/spa.html">English/Spanish Dictionary </a></p>
                                    
                                    <p><a href="http://www.spanishdict.com">Spanish Dictionary with Pronunciation</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Portuguese</div>
                                 
                                 <div>
                                    
                                    <p><em>Lingua e Cultura</em></p>
                                    
                                    <p><strong><em>(Not currently offered at the Osceola Campus) </em></strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <p>Lingua e Cultura (CD)</p>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://faculty.valenciacollege.edu/sguevara/"></a><a href="http://eleaston.com/sp-qz.html#t"></a><a href="http://www.easyportuguese.com">Easy Portuguese</a></p>
                                    
                                    <p> <a href="http://www.sonia-portuguese.com/index.htm">Sonia-Portuguese</a></p>
                                    
                                    <p><a href="http://www.fonetiks.org/indexother.html">Pronunciation</a></p>
                                    
                                    <p><a href="http://www.freedict.com/onldict/por.html">English/Portuguese Dictionary</a></p>
                                    
                                    <p><a href="http://www.verbix.com/languages/portuguese.shtml">Portuguese Verb Conjugations</a></p>
                                    
                                    <p><a href="http://www.multilingualbooks.com/online-newspapers-portuguese.html">Portuguese Newspapers Online</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>French</div>
                                 
                                 <div>
                                    
                                    <p><strong>FRE 1120- Elem. French I and </strong></p>
                                    
                                    <p><strong>FRE1121- Elem. French II </strong></p>
                                    
                                    <p><em>Text: Promenades (with access code) </em></p>
                                    
                                    
                                    
                                    
                                    <p><strong>FRE 2200- Intermediate French I</strong></p>
                                    
                                    <p><em>Text: (selected readings required by instructor)</em></p>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.vhlcentral.com/">Promenades online workbook</a></p>
                                    
                                    <p>Promenades Lab Manual and CD </p>
                                    
                                    <p><a href="http://www.vhldirect.com/store/">VHL Direct </a> -to purchase access code directly 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p> <a href="http://www.bbc.co.uk/languages/french/index.shtml?survey=no&amp;url=www.bbc.co.uk/languages/french/index.shtml&amp;site=languagesfrench&amp;js=yes">BBC - Languages - Learn French </a></p>
                                       
                                       <p><a href="http://www.bonjour.com/">Learn French </a></p>
                                       
                                       <p><a href="http://www.frenchassistant.com/">French Assistant</a> 
                                       </p>
                                       
                                       <p><a href="http://learnfrench.elanguageschool.net/">French Grammar</a> 
                                       </p>
                                       
                                       <p><a href="http://www.uni.edu/becker/french31.html#quizzes%23quizzes">Quizzes</a></p>
                                       
                                       <p><a href="http://www.uni.edu/becker/french31.html#verbs%23verbs">French Verbs </a></p>
                                       
                                       <p><a href="http://www.uni.edu/becker/french31.html#grammar%23grammar">More Grammar Help</a> 
                                       </p>
                                       
                                       <p> <a href="http://www.bbc.co.uk/french/">French News index</a> 
                                       </p>
                                       
                                       <p> <a href="http://french.typeit.org/">French accents</a> 
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <p><a href="../../../locations/osceola/mainlab/SpanishPortuguese.html#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/labs/spanishportuguese.pcf">©</a>
      </div>
   </body>
</html>