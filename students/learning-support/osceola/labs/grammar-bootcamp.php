<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/labs/grammar-bootcamp.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li><a href="/students/learning-support/osceola/labs/">Mainlab</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt=" " height="8" src="../../../locations/osceola/mainlab/arrow.gif" width="8">
                                 <img alt=" " height="8" src="../../../locations/osceola/mainlab/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <a name="navigate" id="navigate"></a>
                                          
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>Navigate</div>
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                                
                                             </div>
                                             
                                          </div> 
                                          
                                          
                                          
                                          <div>
                                             <span>Related Links</span>
                                             
                                             <ul>
                                                
                                                <li><a href="../../../locations/business-office/index.html">Financial Services </a></li>
                                                
                                                <li><a href="../../../locations/student-development/default.html">Student Development </a></li>
                                                
                                             </ul>
                                             
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div><img alt="" height="1" src="../../../locations/osceola/mainlab/spacer.gif" width="15"></div>
                                                      
                                                      <div><img alt="" height="1" src="../../../locations/osceola/mainlab/spacer.gif" width="770"></div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <p><font size="3"><a href="../../../locations/osceola/writing-center/index.html"><font size="3">The Writing Center</font></a> | <a href="../../../locations/osceola/mainlab/EAP_Overview.html">EAP Course Overview</a> | <a href="../../../locations/osceola/mainlab/EAP_Links.html">EAP Links</a> | <a href="../../../locations/osceola/mainlab/SpanishPortuguese.html">World Language Links</a></font></p>
                                          
                                          <hr>
                                          
                                          <p><img alt="Grammar Boot Camp Image" height="255" hspace="0" src="../../../locations/osceola/mainlab/grambootcamppic_000.jpg" vspace="0" width="468"></p>
                                          
                                          
                                          <h2>Grammar Boot Camp Schedule - Summer 2017</h2>
                                          
                                          <h4>Location: Learning Center 3-103 </h4>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Grammar Basics 1: Laying the Write Foundation: Nouns, Pronouns, and Verb Tense </div>
                                                   
                                                   <div>Grammar Basics 2: Making the Write Connections to Avoid Comma Splices, Fused Sentences,
                                                      and Fragments 
                                                   </div>
                                                   
                                                   <div>Grammar Basics 3: Prepositions, Phrases, and Modifiers, Oh My!</div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li>Wednesday, Jun. 21 @ 1:30PM</li>
                                                         
                                                         <li>Thursday, Jun. 22 @ 4:45PM</li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li>Wednesday, Jun. 28 @ 1:30PM</li>
                                                         
                                                         <li>Thursday, Jun. 29 @ 4:45PM</li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li>Wednesday, Jul. 5 @ 1:30PM</li>
                                                         
                                                         <li>Thursday, Jul. 6 @ 4:45PM</li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>Session Resources </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-GrammarTerms2015.pdf" target="_blank">Grammar Terms</a></li>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-NounsandPronouns.pdf" target="_blank">Nouns and Pronouns</a></li>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-UnderstandingCompleteVerbs.pdf" target="_blank">Understanding Complete Verbs</a></li>
                                                         
                                                         <li><a href="https://prezi.com/gtoodmusvc6u/grammar-boot-camp-1-laying-the-write-foundation-nouns-pro/">Prezi 1</a></li>
                                                         
                                                         <li><a href="http://valenciacollege.edu/osceola/mainlab/documents/grammarbootcamp1.pptx" target="_blank">PPT Grammar Boot Camp 1 </a></li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-Clausesandhowtoconnectthem.pdf" target="_blank">Clauses and How to Connect Them</a></li>
                                                         
                                                         <li>
                                                            <a href="https://prezi.com/ld9k7m9_1q6m/grammar-boot-camp-2-making-the-write-connections-to-avoid-comma-splices-fused-sentences-and-fragments/">Prezi 2</a> 
                                                         </li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-ModifiersandPhrases.pdf" target="_blank">Modifiers and Phrases</a></li>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AdjectivesandAdverbs.pdf" target="_blank">Adjectives and Adverbs</a></li>
                                                         
                                                         <li><a href="https://prezi.com/f6i-0uuqm7bj/grammar-boot-camp-3-part-1/">Prezi 3A</a></li>
                                                         
                                                         <li><a href="https://prezi.com/ded284cn0xez/grammar-boot-camp-3-part-2-preopisitions-phrases-and-modifiers-oh-my/">Prezi 3B</a></li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>Grammar Basics 4: Can't We All be Friends: Subject Verb Agreement </div>
                                                   
                                                   <div>Grammar Basics 5: He Said; She Said: Avoiding Pronoun Agreement, Reference, and Case
                                                      Errors 
                                                   </div>
                                                   
                                                   <div>Grammar Basics 6: Not just Sentence Decoration: Using Commas Correctly </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li>Wednesday, Jul. 12 @ 1:30PM</li>
                                                         
                                                         <li>Thursday, Jul. 13 @ 4:45PM</li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li>Wednesday, Jul. 19 @ 1:30PM</li>
                                                         
                                                         <li>Thursday, Jul. 20 @ 4:45PM</li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li>Wednesday, Jul. 26 @ 1:30PM</li>
                                                         
                                                         <li>Thursday, Jul. 27 @ 4:45PM</li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>Session Resources </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/13RulesofSubject-VerbAgreement.pdf" target="_blank">13 Rules of Subject-Verb Agreement</a></li>
                                                         
                                                         <li><a href="https://prezi.com/cy7iubdpxmgi/grammar-boot-camp-4-cant-we-all-be-friends-subject-verb-agreement/">Prezi 4</a></li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-PronounAgreementReferenceandCase2.pdf" target="_blank">Pronoun Agreement, Reference, and Case</a></li>
                                                         
                                                         <li><a href="https://prezi.com/hf-zmxpejf9b/grammar-boot-camp-5-pronoun-agreement-reference-and-case/">Prezi 5</a></li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                   <div>
                                                      <ul>
                                                         
                                                         <li><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-CommaRules.pdf" target="_blank">Comma Rules</a></li>
                                                         
                                                         <li><a href="http://valenciacollege.edu/osceola/mainlab/documents/Grammarbootcampsession6-commas.pptx" target="_blank">PPT Grammar Boot Camp 6 </a></li>
                                                         
                                                         <li><a href="https://prezi.com/mxi5lcjkqiyx/grammar-basics-6-not-just-sentence-decoration-using-commas/">Prezi 6</a></li>
                                                         
                                                      </ul>
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                          
                                          
                                          <hr>
                                          
                                          <h2>Boot Camp Handouts and Resources</h2>
                                          
                                          <h3>Handouts:</h3>
                                          
                                          <blockquote>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/13RulesofSubject-VerbAgreement.pdf" target="_blank">13 Rules of Subject-Verb Agreement</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AdjectivesandAdverbs.pdf" target="_blank">Adjectives and Adverbs</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-Clausesandhowtoconnectthem.pdf" target="_blank">Clauses and How to Connect Them</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-CommaRules.pdf" target="_blank">Comma Rules</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-GrammarTerms2015.pdf" target="_blank">Grammar Terms</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-ModifiersandPhrases.pdf" target="_blank">Modifiers and Phrases</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-NounsandPronouns.pdf" target="_blank">Nouns and Pronouns</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-PronounAgreementReferenceandCase2.pdf" target="_blank">Pronoun Agreement, Reference, and Case</a></p>
                                             
                                             <p><a href="../../../locations/osceola/mainlab/documents/AbbiePotterHenry-UnderstandingCompleteVerbs.pdf" target="_blank">Understanding Complete Verbs </a></p>
                                             
                                          </blockquote>
                                          
                                          <h3><strong>Powerpoints:</strong></h3>
                                          
                                          <blockquote>
                                             
                                             <p><a href="http://valenciacollege.edu/osceola/mainlab/documents/grammarbootcamp1.pptx" target="_blank">PPT Grammar Boot Camp 1</a></p>
                                             
                                             <p><a href="http://valenciacollege.edu/osceola/mainlab/documents/Grammarbootcampsession6-commas.pptx" target="_blank">PPT Grammar Boot Camp 6 </a></p>
                                             
                                          </blockquote>
                                          
                                          <h3><strong>Presentations:</strong></h3>
                                          
                                          <blockquote>
                                             
                                             <p><a href="https://prezi.com/gtoodmusvc6u/grammar-boot-camp-1-laying-the-write-foundation-nouns-pro/">Prezi 1- Laying the Write Foundation </a></p>
                                             
                                             <p><a href="https://prezi.com/ld9k7m9_1q6m/grammar-boot-camp-2-making-the-write-connections-to-avoid-comma-splices-fused-sentences-and-fragments/">Prezi 2- Making the Write Connections</a> 
                                             </p>
                                             
                                             <p><a href="https://prezi.com/f6i-0uuqm7bj/grammar-boot-camp-3-part-1/">Prezi 3A- Prepositions, Phrases, and Modifiers</a> 
                                             </p>
                                             
                                             <p><a href="https://prezi.com/ded284cn0xez/grammar-boot-camp-3-part-2-preopisitions-phrases-and-modifiers-oh-my/">Prezi 3B- Prepositions, Phrases, and Modifiers</a> 
                                             </p>
                                             
                                             <p><a href="https://prezi.com/cy7iubdpxmgi/grammar-boot-camp-4-cant-we-all-be-friends-subject-verb-agreement/">Prezi 4- Subject Verb Agreement</a> 
                                             </p>
                                             
                                             <p><a href="https://prezi.com/hf-zmxpejf9b/grammar-boot-camp-5-pronoun-agreement-reference-and-case/">Prezi 5- Avoiding Pronoun Agreement, Reference, and Case Errors</a></p>
                                             
                                             <p><a href="https://prezi.com/mxi5lcjkqiyx/grammar-basics-6-not-just-sentence-decoration-using-commas/">Prezi 6- Using Commas Correctly</a></p>
                                             
                                          </blockquote>
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/osceola/mainlab/spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/osceola/mainlab/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><br></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/labs/grammar-bootcamp.pcf">©</a>
      </div>
   </body>
</html>