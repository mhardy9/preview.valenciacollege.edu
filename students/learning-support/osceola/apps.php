<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Osceola Campus Learning Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/apps.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Osceola Campus Learning Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Osceola Campus Learning Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../locations/osceola/learningcenter/index.html"></a>
                        
                        <h2>Available Software</h2>
                        
                        <p> The Learning Center houses the Osceola campus's software library, with most software
                           associated with courses at the Osceola campus. Please make sure that you are familiar
                           with the Learning Center's policies. Click on the tab for the academic area that you
                           are interested in to find a detailed list of software available. 
                        </p>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>ENG/EAP Applications </div>
                                 
                                 <div>Math Applications </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Skillsbank 4 </li>
                                       
                                       <li>Academic Vocabulary</li>
                                       
                                       <li>Active Vocabulary</li>
                                       
                                       <li>Perfect Copy</li>
                                       
                                       <li>Real Writing  </li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Brooks Cole Exerciser </li>
                                       
                                       <li>Plato Interactive Mathematics </li>
                                       
                                       <li>Journey Through Calculus </li>
                                       
                                       <li>Minitab</li>
                                       
                                       <li>TG Elementary Algebra 3e </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        <br>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>IT/OST Applications </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong><em>Keyboarding
                                                         </em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Skillbuilding</li>
                                                   
                                                   <li>Glencoe Keyboarding</li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><em>Open Office 3.0
                                                         </em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <ul>
                                                      
                                                      <li> Drawing </li>
                                                      
                                                      <li>Presentations</li>
                                                      
                                                      <li>Spreadsheets</li>
                                                      
                                                      <li>Writer</li>
                                                      
                                                   </ul>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong><em>Microsoft Office 2007</em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Microsoft Access </li>
                                                   
                                                   <li>Microsoft Excel</li>
                                                   
                                                   <li>Microsoft Powerpoint</li>
                                                   
                                                   <li>Microsoft Visio</li>
                                                   
                                                   <li>Microsoft Word</li>
                                                   
                                                   <li>Microsoft Publisher</li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><em>Software Development </em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Jcreator</li>
                                                   
                                                   <li>Microsoft Visual Studio .NET 2008</li>
                                                   
                                                   <li>MSDN Library for Visual Studio. Net 2005<br>               
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong><em>Multimedia</em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Gimp</li>
                                                   
                                                </ul>
                                                
                                                <p><strong><em>Drafting</em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Autocad 2009 </li>
                                                   
                                                </ul>                     
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><em>Internet</em></strong></p>
                                                
                                                <ul>
                                                   
                                                   <li>Mapedit</li>
                                                   
                                                   <li>Internet Explorer</li>
                                                   
                                                   <li>Mozilla Firefox</li>
                                                   
                                                   <li>Safari   </li>
                                                   
                                                   <li>Telnet</li>
                                                   
                                                   <li>Filezilla<br>
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>     
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Science Applications </div>
                                 
                                 <div>Nursing &amp; Health Applications </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong><em>Anatomy and Physiology </em></strong></p>
                                    
                                    <ul>
                                       
                                       <li>A D A M Standard 2.01</li>
                                       
                                       <li>IntPhys-Cardiovascular 1.1</li>
                                       
                                       <li>IntPhys-Muscular 1.1</li>
                                       
                                       <li>IntPhys-Nervous 1.0</li>
                                       
                                       <li>IntPhys-Respiratory 1.0</li>
                                       
                                       <li>Study Partner for A&amp;P 4th Edition</li>
                                       
                                       <li>Urinary System</li>
                                       
                                    </ul>
                                    
                                    <p><strong><em>Chemistry</em></strong></p>
                                    
                                    <ul>
                                       
                                       <li>Chemistry of Life (CD) </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Abbreviations and Equivalents</li>
                                       
                                       <li>Compact Drugs and Solutions</li>
                                       
                                       <li>Dosage and Calculations</li>
                                       
                                       <li>Mosby Adventure</li>
                                       
                                       <li>Mosby Fluids and Electrolytes</li>
                                       
                                       <li>ProCalc Drug Solutions Practice</li>
                                       
                                       <li>Diet Analysys 8 (Limited Workstations)</li>
                                       
                                       <li>Medisoft Patient Accounting (Limited Workstations)</li>
                                       
                                       <li>Radiology in a Flash (Limited Workstations)</li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div> 
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/apps.pcf">©</a>
      </div>
   </body>
</html>