<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/osceola/onlinetutoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/osceola/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <img alt="" height="8" src="../../../locations/osceola/learningcenter/arrow.gif" width="8">
                                 <img alt="" height="8" src="../../../locations/osceola/learningcenter/arrow.gif" width="8">
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             <a name="navigate" id="navigate"></a>
                                             <img alt="Navigate" height="23" src="../../../locations/osceola/learningcenter/hd_navigate.gif" width="162"><br>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div> 
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/index.html">Learning Center</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/tutoring.html">Academic Tutoring</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/cpt.html">CPT Review Sessions</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/OnlineTutoring.html">Online Tutoring</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/tutoring2.html">Tutors &amp; Subjects</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/writing-center/default.html">Writing Center</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/complab.html">Computer Lab</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/engreadlab.html">ENC/REA Labs</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/mathlabs.html">Math Labs</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/workshops.html">Free Workshops</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div><a href="../../../locations/osceola/learningcenter/resources.html">Learning Resources</a></div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div><img height="1" src="../../../locations/osceola/learningcenter/spacer.gif" width="5"></div>
                                                         
                                                         <div><img height="1" src="../../../locations/osceola/learningcenter/spacer.gif" width="157"></div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             <img height="23" src="../../../locations/osceola/learningcenter/hd_bottom.gif" width="162">
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div><strong>Related Links</strong></div>
                                                      
                                                   </div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>•</div>
                                                                  
                                                                  <div><a href="../../../locations/students/student-services/default.html">Student Services</a></div>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>•</div>
                                                                  
                                                                  <div><a href="../../../locations/business-office/index.html">Financial Services</a></div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      <h2>
                                                         <img alt="VCC Tutoring" height="110" src="../../../locations/osceola/learningcenter/tutoring.jpg" width="582"> <br>
                                                         Free Online Tutoring:
                                                      </h2>
                                                      
                                                      <p>Smarthinking is a Free online tutoring service for Osceola Campus students. You can
                                                         get live, online tutoring, as well as reviews and comments about essays and research
                                                         papers.
                                                      </p>
                                                      
                                                      <h3>SMARTHINKING’S Online Tutoring Subjects</h3>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <p><strong><u>Math</u></strong> <br>
                                                                     
                                                                  </p>
                                                                  
                                                                  <p>Basic Math</p>
                                                                  
                                                                  <p> Algebra</p>
                                                                  
                                                                  <p> Geometry</p>
                                                                  
                                                                  <p> Trigonometry</p>
                                                                  
                                                                  <p> Pre-Calculus</p>
                                                                  
                                                                  <p> Single-Variable</p>
                                                                  
                                                                  <p> Calculus</p>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <p><strong><u>Writing</u></strong> 
                                                                  </p>
                                                                  
                                                                  
                                                                  <p>The Online Writing <br>
                                                                     Lab
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <p><strong><u>Science</u></strong> 
                                                                  </p>
                                                                  
                                                                  
                                                                  <p>Chemistry</p>
                                                                  
                                                                  <p> Biology</p>
                                                                  
                                                                  <p> Physics</p>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <p><strong><u>Business/ <br>
                                                                           Economics</u></strong></p>
                                                                  
                                                                  <p> Microeconomics</p>
                                                                  
                                                                  <p> Macroeconomics</p>
                                                                  
                                                                  <p> Statistics</p>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <p><u><strong>ESL</strong></u> 
                                                                  </p>
                                                                  
                                                                  
                                                                  <p>ESL Writing Center<br>
                                                                     Math in Spanish<br>
                                                                     All Tutoring Subjects
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  <div>
                                                                     
                                                                     <p><u><strong>Spanish</strong></u> 
                                                                     </p>
                                                                     
                                                                     
                                                                     <p>Grammar Questions<br>
                                                                        Essay Center
                                                                     </p>
                                                                     
                                                                  </div>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <p>*For additional information, please contact James Coddington at the following:</p>
                                                      
                                                      <p> <br>
                                                         Office: Building 4 room 122 <br>
                                                         E-mail: jcoddington@valenciacollege.edu
                                                      </p>
                                                      
                                                      <h3>Interested in Tutoring at the Osceola Campus?</h3>
                                                      
                                                      <p> If you are inetrested in being an academic tutor, <a href="../../../locations/osceola/learningcenter/tutoring.html">click here</a> to learn about the tutoring program requirements
                                                      </p>
                                                      
                                                      <p><a href="../../../locations/osceola/learningcenter/OnlineTutoring.html#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/osceola/learningcenter/spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="../../../locations/osceola/learningcenter/spacer.gif" width="770"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div><img alt="" height="1" src="../../../locations/osceola/learningcenter/spacer.gif" width="162"></div>
                                       </div>
                                       
                                       <div><img alt="" height="1" src="../../../locations/osceola/learningcenter/spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><br></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/osceola/onlinetutoring.pcf">©</a>
      </div>
   </body>
</html>