<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/learning-centers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li>Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Learning Centers</h2>
                        
                        
                        
                        
                        <div>
                           
                           
                           <h3>East</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h3><a href="../east/academicsuccess/index.html">Academic Success Center Homepage</a></h3>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../east/academicsuccess/eap/index.html">
                                                   EAP Lab 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 4, Rm 105</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-2098</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../east/academicsuccess/writing/index.html">
                                                   Communications Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 4, Rm 120</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-2795</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../east/academicsuccess/tutoring/index.html">
                                                   Tutoring 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 4, Rm 101</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-2540</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../east/academicsuccess/language/index.html">
                                                   Foreign Language Lab 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 10, First Floor</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-2841</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Lake Nona</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../lakenona/tutoring-math.html">
                                                   Math Learning Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">1-230</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-7106</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 9:00am - 7:00pm<br>Friday: 10:00am - 4:00pm<br>Saturday: 9:00am - 1:00pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../lakenona/tutoring-english.html">
                                                   Reading, Writing, Communication, and Speech Learning Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">1-230</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-7106</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 10:00am - 6:00pm<br>Friday: 10:00am - 4:00pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../lakenona/tutoring.html">
                                                   Tutoring 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 1, Rm 230</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-7106</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 9am - 7pm<br>Friday: 10am - 4pm<br>Saturday: 9am - 1pm <br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Osceola</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../osceola/mainlab/language.html">
                                                   Language Lab 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 3, Rm 100 // Bldg 4, Rm 121</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-4250 // 407-582-4146</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 8:00pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../osceola/math/mathdepot.html">
                                                   Math and Science Tutoring 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 4, Rm 121</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-4856</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 8:00pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../osceola/writing-center/index.html">
                                                   Writing Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 3, Rm 100</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-4250</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr"> 
                                          
                                          <div data-old-tag="td">
                                             <a href="mailto:oscwritingcenter@valenciacollege.edu">oscwritingcenter@valenciacollege.edu</a>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../osceola/learningcenter/tutoring.html">
                                                   A.S. Degree Tutoring 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 3, Rm 100</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-4146</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 8:00pm<br>Friday: 8am to 5pm<br> Saturday: 8am-12pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../osceola/mainlab/language.html">
                                                   Language Lab 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 3, Rm 100</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-4146</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 8:00pm<br>Friday: 8am to 5pm<br>Saturday: 8am to 12pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Poinciana</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../poinciana/index.html">
                                                   The Plaza (Tutoring) 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Room 231</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-6118</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr"> 
                                          
                                          <div data-old-tag="td">
                                             <a href="mailto:theplaza@valenciacollege.edu">theplaza@valenciacollege.edu</a>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Mon-Thurs: 8am-7pm<br>Friday: 8am-3pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>West</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="communications/index.html">
                                                   Communications Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 5, Rm 155</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-1812</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 8pm<br>Friday: 8am to 5pm<br>Saturday: 9am to 2pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="math/index.html">
                                                   Math Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 7, Rm 240</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-1780 or 407-582-1720</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 9 am to 8 pm<br>Friday: 9 am to 7 pm<br>Saturday: 10 am to 3 pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="communications/index.html">
                                                   Writing Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 5, Rm 155</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-5454</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am-8pm<br>Friday: 8am-5pm<br>Saturday: 9am-2pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="tutoring/index.html">
                                                   Tutoring 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 7, Rm 240</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-1633</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="tutoring/index.html">
                                                   Tutoring: Foreign Language 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 7, Rm 240</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-1633</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Winter Park</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../wp/mathcenter/index.html">
                                                   Math Support Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 1, Rm 138</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-6817 or 407-582-6912</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to noon and 1pm to 5pm<br>Friday: 8am to 11:30am and noon to 3 pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../wp/cssc/index.html">
                                                   Communications Student Support Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 1, Rm 136</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-6818</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 5pm<br>Friday: 8am to 3pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <div data-old-tag="table">
                                    
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>
                                                
                                                <a href="../wp/cssc/index.html">
                                                   Communications Student Support Center 
                                                   </a>
                                                
                                                </strong></div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Bldg 1, Rm 136</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">407-582-6818</div>
                                          
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Friday: 8am to 3pm<br>
                                             <br>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3><strong><u>Online Support through Smarthinking</u></strong></h3>
                        
                        
                        
                        <p>As a Valencia student you also have free access to Smarthinking, an online academic
                           support program, which is available up to 24 hours a day, 7 days a week through your
                           Atlas account.
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                        
                        
                        <a href="../math/liveScribe.html" title="Math Help 24/7"><img alt="Math Help 24/7" border="0" height="60" src="math-24-7_245x60.png" width="245"></a>
                        
                        
                        <a href="../student-services/skillshops.html"><img border="0" src="SkillShops_270x60.png" width="100%"></a>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/learning-centers.pcf">©</a>
      </div>
   </body>
</html>