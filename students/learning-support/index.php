<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Support | Students | Valencia College  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Support</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-4">
                        
                        <h3>Resources &amp; Services</h3>
                        
                        <ul>
                           
                           <li><a href="/students/labs/index.php">Computer Labs</a></li>
                           
                           <li><a href="/students/learning-support/learning-centers.php">Learning Centers</a></li>
                           
                           <li><a href="/students/learning-support/skillshops.php">Skillshops</a></li>
                           
                           <li><a href="/students/testing-center/index.html">Testing Center</a></li>
                           
                           <li><a href="/students/learning-support/writing-consultations.php">Writing Consultations</a></li>
                           
                           <li><a href="http://net4.valenciacollege.edu/forms/learning-support/contact.cfm" target="_blank">Contact Us</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <div class="col-md-8">
                        
                        <h3>Academic Support</h3>
                        
                        <p>Each Campus has Learning Support Centers to help you succeed</p>
                        
                        <ul>
                           
                           <li><a href="/learning-support/east/">East</a></li>
                           
                           <li><a href="/learning-support/lake-nona/">Lake Nona</a></li>
                           
                           <li><a href="/learning-support/osceola/">Osceola</a></li>
                           
                           <li><a href="/learning-support/poinciana/">Poinciana</a></li>
                           
                           <li><a href="/learning-support/west/">West</a></li>
                           
                           <li><a href="/learning-support/winter-park/">Winter Park</a></li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <h3>Enhanced Courses</h3>
                     
                     <div class="row">
                        
                        <div class="col-md-3 col-sm-3">
                           
                           <h3>Learning in Communities</h3>
                           
                           <p>Courses are paired together and instructors "team teach" <br> 
                           </p>
                           
                        </div>
                        
                        <div class="col-md-3 col-sm-3">
                           
                           <h3>Supplemental Learning</h3>
                           
                           <p>Classes supported by small group sessions. <br> 
                           </p>
                           
                        </div>
                        
                        <div class="col-md-3 col-sm-3">
                           
                           <h3>Service Learning</h3>
                           
                           <p>A teaching and learning strategy integrating meaningful community service with instruction.
                              
                           </p>
                           
                        </div>
                        
                        <div class="col-md-3 col-sm-3">
                           
                           <h3>Continuous Assessment and Responsive Engagement (CARE)</h3>
                           
                           <p>A faculty designed and led early alert system.</p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/index.pcf">©</a>
      </div>
   </body>
</html>