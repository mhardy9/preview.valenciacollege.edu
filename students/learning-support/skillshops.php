<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/skillshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li>Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Skillshops</h2>
                        
                        <p>
                           In an effort to expand humanities, education, and science tutorial services, the Tutoring
                           Center 
                           is currently offering selected education, science, and humanities workshops. 
                           
                        </p>
                        
                        <p>
                           Workshop sessions will maximize retention and develop a greater understanding of course
                           material. Contact the East Campus Academic 
                           Success Center Information Desk for more details.
                           
                        </p>
                        
                        
                        <h3>Upcoming Skillshops</h3>
                        
                        
                        
                        <div data-id="skillshop_the_dreadful_conversation_a_crucial_chat_i_dont_want_to_have">
                           
                           
                           <div>
                              Oct<br>
                              <span>18</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/skillshop_the_dreadful_conversation_a_crucial_chat_i_dont_want_to_have" target="_blank">Skillshop: The Dreadful Conversation: A Crucial Chat I Don't Want to Have at West
                                 Campus UCF Building 11</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="skillshop_unlocking_the_mystery_the_why_behind_the_buy">
                           
                           
                           <div>
                              Oct<br>
                              <span>18</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/skillshop_unlocking_the_mystery_the_why_behind_the_buy" target="_blank">Skillshop: Unlocking the Mystery: The Why Behind the Buy at East Campus Building 8</a><br>
                              
                              <span>4:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="the_why_behind_the_buy">
                           
                           
                           <div>
                              Oct<br>
                              <span>18</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/the_why_behind_the_buy" target="_blank">The Why? Behind the Buy at East Campus Building 8</a><br>
                              
                              <span>4:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="be_a_warrior_not_a_victim_3555">
                           
                           
                           <div>
                              Oct<br>
                              <span>19</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/be_a_warrior_not_a_victim_3555" target="_blank">Be a Warrior, Not a Victim at Osceola Campus</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="skillshop_goal_setting_the_process_of_achieving_success_6612">
                           
                           
                           <div>
                              Oct<br>
                              <span>19</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/skillshop_goal_setting_the_process_of_achieving_success_6612" target="_blank">Skillshop: Goal Setting: The Process of Achieving Success at East Campus Building
                                 3</a><br>
                              
                              <span>5:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="booosting_your_career_options_6864">
                           
                           
                           <div>
                              Oct<br>
                              <span>23</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/booosting_your_career_options_6864" target="_blank">BOOOsting Your Career Options at West Campus Building 8 – Special Events Center</a><br>
                              
                              <span>12:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="civic_engagement_making_a_difference">
                           
                           
                           <div>
                              Oct<br>
                              <span>24</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/civic_engagement_making_a_difference" target="_blank">Civic Engagement: Making a Difference at Lake Nona Campus</a><br>
                              
                              <span>10:00 AM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="its_your_choice_make_it_healthy_6231">
                           
                           
                           <div>
                              Oct<br>
                              <span>24</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/its_your_choice_make_it_healthy_6231" target="_blank">It's Your Choice, Make it Healthy at Osceola Campus</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="skillshop_identity_privilege-_finding_common_understanding">
                           
                           
                           <div>
                              Oct<br>
                              <span>24</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/skillshop_identity_privilege-_finding_common_understanding" target="_blank">Skillshop: Identity Privilege- Finding Common Understanding at West Campus Student
                                 Services Building (SSB)</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div data-id="skillshop_recognizing_emotional_distress">
                           
                           
                           <div>
                              Oct<br>
                              <span>24</span>
                              
                           </div>
                           
                           
                           <div>        
                              <a href="http://events.valenciacollege.edu/event/skillshop_recognizing_emotional_distress" target="_blank">Skillshop: Recognizing Emotional Distress at East Campus Building 5</a><br>
                              
                              <span>1:00 PM</span>
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                        
                        
                        <a href="../math/liveScribe.html" title="Math Help 24/7"><img alt="Math Help 24/7" border="0" height="60" src="math-24-7_245x60.png" width="245"></a>
                        
                        
                        <a href="../student-services/skillshops.html"><img border="0" src="SkillShops_270x60.png" width="100%"></a>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/skillshops.pcf">©</a>
      </div>
   </body>
</html>