<ul>
	<li><a href="/students/learning-support/">Learning Support</a> </li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/browse-by-campus.php">Learning Centers <span class="caret"></span></a> 
	<ul>
	<li><a href="/students/learning-support/east/">East Campus</a></li>
	<li><a href="/students/learning-support/lake-nona/">Lake Nona Campus</a></li>
		<li><a href="/students/learning-support/osceola/">Osceola Campus</a></li>
		<li><a href="/students/learning-support/poinciana/">Poinciana Campus</a></li>
		<li><a href="/students/learning-support/west/">West Campus</a></li>
		<li><a href="/students/learning-support/winter-part/">Winter Park Campus</a></li>
	</ul>
	</li>
		<li><a href="/students/learning-support/skillshops.php">Skillshops</a> </li>
	<li><a href="/students/learning-support/writing-consultations.php">Writing Consultations</a></li>    
	<li><a href="http://net4.valenciacollege.edu/forms/students/learning-support/contact.cfm" target="_blank">Contact Us</a></li>
</ul>