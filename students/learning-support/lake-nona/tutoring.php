<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/lake-nona/">Lake Nona Learning Support</a></li>
               <li>Lake Nona Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Tutoring </h2>
                        
                        <h3>Welcome</h3>
                        
                        <p>Welcome to the Lake Nona Learning Support Tutoring website. We want you to be successful
                           in your current classes, but most importantly help obtain the skills necessary to
                           be successful in all of your future classes. 
                        </p>
                        
                        
                        
                        <h3>Walk-In Tutoring </h3>
                        
                        <ul>
                           
                           <li><a href="../../locations/lakenona/tutoring-business.html">Business</a></li>
                           
                           <li><a href="../../locations/lakenona/tutoring-math.html">Mathematics</a></li>
                           
                           <li><a href="../../locations/lakenona/tutoring-science.html">Science</a></li>
                           
                           <li><a href="../../locations/lakenona/tutoring-english.html">English/Reading</a></li>              
                           
                           <li><a href="../../locations/lakenona/tutoring-spanish.html">Spanish</a></li>
                           
                           <li><a href="../../locations/lakenona/tutoring-pert-review.html">PERT Review</a></li>
                           
                        </ul>
                        
                        <p><span>***<u>Note</u>: TUTOR AVAILABILITY IS NOT GUARANTEED. </span>If we receive advance notice of tutor absence, we will post the notice on the appropriate
                           webpage. We encourage you to call if you need last minute details (407-582-7106 between
                           9am and 5pm). 
                        </p>
                        
                        
                        <h3>Recommended Resources </h3>
                        
                        <p><a href="../../locations/lakenona/TutoringResources.html">Recommended Resources</a> 
                        </p>
                        
                        
                        <h3>How to Become a Tutor </h3>
                        
                        <p><a href="../../locations/lakenona/HowtoBecomeaTutor.html">Apply Here</a> 
                        </p>
                        
                        
                        <h3>Online Tutoring </h3>
                        
                        <p>Need additional help outside of our tutoring hours? Valencia College provides online
                           tutoring  for all  students through SmartThinking (yes, it's FREE!!!). You can access
                           SmartThinking in Atlas and/or Blackboard.
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Atlas</strong>                
                              
                              <ul>
                                 
                                 <li>Courses Tab
                                    
                                    <ul>
                                       
                                       <li>My Courses  Channel
                                          
                                          <ul>
                                             
                                             <li>Tutoring (online) - Smart Thinking </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              <strong>Blackboard
                                 </strong>                
                              
                              <ul>
                                 
                                 <li>Resources (Left pane of Blackboard)
                                    
                                    <ul>
                                       
                                       <li>Tools
                                          
                                          <ul>
                                             
                                             <li>Smart Thinking Login</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Contact Us </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Kevin Beers </strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong><u>Phone</u></strong>: 407-582-7145
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong><u>Email</u></strong>: <a href="mailto:kbeers1@valenciacollege.edu">kbeers1@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Theresa Koehler </strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong><u>Phone</u></strong>: 407-582-7109
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong><u>Email</u></strong>: <a href="mailto:tkoehler@valenciacollege.edu">tkoehler@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           <div>Tutoring Hours and Location</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../../locations/lakenona/tutoring.html">
                                             
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 230</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-7106</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 9am - 7pm<br>Friday: 10am - 4pm<br>Saturday: 9am - 1pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <p>***<u>Note</u>: Please see desired subject for subject specific tutoring hours 
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/lake-nona/tutoring.pcf">©</a>
      </div>
   </body>
</html>