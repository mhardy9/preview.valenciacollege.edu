<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Lake Nona Learning Support Services | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/lake-nona/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Learning Support</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li>Lake Nona Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     		
                     <h3>Lake Nona Learning Support Services</h3>
                     	  
                     <div class="row">
                        
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/students/learning-support/lake-nona/tutoring-math.php"> <span class="far fa-plus-square"></span>
                              
                              <h3>Math Learning Center</h3>
                              
                              <p>
                                 Location: 1-230<br>
                                 Phone: 407-582-7106<br>
                                 	  	Hours: Monday - Thursday: 9:00am - 7:00pm<br>Friday: 10:00am - 4:00pm<br>Saturday: 9:00am - 1:00pm
                                 
                              </p>
                              </a></div>
                        		  
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/students/learning-support/lake-nona/tutoring-english.php"> <span class="far fa-book"></span>
                              
                              <h3>Reading, Writing, Communication, and Speech Learning Center</h3>
                              
                              <p>
                                 Location: 1-230<br>
                                 Phone: 407-582-7106<br>
                                 	  	Hours: Monday - Thursday: 10:00am - 6:00pm<br>Friday: 10:00am - 4:00pm
                                 
                              </p>
                              </a></div>
                        		  
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/students/learning-support/lake-nona/tutoring.php"> <span class="far fa-clipboard"></span>
                              
                              <h3>General Tutoring</h3>
                              
                              <p>
                                 Location: Bldg 1, Rm 230<br>
                                 Phone: 407-582-7106<br>
                                 	  	Hours: Monday - Thursday: 9am - 7pm<br>Friday: 10am - 4pm<br>Saturday: 9am - 1pm
                                 
                              </p>
                              </a></div>
                        		  
                     </div>
                     	  
                     <div class="row">
                        		  
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/students/learning-support/lake-nona/tutoring-business.php"> <span class="far fa-briefcase"></span>
                              
                              <h3>Business Tutoring</h3>
                              
                              <p>
                                 
                                 
                              </p>
                              </a></div>
                        		  
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/students/learning-support/lake-nona/tutoring-science.php"> <span class="far fa-flask"></span>
                              
                              <h3>Science Tutoring</h3>
                              
                              <p>
                                 
                                 
                              </p>
                              </a></div>
                        		  
                        <div class="col-md-4 col-sm-4"><a class="box_feat" href="/students/learning-support/lake-nona/tutoring-spanish.php"> <span class="far fa-globe"></span>
                              
                              <h3>Spanish Tutoring</h3>
                              
                              <p>
                                 
                                 
                              </p>
                              </a></div>
                        		  
                        		  
                     </div>
                     
                     
                     	  
                     	  
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/lake-nona/index.pcf">©</a>
      </div>
   </body>
</html>