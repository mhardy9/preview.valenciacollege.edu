<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-math.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/lake-nona/">Lake Nona Learning Support</a></li>
               <li>Lake Nona Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Mathematics Tutoring </h2>
                        
                        <h3>Goals</h3>
                        
                        <ul>
                           
                           <li>Promote positive beliefs and attitudes towards mathematics</li>
                           
                           <li>Value prior knowledge</li>
                           
                           <li>Make connections between prior knowledge, the world of the student, and the strands
                              and actions of mathematics
                           </li>
                           
                        </ul>
                        
                        <h3><span><strong>Fall 2017</strong> Hours </span></h3>
                        
                        <p><strong>MAT0018C, MAT0022C, MAT0028C, MAT1033C, MAC1105, MAC1114, MAC1140, MAC1147, MAC2233,
                              MAC2311, MGF1106, MGF1107, STA1001C, and STA2023</strong> 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday - Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Friday:</strong> 10:00am - 4:00pm
                           </li>
                           
                           <li>
                              <strong>Saturday:</strong> 9:00am - 1:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>MAC2312</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 9:00am - 7:00pm
                              
                           </li>
                           
                           <li>
                              <strong>Tuesday:</strong> 9:00am - 4:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Friday:</strong> 10:00am - 4:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>MAC2313</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 9:00am - 1:30pm
                           </li>
                           
                           <li>
                              <strong>Tuesday:</strong> 10:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 9:00am - 1:30pm
                           </li>
                           
                           <li>
                              <strong>Thursday: </strong>10:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Friday: </strong>10:00am - 4:00pm
                           </li>
                           
                           <li>
                              <strong>Saturday:</strong> 9:00am - 1:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>MAP2302</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Tuesday:</strong> 10:00am - 4:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday: </strong>11:30am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 10:00am - 4:00pm
                           </li>
                           
                        </ul>
                        
                        <p>***<u>Note</u>: TUTOR AVAILABILITY IS NOT GUARANTEED. If we receive advance notice of tutor absence,
                           we will post the notice on the appropriate webpage. We encourage you to call if you
                           need last minute details (407-582-7106). 
                        </p>
                        
                        <h3>Intermediate Algebra Lab Materials </h3>
                        
                        <p><a href="../../locations/lake-nona/documents/Lab-Manual-Student-Version.pdf" target="_blank">MAT1033C Intermediate Algebra Lab Manual</a></p>
                        
                        <p><br>
                           
                        </p>
                        
                        <p>Lab Week 8 - Factoring Trinomials</p>
                        
                        <ul>
                           
                           <li><a href="../../locations/lake-nona/documents/Method1-TrialandError.pdf" target="_blank">Method 1 - Trial and Error</a></li>
                           
                           <li><a href="../../locations/lake-nona/documents/Method2-ACMethod.pdf" target="_blank">Method 2 - AC Method</a></li>
                           
                           <li><a href="../../locations/lake-nona/documents/Method3-SlideandDivide.pdf" target="_blank">Method 3 - Slide and Divide</a></li>
                           
                           <li>
                              <a href="../../locations/lake-nona/documents/Method4-BoxMethod.pdf" target="_blank">Method 4 - Box Method</a> 
                           </li>
                           
                        </ul>
                        
                        <h3>Recommended Math Resources </h3>
                        
                        <p><a href="../../locations/lakenona/mathresources.html" target="_blank">Math Resources </a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Lake Nona Learning Support</strong><br>407-582-7100
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Student Services</strong><br>407-582-1507
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:jhernandez71@valenciacollege.edu">jhernandez71@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>12350 Narcoossee Rd<br>Orlando, FL 32832
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/lake-nona.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/lake-nona/tutoring-math.pcf">©</a>
      </div>
   </body>
</html>