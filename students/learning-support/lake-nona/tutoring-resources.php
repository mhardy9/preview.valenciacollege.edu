<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/lake-nona/">Lake Nona Learning Support</a></li>
               <li>Lake Nona Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Recommended Resources </h2>
                        
                        <p>Most students that struggle with being successful in college are actually smart enough
                           and have the intelligence to succeed. So why do so many students fail and never finish?
                           Well, the answer isn't as simple as you might think. Hundreds of studies and research
                           projects have been performed in an effort to answer the question. Some progress has
                           been made in identifying what a college student needs in order to be successful and
                           complete their college degree. One of the items discovered is related to what is frequently
                           called "student success skills". From an inability to manage time appropriately to
                           a lack of motivation, these self-management skills are important for success in college
                           and are also crucial for success in a career and life in general.
                        </p>
                        
                        <p>"Student success courses help students build knowledge and skills essential for success
                           in college, from study and time-management skills to awareness of campus facilities
                           and support services. When these courses are required, students are more likely to
                           complete courses, earn better grades, have higher overall GPAs, and obtain degrees."
                           [Moore, C., &amp; Shulock, N. (2009). <em>Student progress toward degree completion: Lessons from the research literatur</em>e. Retrieved from <a href="http://www.csus.edu/ihelp/PDFs/R_Student_Progress_Toward_Degree_Completion.pdf">http://www.csus.edu/ihelp/PDFs/R_Student_Progress_Toward_Degree_Completion.pdf</a>] 
                        </p>
                        
                        <p><strong>Here are some resources we have found to be helpful in developing student success
                              skills:</strong></p>
                        
                        <blockquote>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong><u>Time Management and Scheduling</u>:</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.dartmouth.edu/~acskills/success/time.html">http://www.dartmouth.edu/~acskills/success/time.html</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.timemanagementhelp.com/college.htm">http://www.timemanagementhelp.com/college.htm</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.alliedoffices.com/articles/a-professionals-guide-to-office-time-management.html" target="_blank">http://www.alliedoffices.com/articles/a-professionals-guide-to-office-time-management.html</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li>
                                             <a href="http://www.affordablecollegesonline.org/college-resource-center/study-skills/" target="_blank">http://www.affordablecollegesonline.org/college-resource-center/study-skills/</a> 
                                          </li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong><u>Organization</u>:</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.youtube.com/watch?v=CxZw-3Rl4vs">http://www.youtube.com/watch?v=CxZw-3Rl4vs</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong><u>Note Taking</u>: </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.dartmouth.edu/~acskills/success/notes.html">http://www.dartmouth.edu/~acskills/success/notes.html</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.affordablecollegesonline.org/college-resource-center/study-skills/" target="_blank">http://www.affordablecollegesonline.org/college-resource-center/study-skills/</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong><u>Know Your Learning Style and How to Use It</u>: </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <ul>
                                          
                                          <li><a href="http://www.dartmouth.edu/~acskills/success/selftest.html">http://www.dartmouth.edu/~acskills/success/selftest.html</a></li>
                                          
                                       </ul>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </blockquote>            
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Lake Nona Learning Support</strong><br>407-582-7100
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Student Services</strong><br>407-582-1507
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:jhernandez71@valenciacollege.edu">jhernandez71@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>12350 Narcoossee Rd<br>Orlando, FL 32832
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/lake-nona.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/lake-nona/tutoring-resources.pcf">©</a>
      </div>
   </body>
</html>