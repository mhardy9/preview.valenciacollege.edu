<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/lake-nona/tutoring-science.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Lake Nona Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/lake-nona/">Lake Nona Learning Support</a></li>
               <li>Lake Nona Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Science Tutoring </h2>
                        
                        <h3>Fall 2017 Hours </h3>
                        
                        <p><strong>BSC1005C and BSC1010C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday - Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Friday:</strong> 10:00am - 4:00pm
                           </li>
                           
                           <li>
                              <strong>Saturday:</strong>: 9:00am - 1:00pm
                           </li>
                           
                           
                        </ul>
                        
                        <p><strong>BSC1011C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 2:00pm - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Tuesday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           
                        </ul>
                        
                        <p><strong>BSC1020</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 10:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Tuesday:</strong> 10:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 10:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           
                        </ul>
                        
                        <p><strong>BSC1026</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Tuesday:</strong> 2:00pm - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 2:00pm - 7:00pm
                           </li>
                           
                           
                        </ul>
                        
                        <p><strong>CHM1020, CHM1025C, CHM1045C and CHM1046C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday - Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Friday:</strong> 10:00am - 4:00pm
                           </li>
                           
                           <li>
                              <strong>Saturday:</strong>: 9:00am - 1:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>CHM2210C and CHM2211C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday - Thursday:</strong> 9:00am - 7:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>MCB2010C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Tuesday:</strong> 9:00am - 3:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 9:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 9:00am - 4:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>PHY2053C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 11:30am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Tuesday:</strong> 10:00am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 11:30am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Thursday:</strong> 10:00am - 7:00pm
                           </li>
                           
                        </ul>
                        
                        <p><strong>PHY2054C</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Monday:</strong> 11:30am - 7:00pm
                           </li>
                           
                           <li>
                              <strong>Wednesday:</strong> 11:30am - 7:00pm
                           </li>
                           
                        </ul>
                        
                        <p>***<u>Note</u>: TUTOR AVAILABILITY IS NOT GUARANTEED. If we receive advance notice of tutor absence,
                           we will post the notice on the appropriate webpage. We encourage you tcall if you
                           need last minute details (407-582-7106). 
                        </p>
                        
                        
                        <h3>Resources</h3>
                        
                        <p><strong>Biology:</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.zygotebody.com/" target="_blank">Google Body / Zygote Body</a></li>
                           
                        </ul>
                        
                        <p><strong>Chemistry:</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.ptable.com/" target="_blank">Periodic Table </a></li>
                           
                           <li>
                              <a href="http://molview.org/" target="_blank">MolView</a> - Draw structures and export as .png files 
                           </li>
                           
                           <li>
                              <a href="https://web.chemdoodle.com/demos/sketcher/" target="_blank">ChemDoodle</a> - Draw structures and screenshot tsave
                           </li>
                           
                           <li>
                              <a href="https://web.chemdoodle.com/demos/simulate-nmr-and-ms/" target="_blank">ChemDoodle</a> - NMR &amp; MS Prediction Tools
                           </li>
                           
                           <li>
                              <a href="http://www.nmrdb.org/new_predictor/index.shtml?v=v2.71.3" target="_blank">nmrdb.org</a> - NMR Prediction Tools
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Apply Now</a>
                        
                        
                        
                        <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Schedule a Tour</a>
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>CONTACT</div>
                           <br>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Lake Nona Learning Support</strong><br>407-582-7100
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <strong>Student Services</strong><br>407-582-1507
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="mailto:jhernandez71@valenciacollege.edu">jhernandez71@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>12350 Narcoossee Rd<br>Orlando, FL 32832
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" target="_blank">
                           Directions</a>
                        <a href="../../locations/map/lake-nona.html" target="_blank">
                           Campus Map</a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/lake-nona/tutoring-science.pcf">©</a>
      </div>
   </body>
</html>