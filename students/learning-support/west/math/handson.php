<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/handson.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="../index.html">Learning Support Services Home</a></li>
                                                   
                                                   <li><a href="../communications/index.html">Communications - West</a></li>
                                                   
                                                   <li><a href="../testing/index.html">Testing - West</a></li>
                                                   
                                                   <li><a href="../tutoring/index.html">Tutoring - West</a></li>
                                                   
                                                   <li><a href="../../west/math/index.html">Math Division - West</a></li>
                                                   
                                                   <li><a href="../../east/academicsuccess/math/index.html">East Math Center</a></li>
                                                   
                                                   <li><a href="../../osceola/learningcenter/tutoring2.html">Osceola Tutoring</a></li>
                                                   
                                                   <li><a href="../../wp/mathcenter/index.html">Winter Park Math Center</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a name="content" id="content"></a>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      <img alt="Hands On Learning" height="347" src="HandsOnLearning.jpg" width="770">
                                                      
                                                      
                                                      <h3>
                                                         <img alt="Hands-On Collage" height="275" src="ExActForPPT13.jpg" width="490">Hours of Operation
                                                      </h3>
                                                      
                                                      <h4>Hands-On Learning:</h4>
                                                      
                                                      <p> Monday to Thursday - 10 am to 5 pm<br>
                                                         Friday: CLOSED<br>
                                                         Saturday: CLOSED
                                                      </p>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h3><strong>Calculator Videos</strong></h3>
                                                      
                                                      <ul>
                                                         
                                                         <li><a href="http://www.screencast.com/t/h4cdc8isGoLZ">MAT1033C</a></li>
                                                         
                                                         <li>
                                                            <a href="http://www.screencast.com/t/7VlGsWpSxa">STA1001C</a> 
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      <p>Workshops (Just Walk-In) </p>
                                                      
                                                      <h4>MAT0018, 0022, 0028 Schedule</h4>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="th">Topic</div>
                                                               
                                                               <div data-old-tag="th">Day</div>
                                                               
                                                               <div data-old-tag="th">Date</div>
                                                               
                                                               <div data-old-tag="th">Start</div>
                                                               
                                                               <div data-old-tag="th">End</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Fractions and Equations </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">9/21/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Equations </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">9/25/2017</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Equations </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">9/25/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Problem Solving </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/26/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Proportions and Ratios </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/2/2017</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ratios and Percentages </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/2/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Mental Math </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/5/2017</div>
                                                               
                                                               <div data-old-tag="td">10:30    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Inequalities </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/9/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Problem Solving </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/9/2017</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Solving Equations and Inequalities </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/10/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Midterm </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/16/2017</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Midterm Review </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/16/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Polynomials and Why We Love Them </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">10/16/2017</div>
                                                               
                                                               <div data-old-tag="td">11:00    AM</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Polynomials </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/26/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">factoring </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">11/6/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring Boot Camp (GCF or    Tri) </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">11/6/2017</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplifying Radicals </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">11/20/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">11/27/2017</div>
                                                               
                                                               <div data-old-tag="td">12:00    PM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing </div>
                                                               
                                                               <div data-old-tag="td">Monday</div>
                                                               
                                                               <div data-old-tag="td">11/27/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">4:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/28/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Radicals </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/30/2017</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:30    PM</div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <h4>MAT1033 Schedule</h4>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="th">Topic</div>
                                                               
                                                               <div data-old-tag="th">Day</div>
                                                               
                                                               <div data-old-tag="th">Date</div>
                                                               
                                                               <div data-old-tag="th">Start</div>
                                                               
                                                               <div data-old-tag="th">End</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 2 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/19/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring Boot Camp </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/19/2017</div>
                                                               
                                                               <div data-old-tag="td">9:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring Boot Camp </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/19/2017</div>
                                                               
                                                               <div data-old-tag="td">9:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:00    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">TI-84+ </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/19/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 2 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">9/21/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 3 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">9/21/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring Boot Camp </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">9/21/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 3 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/26/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">TI-84+ </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">9/26/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 3 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">9/28/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 3 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">9/28/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 3 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/3/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Mental Math </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/5/2017</div>
                                                               
                                                               <div data-old-tag="td">10:30    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing (Paper and Pencil) </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/5/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring Boot Camp </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/10/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">2:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 6 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/17/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 6 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/17/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring with Algebra Tiles/Boot    Camp </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/17/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring boot camp </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/19/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">3:45    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">TI-84+ </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/19/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring with Algebra Tiles </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/24/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 6 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/26/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 6 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">10/26/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 3, 4 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/31/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 6 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">10/31/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplifying Radicals </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/2/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 7 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/7/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplify Radicals </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/7/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplifying Radicals </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/9/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Stairstep Radicals </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/9/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 7 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/14/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 7 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/16/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 7 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/16/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 7 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/16/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Completing the Square </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/21/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">TI-84+ (Quadratics) </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/21/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 8 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/28/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Completing the Square </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">11/28/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 8 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/30/2017</div>
                                                               
                                                               <div data-old-tag="td">10:00    AM</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 8 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">11/30/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15 PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 8 Lab </div>
                                                               
                                                               <div data-old-tag="td">Tuesday</div>
                                                               
                                                               <div data-old-tag="td">12/5/2017</div>
                                                               
                                                               <div data-old-tag="td">11:30    AM</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 4 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">12/7/2017</div>
                                                               
                                                               <div data-old-tag="td">12:45    PM</div>
                                                               
                                                               <div data-old-tag="td">2:15    PM</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Ch 8 Lab </div>
                                                               
                                                               <div data-old-tag="td">Thursday</div>
                                                               
                                                               <div data-old-tag="td">12/7/2017</div>
                                                               
                                                               <div data-old-tag="td">1:00    PM</div>
                                                               
                                                               <div data-old-tag="td">3:00    PM</div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <h3>Our Mission</h3>
                                                      
                                                      <p>Serve as a resource for Developmental Math Students (MAT0018C, MAT0022C, MAT0028C,
                                                         and MAT1033C) to gain better understanding of mathematical and algebraic concepts
                                                         using a <u>hands-on</u> approach.
                                                      </p>
                                                      
                                                      
                                                      <h3>Our Goals</h3>
                                                      
                                                      
                                                      <ul>
                                                         
                                                         <li>Assist students in understanding mathematical concepts using manipulatives.      
                                                            
                                                         </li>
                                                         
                                                         <li>                Provide students with an accessible and comfortable space where they
                                                            can perform hands-on activities as individuals or as a group.              
                                                         </li>
                                                         
                                                         <li>                Inform and train faculty on how to incorporate manipulatives during
                                                            classroom lessons.              
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <p>Please call 407-582-5408 or contact <a href="mailto:cwatson17@valenciacollege.edu">Courtney Watson </a> for more information
                                                      </p>
                                                      
                                                      <p><a href="#top">TOP</a></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/handson.pcf">©</a>
      </div>
   </body>
</html>