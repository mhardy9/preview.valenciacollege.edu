<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Math Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/mission.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Math Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li>West Campus Math Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Mission Statement</h2>
                        
                        <p>Support, enhance, and celebrate; teaching and   learning in Mathematics.</p>
                        
                        <strong> </strong>
                        
                        <h3>Design Principles</h3>
                        
                        
                        <ul>
                           
                           <li>The Math Center will provide a welcoming, inviting,       energizing space to create
                              a learning community in mathematics.
                           </li>
                           
                           <li>The Math Center will accommodate all learning styles.</li>
                           
                           <li>The design team for the Math Center Initiative will       collaborate with all stakeholders
                              (math department students, faculty, staff,       and administration).
                           </li>
                           
                           <li>The Math Center will use the most effective technology       available, using standard
                              interfaces when possible.
                           </li>
                           
                           <li>The Math Center’s learning space and resources will allow       for flexibility and
                              adaptability.
                           </li>
                           
                           <li>All stakeholders will be aware of the mission, resources, and 
                              operations of the Math Center.
                           </li>
                           
                           <li>The Math Center will be staffed by trained individuals       with appropriate expertise.</li>
                           
                           <li>The Math Center will <span lang="en-us" xml:lang="en-us">m</span>ake       effective use of available resources.
                           </li>
                           
                           <li>The Math Center’s resources, operations and effectiveness       in accomplishing its
                              mission will be assessed annually.
                           </li>
                           
                           <li>The Math Center’s layout will conform to all       college safety and access standards.</li>
                           
                        </ul>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/mission.pcf">©</a>
      </div>
   </body>
</html>