<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Math Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/cpt.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Math Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li>West Campus Math Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <p> MATH PERT REVIEW ASSISTANCE IS OFFERED IN BUILDING 7, ROOM 241.</p>
                        
                        <h3>Hours for assistance with PERT math review:              </h3>
                        
                        <h4 dir="auto">Fall 2017 PERT Review</h4>
                        
                        <h4 dir="auto">Monday to Thursday: 12 pm to 6 pm<br>
                           Saturday: 11 am to 2 pm
                        </h4>
                        
                        
                        <p><strong>Students must bring their PERT scores as well as their PERT Review book to 7-241.
                              </strong></p>
                        
                        <h3><strong>Please note, there is a $10 retake fee per subsection of the PERT.</strong></h3>
                        
                        
                        <h3>Assessment Placement Chart            </h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Math Placement Scores</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Placement</div>
                                 
                                 <div data-old-tag="th">PERT</div>
                                 
                                 <div data-old-tag="th">CPT</div>
                                 
                                 <div data-old-tag="th">SAT</div>
                                 
                                 <div data-old-tag="th">ACT</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dev. Mathmatics I<br>
                                    MAT 0018C or MAT 0022C
                                 </div>
                                 
                                 <div data-old-tag="td">50-95</div>
                                 
                                 <div data-old-tag="td">&amp;lt; 41</div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Dev. Mathmatics I<br>
                                    MAT 0022C or MAT 0028C
                                 </div>
                                 
                                 <div data-old-tag="td">96-113</div>
                                 
                                 <div data-old-tag="td">42-71</div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                                 <div data-old-tag="td">N/A</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Statistics for Undergaduates<br>
                                    STA 1001C<br><br>
                                    College Math<br>
                                    MGF 1106
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">96 or more</div>
                                 
                                 <div data-old-tag="td">42 or more</div>
                                 
                                 <div data-old-tag="td">440 or more</div>
                                 
                                 <div data-old-tag="td">19 or more</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Intermediate Algebra<br>
                                    MAT 1033C
                                 </div>
                                 
                                 <div data-old-tag="td">114-122</div>
                                 
                                 <div data-old-tag="td">72-89</div>
                                 
                                 <div data-old-tag="td">440-499</div>
                                 
                                 <div data-old-tag="td">19 or 20</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">College Level Math*<br>
                                    MAC 1105 or STA 2023
                                 </div>
                                 
                                 <div data-old-tag="td">123-150</div>
                                 
                                 <div data-old-tag="td">90 or more</div>
                                 
                                 <div data-old-tag="td">500 or more</div>
                                 
                                 <div data-old-tag="td">21 or more</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">* Take CPT-I testing for possible higher placement (optional)</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">CPTI Placement</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">64 or less</div>
                                 
                                 <div data-old-tag="td">MAC 1105: College Algebra</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">65 or more</div>
                                 
                                 <div data-old-tag="td">MAC 1114: College Trigonometry<br>
                                    MAC 1140: Pre-Calculus Algebra<br>
                                    MAC 2233: Calculus for Business and Social Science<br>
                                    MAE 2801: Elementary School Math<br>
                                    MHF 2300: Logic and Proof in Math
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">89 or more</div>
                                 
                                 <div data-old-tag="td">MAC 2311: Calculus with Analytical Geometry I</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3><strong>Read the following to prepare for your PERT/CPT I math review:</strong></h3>
                        
                        <p>The Math PERT Review Workbook is available in the Valencia bookstore or click on the
                           link below to print it:            
                        </p>
                        
                        <ul>
                           
                           <li><a href="../../east/academicsuccess/math/documents/PERTmathbooklet.pdf" target="_blank">PERT Review Booklet</a></li>
                           
                           <li>
                              <a href="documents/CPT-collegelevelIbooklet7-9.pdf" target="_blank">CPT-i booklet</a> 
                           </li>
                           
                        </ul>
                        
                        
                        <p>Come to the Math Center for your PERT/CPT review. You may complete problems in the
                           workbook in advance, or you may complete them in the Math Center. If you complete
                           problems in advance, bring all your work with you, <strong>showing in detail all steps you used to solve the problems</strong>. 
                        </p>
                        
                        <p>Obtain a record of your exact PERT/CPT math scores. Your scores indicate which parts
                           of the workbook you will complete for your PERT/CPT review (see table below).    
                           
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <h3>PERT Scores</h3>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <h3>Workbook Sections to Complete</h3>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">50-95                  </div>
                                 
                                 <div data-old-tag="td">Complete <strong>Parts 1 &amp; 2</strong> of workbook
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">96-113</div>
                                 
                                 <div data-old-tag="td">Complete <strong>Part 3 </strong>of workbook
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">114-122</div>
                                 
                                 <div data-old-tag="td">Complete <strong>Part 4 </strong>of workbook
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">123-150</div>
                                 
                                 <div data-old-tag="td"> Purchase and work in <strong><em>RED</em></strong> CPT workbook 
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>          
                        
                        
                        <h3>
                           <strong><em>**Lab </em></strong><em>Instructors grant PERT Retest certificates only after ALL prescribed work has been
                              completed and evaluated. </em>            
                        </h3>
                        
                        <p>For more information about the PERT please visit the <a href="../../assessments/index.html">Assessment web site</a>.
                        </p>
                        
                        <p><a href="../../assessments/pert/index.html">View locations and times</a> for PERT Review Sessions on other campuses.
                        </p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/cpt.pcf">©</a>
      </div>
   </body>
</html>