<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Math Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/studyrooms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Math Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li>West Campus Math Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Group Study Rooms </h2>
                        
                        <h3>How would I reserve a room? </h3>
                        
                        <ul>
                           
                           <li>Students must visit the front desk in the Math Center 7-240 to make a reservation
                              for a group study room. Reservations are made on a first come first served basis.
                              
                           </li>
                           
                           <li>Faculty and Staff may visit the front desk in the Math Center 7-240 to make a reservation
                              for a group study room. 
                           </li>
                           
                        </ul>
                        
                        <h3><strong>What are the rules for reserving rooms? </strong></h3>
                        
                        <ul>
                           
                           <li>Only reserved to students, staff, and faculty members.</li>
                           
                           <li>Valencia ID required for students.</li>
                           
                           <li>Faculty and Staff may book a room for an entire semester when reoccurring appointments
                              are needed (SL Leaders, Group Tutoring, OSD Tutoring).
                           </li>
                           
                           <li>Students may only book up to 2 weeks in advance.</li>
                           
                           <li>A maximum of 2 hours of reservation time available per reservation, unless special
                              permission granted by manager. 
                           </li>
                           
                           <li>Rooms must be booked to use, even if not currently occupied. Students, Staff, or Faculty
                              need to see front desk staff to book a room. 
                           </li>
                           
                           <li>The person reserving the room is responsible for any damages. </li>
                           
                           <li>Rooms empty for 15 minutes after the reservation time will be open for reservations
                              to other students, staff, or faculty.
                           </li>
                           
                           <li>After 3 No Shows students, staff, or faculty member will need to see manager for approval
                              to book rooms.
                           </li>
                           
                        </ul>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Room</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>Capcity</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>Equipment</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7-240 A Study Room</div>
                                 
                                 <div data-old-tag="td">8</div>
                                 
                                 <div data-old-tag="td">Whiteboard</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7-240 C Study Room</div>
                                 
                                 <div data-old-tag="td">8</div>
                                 
                                 <div data-old-tag="td">Whiteboard</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7-240 E Study Room</div>
                                 
                                 <div data-old-tag="td">8</div>
                                 
                                 <div data-old-tag="td">Whiteboard</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7-240 H Study Room</div>
                                 
                                 <div data-old-tag="td">8</div>
                                 
                                 <div data-old-tag="td">Whiteboard</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">7-240 I Study Room</div>
                                 
                                 <div data-old-tag="td">8</div>
                                 
                                 <div data-old-tag="td">Whiteboard</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/studyrooms.pcf">©</a>
      </div>
   </body>
</html>