<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Hands-On Math  | Valencia College</title>
      <meta name="Description" content="Hands-On Math serves as a resource for Developmental Math Students (MAT0018C, MAT0022C, MAT0028C, and MAT1033C) to gain better understanding of mathematical and algebraic concepts using a hands-on approach. Walk-in workshops are offered throughout the semester.">
      <meta name="Keywords" content="hands, on, math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/math/west/hands-on-math.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/west/">West</a></li>
               <li>Hands-On Math </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     
                     <h2>Hands-On Math</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Our Mission and Goals</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Hands-On Math serves as a resource for Developmental Math Students (MAT0018C, MAT0022C,
                           MAT0028C, and
                           MAT1033C) to gain better understanding of mathematical and algebraic concepts using
                           a hands-on approach.
                           Our goals:
                        </p>
                        
                        
                        <ul class="list_style_1">
                           
                           <li>Assist students in understanding mathematical concepts using manipulatives.</li>
                           
                           <li>Provide students with an accessible and comfortable space where they can perform hands-on
                              activities
                              as individuals or as a group.
                              
                           </li>
                           
                           <li>Inform and train faculty on how to incorporate manipulatives during classroom lessons.</li>
                           
                        </ul>
                        
                        
                        <h4>Hours of Operation</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <strong>Monday to Thursday:</strong> 10am - 5pm
                           </li>
                           
                        </ul>
                        
                        
                        <p>Please call 407-582-5408 or contact <a href="mailto:cwatson17@valenciacollege.edu">Courtney Watson</a>
                           for more information.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Resources</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>Calculator Videos</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.screencast.com/t/h4cdc8isGoLZ" target="_blank">MAT1033C</a></li>
                           
                           <li><a href="http://www.screencast.com/t/7VlGsWpSxa" target="_blank">STA1001C</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Walk-In Workshops</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th scope="col">Course</th>
                                 
                                 <th scope="col">Activity</th>
                                 
                                 <th scope="col">Weekday</th>
                                 
                                 <th scope="col">Date</th>
                                 
                                 <th scope="col">Start Time</th>
                                 
                                 <th scope="col">End Time</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>0028</td>
                                 
                                 <td>Ch9(Real# and Inequ)</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>5/10/2017</td>
                                 
                                 <td>9:45 AM</td>
                                 
                                 <td>11:20 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1033</td>
                                 
                                 <td>TI-84+</td>
                                 
                                 <td>Monday</td>
                                 
                                 <td>5/15/2017</td>
                                 
                                 <td>1:30 PM</td>
                                 
                                 <td>3:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>0028</td>
                                 
                                 <td>CH13 (Graphing)</td>
                                 
                                 <td>Tuesday</td>
                                 
                                 <td>5/16/2017</td>
                                 
                                 <td>9:45 AM</td>
                                 
                                 <td>11:20 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1033</td>
                                 
                                 <td>TI-84+</td>
                                 
                                 <td>Tuesday</td>
                                 
                                 <td>5/16/2017</td>
                                 
                                 <td>3:00 PM</td>
                                 
                                 <td>4:30 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1033</td>
                                 
                                 <td>TI-84+</td>
                                 
                                 <td>Tuesday</td>
                                 
                                 <td>5/16/2017</td>
                                 
                                 <td>4:30 PM</td>
                                 
                                 <td>6:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1033</td>
                                 
                                 <td>TI-84+</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>5/17/2017</td>
                                 
                                 <td>11:30 AM</td>
                                 
                                 <td>1:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1033</td>
                                 
                                 <td>Lab Ch O, 2</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>5/17/2017</td>
                                 
                                 <td>1:30 PM</td>
                                 
                                 <td>3:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1033</td>
                                 
                                 <td>TI-84+</td>
                                 
                                 <td>Thursday</td>
                                 
                                 <td>5/18/2017</td>
                                 
                                 <td>11:30 AM</td>
                                 
                                 <td>1:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>0028</td>
                                 
                                 <td>Ch10, 11 Polynomials and Factoring</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>5/24/2017</td>
                                 
                                 <td>9:45 AM</td>
                                 
                                 <td>11:20 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1105</td>
                                 
                                 <td>Act 1</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>6/7/2017</td>
                                 
                                 <td>10:00 AM</td>
                                 
                                 <td>11:30 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>0028</td>
                                 
                                 <td>Ch6, 12 Rat/Prop, FDP, Rational Exp</td>
                                 
                                 <td>Thursday</td>
                                 
                                 <td>6/8/2017</td>
                                 
                                 <td>9:45 AM</td>
                                 
                                 <td>11:20 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1105</td>
                                 
                                 <td>Act 1</td>
                                 
                                 <td>Thursday</td>
                                 
                                 <td>6/8/2017</td>
                                 
                                 <td>11:30 AM</td>
                                 
                                 <td>1:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1105</td>
                                 
                                 <td>Act 2</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>6/14/2017</td>
                                 
                                 <td>11:30 AM</td>
                                 
                                 <td>1:00 PM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1105</td>
                                 
                                 <td>Act 2</td>
                                 
                                 <td>Thursday</td>
                                 
                                 <td>6/15/2017</td>
                                 
                                 <td>10:00 AM</td>
                                 
                                 <td>11:30 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1105</td>
                                 
                                 <td>Act 3</td>
                                 
                                 <td>Wednesday</td>
                                 
                                 <td>7/5/2017</td>
                                 
                                 <td>10:00 AM</td>
                                 
                                 <td>11:30 AM</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1105</td>
                                 
                                 <td>Act 3</td>
                                 
                                 <td>Thursday</td>
                                 
                                 <td>7/6/2017</td>
                                 
                                 <td>11:30 AM</td>
                                 
                                 <td>1:00 PM</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/math/west/hands-on-math.pcf">©</a>
      </div>
   </body>
</html>