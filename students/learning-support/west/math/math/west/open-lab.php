<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Open Lab  | Valencia College</title>
      <meta name="Description" content="The computer lab in 7-241 is available for Developmental Math students to work on lab activities in the presence of lab instructors, to reinforce concepts learned in their Math classes. Lab assignment instructions and support are also available.">
      <meta name="Keywords" content="open, lab, assignments, math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/math/west/open-lab.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/west/">West</a></li>
               <li>Open Lab </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     
                     <h2>Open Lab</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>About the Open Lab</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>The computer lab in 7-241 is available for Developmental Math students to work on
                           lab activities in the
                           presence of lab instructors, to reinforce concepts learned in their Math classes.
                        </p>
                        
                        
                        <p>The Math Open Lab includes:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Professional Lab Instructors</li>
                           
                           <li>110 computers</li>
                           
                           <li>Student Resources Table (scratch paper, stapler, hole puncher)</li>
                           
                           <li>2 Print Stations (print card required, $0.10/page)</li>
                           
                        </ul>
                        
                        
                        <p>Please check the <a href="policies.html">Math Center Policies and Rules</a> before arriving.
                        </p>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Lab Assignments</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul class="list_style_1">
                           
                           <li>Lab Assignments are done in MyMathLab.</li>
                           
                           <li>Quizzes are done at the Valencia West Campus Math Open Lab <strong>only</strong>.
                           </li>
                           
                           <li>Please check the <a href="policies.html">Math Center Policies and Rules</a> before arriving.
                           </li>
                           
                        </ul>
                        
                        
                        <h4>MAT 0018C, MAT 0022C, MAT 0028C</h4>
                        
                        <ol>
                           
                           <li>Take the Diagnostic Quiz at the West Campus Math Lab. A Lab Instructor will provide
                              a password for
                              this quiz. You may only attempt it once.
                              
                           </li>
                           
                           <li>
                              Complete the personalized homework.
                              
                              <ul class="list_style_1">
                                 
                                 <li>MAT 0018C &amp; MAT0028C: Get at least 70% score on each homework section in the chapter
                                    within
                                    MyMathLab.
                                    
                                 </li>
                                 
                                 <li>MAT 0022C: Get at least 80% score on each homework section in the chapter within MyMathLab.</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Take the Mastery Quiz at the West Campus Math Lab. A Lab Instructor will provide a
                              password for this
                              quiz. A 70% score is considered a passing grade. You may attempt the Mastery Quiz
                              twice. If your score
                              is less than 70%, correct your answers from the first attempt, then retake the Mastery
                              Quiz a second
                              time.
                              
                           </li>
                           
                           <li>If your score is less than 70% on both Master Quiz attempts, see your professor for
                              assistance with
                              concepts and a third attempt on the Mastery Quiz.
                              
                           </li>
                           
                        </ol>
                        
                        
                        <h4>MAT 1033C</h4>
                        
                        <p>Steps 1 and 2 may be completed before coming to the Math Open Lab. Steps 3 and 4 <strong>must</strong> be
                           completed in the West Campus Math Center.
                        </p>
                        
                        <ol>
                           
                           <li>View icebreaker and concept videos in MyMathLab.</li>
                           
                           <li>Complete Lab (Project) Worksheet from printed lab syllabus. Help video is available
                              on West Campus
                              Math Center website
                              
                           </li>
                           
                           <li>
                              Project Self Check-In
                              
                              <ul class="list_style_1">
                                 
                                 <li>Take the completed lab worksheet to Math Connections or Hands-On Learning and request
                                    answer key
                                    to check your worksheet yourself.
                                    
                                 </li>
                                 
                                 <li>A Math Center Instructor will record your grade, stamp &amp; date the completed Lab Project
                                    Worksheet.
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              Complete Lab Assessment
                              
                              <ul class="list_style_1">
                                 
                                 <li>Take the stamped Lab Project Worksheet to the Math Open Lab and request access to
                                    Lab Assessment
                                    in MyMathLab.
                                    
                                 </li>
                                 
                                 <li>Complete the Lab Assessment. Score at least 70% &amp; get signed off by a Math Center
                                    lab
                                    instructor.
                                    
                                 </li>
                                 
                                 <li>3 attempts are allowed on Lab Assessment.</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Lab Grades</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Depending on your course, your lab grade may be worth 10-25% of your overall course
                           grade. Please check
                           with your professor for their individual grading schemes.
                        </p>
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th>Course</th>
                                 
                                 <th>Grade Range</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>MAT 0018C, 0022C, 0028C</td>
                                 
                                 <td>20-25%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>MAT 1033C</td>
                                 
                                 <td>10-15%</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <ul class="list_style_1">
                           
                           <li>3/4ths of the lab grade is based on completing lab assignments.</li>
                           
                           <li>1/4th of the lab grade is based on weekly attendance.</li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Lab Attendance</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Attendance is tracked through the Accutrack program at the West Campus Math Center,
                           including the Math
                           Open Lab, Math Connections and Hands-On Learning.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Weekly attendance is <strong>required</strong> for all students.
                           </li>
                           
                           <li>Each week begins on Monday and ends on Saturday. There are no "rollover minutes" between
                              weeks. You
                              must be present for the required number of minutes for your course each week.
                              
                           </li>
                           
                           <li>You must sign in <strong>and</strong> sign out of the Accutrack program when you are in the lab to
                              recieve credit toward the required Math Center attendance.
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Attendance Requirements by Course</h4>
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th>Course</th>
                                 
                                 <th>Weekly Time Required</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>MAT 0018C, MAT 0028C</td>
                                 
                                 <td>H1/H2 Term: Minimum of 120 minutes per week</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>MAT 0022C</td>
                                 
                                 <td>Full Term: Minimum of 60 minutes per week</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>MAT 1033C</td>
                                 
                                 <td>
                                    Full Term: Minimum of 60 minutes per week<br> A or B Term: Minimum of 120 minutes per week
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Math Lab Orientation</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Additional instructions for logging into the Accutrack system and registering for
                           your course material in
                           MyMathLab can be found in the Orientation presentation below.
                        </p>
                        
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/Orientation-MAT-0018C-MAT-0022C-MAT-0028C.pdf" target="_blank">MAT 0018C, 0022C, 0028C Students</a></li>
                           
                           <li><a href="/documents/students/math/west/Orientation-1033C.pdf" target="_blank">MAT
                                 1033C Students</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Helpful Videos</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p><a href="http://www.screencast.com/t/G9KNZkg5ClK" target="_blank">MAT 0022c/0028c Final Exam Review
                              Video</a></p>
                        
                        
                        <h4>MAT 1033c Lab Project Videos</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="https://youtu.be/tt1zV8PT0xM" target="_blank">Chapter 2 Lab Project</a></li>
                           
                           <li><a href="https://www.youtube.com/watch?v=xiA0yU97OY8" target="_blank">Chapter 3 Lab Project</a></li>
                           
                           <li><a href="https://youtu.be/e9sesPRCEbw" target="_blank">Chapter 4 Lab Project</a></li>
                           
                           <li><a href="https://www.youtube.com/watch?v=zmt5-byUy08" target="_blank">Chapter 6 Lab Project</a></li>
                           
                           <li><a href="https://www.youtube.com/watch?v=mT-W2lKJU2E" target="_blank">Chapter 7 Lab Project</a></li>
                           
                           <li><a href="https://www.youtube.com/watch?v=OrhaceQy850" target="_blank">Chapter 8 Lab Project</a></li>
                           
                        </ul>
                        
                        
                        <h4>Mat 1033 Final Exam Videos</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.screencast.com/t/Gfs7gzt0" target="_blank">Online Review Part I</a></li>
                           
                           <li><a href="http://www.screencast.com/t/ekRoxKa2ARzr" target="_blank">Online Review Part II</a></li>
                           
                           <li><a href="http://www.screencast.com/t/yZ20Dg5xR" target="_blank">Online Review Part III</a></li>
                           
                        </ul>
                        
                        
                        <p>We highly recommend bringing headphones. You can also purchase headphones from the
                           Valencia
                           Bookstore.
                        </p>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/math/west/open-lab.pcf">©</a>
      </div>
   </body>
</html>