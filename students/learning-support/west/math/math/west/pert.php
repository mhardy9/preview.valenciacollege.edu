<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>PERT Review  | Valencia College</title>
      <meta name="Description" content="Math PERT review assistance is offered in Building 7, Room 241. Students must bring their PERT scores as well as their PERT Review Book. Please note that there is a $10 retake fee per sub-section of the PERT.">
      <meta name="Keywords" content="PERT, review, math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/math/west/pert.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/west/">West</a></li>
               <li>PERT Review </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     
                     <h2>PERT Review</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Location and Hours</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Math PERT review assistance is offered in Building 7, Room 241. Students must bring
                           their PERT scores as
                           well as their PERT Review Book. Please note that there is a $10 retake fee per sub-section
                           of the
                           PERT.
                        </p>
                        
                        
                        <h4>Hours:</h4>
                        
                        <ul class="list-unstyled">
                           
                           <li>
                              <strong>Monday to Thursday:</strong> 12pm to 6pm
                           </li>
                           
                           <li>No PERT Review is offered on Friday or Saturday</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Assessment Placement Charts</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>Math Placement Scores</h4>
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th>Placement</th>
                                 
                                 <th>PERT</th>
                                 
                                 <th>CPT</th>
                                 
                                 <th>SAT</th>
                                 
                                 <th>ACT</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>Dev. Mathematics I (MAT 0018C or MAT 0022C)</td>
                                 
                                 <td>50-95</td>
                                 
                                 <td>&amp;lt; 41</td>
                                 
                                 <td>N/A</td>
                                 
                                 <td>N/A</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Dev. Mathematics II (MAT 0022C - MAT 0028C)</td>
                                 
                                 <td>96-113</td>
                                 
                                 <td>42-71</td>
                                 
                                 <td>N/A</td>
                                 
                                 <td>N/A</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Statistics for Undergraduates (STA 1001C) or College Math (MGF 1106)</td>
                                 
                                 <td>96 or more</td>
                                 
                                 <td>42 or more</td>
                                 
                                 <td>440 or more</td>
                                 
                                 <td>19 or more</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Intermediate Algebra (MAT 1033C)</td>
                                 
                                 <td>114-122</td>
                                 
                                 <td>72-89</td>
                                 
                                 <td>440-499</td>
                                 
                                 <td>19 or 20</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>College Level Math* (MAC 1005 or STA 2023)</td>
                                 
                                 <td>123-150</td>
                                 
                                 <td>90 or more</td>
                                 
                                 <td>500 or more</td>
                                 
                                 <td>21 or more</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>(* Optionally, take CPT-I testing for possible higher placement)</p>
                        
                        
                        <h4>CPTI Placement</h4>
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th>Score</th>
                                 
                                 <th>Course(s)</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>64 or less</td>
                                 
                                 <td>MAC 1105: College Algebra</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>65 or more</td>
                                 
                                 <td>
                                    MAC 1114: College Trigonometry<br> MAC 1140: Pre-Calculus Algebra<br> MAC 2233: Calculus for Business
                                    and Social Science<br> MAE 2801: Elementary School Math<br> MHF 2300: Logic and Proof in Math
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>89 or more</td>
                                 
                                 <td>MAC 2311: Calculus with Analytical Geometry</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Preparing for PERT/CPT-I Math Review</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        <h4>Review Workbooks</h4>
                        
                        <p>The Math PERT Review Workbook is available in the Valencia Bookstore, or you can download
                           and print it
                           out yourself:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/pert-math-review.booklet.pdf" target="_blank">PERT
                                 Review Workbook</a></li>
                           
                           <li><a href="/documents/students/math/west/cpt-i-review-booklet.pdf" target="_blank">CPT-I
                                 Review Workbook</a></li>
                           
                        </ul>
                        
                        
                        <h4>Using The Workbooks</h4>
                        
                        <p>Obtain a record of your exact PERT/CPT math scores. Your scores indicate which parts
                           of the workbook you
                           will complete for your PERT/CPT review (see table below).
                        </p>
                        
                        <p>Come to the Math Center for your PERT/CPT review. You may complete problems in the
                           workbook in advance,
                           or you may complete them in the Math Center. If you complete problems in advance,
                           bring all your work with
                           you, <strong>showing in detail all steps you used to solve the problems</strong>.
                        </p>
                        
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th>PERT Scores</th>
                                 
                                 <th>Workbook Sections To Complete</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>50-95</td>
                                 
                                 <td>Complete <strong>Parts 1 &amp; 2</strong>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>96-113</td>
                                 
                                 <td>Complete Part 3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>114-122</td>
                                 
                                 <td>Complete Part 4</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>123-150</td>
                                 
                                 <td>Purchase and work in RED CPT workbook</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p><strong>Lab Instructors grant PERT Retest certificates only after ALL prescribed work has
                              been completed
                              and evaluated.</strong></p>
                        
                        <p>Need to retake the Sentence Skills or Reading portions of the PERT, or to see locations
                           and times for
                           PERT Review sessions on other campuses? Please <a href="/students/learning-support/pert.html">visit the Learning Support PERT Review
                              site</a> for more information.
                        </p>
                        
                        <p>For more information about the PERT, please <a href="https://valenciacollege.edu/assessments/">visit
                              Assessment Services.</a></p>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/math/west/pert.pcf">©</a>
      </div>
   </body>
</html>