<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Connections  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/math/west/connections.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/west/">West</a></li>
               <li>Math Connections </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     
                     <h2>Math Connections</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>About Math Connections</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>The Math Connections (7-255) is located inside the Math Center (7-240) on West Campus.
                           The Math
                           Connections is a learning community, where students are encouraged to interact with
                           their peers along with
                           math instructors. Students currently registered in MAT0018C, MAT0022C, MAT0028C, and
                           MAT1033C are welcome
                           to use the Math Connections services.
                        </p>
                        
                        
                        <h4>Hours of Operation</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <strong>Monday to Thursday</strong>: 10 am to 7 pm
                           </li>
                           
                           <li>
                              <strong>Friday</strong>: Closed
                           </li>
                           
                           <li>
                              <strong>Saturday</strong>: Closed
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Course Worksheets</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>MAT0018C Chapters</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-1.pdf" target="_blank">MAT0018C
                                 Chapter 1</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-2.pdf" target="_blank">MAT0018C
                                 Chapter 2</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-3.pdf" target="_blank">MAT0018C
                                 Chapter 3</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-4.pdf" target="_blank">MAT0018C
                                 Chapter 4</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-5.pdf" target="_blank">MAT0018C
                                 Chapter 5</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-6.pdf" target="_blank">MAT0018C
                                 Chapter 6</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0018c-chapter-7.pdf" target="_blank">MAT0018C
                                 Chapter 7</a></li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat0018c-final-exam-review-workshop-packet.pdf" target="_blank">MAT0018C Final Exam Review Workshop Packet</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>MAT0022C Chapters</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-1-whole-numbers.pdf" target="_blank">MAT0022C Chapter 1 - Whole Numbers</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-2-integers.pdf" target="_blank">MAT0022C Chapter 2 - Integers</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-3-equations.pdf" target="_blank">MAT0022C Chapter 3 - Equations</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-4-fractions-intro.pdf" target="_blank">MAT0022C Chapter 4 - Fractions (Intro)</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-4-fractions.pdf" target="_blank">MAT0022C Chapter 4 - Fractions</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-5-decimals.pdf" target="_blank">MAT0022C Chapter 5 - Decimals</a></li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/mat0022c-chapter-6-ratios-proportions-percentages.pdf" target="_blank">MAT0022C Chapter 6 - Ratios, Proportions, Percentages</a>
                                    
                                 </li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-9-problem-solving.pdf" target="_blank">MAT0022C Chapter 9 - Problem Solving</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-10-polynomials.pdf" target="_blank">MAT0022C Chapter 10 - Polynomials</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-11-factoring.pdf" target="_blank">MAT0022C Chapter 11 - Factoring</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-12-rationals.pdf" target="_blank">MAT0022C Chapter 12 - Rationals</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-13-graphing.pdf" target="_blank">MAT0022C Chapter 13 - Graphing</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-chapter-15-radicals.pdf" target="_blank">MAT0022C Chapter 15 - Radicals</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-midterm-review-mc.pdf" target="_blank">MAT0022C Midterm Review - MC</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mat0022c-midterm-review-sa.pdf" target="_blank">MAT0022C Midterm Review - SA</a></li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/mat0022c-final-exam-review-study-guide.pdf" target="_blank">MAT0022C Final Exam Review Study Guide</a> (print and bring to review session)
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/mat0022c-final-exam-review-video-study-guide.pdf" target="_blank">MAT0022C Final Exam Review VIDEO Study Guide</a>
                                    
                                 </li>
                                 
                                 <li><a href="http://www.screencast.com/t/G9KNZkg5ClK" target="_blank">MAT0022C Video Review</a></li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/mat0022c-and-mat0028c-interactive-final-exam-review.pdf" target="_blank">MAT0022C and MAT0028C Interactive Final Exam Review</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/mat0022c-and-mat0028c-final-exam-review-pdf.pdf" target="_blank">MAT0022C and MAT0028C Final Exam Review PDF</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>MAT0028C Chapters</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-1.pdf" target="_blank">MAT0028C
                                 Chapter 1</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-2.pdf" target="_blank">MAT0028C
                                 Chapter 2</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-3.pdf" target="_blank">MAT0028C
                                 Chapter 3</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-4.pdf" target="_blank">MAT0028C
                                 Chapter 4</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-6.pdf" target="_blank">MAT0028C
                                 Chapter 6</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-7.pdf" target="_blank">MAT0028C
                                 Chapter 7</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-8.pdf" target="_blank">MAT0028C
                                 Chapter 8</a></li>
                           
                           <li><a href="/documents/students/math/west/mat0028c-chapter-9.pdf" target="_blank">MAT0028C
                                 Chapter 9</a></li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat0028c-final-exam-practice.pdf" target="_blank">MAT0028C Final Exam Practice</a> (print and bring to the review)
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat0028c-final-exam-review-video-study-guide.pdf" target="_blank">MAT0028C Final Exam Review VIDEO Study Guide</a>
                              
                           </li>
                           
                           <li><a href="http://www.screencast.com/t/G9KNZkg5ClK" target="_blank">MAT0028C Video Review</a></li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat0022c-and-mat0028c-interactive-final-exam-review.pdf" target="_blank">MAT0022C and MAT0028C Interactive Final Exam Review</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat0022c-and-mat0028c-final-exam-review-pdf.pdf" target="_blank">MAT0022C and MAT0028C Final Exam Review PDF</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>MAT1033C Chapters</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-2.pdf" target="_blank">MAT1033C
                                 Chapter 2</a></li>
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-3.pdf" target="_blank">MAT1033C
                                 Chapter 3</a></li>
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-4.pdf" target="_blank">MAT1033C
                                 Chapter 4</a></li>
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-5.pdf" target="_blank">MAT1033C
                                 Chapter 5</a></li>
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-6.pdf" target="_blank">MAT1033C
                                 Chapter 6</a></li>
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-7.pdf" target="_blank">MAT1033C
                                 Chapter 7</a></li>
                           
                           <li><a href="/documents/students/math/west/mat1033c-chapter-8.pdf" target="_blank">MAT1033C
                                 Chapter 8</a></li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat1033c-practice-final-exam.pdf" target="_blank">MAT1033C Practice Final Exam</a> (Courtesy of Prof. Kincade)
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat1033c-final-exam-review-redo.pdf" target="_blank">MAT1033C Final Exam Review Redo</a> (Courtesy of Prof. Kincade
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat1033c-final-exam-review-study-guide.pdf" target="_blank">MAT1033C Final Exam Review Study Guide</a> (print and bring to the review)
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/mat1033c-final-exam-review-video-study-guide.pdf" target="_blank">MAT1033C Final Exam Review VIDEO Study Guide</a>
                              
                           </li>
                           
                           <li><a href="http://www.screencast.com/t/Gfs7gzt0" target="_blank">MAT1033C Video of Final Exam Review
                                 Part 1</a></li>
                           
                           <li><a href="http://www.screencast.com/t/ekRoxKa2ARzr" target="_blank">MAT1033C Video of Final Exam Review
                                 Part 2</a></li>
                           
                           <li><a href="http://www.screencast.com/t/yZ20Dg5xR" target="_blank">MAT1033C Video of Final Exam Review
                                 Part 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>MAT1033C Lab Project Helpful Hints Videos</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="https://youtu.be/tt1zV8PT0xM" target="_blank">MAT1033C Lab Helpful Hits Chapter 2</a></li>
                           
                           <li><a href="https://youtu.be/xiA0yU97OY8" target="_blank">MAT1033C Lab Helpful Hits Chapter 3</a></li>
                           
                           <li><a href="https://youtu.be/e9sesPRCEbw" target="_blank">MAT1033C Lab Helpful Hits Chapter 4</a></li>
                           
                           <li><a href="https://youtu.be/zmt5-byUy08" target="_blank">MAT1033C Lab Helpful Hits Chapter 6</a></li>
                           
                           <li><a href="https://youtu.be/mT-W2lKJU2E" target="_blank">MAT1033C Lab Helpful Hits Chapter 7</a></li>
                           
                           <li><a href="https://youtu.be/OrhaceQy850" target="_blank">MAT1033C Lab Helpful Hits Chapter 8</a></li>
                           
                        </ul>
                        
                        <p>Closed-captioned videos are available on request.</p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Skills Worksheets</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>Natural Numbers, Whole Numbers, and Integers</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/place-value-1.pdf" target="_blank">Place
                                       Value 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/place-value-2.pdf" target="_blank">Place
                                       Value 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/place-value-3.pdf" target="_blank">Place
                                       Value 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rounding-1.pdf" target="_blank">Rounding
                                       1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rounding-2.pdf" target="_blank">Rounding
                                       2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsubtract-whole-1.pdf" target="_blank">Add/Subtract
                                       Whole 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsubtract-whole-2.pdf" target="_blank">Add/Subtract
                                       Whole 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsubtract-whole-3.pdf" target="_blank">Add/Subtract
                                       Whole 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-whole-1.pdf" target="_blank">Multiply
                                       Whole 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-whole-2.pdf" target="_blank">Multiply
                                       Whole 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-whole-3.pdf" target="_blank">Multiply
                                       Whole 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-whole-1.pdf" target="_blank">Divide
                                       Whole 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-whole-2.pdf" target="_blank">Divide
                                       Whole 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-whole-3.pdf" target="_blank">Divide
                                       Whole 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rounding-1.pdf" target="_blank">Rounding
                                       1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rounding-2.pdf" target="_blank">Rounding
                                       2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rounding-3.pdf" target="_blank">Rounding
                                       3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graph-integers-1.pdf" target="_blank">Graph
                                       Integers 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graph-integers-2.pdf" target="_blank">Graph
                                       Integers 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graph-integers-3.pdf" target="_blank">Graph
                                       Integers 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/add-integers-1.pdf" target="_blank">Add
                                       Integers 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/add-integers-2.pdf" target="_blank">Add
                                       Integers 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/add-integers-3.pdf" target="_blank">Add
                                       Integers 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/subtract-integers-1.pdf" target="_blank">Subtract
                                       Integers 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/subtract-integers-2.pdf" target="_blank">Subtract
                                       Integers 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/subtract-integers-3.pdf" target="_blank">Subtract
                                       Integers 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-integers-1.pdf" target="_blank">Multiply
                                       Integers 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-integers-2.pdf" target="_blank">Multiply
                                       Integers 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-integers-3.pdf" target="_blank">Multiply
                                       Integers 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-integers-1.pdf" target="_blank">Divide
                                       Integers 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-integers-2.pdf" target="_blank">Divide
                                       Integers 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-integers-3.pdf" target="_blank">Divide
                                       Integers 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-1.pdf" target="_blank">Exponents
                                       1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-2.pdf" target="_blank">Exponents
                                       2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-3.pdf" target="_blank">Exponents
                                       3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/order-of-operations-1.pdf" target="_blank">Order of Operations 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/order-of-operations-2.pdf" target="_blank">Order of Operations 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/order-of-operations-3.pdf" target="_blank">Order of Operations 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/order-of-ops-1.pdf" target="_blank">Order
                                       of Ops 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/order-of-ops-2.pdf" target="_blank">Order
                                       of Ops 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/order-of-ops-3.pdf" target="_blank">Order
                                       of Ops 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Arithmetic Including Rational Numbers (Fractions, Decimals)</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/exponents-and-roots-1.pdf" target="_blank">Exponents
                                 and Roots 1</a></li>
                           
                           <li><a href="/documents/students/math/west/exponents-and-roots-2.pdf" target="_blank">Exponents
                                 and Roots 2</a></li>
                           
                           <li><a href="/documents/students/math/west/exponents-and-roots-3.pdf" target="_blank">Exponents
                                 and Roots 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Introduction to Algebra and Variables</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/algebraic-properties-1.pdf" target="_blank">Algebraic
                                 Properties 1</a></li>
                           
                           <li><a href="/documents/students/math/west/algebraic-properties-2.pdf" target="_blank">Algebraic
                                 Properties 2</a></li>
                           
                           <li><a href="/documents/students/math/west/algebraic-properties-3.pdf" target="_blank">Algebraic
                                 Properties 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Combine Like Terms in Expressions (Distributive Property)</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/combining-like-terms-1.pdf" target="_blank">Combining
                                 Like Terms 1</a></li>
                           
                           <li><a href="/documents/students/math/west/combining-like-terms-2.pdf" target="_blank">Combining
                                 Like Terms 2</a></li>
                           
                           <li><a href="/documents/students/math/west/combining-like-terms-3.pdf" target="_blank">Combining
                                 Like Terms 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Expressions</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/evaluate-mixed-expressions-1.pdf" target="_blank">Evaluate Mixed Expressions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/evaluate-mixed-expressions-2.pdf" target="_blank">Evaluate Mixed Expressions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/evaluate-mixed-expressions-3.pdf" target="_blank">Evaluate Mixed Expressions 3</a></li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/evaluate-with-multiplication-and-division-1.pdf" target="_blank">Evaluate Expressions With Multiplication And Division 1</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/evaluate-with-multiplication-and-division-2.pdf" target="_blank">Evaluate Expressions With Multiplication And Division 2</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/evaluate-with-multiplication-and-division-3.pdf" target="_blank">Evaluate Expressions With Multiplication And Division 3</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/translate-expressions-1.pdf" target="_blank">Translate Algebraic Expressions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-expressions-2.pdf" target="_blank">Translate Algebraic Expressions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-expressions-3.pdf" target="_blank">Translate Algebraic Expressions 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-expressions-4.pdf" target="_blank">Translate Algebraic Expressions 4</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-expressions-5.pdf" target="_blank">Translate Algebraic Expressions 5</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-expressions-6.pdf" target="_blank">Translate Algebraic Expressions 6</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Equations</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/solutions-1.pdf" target="_blank">Solutions
                                       of an Equation 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solutions-2.pdf" target="_blank">Solutions
                                       of an Equation 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solutions-3.pdf" target="_blank">Solutions
                                       of an Equation 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solutions-4.pdf" target="_blank">Solutions
                                       of an Equation 4</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solutions-5.pdf" target="_blank">Solutions
                                       of an Equation 5</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solutions-6.pdf" target="_blank">Solutions
                                       of an Equation 6</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-1.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-2.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-3.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-4.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 4</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-5.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 5</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-6.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 6</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-7.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 7</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-8.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 8</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-9.pdf" target="_blank">Solve Linear
                                       Equations by Combining Like Terms 9</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addition-property-1.pdf" target="_blank">Addition
                                       Property of Equality 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addition-property-2.pdf" target="_blank">Addition
                                       Property of Equality 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addition-property-3.pdf" target="_blank">Addition
                                       Property of Equality 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/multiplication-property-1.pdf" target="_blank">Multiplication Property of Equality 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiplication-property-2.pdf" target="_blank">Multiplication Property of Equality 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiplication-property-3.pdf" target="_blank">Multiplication Property of Equality 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/solve-multi-step-equations-1.pdf" target="_blank">Solve Multi-Step Equations 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-equations-1.pdf" target="_blank">Translate Algebraic Equations 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-equations-2.pdf" target="_blank">Translate Algebraic Equations 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-equations-3.pdf" target="_blank">Translate Algebraic Equations 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-equations-4.pdf" target="_blank">Translate Algebraic Equations 4</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-equations-5.pdf" target="_blank">Translate Algebraic Equations 5</a></li>
                                 
                                 <li><a href="/documents/students/math/west/translate-equations-6.pdf" target="_blank">Translate Algebraic Equations 6</a></li>
                                 
                                 <li><a href="/documents/students/math/west/literal-equations-1.pdf" target="_blank">Solve
                                       for One Variable/Formulas/Literal Equations 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/inequalities-1.pdf" target="_blank">Inequalities
                                       1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/inequalities-2.pdf" target="_blank">Inequalities
                                       2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/inequalities-3.pdf" target="_blank">Inequalities
                                       3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Application Problems/Word Problems/Problem Solving</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/discounts-and-mark-ups-1.pdf" target="_blank">Discounts
                                 and Mark-Ups 1</a></li>
                           
                           <li><a href="/documents/students/math/west/discounts-and-mark-ups-2.pdf" target="_blank">Discounts
                                 and Mark-Ups 2</a></li>
                           
                           <li><a href="/documents/students/math/west/discounts-and-mark-ups-3.pdf" target="_blank">Discounts
                                 and Mark-Ups 3</a></li>
                           
                           <li>
                              <a href="/documents/students/math/west/geometry-1.pdf" target="_blank">Geometry 1</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/geometry-2.pdf" target="_blank">Geometry 2</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/geometry-3.pdf" target="_blank">Geometry 3</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/geometry-4.pdf" target="_blank">Geometry 4</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/geometry-5.pdf" target="_blank">Geometry 5</a>
                              
                           </li>
                           
                           <li><a href="/documents/students/math/west/consecutive-integers-1.pdf" target="_blank">Consecutive
                                 Integers 1</a></li>
                           
                           <li><a href="/documents/students/math/west/consecutive-integers-2.pdf" target="_blank">Consecutive
                                 Integers 2</a></li>
                           
                           <li><a href="/documents/students/math/west/consecutive-integers-3.pdf" target="_blank">Consecutive
                                 Integers 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Graphing</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/coordinate-systems-1.pdf" target="_blank">Coordinate
                                       Systems 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/coordinate-systems-2.pdf" target="_blank">Coordinate
                                       Systems 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/coordinate-systems-3.pdf" target="_blank">Coordinate
                                       Systems 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/ordered-pairs-1.pdf" target="_blank">Ordered
                                       Pairs 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/ordered-pairs-2.pdf" target="_blank">Ordered
                                       Pairs 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/ordered-pairs-3.pdf" target="_blank">Ordered
                                       Pairs 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-standard-form-1.pdf" target="_blank">Graphing In Standard Form 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-standard-form-2.pdf" target="_blank">Graphing In Standard Form 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-standard-form-3.pdf" target="_blank">Graphing In Standard Form 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-slope-intercept-form-1.pdf" target="_blank">Graphing In Slope-Intercept Form 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-slope-intercept-form-2.pdf" target="_blank">Graphing In Slope-Intercept Form 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-slope-intercept-form-3.pdf" target="_blank">Graphing In Slope-Intercept Form 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/graphing-in-slope-intercept-form-4.pdf" target="_blank">Graphing In Slope-Intercept Form 4</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li>
                                    <a href="/documents/students/math/west/slope-1.pdf" target="_blank">Slope 1</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/slope-2.pdf" target="_blank">Slope 2</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/slope-3.pdf" target="_blank">Slope 3</a>
                                    
                                 </li>
                                 
                                 <li><a href="/documents/students/math/west/mixed-graphing-1.pdf" target="_blank">Mixed
                                       Graphing 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mixed-graphing-2.pdf" target="_blank">Mixed
                                       Graphing 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mixed-graphing-3.pdf" target="_blank">Mixed
                                       Graphing 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/line-equation-1.pdf" target="_blank">Line
                                       Equation 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/line-equation-2.pdf" target="_blank">Line
                                       Equation 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/line-equation-3.pdf" target="_blank">Line
                                       Equation 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Fractions</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="http://www.screencast.com/t/g4sLpTUrg0d" target="_blank">Fraction Review Video</a></li>
                                 
                                 <li><a href="/documents/students/math/west/prime-factorization-1.pdf" target="_blank">Prime Factorization 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/prime-factorization-2.pdf" target="_blank">Prime Factorization 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/prime-factorization-3.pdf" target="_blank">Prime Factorization 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-fractions-1.pdf" target="_blank">Simplify
                                       Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-fractions-2.pdf" target="_blank">Simplify
                                       Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-fractions-3.pdf" target="_blank">Simplify
                                       Fractions 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/improper-to-mixed-1.pdf" target="_blank">Convert
                                       Improper Fractions to Mixed Number 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/improper-to-mixed-2.pdf" target="_blank">Convert
                                       Improper Fractions to Mixed Number 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/improper-to-mixed-3.pdf" target="_blank">Convert
                                       Improper Fractions to Mixed Number 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mixed-to-improper-1.pdf" target="_blank">Convert
                                       Mixed Numbers to Improper Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mixed-to-improper-2.pdf" target="_blank">Convert
                                       Mixed Numbers to Improper Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mixed-to-improper-3.pdf" target="_blank">Convert
                                       Mixed Numbers to Improper Fractions 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-fractions-1.pdf" target="_blank">Multiply
                                       Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-fractions-2.pdf" target="_blank">Multiply
                                       Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-fractions-3.pdf" target="_blank">Multiply
                                       Fractions 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-fractions-1.pdf" target="_blank">Divide
                                       Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-fractions-2.pdf" target="_blank">Divide
                                       Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/divide-fractions-3.pdf" target="_blank">Divide
                                       Fractions 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/addsub-like-denom-1.pdf" target="_blank">Add
                                       and Subtract Fractions with Like Denominators 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-like-denom-2.pdf" target="_blank">Add
                                       and Subtract Fractions with Like Denominators 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-like-denom-3.pdf" target="_blank">Add
                                       and Subtract Fractions with Like Denominators 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/lcd.pdf" target="_blank">Least Common
                                       Denominator (LCD)</a></li>
                                 
                                 <li><a href="/documents/students/math/west/lcd-and-lcm-1.pdf" target="_blank">LCD and
                                       LCM (Least Common Multiple) 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/lcd-and-lcm-2.pdf" target="_blank">LCD and
                                       LCM 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/lcd-and-lcm-3.pdf" target="_blank">LCD and
                                       LCM 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/equivalent-fractions-1.pdf" target="_blank">Equivalent Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/equivalent-fractions-2.pdf" target="_blank">Equivalent Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/equivalent-fractions-3.pdf" target="_blank">Equivalent Fractions 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-lcd-1.pdf" target="_blank">Add and
                                       Subtract Fractions by Finding the LCD 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-lcd-2.pdf" target="_blank">Add and
                                       Subtract Fractions by Finding the LCD 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-lcd-3.pdf" target="_blank">Add and
                                       Subtract Fractions by Finding the LCD 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/fraction-expressions-1.pdf" target="_blank">Simplify Expressions with Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/fraction-expressions-2.pdf" target="_blank">Simplify Expressions with Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/fraction-expressions-3.pdf" target="_blank">Simplify Expressions with Fractions 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/fraction-equations-1.pdf" target="_blank">Solve
                                       Equations with Fractions 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/fraction-equations-2.pdf" target="_blank">Solve
                                       Equations with Fractions 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/fraction-equations-3.pdf" target="_blank">Solve
                                       Equations with Fractions 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Decimals</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/add-and-subtract-decimals-1.pdf" target="_blank">Add and Subtract Decimals 1</a></li>
                           
                           <li><a href="/documents/students/math/west/add-and-subtract-decimals-2.pdf" target="_blank">Add and Subtract Decimals 2</a></li>
                           
                           <li><a href="/documents/students/math/west/add-and-subtract-decimals-3.pdf" target="_blank">Add and Subtract Decimals 3</a></li>
                           
                           <li><a href="/documents/students/math/west/multiply-decimals-1.pdf" target="_blank">Multiply
                                 Decimals 1</a></li>
                           
                           <li><a href="/documents/students/math/west/divide-decimals-1.pdf" target="_blank">Divide
                                 Decimals 1</a></li>
                           
                           <li><a href="/documents/students/math/west/decimal-equationsproblem-solving-1.pdf" target="_blank">Decimal Equations/Problem Solving 1</a></li>
                           
                           <li><a href="/documents/students/math/west/decimal-equationsproblem-solving-2.pdf" target="_blank">Decimal Equations/Problem Solving 2</a></li>
                           
                           <li><a href="/documents/students/math/west/decimal-equationsproblem-solving-3.pdf" target="_blank">Decimal Equations/Problem Solving 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Fraction, Decimal, Percentage Conversion</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/fdp1.pdf" target="_blank">FDP1</a></li>
                           
                           <li><a href="/documents/students/math/west/fdp2.pdf" target="_blank">FDP2</a></li>
                           
                           <li><a href="/documents/students/math/west/fdp3.pdf" target="_blank">FDP3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Polynomials</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li>
                                    <a href="/documents/students/math/west/multiplying-polynomials-and-exponents-1.pdf" target="_blank">Multiplying Polynomials and Exponents 1</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/multiplying-polynomials-and-exponents-2.pdf" target="_blank">Multiplying Polynomials and Exponents 2</a>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="/documents/students/math/west/multiplying-polynomials-and-exponents-3.pdf" target="_blank">Multiplying Polynomials and Exponents 3</a>
                                    
                                 </li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-1.pdf" target="_blank">Exponents
                                       1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-2.pdf" target="_blank">Exponents
                                       2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-3.pdf" target="_blank">Exponents
                                       3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-and-monomials-1.pdf" target="_blank">Exponents and Monomials 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-and-monomials-2.pdf" target="_blank">Exponents and Monomials 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/exponents-and-monomials-3.pdf" target="_blank">Exponents and Monomials 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/addsub-poly-1.pdf" target="_blank">Add and
                                       Subtract Polynomials 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-poly-2.pdf" target="_blank">Add and
                                       Subtract Polynomials 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/addsub-poly-3.pdf" target="_blank">Add and
                                       Subtract Polynomials 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiplication-of-polynomials-1.pdf" target="_blank">Multiplication of Polynomials 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiplication-of-polynomials-2.pdf" target="_blank">Multiplication of Polynomials 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiplication-of-polynomials-3.pdf" target="_blank">Multiplication of Polynomials 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/special-products-1.pdf" target="_blank">Special
                                       Products 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/special-products-2.pdf" target="_blank">Special
                                       Products 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/special-products-3.pdf" target="_blank">Special
                                       Products 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Geometry</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/perimeter-1.pdf" target="_blank">Perimeter
                                 1</a></li>
                           
                           <li><a href="/documents/students/math/west/perimeter-2.pdf" target="_blank">Perimeter
                                 2</a></li>
                           
                           <li><a href="/documents/students/math/west/perimeter-3.pdf" target="_blank">Perimeter
                                 3</a></li>
                           
                           <li><a href="/documents/students/math/west/area-1.pdf" target="_blank">Area 1</a></li>
                           
                           <li><a href="/documents/students/math/west/area-2.pdf" target="_blank">Area 2</a></li>
                           
                           <li><a href="/documents/students/math/west/area-3.pdf" target="_blank">Area 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Mixed Skills</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/evaluate-order-of-ops-division-1.pdf" target="_blank">Evaluate/Order of Operations (Division) 1</a></li>
                           
                           <li><a href="/documents/students/math/west/evaluate-order-of-ops-division-2.pdf" target="_blank">Evaluate/Order of Operations (Division) 2</a></li>
                           
                           <li><a href="/documents/students/math/west/evaluate-order-of-ops-division-3.pdf" target="_blank">Evaluate/Order of Operations (Division) 3</a></li>
                           
                           <li><a href="/documents/students/math/west/contvarevalorderofops-1.pdf" target="_blank">Constants/Variables/Evaluate/Order
                                 of Operations 1</a></li>
                           
                           <li><a href="/documents/students/math/west/contvarevalorderofops-2.pdf" target="_blank">Constants/Variables/Evaluate/Order
                                 of Operations 2</a></li>
                           
                           <li><a href="/documents/students/math/west/contvarevalorderofops-3.pdf" target="_blank">Constants/Variables/Evaluate/Order
                                 of Operations 3</a></li>
                           
                           <li>
                              <a href="/documents/students/math/west/addition-of-polynomials-and-integers-rationals.pdf" target="_blank">Addition of Polynomials and Integers, Rationals</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/subtraction-of-polynomials-and-integers-rationals.pdf" target="_blank">Subtraction of Polynomials and Integers, Rationals</a>
                              
                           </li>
                           
                           <li>
                              <a href="/documents/students/math/west/addsubtract-polynomimals-and-integers-rationals.pdf" target="_blank">Add/Subtract Polynomials and Integers, Rationals</a>
                              
                           </li>
                           
                           <li><a href="/documents/students/math/west/formulas-1.pdf" target="_blank">Translated
                                 Mixed Word Problems - Formulas 1</a></li>
                           
                        </ul>
                        
                        
                        <h4>Factoring</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/gcf-1.pdf" target="_blank">GCF 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/gcf-2.pdf" target="_blank">GCF 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/gcf-3.pdf" target="_blank">GCF 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/grouping-1.pdf" target="_blank">Grouping
                                       1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/grouping-2.pdf" target="_blank">Grouping
                                       2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/grouping-3.pdf" target="_blank">Grouping
                                       3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/x2bxc-trinomials-1.pdf" target="_blank">x^2+bx+c
                                       Trinomials 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/x2bxc-trinomials-2.pdf" target="_blank">x^2+bx+c
                                       Trinomials 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/x2bxc-trinomials-3.pdf" target="_blank">x^2+bx+c
                                       Trinomials 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/ax2bxc-trinomials-1.pdf" target="_blank">ax^2+bx+c
                                       Trinomials 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/ax2bxc-trinomials-2.pdf" target="_blank">ax^2+bx+c
                                       Trinomials 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/ax2bxc-trinomials-3.pdf" target="_blank">ax^2+bx+c
                                       Trinomials 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/special-products-1.pdf" target="_blank">Special
                                       Products 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/special-products-2.pdf" target="_blank">Special
                                       Products 2</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/special-products-3.pdf" target="_blank">Special
                                       Products 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/perfect-sq-tri-1.pdf" target="_blank">Perfect
                                       Square Trinomials 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/perfect-sq-tri-2.pdf" target="_blank">Perfect
                                       Square Trinomials 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/difference-of-squares-1.pdf" target="_blank">Difference of Squares 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/difference-of-squares-2.pdf" target="_blank">Difference of Squares 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/sum-of-cubes-1.pdf" target="_blank">Sum of
                                       Cubes 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/sum-of-cubes-2.pdf" target="_blank">Sum of
                                       Cubes 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/difference-of-cubes1.pdf" target="_blank">Difference
                                       of Cubes 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/difference-of-cubes-2.pdf" target="_blank">Difference of Cubes 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/factor-completely-1.pdf" target="_blank">Factor
                                       Completely 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/factor-completely-2.pdf" target="_blank">Factor
                                       Completely 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/factor-completely-3.pdf" target="_blank">Factor
                                       Completely 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/completing-the-square-1.pdf" target="_blank">Completing the Square 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/completing-the-square-2.pdf" target="_blank">Completing the Square 2</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Rational Expressions and Equations</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/rational-prereq-skills-1.pdf" target="_blank">Rational
                                 Prereq Skills 1</a></li>
                           
                           <li><a href="/documents/students/math/west/rational-prereq-skills-2.pdf" target="_blank">Rational
                                 Prereq Skills 2</a></li>
                           
                           <li><a href="/documents/students/math/west/rational-prereq-skills-3.pdf" target="_blank">Rational
                                 Prereq Skills 3</a></li>
                           
                           <li><a href="/documents/students/math/west/clearing-fractions-1.pdf" target="_blank">Clearing
                                 Fractions with Numerical Denominators 1</a></li>
                           
                           <li><a href="/documents/students/math/west/clearing-fractions-2.pdf" target="_blank">Clearing
                                 Fractions with Numerical Denominators 2</a></li>
                           
                           <li><a href="/documents/students/math/west/addsubtract-1.pdf" target="_blank">Rational
                                 Expressions - Add/Subtract 1</a></li>
                           
                           <li><a href="/documents/students/math/west/addsubtract-2.pdf" target="_blank">Rational
                                 Expressions - Add/Subtract 2</a></li>
                           
                           <li><a href="/documents/students/math/west/addsubtract-3.pdf" target="_blank">Rational
                                 Expressions - Add/Subtract 3</a></li>
                           
                           <li><a href="/documents/students/math/west/rational-equations-1.pdf" target="_blank">Rational
                                 Equations 1</a></li>
                           
                           <li><a href="/documents/students/math/west/rational-equations-2.pdf" target="_blank">Rational
                                 Equations 2</a></li>
                           
                           <li><a href="/documents/students/math/west/rational-equations-3.pdf" target="_blank">Rational
                                 Equations 3</a></li>
                           
                        </ul>
                        
                        
                        <h4>Radicals</h4>
                        
                        <div class="row">
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/square-roots-1.pdf" target="_blank">Square
                                       Roots 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/square-roots-2.pdf" target="_blank">Square
                                       Roots 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/square-roots-3.pdf" target="_blank">Square
                                       Roots 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-square-roots-1.pdf" target="_blank">Simplify Square Roots 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-square-roots-2.pdf" target="_blank">Simplify Square Roots 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-square-roots-3.pdf" target="_blank">Simplify Square Roots 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-square-roots-4.pdf" target="_blank">Simplify Square Roots 4</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-square-roots-5.pdf" target="_blank">Simplify Square Roots 5</a></li>
                                 
                                 <li><a href="/documents/students/math/west/simplify-square-roots-6.pdf" target="_blank">Simplify Square Roots 6</a></li>
                                 
                                 <li><a href="/documents/students/math/west/add-and-subtract-square-roots-1.pdf" target="_blank">Add and Subtract Square Roots 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/add-and-subtract-square-roots-2.pdf" target="_blank">Add and Subtract Square Roots 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/add-and-subtract-square-roots-3.pdf" target="_blank">Add and Subtract Square Roots 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6">
                              
                              <ul class="list_style_1">
                                 
                                 <li><a href="/documents/students/math/west/mulitply-and-divide-roots-1.pdf" target="_blank">Multiply and Divide Roots 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mulitply-and-divide-roots-2.pdf" target="_blank">Multiply and Divide Roots 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/mulitply-and-divide-roots-3.pdf" target="_blank">Multiply and Divide Roots 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-and-divide-radicals-1.pdf" target="_blank">Multiply and Divide Radicals 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-and-divide-radicals-2.pdf" target="_blank">Multiply and Divide Radicals 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/multiply-and-divide-radicals-3.pdf" target="_blank">Multiply and Divide Radicals 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rationalize-square-roots-1.pdf" target="_blank">Rationalize Square Roots 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rationalize-square-roots-2.pdf" target="_blank">Rationalize Square Roots 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rationalize-square-roots-3.pdf" target="_blank">Rationalize Square Roots 3</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rationalize-radicals-1.pdf" target="_blank">Rationalize Radicals 1</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rationalize-radicals-2.pdf" target="_blank">Rationalize Radicals 2</a></li>
                                 
                                 <li><a href="/documents/students/math/west/rationalize-radicals-3.pdf" target="_blank">Rationalize Radicals 3</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h4>Dimensional Analysis</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="/documents/students/math/west/conversion-measurement.pdf" target="_blank">Conversion
                                 Measurements</a></li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Helpful Videos</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>Algebra</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.kaltura.com/tiny/z1e5d" target="_blank">Domain and Range</a></li>
                           
                        </ul>
                        
                        
                        <h4>Statistics</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="http://www.kaltura.com/tiny/kbwtc" target="_blank">Finding Sample Standard Deviation</a></li>
                           
                           <li><a href="http://www.kaltura.com/tiny/u9639" target="_blank">Sample and Population Standard
                                 Deviation</a></li>
                           
                           <li><a href="http://www.kaltura.com/tiny/qltcg" target="_blank">Probability Distribution</a></li>
                           
                           <li><a href="http://www.kaltura.com/tiny/ko576" target="_blank">Using a TI-84+ to Find Residuals</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/math/west/connections.pcf">©</a>
      </div>
   </body>
</html>