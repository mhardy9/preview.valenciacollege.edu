<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Help 24/7 | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/math/courses/clarklinks_000.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Help 24/7</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/">Math</a></li>
               <li><a href="/students/learning-support/west/math/math/courses/">Courses</a></li>
               <li>Math Help 24/7</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../liveScribe.html"></a>
                        
                        
                        <div>
                           
                           <h2>Intermediate Algebra Videos </h2>
                           
                           <p>The links in this document go to videos that demonstrate exercises similar to the
                              Example exercises in Intermediate Algebra, by Mark Clark and Cynthia Anfinson.
                              
                           </p>
                           
                           <p>To find an appropriate exercise, go to the textbook and locate the Chapter, Section
                              and Example.&nbsp; Then find the related video in the table.
                           </p>
                           
                           <p>If you find any errors, please feel free to contact Joel Berman at <a href="mailto:jberman@valenciacollege.edu">jberman@valenciacollege.edu</a> to report the error.
                           </p>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.1.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=NP4pMGjJKP0">http://www.youtube.com/watch?v=NP4pMGjJKP0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.1.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=FTGwv1jZHYs">http://www.youtube.com/watch?v=FTGwv1jZHYs</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.1.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=GPCIJaE2sGk">http://www.youtube.com/watch?v=GPCIJaE2sGk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.1.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=XxrhGMrCzt0">http://www.youtube.com/watch?v=XxrhGMrCzt0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.2.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=qqGLgeohZxA">http://www.youtube.com/watch?v=qqGLgeohZxA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.2.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p> <a href="http://youtu.be/6RbvCtlYlms">http://youtu.be/6RbvCtlYlms</a> 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.2.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=PFCnE_XL93c">http://www.youtube.com/watch?v=PFCnE_XL93c</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.2.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=GAgFA-N_QQ8">http://www.youtube.com/watch?v=GAgFA-N_QQ8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=qqruAfAhmDM">http://www.youtube.com/watch?v=qqruAfAhmDM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=hBfxwgMemMM">http://www.youtube.com/watch?v=hBfxwgMemMM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p> <a href="http://youtu.be/eBSs-OKb3Jk">http://youtu.be/eBSs-OKb3Jk</a> 
                                       </p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=_Ts7agRKBZQ">http://www.youtube.com/watch?v=_Ts7agRKBZQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=3ith546HosM">http://www.youtube.com/watch?v=3ith546HosM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Q1vWJUsDQ3U">http://www.youtube.com/watch?v=Q1vWJUsDQ3U</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.3.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=i6sNZI0vpaE">http://www.youtube.com/watch?v=i6sNZI0vpaE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.4.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=vFMLct_02Os">http://www.youtube.com/watch?v=vFMLct_02Os</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.4.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=8V0V430GHP8">http://www.youtube.com/watch?v=8V0V430GHP8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.4.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=G20vfwpF_1M">http://www.youtube.com/watch?v=G20vfwpF_1M</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.4.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=4r3JJe9ALfY">http://www.youtube.com/watch?v=4r3JJe9ALfY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.4.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=avYQrlPDhmU">http://www.youtube.com/watch?v=avYQrlPDhmU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ENrl9QfPl7E">http://www.youtube.com/watch?v=ENrl9QfPl7E</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=TAPjFrLtDJ8">http://www.youtube.com/watch?v=TAPjFrLtDJ8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=_Q8xueg6xfM">http://www.youtube.com/watch?v=_Q8xueg6xfM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=1Djhxg4ilJQ">http://www.youtube.com/watch?v=1Djhxg4ilJQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=8ZgqsS573gs">http://www.youtube.com/watch?v=8ZgqsS573gs</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=BKy5tiyIpdw">http://www.youtube.com/watch?v=BKy5tiyIpdw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=uR3Qfn9NUm8">http://www.youtube.com/watch?v=uR3Qfn9NUm8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.8</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ILEkplLA2z0">http://www.youtube.com/watch?v=ILEkplLA2z0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.5.9</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=7cd8oGHUHhQ">http://www.youtube.com/watch?v=7cd8oGHUHhQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.6.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=1QLZYrZJjhc">http://www.youtube.com/watch?v=1QLZYrZJjhc</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.6.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=BpRsZI_fM7Q">http://www.youtube.com/watch?v=BpRsZI_fM7Q</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.6.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=L3fKTY3TqFE">http://www.youtube.com/watch?v=L3fKTY3TqFE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=J7gqAw6hYzE">http://www.youtube.com/watch?v=J7gqAw6hYzE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=oeyNDijW6zQ">http://www.youtube.com/watch?v=oeyNDijW6zQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=6oX6aeMAAEw">http://www.youtube.com/watch?v=6oX6aeMAAEw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=-ZoVVhl3Ckk">http://www.youtube.com/watch?v=-ZoVVhl3Ckk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=e3GLgPx7v6E">http://www.youtube.com/watch?v=e3GLgPx7v6E</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=QGsnRLWo6bw">http://www.youtube.com/watch?v=QGsnRLWo6bw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>1.7.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=LzmSZsfbKmE">http://www.youtube.com/watch?v=LzmSZsfbKmE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.1.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Q3LcnJuefIg">http://www.youtube.com/watch?v=Q3LcnJuefIg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.1.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Tn34FJw78dw">http://www.youtube.com/watch?v=Tn34FJw78dw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.1.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=a2n_W9kYqak">http://www.youtube.com/watch?v=a2n_W9kYqak</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.1.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ZpS-FMy2-Qg">http://www.youtube.com/watch?v=ZpS-FMy2-Qg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.1.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=fVej4F61CWQ">http://www.youtube.com/watch?v=fVej4F61CWQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.2.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=OzAqtHPqDEA">http://www.youtube.com/watch?v=OzAqtHPqDEA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.2.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=GyM2uE7KfDE">http://www.youtube.com/watch?v=GyM2uE7KfDE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.2.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=EwVqNTNqxrI">http://www.youtube.com/watch?v=EwVqNTNqxrI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.2.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=vDEUN1hmrXU">http://www.youtube.com/watch?v=vDEUN1hmrXU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.2.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=0Okn9hZA5NU">http://www.youtube.com/watch?v=0Okn9hZA5NU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.2.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=BC_BjDBp3vM">http://www.youtube.com/watch?v=BC_BjDBp3vM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.3.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=FioRHl-oxSA">http://www.youtube.com/watch?v=FioRHl-oxSA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.3.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=QoO2VWhYE2A">http://www.youtube.com/watch?v=QoO2VWhYE2A</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.3.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=AWMod_B_4xI">http://www.youtube.com/watch?v=AWMod_B_4xI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.3.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=L8Kl_GTRLVk">http://www.youtube.com/watch?v=L8Kl_GTRLVk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.4.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=HGL0OMgW2Pc">http://www.youtube.com/watch?v=HGL0OMgW2Pc</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.4.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>No video for this exercise</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.4.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=TmZwVIxLY7g">http://www.youtube.com/watch?v=TmZwVIxLY7g</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.4.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=27HHk42jT1c">http://www.youtube.com/watch?v=27HHk42jT1c</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.4.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=JfeUNbeSSDg">http://www.youtube.com/watch?v=JfeUNbeSSDg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.4.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=TnWLURT59M4">http://www.youtube.com/watch?v=TnWLURT59M4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ca34GLF2Q3U">http://www.youtube.com/watch?v=ca34GLF2Q3U</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ay3Fw5f7eNM">http://www.youtube.com/watch?v=ay3Fw5f7eNM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=TR1L0SBytvY">http://www.youtube.com/watch?v=TR1L0SBytvY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=MvFO9d_HnIk">http://www.youtube.com/watch?v=MvFO9d_HnIk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=DB1Zrt_GIUY">http://www.youtube.com/watch?v=DB1Zrt_GIUY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=3_IF1z8lEF4">http://www.youtube.com/watch?v=3_IF1z8lEF4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=QhVGT50fqUk">http://www.youtube.com/watch?v=QhVGT50fqUk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.5.8</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=rCJFPETf6b4">http://www.youtube.com/watch?v=rCJFPETf6b4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=8U5fBh_QpLo">http://www.youtube.com/watch?v=8U5fBh_QpLo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ncrZT85Qciw">http://www.youtube.com/watch?v=ncrZT85Qciw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=OFb5ouLpzxo">http://www.youtube.com/watch?v=OFb5ouLpzxo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=4XO8fgStUYY">http://www.youtube.com/watch?v=4XO8fgStUYY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=fWAkTwLEp6g">http://www.youtube.com/watch?v=fWAkTwLEp6g</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=8T_t-OJYpBA">http://www.youtube.com/watch?v=8T_t-OJYpBA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>2.6.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=scbYbz2CMzI">http://www.youtube.com/watch?v=scbYbz2CMzI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=zvvCs86zSjQ">http://www.youtube.com/watch?v=zvvCs86zSjQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=2uCFKBsRDgw">http://www.youtube.com/watch?v=2uCFKBsRDgw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=dSzQ6O-CcjA">http://www.youtube.com/watch?v=dSzQ6O-CcjA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ycO1GcnMoTI">http://www.youtube.com/watch?v=ycO1GcnMoTI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=x3IZ4UkpcYU">http://www.youtube.com/watch?v=x3IZ4UkpcYU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=trZFUM2J2Tw">http://www.youtube.com/watch?v=trZFUM2J2Tw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=G0Oe2RknNq8">http://www.youtube.com/watch?v=G0Oe2RknNq8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.8</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=rg1n5UAr2S4">http://www.youtube.com/watch?v=rg1n5UAr2S4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.1.9</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=k27RNOpEUww">http://www.youtube.com/watch?v=k27RNOpEUww</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=cdlq_tm2968">http://www.youtube.com/watch?v=cdlq_tm2968</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=sqBs8kNltyU">http://www.youtube.com/watch?v=sqBs8kNltyU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=TNTg4gZw0ok">http://www.youtube.com/watch?v=TNTg4gZw0ok</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=tlW9H1ESWwA">http://www.youtube.com/watch?v=tlW9H1ESWwA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=JS9X5sR4kKQ">http://www.youtube.com/watch?v=JS9X5sR4kKQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=rc7R24gFzsc">http://www.youtube.com/watch?v=rc7R24gFzsc</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=70fkozyxUlI">http://www.youtube.com/watch?v=70fkozyxUlI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.8</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=SaQH4fo7fl8">http://www.youtube.com/watch?v=SaQH4fo7fl8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.9</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=o6j3PdWOT8E">http://www.youtube.com/watch?v=o6j3PdWOT8E</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.10</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=99Z7JPuwlK4">http://www.youtube.com/watch?v=99Z7JPuwlK4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.2.11</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=yzRz0pQp-sM">http://www.youtube.com/watch?v=yzRz0pQp-sM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=dIUxmxwU1r0">http://www.youtube.com/watch?v=dIUxmxwU1r0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=xLi_G8jFQ3E">http://www.youtube.com/watch?v=xLi_G8jFQ3E</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=qp924SuugFQ">http://www.youtube.com/watch?v=qp924SuugFQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=EBFf6exwkyU">http://www.youtube.com/watch?v=EBFf6exwkyU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=V5Ly1p4YrMU">http://www.youtube.com/watch?v=V5Ly1p4YrMU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=StchdGc4gww">http://www.youtube.com/watch?v=StchdGc4gww</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.4.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=45E1A7zqmQo">http://www.youtube.com/watch?v=45E1A7zqmQo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.5.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=VAHpW4bs938">http://www.youtube.com/watch?v=VAHpW4bs938</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.5.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=_eCVa_bC9us">http://www.youtube.com/watch?v=_eCVa_bC9us</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.5.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=sisR94LHIh4">http://www.youtube.com/watch?v=sisR94LHIh4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>3.5.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=99_ZVS2DbiU">http://www.youtube.com/watch?v=99_ZVS2DbiU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.1.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Aa4Zi3GH414">http://www.youtube.com/watch?v=Aa4Zi3GH414</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.1.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=QUe5p9F6g7M">http://www.youtube.com/watch?v=QUe5p9F6g7M</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.1.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Ne7c-tlyHBI">http://www.youtube.com/watch?v=Ne7c-tlyHBI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.1.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=vjfH0r9N3GA">http://www.youtube.com/watch?v=vjfH0r9N3GA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.2.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=n4xx3-cyTdk">http://www.youtube.com/watch?v=n4xx3-cyTdk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.2.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=wgepX5Lq3AI">http://www.youtube.com/watch?v=wgepX5Lq3AI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.2.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=HJln-s263n8">http://www.youtube.com/watch?v=HJln-s263n8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.2.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=ik4U_nPK5Vk">http://www.youtube.com/watch?v=ik4U_nPK5Vk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.2.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=1tyjx9USTi8">http://www.youtube.com/watch?v=1tyjx9USTi8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.3.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=hc9P3cJYDoA">http://www.youtube.com/watch?v=hc9P3cJYDoA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.3.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=KwxFyBbQIPU">http://www.youtube.com/watch?v=KwxFyBbQIPU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.3.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=-VaUIdvzyME">http://www.youtube.com/watch?v=-VaUIdvzyME</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.3.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=wyOWQZIbCPA">http://www.youtube.com/watch?v=wyOWQZIbCPA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.4.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=2r15YLRXy5E">http://www.youtube.com/watch?v=2r15YLRXy5E</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.4.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=oyOdDXoBJPk">http://www.youtube.com/watch?v=oyOdDXoBJPk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.4.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=EFIE8UKP7wo">http://www.youtube.com/watch?v=EFIE8UKP7wo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.4.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=-tV5wsbbGpU">http://www.youtube.com/watch?v=-tV5wsbbGpU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.5.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=BbYPJ2IkULg">http://www.youtube.com/watch?v=BbYPJ2IkULg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.5.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=NzXx4l0E30A">http://www.youtube.com/watch?v=NzXx4l0E30A</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.5.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=3axMImrzIDA">http://www.youtube.com/watch?v=3axMImrzIDA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.5.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=k2jPJgCF2u4">http://www.youtube.com/watch?v=k2jPJgCF2u4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.6.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=gzOggyQtSvg">http://www.youtube.com/watch?v=gzOggyQtSvg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.6.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=nb0M5k3u6UM">http://www.youtube.com/watch?v=nb0M5k3u6UM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.6.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=vl8BFqE2apY">http://www.youtube.com/watch?v=vl8BFqE2apY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.7.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=YaGBHlQVunI">http://www.youtube.com/watch?v=YaGBHlQVunI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.7.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=QBsdpT1hBn4">http://www.youtube.com/watch?v=QBsdpT1hBn4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>4.7.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=hS9P4VISSHg">http://www.youtube.com/watch?v=hS9P4VISSHg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.1.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Xa2_bd9aXdw">http://www.youtube.com/watch?v=Xa2_bd9aXdw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.1.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=bvLuEVzynPE">http://www.youtube.com/watch?v=bvLuEVzynPE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.1.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=TN0C-XC-0ew">http://www.youtube.com/watch?v=TN0C-XC-0ew</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.2.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Nmmq-1N7r4g">http://www.youtube.com/watch?v=Nmmq-1N7r4g</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.2.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=6WDUOlEaV94">http://www.youtube.com/watch?v=6WDUOlEaV94</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.2.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=42DB6qXPIe0">http://www.youtube.com/watch?v=42DB6qXPIe0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.2.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=G5Z4glhmMfQ">http://www.youtube.com/watch?v=G5Z4glhmMfQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.2.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=gG56DRbMbtk">http://www.youtube.com/watch?v=gG56DRbMbtk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.2.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=jgyZETxqGT8">http://www.youtube.com/watch?v=jgyZETxqGT8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.3.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=bgujFyvKH00">http://www.youtube.com/watch?v=bgujFyvKH00</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.3.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=o_TG7zvXiu0">http://www.youtube.com/watch?v=o_TG7zvXiu0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.3.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=2ZAlyOkrBx0">http://www.youtube.com/watch?v=2ZAlyOkrBx0</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.3.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=SvqNOy24-MM">http://www.youtube.com/watch?v=SvqNOy24-MM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.4.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=fHagGwivmDo">http://www.youtube.com/watch?v=fHagGwivmDo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.4.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=hvD5xyoPowA">http://www.youtube.com/watch?v=hvD5xyoPowA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.4.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=d_XJCjwdDBg">http://www.youtube.com/watch?v=d_XJCjwdDBg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.4.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=bM0k1SqdQAg">http://www.youtube.com/watch?v=bM0k1SqdQAg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.4.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=vT9UvTjtLJs">http://www.youtube.com/watch?v=vT9UvTjtLJs</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.4.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=r_YI54nnZIU">http://www.youtube.com/watch?v=r_YI54nnZIU</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.5.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=dxA4lf9f7vM">http://www.youtube.com/watch?v=dxA4lf9f7vM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.5.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=OZd6jvdj7So">http://www.youtube.com/watch?v=OZd6jvdj7So</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.5.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=EYAP-U8dKXk">http://www.youtube.com/watch?v=EYAP-U8dKXk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.5.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=eLeoeYJO494">http://www.youtube.com/watch?v=eLeoeYJO494</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.5.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=hpGTm0BysaQ">http://www.youtube.com/watch?v=hpGTm0BysaQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>7.5.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=7U_OkOjacr4">http://www.youtube.com/watch?v=7U_OkOjacr4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.1.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=LLrvj61CYXA">http://www.youtube.com/watch?v=LLrvj61CYXA</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.1.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=IG0baJXSXoc">http://www.youtube.com/watch?v=IG0baJXSXoc</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.1.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=8WrGRbMavIc">http://www.youtube.com/watch?v=8WrGRbMavIc</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.1.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=JVLOPbyi7g4">http://www.youtube.com/watch?v=JVLOPbyi7g4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.1.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=C8kKzCbHuEY">http://www.youtube.com/watch?v=C8kKzCbHuEY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.1.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=05CQH1IO9Yo">http://www.youtube.com/watch?v=05CQH1IO9Yo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.2.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=YdMkRLL_k7E">http://www.youtube.com/watch?v=YdMkRLL_k7E</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.2.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=CHE0lw4zSYQ">http://www.youtube.com/watch?v=CHE0lw4zSYQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.2.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=Vr0uzXu7UJM">http://www.youtube.com/watch?v=Vr0uzXu7UJM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.2.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=C8CZwyU8juQ">http://www.youtube.com/watch?v=C8CZwyU8juQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=DCDtpvb6EjQ">http://www.youtube.com/watch?v=DCDtpvb6EjQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=6a14mWfCkyM">http://www.youtube.com/watch?v=6a14mWfCkyM</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=b4lwCuj3Xzc">http://www.youtube.com/watch?v=b4lwCuj3Xzc</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=nb4HqIdmu8Y">http://www.youtube.com/watch?v=nb4HqIdmu8Y</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=sw7f8CfHSF4">http://www.youtube.com/watch?v=sw7f8CfHSF4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=9tZEfaFpw88">http://www.youtube.com/watch?v=9tZEfaFpw88</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.3.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=zF7kmVgyTWw">http://www.youtube.com/watch?v=zF7kmVgyTWw</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=xgANudHZHLI">http://www.youtube.com/watch?v=xgANudHZHLI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=LxaQAs1SkO8">http://www.youtube.com/watch?v=LxaQAs1SkO8</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=bERwEgCjLiI">http://www.youtube.com/watch?v=bERwEgCjLiI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=2Bf2mQwzDN4">http://www.youtube.com/watch?v=2Bf2mQwzDN4</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p>No Video for this exercise</p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=oniBUYRKexE">http://www.youtube.com/watch?v=oniBUYRKexE</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.4.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=3aQw5xzH4Hg">http://www.youtube.com/watch?v=3aQw5xzH4Hg</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.1</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=yfGrUprJ1vY">http://www.youtube.com/watch?v=yfGrUprJ1vY</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.2</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=iPuFI4iutWQ">http://www.youtube.com/watch?v=iPuFI4iutWQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.3</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=0JDyg8liCMk">http://www.youtube.com/watch?v=0JDyg8liCMk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.4</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=JT4f72PyivI">http://www.youtube.com/watch?v=JT4f72PyivI</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.5</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=PeQJamOFDkQ">http://www.youtube.com/watch?v=PeQJamOFDkQ</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.6</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=sL46q0c7O8s">http://www.youtube.com/watch?v=sL46q0c7O8s</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.7</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=eEAsBEiymQk">http://www.youtube.com/watch?v=eEAsBEiymQk</a></p>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">
                                       <p>8.5.8</p>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <p><a href="http://www.youtube.com/watch?v=hqgsavguECo">http://www.youtube.com/watch?v=hqgsavguECo</a></p>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/math/courses/clarklinks_000.pcf">©</a>
      </div>
   </body>
</html>