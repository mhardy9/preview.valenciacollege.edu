<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/mathconnections.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="../index.html">Learning Support Services Home</a></li>
                                                   
                                                   <li><a href="../communications/index.html">Communications - West</a></li>
                                                   
                                                   <li><a href="../testing/index.html">Testing - West</a></li>
                                                   
                                                   <li><a href="../tutoring/index.html">Tutoring - West</a></li>
                                                   
                                                   <li><a href="../../west/math/index.html">Math Division - West</a></li>
                                                   
                                                   <li><a href="../../east/academicsuccess/math/index.html">East Math Center</a></li>
                                                   
                                                   <li><a href="../../osceola/learningcenter/tutoring2.html">Osceola Tutoring</a></li>
                                                   
                                                   <li><a href="../../wp/mathcenter/index.html">Winter Park Math Center</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a name="content" id="content"></a>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      <img alt="Math Connections" height="111" src="MathConnections.jpg" width="770"> 
                                                      
                                                      <p><strong>The Math Connections (7-255) is located inside the Math Center (7-240) on West Campus.
                                                            The Math Connections  is a learning community, where students are encouraged to interact
                                                            with their peers along with math instructors. Students currently registered in MAT0018C,
                                                            MAT0022C,  MAT0028C, and MAT1033C are welcome to use the Math Connections services.
                                                            </strong></p>
                                                      
                                                      
                                                      <h3>Hours of Operation </h3>
                                                      
                                                      <h4>Math Connections:<br>
                                                         Monday to Thursday - 10 am to 7 pm<br>
                                                         Friday - 10 am to 5 pm<br>
                                                         Saturday - CLOSED
                                                      </h4>
                                                      
                                                      
                                                      <h3>MAT0018C CHAPTERS
                                                         
                                                      </h3>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CChapter1.pdf">MAT0018C Chapter 1 </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CChapter4.pdf">MAT0018C Chapter 4</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CDevelopmentalMathIchapter7.pdf">MAT0018C Chapter 7 </a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CChapter2.pdf">MAT0018C Chapter 2</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CChapter5.pdf">MAT0018C Chapter 5</a></div>
                                                               
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CChapter3.pdf">MAT0018C Chapter 3</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018CChapter6.pdf">MAT0018C Chapter 6 </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0018FER.pdf">Final Exam Review Workshop Packet </a></div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>    
                                                      
                                                      
                                                      <h3 span="">MAT0022C CHAPTERS</h3>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22Ch1.pdf">Chapter 1 - Whole Numbers</a> 
                                                               </div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/22Ch5.pdf">Chapter 5 - Decimals </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/22Ch9.pdf">Chapter 9 - Problem Solving</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/22Ch11.pdf">Chatper 11 - Factoring</a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><a href="documents/MAT0022midtermreviewMC.pdf">Midterm Review - MC</a><br>
                                                                     <a href="documents/MAT0022midtermreviewSA.pdf">Midterm Review - SA
                                                                        </a> 
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/22Ch2.pdf">Chapter 2 - Integers </a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22Ch3.pdf">Chapter 3 - Equations</a> 
                                                               </div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/22Ch13.pdf">Chapter 13 - Graphing </a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22Ch12.pdf">Chapter 12 - Rationals</a> 
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22_28FER.pdf">Final Exam Review Study Guide</a><a href="documents/22_28FinalExamReview.pdf"><br>
                                                                     </a>Print and Bring to Review Session 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><a href="documents/22ch4_Intro.pdf">Chapter 4 - Fractions (Intro)</a> 
                                                                  </p>
                                                                  
                                                                  <p><a href="documents/22ch4.pdf">Chapter 4 - Fractions </a> 
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/22Ch6.pdf">Chapter 6 - Ratios, Proportions, Percentages </a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22Ch10.pdf">Chapter 10 - Polynomials</a> 
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22Ch15.pdf">Chapter 15 - Radicals</a> 
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/22_28FinalExamReviewforvids.pdf">Final Exam Review VIDEO Study Guide</a><br>
                                                                  <br>
                                                                  <a href="http://www.screencast.com/t/G9KNZkg5ClK">Video Review</a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/MAT0022and0028FinalExamReviewPresentationfinal_000.pptx">Interactive Final Exam Review</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/MAT0022and0028FinalExamReviewPresentationfinal.pdf">Final Exam Review PDF</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>    
                                                      
                                                      
                                                      <h3 span="">MAT0028C CHAPTERS</h3>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter1.pdf">MAT0028C Chapter 1</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter4.pdf">MAT0028C Chapter 4</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter8.pdf">MAT0028C Chapter 8</a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter2.pdf">MAT0028C Chapter 2</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter6.pdf">MAT0028C Chapter 6</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter9.pdf">MAT0028C Chapter 9</a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter3.pdf">MAT0028C Chapter 3</a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/MAT0028CChapter7.pdf">MAT0028C Chapter 7</a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><a href="documents/22_28FER.pdf">Final Exam Practice</a><a href="documents/22_28FinalExamReview.pdf"><br>
                                                                        </a>Print and bring to review
                                                                  </p>
                                                                  
                                                                  <p><a href="documents/22_28FinalExamReviewforvids.pdf">Final Exam Review VIDEO Study Guide</a><a href="documents/22_28FinalExamReview.pdf"><br>
                                                                        <br>
                                                                        </a><a href="http://www.screencast.com/t/G9KNZkg5ClK">Video Review </a><a href="documents/22_28FinalExamReview.pdf"><br>
                                                                        </a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/MAT0022and0028FinalExamReviewPresentationfinal.pptx">Interactive Final Exam Review</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/MAT0022and0028FinalExamReviewPresentationfinal.pdf">Final Exam Review PDF</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <h3 span="">MAT1033C CHAPTERS</h3>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter2.pdf">MAT1033C Chapter 2 </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter5.pdf">MAT1033C Chapter 5 </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter8.pdf">MAT1033C Chapter 8 </a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/1033FinalExamReviewGuideLIVE_000.pdf" target="_blank">Final Exam Review Study Guide </a><br>
                                                                  Print and bring to the review 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter3.pdf">MAT1033C Chapter 3 </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter6.pdf">MAT1033C Chapter 6 </a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/PracticeFinalExam1033.pdf" target="_blank">Practice Final Exam</a><br>
                                                                  (Courtesy of Prof. Kincade) 
                                                                  
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/1033FinalExamReviewGuide2.pdf" target="_blank">Final Exam Review VIDEO Study Guide</a><br>
                                                                  <a href="http://www.screencast.com/t/Gfs7gzt0">Video of Final Exam Review Part 1</a><br>
                                                                  <a href="http://www.screencast.com/t/ekRoxKa2ARzr">Video of Final Exam Review Part 2 </a><br>
                                                                  <a href="http://www.screencast.com/t/yZ20Dg5xR">Video of Final Exam Review Part 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter4.pdf">MAT1033C Chapter 4 </a></div>
                                                               
                                                               <div data-old-tag="td"><a href="documents/Chapter7.pdf">MAT1033C Chapter 7 </a></div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/1033FinalExamReviewRedo.pdf" target="_blank">1033 Final Exam Review Redo</a><br>
                                                                  (Courtesy of Prof. Kincade) 
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/MAT-1033C-Final-Exam-Review.pdf" target="_blank">Interactive Final Exam Review</a><br>
                                                                  <a href="documents/MAT-1033C-Final-Exam-Review.ppsx" target="_blank">Interactive Final Exam Review</a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p><br>
                                                         <a href="QRcodesforfinalexamreview.html">Instructors at FINAL EXAM REVIEWS -- Click Here </a></p>
                                                      
                                                      
                                                      <h3 span="">MAT1033C Lab Project Helpful Hints Vids        </h3>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="https://youtu.be/tt1zV8PT0xM">Chapter 2</a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="https://youtu.be/xiA0yU97OY8">Chapter 3</a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="https://youtu.be/zmt5-byUy08">Chapter 6</a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="https://youtu.be/mT-W2lKJU2E">Chapter 7</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="https://youtu.be/OrhaceQy850">Chapter 8</a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="https://youtu.be/e9sesPRCEbw">Chapter 4</a></div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      *Closed Captioned Videos available on request
                                                      
                                                      
                                                      <h2>
                                                         <strong>Skills Worksheets</strong> 
                                                      </h2>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><strong>Natural Numbers, Whole Number and Integers</strong></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Place Value <br>
                                                                     <a href="documents/1.1PlaceValues1.pdf">Place Value 1</a><br>
                                                                     <a href="documents/1.1PlaceValues2.pdf">Place Value 2</a><br>
                                                                     <a href="documents/1.1PlaceValues3.pdf">Place Value 3 </a> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Rounding<br>
                                                                  <a href="documents/rounding_instr.pdf">Rounding 1</a><br>
                                                                  <a href="documents/rounding_stu.pdf">Rounding 2</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Add/Subtract Whole Numbers<br>
                                                                  <a href="documents/1.3AddSubWholeN1.pdf">Add/Subtract Whole 1</a><br>
                                                                  <a href="documents/1.3AddSubWholeN2.pdf">Add/Subtract Whole 2</a><br>
                                                                  <a href="documents/1.3AddSubWholeN3.pdf">Add/Subtract Whole 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiply Whole Numbers<br>
                                                                  <a href="documents/1.6MultiplyingWholeN1.pdf">Multiply Whole 1</a><br>
                                                                  <a href="documents/1.6MultiplyingWholeN2.pdf">Multiply Whole 2</a><br>
                                                                  <a href="documents/1.6MultiplyingWholeN3.pdf">Multiply Whole 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Divide Whole Numbers <br>
                                                                     <a href="documents/1.8DividingWholeN1.pdf">Divide Whole 1</a><br>
                                                                     <a href="documents/1.8DividingWholeN2.pdf">Divide Whole 2</a><br>
                                                                     <a href="documents/1.8DividingWholeN3.pdf">Divide Whole 3</a> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Rounding<br>
                                                                     <a href="documents/1.5RoundingEst1.pdf">Rounding 1</a><br>
                                                                     <a href="documents/1.5RoundingEst2.pdf">Rounding 2</a><br>
                                                                     <a href="documents/1.5RoundingEst3.pdf">Rounding 3</a> <br>
                                                                     
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graph Integers on Number Line <br>
                                                                  <a href="documents/2.1GraphInt1.pdf">Graph Integers 1</a><br>
                                                                  <a href="documents/2.1GraphInt2.pdf">Graph Integers 2</a><br>
                                                                  <a href="documents/2.1GraphInt3.pdf">Graph Integers 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Add Integers <br>
                                                                  <a href="documents/2.3AddingIntegers1.pdf">Add Integers 1</a><br>
                                                                  <a href="documents/2.3AddingIntegers2.pdf">Add Integers 2</a><br>
                                                                  <a href="documents/2.3AddingIntegers3.pdf">Add Integers 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Subtract Integers <br>
                                                                  <a href="documents/2.5SubtractIntegers1.pdf">Subtract Integers 1</a><br>
                                                                  <a href="documents/2.5SubtractIntegers2.pdf">Subtract Integers 2</a><br>
                                                                  <a href="documents/2.5SubtractIntegers3.pdf">Subtract Integers 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiply Integers <br>
                                                                  <a href="documents/2.7multiplyingintegers1.pdf">Multiply Integers 1</a> <br>
                                                                  <a href="documents/2.7multiplyingintegers2.pdf">Multiply Integers 2</a><br>
                                                                  <a href="documents/2.7multiplyingintegers3.pdf">Multiply Integers 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Divide Integers <br>
                                                                  <a href="documents/2.8Dividingintegers1.pdf">Divide Integers 1</a><br>
                                                                  <a href="documents/2.8Dividingintegers2.pdf">Divide Integers 2</a><br>
                                                                  <a href="documents/2.8Dividingintegers3.pdf">Divide Integers 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Exponents<br>
                                                                     <a href="documents/1.9Exponents1.pdf">Exponents 1</a><br>
                                                                     <a href="documents/1.9Exponents2.pdf">Exponents 2</a><br>
                                                                     <a href="documents/1.9Exponents3.pdf">Exponents 3</a> <br>
                                                                     
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Order of Operations <br>
                                                                  <a href="documents/1.9OrderofOps1.pdf">Order of Operations 1</a><br>
                                                                  <a href="documents/1.9OrderofOps2.pdf">Order of Operations 2</a><br>
                                                                  <a href="documents/1.9OrderofOps3.pdf">Order of Operations 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Order of Operations<br>
                                                                  <a href="documents/2.10OrderofOperations1.pdf">Order of Ops 1</a><br>
                                                                  <a href="documents/2.10OrderofOperations2.pdf">Order of Ops 2</a><br>
                                                                  <a href="documents/2.10OrderofOperations3.pdf">Order of Ops 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Arithmetic Including Rational Numbers (Fractions, Decimals)</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Exponents and Roots <br>
                                                                  <a href="documents/1.2ExponentsandSquareRoots1.pdf">Exponents and Roots 1</a> <br>
                                                                  <a href="documents/1.2ExponentsandSquareRoots2.pdf">Exponents and Roots 2</a> <br>
                                                                  <a href="documents/1.2ExponentsandSquareRoots3.pdf">Exponents and Roots 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <h5>
                                                         <br>
                                                         
                                                      </h5>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Introduction to Algebra and Variables</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Algebra Foundations<br>
                                                                  <a href="documents/1.1structureofalgebra1.PDF">Algebraic Properties 1</a><br>
                                                                  <a href="documents/1.1structureofalgebra2.pdf">Algebraic Properties 2 </a><br>
                                                                  <a href="documents/1.1structureofalgebra3.pdf">Algebraic Properties 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Combine Like Terms in Expressions (Distributive Property)<br>
                                                                  <a href="documents/3.1CombiningLikeTermsInAnAlgebraicExpression1.pdf">Combine 1</a><br>
                                                                  <a href="documents/3.1CombiningLikeTermsInAnAlgebraicExpression2.pdf">Combine 2</a><br>
                                                                  <a href="documents/3.1CombiningLikeTermsInAnAlgebraicExpression3.pdf">Combine 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Expressions</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Evaluate Mixed Expressions<br>
                                                                  <a href="documents/Evaluate1.pdf">Evaluate Mixed Expressions 1</a><br>
                                                                  <a href="documents/Evaluate2.pdf">Evaluate Mixed Expressions 2</a><br>
                                                                  <a href="documents/Evaluate3.pdf">Evaluate Mixed Expressions 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Evaluate Expressions with Multiplication and Division<br>
                                                                     <a href="documents/2.9EvaluatewithMultiplicationandDivision1.pdf">Evaluate 1</a> <br>
                                                                     <a href="documents/2.9EvaluatewithMultiplicationandDivision2.pdf">Evaluate 2</a><br>
                                                                     <a href="documents/2.9EvaluatewithMultiplicationandDivision3.pdf">Evaluate 3 </a></p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Translate Algebraic Expressions<br>
                                                                  <a href="documents/1.12TranslatingAlgExpressions1.pdf">Translate Expressions 1</a><br>
                                                                  <a href="documents/1.12TranslatingAlgExpressions2.pdf">Translate Expressions 2</a><br>
                                                                  <a href="documents/1.12TranslatingAlgExpressions3.pdf">Translate Expressions 3 </a><br>
                                                                  <a href="documents/1.12TranslatingAlgExpressions4.pdf">Translate Expressions 4</a><br>
                                                                  <a href="documents/1.12TranslatingAlgExpressions5.pdf">Translate Expressions 5</a><br>
                                                                  <a href="documents/1.12TranslatingAlgExpressions6.pdf">Translate Expressions 6</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Equations</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Solutions of an Equation <br>
                                                                     <a href="documents/1.11SolutionsofEqu1.pdf">Solutions 1 </a><br>
                                                                     <a href="documents/1.11SolutionsofEqu2.pdf">Solutions 2</a><br>
                                                                     <a href="documents/1.11SolutionsofEqu3.pdf">Solutions 3</a> <br>
                                                                     <a href="documents/2.11IdentifySolutionstoanEquation1.pdf">Solutions 4</a><br>
                                                                     <a href="documents/2.11IdentifySolutionstoanEquation2.pdf">Solutions 5</a><br>
                                                                     <a href="documents/2.11IdentifySolutionstoanEquation3.pdf">Solutions 6 </a> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Solve Linear Equations by Combining Like Terms <br>
                                                                  <a href="documents/3.7CombiningLikeTermsinEquations1.pdf">Solve 1</a><br>
                                                                  <a href="documents/3.7CombiningLikeTermsinEquations2.pdf">Solve 2</a><br>
                                                                  <a href="documents/3.7CombiningLikeTermsinEquations3.pdf">Solve 3</a> <br>
                                                                  <a href="documents/2.12SolvingLinearEquationsOneVariable1.pdf">Solve 4</a><br>
                                                                  <a href="documents/2.12SolvingLinearEquationsOneVariable2.pdf">Solve 5</a><br>
                                                                  <a href="documents/2.12SolvingLinearEquationsOneVariable3.pdf">Solve 6 </a><br>
                                                                  <a href="documents/2.3.28TheAdditionPrincipleofEquality1.pdf">Solve 7 </a><br>
                                                                  <a href="documents/2.3.28TheAdditionPrincipleofEquality2.pdf">Solve 8 </a><br>
                                                                  <a href="documents/2.3.28TheAdditionPrincipleofEquality3.pdf">Solve 9 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Addition Property of Equality<br>
                                                                  <a href="documents/2.3.28TheAdditionPrincipleofEquality1_000.pdf">Addition Property 1 </a><br>
                                                                  <a href="documents/2.3.28TheAdditionPrincipleofEquality2_000.pdf">Addition Property 2</a><br>
                                                                  <a href="documents/2.3.28TheAdditionPrincipleofEquality3_000.pdf">Addition Property 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiplication Property of Equality <br>
                                                                  <a href="documents/2.4.28TheMultiplicationPrincipleofEquality1.pdf">Multiplication Property 1 </a><br>
                                                                  <a href="documents/2.4.28TheMultiplicationPrincipleofEquality2.pdf">Multiplication Property 2 </a><br>
                                                                  <a href="documents/2.4.28TheMultiplicationPrincipleofEquality3.pdf">Multiplication Property 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Solve Multi-Step Equations<br>
                                                                  <a href="documents/EquationsMultiStep1.pdf">Solve Multi-Step Equations 1 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Translate Algebraic Equations<br>
                                                                  <a href="documents/1.13TranslatingAlgEquations1.pdf">Translate Equations 1 </a><br>
                                                                  <a href="documents/1.13TranslatingAlgEquations2.pdf">Translate Equations 2</a><br>
                                                                  <a href="documents/1.13TranslatingAlgEquations3.pdf">Translate Equations 3</a><br>
                                                                  <a href="documents/1.13TranslatingAlgEquations4.pdf">Translate Equations 4</a><br>
                                                                  <a href="documents/1.13TranslatingAlgEquations5.pdf">Translate Equations 5</a><br>
                                                                  <a href="documents/2.2.28TranslateEquations1.pdf">Translate Equations 6 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Solve for One Variable/Formulas/Literal Equations<br>
                                                                  <a href="documents/2.2.28LiteralEquations1.pdf">Literal Equations 1</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Inequalities</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <a href="documents/3.8.28Inequalities1.pdf">Inequalities 1</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/3.8.28Inequalities2.pdf">Inequalities 2 </a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="documents/3.8.28Inequalities3.pdf">Inequalities 3 </a></div>
                                                               
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Application Problems/Word Problems/Problem Solving </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Discounts and Mark-Ups<br>
                                                                  <a href="documents/3.1.28DiscMarkup1.pdf">Discounts and Mark-Ups 1</a><br>
                                                                  <a href="documents/3.1.28DiscMarkup2.pdf">Discounts and Mark-Ups 2</a><br>
                                                                  <a href="documents/3.1.28DiscMarkup3.pdf">Discounts and Mark-Ups 3</a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Interest</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Distance</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Geometry<br>
                                                                     <a href="documents/Geom1.pdf">Geometry 1</a><br>
                                                                     <a href="documents/Geom2.pdf">Geometry 2</a><br>
                                                                     <a href="documents/Geom3.pdf">Geometry 3 </a><br>
                                                                     <a href="documents/Geom4.pdf">Geometry 4</a><br>
                                                                     <a href="documents/Geom5.pdf">Geometry 5</a><br>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Consecutive Integers<br>
                                                                     <a href="documents/ConsecInt1.pdf">Consecutive Integers 1</a><br>
                                                                     <a href="documents/ConsecInt2.pdf">Consecutive Integers 2</a><br>
                                                                     <a href="documents/ConsecInt3.pdf">Consecutive Integers 3 
                                                                        </a><br>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Coordinate Systems and Vocabulary <br>
                                                                  <a href="documents/4.1.28RectangularCoordiateSystem1.pdf">Coordinate Systems 1</a><br>
                                                                  <a href="documents/4.1.28RectangularCoordiateSystem2.pdf">Coordinate Systems 2</a><br>
                                                                  <a href="documents/4.1.28RectangularCoordiateSystem3.pdf">Coordinate Systems 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing from Ordered Pairs<br>
                                                                  <a href="documents/4.2.28Graphing-OrderedPairs-MixedGraph1.pdf">Ordered Pairs 1</a><br>
                                                                  <a href="documents/4.2.28Graphing-OrderedPairs-MixedGraph2.pdf">Ordered Pairs 2</a><br>
                                                                  <a href="documents/4.2.28Graphing-OrderedPairs-MixedGraph3.pdf">Ordered Pairs 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing in Standard Form (Ax+By=C)<br>
                                                                  <a href="documents/AXBYC1.pdf">Ax+By=C 1</a><br>
                                                                  <a href="documents/AXBYC2.pdf">Ax+By=C 2</a><br>
                                                                  <a href="documents/AXBYC3.pdf">Ax+By=C 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Graphing in Slope-Intercept Form (y=mx+b)<br>                       
                                                                  <a href="documents/4.3.28ymxb1.pdf">y=mx+b 1 </a><br>
                                                                  <a href="documents/4.3.28ymxb2.pdf">y=mx+b 2</a><br>
                                                                  <a href="documents/4.3.28ymxb3.pdf">y=mx+b 3 </a><br>
                                                                  <a href="documents/4.4.28GraphingEquationsofLines-SlopeIntercept.pdf">y=mx+b 4 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Slope<br>
                                                                  <a href="documents/4.8slope1.pdf">Slope 1</a> <br>
                                                                  <a href="documents/4.8slope2.pdf">Slope 2</a><br>
                                                                  <a href="documents/4.8slope3.pdf">Slope 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Mixed Graphing<br>
                                                                     <a href="documents/4.5.28GraphingMixedLinearEquations.pdf">Graphing 1</a><br>
                                                                     <a href="documents/MixedLinearGraphing2.pdf">Graphing 2</a><br>
                                                                     <a href="documents/MixedLinearGraphing3.pdf">Graphing 3</a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Find the Equation of the Line<br>
                                                                     <a href="documents/EquofLine1.pdf">Line Equation 1</a><br>
                                                                     <a href="documents/EquofLine2.pdf">Line Equation 2</a><br>
                                                                     <a href="documents/EquofLine3.pdf">Line Equation 3 </a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Fractions</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://www.screencast.com/t/g4sLpTUrg0d">Fraction Review Video </a></div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Prime Factorization<br>
                                                                  <a href="documents/4.9PrimeFactor1.pdf">Prime Factorization 1</a><br>
                                                                  <a href="documents/4.9PrimeFactor2.pdf">Prime Factorization 2</a><br>
                                                                  <a href="documents/4.9PrimeFactor3.pdf">Prime Factorization 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplify Fractions<br>
                                                                  <a href="documents/SimplifyFractions1.pdf">Simplify Fractions 1</a><br>
                                                                  <a href="documents/SimplifyFractions2.pdf">Simplify Fractions 2</a><br>
                                                                  <a href="documents/SimplifyFractions3.pdf">Simplify Fractions 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Convert Improper Fractions to Mixed Number<br>
                                                                  <a href="documents/4.4ConversionImproperMixed1.pdf">Improper-&amp;gt;Mixed 1</a><br>
                                                                  <a href="documents/4.4ConversionImproperMixed2.pdf">Improper-&amp;gt;Mixed 2 </a><br>
                                                                  <a href="documents/4.4ConversionImproperMixed3.pdf">Improper-&amp;gt;Mixed 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Convert Mixed Numbers to Improper Fractions<br>
                                                                     <a href="documents/4.4ConversionMixedImproper1.pdf">Mixed-&amp;gt;Improper 1</a><br>
                                                                     <a href="documents/4.5ConversionMixedImproper2.pdf">Mixed-&amp;gt;Improper 2</a><br>
                                                                     <a href="documents/4.5ConversionMixedImproper3.pdf">Mixed-&amp;gt;Improper 3 </a><br>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiply Fractions <br>
                                                                  <a href="documents/4.10MultiplyFractions1.pdf">Multiply Fractions 1</a><a href="documents/4.10MultiplyFractions2.pdf"><br>
                                                                     Multiply Fractions 2</a><br>
                                                                  <a href="documents/4.10MultiplyFractions3.pdf">Multiply Fractions 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Divide Fractions <br>
                                                                  <a href="documents/4.11dividefract1.pdf">Divide Fractions 1</a><br>
                                                                  <a href="documents/4.11dividefract2.pdf">Divide Fractions 2 </a><br>
                                                                  <a href="documents/4.11dividefract3.pdf">Divide Fractions 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Add and Subtract Fractions with like Denominators<br>
                                                                  <a href="documents/4.12ASFractLike1.pdf">AddSub Like Denom 1</a><br>
                                                                  <a href="documents/4.12ASFractLike2.pdf">AddSub Like Denom 2</a><br>
                                                                  <a href="documents/4.12ASFractLike3.pdf">AddSub Like Denom 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Least Common Denominator/Multiple<br>
                                                                     <a href="documents/LCD.pdf">LCD
                                                                        </a><br>
                                                                     <a href="documents/4.16LCDLCM1.pdf">LCD 1</a><br>
                                                                     <a href="documents/4.16LCDLCM2.pdf">LCD 2</a><br>
                                                                     <a href="documents/4.16LCDLCM3.pdf">LCD 3
                                                                        </a> 
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Equivalent Fractions <br>
                                                                     <a href="documents/4.11EquivalentFractions1.pdf">Equivalent Fractions 1</a><br>
                                                                     <a href="documents/4.11EquivalentFractions2.pdf">Equivalent Fractions 2</a><br>
                                                                     <a href="documents/4.11EquivalentFractions3.pdf">Equivalent Fractions 3 
                                                                        </a><br>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Add and Subtract Fractions by Finding an LCD <br>
                                                                  <a href="documents/4.13ASFractLCD1.pdf">AddSub LCD 1 </a><br>
                                                                  <a href="documents/4.13ASFractLCD2.pdf">AddSub LCD 2</a><br>
                                                                  <a href="documents/4.13ASFractLCD3.pdf">AddSub LCD 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplify EXPRESSIONS with fractions <br>
                                                                  <a href="documents/4.14FractionExpressions.pdf">Fraction Expressions 1 </a><br>
                                                                  <a href="documents/4.14FractionExpressions2.pdf">Fraction Expressions 2</a><br>
                                                                  <a href="documents/4.14FractionExpressions3.pdf">Fraction Expressions 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Solve EQUATIONS with Fractions <br>
                                                                  <a href="documents/4.15SolveEQUwithFract1.pdf">Fraction Equations 1</a><br>
                                                                  <a href="documents/4.15SolveEQUwithFract2.pdf">Fraction Equations 2
                                                                     </a><br>
                                                                  <a href="documents/4.15SolveEQUwithFract3.pdf">Fraction Equations 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p><a href="documents/FractionAssessment1Student.pdf">1</a> <a href="documents/FractionAssessment2Student.pdf">2</a> <a href="documents/FractionAssessment3Student.pdf">3 </a></p>
                                                      
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Decimals</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Add and Subtract Decimals<br>
                                                                     <a href="documents/5.2.18AddSubtractDecimals1.pdf">Add and Subtract Decimals 1 </a><br>
                                                                     <a href="documents/5.2.18AddSubtractDecimals2.pdf">Add and Subtract Decimals 2 </a><br>
                                                                     <a href="documents/5.2.18AddSubtractDecimals3.pdf">Add and Subtract Decimals 3 </a> 
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiply Decimals<br>
                                                                  <a href="documents/5.3.18MultiplyingDecimals.pdf">Multiply Decimals 1</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Divide Decimals<br>
                                                                  <a href="documents/5.4.18DividingDecimals.pdf">Divide Decimals 1 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Decimal Equations and Problem Solving<br>
                                                                     <a href="documents/5.6.18DecimalEquationsProblemSolving1.pdf">Decimal Equations/Problem Solving 1 </a><br>
                                                                     <a href="documents/5.6.18DecimalEquationsProblemSolving2.pdf">Decimal Equations/Problem Solving 2 </a><br>
                                                                     <a href="documents/5.6.18DecimalEquationsProblemSolving3.pdf">Decimal Equations/Problem Solving 3 </a><br>
                                                                     
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Fraction -&amp;gt; Decimals -&amp;gt; Percents </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Fraction, Decimal, Percent Conversion <br>
                                                                     <a href="documents/FDP1.docx">FDP1</a><br>
                                                                     <a href="documents/FDP2.docx">FDP2</a><br>
                                                                     <a href="documents/FDP3.docx">FDP3
                                                                        </a><br>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>                 
                                                      
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Polynomials</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Evaluate and Exponents<br>
                                                                  <a href="documents/3.8.22Multippoly-ExpLaws1.pdf">Exponents 1</a><br>
                                                                  <a href="documents/3.8.22Multippoly-ExpLaws2.pdf">Exponents 2</a><br>
                                                                  <a href="documents/3.8.22Multippoly-ExpLaws3.pdf">Exponents 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Exponents<br>
                                                                  <a href="documents/1.9Exponents1.pdf">Exponents 1</a><br>
                                                                  <a href="documents/1.9Exponents2.pdf">Exponents 2</a><br>
                                                                  <a href="documents/1.9Exponents3.pdf">Exponents 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Exponents Rules and Monomials<br>
                                                                  <a href="documents/3.4.28ExponentRulesandMultiplyingMonomials1.pdf">Exponents and Monomials 1 </a><br>
                                                                  <a href="documents/3.4.28ExponentRulesandMultiplyingMonomials2.pdf">Exponents and Monomials 2</a><br>
                                                                  <a href="documents/3.4.28ExponentRulesandMultiplyingMonomials3.pdf">Exponents and Monomials 3</a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Add and Subtract Polynomials<br>
                                                                  <a href="documents/3.3.28Addingandsubtractingpolynomials1.pdf">Add/Sub Poly 1</a><br>
                                                                  <a href="documents/3.3.28Addingandsubtractingpolynomials2.pdf">Add/Sub Poly 2</a><br>
                                                                  <a href="documents/3.3.28Addingandsubtractingpolynomials3.pdf">Add/Sub Poly 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiplication of Polynomials<br>
                                                                  <a href="documents/3.8.28MultiplyingPolynomials1.pdf">Multiplication of Polynomials 1</a><br>
                                                                  <a href="documents/3.8.28MultiplyingPolynomials2.pdf">Multiplication of Polynomials 2</a><br>
                                                                  <a href="documents/3.8.28MultiplyingPolynomials3.pdf">Multiplication of Polynomials 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiplying Polynomials (Special Products)<br>
                                                                  <a href="documents/3.9.28Multiplyusingtherulesforspecialproducts1.pdf">Special Products 1 </a><br>
                                                                  <a href="documents/3.9.28Multiplyusingtherulesforspecialproducts2.pdf">Special Products 2</a><br>
                                                                  <a href="documents/3.9.28Multiplyusimgtherulesforspecialproducts3.pdf">Special Products 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Geometry</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Perimeter (Whole Numbers)<br>
                                                                  <a href="documents/1.4Perimeter1.pdf">Perimeter 1</a><br>
                                                                  <a href="documents/1.4Perimeter2.pdf">Perimeter 2</a><br>
                                                                  <a href="documents/1.4Perimeter3.pdf">Perimeter 3 </a><br>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>Area (whole numbers)<br>
                                                                     <a href="documents/1.7Area1.pdf">Area 1</a><br>
                                                                     <a href="documents/1.7Area2.pdf">Area 2</a><br>
                                                                     <a href="documents/1.7Area3.pdf">Area 3 </a><br>
                                                                     
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Mixed Skills
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Evaluate and Order of Ops with Division<br>
                                                                  <a href="documents/3.2.22Dividingpoly-ExpLaws1.pdf">Evaluate/ Order of Ops (Division) 1</a><br>
                                                                  <a href="documents/3.2.22Dividingpoly-ExpLaws2.pdf">Evaluate/ Order of Ops (Division) 2</a> <br>
                                                                  <a href="documents/3.2.22Dividingpoly-ExpLaws3.pdf">Evaluate/ Order of Ops (Division) 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Constants/Variables/Evaluate/Order of Ops<br>
                                                                  <a href="documents/1.7.22ConVarExpOrderofOP1.pdf">Cont/Var/Eval/OrderofOps 1</a><br>
                                                                  <a href="documents/1.7.22ConVarExpOrderofOP2.pdf">Cont/Var/Eval/OrderofOps 2</a><br>
                                                                  <a href="documents/1.7.22ConVarExpOrderofOP3.pdf">Cont/Var/Eval/OrderofOps 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Addition and Subtraction of Integers, Rationals, and Polynomials<br>
                                                                  <a href="documents/1.6.22AdditionPolyInteg1add.pdf">Addition of Polynomials and Integers, Rationals</a><br>
                                                                  <a href="documents/1.6.22SubPolyIntegSub2.pdf">Subtraction of Polynomials and Integers, Rationals </a><br>
                                                                  <a href="documents/1.6.22Add-SubPolyIntegCombined3.pdf">Add/Subtract Polynomimals and Integers, Rationals</a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Translate Mixed Word Problems<br>
                                                                  <a href="documents/2.2.28Formulas1.pdf">Formulas 1</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>GCF<br>
                                                                     <a href="documents/6.1.28.pdf">GCF 1</a><br>
                                                                     <a href="documents/6.1.28factorGCF2.pdf">GCF 2</a><br>
                                                                     <a href="documents/6.1.28factorGCF3.pdf">GCF 3</a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Grouping<br>
                                                                  <a href="documents/6.2.28grouping1.pdf">Grouping 1</a><br>
                                                                  <a href="documents/6.2.28grouping2.pdf">Grouping 2</a><br>
                                                                  <a href="documents/6.2.28grouping3.pdf">Grouping 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">x^2+bx+c Trinomials<br>
                                                                  <a href="documents/6.3.28Factoringxsquared1.pdf">x^2+bx+c Trinomials 1 </a><br>
                                                                  <a href="documents/6.3.28Factoringxsquared2.pdf">x^2+bx+c Trinomials 2</a><br>
                                                                  <a href="documents/6.3.28Factoringxsquared3.pdf">x^2+bx+c Trinomials 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">ax^2+bx+c Trinomials<br>
                                                                  <a href="documents/4.17.28Factoranotone1.pdf">ax^2+bx+c Trinomials 1</a><br>
                                                                  <a href="documents/4.17.28Factoranotone2.pdf">ax^2+bx+c Trinomials 2</a><br>
                                                                  <a href="documents/4.17.28Factoranotone3.pdf">ax^2+bx+c Trinomials 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Special Products (Perfect Sq Tri, Diff of Sq) <br>
                                                                  <a href="documents/4.16.28SpecialProducts1.pdf">Special Products 1</a><br>
                                                                  <a href="documents/4.16.28SpecialProducts2.pdf">Special Products 2</a><br>
                                                                  <a href="documents/4.16.28SpecialProducts3.pdf">Special Products 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Special Products - Perfect Square Trinomials<br>
                                                                     <a href="documents/PST1.pdf">Perfect Sq Tri 1</a><br>
                                                                     <a href="documents/PST2.pdf">Perfect Sq Tri 2 </a>                         
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Special Products - Difference of Squares <br>
                                                                  <a href="documents/6.19.28SpecialProducts_DifferenceSquares1.pdf">Diff of Squares 1 </a><br>
                                                                  <a href="documents/6.19.28SpecialProducts_DifferenceSquares2.pdf">Diff of Squares 2 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Special Products - Sum and Difference of Cubes <br>
                                                                  <a href="documents/6.19.28SpecialProducts_SumCubes1.pdf">Sum of Cubes 1</a><br>
                                                                  <a href="documents/6.19.28SpecialProducts_SumCubes2.pdf">Sum of Cubes 2</a><br>
                                                                  <a href="documents/6.19.28SpecialProducts_DifferenceCubes1.pdf">Difference of Cubes1</a><br>
                                                                  <a href="documents/6.19.28SpecialProducts_DifferenceCubes2.pdf">Difference of Cubes 2 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factor Completely (including sum and difference of cubes)<br>
                                                                  <a href="documents/6.21factorcompletelywithcubes.pdf">Factor Completely 1</a><br>
                                                                  <a href="documents/6.21factor2.pdf">Factor Completely 2</a><br>
                                                                  <a href="documents/6.21factor3.pdf">Factor Completely 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>More on Quadratics</p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Completing the Square<br>
                                                                  <a href="documents/CtS1.pdf">Completing the Square 1</a> <br>
                                                                  <a href="documents/CtS2.pdf">Completing the Square 2</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Rational Expressions and Equations</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Factoring/Simplifying/LCD with Variables<br>
                                                                  <a href="documents/6ratprereq1.pdf">Rational Prereq Skills 1</a><br>
                                                                  <a href="documents/6ratprereq2.pdf">Rational Prereq Skills 2</a><br>
                                                                  <a href="documents/6ratprereq3.pdf">Rational Prereq Skills 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Clearing Fractions with Numerical Denominators<br>
                                                                  <a href="documents/ClearFractions1.pdf">Clearing Fractions 1</a><br>
                                                                  <a href="documents/ClearFractions2.pdf">Clearing Fractions 2</a><br>
                                                                  Clearing Fractions 3 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Rational Expressions (Add and Subtract)<br>
                                                                  <a href="documents/AddSubRationals1.pdf">Add/Subtract 1</a><br>
                                                                  <a href="documents/AddSubRationals2.pdf">Add/Subtract 2</a><br>
                                                                  <a href="documents/AddSubRationals3.pdf">Add/Subtract 3</a> 
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Rational Equations<br>
                                                                  <a href="documents/6RatEquandPrereq1.pdf">Rational Equations 1</a><br>
                                                                  <a href="documents/6RatEquandPrereq2.pdf">Rational Equations 2</a><br>
                                                                  <a href="documents/6RatEquandPrereq3.pdf">Rational Equations 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      
                                                      
                                                      <p>Radicals</p>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Square Roots<br>
                                                                  <a href="documents/9-1-28-sqroot1.pdf">Square Roots 1</a><br>
                                                                  <a href="documents/9-1-28-sqroot2.pdf">Square Roots 2</a><br>
                                                                  <a href="documents/9-1-28-sqroot3.pdf">Square Roots 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Simplify Sqaure Roots<br>
                                                                  <a href="documents/9-2-28simpliradicals1.pdf">Simplify Sqaure Roots 1 </a><br>
                                                                  <a href="documents/9-2-28simpliradicals2.pdf">Simplify Sqaure Roots 2 </a><br>
                                                                  <a href="documents/9-2-28simpliradicals3.pdf">Simplify Sqaure Roots 3</a> <br>
                                                                  <a href="documents/9-1-33rootsmultiind1.pdf">Simplify Square Roots 4 </a><br>
                                                                  <a href="documents/9-1-33rootsmultiind2.pdf">Simplify Square Roots 5 <br>
                                                                     </a><a href="documents/9-1-33rootsmultiind3.pdf">Simplify Square Roots 6 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Add and Subtract Square Roots<br>
                                                                  <a href="documents/9-3-28addsubtradicals1.pdf">Add and Subtract Square Roots 1</a><br>
                                                                  <a href="documents/9-3-28addsubtradicals2.pdf">Add and Subtract Square Roots 2</a><br>
                                                                  <a href="documents/9-3-28addsubtradicals3.pdf">Add and Subtract Square Roots 3 
                                                                     </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Mulitply and Divide Roots (Mostly Square Roots)<br>
                                                                  <a href="documents/9-4-28multiplydivideradicals1.pdf">Mulitply and Divide Roots 1</a><br>
                                                                  <a href="documents/9-4-28multiplydivideradicals2.pdf">Mulitply and Divide Roots 2</a><br>
                                                                  <a href="documents/9-4-28multiplydivideradicals3.pdf">Mulitply and Divide Roots 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Multiply and Divide Mixed Radicals<br>
                                                                  <a href="documents/9-4-28multiplydivideradicals1.pdf">Multiply and Divide Radicals 1</a><br>
                                                                  <a href="documents/9-4-28multiplydivideradicals2.pdf">Multiply and Divide Radicals 2</a><br>
                                                                  <a href="documents/9-4-28multiplydivideradicals3.pdf">Multiply and Divide Radicals 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Rationalize Square Roots<br>
                                                                  <a href="documents/9-5-28RationalizeSquareRoot1.pdf">Rationalize Square Roots 1</a><br>
                                                                  <a href="documents/9-5-28RationalizeSquareRoot2.pdf">Rationalize Square Roots 2</a><br>
                                                                  <a href="documents/9-4-28multiplydivideradicals3.pdf">Rationalize Square Roots 3 </a>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Rationalize Radicals<br>
                                                                     <a href="documents/9-5-33RationalizeRadicals1.pdf">Rationalize Radicals 1</a><br>                      
                                                                     <a href="documents/9-5-33RationalizeRadicals2.pdf">Rationalize Radicals 2</a><br>
                                                                     <a href="documents/9-5-33RationalizeRadicals3.pdf">Rationalize Radicals 3 </a>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>         
                                                      
                                                      <p>Dimensional Analysis</p>
                                                      
                                                      <p><a href="documents/DimensionalAnalysis.pdf">Conversion Measurements </a></p>
                                                      
                                                      <p>Algebra<br>
                                                         Domain and Range<a href="DomainandRange.mp4">Domain and Range</a>         
                                                      </p>
                                                      
                                                      <p>Statistics</p>
                                                      
                                                      <p><a href="samplestdev.mp4">Finding sample stdev </a><br>
                                                         <a href="stdev.mp4">Sample and Population Standard Deviation "By Hand"</a><br>
                                                         <a href="ProbabilityDistribution.mp4">Probability Distribution </a><br>
                                                         <a href="residuals.mp4">Residuals</a></p>
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/mathconnections.pcf">©</a>
      </div>
   </body>
</html>