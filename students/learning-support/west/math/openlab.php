<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/math/openlab.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/math/">Math</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   <li><a href="../index.html">Learning Support Services Home</a></li>
                                                   
                                                   <li><a href="../communications/index.html">Communications - West</a></li>
                                                   
                                                   <li><a href="../testing/index.html">Testing - West</a></li>
                                                   
                                                   <li><a href="../tutoring/index.html">Tutoring - West</a></li>
                                                   
                                                   <li><a href="../../west/math/index.html">Math Division - West</a></li>
                                                   
                                                   <li><a href="../../east/academicsuccess/math/index.html">East Math Center</a></li>
                                                   
                                                   <li><a href="../../osceola/learningcenter/tutoring2.html">Osceola Tutoring</a></li>
                                                   
                                                   <li><a href="../../wp/mathcenter/index.html">Winter Park Math Center</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <a name="content" id="content"></a>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      <img alt="Open Lab" height="195" src="OpenLab_002.jpg" width="770">
                                                      
                                                      <h5>
                                                         <strong>Math Open Lab: The computer lab in 7-241 where Developmental Math students work on
                                                            lab activities in the presence of Lab Instructors to reinforce concepts learned in
                                                            their Math class</strong>.
                                                      </h5>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <h2><em><strong>Math Lab Orientation Presentations</strong></em></h2>
                                                                  
                                                                  <h4><span><strong>MAT 0018C, 0022C, 0028C</strong></span></h4>
                                                                  
                                                                  
                                                                  <h6><span>MAT 1033C </span></h6>
                                                                  
                                                                  <h6></h6>
                                                                  
                                                                  <h3>Online Orientation Videos: </h3>
                                                                  
                                                                  <p><span><strong>MAT 0018C, 0022C, 0028C</strong></span></p>
                                                                  
                                                                  
                                                                  <p><span><strong>MAT 1033C </strong></span></p>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <h2>Announcements</h2>
                                                                  
                                                                  <h4>Math Open Lab:<br>
                                                                     Monday to Thursday - 9 am to 9 pm<br>
                                                                     Friday - 9 am to 7 pm<br>
                                                                     Saturday - 10 am to 3 pm
                                                                  </h4>
                                                                  
                                                                  
                                                                  <h3>Math Lab Helpful Videos</h3>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="http://www.screencast.com/t/G9KNZkg5ClK">Mat 0022c/0028c final exam review video</a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h4><strong>Mat 1033c Lab project videos. </strong></h4>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><a href="https://youtu.be/tt1zV8PT0xM">Chapter 2 Lab Project </a></li>
                                                                     
                                                                     <li><a href="https://www.youtube.com/watch?v=xiA0yU97OY8">Chapter 3 Lab Project </a></li>
                                                                     
                                                                     <li><a href="https://www.youtube.com/watch?v=zmt5-byUy08">Chapter 6 Lab Project</a></li>
                                                                     
                                                                     <li><a href="https://www.youtube.com/watch?v=mT-W2lKJU2E">Chapter 7 Lab Project </a></li>
                                                                     
                                                                     <li><a href="https://www.youtube.com/watch?v=OrhaceQy850">Chapter 8 Lab Project </a></li>
                                                                     
                                                                     <li><a href="https://youtu.be/e9sesPRCEbw">Chapter 4 Lab Project </a></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <h4><strong>Mat 1033 Final Exam videos</strong></h4>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li><strong><a href="http://www.screencast.com/t/Gfs7gzt0">Online Review Part I</a></strong></li>
                                                                     
                                                                     <li><strong><a href="http://www.screencast.com/t/ekRoxKa2ARzr">Online Review Part II</a> </strong></li>
                                                                     
                                                                     <li><strong><a href="http://www.screencast.com/t/yZ20Dg5xR">Online Review Part III </a><a href="http://www.screencast.com/t/yZ20Dg5xR"></a></strong></li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  
                                                                  <p>To maximize your learning experience, bring your headphones or purchase them at the
                                                                     <a href="http://www.valenciabookstores.com/valencc/main/index.asp">Valencia bookstore</a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/math/openlab.pcf">©</a>
      </div>
   </body>
</html>