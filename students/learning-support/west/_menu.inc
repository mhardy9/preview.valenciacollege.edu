<ul>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/">Learning Support <i aria-hidden="true" class="far fa-angle-down"></i></a> 
		<ul>
			<li><a href="/students/learning-support/learning-centers.php">Learning Centers</a> </li>	
			<li><a href="/students/learning-support/writing-consultations.php">Writing Consultations</a></li>    
			<li><a href="/students/learning-support/skillshops.php">Skillshops</a> </li>
			<li><a href="http://net4.valenciacollege.edu/forms/students/learning-support/contact.cfm" target="_blank">Contact Us</a></li>
		</ul>
	</li>
	<li><a href="/students/learning-support/west/">West Learning Centers</a> </li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/communications">Communications <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
		<li><a href="/students/learning-support/west/communications/hours.php">Hours and Locations</a></li>
		<li><a href="/students/learning-support/west/communications/team.php">Staff</a></li>
		<li><a href="/students/learning-support/west/communications/writing-center/default.php">Writing Center</a></li>
		<li><a href="/students/learning-support/west/communications/writing-center/resources/student-resources.php">Student Resources</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/math/">Math <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/learning-support/math/hours.php">Hours and Locations</a></li>
			<li><a href="/students/learning-support/math/openlab.php">Open Lab</a></li>
			<li><a href="/students/learning-support/math/mission.php">Mission</a></li>
			<li><a href="/students/learning-support/math/mathresources.php">Math Resources</a></li>
			<li><a href="/students/learning-support/math/studyrooms.php">Group Study Rooms</a></li>
			<li><a href="/students/learning-support/math/mathconnections.php">Math Connections</a></li>
			<li><a href="/students/learning-support/math/policies.php">Policies and Procedures</a></li>
			<li><a href="/students/learning-support/math/team.php">Meet the Team</a></li>
			<li><a href="/students/learning-support/math/cpt.php">PERT Review</a></li>
			<li><a href="/students/learning-support/math/handson.php">Hands On Math</a></li>
		</ul></li>
	<li><a href="/students/learning-support/pert/">PERT </a></li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/testing/">Testing <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/learning-support/testing/hours.php">Hours and Locations</a></li>
			<li><a href="/students/learning-support/testing/faq.php">FAQs</a></li>
			<li><a href="/students/learning-support/testing/rules.php">Rules</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/tutoring/">Tutoring <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/learning-support/tutoring/services.php">Services Offered</a></li>
			<li><a href="/students/learning-support/tutoring/small-group-tutoring.php">Tutoring Schedules</a></li>
			<li><a href="/students/learning-support/tutoring/OnlineResources.php">Online Resources</a></li>
			<li><a href="/students/learning-support/tutoring/meet-the-team.php">Meet the Team</a></li>
			<li><a href="/students/learning-support/tutoring/studyrooms.php">Group Study Rooms</a></li>
		</ul>
	</li>
</ul>