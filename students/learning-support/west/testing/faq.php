<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Testing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/testing/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/testing/">Testing</a></li>
               <li>West Campus Testing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>FAQs - <a href="#Student">Student</a> - <a href="#Faculty">Faculty</a>
                           
                        </h2>
                        
                        <h2>
                           <a name="Student" id="Student"></a>Student FAQs
                        </h2>
                        
                        <h3>
                           <strong> What do I bring with me to the Testing Center?</strong> 
                        </h3>
                        
                        <p>A current Valencia student ID card, UCF student ID card, or a state driver's license
                           is required. <span>No exceptions!</span></p>
                        
                        
                        
                        <p>A Valencia I.D. can be obtained from any Campus Security office. </p>
                        
                        
                        <h3>
                           <strong>What do I need to know before I come for testing?</strong> 
                        </h3>
                        
                        <ol>
                           
                           <li>Your professor's last name. Tests are filed by the professor's name. </li>
                           
                           <li>Supplies your professor allows for your test. </li>
                           
                           <li>The deadline date for your test. Most tests are not available after the deadline unless
                              you make special arrangements with your professor in advance. 
                           </li>
                           
                        </ol>
                        
                        
                        <h3>
                           <strong>What are the Testing Center policies?</strong> 
                        </h3>
                        
                        <ol>
                           
                           <li>No electronic devices allowed. Cell phones and all other non-approved electronic devices
                              must be turned off. If you leave the room to answer a phone call or text, your test
                              is considered complete and must be turned in. 
                           </li>
                           
                           <li>
                              
                              <div>All materials not used during testing  must be placed in the RED VALENCIA TESTING
                                 CENTER bag provided during check.
                                 
                                 
                              </div>
                              
                           </li>
                           
                           <li>No restroom breaks are allowed after you receive your exam until your test is completed!
                              
                           </li>
                           
                           <li>You may not start an exam, then turn it in and return later to complete it. Give yourself
                              plenty of time to complete a test in one sitting. You must take a test after you have
                              seen it - incomplete tests are returned to your professor. 
                           </li>
                           
                           <li>All students are expected to comply with the Valencia College academic honesty policy
                              in the student handbook and posted in the Testing Centers. 
                           </li>
                           
                        </ol>
                        
                        <p><strong>Policies subject to change: Updated information available at each specific campus.
                              </strong></p>
                        
                        <p><strong><a href="rules.html">Full set of Rules</a> for West Campus Testing Center </strong></p>
                        
                        
                        <h3><strong>What do I need to do if I want a non-Valencia or non-UCF test proctored? </strong></h3>
                        
                        <ul>
                           
                           <li>PLEASE NOTE: Individual proctoring of non-Valencia tests is not arranged through the
                              Testing Center. It is arranged through Continuing Education. Please see below for
                              information about Continuing Education. <a href="http://preview.valenciacollege.edu/continuing-education/programs/workforce-testing/">http://preview.valenciacollege.edu/continuing-education/programs/workforce-testing/
                                 </a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h2>
                           <a name="Faculty" id="Faculty"></a>Faculty FAQs 
                        </h2>
                        
                        <h3>How do I submit a test for a student? </h3>
                        
                        <ol>
                           
                           <li>Use your Atlas Account - See Steps <span><a href="documents/HowtoUseAtlasOnlineTestingCenterReferralForm.pdf">HOW TO SUBMIT A TEST IN ATLAS</a> </span>
                              
                           </li>
                           
                           <li>Visit us in 11-142 for a paper form </li>
                           
                        </ol>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Any Valencia faculty member can use the Testing Center to provide proctoring for makeup
                                       tests for individual students, special accommodations tests for students with disabilities,
                                       and tests for hybrid or online courses. Some of the types of tests proctored are:
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>traditional paper tests </li>
                                       
                                       <li>computer graded multiple choice tests (ScanTron) </li>
                                       
                                       <li>online exams through WebCT - Blackboard </li>
                                       
                                       <li>other online exams (e.g. Excel) </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        <p>
                           <em><strong>Reminder</strong></em>: <strong>Tests will not be distributed during the last hour the Testing Center is open. Please
                              refer to the Testing Center's hours of operation for details</strong>.
                           
                        </p>
                        
                        <p>The Testing Center can hold tests for pickup, return them by inter-campus mail, or
                           scan/email  them to you. 
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 11, Rm 142 </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1323 </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday 8 am-9 pm<br>Friday: 8 am-12 pm<br>Saturday: 9 am-2 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/testing/faq.pcf">©</a>
      </div>
   </body>
</html>