<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Testing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/testing/hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/testing/">Testing</a></li>
               <li>West Campus Testing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2><strong>The last test is given one hour before the Testing Center closes </strong></h2>
                        
                        <h3><strong>Students who begin a test no later than one hour prior to closing may remain in the
                              Testing Center only until closing time to complete the test or until the instructor’s
                              specified time limit has been reached, whichever comes first. </strong></h3>
                        
                        
                        <h3><strong>College Closed Dates</strong></h3>
                        
                        <ul>
                           
                           <li>Monday, January 16 (Martin Luther King, Jr. Day)</li>
                           
                           <li>Friday, February 10 (Learning Day) </li>
                           
                           <li>Monday, March 13 through Sunday, March 19 (Spring Break)</li>
                           
                           <li>Monday, May 29 (Memorial Day)</li>
                           
                           <li>Tuesday, July 4 (Independence Day)</li>
                           
                           <li>Monday, September 4 (Labor Day)</li>
                           
                           <li> Thursday, October 12 (College Night)</li>
                           
                           <li>Wednesday, November 22 through Sunday, November 26 (Thanksgiving)</li>
                           
                           <li> Thursday, December 21 through Monday, January 1 (Winter Break)</li>
                           
                        </ul>            
                        
                        <h3>PERT Testing</h3>
                        
                        <p>PERT TESTING is only proctored in the West Campus Testing Center on Saturdays. During
                           the week, this testing is proctored in The Assessment Center, located in SSB 171.
                        </p>
                        
                        <p>Your staff contact person at the West Campus Testing Center is: </p>
                        
                        <p><strong><em><a href="mailto:gmorrison@valenciacollege.edu">Gerri Morrison</a>, Testing Center Specialist; 407-582-5639</em></strong></p>
                        
                        <p><strong>Please note: A Valencia ID Number and Offical Photo ID are required for PERT Testing.</strong></p>
                        
                        <h2>
                           <img alt="Testing Center 11-142 Photo" height="232" src="Building11UniversityCenterReduced.jpg" width="348"><a href="documents/Valencia_Campus_Map_West.pdf"> <img alt="West Campus Map Link" border="0" height="235" src="WestCampusMapLinkcopy_001.jpg" width="99"></a>
                           
                        </h2>
                        
                        
                        <p><em><strong><u>A Valencia photo ID card,  UCF photo ID card, or state driver's license is required
                                    for all tests</u>.&nbsp; </strong></em></p>
                        
                        
                        
                        
                        <h2>You can obtain a Valencia ID with VID number on it in the Security office, SSB-170.
                           
                        </h2>
                        
                        
                        <p>Please have your course title and instructor’s name when you check-in at the counter.</p>
                        
                        <p>Bring all required supplies you need for your instructor’s test (e.g., blue books,
                           scantron sheets, pens, pencils, notebook paper). You will be required to turn your
                           cell phone completely off when entering the testing area. 
                        </p>
                        
                        <p>Visit the Collegewide Testing Center website for more information on hours and locations
                           of other campus’ Testing Centers.
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 11, Rm 142 </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1323 </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday 8 am-9 pm<br>Friday: 8 am-12 pm<br>Saturday: 9 am-2 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        <h3>UCF Students: by appointment only via the <a href="http://testing.rc.ucf.edu/">Online Registration</a> site. 
                        </h3>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/testing/hours.pcf">©</a>
      </div>
   </body>
</html>