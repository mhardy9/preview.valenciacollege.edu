<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Testing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/testing/rules.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/testing/">Testing</a></li>
               <li>West Campus Testing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Testing Center Rules</h2>
                        
                        <ul>
                           
                           <li>Please have your <strong>Valencia photo ID</strong><strong>, UCF photo</strong> <strong>ID, or state driver's license</strong> in your hand and ready to show at the front desk in order to receive your test, and
                              <strong>know the last name of the teacher whose test you are taking</strong>.
                           </li>
                           
                           <li>Have all items to be used for testing in hand <strong>before</strong> entering the testing room.
                           </li>
                           
                           <li>Finish food and drinks before entering the testing room.</li>
                           
                           <li>Turn cell phones and electronic devices off before entering the testing room. You
                              may not talk on a cell phone or send or receive text messages in the testing room.
                              Doing so is cause for an accusation of cheating. If you leave the testing room to
                              answer a phone call or text, or for any other reason, your test is considered completed,
                              and must be turned in.
                           </li>
                           
                           <li>Talking to other students in the testing room is cause for an accusation of cheating.</li>
                           
                           <li>Only your teacher can give you permission to use additional materials (notes, books,
                              etc.) during testing, and we must have such permission in writing. You may NOT use
                              additional materials of any kind unless specified by the test administrator <strong>before</strong> testing.
                           </li>
                           
                           <li>Your book bags and other items should be closed and remain closed while inside the
                              testing room. All items should be placed inside the security bag under your assigned
                              desk.
                           </li>
                           
                           <li>All electronic devices must be placed in the security bag provided.</li>
                           
                           <li><strong>Use of the calculator function on cell phones is not permitted on any test.</strong></li>
                           
                           <li>You may not leave the testing room after you have seen your test until the test is
                              completed. <strong>Restroom breaks are not permitted during a test</strong>. Please make a restroom stop before you come to the Testing Center.
                           </li>
                           
                           <li>You must take a test after you have seen it, and you must complete any test you begin.
                              Allow yourself plenty of time to finish a test because you may not return later to
                              complete it.
                           </li>
                           
                           <li>You may not write down any information from the test and take it out of the Testing
                              Center, and you may <strong>not</strong> make copies of a test taken on the computer.
                           </li>
                           
                           <li>All scratch paper must be turned in with the test.</li>
                           
                           <li>All items included with the test (tables, charts, etc.) must be turned in at the completion
                              of the test.
                           </li>
                           
                           <li>Read instructions carefully before you begin your test. If you are not sure of directions,
                              check with a testing staff member at the counter.
                           </li>
                           
                           <li>No tests are distributed the last hour before closing <strong>under any circumstances.</strong></li>
                           
                           <li>Tests will be collected at closing time.</li>
                           
                        </ul>
                        
                        <p><strong>Failure to comply with these regulations gives the test administrator the right to
                              collect test materials and return them to the instructor.</strong></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 11, Rm 142</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1323</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday 8 am-9 pm<br>Friday: 8 am-12 pm<br>Saturday: 9 am-2 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/testing/rules.pcf">©</a>
      </div>
   </body>
</html>