<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Testing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/testing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li>Testing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <p>Valencia's West Campus Testing Center is available to students for:</p>
                        
                        <ul>
                           
                           <li>Make-up exams arranged with a professor </li>
                           
                           <li>Online </li>
                           
                           <li>Special accommodations testing arranged with a professor </li>
                           
                        </ul>
                        
                        
                        <h3>Before Arriving at the Testing Center Students Will Need:</h3>
                        
                        <ul>
                           
                           <li>A student ID or state driver's license</li>
                           
                           <li>Instructor's last name</li>
                           
                           <li> Which exam(s) are being taken </li>
                           
                        </ul>            
                        
                        <h3>Faculty</h3>
                        
                        <ul>
                           
                           <li>For instructions on how to upload a test using Atlas, <a href="documents/HowtoUseAtlasOnlineTestingCenterReferralForm.pdf">click here</a>. 
                           </li>
                           
                           <li>During our peak testing periods, we may be unable to answer the phones, so please
                              contact the Testing Center staff at <a href="mailto:wec-testingcenter@valenciacollege.edu">wec-testingcenter@valenciacollege.edu</a>. 
                           </li>
                           
                           <li>Referral forms may be picked up and filled out in the Testing Center when exams are
                              submitted in person.
                           </li>
                           
                        </ul>            
                        
                        <h3>Important Reminders</h3>
                        
                        <ul>
                           
                           <li>You must have a current Valencia photo ID to take an exam in the Testing Center</li>
                           
                           <li>The last exam is given one hour prior to closing</li>
                           
                           <li>PERT, CPT, Compass, CJBAT, and TEAS tests are proctored in <a href="../../assessments/index.html">Assessment</a>, located in SSB-171, 407-582-1101 
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Closures for 2017</h3>
                        
                        <ul>
                           
                           <li>Monday, January 16 (Martin Luther King, Jr. Day)</li>
                           
                           <li>Friday, February 10 (Learning Day) </li>
                           
                           <li>Monday, March 13 through Sunday, March 19 (Spring Break)</li>
                           
                           <li>Monday, May 29 (Memorial Day)</li>
                           
                           <li>Tuesday, July 4 (Independence Day)</li>
                           
                           <li>Monday, September 4 (Labor Day)</li>
                           
                           <li> Thursday, October 12 (College Night)</li>
                           
                           <li>Wednesday, November 22 through Sunday, November 26 (Thanksgiving)</li>
                           
                           <li> Thursday, December 21 through Monday, January 1 (Winter Break)</li>
                           
                        </ul>            
                        
                        
                        <h3>DISCLAIMER</h3>
                        
                        <ul>
                           
                           <h3><strong><u>ALL STUDENTS MUST BE DONE</u> with their test(s) at the time the Testing Center closes. <em><u>NO EXCEPTIONS.</u></em></strong></h3>
                           
                        </ul>
                        
                        <p>Refusal to follow this rule will result in a staff member of the Testing Center contacting
                           the professor and reporting the student’s behavior!
                        </p>
                        
                        
                        
                        <p><strong>**PLEASE NOTE: Individual proctoring of non-Valencia tests is not arranged through
                              the Testing Center. It is arranged through <a href="http://www.valenciaenterprises.org/">Continuing Education </a>.</strong></p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 11, Rm 142 </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1323 </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday 8 am-9 pm<br>Friday: 8 am-12 pm<br>Saturday: 9 am-2 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        <h2>A current Valencia ID, UCF ID, or state driver's license is required</h2>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/testing/index.pcf">©</a>
      </div>
   </body>
</html>