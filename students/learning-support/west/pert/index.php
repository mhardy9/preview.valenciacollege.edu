<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/pert/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li>Pert</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div> 
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             
                                             <div data-old-tag="tbody">
                                                
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      <h2>PERT Review Schedule</h2>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         <h3>East</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Math PERT Review</strong></p>
                                                               
                                                               <p>4-102 (Math Center) <br>
                                                                  407-582-2775
                                                               </p>
                                                               
                                                               <p>Monday - Thursday: 9:00am - 12:00pm &amp; 2:00pm - 4:00pm </p>
                                                               
                                                               <p><strong>East Campus Pert Review Process</strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Students will go to the Math Center (4-102) and check in with R. Weinsier or his assistant</li>
                                                                  
                                                                  <li>R. Weinsier or his assistant will suggest what sections should be reviewed according
                                                                     to the student’s first PERT math score
                                                                  </li>
                                                                  
                                                                  <li>PERT review book options:</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>Buy the math PERT review booklet in the bookstore for about $4</li>
                                                                     
                                                                     <li>Use the online math PERT review booklet (valenciacollege.edu/east/academicsuccess/math)</li>
                                                                     
                                                                     <li>Borrow a math PERT review booklet from R. Weinsier that can only be used in the Math
                                                                        Center and cannot be written in
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>The math PERT review may be done at home, in the Math Center, or a combination of
                                                                     both with NO minimum or maximum time requirement
                                                                  </li>
                                                                  
                                                                  <li>When student tells R. Weinsier or his assistant that they do not have any more questions
                                                                     and they are made aware that they cannot take the math PERT test a third time then
                                                                     the retake certificate will be signed by R. Weinsier or his assistant
                                                                  </li>
                                                                  
                                                                  <li>Take the signed retake certificate and Business Office Referral to the Business Office
                                                                     (5-214) to pay the $10 fee
                                                                  </li>
                                                                  
                                                                  <li>Student will take the signed retake certificate and Business Office receipt to the
                                                                     Assessment office (5-237) to take their test
                                                                  </li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <hr>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <p><strong>Reading/Writing PERT Review</strong></p>
                                                               
                                                               <p>4-120 (Communication Center) <br>
                                                                  407-582-2795
                                                               </p>
                                                               
                                                               <p>Monday - Thursday: 9:00am - 7:00pm</p>
                                                               
                                                               <p><strong>East Campus Pert Review Process</strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Attend PERT review in the Communications Center (4-120)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>A lab assistant will register you for My Foundations Lab, the online, self-paced program
                                                                        for PERT review
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>After completing the online review, return to the Communications Center to obtain
                                                                     the PERT retake certificate and receive additional review materials as needed
                                                                  </li>
                                                                  
                                                                  <li>Take the PERT retake certificate to the Business Office (5-214) and pay the retake
                                                                     fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (5-237) </li>
                                                                  
                                                                  <li>Take the PERT </li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                         <h3>Lake Nona</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="table">
                                                                  
                                                                  
                                                                  <div data-old-tag="tbody">
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td"><strong>
                                                                              
                                                                              <a href="../../lakenona/tutoring-pert-review.html">
                                                                                 Math PERT Review 
                                                                                 </a>
                                                                              
                                                                              </strong></div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">1-230</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">407-582-7106</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">Monday - Thursday: 9:00am - 7:00pm<br>Friday: 10:00am - 4:00pm<br>Saturday: 9:00am - 1:00pm<br>
                                                                           <br>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <p><strong>LNC PERT Review Process</strong></p>
                                                               
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Assessment (1-206) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Tutoring Center (1-236)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>The PERT review booklet can be purchased in the Campus Store or borrowed in the Tutoring
                                                                        Center
                                                                     </li>
                                                                     
                                                                     <li>A tutor will sign the PERT retake certificate once the review is completed</li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>Take the PERT retake certificate to the Executive Dean’s Office (1-302) to pay for
                                                                     the retake fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (1-206) </li>
                                                                  
                                                                  <li>Take the PERT</li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                               <hr>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="table">
                                                                  
                                                                  
                                                                  <div data-old-tag="tbody">
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td"><strong>
                                                                              
                                                                              <a href="../../lakenona/tutoring-pert-review.html">
                                                                                 Reading/Writing PERT Review 
                                                                                 </a>
                                                                              
                                                                              </strong></div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">1-230</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">407-582-7106</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">Monday - Thursday: 10:00am - 6:00pm<br>Friday: 10:00am - 4:00pm<br>
                                                                           <br>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <p><strong>LNC PERT Review Process</strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Assessment (1-206) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Tutoring Center (1-236)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>The PERT review booklet can be purchased in the Campus Store or borrowed in the Tutoring
                                                                        Center
                                                                     </li>
                                                                     
                                                                     <li>A tutor will sign the PERT retake certificate once the review is completed</li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>Take the PERT retake certificate to the Executive Dean’s Office (1-302) to pay for
                                                                     the retake fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (1-206) </li>
                                                                  
                                                                  <li>Take the PERT </li>
                                                                  
                                                               </ul>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                         <h3>Osceola</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <p><strong>Math PERT Review</strong></p>
                                                               
                                                               <p>4-121 (The Depot)<br>
                                                                  407-582-4146
                                                               </p>
                                                               
                                                               <p>Monday - Thursday 8am -8pm<br>
                                                                  Friday - 8am -5pm<br>
                                                                  Saturday 8am- 12pm
                                                               </p>
                                                               
                                                               
                                                               <p><strong>Osceola PERT Review Process</strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>  Check-in with Assessment (4-248) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li> Attend PERT review in the Depot (4-121)
                                                                     
                                                                     <ul>
                                                                        
                                                                        <li>The PERT review booklet can be purchased in the Campus Store</li>
                                                                        
                                                                        <li>A tutor will sign the PERT retake certificate once the review is completed</li>
                                                                        
                                                                     </ul>
                                                                     
                                                                  </li>
                                                                  
                                                                  <li> Take the PERT retake certificate to the Business Office (2-110) to pay for the retake
                                                                     fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (1-206)</li>
                                                                  
                                                                  <li>Take the PERT</li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                               <hr>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <p><br>
                                                                  <strong>Reading/Writing PERT Review</strong><br>
                                                                  3-100 (Learning Center)<br>
                                                                  407-582-4250<br>
                                                                  
                                                               </p>
                                                               
                                                               <p>Monday - Thursday 8am -8pm<br>
                                                                  Friday - 8am -5pm<br>
                                                                  Saturday 8am- 12pm (At the Depot 4-121)
                                                               </p>
                                                               
                                                               <p><strong>Osceola PERT Review Process</strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li> Check-in with Assessment (4-248) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Learning Center (3-100)                                
                                                                     <ul>
                                                                        
                                                                        <li>The PERT review booklet can be purchased in the Campus Store or downloaded from the
                                                                           <a href="../../assessments/pert/taking-the-exam.html">Assessment website</a>
                                                                           
                                                                        </li>
                                                                        
                                                                     </ul>
                                                                     
                                                                  </li>
                                                                  
                                                                  <li>Take the PERT retake certificate to the Business Office (2-110) to pay for the retake
                                                                     fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (1-206)</li>
                                                                  
                                                                  <li>Take the PERT                                                                    
                                                                     
                                                                  </li>
                                                                  
                                                               </ul>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                         <h3>Poinciana</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>PERT Review Information</strong><strong> </strong></p>
                                                               
                                                               <p><strong>Poinciana Campus:</strong> The Plaza (Tutoring Center) Bldg. 1 Rm. 231<br>
                                                                  <strong>Phone:</strong> 407-582-6118<br>
                                                                  <strong>Hours: </strong><strong> </strong>Monday - Thursday: 10:00am - 6:00pm<br>
                                                                  Friday: 10:00am - 3:00pm
                                                               </p>
                                                               
                                                               <p><strong>Note:</strong> TUTOR AVALABILITY IS NOT GUARANTEED. 
                                                               </p>
                                                               
                                                               
                                                               <p><strong>Math PERT  Review Process </strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Testing &amp;  Assessment (1-325) to receive your PERT retake certificate.</li>
                                                                  
                                                                  <li>Visit The Plaza Tutoring  Center (1-231) to take the Math Diagnostic.</li>
                                                                  
                                                                  <li>Meet with the Lab  Supervisor or Lab Assistant to discuss what sections should be
                                                                     reviewed. <strong></strong>
                                                                     
                                                                  </li>
                                                                  
                                                                  <li><strong>PERT Review Options:</strong></li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>Purchase the PERT Review  Booklet for Mathematics in one of the other campus bookstores
                                                                        for $4.20.
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                               </ul>
                                                               
                                                               <ul type="disc">
                                                                  
                                                                  <ul type="circle">
                                                                     
                                                                     <li>Use the electronic version of        the <a href="http://bit.ly/2yIOKyw">PERT Review Booklet</a>
                                                                        
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                               </ul>
                                                               
                                                               <ul>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>Check-out the PERT  Review Booklet in The Plaza (4 hours).</li>
                                                                     
                                                                     <li>Visit The Plaza Tutoring  Center to register for the PERT Review  Course in IMathAS,
                                                                        a free of charge, online,  self-paced program. 
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>The math PERT review may  be done at home, in the tutoring center, or a combination
                                                                     of both with NO  minimum or maximum time requirement.
                                                                  </li>
                                                                  
                                                                  <li>After completing the  sections suggested either in the Review Booklet or online, return
                                                                     to The Plaza  to obtain the required signature on the PERT retake certificate from
                                                                     the Lab  Supervisor or Lab Assistant.
                                                                  </li>
                                                                  
                                                                  <li>Take the signed retake  certificate to Student Services (1-101) to pay the $10 fee
                                                                     for the math portion. 
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and  the PERT retake certificate back to Testing &amp; Assessment (1-325).</li>
                                                                  
                                                                  <li>Take the math portion of  the PERT.</li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                               <p><strong>Reading/Writing  PERT Review Process</strong></p>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Testing &amp; Assessment (1-325) to receive your  PERT retake certificate.</li>
                                                                  
                                                                  <li>Visit The Plaza Tutoring  Center (1-231) to take the reading and/or writing diagnostic(s).</li>
                                                                  
                                                                  <li>Attend PERT review in The Plaza Tutoring Center (1-231).</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>The PERT Review Booklets can be  purchased in the Osceola Campus Book Store (Reading
                                                                        $1.85 and Writing/Sentence  Skills $2.10), or they can be borrowed in The Plaza tutoring
                                                                        center (4 hours). 
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                               </ul>
                                                               
                                                               <ul type="disc">
                                                                  
                                                                  <ul type="circle">
                                                                     
                                                                     <li>The review booklets can also be accessed online: the "<a href="../../assessments/pert/documents/Reading.pdf" target="_blank">reading        review</a>" and the "<a href="../../assessments/pert/documents/Writing.pdf" target="_blank">writing  review</a>"
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                               </ul>
                                                               
                                                               <ul>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>The Lab Supervisor or Lab Assistant will  sign the PERT retake certificate once the
                                                                        review is completed.
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>Take the PERT retake  certificate to Student Services (1-101) to pay for the retake
                                                                     fee ($10 per  section).
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and  the PERT retake certificate back to Testing &amp; Assessment (1-325).
                                                                     
                                                                  </li>
                                                                  
                                                                  <li>Take the reading and/or  writing section(s) of the PERT.</li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                         
                                                         <h3>West</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               
                                                               
                                                               
                                                               <p>Math PERT Review</p>
                                                               
                                                               <h3>Location and Phone</h3>
                                                               
                                                               <p>7-241<br>
                                                                  407-582-1720 or 407-582-1780
                                                               </p>
                                                               
                                                               <h4>MATH PERT REVIEW ASSISTANCE IS OFFERED IN BUILDING 7, ROOM 241</h4>
                                                               
                                                               
                                                               <h3>Hours</h3>
                                                               
                                                               <p>Monday to Thursday: 12 pm to 6 pm<br>
                                                                  Saturday: 11 am to 2 pm.
                                                               </p>
                                                               
                                                               
                                                               
                                                               
                                                               <h3>PERT Review Procedure</h3>
                                                               
                                                               <h4>Students must bring their first attempt scores as well as the PERT Review book</h4>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Assessment (SSB 171) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Math Center (7-241)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>Purchase the <a href="../../assessments/pert/documents/PERTmathbookletUPDATE.pdf" target="_blank">PERT review booklet</a> in the bookstore (1-142) for $4 or print PDF
                                                                     </li>
                                                                     
                                                                     <li>You must register in the math center! No exceptions. ONLINE REVIEW NOT AVAILABLE FOR
                                                                        CPT-I.
                                                                     </li>
                                                                     
                                                                     <li>Consult  with a lab instructor to determine which sections you should focus on</li>
                                                                     
                                                                     <li>After completing the work, return to the Math Center to get the retake certificate
                                                                        signed 
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>Take the signed PERT retake certificate to the Business Office (SSB 101) and pay the
                                                                     retake fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (SSB 171)</li>
                                                                  
                                                                  <li>Take the PERT</li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                               
                                                               
                                                               <p>Reading/Writing PERT Review</p>
                                                               
                                                               
                                                               
                                                               <h3>Location and Phone</h3>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="table">
                                                                  
                                                                  
                                                                  <div data-old-tag="tbody">
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td"><strong>
                                                                              Reading/Writing PERT Review 
                                                                              </strong></div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">5-155</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">407-582-5454</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               
                                                               <h3>Hours</h3>
                                                               
                                                               <p>                          Monday - Thursday 8 am to 7 pm<br>
                                                                  Friday 8 am to 4 pm<br>
                                                                  Saturdays 9 am to 1 pm
                                                               </p>
                                                               
                                                               
                                                               <h3>PERT Review Procedure</h3>
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Assessment (SSB 171) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Communications Center (5-155)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>A lab assistant will provide information on how to review for a retake exam. </li>
                                                                     
                                                                     <li>Access the  <a href="../../assessments/pert/documents/NewReading.pdf" target="_blank">reading review</a> and the <a href="../../assessments/pert/documents/NewWriting.pdf" target="_blank">writing review</a>. These reviews are also available in the campus store for purchase. 
                                                                     </li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>After completing the online review, take the PERT retake certificate to the Business
                                                                     Office (SSB 101) and pay the retake fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (SSB 171) </li>
                                                                  
                                                                  <li>Take the PERT</li>
                                                                  
                                                               </ul>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                         
                                                         <h3>Winter Park</h3>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="table">
                                                                  
                                                                  
                                                                  <div data-old-tag="tbody">
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td"><strong>
                                                                              
                                                                              <a href="../../wp/mathcenter/mathhours.html">
                                                                                 Math PERT Review 
                                                                                 </a>
                                                                              
                                                                              </strong></div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">Room 138</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">407-582-6817 or 407-582-6912</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">
                                                                           <strong>Fall and Spring</strong><br>Monday - Thursday 8am to 7pm<br>Friday 8am to 3pm<br><br><strong>January 2 - January 6</strong>Monday - Friday 8am to noon and 1pm to 5pm<br>
                                                                           <br>
                                                                           <br>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Assessment (Room 104) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Tutoring Center (Room 138)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>The PERT review booklet can be purchased in the Campus Store</li>
                                                                     
                                                                     <li>A tutor will sign the PERT retake certificate once the review is completed</li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>Take the PERT retake certificate to the Answer Center (Room 210) to pay for the retake
                                                                     fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (Room 104) </li>
                                                                  
                                                                  <li>Take the PERT </li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                               
                                                               <hr>
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               
                                                               <div data-old-tag="table">
                                                                  
                                                                  
                                                                  <div data-old-tag="tbody">
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td"><strong>
                                                                              
                                                                              <a href="www.valenciacollege.edu/wp/cssc.html">
                                                                                 Reading/Writing PERT Review 
                                                                                 </a>
                                                                              
                                                                              </strong></div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">Room 136</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">407-582-6818</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     <div data-old-tag="tr">
                                                                        
                                                                        <div data-old-tag="td">Please allow 2 to 3 hours to complete the review.<br>
                                                                           <strong>Fall and Spring</strong>)<br>
                                                                           Monday - Thursday 8am to 7pm<br>
                                                                           Friday 8am to 3pm<br>
                                                                           <br>
                                                                           <strong>Summer</strong>
                                                                           Monday - Thursday 8am to 7pm<br>
                                                                           Friday 8am to 12pm<br>
                                                                           <br>
                                                                           
                                                                        </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     
                                                                     
                                                                     
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               
                                                               <ul>
                                                                  
                                                                  <li>Check-in with Assessment (Room 104) to receive your PERT retake certificate</li>
                                                                  
                                                                  <li>Attend PERT review in the Tutoring Center (Room 136)</li>
                                                                  
                                                                  <ul>
                                                                     
                                                                     <li>You can review topics through a self-paced computer program in the Communications
                                                                        Center during the listed hours. 
                                                                     </li>
                                                                     
                                                                     <li>A tutor will sign the PERT retake certificate once the review is completed</li>
                                                                     
                                                                  </ul>
                                                                  
                                                                  <li>Take the PERT retake certificate to the Answer Center (Room 210) to pay for the retake
                                                                     fee
                                                                  </li>
                                                                  
                                                                  <li>Bring the receipt and the PERT retake certificate back to Assessment (Room 104) </li>
                                                                  
                                                                  <li>Take the PERT </li>
                                                                  
                                                               </ul>
                                                               
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         <h3>College Closed Dates</h3>
                                                         
                                                         
                                                         
                                                         <div>
                                                            <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                                                            November 22, 2017 
                                                            - November 26, 2017 
                                                            
                                                         </div>
                                                         
                                                         
                                                         <div>
                                                            <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                                                            December 21, 2017 
                                                            - January 1, 2018 
                                                            
                                                         </div>
                                                         
                                                         
                                                         <div>
                                                            <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                                                            March 12, 2018 
                                                            - March 18, 2018 
                                                            
                                                         </div>
                                                         
                                                         
                                                         
                                                         
                                                         <a href="../../math/liveScribe.html" title="Math Help 24/7"><img alt="Math Help 24/7" border="0" height="60" src="math-24-7_245x60.png" width="245"></a>
                                                         
                                                         
                                                         <a href="../../student-services/skillshops.html"><img border="0" src="SkillShops_270x60.png" width="100%"></a>
                                                         
                                                         
                                                         
                                                         
                                                         
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/pert/index.pcf">©</a>
      </div>
   </body>
</html>