<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Writing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/communications/writing-center/services/expectations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/learning-support/communications/writing-center/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Writing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/communications/">Communications</a></li>
               <li><a href="/students/learning-support/west/communications/writing-center/">Writing Center</a></li>
               <li><a href="/students/learning-support/west/communications/writing-center/services/">Services</a></li>
               <li>West Writing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../../writing-center/index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Expectations</h2>
                        
                        <h4>We promise that:</h4>
                        
                        <p>Your consultation will be with an instructor. The Writing Center is staffed with Valencia
                           English, Reading, and EAP adjunct faculty and teachers from other disciplines bringing
                           similar teaching experience and credentials.
                        </p>
                        
                        <ul>
                           
                           <li>The consultation emphasis will be on teaching and learning.</li>
                           
                           <li>We will discuss as much as is reasonable in a 25-minute consultation.</li>
                           
                           <li> Your paper will probably leave better than when it came in. Improvements are inherently
                              a part of the writing consultation process.
                           </li>
                           
                           <li>You will be your own proof-reader, a transferable skill you can use everywhere.</li>
                           
                           <li>We’ll help you try to earn good grades, but grades are your responsibility.</li>
                           
                           <li>We will help you become a better writer!</li>
                           
                        </ul>
                        
                        
                        <h4>VALENCIA COLLEGE, WEST CAMPUS, WRITING CENTER 101</h4>
                        
                        <p>How to use the Writing Center to your benefit:</p>
                        
                        <ol>
                           
                           <li>Plan ahead! Make appointments well in advance of deadlines.</li>
                           
                           <li>Use our services up to three times a week (once each day) during your writing process.</li>
                           
                           <li>Bring your assignment (paper or electronic copy), sources, course materials, and questions
                              to the appointment. HINT: Paper draft copies allow for optimization of your time.
                           </li>
                           
                           <li>Identify and share your goals for the consultation.</li>
                           
                           <li>Let us know what the assignment requirements are and your professor’s expectations.</li>
                           
                           <li>Transfer the strategies you learn during the consultation to other writing tasks you
                              have.
                           </li>
                           
                           <li>Take notes during the consultation to help yourself when you’re working on your own.</li>
                           
                           <li> Plan your next steps as you revise and what you want to learn at your next Writing
                              Center visit.
                           </li>
                           
                           <li>Practice, practice, practice your new writing skills between visits!</li>
                           
                           <li> Be kind to yourself along the way; it’s called the writing process for a reason -
                              there are many steps to becoming a better writer, and you have started down the path!
                              
                           </li>
                           
                        </ol>
                        
                        <p>Check out our resources at: <a href="http://valenciacollege.edu/WestWritingCenter/" rel="nofollow noreferrer">http://valenciacollege.edu/WestWritingCenter</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3> 
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> West Campus </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 155</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-5454</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am-8pm<br>Friday: 8am-5pm<br>Saturday: 9am-2pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <p>
                           <a href="http://chompchomp.com/" target="_blank"><button><strong>Need a grammar refresher?<br>Try the MOOC on Grammar Bytes!</strong></button></a></p>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/communications/writing-center/services/expectations.pcf">©</a>
      </div>
   </body>
</html>