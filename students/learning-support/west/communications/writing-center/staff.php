<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Writing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/communications/writing-center/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/learning-support/communications/writing-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Writing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/communications/">Communications</a></li>
               <li><a href="/students/learning-support/west/communications/writing-center/">Writing Center</a></li>
               <li>West Writing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../writing-center/index.html"></a>
                        
                        
                        <section>
                           
                           <h2>Staff</h2>
                           
                           <h3> Don't let writing get you down. We're here to help.</h3>
                           
                           <div>
                              
                              <p>All of our consultants are highly qualified and ready to handle any writing question
                                 you can throw their way...OK, almost any question. They  love helping students with
                                 writing!
                              </p>
                              
                           </div>
                           
                           <br>
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody"> 
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <h3>Our Team</h3>
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="Meena Udho" height="143" src="Meena.jpg" width="98"><strong>Meena Udho</strong></p>
                                          
                                          <p><strong>Instructional Lab Supervisor</strong><br>
                                             <strong>Phone:</strong> 407-582-5063<br>
                                             <a href="mailto:mudho@valenciacollege.edu">mudho@valenciacollege.edu</a><br>
                                             
                                          </p>
                                          
                                          <p><strong>Bio:</strong> I received my B.A. in English and M.S. in Publishing from Pace University in New
                                             York. Having worked briefly in a small publishing house, I found my calling as an
                                             educator. I have worked at Valencia College as a writing consultant and instructor
                                             since 2012.
                                             I firmly believe that learning is a process; no substantial change will occur overnight,
                                             and every educator should facilitate that development and help nurture students' abilities.
                                             
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="Beverley Meer" height="143" src="Beverley.jpg" width="98"><strong>Beverley Meer</strong></p> 
                                          
                                          <p>
                                             <strong>Instructional Lab Assistant, Senior</strong><br>
                                             
                                          </p>
                                          
                                          <p><strong>Bio:</strong>I was born and raised in Dunkirk in Western New York State. I received my Bachelor
                                             of Arts at Northwestern University in Evanston, Illinois, majoring in English, minoring
                                             in history. I earned my Masters in Education at the State University of New York at
                                             Buffalo. I took additional post graduate hours in teaching reading at Buffalo State
                                             College. I taught Developmental English ten years at Indian River Community College
                                             in Stuart, Florida and twelve at Valencia College in Orlando. At present, I am working
                                             one-on-one with students in the West Campus writing center, a position which I enjoy
                                             very much. Otherwise, I might be singing in a chorus, swinging a pickle ball paddle,
                                             or dancing at Jazzercise! 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="Ellen Costello" height="143" src="Ellen.jpg" width="98"><strong>Ellen Costello</strong></p>                  
                                          
                                          <p>
                                             <strong>Instructional Lab Assistant, Senior</strong><br>
                                             
                                          </p>
                                          
                                          <p><strong>Bio:</strong> I grew up in a family of readers, and went to kindergarten when I was four since
                                             I loved school so much and couldn't wait until I was five! I have lived in New York,
                                             New Jersey, Connecticut, Washington, D.C., and Florida along the way. I have a Master
                                             of Education degree in Curriculum and Instruction with a concentration in Reading.
                                             Ellen worked in public K-12 literacy education in Washington, D.C. and Orlando, Florida
                                             from 1998 - 2015. Previously, I  was also an adjunct professor in the Reading Department
                                             at Valencia College. In addition to my job as a Sr. Instructional Assistant in the
                                             Writing Center, I am currently employed on the West Campus as an adjunct professor
                                             in the Educator Preparation Institute, as a private tutor, and substitute teacher.
                                             I love my job as a tutor at Valencia since I am able to meet and serve people who
                                             have such diverse backgrounds and needs.
                                          </p>
                                          
                                       </div>
                                       
                                    </div>    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="Hassan Tchenchana" height="143" src="Hassan.jpg" width="98"><strong>Hassan Tchenchana</strong></p>                  
                                          
                                          <p>
                                             <strong>Instructional Lab Assistant, Senior</strong><br>
                                             
                                          </p>
                                          
                                          <p><strong>Bio:</strong>  I am Moroccan, and I have been working at Valencia since August 2010. I have a BA
                                             in English, with a concentration in Linguistics from Morocco. Before moving to the
                                             United States, I taught English to non-native speakers in different institutions.
                                             Currently, I am an EAP instructor and a Writing Center consultant. I love languages;
                                             that is why I am trilingual (Arabic, French, and English), and I intend to learn Spanish
                                             soon.
                                          </p>
                                          
                                       </div>
                                       
                                    </div>    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="" height="131" src="photo-Peg-Spellman.jpg" width="98"><strong>Peg Spellman</strong></p>                  
                                          
                                          <p>
                                             <strong>Instructional Lab Assistant, Senior</strong></p>
                                          
                                       </div>
                                       
                                    </div>    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="Rhonda Oehlrich" height="143" src="Rhonda.jpg" width="98"><strong>Rhonda Oehlrich</strong></p>                  
                                          
                                          <p>
                                             <strong>Instructional Lab Assistant, Senior</strong><br>
                                             
                                          </p>
                                          
                                          <p><strong>Bio:</strong> At age 18, my first time in college, I had no desire to enter the teaching profession.
                                             After 20 some years as a Trainer, Coach, and Mentor I returned to school to advance
                                             my career as a Corporate Trainer. While completing my junior and senior years of college
                                             at UCF, as a non-traditional (older) student; I fell in love with academia and was
                                             impressed with this generation of students.
                                             Here I am ...with a passion to work as an instructor, one on one, in the Writing Center
                                             and as a Professor of Education.
                                             
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="" height="130" src="Stephanie.png" width="98"><strong>Stephanie Foley</strong></p>
                                          
                                          <p> <strong>Instructional Lab Assistant, Senior</strong></p>
                                          I graduated with a Bachelor of Fine Arts Degree in Theatre Performance from Northern
                                          Illinois University. Soon after, I found great passion and fulfillment in teaching
                                          as I taught second language learners reading and writing in Chicago and then in Southern
                                          California. I went on to attain a Master’s Degree in Education, specializing in teaching
                                          English to speakers of other languages. I have had the pleasure of teaching English,
                                          speech, reading and writing to middle and high school students of diverse background.
                                          Currently, I am an EAP Adjunct Professor and Writing Center Consultant at the Valencia
                                          West Campus and I am eager and honored to teach and learn with you!
                                       </div>
                                       
                                    </div>    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="" height="143" src="photo-Denise-McKnight.jpg" width="98"><strong>Denise McKnight</strong></p>
                                          
                                          <p><strong>Instructional Lab Assistant, Senior</strong></p>
                                          
                                          <p> I was born in Illinois, but I am almost a native Floridian as I have lived in Florida
                                             since I was three years old. I am a Valencia honors graduate, and I was included in
                                             Who's Who Among Students in American Junior/Community Colleges. I have been working
                                             in the Communications Center and West Campus Writing Center since dinosaurs roamed
                                             the Earth or for over twenty years. Students have remarked about how patient I am.
                                             I strive to recognize that each student is wonderful and unique and treat each as
                                             such. When I'm not on campus, my interests include reading good science fiction, mysteries,
                                             and biographies, collecting scarves, crafting, volunteering, visiting with my extended
                                             family, and being a good cat guardian to my tuxedo cat, Paws, who is otherwise called
                                             cat Houdini as no door remains closed to the talented four-pawed door opener. It continues
                                             to be a pleasure to be a part of the Valencia family and working with all of our wonderful
                                             students. I look forward to many years ahead with helping students discover and develop
                                             their unique talents.
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><img alt="" height="143" src="photo-Shannon-Murdock.jpg" width="98">Shannon Murdock</strong></p>
                                          
                                          <p><strong>Instructional Lab Assistant, Senior</strong></p>
                                          
                                          <p> I was born in Chicago, but spent my life in a small town in Indiana. Bored of the
                                             quiet life, I joined the Marines and served at Camp Pendleton, CA and Iwakuni, Japan.
                                             When my service was over, I moved to Florida because that’s where my parents had headed.
                                             I attended Valencia and earned my AA and then transferred to Rollins. In 2010, I earned
                                             my Bachelor of Fine Arts in English with a minor in Writing. I have always loved the
                                             written word in all its forms. I also believe that we should never stop learning,
                                             so stop by the Writing Center and let’s teach each other something new!
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><img alt="" height="143" src="default_profile-1.jpg" width="98">Leigh Nicoll</strong></p>
                                          
                                          <p><strong>Instructional Lab Assistant, Senior</strong></p>
                                          
                                          <p> I completed my Bachelor of Science degree in English Language Arts Education at UCF,
                                             and my Master's Degree in English Literature at the University of Glasgow, in Scotland.
                                             I also studied Spanish in Ecuador for a year, both at a language school and at the
                                             Universidad Central del Ecuador. I was a high school English teacher in Orlando for
                                             10 years, then decided to teach on the college level. For the last two years, I have
                                             been an Adjunct English Professor at Polytechnic University in Orlando, teaching English
                                             for Speakers of Other Languages and Composition I and II. I enjoy working in the Valencia
                                             College West Campus Writing Center where I am able to help students become stronger
                                             writers. To me, facilitating that growth in students is the best part of teaching!
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="" height="143" src="Krystelle.jpg" width="98"><strong>Krystelle Kubicek</strong></p>                  
                                          
                                          <p>
                                             <strong>Office Aide</strong><br>
                                             
                                          </p>
                                          
                                          <p><strong>Bio:</strong> I was born and raised in a far, far away land called Venezuela. Ever since I can
                                             remember, my dream has always been to become a veterinarian. My adventure began when
                                             I graduated high school and moved to Costa Rica to start veterinary school. After
                                             two years of studying and living in that magical land, my family and I moved again.
                                             So here you see me, a current UCF student who enjoys food, videogames, traveling,
                                             and science while endeavoring to fulfill my dream of becoming the ultimate animal
                                             doctor. 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><img alt="" height="124" src="photo-Earnest-Swan.jpg" width="98"><strong>Earnest Swan</strong></p>
                                          
                                          <p> <strong>Office Aide</strong></p>
                                          I live the Nomadic lifestyle which has taught me more than I could have ever wished
                                          for. Being a student of life is my greatest joy. I'm working towards an Engineering
                                          degree, which I'm going to use to build my dream airplane. I plan on being the top
                                          name in the Aerospace industry. Perfection is a moving target.
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p><strong><img alt="" height="143" src="photo-Andre-Tarifa-De-Oliveira.jpg" width="98"> Andre Tarifa De Oliveira</strong></p>
                                          
                                          <p><strong>Office Aide</strong></p>
                                          
                                          <p> In my early 30s, I decided to start fresh, so I moved from Brazil to Orlando and
                                             became Mickey's new neighbor. Every day I try to live life to its fullest. I love
                                             playing and watching all kinds of sports, and I'm addicted to movies. I've worked
                                             for Whirlpool, Telefonica, and other smaller companies in my career, and now I’m pursuing
                                             my degree in Hospitality Management at Valencia College. I'll do anything possible
                                             to help the writing center continue its excellent support of Valencia’s students.
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                        </section>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3> 
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> West Campus </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 155</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-5454</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am-8pm<br>Friday: 8am-5pm<br>Saturday: 9am-2pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <p>
                           <a href="http://chompchomp.com/" target="_blank"><button><strong>Need a grammar refresher?<br>Try the MOOC on Grammar Bytes!</strong></button></a></p>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/communications/writing-center/staff.pcf">©</a>
      </div>
   </body>
</html>