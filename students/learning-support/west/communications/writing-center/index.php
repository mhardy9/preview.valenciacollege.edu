<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/communications/writing-center/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/learning-support/communications/writing-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/communications/">Communications</a></li>
               <li>Writing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8">  <img alt="" height="8" src="arrow.gif" width="8"> West Writing Center
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Services</h3>
                                       <a href="services/default.html">
                                          <button>Services</button>
                                          </a> <a href="staff.html">
                                          <button>Staff</button>
                                          </a> <a href="resources/student-resources.html">
                                          <button>Student Resources</button>
                                          </a> <a href="resources/faculty-resources.html">
                                          <button>Faculty Resources</button>
                                          </a>
                                       
                                       
                                       <hr>
                                       
                                       <h3><a href="http://www.signupgenius.com/go/20f044aacae29a4f85-communications" target="_blank">Sign up here for Communications Center/Writing Center workshops</a></h3>
                                       
                                       <p><a href="../index.html">West Communications Center</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Making Appointments</h3>
                                       
                                       <p>You can make Writing Center appointments by calling 407-582-5454, stopping by 5-155,
                                          or through Atlas by clicking the button below. It's quick and easy!
                                       </p>
                                       <a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=http%3A%2F%2Fgcp.valenciacollege.edu%2FCPIP%2F%3Fsys%3Dwconline" target="_blank">
                                          <button>Log In</button>
                                          </a>
                                       
                                       
                                       
                                       
                                       <h3><strong>No appointment? The Mobile Writing Center offers first come, first served help, Monday-Thursday</strong></h3>
                                       
                                       <p> In the Communications Center (in 5-155H) 10 A.M.-12 P.M.</p>
                                       
                                       <p>In the group study area on the first floor of the library (Building 6): 12 P.M.-2
                                          P.M. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h3>Location, Contact, &amp; Hours</h3>
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       <div data-old-tag="table">
                                          
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"> West Campus </div>
                                                
                                             </div>
                                             
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">Bldg 5, Rm 155</div>
                                                
                                             </div>
                                             
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">407-582-5454</div>
                                                
                                             </div>
                                             
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">Monday - Thursday: 8am-8pm<br>Friday: 8am-5pm<br>Saturday: 9am-2pm
                                                </div>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       
                                       <hr>
                                       
                                       <p> <a href="http://chompchomp.com/" target="_blank">
                                             <button><strong>Need a grammar refresher?<br>
                                                   Try the MOOC on Grammar Bytes!</strong></button>
                                             </a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/communications/writing-center/index.pcf">©</a>
      </div>
   </body>
</html>