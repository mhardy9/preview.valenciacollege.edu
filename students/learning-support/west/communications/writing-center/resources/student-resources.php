<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Writing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/communications/writing-center/resources/student-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/learning-support/communications/writing-center/resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Writing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/communications/">Communications</a></li>
               <li><a href="/students/learning-support/west/communications/writing-center/">Writing Center</a></li>
               <li><a href="/students/learning-support/west/communications/writing-center/resources/">Resources</a></li>
               <li>West Writing Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../../../../writing-center/index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Student Resources</h2>
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>Writing &amp; Citations</h3>
                              
                              <h3>Dictionary</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://dictionary.reference.com/" target="_blank" title="Dictionary">Dictionary</a></li>
                                 
                                 <li><a href="http://www.merriam-webster.com/" target="_blank" title="merriam-webster">Merriam-Webster</a></li>
                                 
                              </ul>
                              
                              <h3>Research and Citation Links</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://www.apastyle.org/index.aspx?_ga=1.183085615.888214575.1468504136" target="_blank" title="APA style">Apa Style</a></li>
                                 
                                 <li><a href="http://libguides.valenciacollege.edu/west_comm_ctr" target="_blank" title="Communications and Writing Center LibGuide">Communications and Writing Center LibGuide</a></li>
                                 
                                 <li><a href="https://www.mla.org/MLA-Style" target="_blank" title="MLA style">MLA Style</a></li>
                                 
                                 <li><a href="https://owl.english.purdue.edu/owl/" target="_blank" title="Purdue University Online Writing Lab">Purdue University Online Writing Lab</a></li>
                                 
                                 <li><a href="../../../../library/mla-apa-chicago-guides/index.html" target="_blank" title="English Citation Guide">Valencia Library Citation Guide</a></li>
                                 
                                 <li><a href="http://cdn.wwnorton.com/coursepacks/materials/DOC_GUIDELINES_MLA_2016.pdf" target="_blank" title="Norton MLA 8th Ed. Guide">Norton MLA 8th Ed. Guide</a></li>
                                 
                              </ul> 
                              
                              <h3>Research and Citation Handouts</h3>
                              
                              <ul>
                                 
                                 <li><a href="documents/MLA-vs-APA.pdf.html" target="_blank">MLA vs. APA</a></li>
                                 
                              </ul> 
                              
                              <h3>Research and Citation Videos</h3>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=tUSaQ5-mDRI" target="_blank" title="How to Avoid Plagiarism">How to Avoid Plagiarism</a></li>
                                 
                              </ul> 
                              
                              <h3>Writing Links</h3>
                              
                              <ul>    
                                 
                                 <li><a href="http://web2.uvcs.uvic.ca/elc/studyzone/grammar.htm" target="_blank" title="English Language Center">English Language Center Study Zone</a></li>
                                 
                                 <li><a href="http://owl.excelsior.edu/" target="_blank" title="Excelsior College Online Writing Lab">Excelsior College Online Writing Lab</a></li>
                                 
                                 <li><a href="http://freerice.com/" target="_blank" title="Free Rice">Free Rice</a></li>
                                 
                                 <li><a href="http://grammar.ccc.commnet.edu/grammar/index.htm" target="_blank" title="Guide to Grammar and Writing">Guide to Grammar and Writing</a></li>
                                 
                                 
                                 <li><a href="http://www.mywritinglab.com/" target="_blank" title="My Writing Lab">My Writing Lab</a></li>
                                 
                                 <li><a href="http://www.varsitytutors.com/practice-tests" target="_blank" title="Practice Tests">Practice Tests</a></li>
                                 
                                 <li><a href="http://www2.actden.com/writ_den/index.htm" target="_blank" title="Writing Den">Writing Den</a></li>
                                 
                                 
                              </ul>
                              
                              <h3>Writing Handouts</h3>
                              
                              <ul>
                                 
                                 <li><a href="documents/Prof-Simmons-5-Paragraph-Essay.pdf" target="_blank">Five Paragraph Essay</a></li>
                                 
                                 <li><a href="documents/Thesis-Generator.pdf.html" target="_blank">Thesis Generator</a></li>
                                 
                                 <li><a href="documents/Sample-Introductions-and-Conclusions.pdf" target="_blank">Sample Introductions and Conclusions</a></li>
                                 
                              </ul>
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>Reading and Speech</h3>
                              
                              <h3>Reading Links</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://www.dartmouth.edu/~acskills/success/reading.html" target="_blank" title="Active Reading">Active Reading</a></li>
                                 
                                 <li><a href="http://guides.library.harvard.edu/sixreadinghabits" target="_blank" title="Annotating a Textbook">Annotating a Textbook</a></li>
                                 
                                 <li><a href="http://www.freereadingtest.com/free-reading-test.html" target="_blank" title="Fun Reading Practice">Fun Reading Practice</a></li>
                                 
                                 <li><a href="http://www.myreadinglab.com/" target="_blank" title="My Reading Lab">My Reading Lab</a></li>
                                 
                                 <li><a href="http://users.dhp.com/~laflemm/RfT/Tut2.htm" target="_blank" title="Reading for Thinking">Reading for Thinking--Fact and Opinion</a></li>
                                 
                                 <li><a href="http://www.staples.com/sbd/cre/marketing/technology-research-centers/ereaders/speed-reader/" target="_blank" title="Speed and Comprehension Practice">Speed &amp; Comprehension Practice</a></li>
                                 
                                 <li><a href="http://www.studygs.net/index.htm" target="_blank" title="Study Guides">Study Guides and Strategies</a></li>
                                 
                                 <li><a href="http://wordnetweb.princeton.edu/perl/webwn" target="_blank" title="Wordnet">Wordnet Online</a></li>
                                 
                              </ul>
                              
                              <h3>Reading Handouts</h3>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="../../documents/ReadingStrategies.pdf" title="Reading Strategies">Reading Strategies</a> 
                                 </li>
                                 
                                 <li><a href="../../documents/VocabularySkills.pdf" title="Vocabulary Skills">Vocabulary Skills</a></li>
                                 
                                 <li><a href="documents/READING-STRATEGIES.pdf" target="_blank">Reading Strategies</a></li>
                                 
                              </ul>
                              
                              <h3>Reading Videos</h3>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=2QMs24TTZrA&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=12" target="_blank" title="Author's Purpose">Author's Purpose</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=-5DMKme6RR8&amp;index=18&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Character Analysis">Character Analysis</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=Z2GGLbKoy3g&amp;index=44&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Character's Perspectives">Character's Perspectives</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=UmQ3ZSf3xSU&amp;index=20&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Claims and Support">Claims and Support</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=6spWj7Ol3x0&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Context Clues">Context Clues</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=n_g7Nq-sTIA&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=3" target="_blank" title="Literal vs. Inferential">Literal vs. Inferential</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=42SJTk2XSi4" target="_blank" title="Main Idea and supporting details">Main idea and supporting details</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=jjrwi_FCQS4&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=17" target="_blank" title="Multiple Main Ideas">Multiple Main Ideas</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=_AnYSohfjAg&amp;index=25&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Narrator's Point-of-View">Narrator's Point-of-View</a></li>
                                 
                                 <li>
                                    <a href="https://www.youtube.com/watch?v=NwLrYQuFabA&amp;index=5&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Summarizing">Summarizing</a><br>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <h3>Speech</h3>
                              
                              <h4>Speech Links</h4>
                              
                              <ul>
                                 
                                 <li><a href="http://www.ted.com/" target="_blank" title="TED">TED</a></li>
                                 
                                 <li><a href="http://writingcenter.unc.edu/handouts/speeches/" target="_blank">Developing a Speech</a></li>
                                 
                                 <li><a href="http://business.financialpost.com/business-insider/7-excellent-ways-to-start-a-presentation-and-capture-your-audiences-attention" target="_blank">Seven Ways to Start a Presentation</a></li>
                                 
                                 <li><a href="http://members.tripod.com/teaching_is_reaching/delivering_your_speech.htm">Methods of Speech Delivery</a></li>
                                 
                                 <li><a href="http://www.write-out-loud.com/overcoming-public-speaking-anxiety-breathing-exercises.html" target="_blank">Breathing Exercises to Help Overcome Speech Anxiety</a></li>
                                 
                                 <li><a href="http://sixminutes.dlugan.com/speech-preparation-8-practice-presentation/" target="_blank">Speech Preparation: How to Practice Your Presentation</a></li>
                                 
                                 <li><a href="http://www.uwstout.edu/counsel/speechanxiety.cfm" target="_blank">Dealing with Public Speaking Anxiety</a></li>
                                 
                                 <li><a href="http://www.cengage.com/cgi-wadsworth/course_products_wp.pl?fid=M20b&amp;product_isbn_issn=9780618444182" target="_blank">Targeting Pronunciation</a></li>
                                 
                              </ul>
                              
                              <h4>Speech Handouts</h4>
                              
                              <ul>
                                 
                                 <li><a href="../../documents/IPAchart.pdf" title="IPA Pronunciation Chart">IPA Pronunciation Chart</a></li>
                                 
                                 <li><a href="../../documents/SyllableStress.pdf" title="Syllable Stress Rules">Syllable Stress Rules</a></li>
                                 
                                 <li><a href="../../documents/WriteanOutline.pdf" title="Write an Outline">Write an Outline</a></li>
                                 
                                 <li><a href="https://www.nvcc.edu/annandale/asc/occ/_docs/transitions.pdf" target="_blank"> Speech Transitions</a></li>
                                 
                                 <li><a href="documents/MLA-for-speeches.pdf">MLA for Speeches</a></li>
                                 
                                 <li><a href="documents/A-Very-General-Speech-Outline.pdf">A Very General Speech Outline</a></li>
                                 
                                 <li>
                                    <a href="http://publicspeakingproject.org/PDF%20Files/confidence%20web%201.pdf" target="_blank">Speaking with Confidence</a><br>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <h4>Speech Videos</h4>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=wZ2o7huX8ww" target="_blank" title="talking about email video">Talking about Email</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=ZWWUU94T6uM" target="_blank">Give a Great Speech from the Start: Attention Getters</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=2_VvIr1KkLo" target="_blank">Examples of Attention Getters</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=rUeTDKsfGc8" target="_blank">Model Persuasive Speech</a></li>
                                 
                                 <li><a href="http://ed.ted.com/on/otWe6oXv" target="_blank">The Skill of Self Confidence</a></li>
                                 
                                 <li><a href="http://ed.ted.com/lessons/the-science-of-stage-fright-and-how-to-overcome-it-mikael-cho#watch" target="_blank">The Science of Stage Fright</a></li>
                                 
                                 <li><a href="http://www.ted.com/talks/karen_thompson_walker_what_fear_can_teach_us" target="_blank">What Fear Can Teach Us</a></li>
                                 
                                 <li><a href="https://www.ted.com/talks/kelly_mcgonigal_how_to_make_stress_your_friend?utm_source=facebook.com&amp;utm_medium=social&amp;utm_campaign=tedspread" target="_blank">How to Make Stress Your Friend</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=d1u7THOudfw" target="_blank">Tips for Managing Fear</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=F3Rl600Pm3g" target="_blank">Citing Sources in Your Speech</a></li>
                                 
                              </ul>
                              
                              
                           </div>
                           
                           <div>
                              
                              <h3>EAP</h3>
                              
                              <h3>Academic Software Links</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://www.myenglishlab.com/" target="_blank" title="My English Lab">My English Lab</a></li>
                                 
                                 <li><a href="http://myelt.heinle.com/" target="_blank" title="Reading Explorer">Reading Explorer</a></li>
                                 
                                 <li><a href="http://azarinteractive.com/" target="_blank" title="Azar Interactive">Azar Interactive</a></li>
                                 
                                 <li><a href="http://www.cambridgelms.org/main">Grammar And Beyond</a></li>
                                 
                                 <li><a href="http://ngl.cengage.com/sites/well-said/student/well-said/audio" target="_blank">Well Said</a></li>
                                 
                              </ul>
                              
                              <h3>Speech</h3>
                              
                              <h4>Speech Links</h4>
                              
                              <ul>
                                 
                                 <li><a href="http://www.manythings.org/slang/" target="_blank" title="Common American Slang">Common American Slang</a></li>
                                 
                                 <li><a href="http://www.eslcafe.com/" target="_blank" title="Dave's ESL Cafe">Dave's ESL Cafe</a></li>
                                 
                                 <li><a href="http://www.elllo.org/" target="_blank" title="Ello">Ello</a></li>
                                 
                                 <li><a href="http://www.englishlistening.com/" target="_blank" title="English Listening">English Listening</a></li>
                                 
                                 <li><a href="http://www.npr.org/" target="_blank" title="NPR">NPR</a></li>
                                 
                                 <li><a href="http://www.esl-lab.com/" target="_blank" title="Randal's ESL Cyber Listening Lab">Randal's ESL Cyber Listening Lab</a></li>
                                 
                                 <li><a href="http://rong-chang.com/" target="_blank" title="Rong-Chang">Rong-Chang</a></li>
                                 
                                 <li><a href="http://soundsofspeech.uiowa.edu/english/english.html">Phonetics-The Sounds of Spoken Language</a></li>
                                 
                                 <li><a href="http://www.ted.com/" target="_blank" title="TED">TED</a></li>
                                 
                                 <li><a href="http://writingcenter.unc.edu/handouts/speeches/" target="_blank">Developing a Speech</a></li>
                                 
                                 <li><a href="http://business.financialpost.com/business-insider/7-excellent-ways-to-start-a-presentation-and-capture-your-audiences-attention" target="_blank">Seven Ways to Start a Presentation</a></li>
                                 
                                 <li><a href="http://members.tripod.com/teaching_is_reaching/delivering_your_speech.htm">Methods of Speech Delivery</a></li>
                                 
                                 <li><a href="http://www.write-out-loud.com/overcoming-public-speaking-anxiety-breathing-exercises.html" target="_blank">Breathing Exercises to Help Overcome Speech Anxiety</a></li>
                                 
                                 <li><a href="http://sixminutes.dlugan.com/speech-preparation-8-practice-presentation/" target="_blank">Speech Preparation: How to Practice Your Presentation</a></li>
                                 
                                 <li><a href="http://www.uwstout.edu/counsel/speechanxiety.cfm" target="_blank">Dealing with Public Speaking Anxiety</a></li>
                                 
                                 <li><a href="http://www.cengage.com/cgi-wadsworth/course_products_wp.pl?fid=M20b&amp;product_isbn_issn=9780618444182" target="_blank">Targeting Pronunciation</a></li>
                                 
                              </ul>                  
                              
                              <h4>Speech Handouts</h4>
                              
                              <ul>
                                 
                                 <li><a href="../../documents/IPAchart.pdf" title="IPA Pronunciation Chart">IPA Pronunciation Chart</a></li>
                                 
                                 <li><a href="../../documents/SyllableStress.pdf" title="Syllable Stress Rules">Syllable Stress Rules</a></li>
                                 
                                 <li><a href="../../documents/WriteanOutline.pdf" title="Write an Outline">Write an Outline</a></li>
                                 
                                 
                                 <li><a href="https://www.nvcc.edu/annandale/asc/occ/_docs/transitions.pdf" target="_blank"> Speech Transitions</a></li>
                                 
                                 <li><a href="documents/MLA-for-speeches.pdf">MLA for Speeches</a></li>
                                 
                                 <li><a href="documents/A-Very-General-Speech-Outline.pdf">A Very General Speech Outline</a></li>
                                 
                                 <li>
                                    <a href="http://publicspeakingproject.org/PDF%20Files/confidence%20web%201.pdf" target="_blank">Speaking with Confidence</a><br>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <h4>Speech Videos</h4>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=wZ2o7huX8ww" target="_blank" title="talking about email video">Talking about Email</a></li>
                                 
                                 
                                 <li><a href="https://www.youtube.com/watch?v=ZWWUU94T6uM" target="_blank">Give a Great Speech from the Start: Attention Getters</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=2_VvIr1KkLo" target="_blank">Examples of Attention Getters</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=rUeTDKsfGc8" target="_blank">Model Persuasive Speech</a></li>
                                 
                                 <li><a href="http://ed.ted.com/on/otWe6oXv" target="_blank">The Skill of Self Confidence</a></li>
                                 
                                 <li><a href="http://ed.ted.com/lessons/the-science-of-stage-fright-and-how-to-overcome-it-mikael-cho#watch" target="_blank">The Science of Stage Fright</a></li>
                                 
                                 <li><a href="http://www.ted.com/talks/karen_thompson_walker_what_fear_can_teach_us" target="_blank">What Fear Can Teach Us</a></li>
                                 
                                 <li><a href="https://www.ted.com/talks/kelly_mcgonigal_how_to_make_stress_your_friend?utm_source=facebook.com&amp;utm_medium=social&amp;utm_campaign=tedspread" target="_blank">How to Make Stress Your Friend</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=d1u7THOudfw" target="_blank">Tips for Managing Fear</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=F3Rl600Pm3g" target="_blank">Citing Sources in Your Speech</a></li>
                                 
                              </ul>
                              
                              <h3>Writing</h3>
                              
                              <h4>Writing Links</h4>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/user/JenniferESL" target="_blank" title="English with Jeniffer">English with Jeniffer</a></li>
                                 
                                 <li><a href="http://englishpage.com/" target="_blank" title="English Page">English Page</a></li>
                                 
                                 <li><a href="https://www.ego4u.com/en/cram-up/grammar/tenses" target="_blank" title="Grammar Charts">Grammar Charts</a></li>
                                 
                                 <li><a href="http://www.manythings.org/" target="_blank" title="Interesting Things for ESL Students">Interesting Things for ESL Students</a></li>
                                 
                                 <li><a href="http://writing.umn.edu/sws/assets/pdf/verb_tense_chart.pdf" target="_blank" title="Verb Tense Chart">Verb Tense Chart</a></li>
                                 
                                 
                                 <li><a href="http://web2.uvcs.uvic.ca/elc/studyzone/grammar.htm" target="_blank" title="English Language Center">English Language Center Study Zone</a></li>
                                 
                                 <li><a href="http://owl.excelsior.edu/" target="_blank" title="Excelsior College Online Writing Lab">Excelsior College Online Writing Lab</a></li>
                                 
                                 <li><a href="http://freerice.com/" target="_blank" title="Free Rice">Free Rice</a></li>
                                 
                                 <li><a href="http://grammar.ccc.commnet.edu/grammar/index.htm" target="_blank" title="Guide to Grammar and Writing">Guide to Grammar and Writing</a></li>
                                 
                                 <li><a href="http://www.mywritinglab.com/" target="_blank" title="My Writing Lab">My Writing Lab</a></li>
                                 
                                 <li><a href="http://www.varsitytutors.com/practice-tests" target="_blank" title="Practice Tests">Practice Tests</a></li>
                                 
                                 <li><a href="http://www2.actden.com/writ_den/index.htm" target="_blank" title="Writing Den">Writing Den</a></li>
                                 
                              </ul>
                              
                              <h4>Writing Handouts</h4>
                              
                              <ul>
                                 
                                 <li><a href="documents/Prof-Simmons-5-Paragraph-Essay.pdf" target="_blank">Five Paragraph Essay</a></li>
                                 
                                 <li><a href="documents/Thesis-Generator.pdf.html" target="_blank">Thesis Generator</a></li>
                                 
                                 <li><a href="documents/Sample-Introductions-and-Conclusions.pdf" target="_blank">Sample Introductions and Conclusions</a></li>
                                 
                                 <li><a href="../../documents/WriteanOutline.pdf" title="Write an Outline">Write an Outline</a></li>
                                 
                              </ul>
                              
                              <h4>Writing Videos</h4>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=XaIc633l0hM" target="_blank" title="Email Dos and Don'ts Video">Email Dos and Don'ts</a></li>
                                 
                                 
                                 <li><a href="https://www.youtube.com/watch?v=tUSaQ5-mDRI" target="_blank" title="How to Avoid Plagiarism">How to Avoid Plagiarism</a></li>
                                 
                              </ul>
                              
                              <h3>Reading Links</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://www.dartmouth.edu/~acskills/success/reading.html" target="_blank" title="Active Reading">Active Reading</a></li>
                                 
                                 <li><a href="http://guides.library.harvard.edu/sixreadinghabits" target="_blank" title="Annotating a Textbook">Annotating a Textbook</a></li>
                                 
                                 <li><a href="http://www.freereadingtest.com/free-reading-test.html" target="_blank" title="Fun Reading Practice">Fun Reading Practice</a></li>
                                 
                                 <li><a href="http://www.myreadinglab.com/" target="_blank" title="My Reading Lab">My Reading Lab</a></li>
                                 
                                 <li><a href="http://users.dhp.com/~laflemm/RfT/Tut2.htm" target="_blank" title="Reading for Thinking">Reading for Thinking--Fact and Opinion</a></li>
                                 
                                 <li><a href="http://www.staples.com/sbd/cre/marketing/technology-research-centers/ereaders/speed-reader/" target="_blank" title="Speed and Comprehension Practice">Speed &amp; Comprehension Practice</a></li>
                                 
                                 <li><a href="http://www.studygs.net/index.htm" target="_blank" title="Study Guides">Study Guides and Strategies</a></li>
                                 
                                 <li><a href="http://wordnetweb.princeton.edu/perl/webwn" target="_blank" title="Wordnet">Wordnet Online</a></li>
                                 
                                 <li><a href="http://www.laflemm.com/hmco/RfRonline.html" target="_blank" title="Reading for Results">Reading for Results: Online</a></li>
                                 
                                 <li><a href="http://rong-chang.com/" target="_blank" title="Rong-Chang">Rong-Chang</a></li>
                                 
                              </ul>
                              
                              <h3>Reading Handouts</h3>
                              
                              <ul>
                                 
                                 <li><a href="../../documents/ReadingStrategies.pdf" title="Reading Strategies">Reading Strategies</a></li>
                                 
                                 <li><a href="../../documents/VocabularySkills.pdf" title="Vocabulary Skills">Vocabulary Skills</a></li>
                                 
                                 <li><a href="documents/READING-STRATEGIES.pdf" target="_blank">Reading Strategies</a></li>
                                 
                              </ul>
                              
                              <h3>Reading Videos</h3>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=2QMs24TTZrA&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=12" target="_blank" title="Author's Purpose">Author's Purpose</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=-5DMKme6RR8&amp;index=18&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Character Analysis">Character Analysis</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=Z2GGLbKoy3g&amp;index=44&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Character's Perspectives">Character's Perspectives</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=UmQ3ZSf3xSU&amp;index=20&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Claims and Support">Claims and Support</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=6spWj7Ol3x0&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Context Clues">Context Clues</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=n_g7Nq-sTIA&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=3" target="_blank" title="Literal vs. Inferential">Literal vs. Inferential</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=42SJTk2XSi4" target="_blank" title="Main Idea and supporting details">Main idea and supporting details</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=jjrwi_FCQS4&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=17" target="_blank" title="Multiple Main Ideas">Multiple Main Ideas</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=_AnYSohfjAg&amp;index=25&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Narrator's Point-of-View">Narrator's Point-of-View</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=NwLrYQuFabA&amp;index=5&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank" title="Summarizing">Summarizing</a></li>
                                 
                              </ul>
                              
                              <h3>Grammar &amp; Mechanics</h3>
                              
                              <h4>Links</h4>
                              
                              <ul>
                                 
                                 <li><a href="http://www.longman.com/ae/fog_level3/index.html" target="_blank" title="Focus on Grammar Level 3, 3rd edition">Focus on Grammar Level 3, 3rd edition</a></li>
                                 
                                 <li><a href="http://chompchomp.com/" target="_blank" title="Grammar Bytes">Grammar Bytes</a></li>
                                 
                                 <li><a href="http://grammar.quickanddirtytips.com/" target="_blank" title="Grammar Girl">Grammar Girl</a></li>
                                 
                                 <li><a href="http://www.grammar-quizzes.com/" target="_blank" title="Grammar-Quizzes">Grammar-Quizzes</a></li>
                                 
                                 <li><a href="http://grammar.ccc.commnet.edu/grammar/index.htm" target="_blank" title="Guide to Grammar and Writing">Guide to Grammar and Writing</a></li>
                                 
                                 <li><a href="http://www.varsitytutors.com/practice-tests" target="_blank" title="Practice Tests">Practice Tests</a></li>
                                 
                                 <li><a href="http://www.englishpage.com/irregularverbs/irregularverbs.html" target="_blank" title="Irregular Verb Dictionary">Irregular Verb Dictionary</a></li>
                                 
                                 <li><a href="http://www.e-grammar.org/download/most-common-irregular-verbs.pdf" target="_blank" title="Irregular Verb List">Irregular Verb List</a></li>
                                 
                                 <li><a href="http://www.chompchomp.com/handouts/irregularrules01.pdf" target="_blank" title="Rules for Using Irregular Verbs">Rules for Using Irregular Verbs</a></li>
                                 
                              </ul>
                              
                              <h4>Handouts</h4>
                              
                              <ul>
                                 
                                 <li><a href="documents/Basic-Rules-for-Making-Sentences.pdf" target="_blank" title="Basic Rules for Sentence Structure">Basic Rules for Sentence</a></li>
                                 
                                 <li><a href="documents/Verb-Tense-Chart.pdf" target="_blank">Verb Tense</a></li>
                                 
                                 <li><a href="documents/Coordinating-Conjunctions-FANBOYS.pdf" target="_blank">Coordinating Conjunctions (FANBOYS)</a></li>
                                 
                                 <li><a href="documents/NOUNS-VERBS.pdf.html" target="_blank">Nouns and Verbs</a></li>
                                 
                                 <li><a href="documents/Pluras-Versus-Possessive.pdf" target="_blank">Plural vs. Possessive Word Form</a></li>
                                 
                                 <li><a href="documents/Prepositions.pdf" target="_blank">Prepositions</a></li>
                                 
                                 <li><a href="documents/Comma-Colon-Semicolon.pdf" target="_blank">Commas, Colons, and Semicolons</a></li>
                                 
                                 <li><a href="documents/Word-Usage-and-Spelling.pdf" target="_blank">Word Usage and Spelling</a></li>
                                 
                                 <li><a href="../../documents/RulesforCapitalizationTipSheet.pdf" title="Rules for Capitalization TipSheet">Rules for Capitalization TipSheet</a></li>
                                 
                                 <li><a href="../../documents/FivePunctuationPatternsTipSheet.pdf" title="Five Punctuation Patterns TipSheet">Five Punctuation Patterns TipSheet</a></li>
                                 
                                 <li><a href="documents/Compare-Contrast-Transitions.pdf" target="_blank">Compare Contrast Transitions</a></li>
                                 
                              </ul>
                              
                              <h4> Videos</h4>
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=fE0IBPtbY2o" target="_blank" title="Apostrophes">Apostrophes</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=d2sAGY6viDA" target="_blank" title="Interjections">Interjections</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=txK_5awcCnE" target="_blank" title="Modifiers">Modifiers</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=bI2YHX_KOFo" target="_blank" title="Nouns">Nouns</a></li>
                                 
                                 <li>
                                    <a href="https://www.youtube.com/watch?v=ttfS79ej_v0" target="_blank" title="Verbs">Verbs</a>                
                                 </li>
                                 
                              </ul>
                              
                              <h4>&nbsp;</h4>                
                              
                           </div>
                           
                           <div>
                              
                              <h3>Grammar &amp; Mechanics</h3>
                              
                              <h3>Grammar &amp; Mechanics Links</h3>
                              
                              <h4>Links</h4>
                              
                              <ul>
                                 
                                 <li><a href="http://www.longman.com/ae/fog_level3/index.html" target="_blank" title="Focus on Grammar Level 3, 3rd edition">Focus on Grammar Level 3, 3rd edition</a></li>
                                 
                                 <li><a href="http://chompchomp.com/" target="_blank" title="Grammar Bytes">Grammar Bytes</a></li>
                                 
                                 <li><a href="http://grammar.quickanddirtytips.com/" target="_blank" title="Grammar Girl">Grammar Girl</a></li>
                                 
                                 <li><a href="http://www.grammar-quizzes.com/" target="_blank" title="Grammar-Quizzes">Grammar-Quizzes</a></li>
                                 
                                 <li><a href="http://grammar.ccc.commnet.edu/grammar/index.htm" target="_blank" title="Guide to Grammar and Writing">Guide to Grammar and Writing</a></li>
                                 
                                 <li><a href="http://www.varsitytutors.com/practice-tests" target="_blank" title="Practice Tests">Practice Tests</a></li>
                                 
                              </ul>                  
                              
                              <h3>Grammar &amp; Mechanics Handouts</h3>     
                              
                              <ul>
                                 
                                 <li><a href="documents/Basic-Rules-for-Making-Sentences.pdf" target="_blank" title="Basic Rules for Sentence Structure">Basic Rules for Sentence</a></li>
                                 
                                 <li><a href="documents/Verb-Tense-Chart.pdf" target="_blank">Verb Tense</a></li>
                                 
                                 <li><a href="documents/Coordinating-Conjunctions-FANBOYS.pdf" target="_blank">Coordinating Conjunctions (FANBOYS)</a></li>
                                 
                                 <li><a href="documents/NOUNS-VERBS.pdf.html" target="_blank">Nouns and Verbs</a></li>                   
                                 
                                 <li><a href="documents/Pluras-Versus-Possessive.pdf" target="_blank">Plural vs. Possessive Word Form</a></li>
                                 
                                 <li><a href="documents/Prepositions.pdf" target="_blank">Prepositions</a></li>
                                 
                                 <li><a href="documents/Comma-Colon-Semicolon.pdf" target="_blank">Commas, Colons, and Semicolons</a></li>
                                 
                                 <li><a href="documents/Word-Usage-and-Spelling.pdf" target="_blank">Word Usage and Spelling</a></li>
                                 
                                 <li><a href="documents/Compare-Contrast-Transitions.pdf" target="_blank">Compare Contrast Transitions</a></li>
                                 
                              </ul>                   
                              
                              <h3>Grammar &amp; Mechanics Videos</h3>     
                              
                              <ul>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=fE0IBPtbY2o" target="_blank" title="Apostrophes">Apostrophes</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=d2sAGY6viDA" target="_blank" title="Interjections">Interjections</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=txK_5awcCnE" target="_blank" title="Modifiers">Modifiers</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=bI2YHX_KOFo" target="_blank" title="Nouns">Nouns</a></li>
                                 
                                 <li><a href="https://www.youtube.com/watch?v=ttfS79ej_v0" target="_blank" title="Verbs">Verbs</a></li>
                                 
                              </ul>     
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Location, Contact, &amp; Hours</h3> 
                        
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> West Campus </div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 155</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-5454</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8am-8pm<br>Friday: 8am-5pm<br>Saturday: 9am-2pm
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <p>
                           <a href="http://chompchomp.com/" target="_blank"><button><strong>Need a grammar refresher?<br>Try the MOOC on Grammar Bytes!</strong></button></a></p>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/communications/writing-center/resources/student-resources.pcf">©</a>
      </div>
   </body>
</html>