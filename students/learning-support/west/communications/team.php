<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Communications Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/communications/team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Communications Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/communications/">Communications</a></li>
               <li>West Campus Communications Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Meet the Team</h2>
                        
                        
                        
                        <h4>Instructional Lab Supervisor</h4>
                        
                        <ul>
                           
                           <li>Amy Downs (Communications Center) </li>
                           
                        </ul>
                        
                        <h4>Instructional Staff</h4>
                        
                        <ul>
                           
                           <li>Bolivar, Erica</li>
                           
                           <li> Hart, Ellen</li>
                           
                           <li> Jean Louis, Junior</li>
                           
                           <li> Marconi, Nicole</li>
                           
                           <li> Rivera-Guillermety, Karen</li>
                           
                           <li> Ronderos, Oscar</li>
                           
                           <li> Sotis, Mia</li>
                           
                           <li> Woldagabriel, Awet</li>
                           
                        </ul>    
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <link href="../../includes/jquery/css/colorbox_valencia.css" rel="stylesheet" type="text/css">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <h3>Location, Contact, &amp; Hours</h3>      
                           
                           
                           <div> 
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 
                                 <div data-old-tag="tbody">
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 5, Rm 155</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-5454</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 8am-8pm<br>Friday: 8am-5pm<br>Saturday: 9am-2pm
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <div>
                                 <a href="http://valenciacc.ut1.qualtrics.com/SE/?SID=SV_b3me77RlLq3WeAB"><button><strong>Survey</strong></button></a>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/communications/team.pcf">©</a>
      </div>
   </body>
</html>