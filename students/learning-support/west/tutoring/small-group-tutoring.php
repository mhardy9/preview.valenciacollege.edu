<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Tutoring Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/tutoring/small-group-tutoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Tutoring Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/tutoring/">Tutoring</a></li>
               <li>West Campus Tutoring Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Tutoring Schedules</h2>
                        
                        <h2><a href="documents/Learning-Support-West-Campus-Tutor-Schedules.pdf.html" target="_blank">Fall 2017 Tutoring Schedule</a></h2>
                        
                        <h3>Tutoring Information</h3>
                        
                        <p>Math Tutoring </p>
                        
                        <ul>
                           
                           <li>Math tutoring is available through walk-in basis every day the Tutoring Center is
                              open, no appointments needed
                           </li>
                           
                           <li>One-on-one appointments are no longer available due to high student volume</li>
                           
                           <li>All math tutors are qualified to assist with College Algebra, Trigonometry,  Pre-Calculus,
                              and College/Liberal Arts Math. 
                           </li>
                           
                        </ul>
                        
                        <p>Small Group Subject Tutoring </p>
                        
                        <ul>
                           
                           <li>Tutoring for non-math subjects is in a small group format with 1 tutor working with
                              a maximum of 4 students at a time
                           </li>
                           
                           <li>All currently enrolled Valencia students can make appointments to work with a tutor
                              on their available days
                           </li>
                           
                           <li>You can sign up for an appointment up to 2 weeks in advance, book early, reserve your
                              spot.
                           </li>
                           
                           <li> No-show appointments may be given to other students after 15 minutes </li>
                           
                           <li>Appointment can be made online through your Atlas account by selecting the Courses
                              tab, West Campus Tutoring Center. Click the drop down arrow and select Tutoring Center
                              Summer 2017. Click on any white box (which indicates tutor session availability.)
                           </li>
                           
                           <li>No appointments are needed on Walk in Wednesdays</li>
                           
                        </ul>
                        
                        <h3>Tips and tricks to make the most of your visit</h3>
                        
                        <ul>
                           
                           <li>Make sure you come prepared--bring your notes, textbooks, homework, Power Point slides,
                              reviews, etc. so the tutors can see exactly where you are and help you get where you
                              need to go
                           </li>
                           
                           <li>Math tutoring tends be less busy during the morning (we open at 8 am) and on Fridays
                              and Saturdays, so visit us during those times to get even more time with our math
                              tutors
                           </li>
                           
                           <li>Wednesday is walk-in for the small group subject tutoring, take advantage of these
                              hours plus the appointment hours
                           </li>
                           
                           <li>Forgot to print something out? Need to work on online homework? We have you covered!
                              There are 12 computers in the Tutoring Center that have internet access, print, and
                              Microsoft Office
                           </li>
                           
                           <li>When there is no scheduled tutoring happening in the smaller rooms you can check them
                              out for quiet or group study for 2 hour blocks*
                           </li>
                           
                           <li>You can check out math textbooks, solution manuals, and calculators at the Information
                              Desk if you forget yours.
                           </li>
                           
                        </ul>                  
                        
                        <p>*With a valid Valencia ID</p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 7, Rm 240</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1633</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <div><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" target="_blank">TUTORING SATISFACTION SURVEY</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/tutoring/small-group-tutoring.pcf">©</a>
      </div>
   </body>
</html>