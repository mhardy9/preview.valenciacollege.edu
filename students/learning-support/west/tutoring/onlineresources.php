<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Tutoring Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/tutoring/onlineresources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Tutoring Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/tutoring/">Tutoring</a></li>
               <li>West Campus Tutoring Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Online Resources </h2>
                        
                        <p><a href="../communications/links.html">Click here</a> for links on Reading, Writing, and ESL
                        </p>
                        
                        <h3>Study Resources</h3>
                        
                        <p>Want to brush up on your note taking, time management, goal setting, and test taking
                           skills? Check out UCF's <a href="http://sarconline.sdes.ucf.edu/?page_id=344">SARC website for Learning Resources</a>.
                        </p>
                        
                        <h3>Instructional Videos</h3>
                        
                        <p><a href="../../math/liveScribe.html">Math Help 24/7</a>: Valencia professors work through problems and concepts from Developmental Math through
                           Differential Equations. 
                        </p>
                        
                        <p><a href="https://www.khanacademy.org/">Khan Academy</a>: Instructional videos for math, science, economics, and humanities. 
                        </p>
                        
                        <p><a href="http://ed.ted.com/lessons">TED Ed</a>: Explanatory videos from You Tube and TED in a variety of subjects, like science,
                           math, technology, health, psychology, the arts, and much more.
                        </p>
                        
                        <p><a href="http://youtube.com/user/crashcourse">Crash Course</a>: You Tube channel that covers US history, world history, chemistry, biology, and
                           more. 
                        </p>
                        
                        <p><a href="http://youtube.com/user/patrickJMT">Patrick JMT</a>: You Tube channel that covers mostly math concepts, like derivatives and integrals
                           in calculus, graphing functions and the unit circle in trigonometry, etc. 
                        </p>
                        
                        <h3>Reference</h3>
                        
                        <p><a href="http://www.studygeek.org/">Study Geek</a>: A math reference website with definitions, mini lessons, and online calculators;
                           applicable to College Algebra, Trigonometry, Statistics, and Calculus.
                        </p>
                        
                        <p><a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">Organic Compound Database</a>: A spectral database (NMR, IR, MS, etc.) for organic compounds, useful for lab reports
                           and compound identification. 
                        </p>
                        
                        <h3>Simulations For Math &amp; Science</h3>
                        
                        <p><a href="http://mw.concord.org/modeler/">The Concord Consortium</a>: Simulations in physics, chemistry, and biology that help visualize concepts including
                           chemical equilibrium, mechanics, diffusion, and much more. Most simulations have mini
                           lessons included for further explanation. 
                        </p>
                        
                        <p><a href="http://phet.colorado.edu/en/simulations/category/new">PhET Interactive Simulations</a>: Another great resource for simulations. These simulations focus more on user manipulation
                           than explanation.
                        </p>
                        
                        <h3>Language Websites</h3>
                        
                        <p><a href="http://www.busuu.com/enc">busuu</a>: A language website with online lessons and practice with native speakers. Free registration
                           required for this site.
                        </p>
                        
                        <p><a href="http://www.signingsavvy.com/">Signing Savvy</a>: Website specifically for American Sign Language. Resources include ASL dictionary,
                           videos, guides, and more. 
                        </p>
                        
                        <h3>Practice Problems</h3>
                        
                        <p><a href="http://www.businessbookmall.com/accounting%20internet%20library2.htm">Accounting</a>: Videos, Power Points, and problem sets for cost, business accounting, managerial
                           accounting, finance, and more. 
                        </p>
                        
                        <p><a href="http://www.textbooksfree.org/Economics%20Notes.htm">Economics</a>: Notes, videos, and practice problems for both macro and microeconomics.
                        </p>
                        
                        <p><a href="http://www.solvephysics.com/problems.shtml">Physics</a>: Problems for kinematics, dynamics, waves, electricity, magnetism, and more.
                        </p>
                        
                        <p><a href="http://science.widener.edu/svb/tutorial/">General Chemistry</a>: Problems for conversions, scientific notation, empirical formula, gas laws, rate
                           laws, and more.
                        </p>
                        
                        <p><a href="http://www2.chemistry.msu.edu/faculty/reusch/VirtTxtJml/Questions/problems.htm">Organic Chemistry</a>: Problems for nomenclature, reactions, functional groups, stereochemistry, mechanisms,
                           and more.
                        </p>
                        
                        <p><a href="http://www.biologycorner.com/">Biology</a>: Worksheets and quizzes for general topics in Biology I &amp; II.
                        </p>
                        
                        <p><a href="http://www.wiley.com/college/apcentral/anatomydrill/">Anatomy &amp; Physiology</a>: Interactive quizzes for topics covered in A&amp;P I &amp; II.
                        </p>
                        
                        <p><a href="http://highered.mcgraw-hill.com/sites/0072320419/student_view0/">Microbiology</a>: Companion website for a different Microbiology text book that offers free study
                           guides, practice, and quizzes.
                        </p>
                        
                        <p><a href="http://quizlet.com/">Quizlet</a>: Flash card generator for all subjects. You can browse flashcards made by other students
                           or create your own. 
                        </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 7, Rm 240</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1633</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <div><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" target="_blank">TUTORING SATISFACTION SURVEY</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/tutoring/onlineresources.pcf">©</a>
      </div>
   </body>
</html>