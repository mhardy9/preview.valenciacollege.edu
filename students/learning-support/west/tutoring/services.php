<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Tutoring Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/tutoring/services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Tutoring Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/tutoring/">Tutoring</a></li>
               <li>West Campus Tutoring Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">&nbsp;</div>
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h3>Math Tutoring</h3>
                        
                        <p>Math tutoring is available on a walk-in basis during operation hours.&nbsp;Subject tutoring
                           is available for other subjects on select days and times. All math tutors can assist
                           with:
                        </p>
                        
                        <ul>
                           
                           <li>college algebra</li>
                           
                           <li>liberal arts math</li>
                           
                           <li>trignometry</li>
                           
                           <li>pre-calculus</li>
                           
                        </ul>
                        
                        <p>Select tutors can assist with upper level math subjects. Please check the chart below
                           to determine which tutors can help with each subject.
                        </p>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td style="text-align: left;">Tutors</td>
                                 
                                 <td style="text-align: center;">Statistics</td>
                                 
                                 <td style="text-align: center;">Business Calculus</td>
                                 
                                 <td style="text-align: center;">Calculus One</td>
                                 
                                 <td style="text-align: center;">Calculus Two</td>
                                 
                                 <td style="text-align: center;">Calculus Three</td>
                                 
                                 <td style="text-align: center;">Differential Equations</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Erik</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Hector</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Leila</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Maite</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Miguel</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Mithil</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Monica</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Rob</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Scott</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Sean</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Thanapat</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                                 <td style="text-align: center;">X</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        <br>
                        
                        <h3>Math Tutor Schedule</h3>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>Tutor</td>
                                 
                                 <td style="text-align: center;">Monday</td>
                                 
                                 <td style="text-align: center;">Tuesday</td>
                                 
                                 <td style="text-align: center;">Wednesday</td>
                                 
                                 <td style="text-align: center;">Thursday</td>
                                 
                                 <td style="text-align: center;">Friday</td>
                                 
                                 <td style="text-align: center;">Saturday</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Erik</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">9 am-4 pm</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8:30 am-11:30 am;</p>
                                    
                                    <p>12 pm-3 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8 am-12 pm;</p>
                                    
                                    <p>12:30 pm-4 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Hector</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">1:30 pm-6:30 pm</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;"><span>1:30 pm-6:30 pm</span></td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">10 am-3 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Leila</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8 am-9:30 am;</p>
                                    
                                    <p>2:30 pm-6:30 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8:30 am-1 pm;</p>
                                    
                                    <p>5 pm-8 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8 am-9:30 am;</p>
                                    
                                    <p>2:30 pm-6:30 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8:30 am-1 pm;</p>
                                    
                                    <p>5 pm-8 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Maite</td>
                                 
                                 <td style="text-align: center;">2 pm-8 pm</td>
                                 
                                 <td style="text-align: center;"><span>2 pm-8 pm</span></td>
                                 
                                 <td style="text-align: center;"><span>2 pm-8 pm</span></td>
                                 
                                 <td style="text-align: center;"><span>2 pm-8 pm</span></td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Miguel</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>9 am-1:30 pm;</p>
                                    
                                    <p>2 pm-7 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Mithil</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">3 pm-8 pm</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">1 pm-7 pm</td>
                                 
                                 <td style="text-align: center;">10 am-3 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Monica</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;"><span>3 pm-8 pm</span></td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;"><span>3 pm-7 pm</span></td>
                                 
                                 <td style="text-align: center;">10 am-3 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Rob</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>10 am-2 pm;</p>
                                    
                                    <p>3 pm-8 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>10 am-2 pm;</p>
                                    
                                    <p>3 pm-8 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>10 am-2 pm;</p>
                                    
                                    <p>3 pm-8 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Scott</td>
                                 
                                 <td style="text-align: center;">4 pm-8 pm</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>10 am-2 pm;</p>
                                    
                                    <p>2:30 pm-7 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>10 am-2 pm;</p>
                                    
                                    <p>2:30 pm-7 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Sean</td>
                                 
                                 <td style="text-align: center;">9 am-12 pm</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;"><span>9 am-12 pm</span></td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">11 am-4:30 pm</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Thanapat</td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8 am-12 pm;</p>
                                    
                                    <p>12:30 pm-5 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">
                                    
                                    <p>8 am-12 pm;</p>
                                    
                                    <p>12:30 pm-5 pm</p>
                                    
                                 </td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                                 <td style="text-align: center;">&nbsp;</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 7, Rm 240</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1633</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" target="_blank">TUTORING SATISFACTION SURVEY</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/tutoring/services.pcf">©</a>
      </div>
   </body>
</html>