<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Learning Support | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/tutoring/become-a-tutor.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Learning Support</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/tutoring/">Tutoring</a></li>
               <li>Learning Support</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Become a Tutor</h2>
                        
                        
                        <p>Are you interested in becoming a tutor? Learning Support is always looking for great
                           tutors who share any interest in learning and helping others. 
                        </p>
                        
                        <p>A qualified tutor must have completed the course with a minimum grade of a "B" and
                           show an interest in developing a learning centered tutoring approach. For more details
                           by campus, see below.
                        </p>
                        
                        
                        <h3>Subjects Needed</h3>
                        
                        <ul>
                           
                           <li>Economics</li>
                           
                           <li>Calculus</li>
                           
                           <li>Physics</li>
                           
                           <li>College Algebra</li>
                           
                        </ul>
                        
                        
                        <h3>How to Apply</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>East</strong></div>
                                 
                                 <div data-old-tag="td">Follow these <a href="../../east/academicsuccess/employment.html">instructions</a>.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Lake Nona</strong></div>
                                 
                                 <div data-old-tag="td">Follow these <a href="../../lakenona/HowtoBecomeaTutor.html">instructions</a>.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>West</strong></div>
                                 
                                 <div data-old-tag="td">Email <a href="mailto:tgallagher@valenciacollege.edu">Teressa Gallagher</a> or apply in person at Bldg 7, Rm 240.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Osceola</strong></div>
                                 
                                 <div data-old-tag="td"><a href="http://form.jotform.us/form/40093847150149" target="_blank">Please complete the following form</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                        
                        
                        <a href="../../math/liveScribe.html" title="Math Help 24/7"><img alt="Math Help 24/7" border="0" height="60" src="math-24-7_245x60.png" width="245"></a>
                        
                        
                        <a href="../../student-services/skillshops.html"><img border="0" src="SkillShops_270x60.png" width="100%"></a>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/tutoring/become-a-tutor.pcf">©</a>
      </div>
   </body>
</html>