<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Tutoring Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/tutoring/meet-the-team.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Tutoring Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li><a href="/students/learning-support/west/tutoring/">Tutoring</a></li>
               <li>West Campus Tutoring Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Meet the Team</h2>
                        
                        <h3>Front Desk Staff</h3>
                        
                        <ul>
                           
                           <li>Caroline McCoy</li>
                           
                           <li>Carolyn Keglar </li>
                           
                           <li>CeCe Hill </li>
                           
                           <li>Liz Aquino</li>
                           
                           <li>Luis Nolasco </li>
                           
                        </ul>            
                        
                        <h3>Math Tutors</h3>
                        
                        <ul>
                           
                           <li>Andrew Farquaharson </li>
                           
                           <li>Erik Carillo</li>
                           
                           <li>Hector Medina-Reyes </li>
                           
                           <li>Ivan Ordonez</li>
                           
                           <li>Jennifer Nelson</li>
                           
                           <li>Luigi Garcia </li>
                           
                           <li>Maite Frometa</li>
                           
                           <li>Miguel Ramirez</li>
                           
                           <li>Mithil Patel </li>
                           
                           <li>Monica Garcia</li>
                           
                           <li>Nimit Patel</li>
                           
                           <li>Niraj Patel </li>
                           
                           <li>Rob Walls</li>
                           
                           <li>Scott Adams</li>
                           
                           <li>Stephen Cox</li>
                           
                           <li>Thanapat Phoolsuk</li>
                           
                        </ul>            
                        
                        <h3>Subject Tutors</h3>
                        
                        <ul>
                           
                           <li>Abby Marchetti (Macro- and Microeconomics)</li>
                           
                           <li>Ali Khalil (Biology, Anatomy &amp; Physiology)</li>
                           
                           <li>Andrew Farquaharson (Chemistry 1 &amp; 2, Organic Chemistry 1) </li>
                           
                           <li>Carlos Hernandez Pavon (Biology, Microbiology) </li>
                           
                           <li>Fortuné Tabouna (Network Engineering) </li>
                           
                           <li>Hani Alayoubi (Organic Chemistry)</li>
                           
                           <li>Heather  Heberly (Anatomy &amp; Physiology, TEAS) </li>
                           
                           <li>Jared Hansraj (Financial and Managerial Accounting) </li>
                           
                           <li>Jase Smalley (US Government)</li>
                           
                           <li>Joao Pedro dos Santos (Portuguese)</li>
                           
                           <li> Lester Bulosan (Physics with Medical Applications &amp; Psychology)</li>
                           
                           <li>Michelle Cohen (EAP)</li>
                           
                           <li>Vladimir Perez (Spanish)</li>
                           
                        </ul>            
                        
                        <p><strong>Tutorial Center Support Specialists: </strong></p>
                        
                        <p><a href="mailto:tgallagher@valenciacollege.edu">Teresa Gallagher</a> (Tutoring Coordinator) and <a href="mailto:adagiau@valenciacollege.edu">Amanda Forth </a> (Smarthinking and Prescriptive Learning) 
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 7, Rm 240</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1633</div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        <div><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" target="_blank">TUTORING SATISFACTION SURVEY</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/tutoring/meet-the-team.pcf">©</a>
      </div>
   </body>
</html>