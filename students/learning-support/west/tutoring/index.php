<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Tutoring Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/west/tutoring/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Tutoring Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/west/">West Campus</a></li>
               <li>Tutoring</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <p>The West Campus Tutoring Center offers free academic assistance for Valencia and UCF
                           students with a valid&nbsp; Valencia ID number.
                        </p>
                        
                        <p>Math tutoring is available on a walk-in basis during operating hours while all other
                           subjects are by appointment and conducted in small groups. Check out Smarthinking
                           online tutoring through the Courses tab in Atlas for times when the Tutoring Center
                           is full or closed.
                        </p>
                        
                        <p>Materials available for check out with your valid Valencia ID card:&nbsp;</p>
                        
                        <ul>
                           
                           <li>math textbooks (2 hours)</li>
                           
                           <li>TI-84+ graphing calculators (5 hours)</li>
                           
                           <li>TI-89 graphing calculators (5 hours)</li>
                           
                           <li>dry erase markers (2 hours)</li>
                           
                           <li>group study rooms (2 hours)</li>
                           
                        </ul>
                        
                        <p>Any material returned past the designated due time is subject to a $1.20 per hour
                           late fee.
                        </p>
                        
                        <p>Announcements:</p>
                        
                        <ul>
                           
                           <li>The Tutoring Center and all of Valencia will be closed Friday, February 9, 2017 for
                              Learning Day. Smarthinking online tutoring is still available and can be accessed
                              in the Courses tab of Atlas.
                           </li>
                           
                        </ul>
                        
                        <p>House rules:</p>
                        
                        <ul>
                           
                           <li>Treat the Tutoring Center like a classroom and be respectful</li>
                           
                           <li>No mobile phone conversations in the Tutoring Center, please take calls in the atrium</li>
                           
                           <li>Food and smokeless tobacco are not permitted in the Tutoring Center</li>
                           
                           <li>Students are required to show their Valencia ID in order to check out materials from
                              the Information Desk
                           </li>
                           
                           <li>No children are permitted per Valencia policy 6Hx28:04-10</li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 7, Rm 240</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1633</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday - Thursday: 8 am to 8 pm<br>Friday: 8 am to 7 pm<br>Saturday: 10 am to 3 pm<br>Sunday: Closed
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div><a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" target="_blank">TUTORING SATISFACTION SURVEY</a></div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/west/tutoring/index.pcf">©</a>
      </div>
   </body>
</html>