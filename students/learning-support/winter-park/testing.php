<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing Center  | Valencia College</title>
      <meta name="Description" content="Testing Center | Winter Park Campus | Valencia College">
      <meta name="Keywords" content="college, school, educational, winter, park, testing, center">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/testing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Winter Park Campus</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li>Testing Center </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Testing/Assessment Center</h2>
                        
                        <h3 class="v-red"><span style="text-decoration: underline;">Important Information</span></h3>
                        
                        <p dir="auto">2/9/18 - College Closed (Learning day)</p>
                        
                        <p dir="auto">3/12/18 - 3/16/18 College Closed (Spring Break)</p>
                        
                        <p dir="auto">5/28/18&nbsp;College Closed (Memorial Day)</p>
                        
                        <hr class="styled_2">
                        
                        <h3>Hours and Location for the Winter Park Testing/Assessment Center</h3>
                        
                        <p><strong>Location</strong>: 1-104
                        </p>
                        
                        <h3><strong>Testing Center Hours</strong></h3>
                        
                        <p>(Students currently enrolled in Valencia classes)</p>
                        
                        <ul>
                           
                           <li>A <strong>Valencia/UCF student ID</strong> or a <u><strong>current</strong></u> <strong>government issued photo ID </strong>is <strong>required</strong> to take a test in the Testing Center.
                           </li>
                           
                           <li>Please know your professor's name and the name of your course.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                        <h4>Spring&nbsp;2018</h4>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div class="box_style_3" style="padding-left: 210px;">&nbsp; ***Arrive between the hours below***&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Closing
                                    Time
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Monday - Thursday&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 8:00am - 6:00pm&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;7:00pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Friday&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 8:00am - 4:00pm&nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 5:00pm
                                 </div>
                                 
                                 <div>&nbsp;</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Tests will not be given within <strong>1 hour</strong> of closing time.
                        </p>
                        
                        <p><strong>PLEASE NOTE</strong>: Testing Center Hours vary per Campus.
                        </p>
                        
                        <p>Please check the hours on the college <a href="/locations/students/testing/index.html">Testing Center web page</a>.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <h3><strong>Assessment&nbsp;Center Hours</strong></h3>
                        
                        <p>(Placement tests for prospective students)</p>
                        
                        <p>A<strong> <u>current</u> government issued photo ID </strong>or<strong> Valencia ID </strong> is <strong>required</strong></p>
                        
                        <hr class="styled_2">
                        
                        <h3>PERT/CPT/LOEP</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div class="box_style_3" style="padding-left: 180px;">&nbsp; &nbsp; &nbsp; &nbsp;***Arrive between the hours below***&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; Closing Time
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Monday - Thursday&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;8:00am - 5:00pm&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;7:00pm
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Friday&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;8:00am - 4:00pm&nbsp;
                                    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;5:00pm
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <h3>CJBAT/TEAS</h3>
                        
                        <ul>
                           
                           <li>By appointment only</li>
                           
                           <li>Please call for availability</li>
                           
                        </ul>
                        
                        <p>For further information about Assessment Testing, please visit <a href="http://valenciacollege.edu/assessments/">Assessment Services</a>.
                        </p>
                        
                        <p><span style="text-decoration: underline;">Faculty Information</span></p>
                        
                        <ul>
                           
                           <li><strong>Exam Referral</strong>:&nbsp;To submit exams to the Testing Center, log into your ATLAS account. Choose the Faculty
                              Services tab. Select the Testing Center Referral link located in the left column under
                              Faculty Tools. Complete the Referral Form, attach your exam, class roster(s) and submit.&nbsp;<strong>PLEASE NOTE: class rosters must be submitted with referrals.</strong></li>
                           
                           <li>We may be unable to answer the phone during peak testing periods, for&nbsp;a more immediate&nbsp;response,
                              please contact the Testing Center staff via <a href="mailto:WPC-TestingCenter@valenciacollege.edu?subject=Contact%20Via%20Web">email</a>.
                           </li>
                           
                           <li>Referral Forms may be picked up and filled out in the Testing Center and submitted
                              in person.
                           </li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        <h3>Contact information</h3>
                        
                        <ul>
                           
                           <li><a href="mailto:mthorburn@valenciacollege.edu">Michelle Thorburn</a> - Assessment Coordinator (x6527)
                           </li>
                           
                           <li><a href="mailto:msaemmer@valenciacollege.edu">Michael Saemmer</a> - Assessment Specialist (x6086)
                           </li>
                           
                           <li>Jackie Heffelfinger - Assessment Specialist (x6086)</li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/testing.pcf">©</a>
      </div>
   </body>
</html>