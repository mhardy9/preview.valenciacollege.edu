<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/math/tipsheetdevelopmentalmathi.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/math/">Math Support Center</a></li>
               <li>Math Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Developmental Math I Tip Sheets and Work Sheets</h2>
                        
                        <ul>
                           
                           <li><a href="documents/Intergers.pdf">Integers</a></li>
                           
                           <li><a href="documents/OrderofOperations.pdf">Order of Operations</a></li>
                           
                           <li><a href="documents/EvaluatinganExpression.pdf">Evaluating an Expression</a></li>
                           
                           <li><a href="documents/SolvingEquation.pdf">Solving an Equation</a></li>
                           
                           <li><a href="documents/findingGCFandLCM.pdf">Finding the GCF and LCM</a></li>
                           
                           <li><a href="documents/ConqueringFractions.pdf">Conquering Fractions</a></li>
                           
                           <li><a href="documents/FractionWorksheet1.pdf">Fraction Worksheet 1</a></li>
                           
                           <li><a href="documents/Decimals.pdf">Decimals</a></li>
                           
                           <li><a href="documents/Percents.pdf">Percents</a></li>
                           
                           <li><a href="documents/SimpleInterest.pdf">Simple Interest</a></li>
                           
                           <li><a href="documents/Wordproblemtranslation.pdf">Word Problem Translation </a></li>
                           
                           <li><a href="documents/MultiplicationTable.pdf">Multiplication Table</a></li>
                           
                           <li><a href="documents/MeasurementsConversions.pdf">Measurements (Conversions)</a></li>
                           
                           <li><a href="documents/WeightsandMeasures.pdf">Weights and Measures</a></li>
                           
                           <li><a href="documents/TestFordivisibility.pdf">Test for Divisibility</a></li>
                           
                           <li><a href="documents/GeometryReviewandFormulas.pdf">Geometry Review and Formulas</a></li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/math/tipsheetdevelopmentalmathi.pcf">©</a>
      </div>
   </body>
</html>