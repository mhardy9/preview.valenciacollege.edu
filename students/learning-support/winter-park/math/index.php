<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/math/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li>Math Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div> 
                                    <h2> <img alt="Math Support Center" height="75" src="MathSupportCenter.gif" width="530">
                                       
                                    </h2>
                                    
                                    <h2>&nbsp; </h2>
                                    
                                    <h2>
                                       <br>
                                       Our goal at the Math Support Center is to help you become independent learners. To
                                       that end, the Math Support Center is dedicated to providing an environment that promotes
                                       good study habits by helping the students successfully learn to study. 
                                    </h2>
                                    
                                    <p><img alt="Students having fun with Math!" height="292" src="20130409_125449.jpg" width="390"></p>
                                    
                                    <h3>The Math Support Center has   many resources available to students. We have: </h3>
                                    
                                    <ul>
                                       
                                       <li>
                                          
                                          <div>A team of instructional support staff to assist you in your math studies.</div>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <div>Trained and qualified tutors to help you succeed.</div>
                                          
                                       </li>
                                       
                                       <li>Course textbooks and accompanying solution manuals (For use only in the Math Support
                                          Center). 
                                       </li>
                                       
                                       <li>
                                          
                                          <div>Computers and software programs for online course work (corresponding to textbook,
                                             plus other interesting programs).
                                          </div>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <ul>
                                       
                                       <li>DVD  lectures for extra help (Limited courses).</li>
                                       
                                       <li>Tip sheets and worksheets for extra practice.</li>
                                       
                                    </ul>
                                    
                                    <h3>Our services includes: </h3>
                                    
                                    <ul>
                                       
                                       <li>
                                          
                                          <div>Individual help from support personnel (limited to question-and-answer and not a prolonged
                                             tutoring session)
                                          </div>
                                          
                                       </li>
                                       
                                       <li>PERT review</li>
                                       
                                    </ul>
                                    
                                    
                                    <p>Let us help you improve your grades and study skills by providing peer tutoring, areas
                                       for study groups, computer access resource, materials and much more!             
                                       
                                    </p>
                                    
                                    <h3>Stop in and see what's happening!</h3>
                                    
                                    <h3>&nbsp;                        
                                       
                                    </h3>
                                    
                                    <p><a href="index.html#top">TOP</a></p>
                                    
                                    <p><a href="math_rss.xml.html">.</a>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/math/index.pcf">©</a>
      </div>
   </body>
</html>