<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/math/classtypes.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/math/">Math Support Center</a></li>
               <li>Math Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2> <img alt="Math Support Center" height="75" src="MathSupportCenter.gif" width="530">
                           
                        </h2>
                        
                        <p><span>Standard Classroom</span><br>
                           2 variations - Full term and Flex start<br>
                           Much of the information comes from instructor<br>
                           Homework is done outside the classroom (many instructors use on-line homework)<br>
                           Tests are given at discretion of the instructor<br>
                           Supplemental Learning Leaders - an additional resource for some classes
                        </p>
                        
                        <p><span>Intensive Class (also called a Combo class)</span><br>
                           Basically this is 2 classes being done in one semester<br>
                           Much of the information comes from instructor<br>
                           Homework is done outside the classroom (many instructors use on-line homework)<br>
                           Tests are given at discretion of the instructor
                        </p>
                        
                        <p><span>On-Line Class</span><br>
                           All course content is delivered online<br>
                           There may be a suggested orientation with instructor at the beginning of the term<br>
                           Personal support will be given to all students on-line
                        </p>
                        
                        <p><span>Hybrid Class</span><br>
                           Blends online and face-to-face delivery<br>
                           Any one of the blends will be a maximum of 75% of course<br>
                           Homework may be done either on-line or from the textbook
                        </p>
                        
                        <p><span>LinC classes</span><br>
                           These are two classes that are linked together to integrate class material across
                           the two disciplines<br>
                           Both classes are run as standard classrooms
                           
                        </p>
                        
                        <p><span><a href="ilc.html">Individualized Learning Class</a></span><br>
                           There are NO lectures<br>
                           Primary source of information is the textbook<br>
                           While working at your own pace other students may or may not be studying the same
                           material<br>
                           Homework will be done in and out of class as well as on-line.<br>
                           Chapter tests are taken when you and the instructor think you are ready with no time
                           limit<br>
                           Prep courses have competency tests that are taken until you pass with 90% or higher<br>
                           Failing test grades must be redone for passing grade (or max of 3 tries)<br>
                           If work is not completed by end of term, you can sign up for the class again (pay
                           another registration) to finish the course
                        </p>
                        
                        <p><span>College-Level Examination Program (CLEP) Test</span><br>
                           This is a test that you can take at a cost of approximately $70 to get credit for
                           any of the following math classes:<br>
                           College Mathematics (MGF1107)<br>
                           College Algebra (MAC1105)<br>
                           Trigonometry (MAC1114)<br>
                           College Algebra &amp; Trigonometry (MAC1147)<br>
                           Calculus with Elementary Functions (MAC2233)
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/math/classtypes.pcf">©</a>
      </div>
   </body>
</html>