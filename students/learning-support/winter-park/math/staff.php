<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/math/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/math/">Math Support Center</a></li>
               <li>Math Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Coordinator</h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><span>Lisa Keeton </span></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Photo Coming Soon! </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <p>Monday<br>
                                                         <br>
                                                         Tuesday<br>
                                                         <br>
                                                         Wednesday<br>
                                                         <br>
                                                         Thursday<br>
                                                         <br>
                                                         Friday 
                                                      </p>
                                                      
                                                   </div>
                                                   
                                                   <div>
                                                      
                                                      <p>9:00am - 7:00pm <br>
                                                         <br>
                                                         10:00am - 5:00pm <br>
                                                         <br>
                                                         9:00am - 7:00pm<br>
                                                         <br>
                                                         10:00am - 5:00pm<br>
                                                         <br>
                                                         8:00am - noon 
                                                      </p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <p>Contact:<br>
                                          <a href="mailto:lkeeton@valenciacollege.edu">lkeeton@valenciacollege.edu</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <br>
                           <br>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><span>Sharon Bailey </span></div>
                                    </div>
                                    
                                    <div><strong>All Levels of Mathematics</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><img alt="Sharon" height="247" src="bailey.jpg" width="180"></div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>Monday</p>
                                                   
                                                   <p>Tuesday</p>
                                                   
                                                   <p>Wednesday<br>
                                                      <br>
                                                      Thursday<br>
                                                      
                                                   </p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>8:00am - 3:30pm</p>
                                                   
                                                   <p> 8:00am - 2:00pm </p>
                                                   
                                                   <p>8:00am  - 2:00pm</p>
                                                   
                                                   <p>8:00am - 2:00pm </p>
                                                   
                                                   <p><br>
                                                      
                                                   </p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <br>
                           <br>
                           
                        </div>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><span>Marianne</span></div>
                                    </div>
                                    
                                    <div><strong>PreAlgebra - Trigonometry, Statistics, College Mathematics , Calculus I </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><img alt="Marianne" height="248" src="mari.jpg" width="180"></div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>Monday<br>
                                                      <br>
                                                      Tuesday
                                                      
                                                   </p>
                                                   
                                                   <p>Wednesday</p>
                                                   
                                                   <p>Friday</p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>9:30am - 5:30pm</p>
                                                   
                                                   <p>8:00am - 3:30pm </p>
                                                   
                                                   <p>10:30am  - 5:30pm</p>
                                                   
                                                   <p>                                  8:00am - noon</p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>                        
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <br>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Frank</div>
                                    </div>
                                    
                                    <div><strong>PreAlgebra - Trigonometry, PreCalculus, Statistics, <br>
                                          Calculus I, II</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><img alt="Frank" height="247" src="franco.jpg" width="180"></div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p><br>
                                                      <br>
                                                      Tuesday
                                                   </p>
                                                   
                                                   <p>Wednesday</p>
                                                   
                                                   <p>Thursday</p>
                                                   
                                                   <p>Friday</p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>8:00am - 7:00pm </p>
                                                   
                                                   <p>8:00am - 1:00pm</p>
                                                   
                                                   <p>8:00am - 7:00pm </p>
                                                   
                                                   <p>9:00 am - noon </p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><br>
                              
                           </p>
                           
                        </div>
                        
                        <div>  <br>
                           
                        </div>
                        
                        <div>
                           <br>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Josiah</div>
                                    </div>
                                    
                                    <div><strong>PreAlgebra - Trigonometry, Precalculus, Calculus I </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Photo Coming Soon! </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>Monday</p>
                                                   
                                                   <p>Tuesday<br>
                                                      <br>
                                                      Wednesday
                                                   </p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>9:00am - 7:00pm <br>
                                                      <br>
                                                      10:00am - 4:00pm<br>
                                                      <br>
                                                      9:00am  - 7:00pm                              
                                                   </p>
                                                   
                                                   <p><br>
                                                      
                                                   </p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <br>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Rachel</div>
                                    </div>
                                    
                                    <div><strong>PreAlgebra - College Algebra, Statistics</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Photo Coming Soon! </div>
                                    </div>
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>
                                                      Thursday
                                                   </p>
                                                   
                                                   <p>Friday</p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>8:00am  - 2:00pm<br>
                                                      <br>
                                                      8:00am - noon <br>
                                                      <br>
                                                      <br>
                                                      <br>
                                                      
                                                   </p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           <br>
                           <br>
                           <br>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Giorgio</div>
                                    </div>
                                    
                                    <div><strong>PreAlgebra - College Algebra, Trigonometry, Precalculus</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>Monday:</p>
                                                   
                                                   <p>Tuesday:</p>
                                                   
                                                   <p>Wednesday:</p>
                                                   
                                                   <p>Thursday:</p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>10:00am - 7:00pm <br>
                                                      <br>
                                                      1:00pm - 6:00pm 
                                                      
                                                   </p>
                                                   
                                                   <p>1:00pm - 7:00pm                             </p>
                                                   
                                                   <p>1:00pm - 6:00pm </p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div>Denise</div>
                                    </div>
                                    
                                    <div><strong>PreAlgebra - Trigonometry, Precalculus, Calculus I, II, III, Differential Equations,
                                          Matrix &amp; Linear Algebra</strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       
                                       <p><strong>Hours of Availability: </strong></p>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p>Tuesday</p>
                                                   
                                                   <p>Thursday</p>
                                                   
                                                   
                                                   <p><br>
                                                      
                                                   </p>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p>2:00pm - 7:00pm</p>
                                                   
                                                   <p>2:00pm - 7:00pm</p>
                                                   
                                                   <p><br>
                                                      <br>
                                                      
                                                   </p>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           
                           
                           <br>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/math/staff.pcf">©</a>
      </div>
   </body>
</html>