<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/math/helplinks.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/math/">Math Support Center</a></li>
               <li>Math Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>Helpful Math Links </h2>
                        
                        <p><a href="http://mathbits.com/MathBits/StudentResources/GraphPaper/14x14axes.pdf">Graph Paper </a>: Coordinate Grids for graphing.
                        </p>
                        
                        <p>                    <a href="http://www.algebrahelp.com"> Algebra Help</a> : Basic Algebra Lessons, Worksheets, caclcultors and Resources on various algebra
                           topics. <a href="http://www.algebrahelp.com"><br>
                              <br>
                              </a><a href="http://www.wtamu.edu/academic/anns/mps/math/mathlab/">West Texas A&amp;M Virtual Math Lab </a>: Help for Placement Testing, Developmental Math, Intermediate Algebra, College Algrebra
                           and the GRE. 
                        </p>
                        
                        <p><a href="http://math.com/">Math.com</a>: Good general website and great for Developmental Math I and II. 
                        </p>
                        
                        <p><a href="http://www.interactmath.com/">Interact Math </a>:  from Pearson Education. Allows you to work problems in the same format as MyMathLab.
                           
                        </p>
                        
                        <p><a href="http://www.khanacademy.org/">Khan Academy: </a> Videos on everything from Math to Physics, Chemistry , Economics and more. 
                        </p>
                        
                        <p>                    <a href="http://www.oswego.org/ocsd-web/games/BillyBug2/bug2.html">Coordinate Graphing Game</a>: A game where you can practicing graphing ordered pairs. Great for Developmental
                           Math II or as a refersher. <br>
                           <br>
                           <a href="http://www.kidsolr.com/math/fractions.html">Fractions</a>: Good practice on equivalent fractions and adding/subtraticing and mutliplying/dividing
                           fractions. <br>
                           <a href="http://www.netsrq.com/%7Ehahn/notes.html#notation"><br>
                              </a><a href="http://www-spof.gsfc.nasa.gov/stargaze/Smath.htm">Math Refresher - Dr. David Stein</a>: Algebra and Trigonometry basics and elements of logarithms. <br>
                           <br>
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/math/helplinks.pcf">©</a>
      </div>
   </body>
</html>