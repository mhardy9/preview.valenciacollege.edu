<ul>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/">Learning Support <i aria-hidden="true" class="far fa-angle-down"></i></a> 
		<ul>
			<li><a href="/students/learning-support/learning-centers.php">Learning Centers</a> </li>	
			<li><a href="/students/learning-support/writing-consultations.php">Writing Consultations</a></li>    
			<li><a href="/students/learning-support/skillshops.php">Skillshops</a> </li>
			<li><a href="http://net4.valenciacollege.edu/forms/students/learning-support/contact.cfm" target="_blank">Contact Us</a></li>
		</ul>
	</li>
	<li><a href="/students/learning-support/winter-park/">Winter Park Learning Centers</a> </li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/winter-park/communications">Communications <i aria-hidden="true" class="far fa-angle-down"></i></a><ul>
		<li><a href="/students/learning-support/winter-park/writing-workshops.php">Writing Workshops</a></li>
		<li><a href="/students/learning-support/winter-park/WritingConsultations.php">Writing Consultations</a></li>
		<li><a href="/students/learning-support/winter-park/Tutoring.php">One-on-One Tutoring</a></li>
		<li><a href="/students/learning-support/winter-park/resources.php">Writer Resources</a></li>
		<li><a href="/students/learning-support/winter-park/SkillBuildingWorkshops.php">PERT Information</a></li>
		<li><a href="/students/learning-support/winter-park/policies.php">Policies</a></li>
		<li><a href="/students/learning-support/winter-park/ContactUs.php">Contact Us</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/learning-support/winter-park/math/">Math <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/learning-support/winter-park/staff.php">About Us</a></li>
			<li><a href="/students/learning-support/winter-park/classtypes.php">Class Types</a></li>
			<li><a href="/students/learning-support/winter-park/mathhours.php">Center Hours</a></li>
			<li><a href="/students/learning-support/winter-park/mathpath.php">Math Path</a></li>
			<li><a href="/students/learning-support/winter-park/links.php">Resource Links</a></li>
		</ul></li>
	<li><a href="/students/learning-support/winter-park/testing.php">Testing</a></li>
</ul>