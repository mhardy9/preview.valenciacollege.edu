<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/cssc_tipsheets.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div>                       </div>
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Writing – Reading – Speech – Study Skills </h3>
                                    
                                    <p><img alt="CSSC Contact Us" height="292" src="CSSC_Banner_000.jpg" width="519"></p>
                                    
                                    
                                    <h1>CSSC Tip Sheets </h1>
                                    
                                    <h4>Note: More tip sheets are available at the CSSC (Room 136, Winter Park Campus). </h4>
                                    
                                    <h2>
                                       <strong>APA</strong><strong><br>
                                          </strong>
                                       
                                    </h2>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li>
                                             <a href="documents/2009APAFormat.pdf">2009 APA Format</a> <span><strong>New! </strong></span>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <div>
                                          
                                          <h2><strong>MLA<br>
                                                </strong></h2>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="documents/MLADocumentationChecklist.pdf">MLA Documentation Checklist</a> <span><strong>New! </strong></span>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/ResearchPaperChecklist.pdf">MLA Format Research Paper Checklist</a> <span><strong>New!</strong></span> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/BlockQuotingMLA.pdf">How to Block Quote</a><strong> New!</strong>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/20150928115205504.pdf">Incorporating Quotations</a> <span><strong>New!</strong></span> 
                                             </li>
                                             
                                             <li>
                                                <a href="documents/IncorporatingQuotationsII_MLA.pdf">Incorporating Quotations II </a><span><strong>New!</strong></span>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/IntextCitations_MLA.pdf">Works Cited: Print Sources</a> <strong>New! </strong>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/DocumentingOnline_MLA.pdf">Works Cited: Online Sources</a> <span><strong>New! </strong></span>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="documents/MLA_MixedMedia.pdf">Works Cited: Mixed Media </a><strong>New!</strong>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <h2><strong>Grammar<br>
                                             </strong></h2>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/abbreviationsandnumbers.pdf">Abbreviations and Numbers</a></li>
                                          
                                          <li><a href="documents/ColonrevisedTipSheet.pdf">Colon </a></li>
                                          
                                          <li><a href="documents/CommasTipSheet.pdf">Commas </a></li>
                                          
                                          <li><a href="documents/CoordinationTipSheet.pdf">Coordination </a></li>
                                          
                                          <li><a href="documents/FivePunctuationPatternsTipSheet.pdf">Five Punctuation Patterns </a></li>
                                          
                                          <li><a href="documents/Parallelism_2TipSheet.pdf">Parallelism </a></li>
                                          
                                          <li><a href="documents/PronounsTipSheet.pdf">Pronouns</a></li>
                                          
                                          <li><a href="documents/RulesforCapitalizationTipSheet.pdf">Rules for Capitalization </a></li>
                                          
                                          <li><a href="documents/run-onsandcommasplicesTipSheet.pdf">Run-ons and Comma Splices </a></li>
                                          
                                          <li><a href="documents/Sentence_FragmentsTipSheet.pdf">Sentence Fragments </a></li>
                                          
                                          <li><a href="documents/Stalking_the_Wild_PrepositionTipSheet.pdf">Stalking the Wild Preposition </a></li>
                                          
                                       </ul>
                                       
                                       <h2><strong>Writing</strong></h2>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <p><strong>Structure</strong></p>
                                                   
                                                   <ul>
                                                      
                                                      <li><a href="documents/basicessaystructure.pdf">Basic Essay Structure</a></li>
                                                      
                                                      <li><a href="documents/ElementsofanExcellentEssayCSSCTipSheet_Revised_.pdf">Elements of an Excellent Essay</a></li>
                                                      
                                                      <li><a href="documents/IntroductionsandConclusions.pdf">Introductions and Conclusions</a></li>
                                                      
                                                      <li><a href="documents/WritingEffectiveIntroductions.pdf">Writing Effective Introductions</a></li>
                                                      
                                                      <li><a href="documents/TheFiveParagraphEssay.pdf">The Five Paragraph Essay</a></li>
                                                      
                                                      <li><a href="documents/WriteanOutline.pdf">Writing an Outline</a></li>
                                                      
                                                   </ul>
                                                   
                                                   <p><strong>--</strong></p>
                                                   
                                                   <ul>
                                                      
                                                      <li><a href="documents/concretelanguageexample.pdf">Concrete Language</a></li>
                                                      
                                                      <li><a href="documents/LogicalFallaciesCSSCTipSheet_Revised_.pdf">Logical Fallacies </a></li>
                                                      
                                                      <li><a href="documents/OrganizingandConnectingSentences.pdf">Organizing and Connecting Sentences</a></li>
                                                      
                                                      <li><a href="documents/StrategiesforRevisingSentences.pdf">Strategies for Revising Sentences</a></li>
                                                      
                                                      <li><a href="documents/WritingDialogueCSSCTipSheet_Revised_.pdf">Writing Dialogue </a></li>
                                                      
                                                   </ul>                            
                                                </div>
                                                
                                                <div>
                                                   
                                                   <p><strong>Types of Essays</strong></p>
                                                   
                                                   <ul>
                                                      
                                                      <li><a href="documents/TypesofEssaysandTheirMethods.pdf">Types of Essays and Their Methods</a></li>
                                                      
                                                      <li><a href="documents/CRITICALANALYSISintheHumanitiesMusic_Revised_.pdf">Critical Analysis in the Humanities: Music </a></li>
                                                      
                                                      <li><a href="documents/CriticalAnalysisVisualArtCSSCTipSheet_Revised_.pdf">Critical Analysis: Visual Art </a></li>
                                                      
                                                      <li><a href="documents/CriticalAnalysis_LiteratureCSSCTipSheet_Revised_.pdf">Critical Analysis: Literature</a></li>
                                                      
                                                      <li><a href="documents/LiteraryResearchPaperStructure.pdf">Literary Research Paper Structure</a></li>
                                                      
                                                      <li><a href="documents/Whatisapersuasive.pdf">What is a Persuasive Essay?</a></li>
                                                      
                                                      <li><a href="documents/ElementsofPersuasive.pdf">Elements of Persuasive Writing</a></li>
                                                      
                                                      <li><a href="documents/TypesofEvidenceinPersuasiveFINAL.pdf">Types of Evidence in Persuasive Writing</a></li>
                                                      
                                                      <li><a href="documents/Whatisapersuasive.pdf">What is a Persuasive Essay?</a></li>
                                                      
                                                      <li><a href="documents/ThreeFoldAppealsforaWellRoundedArgument.pdf">Three Fold Appeals for a Well Rounded Argument</a></li>
                                                      
                                                      <li><a href="documents/GraphicOrganizerforArgumentCSSCTipSheet_Revised_.pdf">Graphic Organizer for Argument</a></li>
                                                      
                                                      <li><a href="documents/SampleArgumentOutline.pdf">Sample Argument Outline</a></li>
                                                      
                                                   </ul>                            
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <h2>
                                       <strong>Reading</strong><br>
                                       
                                    </h2>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/CommonPatternsofOrganization.pdf">Common Patterns of Organization</a></li>
                                          
                                          <li>Critical Reading <a href="documents/CriticalReadingI.pdf">I</a> <a href="documents/CriticalReadingII.pdf">II</a> <a href="documents/CriticalReadingIII.pdf">III</a> <a href="documents/CriticalreadingIV.pdf">IV</a> <a href="documents/CriticalReadingV.pdf">V</a> <a href="documents/CriticalReadingVI.pdf">VI</a>
                                             
                                          </li>
                                          
                                          <li><a href="documents/MakingInferences.pdf">Making Inferences</a></li>
                                          
                                          <li><a href="documents/OrganizationalPatternsinAcademicWriting.pdf">Organizational Patterns in Academic Writing</a></li>
                                          
                                          <li><a href="documents/PatternsofOrganization.pdf">Patterns of Organization</a></li>
                                          
                                          <li><a href="documents/ReadingStrategies.pdf">Reading Strategies</a></li>
                                          
                                          <li><a href="documents/SignalWordsReading.pdf">Signal Words Reading</a></li>
                                          
                                          <li><a href="documents/TheElementsofEffectiveWriting.pdf">The Elements of Effective Writing</a></li>
                                          
                                          <li><a href="documents/VocabularySkills.pdf">Vocabulary Skills</a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    <h2>
                                       <strong>Study Skills</strong><br>
                                       
                                    </h2>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><a href="documents/GettingtheMostoutofReadingYourTextbooks.pdf">Getting the Most Out of Reading Your Textbooks</a></li>
                                          
                                          <li><a href="documents/ListeninClass.pdf">Listening in Class</a></li>
                                          
                                          <li>Organizing Information <a href="documents/OrganizingInformationI.pdf">I</a> <a href="documents/OrganizingInformationII.pdf">II</a> <a href="documents/OrganizingInformationIII.pdf">III</a> <a href="documents/OrganizingInformationIV.pdf">IV</a> <a href="documents/OrganizingInformationV.1.pdf">V</a> <a href="documents/OrganizingInformationVI.pdf">VI</a>
                                             
                                          </li>
                                          
                                          <li><a href="documents/TakeGoodNotes.pdf">Taking Good Notes</a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Contact Us</strong></p>
                                                               
                                                               <p><strong>Call:<br>
                                                                     </strong>407-582-6818
                                                               </p>
                                                               
                                                               <p><strong>Visit:<br>
                                                                     </strong>850 West Morse Blvd, Room 136<br>
                                                                  Winter Park, Fl 32789
                                                               </p>
                                                               
                                                               <p><strong>Current Hours </strong></p>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Monday - Thursday </div>
                                                                        
                                                                        <div>8:00 am - 7:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Friday</div>
                                                                        
                                                                        <div>8:00 am - 12:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Saturday</div>
                                                                        
                                                                        <div>CLOSED</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div> *Hours are subject to change</div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <p>**Please note: Last consultation is given 30 minutes before closing.</p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Student Tip<br>
                                                                     </strong>Need help with an essay but can't make it into the CSSC? 
                                                               </p>
                                                               
                                                               
                                                               <p>Try our online writing <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">consultation!</a></p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div><a href="consultations.html"><img alt="menu_consultations" border="0" height="66" src="WritingConsultation.gif" width="236"></a></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="cpt.html"><img alt="menu_pert" border="0" height="66" src="Pert.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="CSSCVideos.html"><img alt="cssc_videos" border="0" height="66" src="CSSC_Videos.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h2>&nbsp;</h2>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/cssc_tipsheets.pcf">©</a>
      </div>
   </body>
</html>