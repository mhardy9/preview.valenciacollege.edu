<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/consultations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Consultations</h2>
                        
                        <p>Students currently enrolled in Valencia classes can visit the CSSC (Room 136, Winter
                           Park Campus) for one-on-one consultations. In these consultations, trained consultants
                           provide students with suggestions and corrective feedback they can apply to an assignment
                           before turning it in. 
                        </p>
                        
                        <h2>Writing Consultations </h2>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h2>What Can You Expect from a Writing Consultation?</h2>
                                    
                                    <h3>The CSSC <em>WILL</em>:
                                    </h3>
                                    
                                    <ul>
                                       
                                       <li>Provide feedback on your typed papers </li>
                                       
                                       <li>Analyze strengths and weaknesses in your writing </li>
                                       
                                       <li>Assist you in becoming a better and more effective writer </li>
                                       
                                       <li>Help you understand basic grammatical concepts </li>
                                       
                                       <li>Review MLA and APA styles without doing the work for you </li>
                                       
                                    </ul>
                                    
                                    <h3>The CSSC <em>WILL NOT</em>:
                                    </h3>
                                    
                                    <ul>
                                       
                                       <li>Edit or proofread papers </li>
                                       
                                       <li>Be a drop-off</li>
                                       
                                       <li>Spell or grammar check your entire paper </li>
                                       
                                       <li>Tell students the answers </li>
                                       
                                       <li>Write your paper for you </li>
                                       
                                       <li>Guess about any unwritten intentions behind your assignments </li>
                                       
                                       <li>Proofread or correct citations and references </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    <p><img alt="Consultation" height="203" src="casee.jpg" width="327"></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>      
                        
                        <p> As a writer, your paper is ultimately your responsibility, so you are responsible
                           for all decisions made during the writing process, including proofreading and editing.
                           Our goal is to have you leave the writing consultation as a stronger, more confident
                           writer so you can produce an even better paper next time.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><span>In order to receive a writing consultation, you MUST: </span></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li>
                                             
                                             <div>Bring a <strong>printed copy</strong> of the essay
                                             </div>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <p><span><span>Note: An assignment sheet from the professor is also preferred</span></span></p>
                                       
                                       <p>The last consultation is given 30 minutes before closing. </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>In addition to face-to-face writing consultations, students can work with a consultant
                           via e-mail. 
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><span>In order to receive an e-mail writing consultation, you MUST: </span></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>Submit your essay via <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">this link</a> 
                                       </div>
                                       
                                       <p>Please allow 2-3 business days for a consultant to get back to you on your paper.
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <h2>Study Strategies Consultations </h2>
                           
                        </div>
                        
                        <h2>What Can You Expect from a Study Strategies Consultation?</h2>
                        
                        <ul>
                           
                           <li>We want to help you learn different study strategies that will enable you to work
                              smarter, not harder.
                           </li>
                           
                           <li> Our goal is to help you make the best, most efficient use of your study time.</li>
                           
                           <li> We will be happy to help you master different study strategies; however, all of your
                              needs may not be addressed in one term.
                           </li>
                           
                        </ul>
                        
                        <h2>Reading Consultations </h2>
                        
                        <h2>What Can You Expect from a Reading Consultation?</h2>
                        
                        <ul>
                           
                           <li>We want to assist you in mastering various reading skills.</li>
                           
                           <li> Our goal is to help you improve your reading comprehension.</li>
                           
                           <li> We will be happy to help you understand and apply various reading strategies.</li>
                           
                        </ul>
                        
                        <h2>Speech Consultations </h2>
                        
                        <h2> What Can You Expect from a Speech Consultation?</h2>
                        
                        <ul>
                           
                           <li>We want to help you become a more dynamic and effective speaker.</li>
                           
                           <li> Our goal is to help you improve your public speaking skills.</li>
                           
                           <li> We will be happy to help you plan your speech, offer suggestions and feedback, and
                              assist you in using visual aids.
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/consultations.pcf">©</a>
      </div>
   </body>
</html>