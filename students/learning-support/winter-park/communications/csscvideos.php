<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/csscvideos.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        <h2>CSSC Video Series</h2>
                        
                        <h3><strong>Videos Created by the CSSC </strong></h3>
                        
                        <p>Our staff members have created a series short tutorial videos to help guide you with
                           grammar, writing, and reading skills.
                        </p>
                        
                        <p><strong>Run-On Sentences</strong></p>
                        
                        <p><a href="http://youtu.be/LIZoKwoD8Ow"><img alt="RunOn Image" border="0" height="270" src="runon_logo.jpg" width="491"></a></p>
                        
                        <p>Click image to view the video </p>
                        
                        <p>-</p>
                        
                        <p>More videos are on their way, so keep checking back! </p>
                        
                        <h3>Other Helpful Videos</h3>
                        
                        <p>Although these videos are not created by our staff members, they are produced by Valencia's
                           East Campus Academic Success Center.
                        </p>
                        
                        <p><strong>MLA Assistance </strong></p>
                        
                        <p><span><a href="../../east/academic-success/eap/mla_first_page.swf.html">How to Format the First Page</a></span></p>
                        
                        <p><strong><a href="../../east/academic-success/eap/page_numbers_MLA.swf.html">How to Insert Page Numbers</a></strong></p>
                        
                        <p><strong><a href="../../east/academic-success/eap/mla_works_cited.swf.html">How to Format the Works Cited Page  </a></strong></p>
                        
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/csscvideos.pcf">©</a>
      </div>
   </body>
</html>