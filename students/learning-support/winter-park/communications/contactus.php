<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/contactus.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <div>
                                       <br>
                                       
                                    </div> 
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <h3>Writing – Reading – Speech – Study Skills </h3>
                                    
                                    <p><img alt="CSSC Contact Us" height="292" src="CSSC_Banner_000.jpg" width="519"></p>
                                    
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><img alt="Jason Balserait Picture" border="1" height="201" src="1024.png" width="201"></div>
                                             
                                             <div>
                                                
                                                <h2>Christopher Brown</h2>
                                                
                                                <p> <a href="mailto:cbrown214@valenciacollege.edu" rel="noreferrer"><strong>cbrown214@valenciacollege.edu</strong></a><br>
                                                   <strong>407-582-6820</strong></p>
                                                
                                                <p> Chris earned his Master's degree in Rhetoric from Carnegie Mellon University and
                                                   is the Supervisor of the Communications Student Support Center. He also teaches Composition
                                                   1 and 2 at the Winter Park campus.
                                                </p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    <br>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><img alt="Jason Balserait Picture" border="1" height="201" src="1024.png" width="201"></div>
                                             
                                             <div>
                                                
                                                <h2><strong>Chris Borglum</strong></h2>
                                                
                                                <p><strong><a href="mailto:cborglum@valenciacollege.edu">cborglum@valenciacollege.edu</a><br>
                                                      407-582-6869</strong></p>
                                                
                                                <p>          Chris earned a Bachelor's and Master's degree specializing in Literary Theory
                                                   at the University of Florida. <br>
                                                   
                                                </p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div><img alt="Jason Balserait Picture" border="1" height="128" hspace="0" src="fredpic_000.jpg" vspace="0" width="226"></div>
                                             
                                             <div><strong><img alt="Ashley's Picture" border="1" height="128" hspace="0" src="ashleypic_000.jpg" vspace="0" width="226"></strong></div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong>Fred Lambert </strong> 
                                                </p>
                                                
                                                <p><a href="mailto:flambert@valenciacollege.edu">flambert@valenciacollege.edu<br>
                                                      </a>407-582-6818 
                                                </p>
                                                
                                                <p>Fred is a four-year Marine Corps veteran and graduate of the University of Central
                                                   Florida with a B.A. in Journalism. He is an aggregate Internet news writer for United
                                                   Press International and enjoys writing prose and poetry (mostly prose) in his spare
                                                   time. 
                                                </p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong>Ashley Saunders-McCutcheon</strong> 
                                                </p>
                                                
                                                <p><a href="mailto:asaundersmccutcheon@valenciacollege.edu">asaundersmccutcheon@valenciacollege.edu<br>
                                                      </a>407-582-6818 
                                                </p>
                                                
                                                <p>Ashley is a recent graduate from Brigham Young University–Hawaii with a Bachelor’s
                                                   in Psychology. She loves helping others in achieving their goals, and finds great
                                                   satisfaction working in the CSSC. 
                                                </p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong><img alt="Jason Balserait Picture" border="1" height="133" hspace="0" src="alex_cssc.jpg" vspace="0" width="228"></strong></p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><img alt="Jason Balserait Picture" border="1" height="128" src="16x9_003.png" width="226"></strong></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong>Alex Douce </strong></p>
                                                
                                                <p><a href="mailto:adouce@valenciacollege.edu">adouce@valenciacollege.edu<br>
                                                      </a>407-582-6818 
                                                </p>
                                                
                                                <p>Alex is currently a student at Valencia College and plans on becoming an educational
                                                   RN. She has always loved writing and views it as an outlet for her creativity.   
                                                </p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong>Michel Frage </strong></p>
                                                
                                                <p><a href="mailto:pfrage@valenciacollege.edu">pfrage@valenciacollege.edu<br>
                                                      </a>407-582-6818 
                                                </p>
                                                
                                                <p>Michel is currently a student at Valencia and hopes to become a translator. He has
                                                   previously worked as an SL and is excited to join the CSSC team.
                                                </p>                        
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    <br>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Contact Us</strong></p>
                                                               
                                                               <p><strong>Call:<br>
                                                                     </strong>407-582-6818
                                                               </p>
                                                               
                                                               <p><strong>Visit:<br>
                                                                     </strong>850 West Morse Blvd, Room 136<br>
                                                                  Winter Park, Fl 32789
                                                               </p>
                                                               
                                                               <p><strong>Current Hours </strong></p>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Monday - Thursday </div>
                                                                        
                                                                        <div>8:00 am - 7:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Friday</div>
                                                                        
                                                                        <div>8:00 am - 12:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Saturday</div>
                                                                        
                                                                        <div>CLOSED</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div> *Hours are subject to change</div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <p>**Please note: Last consultation is given 30 minutes before closing.</p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Student Tip<br>
                                                                     </strong>Need help with an essay but can't make it into the CSSC? 
                                                               </p>
                                                               
                                                               
                                                               <p>Try our online writing <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">consultation!</a></p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div><a href="consultations.html"><img alt="menu_consultations" border="0" height="66" src="WritingConsultation.gif" width="236"></a></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="cpt.html"><img alt="menu_pert" border="0" height="66" src="Pert.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="CSSCVideos.html"><img alt="cssc_videos" border="0" height="66" src="CSSC_Videos.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <div>
                                       <a href=""><img alt="Line" height="6" src="line_002.jpg" width="582"></a> 
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/contactus.pcf">©</a>
      </div>
   </body>
</html>