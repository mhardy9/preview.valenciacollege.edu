<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li>Student Support Winter Park</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <h3>Writing - Reading - Speech - Study Skills </h3>
                                                
                                                <p><img alt="We Can Make A Difference!" height="292" src="CSSC_Banner.jpg" width="519"> 
                                                </p>
                                                
                                                <p>The Communications Student Support Center (CSSC) offers students help with understanding
                                                   assignments, developing ideas, drafting papers, assessing grammar and writing skills,
                                                   reading skills, speech skills, and much more
                                                </p>
                                                
                                                <p>Services</p>
                                                
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div><a href="writing-workshops.html"><img alt="E-mail Consultation Service" border="0" height="76" hspace="10" src="Information.png" vspace="10" width="76"></a></div>
                                                         
                                                         <div>
                                                            
                                                            <h3>Writing Workshops</h3>
                                                            
                                                            <p>We are proud to announce the schedule for this semester's <a href="writing-workshops.html">Writing Workshops!</a></p>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div><img alt="E-mail Consultation Service" border="0" height="76" hspace="10" src="Artboard1_000.png" vspace="10" width="76"></div>
                                                         
                                                         <div>
                                                            
                                                            <h3>E-mail Consultations</h3>
                                                            Students can work with one of our consultants regarding assignments via e-mail. Click
                                                            <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">here</a> for more information. 
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div><img alt="Sign Up for Tutoring" border="0" height="76" hspace="10" src="Video_000.png" vspace="10" width="76"></div>
                                                         
                                                         <div>
                                                            
                                                            <h3>Boot Camp Videos</h3>
                                                            Can't make it to the face-to-face Writing Workshop series? View online versions of
                                                            our workshops in <a href="CSSC_Videos.html">our Writing Boot Camp video series</a>. 
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div><img alt="Sign Up for Tutoring" border="0" height="76" hspace="10" src="Pencil.png" vspace="10" width="76"></div>
                                                         
                                                         <div>
                                                            
                                                            <h3>One-on-One Tutoring</h3>
                                                            We offer personalized one-on-one tutoring sessions with grammar, writing, or reading
                                                            skills. If you're interested in this service, stop by the CSSC (Room 136) or <a href="mailto:cbrown214@valenciacollege.edu">e-mail the coordinator</a> for more information. 
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                <div>
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <p><strong>Contact Us</strong></p>
                                                                        
                                                                        <p><strong>Call:<br>
                                                                              </strong>407-582-6818
                                                                        </p>
                                                                        
                                                                        <p><strong>Visit:<br>
                                                                              </strong>850 West Morse Blvd, Room 136<br>
                                                                           Winter Park, Fl 32789
                                                                        </p>
                                                                        
                                                                        <p><strong>Hours </strong></p>
                                                                        
                                                                        <div>
                                                                           
                                                                           <div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    <div>Monday - Thursday </div>
                                                                                 </div>
                                                                                 
                                                                                 <div>
                                                                                    <div>8:00 am - 7:00 pm </div>
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    <div>Friday</div>
                                                                                 </div>
                                                                                 
                                                                                 <div>
                                                                                    <div>8:00 am - 12:00 pm </div>
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    <div>Saturday</div>
                                                                                 </div>
                                                                                 
                                                                                 <div>
                                                                                    <div>CLOSED</div>
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                              
                                                                              <div>
                                                                                 
                                                                                 <div>
                                                                                    <div>*Hours are subject to change</div>
                                                                                 </div>
                                                                                 
                                                                              </div>
                                                                              
                                                                           </div>
                                                                           
                                                                        </div>
                                                                        
                                                                        <p>**Please note: Last consultation is given 30 minutes before closing.</p>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <p><strong>E-mail Us Your Essay! <br>
                                                                              </strong>Need help with an essay but can't make it into the CSSC? 
                                                                        </p>
                                                                        
                                                                        
                                                                        <p>Try our online writing <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">consultation!</a></p>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            <div><a href="consultations.html"><img alt="menu_consultations" border="0" height="66" src="WritingConsultation.gif" width="236"></a></div>
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div><a href="cpt.html"><img alt="menu_pert" border="0" height="66" src="Pert.gif" width="236"></a></div>
                                                         
                                                      </div>
                                                      
                                                      <div>
                                                         
                                                         <div><a href="CSSCVideos.html"><img alt="cssc_videos" border="0" height="66" src="CSSC_Videos.gif" width="236"></a></div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/index.pcf">©</a>
      </div>
   </body>
</html>