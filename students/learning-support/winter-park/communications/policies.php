<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h2>Mission Statement</h2>
                        
                        <p>The Winter Park Campus Communications Student Support Center provides students and
                           faculty a variety of learning-centered services to support and better student outcomes,
                           promote Valencia's core competencies, and strengthen a cooperative learning community.
                           
                        </p>
                        
                        <h2><strong>CSSC Best Practices and Policies:<br>
                              </strong></h2>
                        
                        <h2>General Rules:</h2>
                        
                        <ul>
                           
                           <li>The CSSC is a <span>cell phone free zone</span>. Please take all phone calls outside and place your phone on "silent" or "vibrate"
                              when in the CSSC.
                           </li>
                           
                           <li>Please use your "inside voice" when conversing in the CSSC so as not to disturb your
                              peers.
                           </li>
                           
                           <li>No children are permitted per Valencia policy 6Hx28:04-10</li>
                           
                        </ul>            
                        
                        <h2>Use of CSSC Services:</h2>
                        
                        <ul>
                           
                           <li>CSSC services (including computer use, tutoring, and consultations) are only provided
                              to <span>currently enrolled</span> Valencia students.
                           </li>
                           
                           <li>You must sign in using your <strong>ATLAS</strong> username and password before using any services in the CSSC. Make sure to sign out
                              before you leave. 
                           </li>
                           
                           <li>Please have your <strong>VID</strong> available for use in consultations. We record consultations and these records can
                              be made available to your instructor. 
                           </li>
                           
                           <li>You must have a <span>printed copy</span> of your work for a consultation. Printing can be done in the CSSC; however, you will
                              need a copy/print card which can be purchased at the library or campus bookstore for
                              a minimum of $1.00. 
                           </li>
                           
                        </ul>            
                        
                        <h2>E-mail Consultations:</h2>
                        
                        <ul>
                           
                           <li>Please keep in mind that consultants only work on emailed consultations during the
                              hours that they are scheduled at the CSSC. Do not expect to receive a response during
                              non-CSSC hours.
                           </li>
                           
                           <li>Please expect between a <span>48 to 72 hour turnaround</span> on online consultations, especially during peak times like the last weeks of the
                              semester. Also, since face-to-face consultations take preference, do not expect an
                              instant response. 
                           </li>
                           
                           <li>Please <span>be specific</span> when sending emailed consultations. Include your name, your professor's name and
                              email, and a brief explanation of your concerns about your work in your message to
                              us. 
                           </li>
                           
                           <li>Please include your paper as an attachment in <strong>.doc</strong>, <strong>.docx</strong>, or <strong>.rtf</strong> format. Do not copy and paste your paper into the body of your email message. 
                           </li>
                           
                        </ul>            
                        
                        <h2>Internet Use Policy:</h2>
                        
                        <p><strong><strong>THE WINTER PARK CAMPUS RESERVES THE RIGHT TO MODIFY THIS POLICY AT ANY TIME. </strong></strong></p>
                        
                        <p><font color="#FF0000"><strong>ACCESS TO THE INTERNET IS PROVIDED 
                                 FOR THE ACADEMIC SUPPORT AND RESEARCH OF VALENCIA'S STUDENTS, FACULTY, 
                                 AND STAFF. </strong></font></p>
                        
                        <ul>
                           
                           <li>Users must have a current Valencia Student I.D. card that must 
                              be presented to staff. Use of Internet-access computers is on 
                              a first-come-first-served basis. The College reserves the right 
                              to limit access to its computing resources and its equipment. 
                              <br>
                              <br>
                              
                           </li>
                           
                           <li>Internet workstation use is <strong>LIMITED TO TWO-HOUR TIME 
                                 </strong><strong>increments. </strong><br>
                              <br>
                              
                           </li>
                           
                           <li>As the Internet workstation display screen is in view of all 
                              patrons and staff, <u><strong>users are not permitted to display 
                                    any visual images, sound, or text that would create an atmosphere 
                                    of discomfort or harassment for others. <br>
                                    <br>
                                    </strong></u> 
                              
                           </li>
                           
                           <li>Although staff is able to offer searching suggestions and answer 
                              some questions, they will not provide in-depth training on Internet 
                              searching, software programs, and personal computer use. Classes 
                              are available for this purpose. <br>
                              <br>
                              
                           </li>
                           
                           <li>If you wish to save files, you may bring a blank formatted diskette 
                              or a recordable cd-rom. Software downloaded from the Internet 
                              may contain a virus; you need to have virus-checking software 
                              on your computer. The campus is not responsible for damage or 
                              liability that may occur from a customer's use of the campus computers. 
                              <br>
                              <br>
                              
                           </li>
                           
                           <li>The computer operator is liable for any damage done to the workstation's 
                              hardware or software and for any illegal or unethical acts performed 
                              through the system. This is not limited to physical damage or 
                              vandalism. The computer equipment and software is to be used as 
                              installed. Do not delete, add to, or modify the installed hardware 
                              or software. Do not use the campus computers to make unauthorized 
                              entry into any other computers or networks. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>WARNING: </strong> As voluntary users, Internet searchers 
                              are responsible for defining the constraints of the search. The 
                              College shall not be held accountable for the accuracy or decency 
                              of data retrieved via the Internet. Not all sources on the Internet 
                              provide accurate, complete, or current information. Be a selective 
                              consumer, questioning the validity of the information you find. 
                              
                           </li>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <p><strong>UNACCEPTABLE USE INCLUDES ANY ACTIVITY 
                                          WHICH IS NOT RELATED TO EDUCATIONAL OR RESEARCH NEEDS INCLUDING, 
                                          BUT NOT LIMITED TO: </strong></p>
                                    
                                    <ul>
                                       
                                       <li>Any use of the Internet for commercial purposes. <br>
                                          
                                       </li>
                                       
                                       <li>Downloading to or installing any program on the hard drive 
                                          of any computer. <br>
                                          
                                       </li>
                                       
                                       <li>Accessing chat rooms or chat channels other than those 
                                          related to classroom assignments. <br>
                                          
                                       </li>
                                       
                                       <li>Downloading or installing music files onto any computer. 
                                          <br>
                                          
                                       </li>
                                       
                                       <li>Committing illegal or unethical acts, including unauthorized 
                                          entry into other computers. <br>
                                          
                                       </li>
                                       
                                       <li>Game playing from any Internet site. <br>
                                          
                                       </li>
                                       
                                       <li>Any display of images, sounds, or text, which could create 
                                          an atmosphere of distress or harassment of other library 
                                          users. <br>
                                          
                                       </li>
                                       
                                       <li>Displaying pornographic images which disrupt or create 
                                          an atmosphere of distress or harassment of other library 
                                          users. <br>
                                          
                                       </li>
                                       
                                       <li>Violating copyright laws and fair-use provisions through 
                                          inappropriate reproduction or dissemination of copyrighted 
                                          text, images, or other resources. Everything on the Internet 
                                          is to be considered copyrighted. 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/policies.pcf">©</a>
      </div>
   </body>
</html>