<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/cssc_helpfulwebsites.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div> 
                                    <div>
                                       <br>
                                       
                                    </div> 
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <h3>Writing – Reading – Speech – Study Skills </h3>
                                    
                                    <p><img alt="CSSC Contact Us" height="292" src="CSSC_Banner_000.jpg" width="519"></p>
                                    
                                    
                                    
                                    <h2>Helpful Websites </h2>
                                    
                                    <h3>Grammar </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://web2.uvcs.uvic.ca/elc/studyzone/grammar.htm">Grammar presentations, interactive practice exercises, and help pages for basic writing
                                             skills</a></li>
                                       
                                       <li><a href="http://chompchomp.com/menu.htm">Grammar Bytes! An awesome grammar site from Valencia’s own Prof. Robin Simmons</a></li>
                                       
                                    </ul>
                                    
                                    <h3>Writing </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://web.uvic.ca/wguide/Pages/MasterToc.html"> Discusses the writing process and sentence/paragraph structure</a></li>
                                       
                                       <li>
                                          <a href="http://www.unc.edu/depts/wcweb/handouts/readassign.html"> Tips on how to read and understand an academic writing assignment</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.wisc.edu/writing/Handbook/CommonErrors.html"> 12 Common Errors: An Editing Checklist</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="https://owl.english.purdue.edu/owl/resource/561/01/"> Proofreading, Editing and Revising</a> 
                                       </li>
                                       
                                       <li><a href="http://www.presentationhelper.co.uk/">Presentations, speeches, and Powerpoint tutorials with templates and more</a></li>
                                       
                                    </ul>
                                    
                                    <h3>Speech </h3>
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="http://www.rpi.edu/"> Developing and Delivering a Presentation</a><br>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <h3>Reading </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://users.dhp.com/~laflemm/RfT/Tut2.htm"> Practice distinguishing fact and opinion</a></li>
                                       
                                       <li><a href="http://literacynet.org/cnnsf/">Use CNN news articles to practice various reading comprehension skills <br>
                                             </a></li>
                                       
                                    </ul>
                                    
                                    <h3>Computer Skills </h3>
                                    
                                    <ul>
                                       
                                       <li>
                                          <span>New!</span> <a href="http://www.investintech.com/content/beginnersmsoffice/">A Beginner's Guide to Microsoft Office</a> 
                                       </li>
                                       
                                       <li><a href="https://support.office.com/en-in/article/Word-2003-to-Word-2007-tools-and-commands-reference-guide-1fa4c0fa-9342-46bd-bdd5-65b7521b1dc5">Microsoft Word 2007 Command Reference Guide</a></li>
                                       
                                       <li><a href="http://support.microsoft.com/ph/11377">Microsoft Word 2007 Solutions Center </a></li>
                                       
                                    </ul>
                                    
                                    <h3>Study Skills </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.ucc.vt.edu/stdysk/stdyhlp.html"> Virginia Tech’s study skills self-help information</a></li>
                                       
                                    </ul>
                                    
                                    <h3>Competency Exams </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="../../learning-support/index.html"> On-line Prep English II Florida Basic Skills practice test. </a></li>
                                       
                                       <li><a href="../../learning-support/index.html"> Sample essay prompts for the essay portion of the Prep English II competency exam.
                                             </a></li>
                                       
                                    </ul>
                                    
                                    <h3>EAP </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.manythings.org/"> Interesting things for ESL students </a></li>
                                       
                                       <li><a href="http://www.eslcafe.com/"> Dave Sperling’s ESL Café features help with idioms and phrasal verbs as well as an
                                             assortment of quizzes. </a></li>
                                       
                                    </ul>
                                    
                                    <h3>Research and Documentation (Additional MLA and APA help) </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.aresearchguide.com"> Covers all stages of research, parts of a paper, and presentations</a></li>
                                       
                                       <li><a href="http://www.mla.org/style"> Official MLA website</a></li>
                                       
                                       <li><a href="http://www.apastyle.org/"> Official APA website</a></li>
                                       
                                       <li><a href="http://www.uwsp.edu/psych/apa4b.htm"> Site with guidelines for correct APA citations</a></li>
                                       
                                       <li><a href="http://www.lib.duke.edu/libguide/citing.htm"> Duke University’s Libraries’ Guide to Citing Sources and Avoiding Plagiarism:<br>
                                             Documentation Guidelines</a></li>
                                       
                                       <li><a href="http://www.dianahacker.com/resdoc/"> Diana Hacker’s Research and Documentation On-Line, includes sample MLA and APA papers</a></li>
                                       
                                       <li><a href="http://www.utoronto.ca/"> How to take notes from research reading</a></li>
                                       
                                    </ul>
                                    
                                    <h3> Dictionaries and Thesauruses </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.m-w.com/"> Merriam-Webster on-line dictionary and thesaurus, plus fun word games</a></li>
                                       
                                       <li><a href="http://dictionary.reference.com/"> On-line dictionary</a></li>
                                       
                                       <li><a href="http://dictionary.cambridge.org/"> Includes dictionaries of idioms and phrasal verbs</a></li>
                                       
                                       <li><a href="http://www.bartleby.com/"> Great books on-line including the American Heritage Dictionary, Fourth Edition, and
                                             Roget’s II: The New Thesaurus</a></li>
                                       
                                    </ul>
                                    
                                    <h3> All of the Above </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://owl.english.purdue.edu/sitemap.html"> ESL, general writing concerns, and grammar information and activities</a></li>
                                       
                                    </ul>
                                    
                                    <h3>College Admissions and Scholarship Essays </h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.collegeadmissionsessays.com/"> This website is devoted entirely to the college admission essays and helping you
                                             write the most effective essays to gain admission into your top-choice colleges</a></li>
                                       
                                    </ul>                  
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Contact Us</strong></p>
                                                               
                                                               <p><strong>Call:<br>
                                                                     </strong>407-582-6818
                                                               </p>
                                                               
                                                               <p><strong>Visit:<br>
                                                                     </strong>850 West Morse Blvd, Room 136<br>
                                                                  Winter Park, Fl 32789
                                                               </p>
                                                               
                                                               <p><strong>Current Hours </strong></p>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Monday - Thursday </div>
                                                                        
                                                                        <div>8:00 am - 7:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Friday</div>
                                                                        
                                                                        <div>8:00 am - 12:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Saturday</div>
                                                                        
                                                                        <div>CLOSED</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div> *Hours are subject to change</div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <p>**Please note: Last consultation is given 30 minutes before closing.</p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Student Tip<br>
                                                                     </strong>Need help with an essay but can't make it into the CSSC? 
                                                               </p>
                                                               
                                                               
                                                               <p>Try our online writing <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">consultation!</a></p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div><a href="consultations.html"><img alt="menu_consultations" border="0" height="66" src="WritingConsultation.gif" width="236"></a></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="cpt.html"><img alt="menu_pert" border="0" height="66" src="Pert.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="CSSCVideos.html"><img alt="cssc_videos" border="0" height="66" src="CSSC_Videos.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/cssc_helpfulwebsites.pcf">©</a>
      </div>
   </body>
</html>