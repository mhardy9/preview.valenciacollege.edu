<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/tutoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 <div>  
                                    
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <h3>Writing – Reading – Speech – Study Skills </h3>
                                    
                                    <p><img alt="CSSC Contact Us" height="292" src="CSSC_Banner_000.jpg" width="519"></p>
                                    
                                    
                                    
                                    <p>Submit a Tutoring Request 
                                       
                                    </p>
                                    
                                    <p>If you'd like to request a reocurring weekly tutoring session with one of our friendly
                                       staff members, please submit a request below: 
                                    </p>
                                    
                                    <h3><a href="http://net4.valenciacollege.edu/forms/wp/cssc/tutor-request.cfm" target="_blank">Request Tutoring</a></h3>
                                    
                                    
                                    <p>Smart Thinking<br>
                                       
                                    </p>
                                    
                                    Need immediate assistance but can't make it to the CSSC? Check out Smart Thinking!
                                    
                                    <p>1. Log into your Atlas account and click the "Courses" tab. Then click the "Tutoring
                                       (online) - Smart Thinking" link. 
                                    </p>
                                    
                                    <p><img alt="SmartThinking Main" border="2" height="377" src="smartthink_main2.jpg" width="582"></p>
                                    
                                    <p>2. Choose the type of assistance.</p>
                                    
                                    <p><img alt="Smart Thinking Menu" border="2" height="304" src="smartthink_menu.jpg" width="582"></p>
                                    
                                    <p>3. Select a subject. </p>
                                    
                                    <p><img alt="SmartThinking Menu 2" border="2" height="197" src="smartthink_menu2.jpg" width="582"></p>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Contact Us</strong></p>
                                                               
                                                               <p><strong>Call:<br>
                                                                     </strong>407-582-6818
                                                               </p>
                                                               
                                                               <p><strong>Visit:<br>
                                                                     </strong>850 West Morse Blvd, Room 136<br>
                                                                  Winter Park, Fl 32789
                                                               </p>
                                                               
                                                               <p><strong>Current Hours </strong></p>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Monday - Thursday </div>
                                                                        
                                                                        <div>8:00 am - 7:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Friday</div>
                                                                        
                                                                        <div>8:00 am - 12:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Saturday</div>
                                                                        
                                                                        <div>CLOSED</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div> *Hours are subject to change</div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <p>**Please note: Last consultation is given 30 minutes before closing.</p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Student Tip<br>
                                                                     </strong>Need help with an essay but can't make it into the CSSC? 
                                                               </p>
                                                               
                                                               
                                                               <p>Try our online writing <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">consultation!</a></p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div><a href="consultations.html"><img alt="menu_consultations" border="0" height="66" src="WritingConsultation.gif" width="236"></a></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="cpt.html"><img alt="menu_pert" border="0" height="66" src="Pert.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="CSSCVideos.html"><img alt="cssc_videos" border="0" height="66" src="CSSC_Videos.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/tutoring.pcf">©</a>
      </div>
   </body>
</html>