<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <h2>Online Resources</h2>
                        
                        <p><strong><span>For a quick overview of our services, check out our</span> <a href="http://valenciacollege.edu/wp/cssc/cssc_online_guide.pptx">CSSC Online Brochure</a></strong></p>
                        
                        <h3>Downloadable Tutorials </h3>
                        
                        <p>Here are a few downloadable workshops, developed the CSSC Staff. Each of our downloadable
                           tutorials is presented as a PowerPoint presentation. Enjoy!
                        </p>
                        
                        <p><strong>Instructions:</strong></p>
                        
                        <ol>
                           
                           <li>
                              
                              <div><strong> Download and open file</strong></div>
                              
                           </li>
                           
                           <li>
                              
                              <div><strong>Click "Slide Show" </strong></div>
                              
                           </li>
                           
                           <li>
                              
                              <div><strong> Click "From Beginning" </strong></div>
                              
                           </li>
                           
                        </ol>                  
                        
                        <p><em>All PowerPoints are created by CSSC Staff </em></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <h4>Types of Writing </h4>
                                 </div>
                                 
                                 <div>
                                    <h4>Grammar Help </h4>
                                 </div>
                                 
                                 <div>
                                    <h4>MLA Help </h4>
                                 </div>
                                 
                                 <div>
                                    <h4>Everything You Need to Know About... </h4>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong><a href="http://valenciacollege.edu/wp/cssc/Narrative101pt2.pptx"><span><em>New!</em></span> Writing a Narrative</a></strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="http://valenciacollege.edu/wp/cssc/SV_Agreementpp.pptx"><strong>Subject/Verb Agreement </strong></a></p>
                                    
                                    <p>An interactive PowerPoint</p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><a href="documents/MLAWorkshop.ppt"><strong>MLA Basics Tutorial</strong></a></p>
                                    
                                    <p>An interactive PowerPoint </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><a href="http://valenciacollege.edu/wp/cssc/ThesisStatementsandTopicSentences.pptx">Thesis Statements &amp; Topic Sentences </a></strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong><a href="http://valenciacollege.edu/wp/cssc/ProcessEssays.pptx"><span><em></em></span> Writing a Process Essay</a></strong></p>
                                    
                                    <p><strong>An Interactive PowerPoint</strong> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><a href="http://valenciacollege.edu/wp/cssc/POWERPOINTFRAGMENTFINAL.pptx"> Fragments</a></strong> 
                                    </p>
                                    
                                    <p><strong>An Interactive PowerPoint</strong> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div><a href="http://valenciacollege.edu/wp/cssc/BLOCKQUOTINGCOMPUTERDIRECTIONSPOWERPOINTCOMPLETED.pptx"><strong>Block Quoting Tutorial </strong></a></div>
                                 
                                 <div>-</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Writing an Argumentative Essay</strong></p>
                                    
                                    <p><em><strong>Coming Soon!  </strong></em></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong><a href="http://valenciacollege.edu/wp/cssc/FragmentsPowerPoint.pptx">Fragments 2 </a></strong></p>
                                    
                                    <p><strong>A Guide to Prevent Sentence Fragments </strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Incorporating Citations</p>
                                    
                                    <p><em>Coming Soon!</em><em> </em></p>
                                    
                                 </div>
                                 
                                 <div>-</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>-</strong></div>
                                 
                                 <div>
                                    
                                    <p><strong> <a href="http://valenciacollege.edu/wp/cssc/Run-OnsPowerPoint.pptx">Run-Ons</a></strong></p>
                                    
                                    <p><strong>A Guide to Prevent Run-On Sentences </strong></p>
                                    
                                 </div>
                                 
                                 <div>-</div>
                                 
                                 <div>-</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>If you have any suggestions for future tutorials, please <a href="mailto:wpcenglishtutor@mail.valenciacollege.edu">e-mail us</a>! </strong></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>                  
                        
                        <h3>MLA and APA Help </h3>
                        
                        <p>Click <a href="../../library/mla-apa-chicago-guides/index.html">here</a> for an excellent resource provided by the Valencia College Library 
                        </p>
                        
                        <h3>Helpful Websites </h3>
                        
                        <p>Grammar </p>
                        
                        <ul>
                           
                           <li>
                              <a href="../../learning-support/communications/index.html"> Various grammar activities for Prep English students from the West Campus CSSC</a> 
                           </li>
                           
                           <li><a href="http://web2.uvcs.uvic.ca/elc/studyzone/grammar.htm">Grammar presentations, interactive practice exercises, and help pages for basic writing
                                 skills</a></li>
                           
                           <li><a href="http://chompchomp.com/menu.htm">Grammar Bytes! An awesome grammar site from Valencia’s own Prof. Robin Simmons</a></li>
                           
                        </ul>
                        
                        <p>Writing </p>
                        
                        <ul>
                           
                           <li><a href="http://web.uvic.ca/wguide/Pages/MasterToc.html"> Discusses the writing process and sentence/paragraph structure</a></li>
                           
                           <li>
                              <a href="http://www.unc.edu/depts/wcweb/handouts/readassign.html"> Tips on how to read and understand an academic writing assignment</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.wisc.edu/writing/Handbook/CommonErrors.html"> 12 Common Errors: An Editing Checklist</a> 
                           </li>
                           
                           <li>
                              <a href="https://owl.english.purdue.edu/owl/resource/561/1/"> Proofreading, Editing and Revising</a> 
                           </li>
                           
                           <li><a href="https://owl.english.purdue.edu/owl/resource/600/01/"> Strategies for improving sentence clarity</a></li>
                           
                           <li><a href="http://www.presentationhelper.co.uk/">Presentations, speeches, and Powerpoint tutorials with templates and more</a></li>
                           
                        </ul>
                        
                        <p>Speech </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.ccp.rpi.edu/resources/presentations/"> Developing and Delivering a Presentation</a><br>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Reading </p>
                        
                        <ul>
                           
                           <li><a href="http://users.dhp.com/~laflemm/RfT/Tut2.htm"> Practice distinguishing fact and opinion</a></li>
                           
                           <li><a href="http://literacynet.org/cnnsf/">Use CNN news articles to practice various reading comprehension skills <br>
                                 </a></li>
                           
                        </ul>
                        
                        <p>Computer Skills </p>
                        
                        <ul>
                           
                           <li>
                              <span>New!</span> <a href="http://www.investintech.com/content/beginnersmsoffice/">A Beginner's Guide to Microsoft Office</a> 
                           </li>
                           
                           <li><a href="http://download.microsoft.com/download/a/e/3/ae32801c-8122-436d-8167-f51c31901cad/AF010193868_en-us_wordmap_af010193868.xlsx">Microsoft Word 2007 Command Reference Guide</a></li>
                           
                           <li><a href="http://support.microsoft.com/ph/11377">Microsoft Word 2007 Solutions Center </a></li>
                           
                        </ul>
                        
                        <p>Study Skills </p>
                        
                        <ul>
                           
                           <li><a href="http://www.ucc.vt.edu/stdysk/stdyhlp.html"> Virginia Tech’s study skills self-help information</a></li>
                           
                        </ul>
                        
                        <p>Competency Exams </p>
                        
                        <ul>
                           
                           <li><a href="../../learning-support/communications/index.html"> On-line Prep English II Florida Basic Skills practice test. </a></li>
                           
                           <li><a href="../../learning-support/communications/index.html"> Sample essay prompts for the essay portion of the Prep English II competency exam.
                                 </a></li>
                           
                        </ul>
                        
                        <p>EAP </p>
                        
                        <ul>
                           
                           <li><a href="http://www.manythings.org/"> Interesting things for ESL students </a></li>
                           
                           <li><a href="http://www.eslcafe.com/"> Dave Sperling’s ESL Café features help with idioms and phrasal verbs as well as an
                                 assortment of quizzes. </a></li>
                           
                        </ul>
                        
                        <p>Research and Documentation (Additional MLA and APA help) </p>
                        
                        <ul>
                           
                           <li><a href="http://www.aresearchguide.com"> Covers all stages of research, parts of a paper, and presentations</a></li>
                           
                           <li><a href="http://www.mla.org/style"> Official MLA website</a></li>
                           
                           <li><a href="http://www.apastyle.org/"> Official APA website</a></li>
                           
                           <li><a href="http://www.uwsp.edu/psych/apa4b.htm"> Site with guidelines for correct APA citations</a></li>
                           
                           <li><a href="http://www.lib.duke.edu/libguide/citing.htm"> Duke University’s Libraries’ Guide to Citing Sources and Avoiding Plagiarism:<br>
                                 Documentation Guidelines</a></li>
                           
                           <li><a href="http://www.dianahacker.com/resdoc/"> Diana Hacker’s Research and Documentation On-Line, includes sample MLA and APA papers</a></li>
                           
                           <li><a href="http://www.writing.utoronto.ca/advice/reading-and-researching"> How to take notes from research reading</a></li>
                           
                        </ul>
                        
                        <p> Dictionaries and Thesauruses </p>
                        
                        <ul>
                           
                           <li><a href="http://www.m-w.com/"> Merriam-Webster on-line dictionary and thesaurus, plus fun word games</a></li>
                           
                           <li><a href="http://dictionary.reference.com/"> On-line dictionary</a></li>
                           
                           <li><a href="http://dictionary.cambridge.org/"> Includes dictionaries of idioms and phrasal verbs</a></li>
                           
                           <li><a href="http://www.bartleby.com/"> Great books on-line including the American Heritage Dictionary, Fourth Edition, and
                                 Roget’s II: The New Thesaurus</a></li>
                           
                        </ul>
                        
                        <p> All of the Above </p>
                        
                        <ul>
                           
                           <li><a href="http://owl.english.purdue.edu/sitemap.html"> ESL, general writing concerns, and grammar information and activities</a></li>
                           
                        </ul>
                        
                        <p>College Admissions and Scholarship Essays </p>
                        
                        <ul>
                           
                           <li><a href="http://www.collegeadmissionsessays.com/"> This website is devoted entirely to the college admission essays and helping you
                                 write the most effective essays to gain admission into your top-choice colleges</a></li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/links.pcf">©</a>
      </div>
   </body>
</html>