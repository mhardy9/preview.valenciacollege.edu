<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Communications Student Support Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/learning-support/winter-park/communications/cssc_downloadabletutorials.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/learning-support/winter-park/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Communications Student Support Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/learning-support/">Learning Support</a></li>
               <li><a href="/students/learning-support/winter-park/">West Campus</a></li>
               <li><a href="/students/learning-support/winter-park/communications/">Student Support Winter Park</a></li>
               <li>Communications Student Support Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div> 
                                 
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <h3>Writing – Reading – Speech – Study Skills </h3>
                                    
                                    <p><img alt="CSSC Contact Us" height="292" src="CSSC_Banner_000.jpg" width="519"></p>
                                    
                                    
                                    
                                    <h2>Online Resources</h2>
                                    
                                    <p><strong><span>For a quick overview of our services, check out our</span> <a href="http://valenciacollege.edu/wp/cssc/cssc_online_guide.pptx">CSSC Online Brochure</a></strong></p>
                                    
                                    <h2>Downloadable Tutorials </h2>
                                    
                                    <p>Here are a few downloadable workshops, developed the CSSC Staff. Each of our downloadable
                                       tutorials is presented as a PowerPoint presentation. Enjoy!
                                    </p>
                                    
                                    <p><strong>Instructions:</strong></p>
                                    
                                    <ol>
                                       
                                       <li>
                                          
                                          <div><strong> Download and open file</strong></div>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <div><strong>Click "Slide Show" </strong></div>
                                          
                                       </li>
                                       
                                       <li>
                                          
                                          <div><strong> Click "From Beginning" </strong></div>
                                          
                                       </li>
                                       
                                    </ol>
                                    
                                    <p><em>All PowerPoints are created by CSSC Staff </em></p>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                <h4>Types of Writing </h4>
                                             </div>
                                             
                                             <div>
                                                <h4>Grammar Help </h4>
                                             </div>
                                             
                                             <div>
                                                <h4>MLA Help </h4>
                                             </div>
                                             
                                             <div>
                                                <h4>Everything You Need to Know About... </h4>
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                <p><strong><a href="http://valenciacollege.edu/wp/cssc/Narrative101pt2.pptx"> Writing a Narrative</a></strong></p>
                                             </div>
                                             
                                             <div>
                                                
                                                <p><a href="http://valenciacollege.edu/wp/cssc/SV_Agreementpp.pptx"><strong>Subject/Verb Agreement </strong></a></p>
                                                
                                                <p>An interactive PowerPoint</p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><a href="documents/MLAWorkshop.ppt"><strong>MLA Basics Tutorial</strong></a></p>
                                                
                                                <p>An interactive PowerPoint </p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><a href="http://valenciacollege.edu/wp/cssc/ThesisStatementsandTopicSentences.pptx">Thesis Statements &amp; Topic Sentences </a></strong></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong><a href="http://valenciacollege.edu/wp/cssc/ProcessEssays.pptx"><span><em></em></span> Writing a Process Essay</a></strong></p>
                                                
                                                <p><strong>An Interactive PowerPoint</strong> 
                                                </p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><a href="http://valenciacollege.edu/wp/cssc/POWERPOINTFRAGMENTFINAL.pptx"> Fragments</a></strong> 
                                                </p>
                                                
                                                <p><strong>An Interactive PowerPoint</strong> 
                                                </p>
                                                
                                             </div>
                                             
                                             <div><a href="http://valenciacollege.edu/wp/cssc/BLOCKQUOTINGCOMPUTERDIRECTIONSPOWERPOINTCOMPLETED.pptx"><strong>Block Quoting Tutorial </strong></a></div>
                                             
                                             <div>-</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong>Writing an Argumentative Essay</strong></p>
                                                
                                                <p><em><strong>Coming Soon! </strong></em></p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong><a href="http://valenciacollege.edu/wp/cssc/FragmentsPowerPoint.pptx">Fragments 2 </a></strong></p>
                                                
                                                <p><strong>A Guide to Prevent Sentence Fragments </strong></p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><span><em>New! </em></span><a href="http://valenciacollege.edu/wp/cssc/POWERPOINTINCORPORATINGQUOTATIONSPARTI.pptx">Incorporating Quotations I </a></p>
                                                
                                             </div>
                                             
                                             <div>-</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong>Writing an Argumentative Comparison Essay</strong></p>
                                                
                                                <p><em><strong>Coming Soon! </strong></em></p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><strong> <a href="http://valenciacollege.edu/wp/cssc/Run-OnsPowerPoint.pptx">Run-Ons</a></strong></p>
                                                
                                                <p><strong>A Guide to Prevent Run-On Sentences </strong></p>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <p><span><em>New! </em></span><a href="http://valenciacollege.edu/wp/cssc/CSSCINCORPORATINGQUOTATIONSII.pptx">Incorporating Quotations II </a></p>
                                                
                                             </div>
                                             
                                             <div>-</div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div><strong>If you have any suggestions for future tutorials, please <a href="mailto:wpcenglishtutor@mail.valenciacollege.edu">e-mail us</a>! </strong></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <h2>MLA and APA Help </h2>
                                    
                                    <p>Click <a href="../../library/mla-apa-chicago-guides/index.html">here</a> for an excellent resource provided by the Valencia College Library
                                    </p>
                                    
                                    <p><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Contact Us</strong></p>
                                                               
                                                               <p><strong>Call:<br>
                                                                     </strong>407-582-6818
                                                               </p>
                                                               
                                                               <p><strong>Visit:<br>
                                                                     </strong>850 West Morse Blvd, Room 136<br>
                                                                  Winter Park, Fl 32789
                                                               </p>
                                                               
                                                               <p><strong>Current Hours </strong></p>
                                                               
                                                               <div>
                                                                  
                                                                  <div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Monday - Thursday </div>
                                                                        
                                                                        <div>8:00 am - 7:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Friday</div>
                                                                        
                                                                        <div>8:00 am - 12:00 pm </div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div>Saturday</div>
                                                                        
                                                                        <div>CLOSED</div>
                                                                        
                                                                     </div>
                                                                     
                                                                     <div>
                                                                        
                                                                        <div> *Hours are subject to change</div>
                                                                        
                                                                     </div>
                                                                     
                                                                  </div>
                                                                  
                                                               </div>
                                                               
                                                               <p>**Please note: Last consultation is given 30 minutes before closing.</p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <p><strong>Student Tip<br>
                                                                     </strong>Need help with an essay but can't make it into the CSSC? 
                                                               </p>
                                                               
                                                               
                                                               <p>Try our online writing <a href="http://net4.valenciacollege.edu/forms/wp/cssc/writing-consultation.cfm" target="_blank">consultation!</a></p>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div>
                                                   <div><a href="consultations.html"><img alt="menu_consultations" border="0" height="66" src="WritingConsultation.gif" width="236"></a></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="cpt.html"><img alt="menu_pert" border="0" height="66" src="Pert.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                             <div>
                                                
                                                <div><a href="CSSCVideos.html"><img alt="cssc_videos" border="0" height="66" src="CSSC_Videos.gif" width="236"></a></div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div> 
                                 
                                 <div>
                                    
                                    <div>
                                       <img alt="Line" height="6" src="line_002.jpg" width="582"> 
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h2>&nbsp;</h2>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/learning-support/winter-park/communications/cssc_downloadabletutorials.pcf">©</a>
      </div>
   </body>
</html>