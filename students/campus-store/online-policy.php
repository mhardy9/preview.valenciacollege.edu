<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Store | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/campus-store/online-policy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/campus-store/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Store</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/campus-store/">Campus Store</a></li>
               <li>Campus Store</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>Valencia Online Policy</h2>  
                        
                        
                        <p>We think the following information is important. 
                           Please read carefully before placing an order.
                        </p>
                        
                        <p>We try to the best of our abilities to accommodate 
                           your order preferences, whether it is that you want a New or Used book. 
                           Based on the availability at the time we pull the order, we do reserve 
                           the right to substitute a book (New or Used) for your order. We are 
                           sorry for any inconvenience this may cause. 
                        </p>
                        
                        <p>Please select your books from the Campus where you 
                           will attend class. 
                        </p>
                        
                        <p><i>Financial Aid Students</i>: Your order will be 
                           pulled upon availability of your funds. 
                        </p>
                        
                        <p><i>VA</i>, <i>VR</i>, <i>
                              Workforce</i>, etc  students that fall under this group please make 
                           sure your voucher is in place before placing your online order. Your 
                           order will be charged to the account that is in place.
                        </p>
                        
                        <p>Please note that your credit card or 
                           financial aid account will not be charged until we physically pull the 
                           books for your order. The total at the end of the transaction posted 
                           online may not necessarily be the total charged to your credit card or 
                           financial aid account because of the following reasons:  
                        </p>
                        
                        <ul>
                           
                           <li>new/used book substitution made </li>
                           
                           <li>publisher price changes </li>
                           
                           <li>book(s) is/are not available at the time of 
                              pulling your order
                           </li>
                           
                        </ul>
                        
                        <p>Note that this will mean that you will be paying more for your books.</p>
                        
                        <p>Please also note the refund policy for in-store 
                           purchases is also applicable to online orders, meaning the refund 
                           deadline dates are the same and an original receipt is required for all 
                           refund/exchange transactions. Shipping charges are non-refundable. 
                        </p>
                        
                        <p><strong><font color="#ff0000"><u>PLEASE DO NOT ASSUME</u>
                                 </font></strong>your online order is automatically ready for you to pick up 
                           as soon as you submit the order. It is vital for you to provide 
                           up-to-date and correct account information, name, address, phone number 
                           and email address. A confirmation number will also be sent to you via 
                           email. Please print out the confirmation order number email. However, a 
                           confirmation number does not mean that your order is ready to be picked 
                           up, but rather it is a tracking and reference number for your order. 
                           Advanced orders placed one week minimum prior to the start of the 
                           semester should be ready to be picked up during the first week of the 
                           semester. 
                        </p>
                        
                        <p><strong><font color="#FF0000">If the website has not given 
                                 you a confirmation number, your order was not complete.</font></strong></p>
                        
                        
                        
                        
                        
                        <h3>RESTOCKING FEE CHARGE</h3>
                        
                        <p>If you do not pick up your order or have not cancelled your order by 
                           the close of business on Friday the 3rd week of class, you will be refunded your order
                           
                           but charged a $25.00 restocking fee.
                        </p>
                        
                        <p>No early pick-ups.</p>
                        
                        
                        
                        
                        
                        <h3>IMPORTANT INFO</h3>
                        
                        <p><strong>PRICES ARE SUBJECT TO CHANGE WITHOUT NOTICE.&nbsp; PLEASE CHECK ISBN'S 
                              AS PUBLISHERS SOMETIMES SUBSTITUTE WITH DIFFERENT ISBN'S.</strong></p>
                        
                        <p>ISBNs are available on the Web page or come to the bookstore and 
                           check the shelf tags.&nbsp;<strong>We do not give ISBNs over the phone</strong></p>
                        
                        <p>Please note several classes use books that are bundled with WebCT 
                           components, passcodes, etc. That may not be available when ordering 
                           these bundles from other online sources.&nbsp;Please use ISBNs to match the 
                           book or bundle exactly.
                        </p>
                        
                        <p>Please be aware that we will do our best to complete your order.&nbsp; 
                           However, if there are items that have not come in from the publisher, we 
                           will check for them at the time of pick up. If they are available we 
                           will ring them up when you pick up your order.
                        </p>
                        
                        <p>Shipping orders, we will correspond by email concerning the 
                           out of stock items.&nbsp; Please provide a regularly used email address.&nbsp; 
                        </p>
                        
                        
                        
                        
                        
                        <h3>Customer Service email addresses </h3>
                        
                        <ul>
                           
                           <li>
                              <strong>West Campus</strong> - <a href="mailto:westbookstore@valenciacollege.edu">westbookstore@valenciacollege.edu</a>
                              <br>Store Manager, Merlyn Ramdial, <a href="mailto:mramdial@valenciacollege.edu">mramdial@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>
                              <strong>East Campus</strong> - <a href="mailto:eastbookstore@valenciacollege.edu">eastbookstore@valenciacollege.edu</a>
                              <br>Store Manager, Ken Davis, <a href="mailto:kdavis174@valenciacollege.edu">kdavis174@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>
                              <strong>Osceola Campus</strong> - <a href="mailto:oscbookstore@valenciacollege.edu">oscbookstore@valenciacollege.edu</a>
                              <br>Store Manager, TBD, e-mail  <a href="mailto:yfullana@valenciacollege.edu">fullana@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>
                              <strong>Winter Park Campus</strong> - <a href="mailto:wpcbookstore@valenciacollege.edu">wpcbookstore@valenciacollege.edu</a>
                              <br>Store Manager, Lise Moodie, <a href="mailto:lmoodie@valenciacollege.edu">lmoodie@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>
                              <strong>Lake Nona Campus</strong> - <a href="mailto:lncbookstore@valenciacollege.edu">lncbookstore@valenciacollege.edu</a>
                              <br>Store Manager, Rick Jones, <a href="mailto:rjones83@valenciacollege.edu">rjones83@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>
                              <strong>Online Orders</strong> - <a href="mailto:onlineorder@valenciacollege.edu">onlineorder@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>
                              <strong>Campus Store Director</strong> - Yaremis Fullana, <a href="mailto:yfullana@valenciacollege.edu">yfullana@valenciacollege.edu</a>
                              
                           </li>
                           
                        </ul>
                        <br>Please use the above email addresses for questions or inquires about your online orders.
                        Make sure you reference your confirmation order number in your email.
                        
                        
                        
                        
                        <h3>Financial Aid Students </h3>
                        
                        <p>Attention: Before ordering you must authorize your financial aid funds to be released
                           to the bookstore. Please refer to the Financial Aid website for more details. Once
                           you have authorized release of your financial aid, we can fulfill your order online
                           or you can choose to shop in our stores.
                        </p>
                        
                        
                        
                        
                        
                        
                        <p>Florida statue states that the bookstore web site should be available for purchasing
                           45 days prior to the start of classes. We strive to have our sites available  prior
                           to the start of classes for your convenience. 
                        </p>
                        
                        <p><strong>Disclaimer (please read):</strong> Valencia Bookstores provide information on course materials used for courses at Valencia,
                           based on information submitted by instructors and suppliers, and is believed to be
                           correct and current information at the time of publication.  
                        </p>
                        
                        <p>The Valencia Bookstores reserve the right to substitute new books for used, or used
                           books for new, depending on availability. The price will be adjusted accordingly.
                           We cannot guarantee that every book is in stock, or that new and used copies are available
                           for each title listed. Book prices are subject to change by the publisher at any time,
                           and are updated as stock arrives.
                        </p>
                        
                        <p>Be aware that the site will be updated daily as we receive new orders from faculty
                           and shipments from publishers. Please note that because course material information
                           can change at any time (faculty changes, publisher upgrades, cancelled classes, etc.)
                           we are not responsible for information used from this site for the purchase of texts
                           from other sources. Students purchasing materials from Valencia bookstore sites are
                           protected under our returns policies.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div><a href="http://www.valenciabookstores.com/valencc" title="Valencia Campus Store Online">Valencia Campus Store Online</a></div>
                        <br>
                        
                        <div><a href="https://valenciacollege.redshelf.com/" target="_blank" title="Redshelf Digital Textbooks">Redshelf: Digital Textbooks</a></div>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             East Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 120</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2237 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  7:30AM-6:30PM
                                       Friday:  7:30AM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             Lake Nona Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 135</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-7103</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday: 9:00AM-6:30PM
                                       Friday: 9:00AM-1:00PM 
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             Osceola Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 4, Room 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4160 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  8:00AM-6:30PM
                                       Friday:  8:00AM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="www.valenciabookstores.com.html">
                                             Poinciana Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Multipurpose Room</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Temporary Campus Store Closed for the Fall Term. Will Reopen January 1, 2018.<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             West Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 142A</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1471 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  7:30AM-6:30PM
                                       Friday:  7:30PM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             West Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 121</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1166 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  7:30AM-6:30PM
                                       Friday:  7:30PM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             Winter Park Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 101</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6950 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday: 9:00AM-6:30PM
                                       Friday: 9:00AM-1:00PM 
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        <br>
                        
                        
                        <div>
                           
                           <h3>Store Managers:</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>East Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:lcuevas2@valenciacollege.edu">Lydia Cuevas</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>Lake Nona Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:rjones83@valenciacollege.edu">Rick Jones</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>Osceola Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:wrahiem@valenciacollege.edu">Wahida Rahiem</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>West Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:kdavis174@valenciacollege.edu">Ken Davis</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>Winter Park Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:lmoodie@valenciacollege.edu">Lisa Moodie</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>   
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/campus-store/online-policy.pcf">©</a>
      </div>
   </body>
</html>