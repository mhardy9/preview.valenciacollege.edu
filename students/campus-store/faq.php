<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Store | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/campus-store/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/campus-store/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Store</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/campus-store/">Campus Store</a></li>
               <li>Campus Store</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        <h2>FAQs</h2>
                        
                        <p><strong>Why order online?</strong></p>
                        
                        <ul>
                           
                           <li>Avoid long lines</li>
                           
                           <li>Pickup at local campus</li>
                           
                           <li>Have your order sent to the campus most convenient to you </li>
                           
                           <li>No added charges for these services</li>
                           
                           <li>Have them shipped to your home via UPS at a nominal charge </li>
                           
                           <li> 
                              <a href="http://www.valenciabookstores.com/valencc/main/index.asp">Online Bookstore</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p> <strong>Ownership</strong></p>
                        
                        <p>The campus stores are owned and operated by the college.&nbsp; The bookstores 
                           provide services to students, faculty, and staff.
                        </p>
                        
                        <p><strong>Services and Merchandise</strong></p>
                        
                        <p>The main job of the campus store is to provide course materials for classes offered
                           at Valencia. The campus store also provides other items as a convenience to students.
                           
                        </p>
                        
                        <p><strong>Prices</strong></p>
                        
                        <p>Prices are competitively determined but are primarily dependent on the wholesale cost
                           of the items. This is particularly true with course materials/textbooks costs.. 
                        </p>
                        
                        <p><strong>Campus Store Staff</strong></p>
                        
                        <p>The campus store staff is available to assist you and enhance your shopping experience.
                           
                           They will help you identify which books are required for the specific course and instructor
                           you have selected . Campus Store staff and management are 
                           available to answer your questions.
                        </p>
                        
                        <p><strong>Hours </strong></p>
                        
                        <p>Regular campus store hours are Monday-Thursday, 7:30am -6:30pm. and Friday, 7:30 a.m.-3:00
                           p.m. The Winter Park and Osceola campus stores open at 8:00am throughout the year.
                           The campus store observes 
                           extended hours during the first two weeks of Fall and Spring terms. During 
                           the summer terms, Full, A &amp; B, the bookstores, like the College will be open Monday
                           - Thursday, 7:30am -6:30pm and Friday, 7:30am - 12 Noon.
                        </p>
                        
                        <p> The campus stores are open on Saturdays prior to Fall, Spring and Full Summer terms.
                           Please check the campus store splash page on the Valencia web site to see exact dates
                           and times.
                        </p>
                        
                        <p><a href="#top">TOP</a><br>
                           
                        </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div><a href="http://www.valenciabookstores.com/valencc" title="Valencia Campus Store Online">Valencia Campus Store Online</a></div>
                        <br>
                        
                        <div><a href="https://valenciacollege.redshelf.com/" target="_blank" title="Redshelf Digital Textbooks">Redshelf: Digital Textbooks</a></div>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             East Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 120</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2237 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  7:30AM-6:30PM
                                       Friday:  7:30AM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             Lake Nona Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 135</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-7103</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday: 9:00AM-6:30PM
                                       Friday: 9:00AM-1:00PM 
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             Osceola Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 4, Room 103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4160 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  8:00AM-6:30PM
                                       Friday:  8:00AM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="www.valenciabookstores.com.html">
                                             Poinciana Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Multipurpose Room</div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Temporary Campus Store Closed for the Fall Term. Will Reopen January 1, 2018.<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             West Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 142A</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1471 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  7:30AM-6:30PM
                                       Friday:  7:30PM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             West Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 121</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1166 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday:  7:30AM-6:30PM
                                       Friday:  7:30PM-3:00PM
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          
                                          <a href="http://www.valenciabookstores.com/">
                                             Winter Park Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 101</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6950 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Monday-Thursday: 9:00AM-6:30PM
                                       Friday: 9:00AM-1:00PM 
                                       Saturday &amp; Sunday: CLOSED<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        <br>
                        
                        
                        <div>
                           
                           <h3>Store Managers:</h3>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>East Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:lcuevas2@valenciacollege.edu">Lydia Cuevas</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>Lake Nona Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:rjones83@valenciacollege.edu">Rick Jones</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>Osceola Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:wrahiem@valenciacollege.edu">Wahida Rahiem</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>West Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:kdavis174@valenciacollege.edu">Ken Davis</a></div>
                                 </div>
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><strong>Winter Park Campus</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    <div data-old-tag="td"><a href="mailto:lmoodie@valenciacollege.edu">Lisa Moodie</a></div>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>   
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/campus-store/faq.pcf">©</a>
      </div>
   </body>
</html>