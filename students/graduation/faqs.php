<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/graduation/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/graduation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Graduation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/graduation/">Graduation</a></li>
               <li>Graduation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>FAQs</h2>
                        
                        <p>Graduation</p>
                        
                        <ol>
                           
                           <li>
                              <strong>What do I need to do to graduate? <br>
                                 </strong><br>
                              Answer: Students must apply to graduate. The Graduation Application is available 
                              through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account and must be completed and submitted 
                              online.<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>Does it cost anything to graduate?</strong><strong><br>
                                 </strong><br>
                              Answer: There is no Graduation Application Fee. Your Student Activities Fee covers
                              the costs associated with graduation and the commencement ceremony. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>Does the graduation application need to be submitted 
                                 by a certain date? <br>
                                 </strong><br>
                              Answer: Yes, there is a Graduation Application Deadline for each term and your application
                              to graduate must  be submitted through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account. 
                              Please see our <a href="/academics/calendar/">Important Dates &amp; Deadlines</a> calendar for the Graduation Application Deadlines. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>Can a student who is graduating in August attend the 
                                 commencement ceremony of the same year? <br>
                                 </strong><br>
                              Answer: No, the student will have to wait until the next commencement ceremony, 
                              which is held the following year.<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>How do I change my graduation date?</strong><br>
                              <br>
                              Answer: You can change your graduation date by sending an e-mail through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account to the <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a> department. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>How many guests can I invite to graduation and does 
                                 each person (including children) need a ticket? Am I able to get extra tickets? <br>
                                 </strong><br>
                              Answer: 
                              
                              
                              All guests are required to have a ticket to attend the commencement ceremony unless
                              they are less than nine (9) months old and sitting on the lap of an adult. Each graduate
                              will receive six (6) guest tickets. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>Will I receive any information about my graduation status and the commencement ceremony?
                                 <br>
                                 </strong><br>
                              Answer: Yes. 
                              
                              
                              Acknowledgement of your graduation application will be e-mailed to your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account. &nbsp;Students participating in the commencement ceremony will be sent a detailed
                              letter informing them about ceremony related information. <br>
                              
                           </li>
                           
                           <li>
                              <strong>What do I wear for graduation?</strong><br>
                              <br>
                              Answer: The commencement gown is thin, it is recommended that business 
                              attire be worn underneath.<br> 
                              <strong>Women: </strong>Dress or blouse and slacks/skirt. If you're wearing heels, be sure to try them on
                              before the ceremony to ensure they are comfortable. You will be walking on cement
                              floors. Low heeled or flat shoes are highly recommended. <br>
                              <strong>Men:</strong> Dress shirt with slacks (tie optional) and dress shoes (i.e. no sneakers). <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>When will I know if I did everything I need to do for graduation? <br>
                                 </strong><br>
                              Answer: 
                              
                              
                              You can get a degree audit by going to your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account. If, after reviewing the degree audit, you need futher assistance, please
                              stop by the <a href="/students/answer-center/index.html">Answer Center</a>. If you are seeking an Associate in Science or an Associate in Applied Science degree,
                              please see your Career Program Advisor. Your graduation advisor will also be e-mailing
                              you through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account and will let you know what, if anything, you are missing. <br>
                              <br>
                              
                           </li>
                           
                           <li> <strong>What should/shouldn’t I bring to the commencement ceremony?</strong> <br>
                              <br>
                              Answer: 
                              
                              
                              When accepting your diploma, you will need both hands free.&nbsp;Therefore, it is recommended
                              that you <strong>do not carry any items with you into the ceremony (including cell phones, cameras,
                                 purses, books, etc.</strong>).&nbsp; The only thing you should have with you is a picture ID that you can properly
                              secure on your person.<br>
                              <br>
                              
                           </li>
                           
                           <li> <strong>Where should I park for the commencement ceremony and do I need a parking permit?</strong> <br>
                              <br>
                              Answer: You will receive a parking pass that MUST be visibly displayed on your vehicle’s
                              dashboard.&nbsp; You are required to park in the areas specifically designated for graduates.<br>
                              <br>
                              
                           </li>
                           
                           <li>                <strong>Where should I stand to wait until it’s time to go inside the arena?</strong> <br>
                              <br>
                              Answer: You will be required to wait in a specific area prior to entering the arena.&nbsp;
                              Please follow the instructions of the College's Commencement Marshalls and stay in
                              the designated areas  until instructed to enter.<br>
                              <br>
                              
                              
                           </li>
                           
                           <li>                <strong>What can I expect when walking inside the arena? &nbsp;What are the seating procedures?</strong> <br>
                              <br>
                              Answer: There are no specific seat assignments.&nbsp; Once you receive your diploma, you
                              may <strong>not </strong>be returning to the seat you originally sat in.&nbsp; If you brought any personal items
                              with you (<strong>not recommended</strong>), remember to take them with you when you go to receive your diploma.&nbsp; Again, keep
                              in mind that you will need both hands free when you go to receive your diploma.&nbsp; Do
                              not bring anything that may be lost.<br>
                              <br>
                              
                           </li>
                           
                           <li>                <strong>What is the proper way to receive the diploma and the procedure for walking on stage?</strong> <br>
                              <br>
                              Answer:&nbsp; When accepting your diploma, you will need both hands free.&nbsp; You will accept
                              your diploma with your left hand and shake with your right hand simultaneously.&nbsp;&nbsp;
                              Follow the instructions given to you during the Graduation Information Session (usually
                              held in April).<br>
                              <br>
                              
                           </li>
                           
                           <li>                <strong>What are the restroom procedures once seated inside the arena?</strong> <br>
                              <br>
                              Answer: &nbsp;The graduation ceremony varies in length depending on the number of graduates
                              who participate that year. We encourage graduates to stay in their seats throughout
                              the duration of the ceremony, with the exception of when it is time for your entire
                              row to cross the stage to receive your diploma. If you do need to use the restroom
                              during the ceremony, we ask you to quietly and quickly walk to the NEAREST Exit. Please
                              be sure to bring all of your belongings with you. You will be asked, by one of the
                              College's Commencement Marshalls, to turn in your diploma cover until your return
                              to the Arena.&nbsp; When returning to the seats, please be prepared <strong>not</strong> to sit in the seat in which you were originally seated. The graduation staff will
                              direct you to an available seat.<br>
                              <br>
                              <br>
                              
                           </li>
                           
                           <li>                <strong>Where do I tell my guests to meet me after the ceremony?</strong> <br>
                              <br>
                              Answer:&nbsp; It is highly recommended that you make arrangements, prior to the ceremony,
                              to meet with them at a specific location after the ceremony ends so that you can take
                              additional photographs with family members if you wish.<br>
                              <br> 
                              
                           </li>
                           
                           <li>                  
                              
                              <div>
                                 <strong>Will I receive my diploma the day of the commencement?</strong> <br>
                                 <br>
                                 Answer: &nbsp;&nbsp;No. Diplomas are mailed to students approximately 4 – 6 weeks after the
                                 end of the semester in which they graduate.&nbsp; At Commencement, students will receive
                                 a diploma cover with a message from the College until degrees have been certified.
                                 If your graduation date is:<br> 
                                 <strong><br>
                                    December:</strong> Your diploma will be mailed in February<strong><br>
                                    May:</strong> Your diploma will be mailed in June<strong><br>
                                    August:</strong> Your diploma will be mailed in September<br>
                                 <br>
                                 Diplomas are printed and mailed using the diploma name and address we have listed
                                 for you in Atlas. Be sure to update your information in <a href="https://atlas.valenciacollege.edu/">Atlas</a>. If you no longer have access to your Atlas account you may send a written letter
                                 which includes the information listed below. We will not change the name on your diploma
                                 without written authorization from you (including your original signature; therefore,
                                 faxed or e-mailed requests cannot be accepted).<br>
                                 <br>
                                 You may order a replacement diploma by submitting a <a href="graduation-forms.html">Duplicate Diploma Request</a> form which must include the following:<br>
                                 <br>
                                 
                                 <ul>
                                    
                                    <li>Your name</li>
                                    
                                    <li>Your Valencia Identification Number (VID) or Social Security Number (SSN) </li>
                                    
                                    <li>Your Date of Birth </li>
                                    
                                    <li>Your Date of Graduation (Month and Year)</li>
                                    
                                    <li>Address to which replacement diploma should be mailed </li>
                                    
                                    <li>Your signature </li>
                                    
                                    <li>A check or money order in the amount of $15.00 payable to Valencia College for the
                                       Duplicate Diploma Request Fee. 
                                    </li>
                                    
                                 </ul>
                                 <br>
                                 <br>
                                 Send the Duplicate Diploma Request form to:<br>
                                 <br>
                                 Valencia College<br>
                                 Records/Graduation Office<br>
                                 1800 South Kirkman Road<br>
                                 Orlando, FL 32811<br>
                                 <br>
                                 
                              </div>
                              
                           </li>
                           
                           <li>
                              
                              <div>                        <strong>I received a regret letter stating my degree requirements were not met; now what?<br>
                                    <br>
                                    </strong>Answer: The letter will indicate why you did not meet graduation requirements. You
                                 will need to meet with an academic advisor, counselor, or Career Program Advisor for
                                 additional assistance. Once you have met your graduation requirement(s) you will need
                                 to submit a new Graduation Application. Please see our <a href="/academics/calendar/">Important Dates &amp; Deadlines</a> calendar for the Graduation Application deadlines. <br>
                                 <br>
                                 
                              </div>
                              
                           </li>
                           
                           <li>
                              <strong> <strong>When do I request my final official Valencia College transcript showing my date of
                                    graduation and the degree I was awarded? </strong> <br>
                                 </strong><br>
                              Answer: You may request official transcripts through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account at any time. However, make sure to indicate <strong>HOLD FOR DEGREE</strong> if you want your transcript to reflect your degree information.<br>
                              <br>
                              
                           </li>
                           
                        </ol>
                        
                        <p>If you have additional questions or concerns, e-mail the <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a> department. 
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a href="information-sessions.html" class="button-action_outline">Commencement Ceremony Information Sessions</a>
                        <a href="video/" class="button-action_outline">#ValenciaGrad</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           <iframe allowtransparency="true" data-hashtag="ValenciaGrad" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.82c7dfc5ca6196724781971f8af5eca4.en.html#button-hashtag=ValenciaGrad&amp;dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fvalenciacollege.edu%2Fgraduation%2F&amp;related=jasoncosta&amp;size=l&amp;text=I%20am%20a%20%40ValenciaCollege&amp;time=1508361221353&amp;type=hashtag" title="Twitter Tweet Button"></iframe>
                           
                           
                        </div>
                        
                        
                        
                        <p>Many answers to your questions can be found in the online Valencia College <a href="../catalog/index.html">catalog</a>. If you need additional assistance, please e-mail <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a>.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/graduation/faqs.pcf">©</a>
      </div>
   </body>
</html>