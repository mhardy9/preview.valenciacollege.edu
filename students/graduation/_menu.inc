<ul>
	<li><a href="/students/graduation/">Graduation</a></li>

	<li class="submenu"><a class="show-submenu" href="javascript:void(0);">Information <span class="caret"></span></a>
		<ul>
			<li><a href="/students/graduation/commencement-location.php">Commencement Location</a></li>
			<li><a href="/students/graduation/information-sessions.php">Information Sessions</a></li>
			<li><a href="/students/graduation/graduation-forms.php">Forms &amp; Information</a></li>
			<li><a href="/students/graduation/faqs.php">FAQs</a></li>
			<li><a href="/students/graduation/order-gown.php">Order Cap &amp; Gown</a></li>
			<li><a href="/students/graduation/transfer-guarantees.php">Transfer Guarantees</a></li>
			<li><a href="http://net4.valenciacollege.edu/forms/graduation/contact.cfm" target="_blank">Contact Graduation</a></li>
		</ul>
	</li>

	<li class="submenu"><a class="show-submenu" href="/students/graduation/video/">Video Archive <span class="caret"></span></a>
		<ul>
			<li><a href="/students/graduation/video/archive/2016-Commencement.php">2016 Commencement</a></li>
			<li><a href="/students/graduation/video/archive/2015-Commencement.php">2015 Commencement</a></li>
			<li><a href="/students/graduation/video/archive/2014-Commencement.php">2014 Commencement</a></li>
			<li><a href="/students/graduation/video/archive/2013-Commencement.php">2013 Commencement</a></li>
		</ul>
	</li>

</ul>