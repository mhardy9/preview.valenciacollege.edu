<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/graduation/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/graduation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Graduation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Graduation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Accepting applications for Summer 2018</h2>
                        
                        <h3>Application Deadline: Friday, MAY 25, 2018</h3>
                        
                        <h4>Apply online through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account
                        </h4>
                        
                        <p><strong>Before you submit your application for graduation please be sure that you have: </strong></p>
                        
                        <ul>
                           
                           <li>Fulfilled all admissions requirements <strong>prior to the Graduation Application Deadline</strong> (i.e. submitted all official transcripts: high school, college, CLEP scores, Advanced
                              Placement, DANTES, military, etc.);
                           </li>
                           
                           <li>Completed or be registered for all coursework required for your degree. Review your
                              Degree Audit online through your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account (click on the <strong>Registration</strong> tab and in the <strong>Path to Graduation</strong> area click on <strong>My Academic Progress (Degree Audit)</strong> in order to run a degree audit) and/or meet with an advisor to verify that you have
                              met or will meet your degree requirements;
                           </li>
                           
                           <li>Verified that the address in your <a href="https://atlas.valenciacollege.edu/">Atlas</a> account is current and that your Program of Study and catalog year are accurate.
                              If necessary, update your Program of Study and/or catalog year information BEFORE
                              you apply for graduation by submitting a <a href="/admissions/admissions-records/forms.html">Student Records Information Form</a></li>
                           
                        </ul>
                        
                        <p><strong>Log into your Atlas account, click on the Students tab</strong></p>
                        
                        <ul>
                           
                           <li>In the <strong>Path to Graduation</strong> area click on <strong>Graduation Application<br> </strong></li>
                           
                           <li>Complete all fields on the Graduation Application (including the Cap &amp; Gown section
                              if you plan to attend the Commencement Ceremony), review the information, and click
                              <strong>Submit<br> </strong></li>
                           
                           <li>Check <strong><em>Graduation Application Status</em></strong> in Atlas under Path to Graduation to confirm the submission
                           </li>
                           
                        </ul>
                        
                        <p>All communication regarding your graduation status will be via your <a href="https://atlas.valenciacollege.edu/">Atlas</a> email account, therefore; be sure to check it frequently for updates.
                        </p>
                        
                        <h3>Diplomas</h3>
                        
                        <p>Diplomas are mailed to students approximately 4-6 weeks after the end of the semester
                           in which they graduate. At Commencement, students receive a diploma cover with a message
                           from the College.
                        </p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">If you graduate in:</th>
                                 
                                 <th scope="col">Your diploma will be mailed:</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Fall Term (December)</td>
                                 
                                 <td>February</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Spring Term (May)</td>
                                 
                                 <td>June</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Summer Term (August)</td>
                                 
                                 <td>September</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>Please remember to review your Degree Audit before applying for Graduation. You may
                           also see an advisor regarding requirements for your specific program or major. Once
                           you have applied for graduation, check your Atlas account frequently. You will be
                           sent e-mails regarding your graduation status throughout the graduation review process.
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3"><a class="button-action_outline" href="/students/graduation/information-sessions.html">Commencement Ceremony Information Sessions</a> <a class="button-action_outline" href="/students/graduation/video/">#ValenciaGrad</a>
                        
                        <div><iframe title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.82c7dfc5ca6196724781971f8af5eca4.en.html#button-hashtag=ValenciaGrad&amp;dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fvalenciacollege.edu%2Fgraduation%2F&amp;related=jasoncosta&amp;size=l&amp;text=I%20am%20a%20%40ValenciaCollege&amp;time=1508361221353&amp;type=hashtag" width="300" height="150" frameborder="0" scrolling="no" data-hashtag="ValenciaGrad"></iframe></div>
                        
                        <p>Many answers to your questions can be found in the online Valencia College <a href="/students/catalog/index.html">catalog</a>. If you need additional assistance, please e-mail <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a>.
                        </p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/graduation/index.pcf">©</a>
      </div>
   </body>
</html>