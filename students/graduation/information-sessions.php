<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/graduation/information-sessions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/graduation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Graduation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/graduation/">Graduation</a></li>
               <li>Graduation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>GRAD FINALE INFORMATION SESSIONS</h2>
                        
                        <iframe src="http://cdnapi.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/30648821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player_1517959255&amp;entry_id=0_zag1hilu&amp;flashvars[streamerType]=auto" width="560" height="395" frameborder="0" style="width: 560px; height: 395px;" itemprop="video" itemtype="http://schema.org/VideoObject">
                           <span itemprop="name" content="20180124-01GRAD.mov"></span>
                           <span itemprop="description" content="Grad Finale Information Session"></span>
                           <span itemprop="duration" content="274"></span>
                           <span itemprop="thumbnail" content="http://cfvod.kaltura.com/p/1881491/sp/188149100/thumbnail/entry_id/0_zag1hilu/version/100002"></span>
                           <span itemprop="width" content="560"></span>
                           <span itemprop="height" content="395"></span>
                           </iframe>
                        
                        
                        
                        <p>Grad Finale Information Sessions will begin the last week of February. Students who
                           plan to partake in the Commencement Ceremony must attend the Grad Finale and sign
                           up for a <strong><em>mandatory</em></strong> Information Session. <br>
                           <br>
                           You will receive an information packet early February,  via ATLAS e-mail. This packet
                           will outline the events to come and instructions regarding sign up.
                        </p>
                        
                        <p>Grad Finale information Sessions are organized to provide an opportunity to: </p>
                        
                        <ul>
                           
                           <li>Pick up your cap and gown and guest tickets</li>
                           
                           <li>Meet with personnel from various departments, including<strong> Financial Aid, the Alumni Associations, and Graduation</strong>
                              
                           </li>
                           
                           <li>Representatives from<strong> University of Central Florida</strong> will be available to answer your questions 
                           </li>
                           
                           <li>Meet with<strong> Jostens</strong> to purchase
                              
                              <ul>
                                 
                                 <li>Class Rings </li>
                                 
                                 <li>Diploma Frames and more </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p>DATES, TIMES AND LOCATION: </p>
                        
                        <table class="table ">
                           
                           <tr>
                              
                              <th scope="col"><strong>Campus </strong></th>
                              
                              <th scope="col"><strong>Date</strong></th>
                              
                              <th scope="col"><strong>Time </strong></th>
                              
                              <th scope="col"><strong>Information    Session </strong></th>
                              
                           </tr>
                           
                           <tr>
                              
                              <th scope="row"><strong>Winter    Park</strong></th>
                              
                              <td>Tuesday, March 6, 2018</td>
                              
                              <td>3:00 PM - 5:00 PM</td>
                              
                              <td>Building 1-226</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th scope="row"><strong>East</strong></th>
                              
                              <td>Thursday, March 8, 2018</td>
                              
                              <td>1:00 PM - 5:00 PM</td>
                              
                              <td>Building 3-113</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th scope="row"><strong>Poinciana</strong></th>
                              
                              <td>Friday, March 9, 2018</td>
                              
                              <td>1:00 PM - 4:00 PM</td>
                              
                              <td>Building 1-125</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th scope="row"><strong>Lake    Nona</strong></th>
                              
                              <td>Tuesday, March 20, 2018</td>
                              
                              <td>1:00 PM - 4:00 PM</td>
                              
                              <td>Building 1-148</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th scope="row"><strong>Osceola</strong></th>
                              
                              <td>Thursday, March 22, 2018</td>
                              
                              <td>1:00 PM - 5:00 PM</td>
                              
                              <td>Building 4-105</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th scope="row"><strong>West </strong></th>
                              
                              <td>Tuesday, March 27, 2018</td>
                              
                              <td>10:00 AM - 4:00 PM</td>
                              
                              <td>Building 8</td>
                              
                           </tr>
                           
                        </table>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a href="information-sessions.html" class="button-action_outline">Commencement Ceremony Information Sessions</a>
                        <a href="video/" class="button-action_outline">#ValenciaGrad</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           <iframe allowtransparency="true" data-hashtag="ValenciaGrad" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.82c7dfc5ca6196724781971f8af5eca4.en.html#button-hashtag=ValenciaGrad&amp;dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fvalenciacollege.edu%2Fgraduation%2F&amp;related=jasoncosta&amp;size=l&amp;text=I%20am%20a%20%40ValenciaCollege&amp;time=1508361221353&amp;type=hashtag" title="Twitter Tweet Button"></iframe>
                           
                           
                        </div>
                        
                        
                        
                        <p>Many answers to your questions can be found in the online Valencia College <a href="../catalog/index.html">catalog</a>. If you need additional assistance, please e-mail <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a>.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/graduation/information-sessions.pcf">©</a>
      </div>
   </body>
</html>