<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/graduation/video/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/graduation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Graduation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/graduation/">Graduation</a></li>
               <li>Video</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Video Archive</h2>
                        
                        
                        
                        <h3>48th Annual Valencia College Commencement (2017)</h3>
                        
                        <h4>Morning Ceremony - Sunday, May 7, 2017 - 10:00 am </h4>
                        
                        <p>
                           <iframe src="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/30648821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player_1509373310&amp;entry_id=0_n58sik0m&amp;flashvars%5BstreamerType%5D=auto" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                           
                        </p>
                        
                        
                        
                        <h4>Afternoon Ceremony - Sunday, May 7, 2017 - 2:00 pm</h4>
                        
                        <p>
                           <iframe src="https://cdnapisec.kaltura.com/p/1881491/sp/188149100/embedIframeJs/uiconf_id/30648821/partner_id/1881491?iframeembed=true&amp;playerId=kaltura_player_1509373310&amp;entry_id=0_caoq20ik&amp;flashvars%5BstreamerType%5D=auto" allowfullscreen="" webkitallowfullscreen="" mozallowfullscreen="" frameborder="0"></iframe>
                           
                        </p>
                        
                        
                        <p>Choose a Year below to watch the Archived Commencement Video</p>
                        
                        
                        <ul>
                           
                           <li><a href="archive/2016-Commencement.html">2016 Commencement</a></li>
                           
                           <li><a href="archive/2015-Commencement.html">2015 Commencement</a></li>
                           
                           <li><a href="archive/2014-Commencement.html">2014 Commencement</a></li>
                           
                           <li><a href="archive/2013-Commencement.html">2013 Commencement</a></li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <a href="information-sessions.html" class="button-action_outline">Commencement Ceremony Information Sessions</a>
                        <a href="video/" class="button-action_outline">#ValenciaGrad</a>
                        
                        
                        
                        
                        
                        
                        <div>
                           <iframe allowtransparency="true" data-hashtag="ValenciaGrad" frameborder="0" scrolling="no" src="https://platform.twitter.com/widgets/tweet_button.82c7dfc5ca6196724781971f8af5eca4.en.html#button-hashtag=ValenciaGrad&amp;dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fvalenciacollege.edu%2Fgraduation%2F&amp;related=jasoncosta&amp;size=l&amp;text=I%20am%20a%20%40ValenciaCollege&amp;time=1508361221353&amp;type=hashtag" title="Twitter Tweet Button"></iframe>
                           
                           
                        </div>
                        
                        
                        
                        <p>Many answers to your questions can be found in the online Valencia College <a href="../catalog/index.html">catalog</a>. If you need additional assistance, please e-mail <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a>.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/graduation/video/index.pcf">©</a>
      </div>
   </body>
</html>