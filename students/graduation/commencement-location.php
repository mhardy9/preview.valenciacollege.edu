<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graduation | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/graduation/commencement-location.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/graduation/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Graduation</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/graduation/">Graduation</a></li>
               <li>Graduation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h3>Commencement Location</h3>
                        
                        <h3>COMMENCEMENT CEREMONY</h3>
                        
                        <div>
                           
                           <h3>Sunday, May 6, 2018</h3>
                           
                           <h4>University of Central Florida, CFE Arena<br> 12777 Gemini Boulvard, North<br> Orlando, Fl 32816
                           </h4>
                           
                        </div>
                        
                        <h3>New Security policies at The CFE Arena for the Commencement Ceremony</h3>
                        
                        <ul>
                           
                           <li>The University of Central Florida has implemented thorough screenings at the main
                              security checkpoints of the CFE Arena. Everyone passing through a security checkpoint
                              will be required to have a ticket, and will have their bags or person searched.
                           </li>
                           
                           <li>Students will be screened by security staff at the graduate entrance using security
                              wands.
                           </li>
                           
                           <li>Guests will be screened by security staff at the front entrance using security wands.</li>
                           
                           <li>Anyone who bring backpacks or large bags greater than 12'' x 6'' x 12'', will be asked
                              to return them to their car before entering the building.
                           </li>
                           
                           <li>Valencia College is advising students and their guest to arrive early to ensure they
                              clear security for a timely start to the ceremony.
                           </li>
                           
                        </ul>
                        
                        <div>
                           
                           <h3>Morning Ceremony Schedule</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Morning Ceremony Graduates Must Arrive by: 8:40 AM<br><br> Graduates will process into the arena promptly at: 9:40 AM<br><br> Commencement ceremony will began at: 10:00 AM</strong></p>
                                 
                                 <p>Note: Graduates arriving after the processional will not be permitted to process with
                                    the other graduates.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Afternoon Ceremony Schedule</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong> Afternoon Ceremony Graduates Must Arrive by: 12:40 PM<br><br> Graduates will process into the arena promptly at: 1:40 PM<br><br> Commencement ceremony will began at: 2:00 PM</strong></p>
                                 
                                 <p>Note: Graduates arriving after the processional will not be permitted to process with
                                    the other graduates.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Directions</h3>
                        
                        <div><strong>Map of the directions to the CFE Arena: <a href="http://www.cfearena.com/directions.php" target="_blank"> CFE Arena Map</a> </strong></div>
                        
                        <h3>Parking Information</h3>
                        
                        <div>
                           
                           <p><strong>Graduates and their guest should park in parking garage D or H which is across from
                                 the CFE Arena. There will be signs to direct you. If you or your guest have been issued
                                 a state disabled parking permit you will be directed to park on the lower level of
                                 parking garage D.</strong></p>
                           
                           <p><strong>Parking is free, but you must remember to display your parking pass on the dashboard
                                 of your car.</strong></p>
                           
                           <p><a href="/students/graduation/documents/UCF-Parking-Pass-Map-Students-Guests.pdf" target="_blank">Parking Pass</a></p>
                           
                           <p><span>Note</span> The drop off area is only for those who are disabled, elderly and mobility impaired.
                              There are no graduate drop off areas.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3"><a class="button-action_outline" href="/students/graduation/information-sessions.html">Commencement Ceremony Information Sessions</a> <a class="button-action_outline" href="/students/graduation/video/">#ValenciaGrad</a>
                        
                        <div><iframe title="Twitter Tweet Button" src="https://platform.twitter.com/widgets/tweet_button.82c7dfc5ca6196724781971f8af5eca4.en.html#button-hashtag=ValenciaGrad&amp;dnt=false&amp;id=twitter-widget-0&amp;lang=en&amp;original_referer=http%3A%2F%2Fvalenciacollege.edu%2Fgraduation%2F&amp;related=jasoncosta&amp;size=l&amp;text=I%20am%20a%20%40ValenciaCollege&amp;time=1508361221353&amp;type=hashtag" width="300" height="150" frameborder="0" scrolling="no" data-hashtag="ValenciaGrad"></iframe></div>
                        
                        <p>Many answers to your questions can be found in the online Valencia College <a href="/students/catalog/index.html">catalog</a>. If you need additional assistance, please e-mail <a href="mailto:records_graduation@valenciacollege.edu">Records/Graduation</a>.
                        </p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/graduation/commencement-location.pcf">©</a>
      </div>
   </body>
</html>