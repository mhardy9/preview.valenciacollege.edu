<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <meta name="author" content="Valencia College">
      <title>Faculty Mentoring for the Arts | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/faculty-mentors/arts/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/faculty-mentors/arts/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_landing_1" style="background-image: url('/students/faculty-mentors/images/faculty-mentoring-bg.jpg');">
         <div id="intro_txt">
            <h1>Faculty Mentoring for the Arts</h1>
            <nav>
               <div class="page-menu" id="page_menu"></div>
            </nav>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/faculty-mentors/">Faculty Mentors</a></li>
               <li>Arts Mentoring</li>
            </ul>
         </div>
      </div>
      <main role="main">
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-9">
                  <div class="box_style_1">
                     <div class="wrapper_indent">
                        <h2>Why Get a Mentor?</h2>
                        
                        <p><u></u>A Mentor is a trusted ally you can go to for guidance, encouragement, and support
                           throughout your time here at Valencia as an Arts &amp; Entertainment major and beyond,
                           towards a successful artistic career.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </p>
                        
                        <ol>
                           
                           <li>Guidance in developing an educational and personal plan for <br>
                              
                              <ul style="list-style-type: circle;">
                                 
                                 <li>Courses to take</li>
                                 
                                 <li>Skills development</li>
                                 
                                 <li>Solid graduation direction</li>
                                 
                                 <li>College entrance and choices</li>
                                 
                                 <li>Next-step preparation for artistic careers—career readiness</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>To guide you in setting and achieving your goals, short-term and long-term</li>
                           
                           <li>Identify and develop your personal strengths and skills
                              
                              <ul style="list-style-type: circle;">
                                 
                                 <li>Help you find the right “fit” for your talents&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Identify and overcome challenges and obstacles towards success</li>
                           
                           <li>How to develop tools for time management
                              
                              <ul style="list-style-type: circle;">
                                 
                                 <li>Develop a work/school balance</li>
                                 
                                 <li>Manage your stress and ongoing health</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Ongoing and honest assessment of your skills
                              
                              <ul style="list-style-type: circle;">
                                 
                                 <li>One-on-one coaching: through mock auditions, portfolio reviews, and interviews</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Prepare you to get emotionally and mentally ready for the real world of auditions/portfolio
                              review/interviews
                           </li>
                           
                           <li>Help you find opportunities right here at Valencia:
                              
                              <ul style="list-style-type: circle;">
                                 
                                 <li>To develop your craft and gain professional skills through Workshops, internships,
                                    volunteering (i.e., Acting Gym, Film Club, Arts &amp; Entertainment Technology club…)
                                 </li>
                                 
                                 <li>Auditions; portfolio reviews; interviews of faculty &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Help you find opportunities to connect to your local and national artistic community
                              
                              <ul style="list-style-type: circle;">
                                 
                                 <li>Attend workshops, volunteering, internships</li>
                                 
                                 <li>Auditions; portfolio reviews; interviews with community artists and leaders</li>
                                 
                                 <li>Find paid professional opportunities</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ol>
                        
                        <p>&nbsp;</p>
                        
                        <blockquote>
                           
                           <p><span class="main-title_p_ed"><strong>Stats:</strong>&nbsp;&nbsp;&nbsp; While 63 percent of the poll respondents reported having at least one college
                                 professor who made them excited about learning, only 27 percent said they had had
                                 a professor who cared “about [them] as a person,” and only 22 percent had a college
                                 mentor who encouraged their&nbsp; “ goals and dreams.” Graduates who "strongly" agreed
                                 that they had all three of these experiences were <u>twice</u> as likely to have been positively engaged in their post-college work, the poll found.&nbsp;</span></p>
                           
                        </blockquote>
                        
                        <p><img class="img-responsive fullwidth" src="/_resources/images/landing/IMG_0020.jpg" alt="Valencia image description"></p>
                        
                        <h3>Admissions Deadlines</h3>
                        
                        <p>Remember, it takes time to process your admissions application. The earlier you apply,
                           the better! Please visit the online Important Dates &amp; Deadlines calendar for admissions
                           Application Priority Deadlines.
                        </p>
                        
                        <h3>Application Fees</h3>
                        
                        <p>There is a non-refundable application fee of $35.00 for Associate's degree level applicants
                           and for Bachelor's degree level applicants (you must have already completed an Associate's
                           degree in order to apply for admission to a Bachelor's degree program at Valencia).
                           Dual Enrollment students cannot apply online and should visit the Dual Enrollment
                           website for application instructions. Previous Valencia students who have not taken
                           classes here in the past two (2) years will need to reapply for admission and pay
                           the non-refundable $35.00 readmission fee.
                        </p>
                     </div>
                  </div>
               </div>
               <aside class="col-md-3">
                  <p>&nbsp;</p>
                  
                  <div>
                     <h4 class="v-red cap"><strong>Resources</strong></h4>
                     <p>Schedule an appointment with your advisor</p>
                     <div><br><i class="far fa-envelope fa-spacer"></i>????@valenciacollege.edu<br><i class="far fa-phone fa-spacer"></i>407-582-xxxx<br></div>
                  </div>
               </aside>
            </div>
         </div>
         <div class="container-fluid container_gray_bg">
            <div class="container margin_30">
               <div class="row wrapper_indent">
                  <div class="col-md-2 v-red">STEPS TO ENROLL</div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">1</span><span class="step_to_do_text">Do this</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">2</span><span class="step_to_do_text">Then this</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">3</span><span class="step_to_do_text">This is next</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">4</span><span class="step_to_do_text">One more thing</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
                  <div class="col-md-2"><span class="step_to_do_number">5</span><span class="step_to_do_text">And you're done</span><a href="#" target="" class="link_underline text-center mobile_only">more info</a></div>
               </div>
               <div class="row desktop_only">
                  <div class="col-md-2"></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                  <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
               </div>
            </div>
         </div>
         
         <div class="container margin_60">
            <div class="row">
               <div class="col-md-6">
                  <div class="wrapper_indent">
                     
                     <h4 class="v-red cap">Degree Verification</h4>
                     
                     <p>DegreeVerify provides instant online verifications of college degrees or attendance
                        claimed by job applicants. The service is designed to simplify verification for employers,
                        background screening firms, executive search firms and employment agencies, who regularly
                        screen candidates.
                     </p>
                     <strong>Degree Verification Instructions</strong>
                     
                     <p>For Degree Verification go to the National Student Clearinghouse website and click
                        on the VERIFY button to get started.
                     </p>
                     
                     <p>Valencia College submits degree information to the Clearinghouse once per term. There
                        is a fee for Degree Verification; please see the National Student Clearinghouse website
                        for more information.
                     </p>
                     
                  </div>
               </div>
               <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;"><img class="img-responsive fullwidth" src="/_resources/images/landing/IMG_9913.jpg" alt="Students in Computer Lab"></div>
            </div>
         </div>
         
         <div class="container-fluid container_gray_bg">
            <div class="container margin_30">
               <div class="indent_title_in">
                  <h4 class="v-red cap">Florida residency</h4>
               </div>
               <div class="row">
                  <div class="col-md-6">
                     <div class="wrapper_indent"><strong>Reminder</strong>
                        
                        <p>Students who are eligible for Florida residency for tuition purposes pay a much lower
                           tuition rate. See the current rates.
                        </p>
                        
                        <p>For students who fail to submit documentation and still register for classes, tuition
                           will be charged at the out-of-state rate.
                        </p>
                        
                        <p>If your request for Florida Residency and the necessary documentation is not received
                           prior to the Proof of Florida Residency Deadline as listed in the Important Dates
                           and Deadlines calendar, your residency will be processed for the next available term.
                        </p>
                        <strong>Degree Verification Instructions</strong>
                        
                        <p>For Degree Verification go to the National Student Clearinghouse website and click
                           on the VERIFY button to get started.
                        </p>
                        
                        <p>Valencia College submits degree information to the Clearinghouse once per term. There
                           is a fee for Degree Verification; please see the National Student Clearinghouse website
                           for more information.
                        </p>
                        
                     </div>
                  </div>
                  <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;">
                     
                     <p><strong>Dependent vs. Independent Students</strong></p>
                     
                     <p>The determination of dependent or independent student status is important because
                        it is the basis for whether the student has to submit his/her own documentation for
                        residency (as an independent) or his/her parent's or guardian's documentation of residency
                        (as a dependent). Parent means one or both of the parents of a student, any guardian
                        of a student or any person in a parental relationship to a student.
                     </p>
                     
                     <div class="row"><span class="date_stacked col-md-2">09 <span>MAR</span></span><span>Proof of Florida Residency Deadline</span><span><a class="button_outline_small" href="/" target="">submit residency</a></span></div>
                     
                  </div>
               </div>
            </div>
         </div>
         
         <div class="container margin_60">
            <div class="main-title">
               
               <h2>Frequently questions</h2>
               
               <p>Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.</p>
               
            </div>
            
            <div class="row">
               <div class="col-md-4">
                  <div class="box_style_2">
                     
                     <h3>Et ius tota recusabo democritum?</h3>
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     
                     <h3>Posse exerci volutpat has?</h3>
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     
                     <h3>Te pri facete latine salutandi?</h3>
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-md-4">
                  <div class="box_style_2">
                     
                     <h3>Et ius tota recusabo democritum?</h3>
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     
                     <h3>Mediocritatem sea ex, nec id agam?</h3>
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="box_style_2">
                     
                     <h3>Te pri facete latine salutandi?</h3>
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                        Ne his sint graeco detraxit, posse exerci volutpat has in.
                     </p>
                     
                  </div>
               </div>
            </div>
            
         </div>
      </main><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/faculty-mentors/arts/index.pcf">©</a>
      </div>
   </body>
</html>