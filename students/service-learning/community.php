<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Community  | Valencia College</title>
      <meta name="Description" content="Community | Service Learning">
      <meta name="Keywords" content="college, school, educational, service, learning, community">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/community.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/service-learning/">Service Learning</a></li>
               <li>Community </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Community</h2>
                        
                        
                        <h3>Community Partnerships</h3>
                        
                        
                        <p> <img src="/images/students/service-learning/D61_0739.jpg" alt="Gilmore" width="350" height="233" hspace="0" vspace="0" align="default">   
                           <img src="/images/students/service-learning/D61_0762.jpg" alt="Eshma" width="350" height="233" hspace="0" vspace="0" align="default"></p>
                        
                        
                        
                        <p>From left: John Gilmore (GKTW) and Eshma Harry (City Year Orlando) </p>
                        
                        <p>Valencia recognizes its role in providing service to the community. Collaboration
                           between Valencia and community organizations strengthens our community by meeting
                           needs in creative ways. 
                        </p>
                        
                        <strong>Benefits of Service Learning for the Community:</strong>
                        
                        
                        <p>Through service learning, community agencies will</p>
                        
                        
                        <ul>
                           
                           <li>Provide increased direct aid, human interaction, and personal empowerment to people
                              in need.
                           </li>
                           
                           <li>Develop short and long term solutions to pressing community needs.</li>
                           
                           <li>Gain the opportunity to participate in an educational partnership.</li>
                           
                           <li>Enhances  the relationships with the college.</li>
                           
                           <li>Increase positive exposure in the community.</li>
                           
                        </ul>
                        
                        <strong><a href="/documents/students/service-learning/Listing-of-Community-Partners.pdf">Download our Agency Listing of Community Partners</a></strong>
                        
                        
                        <p>Are you a community partner in the Orlando vacinity and not listed on our Agency Listing?
                           If so, please contact the Office of Curriculum Initiatives at <a href="mailto:layalacamacho@valenciacollege.edu">layalacamacho@valenciacollege.edu</a></p>
                        
                        <strong>Memo of Understanding</strong>
                        
                        <p>The <a href="/documents/students/service-learning/MEMOOFUNDERSTANDING--ServiceLearningLBGRevisions2015.pdf">Memo of Understanding</a> is a legal binding document that must be signed be each community partner before
                           the student(s) begin their service project. The completed form should be sent to the
                           Office of Curriculum Initiatives where it will be kept on file for three consecutive
                           years. 
                        </p>
                        
                        
                        <p>Attention: The Office of Curriculum Initiatives</p>
                        
                        <p>Address: 1768 Park Center Drive Orlando, FL 32835</p>
                        
                        <p>Community parnters can also scan and e-mail the signed form to the <a href="mailto:layalacamacho@valenciacollege.edu">layalacamacho@valenciacollege.edu. </a></p>
                        <strong>Project Scopes:</strong>
                        
                        
                        <p>The Project Scope Form provides a framework for service  projects intended for students
                           enrolled in SLS2940 or SLS2940 (H).  The form can be downloaded using the link below
                           and  should be completed by the organization where the project will take place. If
                           you have questions about the form, please contact Leila Ayala Camacho at <a href="mailto:layalacamacho@valenciacollege.edu">layalacamacho@valenciacollege.edu.</a> 
                        </p>
                        
                        <p>Steps for community partners:</p>
                        
                        <ol>
                           
                           <li>Download the form <a href="/documents/students/service-learning/ProjectSopeFormEditableFormat.pdf">here</a> (you may need to  save the form to your desktop before editing) 
                           </li>
                           
                           <li>Complete the form and save it to your desktop </li>
                           
                           <li>E-mail the completed form to the Office of Curriclum Initiatives, Attn. Leila Ayala
                              Camacho, at layalacamacho@valenciacollege.edu
                           </li>
                           
                           <li>Approved forms will be available on the Service Learning webpage</li>
                           
                        </ol>
                        
                        <strong>Course Curriculum: </strong>
                        
                        <p>Students completing SLS2940 or SLS2940H are required to complete the assignments below
                           to fullfill the course requirements. 
                        </p>
                        
                        <strong>SLS2940 &amp; SLS2940H Course Materials</strong>
                        
                        <ol>
                           
                           <li><a href="/documents/students/service-learning/SyllabusServiceLearning.pdf">Course Syllabus</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningDetailedSemesterSchedule.pdf">Detailed Course Schedule</a> 
                           </li>
                           
                           <li><a href="/documents/students/service-learning/Listing-of-Community-Partners.pdf">Listing of Community Partners </a></li>
                           
                           <li><a href="/documents/students/service-learning/StudentAgencyAgreement_002.pdf">Agency Agreement Form</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceHoursLog.pdf">Attendance Sheet</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningGoalSetting.pdf">Goal Setting</a></li>
                           
                           <li><a href="/documents/students/service-learning/Pre-ServiceActivity.pdf">Pre-Service Assignment</a></li>
                           
                           <li><a href="/documents/students/service-learning/PostServiceActivity_004.pdf">Post-Service Assignment</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningJournal.pdf">Journal Assignments</a> and,
                           </li>
                           
                           <li><a href="/documents/students/service-learning/AgencyEvaluation.pdf">Agency Evaluation</a></li>
                           
                        </ol>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>Service Learning Advisory Board</h3>
                        
                        
                        <p>The Service Learning Advisory Board is comprised of college-wide and campus administration,
                           faculty, staff, students, and community partners. The board convienes once a semester
                           to discuss the progress and plans for the strategic goals. The Advisory Board is coordinated
                           through the Office of Curriculum Initiatives.
                        </p>
                        <strong>Strategic Goals</strong>
                        
                        <ol>
                           
                           <li>Goal 1 | Curricular Review and Enhancement</li>
                           
                           <li>Goal 2 | Increase Student Involvement in SLS2940 and SLS2940H Courses</li>
                           
                           <li>Goal 3 | Increase the Number of Courses that Incorporate Service Learning</li>
                           
                           <li>Goal 4 | Expand College and Community Partnerships</li>
                           
                           <li>Goal 5 | Recognize and Celebrate Service Learning Participants</li>
                           
                           <li>Goal 6 | Increase Funding for Service Learning</li>
                           
                           <li>Goal 7 | Establish a Process Between the Director of Curriculum Initiatives and the
                              campus administration for Schedulijng Service Learning Sections and Data Collection
                              
                           </li>
                           
                        </ol>
                        <strong>Past Advisory Board Agenda &amp; Minutes </strong>
                        
                        <ol>
                           
                           <li><a href="/documents/students/service-learning/Service-Learning-Advisory-Board-Minutes-3-30-17.pdf">March, 2017</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardMeetingMinutes10-25-16.pdf">October, 2016 </a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoard5-24Minutes.pdf">May, 2016 </a></li>
                           
                           <li><a href="/documents/students/service-learning/Jan2016ServiceLearningAdvisoryBoardCommitteeMeetingminutes.pdf">January, 2016</a> 
                           </li>
                           
                           <li><a href="/documents/students/service-learning/Sept2015ServiceLearningAdvisoryBoardCommitteeMeeting.pdf">September, 2015 </a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoard5-27-15.pdf">May, 2015 </a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoard1-27-15.pdf">January, 2015</a> 
                           </li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoard9-23-14.pdf">September, 2014 </a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardMeetingMay2014.pdf">May, 2014 </a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardMeetingJanuary2014.docx">January, 2014</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardMeetingSeptember2013.docx">September, 2013</a></li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardMeetingMay2013MINUTES.docx">May, 2013</a> 
                           </li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardJanuary15.docx">January, 2013</a> 
                           </li>
                           
                           <li><a href="/documents/students/service-learning/ServiceLearningAdvisoryBoardMinutesSeptember2012.docx">September, 2012</a> 
                           </li>
                           
                           <li><a href="documents/SVLAdvisoryBoardMeetingMinutesJuly122012.doc">July, 2012 </a></li>
                           
                        </ol> 
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/community.pcf">©</a>
      </div>
   </body>
</html>