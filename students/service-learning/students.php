<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Students  | Valencia College</title>
      <meta name="Description" content="Students | Service Learning">
      <meta name="Keywords" content="college, school, educational, service, learning, students">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/service-learning/">Service Learning</a></li>
               <li>Students </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Students</h2>
                        
                        
                        <div>
                           
                           
                           <h3>Serve Your Community. Earn College Credits</h3>
                           
                           <strong>Benefits of Service Learning </strong>
                           
                           <ul>
                              
                              <li>
                                 <strong>LEARN</strong> through hands-on work in a non-profit community environment.
                              </li>
                              
                              <li>
                                 <strong>EXPLORE</strong> career options, gain work experience and make job contacts.
                              </li>
                              
                              <li>
                                 <strong>DEVELOP</strong> connections with people of diverse cultures and lifestyles.
                              </li>
                              
                              <li>
                                 <strong>IMPROVE</strong> self-confidence by becoming an active citizen in your community.
                              </li>
                              
                              <li>
                                 <strong>EARN</strong> recognition for service projects with a special medallion at graduation.
                              </li>
                              
                              <ul>
                                 
                                 <li>Bronze medallion: 40 service hours<strong></strong>
                                    
                                 </li>
                                 
                                 <li>Silver medallion: 60 service hours<strong></strong>
                                    
                                 </li>
                                 
                                 <li>Gold medallion: 80-100 service hours</li>
                                 
                              </ul>
                              
                           </ul>
                           <strong>Download the Service Learning Brochure<a href="documents/ServiceLearningBrochure_002.pdf"> <em>here</em></a></strong>
                           
                           
                           <hr class="styled_2">                
                           
                           
                           <h3>Service Learning Courses at Valencia</h3>
                           
                           <strong>SLS2940 &amp; SLS2940H</strong>
                           
                           
                           
                           
                           <p>Students receive 1 credit hour for every 20 service hours they complete. Students
                              are expected to meet with their faculty mentor periodically throughout the semester
                              and complete extensive research and reflection on their service experience. 
                           </p>
                           
                           <strong>SLS2940 &amp; SLS2940H Course Materials</strong><p></p>
                           
                           
                           <ol>
                              
                              <li><a href="/documents/students/service-learning/SyllabusServiceLearningforwebpage_000.doc" target="_blank">Course Syllabus</a></li>
                              
                              <li><a href="/documents/students/service-learning/ServiceLearningDetailedSemesterSchedule.pdf" target="_blank">Detailed Course Schedule</a> 
                              </li>
                              
                              <li><a href="/documents/students/service-learning/Listing-of-Community-Partners.pdf" target="_blank">Listing of Community Partners </a></li>
                              
                              <li><a href="/documents/students/service-learning/StudentCommunityPartnerAgreementforwebpage_000.pdf" target="_blank">Student/Community Partner Agreement Form</a></li>
                              
                              <li><a href="/documents/students/service-learning/ServiceHoursLog.pdf" target="_blank">Attendance Sheet</a></li>
                              
                              <li><a href="/documents/students/service-learning/ServiceLearningGoalSetting.pdf" target="_blank">Goal Setting</a></li>
                              
                              <li><a href="/documents/students/service-learning/Pre-ServiceActivity.pdf" target="_blank">Pre-Service Assignment</a></li>
                              
                              <li><a href="/documents/students/service-learning/PostServiceAssignment.pdf" target="_blank">Post-Service Assignment</a></li>
                              
                              <li><a href="/documents/students/service-learning/ServiceLearningJournal.pdf" target="_blank">Journal Assignments</a> and,
                              </li>
                              
                              <li><a href="/documents/students/service-learning/AgencyEvaluation.pdf" target="_blank">Community Partner  Evaluation </a></li>
                              
                           </ol>
                           
                           
                           <strong>SLS2940H</strong>
                           
                           <p>Students receive 1 elective credit for every 20 service hours they complete. Students
                              complete extensive research and reflection on their service experience under the guidance
                              of a trained faculty mentor.
                           </p>
                           
                           <strong>Integrated Classes</strong>
                           
                           
                           <p> Faculty choose to integrate a service project into a traditional course. Students
                              work with their peers to complete a direct, indirect, or advocacy project with a community
                              partner. Students gain service hours on their transcripts without additional college
                              credits . For a list of available courses, please visit the <a href="http://net5.valenciacollege.edu/schedule/"><em>Credit Class Schedule Search</em></a> and check the box for Service Learning here.
                           </p>
                           
                           <strong>Sample Service Projects</strong>
                           
                           <ul>
                              
                              <li>Peace and Justice Ambassadors | PJI offers a Service Learning course in the Fall 2015
                                 term on East Campus. The course supports the Peace and Justice Ambassadors program
                                 and gives students a chance to work closely with the PJI, train and serve as peace
                                 and justice leaders, and earn service learning credit. Ambassadors will meet as a
                                 group on Thursday afternoons from 2:30 – 3:45 pm during the fall semester and will
                                 be involved in all of the PJI events. Click <a href="/documents/students/service-learning/ServiceLearningAd3.pdf">here</a> to view the course flyer. 
                              </li>
                              
                              <li>Give Kids the World (click the document below to get the project overview):
                                 
                                 <ul>
                                    
                                    <li><a href="/documents/students/service-learning/DinoPuttAudioSystemProject_000.pdf">Dino Putt Audio Systems</a></li>
                                    
                                    <li><a href="/documents/students/service-learning/DinoPuttPLCDiagnosticsSystemProjectversion1_1_JJensen_Valenciasubmittal_Fall2013.pdf">Dino putt PLC Diagnostics System</a></li>
                                    
                                    <li><a href="/documents/students/service-learning/EntranceLightingProject_000.pdf">Entrance Lighting Project</a></li>
                                    
                                    <li><a href="/documents/students/service-learning/PersonalizedPaverProject_000.pdf">Personalized Paver</a></li>
                                    
                                    <li><a href="/documents/students/service-learning/PuttPuttHoleSensors_000.pdf">Putt Putt Hole Sensors</a></li>
                                    
                                    <li><a href="/documents/students/service-learning/TunnelRainCurtainProject_000.pdf">Tunnel Rain Curtain     </a></li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Solar Suitcase Technical Project
                                 
                                 <ul>
                                    
                                    <li>Learn how to design and build a “solar suitcase” (portable 12V DC solar power system)
                                       that will be then shipped abroad to be utilized within the Developing World. You will
                                       also be simultaneously mentoring and updating 5th grade students on the progress of
                                       the various design and final testing phases of your solar suitcase under the guidance
                                       of a west campus professor from Valencia’s Division of Engineering, Computer Programming,
                                       and Technology.
                                    </li>
                                    
                                    <li> <a href="ServiceLearningProjects.php">Service Learning Projects</a> 
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Pine Ridge Reservation | In April of 2013, eleven students and four faculty members
                                 (Stacey DiLiberto, Stephanie Frueler, Christie Miller and Michael Robbins) from the
                                 Osceola Campus traveled to the Pine Ridge Indian Reservation in southwest South Dakota
                                 as part of a service learning course within the Seneff Honors College Leadership Track.
                                 
                                 
                                 
                                 The group volunteered their time with RE-MEMBER, a non-profit organization that coordinates
                                 volunteers with the Oglala Lakota Nation on the Pine Ridge Reservation for non-evangelical
                                 outreach projects. Click the link below to hear more about their trip: <a href="http://www.youtube.com/watch?v=mljwLuMQR7U&amp;feature=youtu.be">http://www.youtube.com/watch?v=mljwLuMQR7U&amp;feature=youtu.be</a>
                                 
                              </li>
                              
                              <li>
                                 <a href="/documents/students/service-learning/ProjectSopeFormOrlandoHousing.pdf">Orlando Cloisters Apartments</a> (click on name to view project scope) 
                              </li>
                              
                              <li><a href="https://www.youtube.com/watch?v=s_3baqcO-78" target="_blank">Top 10 Reasons Millenials Should Volunteer with IDignity</a></li>
                              
                           </ul>
                           
                           <strong>International Service Learning</strong>
                           
                           <p>The offices of Curriculum Initiatives and Study Abroad and Global Experiences are
                              excited to now offer International Service Learning trips. Four students will be traveling
                              to a remote part of Spain in the summer 2016 term with Professor Sarah Melanson to
                              partner with elementary schools in the region. In addition, we are in the process
                              of planning trips to Central and/or South America for the 2017 year. More information
                              will be provided on our website soon
                           </p>
                           
                           
                           
                           <hr class="styled_2">   
                           
                           
                           <h4><strong>Project Submission Form</strong></h4>
                           
                           <strong>Project Scopes</strong>
                           
                           <p>The Project Scope Form provides a framework for service projects intended for students
                              enrolled in SLS2940 or SLS2940 (H). The form can be downloaded using the link below
                              and should be completed by the organization where the project will take place. If
                              you have questions about the form, please contact Julianna Burchett at <a href="mailto:v_jburchett@valenciacollege.edu">v_jburchett@valenciacollege.edu</a>. 
                           </p>
                           
                           <p>Steps for community partners:</p>
                           
                           <ol>
                              
                              <li>Download the form <a href="/documents/students/service-learning/ProjectSopeFormEditableFormat.pdf">here</a> 
                              </li>
                              
                              <li>Complete the form and save it to your desktop </li>
                              
                              <li>E-mail the completed form to the Office of Curriclum Initiatives, Attn. Julianna Burchett,
                                 at <a href="mailto:v_jburchett@valenciacollege.edu">v_jburchett@valenciacollege.edu</a>
                                 
                              </li>
                              
                              <li>Approved forms will be available on the Service Learning webpage </li>
                              
                           </ol>
                           
                           <strong>Completed Project Scope Forms</strong>
                           
                           <ol>
                              
                              <li><a href="/documents/students/service-learning/CathedralCloisters.pdf">Cathedral Cloisters, Inc.</a></li>
                              
                           </ol>
                           
                           
                           
                           <hr class="styled_2">
                           
                           <h4><strong>Steps to SERVE</strong></h4>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Thank you for your interest in Service Learning! Students who wish to enroll in SLS2940
                                    or SLS2940H should complete the steps below to receive departmental approval to take
                                    the course. If you are not ready to enroll but would like more information about Service
                                    Learning, please contact the Office of Curriculum Initiatives<em> at <a href="mailto:servicelearning@valenciacollege.edu">servicelearning@valenciacollege.edu</a> </em>for more information about Service Learning opportunities. 
                                 </p>
                                 
                                 <p><strong>Step 1</strong> | Meet the Requirements for Service Learning Participation:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>2.0 GPA or better </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Step 2</strong> | Complete the SLS2940 <a href="http://net4.valenciacollege.edu/forms/servicelearning/serviceApplication.cfm" target="_blank"><em>application</em></a>  by the assigned deadlines:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Fall 2017: August 4, 2017</li>
                                    
                                    <li>Spring 2018: December 1, 2017</li>
                                    
                                    <li>Summer 2018: April 13, 2018</li>
                                    
                                 </ul>
                                 
                                 
                                 
                                 <p><em>NOTE: Applications will be accepted after the deadlines listed above, but placement
                                       with a faculty mentor is NOT guaranteed</em> 
                                 </p>
                                 
                                 
                                 
                                 <p><strong>Step 3</strong> | A representative from the Curriculum Initiatives department will contact you to
                                    confirm your eligibility and placement with a faculty mentor
                                 </p>
                                 
                                 
                                 
                                 <p><em>NOTE: If you do not receive a confirmation email within two business days (M-F, 8am-5pm),
                                       please contact the Office of Curriculum Initiatives at <a href="mailto:servicelearning@valenciacollege.edu">servicelearning@valenciacollege.edu</a> to verify that your application was processed. </em></p>
                                 
                                 
                                 
                                 <p><strong>Step 4</strong> | After you are enrolled in the Service Learning course, you will need to make arrangements
                                    with the Business Office to cover the course tuitition. Please refer to the college's
                                    <a href="../../business-office/tuition-fees.html">Tuition and Fee schedule</a>. 
                                 </p>
                                 
                                 <p><strong>Step 5</strong> | Contact your Faculty Mentor to schedule a meeting during the first week of the
                                    term you are enrolled in the Service Learning course
                                 </p>
                                 
                                 <p><strong>Step 6 </strong> | Complete the <a href="/documents/students/service-learning/StudentAgencyAgreement.pdf"><em>Student/Community Partner Agreement Form</em></a> and have your Faculty Mentor forward it to the Office of Curriculum Initiatives before
                                    the add/drop deadline 
                                 </p>
                                 
                                 <p>If you have questions about the registration process, please contact the Office of
                                    Curriculum Initiatives<em> at <a href="mailto:servicelearning@valenciacollege.edu">servicelearning@valenciacollege.edu</a> </em></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/students.pcf">©</a>
      </div>
   </body>
</html>