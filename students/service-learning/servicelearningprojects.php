<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Projects  | Valencia College</title>
      <meta name="Description" content="Student Projects | Service Learning | Valencia College">
      <meta name="Keywords" content="college, school, educational, service, learning, student, projects">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/servicelearningprojects.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/service-learning/">Service Learning</a></li>
               <li>Student Projects </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           
                           <h2>Service Learning Projects at Valencia</h2>
                           
                           
                           <h4><strong>Some projects that have been incorporated into existing courses include:</strong></h4>
                           
                           <p>Colin Archibald (<strong>Web Design</strong>):&nbsp; Students designed and developed a website for the non-profit agency Greyhound
                              Pets of America.
                           </p>
                           
                           <p>Linda Anthon (<strong>Speech</strong>):&nbsp; Students created videos using proper speech methods to advocate for a non-profit
                              organizations.
                           </p>
                           
                           <p>Erin O’Brien (<strong>English</strong>): Students in ENC 1101 volunteered for 5-10 hours at various non-profits, wrote about
                              their experiences in journals, and researched similar organizations through our library
                              databases and by conducting personal interviews. This Service Learning project resulted
                              in a research paper and presentation on various non-profit organizations and the students
                              recommended ways others could get involved in serving them.
                           </p>
                           
                           <p>Ellen Pastorino (<strong>Psychology</strong>):&nbsp; Students have completed service at Second Harvest Food Bank while exploring the
                              effects of poverty on developmental processes.
                           </p>
                           
                           
                           <p>Nicole Valentino (<strong>English</strong>):&nbsp; Students in ENC 1101 work in teams to plan and manage a donation drive which&nbsp;benefits
                              5&nbsp;non-profit organizations in Orlando. Through this long-term project,&nbsp;students research
                              information about these organizations&nbsp;and share what they have learned&nbsp;to their peers
                              in a presentation. &nbsp;
                           </p>
                           
                           
                           <h4><strong>Some projects that have been done outside of class include:</strong></h4>
                           <strong>Give Kids the World </strong>
                           
                           <p>Deb Hall (<strong>Engineering) </strong>took students to work on the lighting in the theater at Give Kids the World. Students
                              worked side by side with engineers to remove wiring, install lighting, and conceptualize
                              future projects.
                              
                           </p>
                           
                           <p>Students who serve at Give Kids the World have an opportunity to work on a variety
                              of projects throughout the agency, including:
                           </p>
                           
                           <ol>
                              
                              <li><a href="documents/DinoPuttAudioSystemProject.pdf">Dino Putt Audio System Project</a></li>
                              
                              <li><a href="documents/EntranceLightingProject.pdf">Entrance Lighting Project</a></li>
                              
                              <li><a href="documents/PersonalizedPaverProject.pdf">Personalized Paver Project</a></li>
                              
                              <li><a href="documents/PuttPuttHoleSensors.pdf">Putt Putt Hole Sensors</a></li>
                              
                              <li><a href="documents/TunnelRainCurtainProject.pdf">Tunnel Rain Curtain Project</a></li>
                              
                           </ol>            
                           <strong>Habitat for Humanity</strong>
                           
                           <p>Rachel Allen (<strong>Humanities</strong>), Ann Farrell (<strong>Political Science</strong>), Andy Ray (<strong>Architecture</strong>) took students to South Florida to participate in a Habitat for Humanity project.
                           </p>
                           
                           <strong>Coalition for the Homeless</strong>
                           
                           <p>Rachel Allen (<strong>Humanities)</strong> takes students to the Coalition each fall and spring to host a family festival for
                              the homeless families.
                           </p>
                           
                           <strong>Domincan Republic 2008 </strong>
                           
                           <p>Ann Farrell (<strong>Political Science</strong>), Michelle Terrell (<strong>Internships</strong>), Tracy Harrison (<strong>Reading</strong>), Heith Hennel (<strong>Business, IT</strong>), Lana Powell (<strong>Business</strong>), Christie Pickeral (<strong>EAP</strong>) took a group to the Dominican Republic to do an arts &amp; crafts camp with the children
                              of a remote mountain village.
                           </p>
                           
                           <strong>Dominican Republic</strong> <strong>2009</strong>
                           
                           <p>Christie Pickeral (<strong>EAP</strong>) and Lana Powell (<strong>Business</strong>) took a group of students who wrote and published a book about the people of the
                              remote mountain village.&nbsp;They also looked for a cottage industry to benefit the commpunity.
                              Jewlelry was created from the local rocks. Students are selling the book and jewelry
                              to raise funds for the education of the children in the village.  To Read more about
                              this book, click <a href="InOurVillage.html">here. </a></p>
                           
                           <strong>Domincan Republic 2010 </strong>
                           
                           <p>Adrienne Mathews (<strong>Political Science</strong>) and Christie Pickeral (<strong>EAP</strong>) took students to the Dominican Republic to host a math/science camp for the children.
                           </p>
                           <strong>Dominican Republic 2011</strong>
                           
                           <p>Kevin Mulholland (<strong>Humanities) </strong>and Christie Pickeral <strong>(EAP)</strong> took students to the Dominican Republic to host a science camp for the children.
                              See photos <a href="http://www.flickr.com/photos/valencia_sage/sets/72157625033275577/">on Flickr click here.</a></p>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3>Related Links</h3>
                           
                           
                           <ul>
                              
                              <li><a href="../../internship/about-us.html">Internship and Work Services</a></li>
                              
                              <li><a href="../student-development/valencia-volunteers/index.html">Volunteer</a></li>
                              
                           </ul>    
                           
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/servicelearningprojects.pcf">©</a>
      </div>
   </body>
</html>