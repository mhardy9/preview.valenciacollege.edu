<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty  | Valencia College</title>
      <meta name="Description" content="Faculty | Service Learning">
      <meta name="Keywords" content="college, school, educational, service, learning, faculty">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/service-learning/">Service Learning</a></li>
               <li>Faculty </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Faculty</h2>
                        
                        
                        <div>
                           
                           
                           <h3>Welcome, Faculty</h3>
                           
                           
                           <p> <img src="/images/students/service-learning/D61_0710.jpg" alt="Lauri" width="230" height="146" hspace="0" vspace="0" align="default"> 
                              <img src="/images/students/service-learning/D61_0675.jpg" alt="Larry and Angel" width="230" height="146" hspace="0" vspace="0" align="default"> 
                              <img src="/images/students/service-learning/D61_0695.jpg" alt="Deb" width="230" height="146" hspace="0" vspace="0" align="default"> 
                           </p>
                           
                           
                           
                           <p>From left: Lauri Lott, Angel Sanchez, Larry Herndon, Robyn Brighton, and Deb Hall
                              
                           </p>
                           <strong>Benefits of Service Learning for Faculty</strong>
                           
                           <ul>
                              
                              <li>Develop more powerful curricula by providing students with a "real world" context
                                 for theory and discipline-specific knowledge.
                              </li>
                              
                              <li>Provide opportunities to accommodate different learning styles</li>
                              
                              <li>Enhance teaching effectiveness by becoming a "facilitator" rather than a "giver" of
                                 knowledge.
                              </li>
                              
                              <li>Engage and motivate students, leading to student retention in courses.</li>
                              
                              <li>Raise awareness of current social issues as they relate to academic areas of interest.</li>
                              
                              <li>Foster a culture of student participation and leadership on campus</li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           <h3>Getting Involved</h3>
                           
                           <strong>Integrated Service Learning </strong>
                           
                           <p>These are traditional courses that have been integrated with service hours throughout
                              the semester. For example, students enrolled in a web design class can choose to design
                              and implement a website for a non-profit agency. The number of service hours varies
                              by course and are up to the professor’s discretion. Faculty interested in integrated
                              a course with service hours should complete the Faculty Development course: Service
                              Learning Across the Curriculum. 
                           </p>
                           
                           
                           <p><strong>Faculty Development | "Service Learning Across the Curriculum"</strong></p>
                           
                           
                           
                           <p>Faculty are provided a detailed overview of Service Learning and given strategies
                              for integrating their course with service hours. Faculty are awarded 20 professional
                              development hours for successful completion of the course. For available course dates,
                              please visit: <a href="../faculty/development/courseSearch.html">http://valenciacollege.edu/faculty/development/courseSearch.cfm</a>. 
                           </p>
                           
                           
                           <strong>Integrated Course Approval Form</strong>
                           
                           
                           
                           <p>Faculty that have successfully completed Service Learning Across the Curriculum are
                              eligible to teach an integrated Service Learning course. Faculty can download the
                              approval form<em> </em><a href="/documents/students/service-learning/Service-Learning-Integrated-Course-Form.pdf" target="_blank"><em>here</em></a> and have their Dean approve the form before sending it to the office of Curriculum
                              Initiatives.
                           </p>
                           
                           <p>Deadlines for integrated course approval:</p>
                           
                           <ul>
                              
                              <li>Spring: October 2, 2017</li>
                              
                              <li>Summer: February 1, 2018</li>
                              
                              <li>Fall: May 1, 2018</li>
                              
                           </ul>
                           
                           <strong>SLS2940 &amp; SLS2940H Courses </strong>
                           
                           <p>Faculty Mentors periodically meet with students and guide them throughout their service
                              experience. Faculty Mentors can be requested by students or paired with students if
                              they have a level of expertise in the student's major and/or career pathway. If you
                              are interested in becoming a Faculty Mentor, please contact Robyn Brighton at rbrighton1@valenciacollege.edu
                              to complete  a one-time informational session.
                           </p>
                           
                           <strong>SLS2940 &amp; SLS2940H Course Materials</strong>
                           
                           
                           
                           <ol>
                              
                              <li><a href="/documents/students/service-learning/SyllabusServiceLearningforwebpage.doc">Course Syllabus</a></li>
                              
                              <li>
                                 <a href="/documents/students/service-learning/ServiceLearningDetailedSemesterSchedule.pdf">Detailed Course Schedule</a> 
                              </li>
                              
                              <li><a href="/documents/students/service-learning/Listing-of-Community-Partners.pdf">Listing of Community Partners</a></li>
                              
                              <li><a href="/documents/students/service-learning/StudentCommunityPartnerAgreementforwebpage.pdf">Student/Community Partner Agreement Form</a></li>
                              
                              <li><a href="/documents/students/service-learning/ServiceHoursLog.pdf">Attendance Sheet</a></li>
                              
                              <li><a href="/documents/students/service-learning/ServiceLearningGoalSetting.pdf">Goal Setting</a></li>
                              
                              <li><a href="/documents/students/service-learning/Pre-ServiceActivity.pdf">Pre-Service Assignment</a></li>
                              
                              <li><a href="/documents/students/service-learning/PostServiceActivity_002.pdf">Post-Service Assignment</a></li>
                              
                              <li>
                                 <a href="/documents/students/service-learning/ServiceLearningJournal.pdf">Journal Assignments</a> and,
                              </li>
                              
                              <li><a href="/documents/students/service-learning/AgencyEvaluation.pdf">Community Partner  Evaluation</a></li>
                              
                           </ol>
                           
                           <strong>International Service Learning</strong>
                           
                           <p>The offices of Curriculum Initiatives and Study Abroad and Global Experiences are
                              excited to now offer International Service Learning trips. Four students will be traveling
                              to a remote part of Spain in the summer 2016 term with Professor Sarah Melanson to
                              partner with elementary schools in the region. In addition, we are in the process
                              of planning trips to Central and/or South America for the 2017 year. More information
                              will be provided on our website soon.
                           </p>
                           
                           
                           <p>Faculty interested in leading a short-term International Service Learning trip should
                              review the <a href="/documents/students/service-learning/ISLOvierview.pdf">Guidelines</a> document and contact Robyn Brighton at <a href="mailto:rbrighton1@valenciacolleg">rbrighton1@valenciacollege</a>. 
                           </p>
                           
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3>Service Learning &amp; General Education Outcomes</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Cultural and Historical Understanding: </strong>Demonstrate understanding of the diverse traditions of the world, and an individual's
                                    place in it.
                                    
                                 </p>
                                 
                                 
                                 <p>a. International service learning fosters genuine understanding of diverse traditions
                                    of the world.<br>
                                    <br>
                                    b. Local service learning develops understanding of the different cultures in the
                                    area.<br>
                                    <br>
                                    c. Both types of service learning foster civic responsibility to meet the needs of
                                    the global and local community.
                                 </p>
                                 
                                 
                                 
                                 <p><strong> Quantitative and Scientific Reasoning: </strong>Use processes, procedures, data, or evidence to solve problems and make effective
                                    decisions.                
                                 </p>
                                 
                                 
                                 <p>Service Learning includes identifying a community need and through the analysis of
                                    data and evidence to devise a project to meet the needs of the community through problem
                                    solving.
                                 </p>
                                 
                                 
                                 <p><strong>Communication Skills: </strong>Engage in effective interpersonal, oral, written communication.
                                    
                                 </p>
                                 
                                 
                                 <p>Through service learning, students communicate with community partners in both oral
                                    and written form.
                                 </p>
                                 
                                 
                                 <p><strong>Ethical Responsibility: </strong>Demonstrate awareness of personal responsibility in one's civic, social, and academic
                                    life.
                                    
                                 </p>    
                                 
                                 
                                 <p> The foundation of service learning is to invoke the sense of civic and social responsibility.</p>
                                 
                                 
                                 <p><strong>Information Literacy: </strong>Locate, evaluate, and effectively use information from diverse sources.
                                    
                                 </p>
                                 
                                 
                                 <p>Because of the nature of service learning, students use various resources to identify
                                    the nature of the problem.  They use interviews, technology, and print materials in
                                    the research stage of service learning. 
                                 </p>
                                 
                                 
                                 <p><strong>Critical and Creative Thinking: </strong>Effectively analyze, evaluate, synthesize, and apply information and ideas from diverse
                                    sources and disciplines.
                                    
                                 </p>
                                 
                                 
                                 <p>In a service learning project, students meet community needs through the development
                                    of a service project.  In order to identify the problem and possible solutions, students
                                    must analyze, evaluate, synthesize, and apply information and ideas from diverse sources
                                    and disciplines. 
                                 </p>
                                 
                                 
                              </div>
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                     
                     
                     <main role="main">
                        
                        
                        
                     </main>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/faculty.pcf">©</a>
      </div>
   </body>
</html>