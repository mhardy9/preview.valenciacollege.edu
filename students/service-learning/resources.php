<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources  | Valencia College</title>
      <meta name="Description" content="Resources | Service Learning">
      <meta name="Keywords" content="college, school, educational, service, learning, resources">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/service-learning/">Service Learning</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Resources</h2>
                        
                        
                        
                        <div>
                           
                           
                           <h3>General Resources</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>American Association of Community Colleges (AACC)<br><a href="http://www.aacc.nche.edu/Resources/aaccprograms/horizons/Pages/default.aspx">http://www.aacc.nche.edu/Resources/aaccprograms/horizons/Pages/default.aspx</a></p>
                                 
                                 <p>Campus Compact<br><a href="http://www.compact.org/">http://www.compact.org/</a></p>
                                 
                                 <p>Florida Campus Compact<br><a href="http://www.floridacompact.org/">http://www.floridacompact.org/</a></p>
                                 
                                 <p>Learn and Serve<br><a href="http://www.learnandserve.gov/about/service_learning/index.asp">http://www.learnandserve.gov/about/service_learning/index.asp</a></p>
                                 
                                 <p>National Service Learning Clearinghouse<br><a href="http://www.servicelearning.org/">http://www.servicelearning.org/</a></p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Faculty Resources</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Compact Resources including sample projects and syllabi by discipline<br><a href="http://www.compact.org/resources-for-faculty/">http://www.compact.org/resources-for-faculty/</a></p>
                                 
                                 <p>Higher Education Sector--Getting Started<br><a href="http://www.servicelearning.org/hehome/index.php">http://www.servicelearning.org/hehome/index.php</a></p>
                                 
                                 <p>Guidelines for High Quality Syllabi<br><a href="http://www.servicelearning.org/filemanager/download/slice/guidelines_syllabi.pdf">http://www.servicelearning.org/filemanager/download/slice/guidelines_syllabi.pdf</a></p>
                                 
                                 <p>Standards and Indicators for Effective Service Learning Practice <a href="http://www.servicelearning.org/page/index.php?detailed=278">http://www.servicelearning.org/page/index.php?detailed=278</a></p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Local Community Resources</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Hands on Orlando<br><a href="http://www.handsonorlando.com/">http://www.handsonorlando.com/</a></p>
                                 
                                 <p>Volunteer Match<br><a href="http://www.volunteermatch.org/search/">http://www.volunteermatch.org/search/</a></p>
                                 
                                 <p>Volunteer Orlando<br><a href="http://volunteerorlando.com/volunteer/">http://volunteerorlando.com/volunteer/</a></p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/resources.pcf">©</a>
      </div>
   </body>
</html>