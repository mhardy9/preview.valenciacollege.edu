<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Service Learning  | Valencia College</title>
      <meta name="Description" content="Service Learning | Valencia College">
      <meta name="Keywords" content="college, school, educational, service, learning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Service Learning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <p>Service Learning is a teaching and learning strategy that integrates meaningful community
                              service with instruction<strong> </strong>and reflection to enrich the learning experience, teach civic responsibility, and
                              strengthen communities. Service Learning allows students and faculty to blend educational
                              goals with their passions.
                           </p>
                           
                           <p>NEW! Learn more about Service Learning opportunities available at Valencia by watching
                              our <a href="https://youtu.be/RtnYS1qlwog">Video</a>. 
                           </p>
                           <img alt="Service Learning Reception" height="326" hspace="0" src="/images/students/service-learning/D82_1961.jpg" vspace="0" width="490">
                           
                           <p>From left: Steven Patterson, Angel Sanchez, Jennifer Carl, Lindsay Bradley, Mariela
                              Gil Rosario, &amp; Steven Forsyth.
                           </p>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           <h3>Our Mission</h3>
                           
                           
                           <p>
                              Service Learning creates opportunities for Valencia students, faculty, and staff to
                              learn in partnership with the community and to develop competencies that relate to
                              course content and promote the advancement of a socially responsible citizenry. 
                           </p>
                           <strong>Download our Service Learning Brochure <a href="/documents/students/service-learning/ServiceLearningBrochure_001.pdf" target="_blank"><em>here</em></a></strong>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3>Essential components of Service Learning at Valencia</h3>
                           
                           
                           <ul>
                              
                              <li>
                                 <strong>Academics | </strong>Students will improve their higher order thinking skills through analysis and understanding
                                 of complex problems. Students will connect the learning in the classroom to the service
                                 experience. 
                              </li>
                              
                              <li>
                                 <strong>Pre-professional | </strong>Students will connect service to career exploration. Students will analyze their skill
                                 set growth related to potential careers. 
                              </li>
                              
                              <li>
                                 <strong>Civic Engagement | </strong>Students will gain an understanding of the community organization and those it serves.
                                 Students will gain an understanding of their role as an active member of the society.
                                 
                              </li>
                              
                              <li>
                                 <strong>Personal | </strong>Students will experience personal growth through challenges and will develop new skills.
                                 
                              </li>
                              
                              <li>
                                 <strong>Reciprocity</strong> | Each Service Learning experience should benefit students and the community. Students
                                 are encouraged to identify a community agency and complete their service hours in
                                 a way that aligns with their Meta-majors and career pathways. In doing so, students
                                 gain pre-professional experience and college credits while giving back to our community.
                                 
                              </li>
                              
                           </ul>          
                           
                           
                           <hr class="styled_2">                
                           
                           
                           <h3>Service Learning Recognition </h3>
                           
                           
                           <p> Students have the opportunity to earn recognition by completing a service project
                              in an integrated Service Learning course or an independent Service Learning course
                              (SLS2940 and SLS2940H). There are three ways to be recognized at the college and they
                              are outlined below: 
                           </p>
                           
                           <ul>
                              
                              <li>
                                 <strong>Service Learning Lapel Pin</strong>: Each student enrolled in a Service Learning course receives a lapel pin the semester
                                 they participate in the course. Students are encouraged to wear their pins while they
                                 visit with community partners. 
                              </li>
                              
                              <li>
                                 <strong>Transcript Coding</strong>: The Office of Curriculum Initiatives codes student's transcripts with the number
                                 of service hours a student completes. The number of hours coded on the transcripts
                                 is consistent with the service hours connected to the course CRN(s). 
                              </li>
                              
                              <li>
                                 <strong>Service Learning Medallion</strong>: Students that complete a minimum of 40 service hours earn a medallion  to wear at
                                 graduation. Medallions are awarded annually and come in 4 tiers:
                                 
                                 <ul>
                                    
                                    <li>Bronze (40-55 service hours)</li>
                                    
                                    <li>Silver (60-75 service hours)</li>
                                    
                                    <li>Gold (80-95 service hours)</li>
                                    
                                    <li>Gold Plus (100 or more service hours) </li>
                                    
                                 </ul>
                                 
                                 <p><em>Important: If you anticipate receiving a medallion,  review the steps to recieving
                                       graduation regalia below. </em></p>
                                 
                              </li>
                              
                           </ul>
                           
                           <p><strong>Steps to Receiving Graduation Regalia (Service Learning Medallions) </strong></p>
                           
                           <p><strong>Step 1</strong> | Complete a minimum of 40 service hours in a Service Learning designated course
                           </p>
                           
                           <p><strong>Step 2</strong> | Complete an<a href="../graduation/index.php"> Intent to Graduate</a> form before the application deadline 
                           </p>
                           
                           <p><strong>Step 3</strong> | Verify your service hours with the Office of Curriculum Initiatives at <a href="mailto:servicelearning@valenciacollege.edu">servicelearning@valenciacollege.edu</a> 
                           </p>
                           
                           <p><strong>Step 4</strong> | Collect Service Learning regalia one of the following ways:
                           </p>
                           
                           <ul>
                              
                              <li>Attend Annual Night of Distinction Event: Students who are registered to graduate
                                 with the Registrar's Office and have earned a Service Learning medallion will be invited
                                 to the Night of Distinction. 
                              </li>
                              
                              <li>Have the medallion mailed to the student's home address (student must notify the Office
                                 of Curriculum Initiatives)
                              </li>
                              
                           </ul>
                           
                           
                           <p><img src="/images/students/service-learning/D61_0674.jpg" alt="Angel and Larry" width="230" height="146" hspace="0" vspace="0" align="default">  
                              <img src="/images/students/service-learning/D82_1927.jpg" alt="christie with students" width="230" height="146" hspace="0" vspace="0" align="default"> 
                              <img src="/images/students/service-learning/D61_0741.jpg" alt="student" width="230" height="146" hspace="0" vspace="0" align="default"> 
                           </p>   
                           
                           
                           
                           <p>From left: Angel Sanchez, Larry Herndon, Mariela Gil Rosario, John Gilmore, Christie
                              Miller, Steven Patterson, Deb Hall, Wilfredo Ortiz, &amp; Leonard Bass 
                           </p>
                           
                           
                           <hr class="styled_2">   
                           
                           
                           <h3>Contact</h3>
                           
                           
                           <p>
                              <strong>Service Learning is coordinated through the Office of Curriculum Initiatives. For
                                 more information, please contact our office at:</strong>
                              
                           </p>
                           
                           
                           <p>
                              <img src="/images/students/service-learning/BrightonPhoto.jpg" alt="Robin Brighton Photo" align="left">
                              <br><strong>Robyn Brighton</strong><br>
                              Director, Curriculum Initiatives<br>
                              <a href="mailto:rbrighton1@valenciacollege.edu">E-mail</a> | <a href="documents/RobynBrighton.docx">Bio</a><br>
                              407-582-3895
                              
                           </p>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/index.pcf">©</a>
      </div>
   </body>
</html>