<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>In Our Village  | Valencia College</title>
      <meta name="Description" content="In Our Village | Service Learning | Valencia College">
      <meta name="Keywords" content="college, school, educational, service, learning, village">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/service-learning/inourvillage.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/service-learning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Service Learning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/service-learning/">Service Learning</a></li>
               <li>In Our Village </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           
                           
                           <p>In Our Village, La Loma Dominican Republic</p>
                           
                           
                           <p>&nbsp; For the past three summers, Valencia service-learning students have visited the
                              Dominican Republic to host youth summer camps and help improve the living conditions
                              of the families who make up a village called La Loma. They were soon welcomed as members
                              of the community, and began to document their time there with wonderful anecdotes
                              of family meals and musical celebrations. Students also took pictures that offer a
                              glimpse into the community live enriched with love and respect for one another. People
                              from La Loma were curious as to why anyone might want to write about them and how
                              they live. However, what they don’t realize is that they have much to teach all of
                              us about how cooperation and having faith can go a long way in building joyful community
                              relationships. Their story deserves to be told.
                           </p>
                           
                           <p>The pictures and stories have since been used in the completion of a book. The book
                              is called <strong><em>In Our Village: La Loma, Dominican Republic</em></strong><em>.</em> The children of the tightly knit community of La Loma have had a much different childhood
                              than most of us in the U.S., but their dreams for the future share our feelings of
                              hope and optimism. Digna, age 10, wants to be a teacher, so she can help give other
                              children in her village the same opportunities she has had. Frankelina, age 11, wants
                              to be a doctor, so she can help heal people in her village once a hospital is built.
                              Francisca, age 13, wants to become a lawyer, so she can help her community when there
                              is a problem. The one thing that connects the aspirations of all of these ambitious
                              children is their dedication to their families and their community, which is something
                              we can all learn from. 
                           </p>
                           
                           <p>In La Loma, a small community largely isolated from major cities, the simple act of
                              gathering viable drinking water can take several hours. This is why the people must
                              depend on their faith and each other to overcome every challenge they face. The tales
                              the book relates of coming together to do things like building a church and cooking
                              Sunday meals that feed entire families are so inspiring and indeed worth reading about.
                           </p>
                           
                           <p><strong><em>Our Village: La Loma, Dominican Republic</em></strong> is currently available at any Valencia Campus bookstore, or you can contact Christie
                              Pickeral at 407-582-4092 to have it sent to you. All the proceeds from the book will
                              be used to improve the education system in La Loma.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/service-learning/inourvillage.pcf">©</a>
      </div>
   </body>
</html>