<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-development/get-involved/ufit.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-development/get-involved/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-development/">Student Development</a></li>
               <li><a href="/students/student-development/get-involved/">Get Involved</a></li>
               <li>Student Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <article>
                           
                           <h2>UFit</h2>
                           
                           
                           <p>UFit is an intramural sports and fitness program that is committed to improving the
                              overall college health and wellness status at Valencia. The program remains responsive
                              to the wholistic developmental needs of the Valencia College student body by promoting
                              healthy lifestyle habits through educational programming, participatory events/activities
                              and quality support services.
                           </p>
                           
                           <p>To see a list of the activities offered on campus and to sign up, visit <a href="http://www.imleagues.com/spa/intramural/32c0f684c05241a9a7e89b162dafd3b2/home">IMLeagues</a> 
                              
                           </p> 
                           
                           
                        </article>     
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div title="Student Development">
                           
                           <p><em>          Student Empowerment</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <like-box data-border-color="#CCCCCC" data-header="true" data-height="350" data-show-faces="true" data-stream="true" data-width="245" fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;header=true&amp;height=350&amp;href=http%3A%2F%2Fwww.facebook.com%2Fvalenciacollege&amp;locale=en_US&amp;sdk=joey&amp;show_faces=true&amp;stream=true&amp;width=245" fb-xfbml-state="rendered" header="true" href="http://www.facebook.com/valenciacollege" stream="true"></like-box>
                        
                        
                        
                        
                        
                        
                        
                        <center>
                           <iframe alt="Virtual Reality, Virtual Tour" frameborder="0" scrolling="no" src="https://cdn.youvisit.com/tour/Embed/imageFrame?v=2.17.10.170&amp;inst=60307&amp;w=245&amp;h=200&amp;loc=&amp;pl=v&amp;index=0&amp;alpha=0.2&amp;quality=10&amp;legacy=0&amp;debug=&amp;titleshow=hidden&amp;virtualreality=0" title="Virtual Reality, Virtual Tour"></iframe>
                           
                           
                        </center>
                        
                        
                        
                        <center>
                           <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.176789029.1279955482.1460335307" target="_blank">Request a Tour</a> 
                           
                        </center>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-development/get-involved/ufit.pcf">©</a>
      </div>
   </body>
</html>