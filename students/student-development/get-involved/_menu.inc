<ul>
<li><a href="/students/student-development/">Student Development</a></li>
<li><a href="/students/student-development/about/index.php">About Us</a></li>

	
	
<li class="submenu">
<a class="show-submenu" href="/students/student-development/get-involved/index.php">Get Involved <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="/students/student-development/get-involved/clubs-and-organizations.php">Clubs and Organizations</a></li>
<li><a href="/students/student-development/get-involved/student-leadership-series.php">Student Leadership Series</a></li>
<li><a href="/students/student-development/get-involved/student-government-association.php">Student Government Association</a></li>
<li><a href="/students/student-development/get-involved/ufit.php">UFit</a></li>
<li><a href="/students/student-development/get-involved/wellness-ambassadors.php">Wellness Ambassadors</a></li>
<li><a href="/students/student-development/get-involved/upcoming-events.php">Upcoming Events</a></li>
</ul>
</li>
	
<li class="submenu">
<a class="show-submenu" href="/students/student-development/valencia-volunteers/index.php">Valencia Volunteers <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="http://net4.valenciacollege.edu/forms/student-development/valencia-volunteers/volunteers-registration.cfm" target="_blank">Volunteer Registration</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/student-development/valencia-volunteers/volunteers-hour-verification.cfm" target="_blank">Hour Verification</a></li>
</ul>
</li>
	
<li class="submenu">
<a class="show-submenu" href="/students/student-development/clubs/index.php">Room Requests <i aria-hidden="true" class="far fa-angle-down"></i></a>
<ul>
<li><a href="http://net4.valenciacollege.edu/forms/student-development/clubs/room-request.cfm" target="_blank">Clubs &amp; Organizations Room Requests</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/student-development/clubs/outside-organization-room-request.cfm" target="_blank">Outside Organization Request</a></li>
<li><a href="/students/student-development/student-employment.php">Student Employment</a></li>
<li><a href="/students/student-development/locations-information.php">Campus Information</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/student-development/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
</li>
	

<li><a href="http://net4.valenciacollege.edu/forms/student-development/contact.cfm" target="_blank">Contact Us</a></li>
</ul>

