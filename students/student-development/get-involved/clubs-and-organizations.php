<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-development/get-involved/clubs-and-organizations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-development/get-involved/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/student-development/">Student Development</a></li>
               <li><a href="/students/student-development/get-involved/">Get Involved</a></li>
               <li>Student Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <article>
                           
                           <h2>Clubs and Organizations</h2>
                           
                           <p>Interested in joining a club or an organization at Valencia College? Take a look at
                              the current groups on each campus. Click on the letter of the campus (E, LN, O, W,
                              or WP) next to the club or organization and email the advisor for more information.
                              
                           </p>
                           
                           <p>If you do not see a club or an organization that you are interested in joining and
                              want to start your own, review the <a href="documents/StudentDevelopmentNewClubandOrganizationPacket.pdf">New Club Packet</a> to learn more. 
                           </p>
                           
                           <p>Either way, we hope that you will Get Involved! </p>
                           
                           <div> <em>E = East <br>
                                 LN = Lake Nona<br>
                                 O = Osceola<br>
                                 W = West<br>
                                 WP = Winter Park</em>
                              
                           </div>
                           
                           <div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="thead">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">Clubs and Organizations  </div>
                                       
                                       <div data-old-tag="th">E</div>
                                       
                                       <div data-old-tag="th">LN</div>
                                       
                                       <div data-old-tag="th">O</div>
                                       
                                       <div data-old-tag="th">W</div>
                                       
                                       <div data-old-tag="th">WP</div>
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">African-American Cultural Society (A2C2)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:asaintil@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">ALPFA</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:agroccia@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">American Institute of Architecture Students (AIAS)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Art-In Motion Dance Club</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ssalapa@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Arts Guild</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:RCal@valenciacollege.edu%20">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Association of Pre-Medical Students</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Campus Crusade for Christ (CRU)</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ocuan@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Cardiovascular Technology (CVT)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Creatologists</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jmohess@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Culinary Arts Student Association (CASA)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Cyber Security Club</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Debate Club </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:astone28@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Diamond Life</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Dive Club</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:istryker@valenciacollege.edu%20">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Fearless Leaders Stand Out</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:krushing@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Feminist Association of Valencia East </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:mmahaffey@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Florida Engineering Society</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Future Hospitality Leaders of America</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Future Medical Professionals</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ovazque1@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Gay Straight Alliance (GSA)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ejusino2@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:vruiz9@valenciacollege.edu">LN</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jaustin27@valenciacollege.edu%20">O</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Give an Answer</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Guardians of the Galaxy</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Human Empathy Rights Organization (HERO)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Ideas for Valencia</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Institute of Electrical &amp; Electronics Engineers (IEEE)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">International Club</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:mtorres45@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Intervarsity Christian Fellowship</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ahammack@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Japanese Anime Research Society</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:mrobbins9@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Latin American Student Organization (LASO) </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jsalto@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Latin Fire</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Literary Arts Club</div>
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:mwheaton1@valenciacollege.edu">LN</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Model United Nations (MUN)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:skandiah@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Muslim Ambassadors for Peace (MAP)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:yqadri@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Muslim Student Association (MSA)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">National Society of Black Engineers (NSBE)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Phi Beta Lambda (PBL)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:gahmad@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Phi Theta Kappa (PTK)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jtaylor@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:dhollister@valenciacollege.edu">LN</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:mpedone@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Pop Culture Club</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:sfrancis17@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Respiratory Care</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Social Entrepreneurship Club </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jkalakay@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Society of Women Engineers</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Student Association - Dental Hygienist (SADHA)</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Student Government Association (SGA)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:lkaplan3@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:akirby9@valenciacollege.edu">LN</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:nsepulveda@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:sgawppres@valenciacollege.edu">WP</a>&nbsp;
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Student Veterans of America</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:%20ldelagua@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">The Theater Opera Project </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:cdelvillageio@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Theatre and Entertainment Technology Club </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:spasqual@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Bike Club</div>
                                       
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:vwoldman@valenciacollege.edu">WP</a>&nbsp;
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia College Film Production </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ematyas@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Criminal Justice</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Earth Studies Association (VESA)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jadamski1@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia East Book Nerds</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:cmoore71@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Future Educators (VFE)</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:%20llott1@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Hospitality Society (VHS) </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:Nplaca2@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Intercultural Student Association</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Math Club </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:ashaw17@valenicacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Nursing Student Association </div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Psychology Association</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Radiography Student Association</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Technology Club</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:dhunchuck@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Ultrasound Student Association</div>
                                       
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Valencia Voice</div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:rnewman@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">W</div>
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Video Gamers Association (VGA) </div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:Bsage@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Visual Arts Club </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:mgalletta@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Women in STEM </div>
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:jvandecar@valenciacollege.edu">E</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">World Citizens Club</div>
                                       
                                       
                                       
                                       <div data-old-tag="td">
                                          <a href="mailto:tgitto@valenciacollege.edu">O</a>&nbsp;
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div> 
                           
                           
                           
                           
                           
                           
                        </article>     
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div title="Student Development">
                           
                           <p><em>          Student Empowerment</em> 
                           </p>
                           
                        </div>
                        
                        
                        
                        <like-box data-border-color="#CCCCCC" data-header="true" data-height="350" data-show-faces="true" data-stream="true" data-width="245" fb-iframe-plugin-query="app_id=&amp;container_width=0&amp;header=true&amp;height=350&amp;href=http%3A%2F%2Fwww.facebook.com%2Fvalenciacollege&amp;locale=en_US&amp;sdk=joey&amp;show_faces=true&amp;stream=true&amp;width=245" fb-xfbml-state="rendered" header="true" href="http://www.facebook.com/valenciacollege" stream="true"></like-box>
                        
                        
                        
                        
                        
                        
                        
                        <center>
                           <iframe alt="Virtual Reality, Virtual Tour" frameborder="0" scrolling="no" src="https://cdn.youvisit.com/tour/Embed/imageFrame?v=2.17.10.170&amp;inst=60307&amp;w=245&amp;h=200&amp;loc=&amp;pl=v&amp;index=0&amp;alpha=0.2&amp;quality=10&amp;legacy=0&amp;debug=&amp;titleshow=hidden&amp;virtualreality=0" title="Virtual Reality, Virtual Tour"></iframe>
                           
                           
                        </center>
                        
                        
                        
                        <center>
                           <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.176789029.1279955482.1460335307" target="_blank">Request a Tour</a> 
                           
                        </center>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-development/get-involved/clubs-and-organizations.pcf">©</a>
      </div>
   </body>
</html>