<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Development | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-development/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-development/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Development</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Student Development</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <p>Active involvement in events, activities, and organizations outside of class is a
                           key to your future success. Student Development partners with the college's academic
                           curriculum to provide co-curricular and extracurricular experiences that lead to our
                           student's wholistic growth and development.&nbsp;
                        </p>
                        
                        <div>
                           
                           <article>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p><br>Whether it is a club or organization, a work-study position, a leadership series,
                                          or a co-curricular certificate - Student Development invites you to find your interests
                                          and passions here at Valencia.
                                       </p>
                                       
                                       <h3>&nbsp;Helpful Links</h3>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a title="About Us" href="/students/student-development/about/default.html" target="_self">About Us</a></li>
                                    
                                    <li><a title="Get Involved" href="/students/student-development/get-involved/index.html" target="_self">Get Involved </a></li>
                                    
                                    <li><a title="Valencia Volunteers" href="/students/student-development/get-involved/student-government-association.html" target="_self"> Apply for SGA </a></li>
                                    
                                    <li><a title="Student Employment" href="/students/student-development/student-employment.html" target="_self">Student Employment</a></li>
                                    
                                    <li><a title="Upcoming Events" href="/students/student-development/get-involved/upcoming-events.html" target="_self">Upcoming Events</a></li>
                                    
                                    <li><a title="Contact Us" href="http://net4.valenciacollege.edu/forms/student-development/contact.cfm" target="_blank">Contact Us</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>&nbsp;</div>
                                    <br>
                                    
                                    <div>&nbsp;</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </article>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-development/index.pcf">©</a>
      </div>
   </body>
</html>