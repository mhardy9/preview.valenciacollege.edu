<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li>Phoenix</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <p class="enlarge"><strong>WELCOME!&nbsp;</strong>Valencia College's West Campus art and literature magazine, the&nbsp;<strong><em>Phoenix</em></strong>, features art, poetry, and prose from some of Valencia's most talented students.
                     </p>
                     
                     <p class="enlarge v-red"><strong>THE 2017 ISSUE <em>COLLECTIONS</em> IS CURRENTLY AVAILABLE!</strong></p>
                     
                     <p class="enlarge">This special edition of the magazine celebrates Valencia College’s 50<sup>th</sup> Anniversary by reflecting on our ‘collections’ as a community.
                     </p>
                     
                     <p class="enlarge"><strong>This year’s Student Editor, Bruno Sato de Oliveira, writes: </strong></p>
                     
                     <p class="enlarge v-red" style="padding-left: 120px;"><em>“As mental postcards, our experiences, both constructive and undesirable, can be shared
                           and eternalized. Each one of us continues to build a unique collection of memories,
                           experiences, and emotions that shape our lives. Proudly, the collection represents
                           inspiration and cooperation.”</em></p>
                     
                     <hr>
                     
                     <p class="enlarge"><strong>PICK UP YOUR FREE ISSUE IN THE LOBBIES OF MOST WEST CAMPUS BUILDINGS.</strong><br> Can't find one? Contact Professor Jackie Zuromski in 3-143 or by&nbsp;<a href="mailto:jzuromski1@valenciacollege.edu?subject=Looking%20for%20a%20copie%20of%20the%20Phoenix%20Magazine.">email.</a>&nbsp;<br> (On East Campus contact Marcelle Cohen in 2-113.)
                     </p>
                     
                     <hr>
                     
                     <p class="enlarge"><strong>ATTENTION ALL WRITERS AND ARTISTS:&nbsp;GET PUBLISHED! EARN MONEY!</strong></p>
                     
                     <p class="enlarge">Courtesy of Student Development, a&nbsp;<strong>$100 prize&nbsp;</strong>will be awarded the best entry in each of the following four categories: Non-Fiction,
                        Fiction, Poetry, and Art.&nbsp;
                     </p>
                     
                     <p class="enlarge">Take a look at our&nbsp;<a href="http://valenciacollege.edu/Phoenix/awards.cfm">Awards &amp; Recognition</a> page for the winners chosen for publication in our upcoming issue as well as links
                        to previous winners' pages.&nbsp;
                     </p>
                     
                     <p class="enlarge">All submissions to the Phoenix are AUTOMATICALLY entered to win in their respective
                        category.&nbsp; <a href="http://valenciacollege.edu/phoenix/guidelines.cfm">Guidelines and Submissions</a>&nbsp;to submit your entry TODAY!
                     </p>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/index.pcf">©</a>
      </div>
   </body>
</html>