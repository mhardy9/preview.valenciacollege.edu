<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <p><img alt="Cover2016" height="291" src="smPhoenix2016Cover003_002.jpg" width="513"></p>
                        
                        <h2>Awards &amp; Recognition </h2>
                        
                        <p>Just Announced: Editors' Choice Awards for the <em>Phoenix</em> magazine's 20115/16 Issue! <br>
                           Volume XVIII Due out May 2016 
                        </p>
                        
                        <h2><u>Editors' <span>Choice</span> 2015/16 (Volume 18) </u></h2>
                        
                        <h2><strong>Poetry Winner</strong></h2>
                        
                        <h2>
                           <strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Cliff Erickson "These Words</strong>”
                        </h2>
                        
                        <h2><strong>Fiction Winner (tie) </strong></h2>
                        
                        <h2>
                           <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Chris Spagnola </strong>“Gus and Mortimer ” 
                        </h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp; Shumaita Kabir "A First" </strong></h2>
                        
                        <h2><strong>Nonfiction Winner</strong></h2>
                        
                        <h2>
                           <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Christopher Cutillar "The Definition of Us </strong>"
                        </h2>
                        
                        <h2><strong>Art Winners (tie) </strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Andrew Costa "Untitled"</strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Na'ah Gordon "Hana" </strong></h2>
                        
                        <p><strong>$100 per category was awarded courtesy of <a href="../student-development/default.html">Student Development.</a> </strong></p>
                        Winners have been emailed and will be contacted again once the checks are available
                        to be picked up in the Communications Office (5-231).
                        
                        <p><em>The Phoenix </em>2015 (XVII) won top honors in a number of contests! 
                        </p>
                        
                        <p><strong>National ACP Pacemaker Awards</strong> 
                        </p>
                        
                        <p>Since their inception in 1927, ACP’s Pacemaker awards for general excellence have
                           been the most prestigious and competitive awards dedicatedto student media.Winners
                           of the 2015 Magazine Pacemakers was announced at the convention’s awards ceremony
                           Saturday, October 31st. <strong>The Phoenix was awarded the Pacemaker Award in Literary Magazines</strong> (Two-Year). 
                        </p>
                        
                        <p><strong>Florida College System Publications Association Magazine Competition 2015</strong></p>
                        
                        <p><strong><a href="http://www.thefcsaa.com/images/FCSPA/2015%20Magazine%20Book.pdf">http://www.thefcsaa.com/images/FCSPA/2015%20Magazine%20Book.pdf</a> </strong> 
                        </p>
                        
                        <p><strong>Division B—Valencia College Awarded:</strong> 
                        </p>
                        
                        <p><strong>General Excellence Awards</strong><br>
                           First Place:<em>Phoenix</em> – Valencia College 
                        </p>
                        
                        <p><strong>Inner Circle Awards </strong>(Honoring students winning awards in three or more categories.)<br>
                           Jennifer Valero<br>
                           <em>Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Nonfiction</strong><br>
                           First Place—Velaina Koren, <em>Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Art – Individual</strong><br>
                           Third Place—Grace Miller, <em>Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Art Works – Magazine</strong><br>
                           First Place—Grace Miller, Janeza Dina, Madison Duerling,<em> Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Two-Page Spread Design</strong><br>
                           First Place—Phoenix Design Team [Russell “Andy” Hamer],<em> Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Individual Photo</strong><br>
                           Third Place—Rebekah Rigel, <em>Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Design</strong><br>
                           First Place—Phoenix Design Team [Arlianna Oraa, Crissy Matrick, Russell “Andy” Hamer,
                           Yuhai Chung, Joaris Manning &amp; Sean Thompson], and Jennifer Valero,Editor, <em>Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Editing</strong><br>
                           Third Place—Jennifer Valero, <em>Phoenix</em>, Valencia College 
                        </p>
                        
                        <p><strong>Cover</strong><br>
                           First Place—Design Team, <em>Phoenix</em>, Valencia College [Russell “Andy” Hamer] 
                        </p>
                        
                        <p><strong>Contents Page</strong><br>
                           First Place—Design Team and Jennifer Valero, <em>Phoenix</em>, Valencia College [Yuhai Chung] 
                        </p>
                        
                        <p><strong>Staff Page</strong><br>
                           First Place—Design Team, <em>Phoenix</em>, Valenica College [Joaris Manning] 
                        </p>
                        
                        <p><strong>Community College Humanities Association Literary Magazine Competition 2015:</strong> 
                        </p>
                        
                        <p><em>Phoenix </em>magazine was the second place winner, Southern Division, in the annual <br>
                           Community College Humanities Association literary magazine competition. 
                        </p>
                        
                        <hr>            
                        <p>Congratulations to the <em>Phoenix</em> magazine contributors selected "Best in Category" for the 2015 issue:
                        </p>
                        
                        <h2><u>Editors' <span>Choice</span> 2015 (Volume 17) </u></h2>
                        
                        <h2><strong>Poetry Winner</strong></h2>
                        
                        <h2>
                           <strong> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Debbie Rojas "Two Souls in the Night </strong>”
                        </h2>
                        
                        <h2><strong>Fiction Winner</strong></h2>
                        
                        <h2>
                           <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Sydney Nangle </strong>“The Englishman's Guide to Murder” 
                        </h2>
                        
                        <h2><strong>Nonfiction Winner</strong></h2>
                        
                        <h2>
                           <strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Velaina Koren "The Final Exhale</strong>"
                        </h2>
                        
                        <h2><strong>Art Winners (shared) </strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Janeza Marie Dino "Ali" (pencil/colored pencil)</strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Janeza Marie Dino "Our Humanity" (mixed media)</strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grace Miller "Beloved" (acrylic)</strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grace Miller "Madiba" (acrylic)</strong></h2>
                        
                        <h2><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Grace Miller "Dancers" (acrylic)</strong></h2>
                        
                        <p><strong>$100 per category was awarded courtesy of</strong> <a href="../student-development/default.html" target="_blank">Student Development.</a></p>
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards.pcf">©</a>
      </div>
   </body>
</html>