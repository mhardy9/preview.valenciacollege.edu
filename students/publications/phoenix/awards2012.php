<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards2012.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2>Awards &amp; Recognition <span>2012 (Volume 14) </span>
                           
                        </h2>
                        
                        <h2> <img alt="Cover 2012Phoenix " height="341" src="PhoenixCover2012_000.JPG" width="436">
                           
                        </h2>
                        
                        <hr>
                        
                        <h2>Congratulations to the <em>Phoenix</em> magazine contributors selected "Best in Category" for the 2012 issue:
                        </h2>
                        
                        <h2><strong>Poetry</strong></h2>
                        
                        <blockquote>Mary Elizabeth Da Silva,<a href="documents/IHadaDreamLastNight.pdf"> “I Had a Dream Last Night”</a>
                           
                        </blockquote>
                        
                        <h2><strong>Non-Fiction</strong></h2>
                        
                        <blockquote>
                           
                           <p>Christopher Vanderkaay,<a href="documents/HaveYouMettheBaby.pdf"> “Have You Met the Baby?”</a></p>
                           
                        </blockquote>
                        
                        <h2><strong>Fiction</strong></h2>
                        
                        <blockquote> Michael Ruvalcaba,<a href="documents/TheRedBrandofCourage.pdf"> “The Red Brand of Courage”</a>
                           
                        </blockquote>
                        
                        <h2><strong>Art</strong></h2>
                        
                        <blockquote>
                           
                           <p>Alyssa Aviles,<a href="documents/DeathIsSoSly.pdf"> "Death Is So Sly"</a> 
                           </p>
                           
                        </blockquote>
                        
                        <hr>            
                        <p><span>$100 per category was awarded courtesy of <a href="../student-development/default.html" target="_blank">Student Development.</a></span><br>
                           <strong>Winners need to contact the Student Development office </strong>
                           <strong>to arrange payment, which will take several weeks. </strong></p>
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards2012.pcf">©</a>
      </div>
   </body>
</html>