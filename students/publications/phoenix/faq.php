<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2>FAQ</h2>
                        
                        
                        <ol>
                           
                           <li><a href="FAQ.html#01">How many fiction/nonfiction pieces am I allowed to submit?</a></li>
                           
                           <li>
                              <a href="FAQ.html#02">Can I send a picture that I think goes with a poem or a story?</a> 
                           </li>
                           
                           <li><a href="FAQ.html#03">Is a poem allowed to be in a font and/or design of my choosing? I have one about flying
                                 and the poem is written in the shape of a bird.</a></li>
                           
                           <li><a href="FAQ.html#04">What kind of art are you looking for?</a></li>
                           
                           <li><a href="FAQ.html#05">I wonder if my languages choices or subject matter might be offensive-who can I ask?</a></li>
                           
                           <li><a href="FAQ.html#06">I have submitted my work. When will I hear if it is selected?</a></li>
                           
                           <li><a href="FAQ.html#07">I don't have email. Can I submit from the Valencia College library computer?</a></li>
                           
                           <li><a href="FAQ.html#08">My work was selected for publication. Am I automatically entered in the annual FCCPA
                                 contest?</a></li>  
                           
                           <li><a href="FAQ.html#09">Can I submit my poetry, prose and/or art if I am an editor?</a></li>      
                           
                           <li><a href="FAQ.html#10">How are editors selected?</a></li>     
                           
                           <li><a href="FAQ.html#11">How can I find out who won the cash prizes in the annual contest sponsored by West
                                 Campus Student Development?</a></li> 
                           
                           <li><a href="FAQ.html#12">Is the prize for each category always $100?</a></li> 
                           
                           <li><a href="FAQ.html#13">Who chooses the winners for the cash prizes?</a></li> 
                           
                           <li><a href="FAQ.html#14">If my work wins a cash prize, what will Valencia do with my piece-can they use it
                                 for other things?</a></li>
                           
                           <li><a href="FAQ.html#15">My work is copyrighted. Will you put the copyright symbol on my piece(s)?</a></li>     
                           
                           <li><a href="FAQ.html#16">Can I use a penname?</a></li>  
                           
                           <li><a href="FAQ.html#17">Can I change my penname or edit a piece AFTER I have submitted it?</a></li>  
                           
                           <li><a href="FAQ.html#18">My work wasn't selected. What should I do now?</a></li>
                           
                           <li><a href="FAQ.html#19">How will the Phoenix know whether my submission is my own work?</a></li>              
                           
                        </ol>
                        
                        
                        
                        <div>
                           <img alt="Cover2016" height="710" src="Phoenix2016Cover002_000.jpg" width="513"><br>
                           
                           
                        </div>
                        
                        <p><a name="01" id="01"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>1.  How many fiction/nonfiction pieces am I allowed to submit?</p>
                        
                        <p>Those categories are based on word counts-2000 maximum-so a writer can either submit
                           one longer piece in either or both categories, or up to three shorter pieces, as long
                           as the total word count for either genre does not exceed 2000 words per category.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="02" id="02"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>2.  Can I send a picture that I think goes with a poem or a story?</p>
                        
                        <p><em>Phoenix</em> editors choose all art / text pairings. Please submit original art and text separately.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="03" id="03"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>3.  Is a poem allowed to be in a font and/or design of my choosing? I have one about
                           flying and the poem is written in the shape of a bird.
                        </p>
                        
                        <p>Due to layout restrictions, "shape" poems or fancy fonts are usually not done, but
                           that is up to the discretion of the editors and/or designers.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="04" id="04"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>4.  What kind of art are you looking for?</p>
                        
                        <p>The <em>Phoenix</em> is diverse; we  publish photographs as art, plus photos of other artwork such as
                           sculptures, paintings, collages, sketches, and drawings of varied topics and in varied
                           mediums. Please submit up to seven pieces, each separately. Please let us know which
                           medium was used for each entry. (See the <a href="guidelines.html">Guidelines &amp; Submissions</a> page for additional details.) 
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="05" id="05"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>5.  I wonder if my languages choices or subject matter might be offensive-who can
                           I ask?
                        </p>
                        
                        <p>Send submissions questions to the advisor <a href="http://net4.valenciacollege.edu/forms/phoenix/contact.cfm" target="_blank">here</a> 
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="06" id="06"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>6.  I have submitted my work. When will I hear if it is selected? </p>
                        
                        <p>All selections are made by January/February following the submissions period, and
                           a group e-mail is sent to all students whose work has been selected. Students are
                           responsible for monitoring their e-mail accounts and responding to any additional
                           requests, or their work may not be used. 
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="07" id="07"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>7.  I don't have email. Can I submit from the Valencia College library computer? </p>
                        
                        <p>All Valencia College students have <a href="https://atlas.valenciacollege.edu">Atlas</a> email; please use that as your default. Any email sent from a general location will
                           not be able to receive the return "receipt" for the submission(s). 
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="08" id="08"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>8.  My work was selected for publication. Am I automatically entered in the annual
                           FCCPA contest?
                        </p>
                        
                        <p>No--from all submissions, the advisor selects the specific pieces to enter in each
                           category. Any winners from the state contest will be contacted during the fall following
                           the original submissions to the Phoenix. (i.e. one full year to wait for that one!)
                           The list of FCCPA winners will be posted on the site each year, with individual winners
                           being contacted by the advisor to claim their certificates.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="09" id="09"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>9.  Can I submit my poetry, prose and/or art if I am an editor?</p>
                        
                        <p>Editors MAY submit to the magazine following standard criteria, since all pieces are
                           selected anonymously, but editors are NOT eligible to win cash prizes. (The Editor-in-Chief
                           may not submit work for consideration due to the nature of the position.)
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="10" id="10"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>10.  How are editors and other staff selected?</p>
                        
                        <p>Poetry, fiction, and nonfiction editors responsible for choosing content that will
                           appear in the magazine are students enrolled in the fall course, ENC 2341, Advanced
                           Creative Writing/Literary Magazine.
                        </p>
                        
                        <p>Graphic Designers who create the layout and design of the magazine are students enrolled
                           in the spring course, GRA 2930, Special Topics: The Phoenix Design Project.
                        </p>
                        
                        <p>The Editor-in Chief is selected by the faculty advisor for the <em>Phoenix</em>, usually from outstanding previous magazine editors. <br>
                           <br>
                           Copy editors are people who profess a love and skill for the job-these students may
                           volunteer in class.
                        </p>
                        
                        <p>For additional details regarding prerequisites and other requirements or if you have
                           further questions, please visit the <em><a href="apply.html">Phoenix</a></em><a href="apply.html"> Positions</a> page. 
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="11" id="11"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>11.  How can I find out who won the cash prizes in the annual contest sponsored by
                           West Campus Student Development?
                        </p>
                        
                        <p>All cash winners will be posted under <a href="awards.html">Awards</a> each January. All winners must check the website to see if they have won (we will
                           no longer send individual letters or emails), and then each winner must contact  Student
                           Development, West, 3-147 to arrange their payments. Any winner who neglects to contact
                           Student Development and collect his or her cash prize by the end of spring semester
                           forfeits the cash prize, and the money returns to student development funds.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="12" id="12"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>12.  Is the prize for each category always $100?</p>
                        
                        <p>We have been lucky that Student Development continues to sponsor all four genres of
                           cash prizes, for a TOTAL of $400 (by the way, this money is not guaranteed to be available
                           every year!). This amount means $100 per genre, but we have had ties, where two students
                           have split the $100 and won $50 each.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="13" id="13"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>13. Who chooses the winners for the cash prizes?</p>
                        
                        <p>The winners are chosen by the student editors working on the <em>Phoenix</em> each fall.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="14" id="14"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>14. If my work wins a cash prize, what will Valencia do with my piece-can they use
                           it for other things?
                        </p>
                        
                        <p>All winners will be published in the next <em>Phoenix</em>. Additionally, all winning pieces will be available on the awards page once the magazine
                           has been printed. Advertisement materials for a given issue may use text or images
                           from that issue.
                        </p>
                        
                        
                        <p><a name="15" id="15"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>15. My work is copyrighted. Will you put the copyright symbol on my piece(s)?</p>
                        
                        <p>Valencia's policy is that ALL student writers and artists retain their individual
                           copyrights, and a statement indicating such appears in the published magazine. Therefore,
                           no individual copyright symbols will be used.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="16" id="16"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>16. Can I use a penname?</p>
                        
                        <p>Yes. There is a spot for that on the submissions form. Any penname given on that form
                           will be used for publication purposes.
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="17" id="17"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>17. Can I change my penname or edit a piece AFTER I have submitted it?</p>
                        
                        <p>Unfortunately, we have too many contributors to allow changes, so please be sure to
                           submit the correct information. Any work chosen will be edited for correctness and/or
                           length by <em>Phoenix</em> editors.
                        </p>            
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        <p><a name="18" id="18"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>18. My work wasn't selected. What should I do now?</p>
                        
                        <p>Plan on submitting again next year! Just because a piece may not work for a given
                           issue does NOT mean it has no merit. Perhaps revise and try again, or submit new work.
                           Occasionally, other departments or campus publications ask the <em>Phoenix</em> for unused pieces with merit, and we have "shared" contributors' work for display
                           and/or publication elsewhere on campus. If this happens, the contributor is contacted.
                        </p>            
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        
                        <p><a name="19" id="19"></a></p>
                        
                        <hr size="1" width="75%">
                        <br>
                        
                        <p>19. How will the Phoenix know whether my submission is my own work?</p>  All texts and images will be scanned with anti-plagiarism software, but students
                        are responsible for knowing the rules, and all students will be required to agree
                        to the following statement before a submission is possible:
                        
                        <p>
                           "I certify that the work I am submitting is my own original work and to the extent
                           I have used material from another source, that source has been appropriately acknowledged.
                           I understand and acknowledge that submitting another's work may constitute a violation
                           of the Student Code of Conduct and may result in disciplinary action from the College."
                           
                           
                        </p>
                        
                        <p><a href="FAQ.html#top">TOP</a></p>
                        
                        
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        <img alt="Poster Info Only" border="4" height="327" src="PosterInfoOnly_000.jpg" width="582">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/faq.pcf">©</a>
      </div>
   </body>
</html>