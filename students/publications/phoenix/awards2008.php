<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards2008.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2>Awards</h2>
                        
                        
                        <p>Valencia proudly congratulates the winners of the 2008 Florida Community College Publication
                           Association contest. 
                        </p>
                        
                        
                        <p>Click to view a sample PDF. </p>
                        
                        
                        <p>To download a list of all winners, <a href="docs/08_Winners.pdf" target="_blank" title="Click to view PDF">click here</a></p>
                        
                        
                        <p><strong>Division B, Second Place</strong> <br>
                           <em>Phoenix</em> ï¿½ Valencia College (15 Points)
                        </p>
                        
                        
                        <p><strong>Poem</strong><br>
                           Second Place:<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Irina Nicula, Park Ave.
                        </p>
                        
                        
                        <p><strong>Poetry</strong> <br>
                           Third Place (Tie): <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Parris Baker, Paint?!<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Katelyn Kustura, My Words<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cara McGarian, Mares of the Night 
                        </p>
                        
                        
                        <p><strong>Fiction</strong> <br>
                           First Place: <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Pat Lee, <a href="docs/Phoenix08_TommysStory.pdf" target="_blank" title="Click to view PDF">Tommy's Story</a></p>
                        
                        
                        <p><strong>Illustration w/ Text</strong> <br>
                           Second Place: <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jeremy Veverka, <a href="docs/Phoenix08_Gaddani.pdf" target="_blank" title="Click to view PDF">Gaddani</a></p>
                        
                        
                        <p><strong>Illustrations w/ Text</strong> <br>
                           Third Place: <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Victor Lopez, Sun Up <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Danielle Malambri, Seeing Red<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Gail Peck, Every Picture Tells a Story
                        </p>
                        
                        
                        <p><strong>Photo</strong> <br>
                           First Place: <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jeremy Veverka, <a href="docs/Phoenix08_Gaddani.pdf" target="_blank" title="Click to view PDF">Gaddani</a></p>
                        
                        
                        <p><strong>Photography</strong> <br>
                           First Place: <br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Victor Lopez, Sun Up<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Danielle Malambri, Seeing Red<br>
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Jeremy Veverka, <a href="docs/Phoenix08_Gaddani.pdf" target="_blank" title="Click to view PDF">Gaddani</a> 
                        </p>
                        
                        
                        
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards2008.pcf">©</a>
      </div>
   </body>
</html>