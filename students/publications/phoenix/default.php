<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="index.html"></a>
                        
                        <hr>
                        
                        <h3>
                           <span><strong><em>PHOENIX</em> sweeps the map with regional, state, and national awards! </strong></span>Click <a href="awards.html#PhoenixHonors">here</a> for details! 
                        </h3>
                        
                        <h3>CONGRATULATIONS! to our latest Editors' Choice winners! Visit our <strong> <a href="awards.html">Awards &amp; Recognition</a> page for more! </strong>
                           
                        </h3>
                        
                        <div>
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><img alt="Cover2016" height="291" src="smPhoenix2016Cover003.jpg" width="513"><br>
                                       <br>
                                       <strong>The 2016 issue is currently available!<br>
                                          </strong>Pick up your FREE issue in the lobbies of most West Campus buildings.<br>
                                       Can't find one? Contact Professor Jackie Zuromski in 3-143 or by <a href="mailto:jzuromski1@valenciacollege.edu">email.</a> <br>
                                       (On East Campus contact Marcelle Cohen in 2-113.)
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <p><strong>CORRECTION:<br>
                                                      </strong>The poem "Straightjacket" on page 46 of our 2015 print issue was incorrectly attributed.
                                                   This poem was written by award-winning  student poet <strong>Chantall Roberts</strong>. 
                                                </p>
                                                
                                                <p>In addition to being presented here, "Straitjacket" will appear (with correct attribution)
                                                   in the next issue of <em>The Phoenix</em> as well. <strong><br>
                                                      </strong></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div><img alt="CORRECTION" height="384" src="CORRECTION.jpg" width="582"></div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>             
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Welcome!</p>
                                    
                                    <p>Valencia College's West Campus art and literature magazine, the <strong><em>Phoenix</em></strong>, features art, poetry, and prose from some of Valencia's most talented students.
                                    </p>
                                    
                                    <h3>
                                       <strong><strong>Attention All Writers and Artists:</strong><br>
                                          Get PUBLISHED! Earn MONEY!</strong> 
                                    </h3>
                                    
                                    <p>Courtesy of Student Development, a <span>$100 prize</span> will be awarded the best entry in each of the following four categories: Non-Fiction,
                                       Fiction, Poetry, and Art. 
                                    </p>
                                    
                                    <p><strong>Take a look at our <a href="awards.html">Awards &amp; Recognition</a> page for the winners chosen for publication in our upcoming issue as well as links
                                          to previous winners' pages. </strong></p>
                                    
                                    <p>All submissions to the Phoenix are AUTOMATICALLY entered to win in their respective
                                       category<em>.</em>               <strong><a href="guidelines.html">Click here</a> for guidelines and to submit your entry TODAY! </strong></p>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 <div>
                                    
                                    <hr>
                                    
                                    <p><strong>Get Involved</strong><br>
                                       Would you like to be a part of the <em>Phoenix</em> team?<br>
                                       <a href="apply.html">Click Here</a></p>
                                    
                                 </div>
                              </div>
                              
                           </div>
                        </div>
                        
                        
                        
                        <h3>LET US SEE WHAT YOU'VE GOT TO OFFER! </h3>
                        
                        <h3>The <em>Phoenix</em> wants to publish YOUR creative work! We are now accepting fiction, non-fiction, poetry,
                           and art submissions for the 2016/17 issue. Visit the <a href="guidelines.html">Guidelines &amp; Submissions</a> page for the submission deadlines for each category  . (Any entries received after
                           the deadlines will be held and <u>may</u> be considered for the NEXT ISSUE.)
                        </h3>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/default.pcf">©</a>
      </div>
   </body>
</html>