<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards2010.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2>Awards &amp; Recognition </h2>
                        
                        <p><strong>Valencia College proudly congratulates our winners of the</strong> <strong>2010 Florida Community College Press Association Magazine Competition</strong>.
                        </p>
                        
                        <h2>
                           <strong>General Excellence:</strong> <span>First Place</span>
                           
                        </h2>
                        
                        <h2><strong>Valencia College, <em>Phoenix</em> magazine</strong></h2>
                        
                        <h2>Individual Excellence Awards to Valencia Students </h2>
                        
                        <h2>
                           <strong>Poem:</strong> <span>First Place</span> 
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_poetry_Ryan_Patton.pdf" target="_blank">"Ghost"</a> by Ryan Patton
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Poetry:</strong> <span>Third place</span>
                           
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_poetry_Mark_Alton.pdf" target="_blank">"Independence Day"</a> by Mark Alton<br>
                           <a href="docs/PHOENIX10/PhoenixXII_poetry_Chris_Garcia_Where_Went_My_Forrest.pdf" target="_blank">"Where Went my Forest?"</a> by Chris Garcia<br>
                           <a href="docs/PHOENIX10/PhoenixXII_poetry_Ryan_Patton_Gentlemen.pdf" target="_blank">"Gentlemen"</a> by Ryan Patton
                        </blockquote>
                        
                        
                        <h2> <strong>Fiction:</strong> <span>Third place</span>
                           
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_fiction_Rania_Elbohy.pdf" target="_blank">"Starry Night"</a> by Rania Elbohy
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Illustration with Text, Individual:</strong> <span>Second Place</span>
                           
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_art_Samantha_Harvey_Determination.pdf" target="_blank">"Determination: A World at his Back"</a> by Samantha Harvey
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Photo:</strong> <span>Third Place</span>
                           
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_art_Joanna_Knowles_Fall_Rhapsody.pdf" target="_blank">"Fall Rhapsody"</a> by Joanna Knowles
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Design:</strong> <span>First Place</span>
                           
                        </h2>
                        
                        <blockquote> Michelle Cavanaugh and Katie Mascarello Simari</blockquote>
                        
                        
                        <h2>
                           <strong>Editing:</strong> <span>Third Place</span>
                           
                        </h2>
                        
                        <blockquote>Michelle Cavanaugh</blockquote>
                        
                        
                        <h2>
                           <strong>Cover:</strong> <span>First Place</span>
                           
                        </h2>
                        
                        <blockquote> <a href="docs/PHOENIX10/PhoenixXII_art_cover_Jody_Roun_Katie_Simari.pdf" target="_blank">"Frame Work,"</a> art by Jody Roun &amp; design by Katie Mascarello Simari
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Contents Page:</strong><span> First Place</span>
                           
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_contents_page.pdf" target="_blank">Contents</a> by Michelle Cavanaugh and Katie Mascarello Simari
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Staff Page:</strong> <span>First Place</span>
                           
                        </h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_staff_page.pdf" target="_blank">Staff</a> by Michelle Cavanaugh and Katie Mascarello Simari
                        </blockquote>
                        
                        
                        <h2>
                           <strong>Inner Circle of Excellence Award </strong><span>(for students placing in 3 or more categories)</span>
                           
                        </h2>
                        
                        <blockquote> Michelle Cavanaugh and Katie Mascarello Simari</blockquote>
                        
                        
                        <p><strong>For the complete list of state-wide winners, please visit: <a href="http://www.flccaa.org/studentpublications.html">http://www.flccaa.org/studentpublications.html</a></strong></p>
                        
                        <hr>
                        
                        <h2>Congratulations to the cash winners for <em>Phoenix</em> magazine for 2010:
                        </h2>
                        
                        <h2><strong>Poetry</strong></h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_poetry_Ryan_Patton.pdf" target="_blank">"Ghost"</a> by Ryan Patton<br>
                           <a href="docs/PHOENIX10/PhoenixXII_poetry_Mark_Alton.pdf" target="_blank">"Independence Day"</a> by Mark Alton
                           
                        </blockquote>
                        
                        <h2><strong>Non-Fiction</strong></h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_nonfiction_Sean_Drake.pdf" target="_blank">"Brotherhood"</a> by Sean Drake
                        </blockquote>
                        
                        <h2><strong>Fiction</strong></h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_fiction_Rania_Elbohy.pdf" target="_blank">"Starry Night"</a> by Rania Elbohy
                        </blockquote>
                        
                        <h2><strong>Art</strong></h2>
                        
                        <blockquote>
                           <a href="docs/PHOENIX10/PhoenixXII_art_Ryan_Neri_Desideratum.pdf" target="_blank">"Desideratum"</a> by Ryan Neri<br>
                           <a href="docs/PHOENIX10/PhoenixXII_art_Ryan_Neri_Tessitura.pdf" target="_blank">"Tessitura"</a> by Ryan Neri
                        </blockquote>
                        
                        <p>$100 per category was awarded courtesy of <a href="../student-development/default.html" target="_blank">Student Development.</a><br>
                           Check out winning pieces here toward the end of each spring semester when the latest
                           Phoenix is released.
                        </p>
                        
                        <hr>
                        
                        <p><a href="awards2009.html">Click Here</a> for the winners of the <strong>2009 FCCAA Magazine Contest</strong>.
                        </p>
                        
                        <p><a href="awards2008.html">Click Here</a> for the <strong>2008 Florida Community College Publication Association Award List</strong></p>
                        
                        
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards2010.pcf">©</a>
      </div>
   </body>
</html>