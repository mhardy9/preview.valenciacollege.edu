<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards2009.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2>Awards</h2>
                        
                        <p>Valencia proudly congratulates the winners of the <strong>2009 FCCAA Magazine Contest</strong>.
                        </p>
                        
                        <p><strong>Third Place: General Excellence, Division B</strong></p>
                        
                        <p>First Place: Nonfiction
                           
                        </p>
                        
                        <blockquote>
                           <a href="docs/PHOENIX09/LikeDylanAndBaez_by_HollyKnowlesBaker.pdf">"Like Dylan and Baez"</a> by Holly Knowles Baker
                        </blockquote>
                        
                        <p>First Place: Poem
                           
                        </p>
                        
                        <blockquote> <a href="docs/PHOENIX09/TheOrangeGroves_by_MarySeitz.pdf">"The Orange Groves"</a> by Mary Seitz
                        </blockquote>
                        
                        <p>Second Place: Fiction
                           
                        </p>
                        
                        <blockquote>
                           <a href="docs/PHOENIX09/TooYoungForEggnog_by_AliciaBarton.pdf">"Too Young for Eggnog"</a> by Alicia Barton
                        </blockquote>
                        
                        <p>Second Place: Poetry (magazine)
                           
                        </p>
                        
                        <blockquote>
                           <a href="docs/PHOENIX09/FreedomExistsInUnlikelyPlaces_byMagdaKrysinski.pdf">"Freedom Exists in Unlikely Places"</a> by Magda Krysinski<br>
                           <a href="docs/PHOENIX09/Samson_by_MichaelRowland.pdf">"Samson"</a> by Michael Rowland<br>
                           <a href="docs/PHOENIX09/MomentsInTheMoonlight_by_AliciaCooperArchibald.pdf">"Moments in the Moonlight"</a> by Alicia Cooper Archibald
                        </blockquote>
                        
                        <p>Third Place: Illustration with text (individual)
                           
                        </p>
                        
                        <blockquote>
                           <a href="docs/PHOENIX09/Tranquility_by_EmilyHolden.pdf">"Tranquility"</a> by Emily Holden
                        </blockquote>
                        
                        <p>Third Place: Illustrations with text (magazine)
                           
                        </p>
                        
                        <blockquote> <a href="docs/PHOENIX09/BreakingUp_by_FernandoArruda.pdf">"Breaking Up"</a> by Fernando Arruda<br>
                           <a href="docs/PHOENIX09/HarvestMoon_by_JeneeEpping.pdf">"Harvest Moon"</a> by Jenee Epping<br>
                           <a href="docs/PHOENIX09/GetWet_by_KaleyLynn.pdf">"Get Wet"</a> by Kaley Lynn
                        </blockquote>
                        
                        <p>Third Place: Photo
                           
                        </p>
                        
                        <blockquote>
                           <a href="docs/PHOENIX09/Becoming_by_DanielleMalambri.pdf">"Becoming"</a> by Danielle Malambri
                        </blockquote>
                        
                        <p>Third place: Art (individual)
                           
                        </p>
                        
                        <blockquote>
                           <a href="docs/PHOENIX09/Martyr_by_JeneeEpping.pdf">"Martyr"</a> by Jenee Epping 
                        </blockquote>
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards2009.pcf">©</a>
      </div>
   </body>
</html>