<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards2011.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2><img alt="Phoenix Magazine" border="1" height="156" src="Cover2011_000.JPG" width="150"></h2>
                        
                        <h2>Awards<br>
                           <span>Valencia proudly congratulates the winners of the <strong>2011 FCCAA Magazine Contest</strong>.</span>
                           
                        </h2>
                        
                        <hr>            
                        <p><span><strong>General Excellence Award (based on overall points):</strong></span> <span><strong>1st place</strong></span></p>
                        
                        <p><strong><em>Phoenix</em> magazine</strong></p>
                        
                        <hr>                  <span>Poetry (collection): </span><span>1st place
                           </span>
                        
                        <p> Michael Martin, “Delayed Reaction” (100)</p>
                        
                        <p>Helen MacMaster, “Sister” (53)</p>
                        
                        <p>Philip Pantely, “Good Friday” (76)</p>
                        
                        
                        <p><span>Poem:</span> <span>3rd place</span></p>
                        
                        <p>Michael Martin, “Delayed Reaction” (100)</p>
                        
                        
                        <p><span>Nonfiction:</span> <span>1st place</span></p>
                        
                        <p> Merlaine Sivels, “Daddy Issues” (39)</p>
                        
                        
                        <p><span>Art: </span><span>1st place</span></p>
                        
                        <p> Alyssa Aviles, “Self Portrait” (93)</p>
                        
                        
                        <p><span>Illustration with Text, Individual:</span> <span>1st place</span></p>
                        
                        <p> Alyssa Aviles, “Endless Adoration” (51)</p>
                        
                        
                        <p><span>Illustrations with Texts (collection), Magazine:</span> <span>1st place</span></p>
                        
                        <p> Alyssa Aviles, “Endless Adoration” (51)</p>
                        
                        <p> Alyssa Aviles, “Skull Balloons” (71)</p>
                        
                        <p> Angel Rivera, “Death Angel” (60)</p>
                        
                        
                        <p><span>Design: </span><span>1st place</span></p>
                        
                        <p> Meg Lavinghousez and Patrick O’Connor</p>
                        
                        
                        <p><span>Editing:</span> <span>1st place</span></p>
                        
                        <p> Echo Slocum</p>
                        
                        
                        <p><span>Cover:</span><span> 1st place</span></p>
                        
                        <p> Meg Lavinghousez and Patrick O’Connor</p>
                        
                        
                        <p><span>Contents Page: </span><span>1st place</span></p>
                        
                        <p> Meg Lavinghousez and Patrick O’Connor</p>
                        
                        <span><strong>***Special award! The Debra Vazquez award in poetry</strong></span><strong> <br>
                           This honor is given to one poem annually to honor a slain English teacher and poet
                           who was a key presence in the FCSAA until her untimely death in 2004. For the first
                           time ever, one of the <em>Phoenix </em>poets, Helen MacMaster, has won this award with her poem “Sister.” </strong>
                        
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards2011.pcf">©</a>
      </div>
   </body>
</html>