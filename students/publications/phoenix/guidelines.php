<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/guidelines.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Guidelines &amp; Submissions</h2>
                        
                        <div>
                           <hr>
                           
                           <h3>LITERARY GUIDELINES &amp; DEADLINES <br> 
                           </h3>
                           
                           <h3>Literary Entries (Non-Fiction, Fiction, &amp; Poetry)</h3>
                           
                           <p><strong>Although the Phoenix accepts student literary submissions ALL YEAR, please be advised
                                 that:<span>&nbsp;</span></strong></p>
                           
                           <ul>
                              
                              <li>Poetry and prose entries MUST be submitted <strong>by<span></span></strong><strong>midnight on September 30</strong><span><strong>&nbsp;</strong></span><strong>to be considered for the next issue</strong>; the official reading period for poetry and prose is from October through December.
                                 Winners are chosen in January and final selections are made by the end of February.
                              </li>
                              
                              <li>Any poetry and prose submission in by the end of September will be considered for
                                 that fall-all other entries will not be considered until the following year! <br> Example: A poem submitted on October 1, will not be read for consideration until
                                 fall of the following year (Sept).
                              </li>
                              
                              <li><strong>EACH poem or prose piece MUST be submitted separately</strong>, or may be disqualified
                              </li>
                              
                              <li><strong>All entries MUST be students enrolled for classes during the year in which they submit</strong>, or their entries may be disqualified
                              </li>
                              
                           </ul>
                           
                           <p>Literary submissions MUST be submitted as a <strong>Microsoft Word Document</strong> (with a 12-point font and double spacing), Rich Text File, or Plain Text File. Please
                              give each piece a title; remove any other identifying information.<span>&nbsp;</span></p>
                           
                           <p><strong>Guidelines for each category are as follows:</strong></p>
                           
                           <ul>
                              
                              <li>Screenplay: up to three pieces, 2000 words total</li>
                              
                              <li>Fiction: up to three pieces, 2000 words total</li>
                              
                              <li>Graphic Fiction: up to 6 pages total
                                 
                                 <ul>
                                    
                                    <li>Acceptable file types for Graphic Fiction are JPG, TIFF &amp; PDF</li>
                                    
                                    <li>Please remove<span></span><u>all</u><span>&nbsp;</span>text from Graphic Fiction submissions, including artist/title information
                                    </li>
                                    
                                    <li>All Graphic Fiction submissions must be submitted as high resolution graphic files</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Non-fiction: up to three pieces, 2000 words total</li>
                              
                              <li>Poetry: 3 poems max, not more than one page each</li>
                              
                           </ul>
                           
                           <p><strong>ALL entries are automatically entered to win $100 in each of the four categories,
                                 <br> courtesy of Student Development.</strong></p>
                           
                           <p class="button-outline large"><a title="Literary Submission" href="http://net4.valenciacollege.edu/forms/phoenix/submissions.cfm">SUBMISSION FORM</a></p>
                           
                           <hr>
                           
                           <p class="enlarge">ART GUIDELINES &amp; DEADLINES</p>
                           
                           <p>Art entries are also accepted all year, but please know that:</p>
                           
                           <ul>
                              
                              <li>Submissions&nbsp;<strong>entered by the end of the fall semester&nbsp;</strong>are eligible for the January/February selection for the upcoming issue; all submissions
                                 made after that will be considered for the following year. <em>Example: A piece of art submitted in January will not be considered until the following
                                    January!</em></li>
                              
                              <li>Finalists may be contacted via Atlas email by Jan./Feb., and will need to provide
                                 a high-resolution copy of their selected piece(s) suitable for reproduction in print
                                 OR may be asked to bring in the art to be professionally photographed here on West
                                 Campus.
                              </li>
                              
                              <li>Any finalist who does not respond to the email message/provide the art may not get
                                 published
                              </li>
                              
                              <li>All entries MUST be students enrolled for classes during the year in which they submit,
                                 or their entries may be disqualified
                              </li>
                              
                           </ul>
                           
                           <p>Upload a graphic file of your art submission(s) (7 max). Each submission must be submitted&nbsp;<strong>separately</strong>. Please give each piece a title. All entries must be submitted as high resolution
                              graphic files at the following specifications:
                           </p>
                           
                           <ul>
                              
                              <li>JPG, TIFF, PDF file</li>
                              
                              <li>Please remove&nbsp;<u>all</u>test from art submissions, including artist/title information&nbsp;
                              </li>
                              
                              <li>Minimum dimension of 8" on the shortest side (make sure the dpi is 300 or higher)<br> **NOTE! Any entry not meeting these criteria CANNOT BE USED. Please follow.
                              </li>
                              
                           </ul>
                           
                           <p>If you need help resizing your entry to meet the DPI, please consult these resources
                              (these links take you to an external website not affiliated with the&nbsp;<em>Phoenix</em>&nbsp;or with Valencia College) :
                           </p>
                           
                           <ul>
                              
                              <li><a href="http://www.brighthub.com/multimedia/photography/articles/43796.aspx">How to Resize Images</a></li>
                              
                              <li><a href="http://www.rideau-info.com/photos/changedpi.html">Changing the DPI</a></li>
                              
                           </ul>
                           
                           <p>The following formats are&nbsp;<strong>unacceptable</strong>: GIF, PNG, BMP
                           </p>
                           
                           <p><strong>ALL entries are automatically entered to win $100 in each of the four categories,
                                 <br> courtesy of Student Development.</strong></p>
                           
                           <p class="button-outline large"><a title="Artwork Submission " href="http://net4.valenciacollege.edu/forms/phoenix/submissions.cfm">SUBMISSION FORM</a></p>
                           
                        </div>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/guidelines.pcf">©</a>
      </div>
   </body>
</html>