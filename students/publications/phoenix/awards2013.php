<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/awards2013.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> <a href="index.html"></a> 
                        
                        
                        <h2>Awards &amp; Recognition </h2>
                        
                        <h2><span>Editors' Choice 2013 (Volume 15) <img alt="Phoenix 2013 Cover" height="440" src="Phoenix2013FrontCover_001.jpg" width="582"></span></h2>
                        
                        <h2>Congratulations to the <em>Phoenix</em> magazine contributors selected "Best in Category" for the 2013 issue:
                        </h2>
                        
                        <p> (Links to view the vwinning entries COMING SOON!) </p>
                        
                        <h2><strong>Poetry Winner</strong></h2>
                        
                        <p><strong>Joy Richardson, </strong>“Indonesia”
                        </p>
                        
                        <h2><strong>Fiction Winner</strong></h2>
                        
                        <p><strong>Tara Nelson, </strong>“Slaying Giants”
                        </p>
                        
                        <h2><strong>Nonfiction Winner</strong></h2>
                        
                        <p><strong>Philip Belcastro, </strong>“Introduction”
                        </p>
                        
                        <h2><strong>Art Winner</strong></h2>
                        
                        <p><strong>Mariya Karaulova, </strong>“Smoking Bird”
                        </p>
                        
                        <hr>            
                        <p><span>$100 per category was awarded courtesy of <a href="../student-development/default.html" target="_blank">Student Development.</a></span><br>
                           <strong>Winners need to contact 
                              
                              
                              Ms. Ona Cummings Bronson at 
                              
                              
                              <a href="mailto:ocummingsbronson@valenciacollege.edu">ocummingsbronson@valenciacollege.edu</a>  to arrange payment, which will take several weeks. (Please copy the e-mail to Professor
                              Jackie Zuromski at <a href="mailto:Jzuromski1@valenciacollege.edu">Jzuromski1@valenciacollege.edu</a> so that she knows the status of all awards.) </strong></p>
                        
                        <hr>
                        
                        <p><strong>Get Involved</strong><br>
                           Would you like to be a part of the <em>Phoenix</em> team?<br>
                           <a href="apply.html">Click Here</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/awards2013.pcf">©</a>
      </div>
   </body>
</html>