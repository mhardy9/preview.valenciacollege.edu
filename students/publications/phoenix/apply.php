<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Phoenix Magazine | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/phoenix/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/phoenix/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Phoenix Magazine</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/phoenix/">Phoenix</a></li>
               <li>Phoenix Magazine</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <p>HOW TO PARTICIPATE</p>
                        
                        <h3 class="v-red">SUBMIT YOUR WORK<br><span class="small">[<strong>LITERARY</strong>&nbsp;OR <strong>ART/DESIGN</strong>]</span><br> <br> <strong>Welcome, Student Contributors!</strong><span>&nbsp;</span></h3>
                        
                        <p>The <em>Phoenix</em> art and literary magazine is an award-winning student publication published annually
                           by Valencia College through Student Development funds. The publication features 100%
                           student work in the categories of art, fiction, non-fiction and poetry and is 100%
                           designed by students.
                        </p>
                        
                        <h3><span class="small"><strong>Courtesy of Student Development,</strong>&nbsp;<em>all submissions to the Phoenix are AUTOMATICALLY entered</em>&nbsp;<strong>in a CONTEST for which a</strong>&nbsp;$100 prize <strong>will be awarded to the best entry in each of the following four categories: Non-Fiction,
                                 Fiction, Poetry, and Art</strong><em>.</em></span></h3>
                        
                        <hr>
                        
                        <h3 class="v-red" style="text-align: center;"><em><a title="LITERARY GUIDELINES &amp; DEADLINES" href="/students/publications/phoenix/guidelines.php">LITERARY GUIDELINES &amp; DEADLINES</a> </em></h3>
                        
                        <hr>
                        
                        <h3 class="v-red" style="text-align: center;"><em>&nbsp;<a title="ART GUIDELINES &amp; DEADLINES" href="/students/publications/phoenix/guidelines.php">ART GUIDELINES &amp; DEADLINES</a></em></h3>
                        
                        <hr>
                        
                        <h3 class="v-red">&nbsp;</h3>
                        
                        <h3 class="v-red">JOIN THE TEAM <br><span class="small">[<strong>EDITORIAL TEAM</strong> OR <strong>DESIGN TEAM</strong>]</span></h3>
                        
                        <hr>
                        
                        <p>EDITORIAL TEAM</p>
                        
                        <h3>Earn elective credit while working as an editor on our award-winning student literary
                           magazine!<span>&nbsp;</span>No previous experience needed!<span>&nbsp;</span></h3>
                        
                        <p class="v-red"><strong>Editors (fiction, non-fiction, and poetry):</strong></p>
                        
                        <p>Editorial positions for the<span>&nbsp;</span><em>Phoenix</em><span>&nbsp;</span>are staffed by students who sign up for Professor Zuromski's fall class,<span>&nbsp;</span><strong>ENC 2341</strong>—<strong>Advanced Creative Writing/Literary Magazine</strong>.<span>&nbsp;</span></p>
                        
                        <p>Students who sign up for this class should:<span>&nbsp;</span></p>
                        
                        <ul style="list-style-type: square;">
                           
                           <li>LOVE to read (students<span></span><u>may</u><span>&nbsp;</span>have some choice as far as becoming an editor of fiction, non-fiction, or poetry)<span>&nbsp;</span></li>
                           
                           <li>Have a passion for writing, as we will be writing in different genres</li>
                           
                           <li>Enjoy offering colleagues constructive criticism through workshops</li>
                           
                           <li>Value receiving colleagues' constructive criticism through workshops</li>
                           
                        </ul>
                        
                        <p><strong>Advanced Creative Writing/Literary Magazine</strong><span>&nbsp;</span>is offered in the fall semester only and only on West Campus. Students who successfully
                           complete the course will be listed as editors in the<span>&nbsp;</span><em>PHOENIX</em>.<span>&nbsp;</span><br> <strong>Questions? Contact Professor Zuromski at</strong><span>&nbsp;</span><a href="mailto:jzuromski1@mail.valenciacollege.edu">jzuromski1@mail.valenciacollege.edu</a><span>&nbsp;</span><strong>.</strong></p>
                        
                        <p class="v-red"><strong>Editor in Chief:&nbsp;</strong></p>
                        
                        <p>This premium role is held by one student who is selected by the<span>&nbsp;</span><em>Phoenix</em><span>&nbsp;</span>advisor (usually after the student has successfully completed ENC 2341). This person
                           needs to be motivated, responsible, prompt, creative, and appreciative of art and
                           literature. The editor-in-chief will rely upon the poetry and prose/copy editors for
                           initial assistance and input, but will have the final say on content, mechanics, and
                           grammar.
                        </p>
                        
                        <p>Most of the work of the editor-in-chief is done over winter break and in January.
                           After the last editors' meeting in December, the editor-in-chief must choose the final
                           pieces to appear in print, carefully and respectfully considering the wants and needs
                           of the poetry and prose editors as well as the student body at large. The editor-in-chief
                           works closely with the graphic designers to make layout and stylistic choices. He
                           or she selects the overall "mood" of the magazine, arranges the order of the pieces,
                           and selects art to complement certain poems and prose. All of this must be done while
                           following specific guidelines and budgetary constraints.
                        </p>
                        
                        <p>A student may be editor-in-chief for two consecutive years if approved.</p>
                        
                        <p>Note: The editor-in-chief may NOT submit entries for consideration. Other editors
                           MAY submit to the magazine following standard criteria, since all pieces are selected
                           anonymously, but editors are NOT eligible to win cash prizes.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <hr>
                        
                        <p>DESIGN TEAM</p>
                        
                        <p class="v-red"><strong>Graphic Designers:</strong></p>
                        
                        <p>Layout and design for the&nbsp;Phoenix&nbsp;are done by students enrolled in Professor Curtiss'
                           spring Graphic’s Elective course,&nbsp;GRA 1956c—Phoenix Design Project. This course is
                           limited to 8&nbsp;students and requires previous successful completion of&nbsp;GRA 1203C, Layout
                           and Design&nbsp;AND&nbsp;GRA 1206C, Typography.&nbsp;
                        </p>
                        
                        <p>The Phoenix Design Project is offered in the spring semester only and only on West
                           Campus. There is a GRA pre-requisite requirement to enroll in this course. Students
                           must have completed and earned a C or better in GRA1142c—Graphic Design Essentials
                           to participate. Graphics students will earn credit towards their Graphics Elective
                           in participating and earning a C or better in this course. Students who successfully
                           complete the course will be listed as Graphic Designers in the&nbsp;PHOENIX. &nbsp;&nbsp;<br> Questions? Contact Professor Meg Curtiss at&nbsp;<a href="mailto:mcurtiss@mail.valenciacollege.edu">mcurtiss@mail.valenciacollege.edu</a>&nbsp;.&nbsp;
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/phoenix/apply.pcf">©</a>
      </div>
   </body>
</html>