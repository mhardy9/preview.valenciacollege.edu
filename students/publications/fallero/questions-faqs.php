<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/fallero/questions-faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/fallero/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/fallero/">Fallero</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured Image 1" border="0" height="260" src="fallero-slider.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                         </div>
                                                         <br>
                                                         
                                                      </div>
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <a name="content" id="content"></a>
                                                      
                                                      <div>
                                                         
                                                         <h2><a href="default.html">Fallero Literary Arts Magazine</a></h2>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <p><strong>Submission Deadline &amp; Dates</strong></p>
                                                      
                                                      <p><strong>Spring, 2016 Submission Deadline</strong>: Monday, May 2nd, at midnight.
                                                      </p>
                                                      
                                                      <p><strong>Guidelines</strong></p>
                                                      
                                                      <p><strong>Poetry</strong>: Submit up to three poems.
                                                      </p>
                                                      
                                                      <p><strong>Short Stories, Creative Non-Fiction, Scholarly Essays</strong>: Submit one entry. No maximum length. 
                                                      </p>
                                                      
                                                      <p><strong>Photography/Fine Art</strong>: Submit up to three entries. 
                                                      </p>
                                                      
                                                      <p><strong>Digital Media (short films, music videos)</strong>: 7 mins. maximum length. Submit up to three entries.
                                                      </p>
                                                      
                                                      <p><strong>Note for Simultaneous Submissions </strong>: If you are also submitting your work to the West Campus-based magazine,<em>The Phoenix</em>, please let us know at the time of your submission. 
                                                      </p>
                                                      
                                                      <p><strong>Formatting Your Submissions</strong></p>
                                                      
                                                      <p><strong> <u>Poetry, Short Stories,  Creative Non-Fiction, &amp; Scholarly Essays</u></strong><u>:</u></p>
                                                      
                                                      <p>Please include your name and VID in the upper-left of the first page, create a title,
                                                         and use Times New Roman font at size 12. Before uploading your submission, save the
                                                         file in either Word (.doc or .docx) or Rich Text Format (.rtf) using the following
                                                         file-name nomenclature:
                                                      </p>
                                                      
                                                      <p> <strong>lastname_firstname_submissiongenre</strong>.
                                                      </p>
                                                      
                                                      <p> Here's a couple of samples: <strong>smith_mary_poetry.doc</strong> or <strong>johnson_tony_scholarlyessay.do</strong>c.
                                                      </p>
                                                      
                                                      <p>If you're submitting a research essay, be sure to include your Works Cited or Bibliography
                                                         and use a consistent in-text citation system throughout (MLA, APA, etc.). 
                                                      </p>
                                                      
                                                      <p><strong>NOTE</strong>: If your work is selected for publication, the <em>Fallero</em> editors reserve the right to edit it for punctuation/grammar errors. 
                                                      </p>
                                                      
                                                      <p><strong> <u>Photography &amp; Fine Art Submissions</u></strong>:
                                                      </p>
                                                      
                                                      <p>First, be sure your image's ppi (pixels-per-inch) is at 72. To do this using Photoshop,
                                                         follow these steps: <a href="http://www.youtube.com/watch?v=NT9j0O4kyrg">How to Set Your PPI to 72.</a></p>
                                                      
                                                      <p>Next, save your image file in either TIFF or PDF format. Here's how to do so in Photoshop:
                                                         <a href="http://www.youtube.com/watch?v=SmZlszxysrA">How to Save Your Image Files</a>.
                                                      </p>
                                                      
                                                      <p>When naming your file, please use the following file-name nomenclature: </p>
                                                      
                                                      <p><strong>lastname_firstname_submissiongenre</strong>.
                                                      </p>
                                                      
                                                      <p> Here's a sample: <strong>rodriguez_tonya_photography.tiff</strong> or <strong>marano_jose_fineart.pdf</strong>. 
                                                      </p>
                                                      
                                                      <p><strong>Do You Plan on Scanning a Print Photograph or Artwork?</strong></p>
                                                      
                                                      <p>If so, set your scanner to at least 300 dpi to make sure you get a high-quality scan.
                                                         Afterwards, reset your final ppi to 72 as explained above before submitting.
                                                      </p>
                                                      
                                                      <p><u><strong>Digital Media Submissions:</strong></u></p>
                                                      
                                                      <p>For submission and evaluation purposes, please upload your video to either YouTube
                                                         or Vimeo and copy/paste a link to it on the <strong>Submit Your Work </strong>form. If your video is selected for publication, we will contact you via Atlas with
                                                         instructions on how to provide us with a copy of your video for inclusion in the magazine.
                                                         
                                                      </p>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="490"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="265"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/fallero/questions-faqs.pcf">©</a>
      </div>
   </body>
</html>