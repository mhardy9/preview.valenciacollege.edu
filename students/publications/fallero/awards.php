<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/fallero/awards.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/fallero/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li><a href="/students/publications/fallero/">Fallero</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured Image 1" border="0" height="260" src="fallero-slider.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                         </div>
                                                         <br>
                                                         
                                                      </div>
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <a name="content" id="content"></a>
                                                      
                                                      <div>
                                                         
                                                         <h2><a href="default.html">Fallero Literary Arts Magazine</a></h2>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h3>Awards Offered </h3>
                                                      
                                                      <p><em>Fallero</em> offers cash prizes in each of the following categories:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li> <span>Poetry</span>
                                                            
                                                         </li>
                                                         
                                                         <li>Fiction/Creative Non-Fiction</li>
                                                         
                                                         <li>Scholarly Essays</li>
                                                         
                                                         <li>Photography/Fine Art</li>
                                                         
                                                         <li>Digital Media (short films, music videos, etc.)</li>
                                                         
                                                      </ul>            
                                                      
                                                      <p>Each Fall and Spring, awards will be given for 1st ($75), 2nd ($50), &amp; 3rd ($35) places
                                                         in each category. 
                                                      </p>
                                                      
                                                      <p>Spring, 2016 Winners</p>
                                                      
                                                      <p>Academic Essays  </p>
                                                      
                                                      <p><strong>1st Place: </strong>Caitlin Mason 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Angel Guzman 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Edward Lavandier 
                                                      </p>
                                                      
                                                      <p>Poetry</p>
                                                      
                                                      <p><strong>1st Place: </strong>Caitlin Mason 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Teddy Duncan 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Cynthia Trippe 
                                                      </p>
                                                      
                                                      <p>Fiction/Creative Non-Fiction</p>
                                                      
                                                      <p><strong>1st Place: </strong>Kim Marquard 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Anne Nguyen 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Kayla Davis 
                                                      </p>
                                                      
                                                      <p>Digital Media</p>
                                                      
                                                      <p><strong>1st Place: </strong>Jose Herrera 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Greg Valentin 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Francisco Cruz 
                                                      </p>
                                                      
                                                      
                                                      <p><strong>Fall, 2015 Winners</strong></p>
                                                      
                                                      
                                                      <p><strong>Academic Essays</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Vanesa Rivoda 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>MaKenna Wyatt 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>J.P. Holcomb 
                                                      </p>            
                                                      
                                                      <p><strong>Poetry</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Marina Collier 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Karla Yepes 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Faseem Ali 
                                                      </p>            
                                                      
                                                      <p><strong>Fiction/Creative Non-Fiction</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Sascha Davila 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Chris Cutilar 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Salwa Marzan 
                                                      </p>            
                                                      
                                                      <p><strong>Art/Photography</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Astria Hollingsworth 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Kayla Greaux
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Maximilian Salazar 
                                                      </p>            
                                                      
                                                      <p><strong>Digital Media </strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Amanda Coker 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Laura Ballester 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Jean-Pierre Castrillon 
                                                      </p>            
                                                      
                                                      
                                                      <p>Spring, 2015 Winners </p>
                                                      
                                                      <p><strong>Academic Essays</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Paul Hunt 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Elizabeth Binkley 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Daniel Meta
                                                      </p>
                                                      
                                                      <p><strong>Poetry</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Jessica kavelic-Miller 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Loreyn Annabell Workman 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Pricilla Orta
                                                      </p>
                                                      
                                                      <p><strong>Fiction/Creative Non-Fiction</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Robert Muraski 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Jessica Kavelic-Miller 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Christopher Cutillar
                                                      </p>
                                                      
                                                      <p><strong>Art/Photography</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Seetara Matai 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Barbara Litman 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Charles Andrew
                                                      </p>
                                                      
                                                      <p><strong>Digital Media</strong></p>
                                                      
                                                      <p><strong>1st Place: </strong>Capri Fedler 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Patricia Alfaro 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Katrina Figueroa 
                                                      </p>            
                                                      
                                                      
                                                      
                                                      <p><em>Fallero</em> Fall, 2014 Winners! 
                                                      </p>
                                                      
                                                      
                                                      <p>Academic Essays </p>
                                                      
                                                      <p><strong>1st Place: </strong>Paulette Cedano
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Turner Flynn
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Mason Hilado
                                                      </p>
                                                      
                                                      <p>Poetry</p>
                                                      
                                                      <p><strong>1st Place: </strong>Nora Quinn 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Jessica Unkel 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Alicia Stinnett 
                                                      </p>
                                                      
                                                      <p>Art &amp; Photography </p>
                                                      
                                                      <p><strong>1st Place: </strong>Hilary Dunkley 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong>Randy Smith 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Melissa Simon 
                                                      </p>
                                                      
                                                      <p>Fiction &amp; Creative Non-Fiction </p>
                                                      
                                                      <p><strong>1st Place: </strong>Nora Quinn 
                                                      </p>
                                                      
                                                      <p><strong>2nd Place: </strong> Brooke Dennis 
                                                      </p>
                                                      
                                                      <p><strong>3rd Place: </strong>Ed Lamoso
                                                      </p>
                                                      
                                                      <p>.</p>
                                                      
                                                      <p>Spring, 2014 Issue Winners!</p>
                                                      
                                                      <p>We'd like to congratulate the following students for having their work chosen as winners
                                                         in each of <em>Fallero's</em> categories and recipients of cash awards from Student Development. Our Spring issue
                                                         is currently in production and will be published as soon as possible. All paperwork
                                                         for the Fall, 2013 and Spring, 2014 awards has been submitted and is being processed.
                                                         .Awards will be mailed directly to winners' official addresses on record with the
                                                         College. 
                                                      </p>
                                                      
                                                      <p><strong>Poetry</strong>:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>1st Place: Kaley Craig </li>
                                                         
                                                         <li>2nd Place: Estefania Gropusso </li>
                                                         
                                                         <li>3rd Place: Luis de Pena </li>
                                                         
                                                      </ul>            
                                                      
                                                      <p><strong>Fiction:</strong></p>
                                                      
                                                      <ul>
                                                         
                                                         <li>1st Place: Josh Corson </li>
                                                         
                                                         <li>2nd Place: Garrett Ivan Colon </li>
                                                         
                                                         <li>3rd Place: Stephanie Lewis</li>
                                                         
                                                      </ul>            
                                                      
                                                      <p><strong>Scholarly Essays</strong>:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>1st Place: Ethan Duke </li>
                                                         
                                                         <li>2nd Place: Kiomarie Mendez</li>
                                                         
                                                         <li>3rd Place: Francisco Portillo </li>
                                                         
                                                      </ul>            
                                                      
                                                      <p><strong>Photography/Fine Art</strong>:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>1st Place: William Conine </li>
                                                         
                                                         <li>2nd Place: Robert Goldman </li>
                                                         
                                                         <li>3rd Place: Maximiliano Salazar </li>
                                                         
                                                      </ul>            
                                                      
                                                      <p><strong>Short Film</strong>:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>1st Place: Zachary Bagnell </li>
                                                         
                                                         <li>2nd Place: Douglass Hoover </li>
                                                         
                                                         <li>3rd Place: Gabriel Dorta </li>
                                                         
                                                      </ul>            
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="490"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="265"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/fallero/awards.pcf">©</a>
      </div>
   </body>
</html>