<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/publications/fallero/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/publications/fallero/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/publications/">Publications</a></li>
               <li>Fallero</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                <ul>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured Image 1" border="0" height="260" src="fallero-slider.jpg" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            
                                                            
                                                         </div>
                                                         
                                                         
                                                         <p><br>
                                                            
                                                         </p>
                                                         
                                                      </div>
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <a name="content" id="content"></a>
                                                      
                                                      <div>
                                                         
                                                         <h2><a href="default.html">Fallero Literary Arts Magazine</a></h2>
                                                         
                                                      </div>
                                                      
                                                      <p><strong><em>Fallero</em>, Fall, 2015: Issue 3.1</strong></p>
                                                      
                                                      <p><a href="documents/FalleroFall2015Final3.1.pdf"><img alt="Fallero, Fall, 2015" border="0" height="240" src="fallerofall2015webimagegif.gif" width="320"></a> 
                                                      </p>
                                                      
                                                      <p>The Fall, 2015 issue of <em>Fallero</em> is now available. Please right-click the image above, choose Save as, and download.
                                                         Navigate using your mouse or down-arrow key. Trying to view <em>Fallero</em> in your Internet browser will not allow embedded fonts to display properly, and our
                                                         digital media works might not play. Therefore, we encourage you to download the issue
                                                         to your computer. Also, be sure you have the latest version of Adobe Reader, available
                                                         for free <a href="http://get.adobe.com/reader">here</a>.
                                                      </p>
                                                      
                                                      
                                                      
                                                      <p>Archived Issues </p>
                                                      
                                                      
                                                      
                                                      <p>Second Year </p>
                                                      
                                                      
                                                      
                                                      <p><em>Fallero</em>, Spring, 2015: : Issue 2.2
                                                      </p>
                                                      
                                                      <p><a href="documents/Fallero2.2.pdf"><img alt="FalleroCover" border="0" height="266" src="WebImageCoverResized.gif" width="400"></a></p>
                                                      
                                                      <p>Please right-click and download if the file doesn't open automatically. To navigate,
                                                         use your down-arrow key or your mouse. 
                                                      </p>
                                                      
                                                      <p><strong>About this Issue</strong></p>
                                                      
                                                      <p> On May 25th, 1895, one of England's least benevolent judges condemned Ireland's most
                                                         sardonic wit to twenty-four months of solitary confinement, caloric as well as intellectual
                                                         deprivation, and sustained, enforced silence. A graduate of both Trinity College and
                                                         Oxford University, Oscar Wilde had to necessarily abandon his previous labors (hilariously
                                                         eviscerating the Vapid and the Venal) for creatively-bereft drudgery: grinding grain,
                                                         turning crank handles on metal drums, unspooling fibrous strands from ship-caulking
                                                         rope ("picking oakum" for our Melville fans), and sewing mail bags. Ever faithful
                                                         to the Moronic Penal Myth that falsely correlates Harsh Punishment with Low Recidivism,
                                                         Wilde's jailers released him, at which point he began a new period of incarceration,
                                                         this time existential: three-and-a-half years of lonely, self-medicated wandering
                                                         and impoverishment before dying, at 46, from meningitis. 
                                                      </p>
                                                      
                                                      <p> Loam stopping a beer-barrel. </p>
                                                      
                                                      <p> What vileness could have inspired absurdly be-wigged presiding judge Sir Alfred Wills--bro-ish
                                                         Alpinist, consummate bore--to declare Wilde's "the worst case [he had] ever seen"?
                                                         How could an innocuous, sartorial author of immortally sublime quips (like this, via
                                                         Lord Illingworth: "The only difference between the saint and the sinner is that every
                                                         saint has a past, and every sinner has a future") be placed on humiliating, sadistic
                                                         display atop a train platform upon his transfer to Reading Gaol? While a salacious
                                                         list of gruesome offenses (in Times New Roman, no doubt) now likely scrolls upward
                                                         on our ever-obedient readers' mental docket-screens, Wilde's actual "crime" embodies
                                                         a tragically common example of unconscionable judicial legalese: inanely prudish,
                                                         purposely obfuscating, invariably self-serving (the sort of euphemistic bastardizing
                                                         preferred by all comfortably smug sycophants, be they Socrates's appointed <em>Archon</em> and <em>dikastes</em> or the infamous New Testament ménage--Pilate, Herod, the Sanhedrin that initiated
                                                         the sad proceedings--who mocked, then tortured to death, a teacher no less talented
                                                         [and, therefore, equally dangerous] than the Great Loquacious Greek). 
                                                      </p>
                                                      
                                                      <p>"Gross indecency" read the indictment (delivered, one imagines, "in a whisper. All
                                                         rose, exchanging smiles"). The prosecutor next specified Wilde's particular act of
                                                         supposed lewdness: "engaging in homosexual acts."
                                                      </p>
                                                      
                                                      <p>Today, we call this "being gay." </p>
                                                      
                                                      <p>Our Spring, 2015 issue marked the 120-year anniversary of Oscar Wilde's trial and
                                                         imprisonment for violating Victorian-Era England's shameful laws depriving human beings
                                                         of that One Birthright inherent to us all: the privilege to exist. Unfortunately,
                                                         revisiting Wilde's final years made us realize that things haven't really changed
                                                         all that much. When current Alabama Supreme Court Justice Roy Moore continues to ignore
                                                         the federal ruling legalizing gay marriage (claiming, in 2002, that homosexuality
                                                         is "abhorrent, immoral, detestable, a crime against nature and a violation of the
                                                         laws of nature"), when nearly 80 countries still criminalize LGBT community members
                                                         in one form or another (including death), we just  had, again, to sigh. 
                                                      </p>
                                                      
                                                      <p>This issue (as well as our Fall, 2015 collaboration with the Peace &amp; Justice Institute)
                                                         is devoted to those who are condemned for the crime of Simply Being Themselves. Our
                                                         cover depicts a Reading Gaol cell similar to Wilde's (where, at times, he was confined
                                                         for 23 hours a day, communication and camaraderie with fellow inmates being strictly
                                                         <em>verboten</em> ). We furthermore scrapped the "magazine" metaphor, opting instead to construct a
                                                         digital prison in the architectural vein of Jeremy Bentham's Panopticon, a theoretical
                                                         structure designed to keep prisoners under perpetual surveillance by way of two very
                                                         handy tools: Fear &amp; Efficiency (few things are more valued by Apparatuses of Control
                                                         than the shrewd deployment of their power). As every hapless English major knows,
                                                         the Panopticon was also employed by French Uber Nerd Michel Foucault (who himself
                                                         suffered emotionally and socially because of his own homosexuality--including repeated
                                                         acts of self-harm and attempted suicide--before finally succumbing, at the apex of
                                                         his academic celebrity, to AIDS) in his book <em>Discipline and Punish</em> to explain how institutions--governmental, legal, educational, religious, bureaucratic,
                                                         cultural--surveil, directly or by proxy, their subjects and punish those who fail
                                                         to conform. Says Foucault: 
                                                      </p>
                                                      
                                                      <blockquote>
                                                         
                                                         <p> The judges of normality are present everywhere . . . it is on them that the universal
                                                            reign of the normative is based; and each individual, wherever he may find himself,
                                                            subjects to it his body, his gestures, his behavior, his aptitudes, his achievements.
                                                            
                                                         </p>
                                                         
                                                      </blockquote>
                                                      
                                                      <p> Miraculously, during his period of subjugation, Wilde managed to resist. Permitted
                                                         to write a single page each day, he eventually completed "De Profundis," a 50,000
                                                         word "letter"--part personal remonstrance against his accusers, part re-evaluation
                                                         of his spirituality. Ironically (given the "moral foundations" upon which large portions
                                                         of British law were shabbily constructed), he found himself contemplating another
                                                         figure who had been condemned for being "different" and espousing words and ideas
                                                         the institutions of His own time found unpopular--Good Old Jesus, a guy who would
                                                         hang out with pretty much anyone: tax collectors, prostitutes, Pharisees, lepers,
                                                         criminals, betrayers. Like Him, <em>Fallero</em> will always encourage and celebrate our students' voices, never more so than when
                                                         their words, art, lifestyles, or ideas threaten those who seek to silence them. 
                                                      </p>
                                                      
                                                      <p>We'll let Wilde himself conclude for us. From "De Profundis": </p>
                                                      
                                                      <blockquote>
                                                         
                                                         <p> The gods are strange. It is not our vices only they make instruments to scourge us.
                                                            They bring us to ruin through what in us is good, gentle, humane, loving. . . People
                                                            used to say of me that I was too individualistic. 
                                                            I must be far more of an individualist than ever I was. I must get far more out of
                                                            myself than ever I got, and ask far less of the world than ever I asked. Indeed, my
                                                            ruin came not from too great individualism of life, but from too little. The one disgraceful,
                                                            unpardonable, and to all time contemptible action of my life was to allow myself to
                                                            appeal to society for help and protection. To have made such an appeal would have
                                                            been from 
                                                            the individualist point of view bad enough, but what excuse can there ever be put
                                                            forward for having made it? Of course once I had put into motion the forces of society,
                                                            society turned on me and said, ‘Have you been living
                                                            all this time in defiance of my laws, and do you now appeal to those laws for protection?
                                                            You shall have those laws exercised to the full. You shall abide by what you have
                                                            appealed to.’ The result is I am in gaol. Certainly no man ever fell so ignobly, and
                                                            by such ignoble instruments, as I did. . . Christ, through some divine instinct in
                                                            him, seems to have always loved the sinner as being the nearest possible approach
                                                            to the perfection of man. His primary desire was not to reform people, any more than
                                                            his primary desire was to relieve suffering. To turn an interesting thief into a tedious
                                                            honest man was not his aim. He would have thought little of the Prisoners’ Aid Society
                                                            and other modern movements of the kind. The conversion of a publican into a Pharisee
                                                            would not have seemed to him a great achievement. But in a manner not yet understood
                                                            of the world he regarded sin and suffering as being in themselves beautiful holy things
                                                            and modes of perfection.
                                                         </p>
                                                         
                                                      </blockquote>
                                                      
                                                      <p><em>Fallero</em> Editors
                                                      </p>
                                                      
                                                      <p><em>Fallero</em> Fall, 2014: : Issue 2.1
                                                      </p>
                                                      
                                                      <p>'Twas a painful birth this time--but she's out at last. </p>
                                                      
                                                      <p>For this installment, we decided, design-wise, to eschew our methods from the first
                                                         two issues: mimicking print-based magazines. Therefore, you will see neither a traditional
                                                         table of contents nor page numbers. In addition, we spent a chunk of Christmas break
                                                         getting our Nerd On by learning some new software that allows us to incorporate basic
                                                         animation into the design. When the issue first opens, therefore, you'll see  the
                                                         cover (kinda) come to life for about ten seconds. 
                                                      </p>
                                                      
                                                      <p>Right-click the image below, choose "Save as," and download to your computer (if your
                                                         browser is updated with the latest version of Flash, it might play for you that way):
                                                         
                                                      </p>
                                                      
                                                      <p><a href="documents/fallero2.1.pdf"><img alt="FalleroCover" border="0" height="192" src="moonscapetowersnotext.jpg" width="256"></a></p>
                                                      
                                                      
                                                      
                                                      <p>First Year </p>
                                                      
                                                      
                                                      
                                                      <p><em>Fallero</em>, Spring, 2014: Issue 1.2
                                                      </p>
                                                      
                                                      <p><em>Fallero</em> is published as an "Interactive PDF." Therefore, your browser's built-in PDF reader
                                                         may not properly display the file or allow its interactive features to function. We
                                                         strongly suggest, then, to download the file  and open in Adobe Reader, available
                                                         <a href="http://www.adobe.com/products/reader.html">here</a> for free. To download, right click on the image below and choose "Save as" or "Save
                                                         link as" (depending on your operating system and browser). Afterwards, go take a brief
                                                         nap or Netflix it for a bit: since there's two 8-9 minute short films in this issue,
                                                         the file is fairly large. Using your phone or tablet? Ha.<em> Fallero</em>'s Awesomeness will make it explode. Please, therefore, use a computer. 
                                                      </p>
                                                      
                                                      <p>Navigating the Issue:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>using your "up" and "down" arrow keys or clicking your mouse/trackpad is the easiest
                                                            way to go from page to page.
                                                         </li>
                                                         
                                                         <li>on the Table of Contents, clicking the page number next to each student's name will
                                                            take you directly to that student's work. 
                                                         </li>
                                                         
                                                         <li> the short films should start playing automatically, and there are controls to pause,
                                                            etc. 
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>To view/download the Spring, 2014 issue, click on the image below: </p>
                                                      
                                                      <p><a href="documents/Fallero1.2Final_000.pdf"><img alt="1.2WebImageSmall" border="0" height="250" src="Fallero1.2WebImageSmall.gif" width="187"></a></p>
                                                      
                                                      <p><em>Fallero</em>, Fall, 2013: Issue 1.1 
                                                      </p>
                                                      
                                                      <p><em>Fallero</em> is published as an "Interactive PDF." Therefore, your browser's built-in PDF reader
                                                         may not properly display the file or allow its interactive features to function. We
                                                         strongly suggest, then, to download the file and open in Adobe Reader, available <a href="http://www.adobe.com/products/reader.html">here</a> for free. To download, right click on the image below and choose "Save as" or "Save
                                                         link as" (depending on your operating system and browser).
                                                      </p>
                                                      
                                                      <p>Navigating the Issue:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>using your "up" and "down" arrow keys or clicking your mouse/trackpad is the easiest
                                                            way to go from page to page.
                                                         </li>
                                                         
                                                         <li>on the Table of Contents, clicking the image next to each student's name will take
                                                            you directly to that student's work. 
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p><a href="documents/Fallero1.1Fall2013.pdf"><img alt="Fallero, Fall, 2013" border="0" height="240" src="Fall2013WebImageArchives.gif" width="320"></a></p>
                                                      
                                                      
                                                      <p name="undefined" id="undefined"><strong>Fallero? No Comprendo</strong></p>
                                                      
                                                      <p>"Fallero" is the name given to one who attends the annual arts and cultural festival
                                                         called Las Fallas that takes place each Spring in Valencia, Spain. Learn all about
                                                         it here:<a href="http://www.youtube.com/watch?v=DS1S-m9a7uo"> Las Fallas</a>.
                                                      </p>
                                                      
                                                      <p><strong>Why Fallero? </strong></p>
                                                      
                                                      <p>Several reasons: one, its obvious "Valencia" theme (we know--it's all about oranges,
                                                         historically speaking, but we enjoy taking liberties with the truth); two, both the
                                                         word  and the Las Fallas festival in general embody the fiery celebration of art and
                                                         culture—just like the magazine itself; and three,  it reflects the important role
                                                         of Hispanic culture  at Valencia (East Campus in particular) as well as in our surrounding
                                                         community.
                                                      </p>
                                                      
                                                      <p><strong>What We Do</strong></p>
                                                      
                                                      <p>Each Fall and Spring semester, <em>Fallero</em> publishes the best student work at Valencia in a variety of categories: poetry, fiction/creative
                                                         non-fiction, scholarly essays, photography/fine arts, and digital media. Moreover,
                                                         for each category, we offer a monetary reward for 1st, 2nd, and 3rd places ($75, $50,
                                                         &amp; $35, respectively: Thanks, Student Development!). 
                                                      </p>
                                                      
                                                      <p><strong>How We Do It</strong></p>
                                                      
                                                      <p>All digitally. Using the latest in digital publishing software and technologies, <em>Fallero</em> can publish not only traditional text-based work but also high-resolution photography,
                                                         
                                                      </p>
                                                      
                                                      <p>illustrations, paintings, and digital video. As a result, students passionate about
                                                         nearly every academic and creative discipline at Valencia have the chance to have
                                                         their work celebrated--with the whole universe. Neat. 
                                                      </p>
                                                      
                                                      <p><strong>Want to Submit?</strong></p>
                                                      
                                                      <p>Click on the <strong>Submit Your Work</strong> link above. 
                                                      </p>
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="490"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="265"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/publications/fallero/index.pcf">©</a>
      </div>
   </body>
</html>