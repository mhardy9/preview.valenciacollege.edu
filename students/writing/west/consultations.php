<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Consultations  | Valencia College</title>
      <meta name="Description" content="The Writing Center is staffed with Valencia English, Reading, and EAP adjunct faculty and teachers from other disciplines bringing similar teaching experience and credentials. Instructors are available by appointment, and appointments can be made through your ATLAS account.">
      <meta name="Keywords" content="writing, center, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/writing/west/consultations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/writing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/writing/">Writing</a></li>
               <li><a href="/students/writing/west/">West</a></li>
               <li>Consultations </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Consultations</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Expectations</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <p>Your consultation will be with an instructor. The Writing Center is staffed with Valencia
                              English,
                              Reading, and EAP adjunct faculty and teachers from other disciplines bringing similar
                              teaching experience
                              and credentials.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>The consultation emphasis will be on teaching and learning.</li>
                              
                              <li>We will discuss as much as is reasonable in a 30-minute consultation.</li>
                              
                              <li>Your paper will probably leave better than when it came in. Improvements are inherently
                                 a part of the
                                 writing consultation process.
                                 
                              </li>
                              
                              <li>You will be your own proof-reader, a transferable skill you can use everywhere.</li>
                              
                              <li>We’ll help you try to earn good grades, but grades are your responsibility.</li>
                              
                              <li>We will help you become a better writer!</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Availability</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Instructors are available by appointment, and appointments can be made through your
                              ATLAS account by
                              clicking on the "Courses" tab then on "West Campus Writing Center," by calling (407)
                              582-5454, or by
                              stopping by 5-155A.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>The Writing Center is only for currently enrolled Valencia students.</li>
                              
                              <li>Unfortunately, faculty and staff may not have a consultation in the Writing Center.</li>
                              
                              <li>Each appointment is 30 minutes; one-hour appointments are granted with a consultant's
                                 referral or by
                                 instructor request.
                                 
                              </li>
                              
                              <li>One appointment per day, per student.</li>
                              
                              <li>Three appointments per week, per student.</li>
                              
                              <li>Appointments can be made up to two weeks in advance.</li>
                              
                              <li>No-Show appointments may be given to walk-in students after 5 minutes.</li>
                              
                              <li>First come, first served consultations are available at the Mobile Writing Center.
                                 Please check the
                                 West Campus Writing Center main page for times and locations.
                                 
                              </li>
                              
                              <li>Food, drinks, and smokeless tobacco are not permitted in the Writing Center.</li>
                              
                              <li>No children are allowed in the Communications Center or Writing Center per Valencia
                                 policy
                                 6Hx28:04-10.
                                 
                              </li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>What To Bring</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>In order to best serve students in the Writing Center and make the most of the consultation's
                              time,
                              please bring:
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>A picture ID</li>
                              
                              <li>Print copy of your work is preferred; however, students can receive general feedback
                                 on electronic
                                 copies of their work
                                 
                              </li>
                              
                              <li>The assignment as given by the instructor</li>
                              
                              <li>Any other materials that might help the Writing Center instructor in assisting you
                                 with your work
                              </li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 22 campusID = 3 department_fields = "none" office_fields = "website,
                        email, phone, location, hours" campus_fields = "none"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://chompchomp.com/" class="button btn-block text-center" target="_blank">Need a grammar
                           refresher?<br>Try the MOOC on Grammar Bytes!</a>
                        
                     </div>
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/writing/west/consultations.pcf">©</a>
      </div>
   </body>
</html>