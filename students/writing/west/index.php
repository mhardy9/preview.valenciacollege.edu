<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Writing Center  | Valencia College</title>
      <meta name="Description" content="The Writing Center will help students at any stage of the writing/learning process. We go over English language assignments with students from all disciplines and all courses, college wide">
      <meta name="Keywords" content="writing, center, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/writing/west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/writing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/writing/">Writing</a></li>
               <li>West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>West Campus Writing Center</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>About Writing Consultations</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>We will help students at any stage of the writing/learning process. We go over English
                              language
                              assignments with students from all disciplines and all courses, college wide, including:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Help with grammar assignments</li>
                              
                              <li>Help with pronunciation</li>
                              
                              <li>Help understanding Main Ideas and other reading skills</li>
                              
                              <li>Help organizing and developing a speech</li>
                              
                              <li>Help constructing paragraphs, essays, or research papers</li>
                              
                              <li>Help with scholarship essays</li>
                              
                           </ul>
                           
                           
                           <div>
                              <a href="consultations.html" class="button">More About Consultations</a><a href="http://www.signupgenius.com/go/20f044aacae29a4f85-communications" class="button">Sign up for
                                 Communications/Writing Center Workshops</a>
                              
                           </div>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Making Appointments</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You can make Writing Center appointments by calling 407-582-5454, stopping by 5-155,
                              or through Atlas by
                              clicking the button below. It's quick and easy!
                           </p>
                           
                           <div>
                              <a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=http%3A%2F%2Fgcp.valenciacollege.edu%2FCPIP%2F%3Fsys%3Dwconline" class="button">Log In</a>
                              
                           </div>
                           
                           
                           <h4>No Appointment? The Mobile Writing Center is Here!</h4>
                           
                           <p>The Mobile Writing Center offers first come, first served help, Monday - Thursday:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>In the Communications Center (in 5-155H) 10 A.M.-12 P.M.</li>
                              
                              <li>In the group study area on the first floor of the library (Building 6): 12 P.M.-2
                                 P.M.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 22 campusID = 3 department_fields = "none" office_fields = "website,
                        email, phone, location, hours" campus_fields = "none"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://chompchomp.com/" class="button btn-block text-center" target="_blank">Need a grammar
                           refresher?<br>Try the MOOC on Grammar Bytes!</a>
                        
                     </div>
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/writing/west/index.pcf">©</a>
      </div>
   </body>
</html>