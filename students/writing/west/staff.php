<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Staff  | Valencia College</title>
      <meta name="Description" content="Meet the staff of the Writing Center! All of our consultants are highly qualified and ready to handle any writing question you can throw their way. ...Okay, almost any question. They love helping students with writing!">
      <meta name="Keywords" content="staff, writing, center, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/writing/west/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/writing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/writing/">Writing</a></li>
               <li><a href="/students/writing/west/">West</a></li>
               <li>Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Writing Center Staff</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>About Us</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>Don't let writing get you down. We're here to help.</strong></p>
                           
                           
                           <p>All of our consultants are highly qualified and ready to handle any writing question
                              you can throw their
                              way. ...Okay, <em>almost</em> any question. They love helping students with writing!
                           </p>
                           
                           <hr>
                           
                           
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Meena.jpg" alt="Portrait of Meena Udho">
                              
                              <h4><a href="mailto:mudho@valenciacollege.edu">Meena Udho</a></h4>
                              
                              <p>
                                 Instructional Lab Supervisor<br> Phone: 407-582-5063
                                 
                              </p>
                              
                              
                              <p>Bio: I received my B.A. in English and M.S. in Publishing from Pace University in
                                 New York. Having
                                 worked briefly in a small publishing house, I found my calling as an educator. I have
                                 worked at Valencia
                                 College as a writing consultant and instructor since 2012. I firmly believe that learning
                                 is a process;
                                 no substantial change will occur overnight, and every educator should facilitate that
                                 development and
                                 help nurture students' abilities.
                              </p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Beverley.jpg" alt="Portrait of Beverley Meer">
                              
                              <h4>Beverley Meer</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                              
                              <p>
                                 Bio: I was born and raised in Dunkirk in Western New York State. I received my Bachelor
                                 of Arts at
                                 Northwestern University in Evanston, Illinois, majoring in English, minoring in history.
                                 I earned my
                                 Masters in Education at the State University of New York at Buffalo. I took additional
                                 post graduate
                                 hours in teaching reading at Buffalo State College. I taught Developmental English
                                 ten years at Indian
                                 River Community College in Stuart, Florida and twelve at Valencia College in Orlando.
                                 At present, I am
                                 working one-on-one with students in the West Campus writing center, a position which
                                 I enjoy very much.
                                 Otherwise, I might be singing in a chorus, swinging a pickle ball paddle, or dancing
                                 at Jazzercise!
                                 
                              </p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/default.jpg" alt="Portrait of Denise Mcknight">
                              
                              <h4>Denise Mcknight</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Ellen.jpg" alt="Portrait of Ellen Costello">
                              
                              <h4>Ellen Costello</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                              
                              <p>
                                 Bio: I grew up in a family of readers, and went to kindergarten when I was four since
                                 I loved school so
                                 much and couldn't wait until I was five! I have lived in New York, New Jersey, Connecticut,
                                 Washington,
                                 D.C., and Florida along the way. I have a Master of Education degree in Curriculum
                                 and Instruction with
                                 a concentration in Reading. Ellen worked in public K-12 literacy education in Washington,
                                 D.C. and
                                 Orlando, Florida from 1998 - 2015. Previously, I was also an adjunct professor in
                                 the Reading Department
                                 at Valencia College. In addition to my job as a Sr. Instructional Assistant in the
                                 Writing Center, I am
                                 currently employed on the West Campus as an adjunct professor in the Educator Preparation
                                 Institute, as
                                 a private tutor, and substitute teacher. I love my job as a tutor at Valencia since
                                 I am able to meet
                                 and serve people who have such diverse backgrounds and needs.
                                 
                              </p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Hassan.jpg" alt="Portrait of Hassan Tchenchana">
                              
                              <h4>Hassan Tchenchana</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                              <p>
                                 Bio: I am Moroccan, and I have been working at Valencia since August 2010. I have
                                 a BA in English, with
                                 a concentration in Linguistics from Morocco. Before moving to the United States, I
                                 taught English to
                                 non-native speakers in different institutions. Currently, I am an EAP instructor and
                                 a Writing Center
                                 consultant. I love languages; that is why I am trilingual (Arabic, French, and English),
                                 and I intend to
                                 learn Spanish soon.
                                 
                              </p>
                              
                           </div>
                           
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/laneshia.jpg" alt="Portrait of Laneshia Moore">
                              
                              <h4>Laneshia Moore</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                              
                              <p>
                                 Bio: I was born and raised in a small town in South Carolina where, as a child, I
                                 developed a love for
                                 reading and an interest in writing. I knew at a young age that whatever career path
                                 I chose would have
                                 to combine both of these subjects. I have been working at Valencia College for a year
                                 as a senior
                                 instructional assistant, a position that allows me to combine my passions with helping
                                 students develop
                                 and improve their writing skills.
                                 
                              </p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/default.jpg" alt="Portrait of Milena Zaleckaite">
                              
                              <h4>Milena Zaleckaite</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Paul.jpg" alt="Portrait of Paul Jacob">
                              
                              <h4>Paul Jacob</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                              <p>
                                 Bio: I currently teach creative writing/poetics and spiritual/religious courses in
                                 the Rollins College
                                 RCLL program and English Composition at Valencia College. I also teach literature
                                 classes and lead a
                                 contemplative lecture series at Westminster Winter Park. I have also been a guest
                                 lecturer on
                                 alternative media at Duke University and have taught Travel Writing and Creative Nonfiction
                                 at Rollins
                                 College. I also facilitate contemplative retreats at spiritual centers like Ghost
                                 Ranch in New Mexico
                                 and Cobblestone Springs in New York.
                                 
                              </p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/default.jpg" alt="Portrait of Peg Spellman">
                              
                              <h4>Peg Spellman</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                           </div>
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Rhonda.jpg" alt="Portrait of Rhonda Oehlrich">
                              
                              <h4>Rhonda Oehlrich</h4>
                              
                              <p>Instructional Lab Assistant</p>
                              
                              <p>
                                 Bio: At age 18, my first time in college, I had no desire to enter the teaching profession.
                                 After 20
                                 some years as a Trainer, Coach, and Mentor I returned to school to advance my career
                                 as a Corporate
                                 Trainer. While completing my junior and senior years of college at UCF, as a non-traditional
                                 (older)
                                 student; I fell in love with academia and was impressed with this generation of students.
                                 Here I am...
                                 with a passion to work as an instructor, one on one, in the Writing Center and as
                                 a Professor of
                                 Education.
                                 
                              </p>
                              
                           </div>
                           
                           
                           <hr>
                           
                           
                           <div>
                              <img src="/_resources/img/students/writing/west/Krystelle.jpg" alt="Portrait of Krystelle Kubicek">
                              
                              <h4>Krystelle Kubicek</h4>
                              
                              <p>Office Aide</p>
                              
                              <p>
                                 Bio: I was born and raised in a far, far away land called Venezuela. Ever since I
                                 can remember, my dream
                                 has always been to become a veterinarian. My adventure began when I graduated high
                                 school and moved to
                                 Costa Rica to start veterinary school. After two years of studying and living in that
                                 magical land, my
                                 family and I moved again. So here you see me, a current UCF student who enjoys food,
                                 videogames,
                                 traveling, and science while endeavoring to fulfill my dream of becoming the ultimate
                                 animal doctor.
                                 
                              </p>
                              
                           </div>
                           
                           <hr>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 22 campusID = 3 department_fields = "none" office_fields = "website,
                        email, phone, location, hours" campus_fields = "none"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://chompchomp.com/" class="button btn-block text-center" target="_blank">Need a grammar
                           refresher?<br>Try the MOOC on Grammar Bytes!</a>
                        
                     </div>
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/writing/west/staff.pcf">©</a>
      </div>
   </body>
</html>