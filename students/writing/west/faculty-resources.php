<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Resources  | Valencia College</title>
      <meta name="Description" content="Helpful links, handouts, and videos for faculty on topics related to reading and writing.">
      <meta name="Keywords" content="writing, center, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/writing/west/faculty-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/writing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/writing/">Writing</a></li>
               <li><a href="/students/writing/west/">West</a></li>
               <li>Faculty Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Faculty Resources</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Links</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://writing2.richmond.edu/wac/wtl.html" target="_blank">Richmond Write to Learn
                                    Resources</a></li>
                              
                              <li><a href="http://www.cws.illinois.edu/initiatives/wac/resources/handouts/index.html" target="_blank">The
                                    Center for Writing Across the Curriculum</a></li>
                              
                              <li><a href="http://wac.colostate.edu/intro/pop2d.cfm" target="_blank">The Writing Across the Curriculum
                                    Clearinghouse</a></li>
                              
                              <li><a href="http://writing.wisc.edu/wac/" target="_blank">University of Wisconsin - Madison Writing
                                    Across the Curriculum</a></li>
                              
                              <li>
                                 <a href="http://www.weareteachers.com/blogs/post/2013/06/25/writing-across-the-curriculum-what-how-and-why" target="_blank">We are Teachers: Writing Acrosss the Curriculum</a>
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Handouts</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://web.csulb.edu/divisions/aa/gwar/faculty/documents/WritingtoLearnActivities.pdf" target="_blank">California State University Write to Learn Resources</a></li>
                              
                           </ul>
                           
                           
                           <h4>Videos</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://www.teachingchannel.org/videos/writing-to-learn" target="_blank">Teaching Channel:
                                    Write to Learn</a></li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 22 campusID = 3 department_fields = "none" office_fields = "website,
                        email, phone, location, hours" campus_fields = "none"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://chompchomp.com/" class="button btn-block text-center" target="_blank">Need a grammar
                           refresher?<br>Try the MOOC on Grammar Bytes!</a>
                        
                     </div>
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/writing/west/faculty-resources.pcf">©</a>
      </div>
   </body>
</html>