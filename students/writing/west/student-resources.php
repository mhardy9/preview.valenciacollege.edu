<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Resources  | Valencia College</title>
      <meta name="Description" content="Helpful links, handouts, and videos for students on topics related to reading, writing, and learning English for academic purposes.">
      <meta name="Keywords" content="student, resources, writing, center, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/writing/west/student-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/writing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/writing/">Writing</a></li>
               <li><a href="/students/writing/west/">West</a></li>
               <li>Student Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Student Resources</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>English</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Dictionary</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://dictionary.reference.com/" target="_blank">Dictionary</a></li>
                              
                              <li><a href="http://www.merriam-webster.com/" target="_blank">Merriam-Webster</a></li>
                              
                           </ul>
                           
                           
                           <h4>Citation Help</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://www.apastyle.org/index.aspx?_ga=1.183085615.888214575.1468504136" target="_blank">Apa
                                    Style</a></li>
                              
                              <li><a href="http://libguides.valenciacollege.edu/west_comm_ctr" target="_blank">Communications and
                                    Writing Center LibGuide</a></li>
                              
                              <li><a href="https://www.mla.org/MLA-Style" target="_blank">MLA Style</a></li>
                              
                              <li>
                                 <a href="https://owl.english.purdue.edu/owl/" target="_blank">Purdue University Online Writing Lab</a>
                                 
                              </li>
                              
                              <li><a href="https://valenciacollege.edu/library/mla-apa-chicago-guides/" target="_blank">Valencia Library
                                    Citation Guide</a></li>
                              
                              <li><a href="http://cdn.wwnorton.com/coursepacks/materials/DOC_GUIDELINES_MLA_2016.pdf" target="_blank">Norton
                                    MLA 8th Ed. Guide</a></li>
                              
                           </ul>
                           
                           
                           <h4>Other Resources</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://web2.uvcs.uvic.ca/elc/studyzone/grammar.htm" target="_blank">English Language Center
                                    Study Zone</a></li>
                              
                              <li><a href="http://owl.excelsior.edu/" target="_blank">Excelsior College Online Writing Lab</a></li>
                              
                              <li><a href="http://www.longman.com/ae/fog_level3/index.html" target="_blank">Focus on Grammar Level 3,
                                    3rd edition</a></li>
                              
                              <li><a href="http://freerice.com/" target="_blank">Free Rice</a></li>
                              
                              <li><a href="http://chompchomp.com/" target="_blank">Grammar Bytes</a></li>
                              
                              <li><a href="http://grammar.quickanddirtytips.com/" target="_blank">Grammar Girl</a></li>
                              
                              <li><a href="http://www.grammar-quizzes.com/" target="_blank">Grammar-Quizzes</a></li>
                              
                              <li><a href="http://grammar.ccc.commnet.edu/grammar/index.htm" target="_blank">Guide to Grammar and
                                    Writing</a></li>
                              
                              <li><a href="http://www.mywritinglab.com/" target="_blank">My Writing Lab</a></li>
                              
                              <li><a href="http://www.varsitytutors.com/practice-tests" target="_blank">Practice Tests</a></li>
                              
                           </ul>
                           
                           
                           <h4>Handouts</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/writing/west/sentence-rules.pdf" target="_blank">Basic
                                    Rules for Making Sentences</a></li>
                              
                           </ul>
                           
                           
                           <h4>Videos</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://www.youtube.com/watch?v=fE0IBPtbY2o" target="_blank">Apostrophes</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=tUSaQ5-mDRI" target="_blank">How to Avoid Plagiarism</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=d2sAGY6viDA" target="_blank">Interjections</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=txK_5awcCnE" target="_blank">Modifiers</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=bI2YHX_KOFo" target="_blank">Nouns</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=ttfS79ej_v0" target="_blank">Verbs</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Reading</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Links</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://www.dartmouth.edu/~acskills/success/reading.html" target="_blank">Active Reading</a>
                                 
                              </li>
                              
                              <li>
                                 <a href="http://guides.library.harvard.edu/sixreadinghabits" target="_blank">Annotating a Textbook</a>
                                 
                              </li>
                              
                              <li><a href="http://www.freereadingtest.com/free-reading-test.html" target="_blank">Fun Reading
                                    Practice</a></li>
                              
                              <li><a href="http://www.myreadinglab.com/" target="_blank">My Reading Lab</a></li>
                              
                              <li><a href="http://users.dhp.com/~laflemm/RfT/Tut2.htm" target="_blank">Reading for Thinking--Fact and
                                    Opinion</a></li>
                              
                              <li><a href="http://www.staples.com/sbd/cre/marketing/technology-research-centers/ereaders/speed-reader/" target="_blank">Speed &amp; Comprehension Practice</a></li>
                              
                              <li><a href="http://www.studygs.net/index.htm" target="_blank">Study Guides and Strategies</a></li>
                              
                              <li><a href="http://wordnetweb.princeton.edu/perl/webwn" target="_blank">Wordnet Online</a></li>
                              
                              <li><a href="http://www2.actden.com/writ_den/index.htm" target="_blank">Writing Den</a></li>
                              
                           </ul>
                           
                           
                           <h4>Handouts</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/writing/west/reading-strategies.pdf" target="_blank">Reading
                                    Strategies</a></li>
                              
                              <li><a href="/documents/students/writing/west/vocabulary-skills.pdf" target="_blank">Vocabulary
                                    Skills</a></li>
                              
                           </ul>
                           
                           
                           <h4>Videos</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://www.youtube.com/watch?v=2QMs24TTZrA&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=12" target="_blank">Author's Purpose</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=-5DMKme6RR8&amp;index=18&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank">Character Analysis</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=Z2GGLbKoy3g&amp;index=44&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank">Character's Perspectives</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=UmQ3ZSf3xSU&amp;index=20&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank">Claims and Support</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=6spWj7Ol3x0&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank">Context Clues</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=n_g7Nq-sTIA&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=3" target="_blank">Literal vs. Inferential</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=42SJTk2XSi4" target="_blank">Main idea and supporting
                                    details</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=jjrwi_FCQS4&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA&amp;index=17" target="_blank">Multiple Main Ideas</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=_AnYSohfjAg&amp;index=25&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank">Narrator's Point-of-View</a></li>
                              
                              <li><a href="https://www.youtube.com/watch?v=NwLrYQuFabA&amp;index=5&amp;list=PLYmYDLb2oJqFmrlRnlKyw8ibFMHBtEyKA" target="_blank">Summarizing</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>EAP Speech and Reading</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Links</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://www.manythings.org/slang/" target="_blank">Common American Slang</a></li>
                              
                              <li><a href="http://www.eslcafe.com/" target="_blank">Dave's ESL Cafe</a></li>
                              
                              <li><a href="http://www.elllo.org/" target="_blank">Ello</a></li>
                              
                              <li><a href="http://www.englishlistening.com/" target="_blank">English Listening</a></li>
                              
                              <li><a href="http://www.npr.org/" target="_blank">NPR</a></li>
                              
                              <li><a href="http://www.esl-lab.com/" target="_blank">Randal's ESL Cyber Listening Lab</a></li>
                              
                              <li>
                                 <a href="http://www.laflemm.com/hmco/RfRonline.html" target="_blank">Reading for Results: Online</a>
                                 
                              </li>
                              
                              <li><a href="http://rong-chang.com/" target="_blank">Rong-Chang</a></li>
                              
                              <li><a href="http://soundsofspeech.uiowa.edu/english/english.html" target="_blank">Phonetics-The Sounds of
                                    Spoken Language</a></li>
                              
                              <li><a href="http://www.ted.com/" target="_blank">TED</a></li>
                              
                           </ul>
                           
                           
                           <h4>Handouts</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/writing/west/ipa-chart.pdf" target="_blank">IPA
                                    Pronunciation Chart</a></li>
                              
                              <li><a href="/documents/students/writing/west/syllable-stress.pdf" target="_blank">Syllable
                                    Stress Rules</a></li>
                              
                              <li><a href="/documents/students/writing/west/write-an-outline.pdf" target="_blank">Write
                                    an Outline</a></li>
                              
                           </ul>
                           
                           
                           <h4>Videos</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://www.youtube.com/watch?v=wZ2o7huX8ww" target="_blank">Talking about Email</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>EAP Writing and Grammar</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Links</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://www.youtube.com/user/JenniferESL" target="_blank">English with Jeniffer</a></li>
                              
                              <li><a href="http://englishpage.com/" target="_blank">English Page</a></li>
                              
                              <li><a href="https://www.ego4u.com/en/cram-up/grammar/tenses" target="_blank">Grammar Charts</a></li>
                              
                              <li><a href="http://www.manythings.org/" target="_blank">Interesting Things for ESL Students</a></li>
                              
                              <li><a href="http://www.englishpage.com/irregularverbs/irregularverbs.html" target="_blank">Irregular Verb
                                    Dictionary</a></li>
                              
                              <li><a href="http://www.e-grammar.org/download/most-common-irregular-verbs.pdf" target="_blank">Irregular
                                    Verb List</a></li>
                              
                              <li><a href="http://www.chompchomp.com/handouts/irregularrules01.pdf" target="_blank">Rules for Using
                                    Irregular Verbs</a></li>
                              
                              <li><a href="http://writing.umn.edu/sws/assets/pdf/verb_tense_chart.pdf" target="_blank">Verb Tense
                                    Chart</a></li>
                              
                           </ul>
                           
                           
                           <h4>Other Resources</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://azarinteractive.com/" target="_blank">Azar Interactive</a></li>
                              
                              <li><a href="http://www.cambridgelms.org/main" target="_blank">Grammar And Beyond</a></li>
                              
                              <li><a href="http://www.myenglishlab.com/" target="_blank">My English Lab</a></li>
                              
                              <li><a href="http://myelt.heinle.com/" target="_blank">Reading Explorer</a></li>
                              
                              <li>
                                 <a href="http://www.cengage.com/cgi-wadsworth/course_products_wp.pl?fid=M20b&amp;product_isbn_issn=9780618444182" target="_blank">Targeting Pronunciation</a>
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Handouts</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/writing/west/rules-for-capitalization.pdf" target="_blank">Rules for Capitalization TipSheet</a></li>
                              
                              <li><a href="/documents/students/writing/west/five-punctuation-patterns.pdf" target="_blank">Five Punctuation Patterns TipSheet</a></li>
                              
                           </ul>
                           
                           
                           <h4>Videos</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://www.youtube.com/watch?v=XaIc633l0hM" target="_blank">Email Dos and Don'ts</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 22 campusID = 3 department_fields = "none" office_fields = "website,
                        email, phone, location, hours" campus_fields = "none"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://chompchomp.com/" class="button btn-block text-center" target="_blank">Need a grammar
                           refresher?<br>Try the MOOC on Grammar Bytes!</a>
                        
                     </div>
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/writing/west/student-resources.pcf">©</a>
      </div>
   </body>
</html>