<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Writing Center  | Valencia College</title>
      <meta name="Description" content="The Writing Center @ Lake Nona wants to help students become better writers. That is our simple but important mission.  Our friendly staff consists of experienced writers, familiar with anything from mastering the basics of grammar to composing and documenting college level essays.">
      <meta name="Keywords" content="writing, center, tutoring, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/writing/lake-nona/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/writing/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/writing/">Writing</a></li>
               <li>Lake Nona</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Writing Center</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Welcome</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Writing Center @ Lake Nona wants to help students become better writers. That
                              is our simple but
                              important mission. Our friendly staff consists of experienced writers, familiar with
                              anything from
                              mastering the basics of grammar to composing and documenting college level essays.
                              We are excited to help
                              Valencia students with any writing assignment for any class. We are also available
                              to offer advice for
                              scholarship essays and speeches.
                           </p>
                           
                           
                           <p>Our center enthusiastically supports students by offering them multiple resources,
                              including Writing
                              Consultations and Grammar Therapy Sessions.
                           </p>
                           
                           
                           <h4>Writing Consultations</h4>
                           
                           <p>One on one writing consultations last between 30 and 45 minutes and can be conducted
                              at any time
                              throughout the writing process. These sessions are focused on a particular writing
                              assignment or essay and
                              can assist students in many areas, including brainstorming, thesis development, essay
                              organization,
                              paragraph development, revision and documentation. Students can maximize their time
                              by coming prepared
                              with all materials relevant to the assignment, including assignment instructions,
                              notes from class, and
                              textbooks.
                           </p>
                           
                           
                           <h4>Grammar Therapy</h4>
                           
                           <p>In addition to writing consultations, the Writing Center @ Lake Nona also offers students
                              individualized
                              15-30 minute Grammar Therapy sessions, dedicated to helping students become more confident
                              in the areas
                              they feel least comfortable. For many students, these areas include:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Comma use</li>
                              
                              <li>Coordination and subordination</li>
                              
                              <li>Sentence structure / Syntax</li>
                              
                              <li>Pronouns</li>
                              
                              <li>Verb tenses</li>
                              
                              <li>Prepositions</li>
                              
                              <li>Fragments</li>
                              
                              <li>Subject-verb agreement</li>
                              
                              <li>Modifiers</li>
                              
                              <li>Apostrophes</li>
                              
                              <li>And more!</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Summer 2017 Hours</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 10:00am - 6:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 10:00am - 6:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 10:00am - 6:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 10:00am - 6:00pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 8:00am - 12:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <p><strong>NOTE: Tutor availability is not guaranteed!</strong> If we receive advance notice of tutor
                              absence, we will post the notice on the appropriate subject page. We encourage you
                              to call if you need
                              last-minute details (408-582-7106 between 9:00am and 5:00pm).
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://valenciacollege.edu/library/mla-apa-chicago-guides/">MLA/APA/Chicago Citation
                                    Guides</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        
                        <br>
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" class="button btn-block text-center">Directions</a> <a href="http://valenciacollege.edu/map/lake-nona.cfm" class="button btn-block text-center">Campus Map</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                        
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/"><i class="social_facebook"></i>&nbsp;Lake Nona
                        on Facebook</a>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/writing/lake-nona/index.pcf">©</a>
      </div>
   </body>
</html>