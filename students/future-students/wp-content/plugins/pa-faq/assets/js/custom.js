jQuery(document).ready(function(){
    
    jQuery('.faq-content').hide();
    
    jQuery('.faq .entry-title').click(function(){
        if(jQuery(this).hasClass('faq-close')){
            
            jQuery('.faq-open').removeClass('faq-open').addClass('faq-close');
            jQuery('.faq-content').slideUp();
            
            jQuery(this).parents('.faq').first().find('.faq-content').slideDown();
            jQuery(this).removeClass('faq-close').addClass('faq-open');
            
        }else{
            jQuery(this).parents('.faq').first().find('.faq-content').slideUp();
            jQuery(this).removeClass('faq-open').addClass('faq-close');
        }
    }).addClass('faq-close');
    
});