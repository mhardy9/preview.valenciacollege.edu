<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Lab Hours | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/labs/lake-nona.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/labs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Lab Hours</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/labs/">Labs</a></li>
               <li>Campus Lab Hours</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>Lake Nona Campus</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">LABS 
                                    OPEN TO ALL STUDENTS 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Lab</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Lab Manager</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Main Purpose</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Hours Sessions I &amp; II</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3>
                                          <strong><i>Building</i></strong>
                                          
                                          
                                       </h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Webpage</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p> <strong>Atlas Access Lab</strong> 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> Melinda Smith </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> Open lab for any student to conduct educational and/or career related research and
                                       planning.&nbsp; Open use includes access to Atlas accounts with staff on hand to assist
                                       with course registration, class schedules, FAFSA, and printing Valencia College parking
                                       permit applications. 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <div>
                                       <br>
                                       Please visit our website for our current hours
                                    </div>                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 1-147 </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> <a href="../student-services/atlas-access-labs.html">LNC Atlas Access Lab</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>Library</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <div>Michael Blackburn</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> Computer workstations open to all students with an Atlas account.&nbsp; Librarians help
                                    students locate, access, evaluate, and cite information and sources for research papers.&nbsp;
                                    Open use also includes access to the internet, printing, copying, and some course
                                    specific software. 
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <div>Please visit our website for our current hours </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <div>1-330 </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><a href="../library/locations/lake-nona-library.html">Library</a></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       <strong>Open Access Computer Lab</strong> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <div>Michael Blackburn</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <p>Computer workstations open to all students with an Atlas account.&nbsp; Open use includes
                                                   access to the internet, printing, copying, scanning, and some course specific software.
                                                </p>
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <div>Please visit our website for our current hours </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> 
                                    <div>1-230 </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       <a href="../../../lakenona/computerlab.html">Open Access Computer Lab</a> 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="#top">TOP</a></p>
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/labs/lake-nona.pcf">©</a>
      </div>
   </body>
</html>