<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Lab Hours | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/labs/winterpark.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/labs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Lab Hours</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/labs/">Labs</a></li>
               <li>Campus Lab Hours</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>Winter Park Campus</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> LABS OPEN TO ALL STUDENTS</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Lab</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Main Purpose</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Hours Sessions I &amp; II</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Building</i></strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Library</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Computer workstations open to all students with an active Atlas account. Internet,
                                       databases, and word processing with printing access available. Research assistance
                                       is also available. For further information, please call 407-582-6814.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 7:00pm 
                                    </p>
                                    
                                    <p> F<br>
                                       8:00am - 5:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>140</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Career Center<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Provides career advising and research for Valencia students and general population</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 6:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       9:00am - 5:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>214</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Atlas Access Lab<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to all students. Provides for homework projects, Internet research and other
                                       assignments.<br>
                                       Photo ID required. 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 6:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       9:00am - 5:00pm<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>217</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">LABS OPEN TO STUDENTS IN SPECIFIC DISCIPLINES</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Lab</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Main Purpose</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Hours Sessions I &amp; II</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Building</i></strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Communications Student Support Center (CSSC) <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Supports students enrolled in Prep English I &amp; II and Reading 1 &amp; 2 course. <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 7:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       8:00 - 12:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>136</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Math Support Center<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>For students enrolled in any math course. Supports students enrolled in Prep Math
                                       1 &amp; 2 and college level math courses.<br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 7:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       8:00am - 12:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>138</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/labs/winterpark.pcf">©</a>
      </div>
   </body>
</html>