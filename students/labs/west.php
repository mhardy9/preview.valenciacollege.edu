<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Lab Hours | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/labs/west.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/labs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Lab Hours</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/labs/">Labs</a></li>
               <li>Campus Lab Hours</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>West Campus</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">LABS 
                                    OPEN TO ALL STUDENTS
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Lab</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Main Purpose</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Hours Sessions I &amp; II</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Building</i></strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Atlas Access Lab</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>The Atlas Access Lab is open to all students as well as potential students (with a
                                       photo ID). Open use includes access to the internet and Atlas account with staff on
                                       hand to assist. The intention of this lab is to help students with their educational
                                       planning as opposed to their class work.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 6:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       9:00am - 5:00pm
                                    </p>
                                    
                                    <p>Summer<br>
                                       8am - 12 Noon
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>SSB-172</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Communications Center</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Open to all students. Tutoring Available by appointment.<br> Software: Skills bank (grammar, reading,vocabulary,etc) <br> ELLIS (English as a 2nd language) Speed Reader, Microsoft Word, Internet Access,
                                       AZAR (grammar program)
                                    </p>
                                    <a href="../learning-support/communications/index.html">
                                       <p>Communication Center</p></a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M - R<br> 8:00am - 8:00pm
                                    </p>
                                    
                                    <p>F<br>8:00am - 5:00pm
                                    </p>
                                    
                                    <p>Sat<br>9:00 am - 2:00pm
                                    </p>
                                    
                                    <p>Sun<br>Closed
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>5-155</p>
                                 </div>
                                 
                              </div>              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Computer Access Lab</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Open to all students with an active Atlas account. Internet &amp; word processing for
                                       research papers. Atlas &amp; e-mail, access to course software. Lab staff available to
                                       help students with software applications. Software catalog available on <a href="../library/west/cal/index.html">Computer Access Lab</a> web page. 
                                    </p>
                                    
                                    <p>Phone 407-582-1646</p>                  
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Summer HoursÂ&nbsp;</strong><br>
                                       M-Th <br>
                                       7:30am-10pm
                                    </p>
                                    
                                    <p>                    Friday <br>
                                       7:30am-12:00pmÂ&nbsp; 
                                    </p>
                                    
                                    <p>Sat - Sat.<br>
                                       closedÂ&nbsp;
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>6-101</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Library</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Computer workstations open to all students with an active Atlas account. Internet
                                       &amp; word processing are available for research papers. Librarians help students locate,
                                       access, evaluate and cite information &amp; sources for research papers.
                                    </p>
                                    
                                    <p>Phone 407-582-1432</p>
                                    
                                    <p><a href="../library/index.html">Library Web Site</a></p>                  
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       7:30am - 9:00pm 
                                    </p>
                                    
                                    <p> F<br>
                                       7:30am - 5:00pm
                                    </p>
                                    
                                    <p> S<br>
                                       9 :00am - 1:00pm
                                    </p>
                                    
                                    <p> Su<br>
                                       2:00pm - 6:00p
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">6-201</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Writing Center</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The <a href="../learning-support/communications/writing.html">Writing Center</a> is staffed with faculty-level English, Reading, and EAP instructors. The instructors
                                       go over essays and other writing assignments with students. Open to all students.
                                       
                                    </p>
                                    
                                    <p>Appointments are required. </p>
                                    
                                    <p>Phone 407-582-1812</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>M - R <br>
                                       8:00 am - 8 pm<br>
                                       <br>
                                       F <br>
                                       8:00 am - 5 pm<br>
                                       <br>
                                       Sat. <br>
                                       9:00 am - 2 pm<br>
                                       <br>
                                       Sun. <br>
                                       Closed
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p> 5 - 155A<br>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> LABS OPEN TO STUDENTS IN SPECIFIC DISCIPLINES</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Lab</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Main Purpose</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Hours Sessions I &amp; II</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Building</i></strong></p>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Testing Center </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The <a href="../learning-support/testing/index.html">Testing Center </a> is open to  students for testing. Valencia ID required for test to be administered.
                                    </p>
                                    
                                    
                                    <p>Phone 407-582-1323</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">M - R <br>
                                    8:00 am - 8 pm<br>
                                    <br>
                                    F <br>
                                    8:00 am - 5 pm<br>
                                    <br>
                                    Sat. <br>
                                    9:00 am - 2 pm<br>
                                    <br>
                                    Sun. <br>
                                    Closed
                                 </div>
                                 
                                 <div data-old-tag="td">11-142</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Engineering, Computer Programming, and Technology</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students in Engineering &amp; Drafting Construction (auto-CAD), Computer Programming
                                       and Technology&nbsp; disciplines.&nbsp;
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td"><a href="../../../west/engineering/LabHours.html">http://valenciacollege.edu/west/engineering/LabHours.cfm</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Nursing Arts Lab </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students enrolled in Nursing courses.&nbsp; </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M- 8:00am-5:00pm</p>
                                    
                                    <p>T-W 7:30am-6:00pm</p>
                                    
                                    <p>R- 7:30am - 5:30 pm</p>
                                    
                                    <p>Sat. Closed</p>
                                    
                                    <p>Sun 10:00am -3:00pm </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>HSB 225 and HSB228 </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Earth Science</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Scheduled lab open to students enrolled in science courses.&nbsp; Teaches difference between
                                       rocks/minerals.&nbsp; No specific software.&nbsp; Work mostly with CD-ROMs and books.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>M-R<br>
                                       8:00am - 9:00pm
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1-228</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Electronics </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students enrolled in engineering technology courses only to perform laboratory
                                       exercises related to their required courses.&nbsp; <br>
                                       <br>
                                       (Summer hours: Fridays open 9am -1pm)
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td"> M - R&nbsp; <br>
                                    9:00am to 9:00pm<br>
                                    <br>
                                    F <br>
                                    Spring &amp; Fall semesters only <br>
                                    9:00am to 2:00pm 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>9-211<br>
                                       9-219
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>French &amp; Spanish Tutors</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students enrolled in French and Spanish courses.</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>M-F<br>
                                       Hours vary with class schedule.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>5-231</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Graphics</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students enrolled in Graphic Technology courses.&nbsp; Uses software related to
                                       this field as fundamental part of their education.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>9:00am - 9:00pm<br>
                                       Monday - Friday
                                    </p>
                                    
                                    <p>9:00am - 3:00pm<br>
                                       Saturdays
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>3-151</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Math  Center</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The <a href="../learning-support/math/index.html">Math Center</a> houses: 
                                    </p>
                                    
                                    <p>Tutoring Center: Where walk-in tutoring is available for mathematics and appointments
                                       are made on an as-available basis for one-on-one tutoring for a variety of other subjects.
                                    </p>
                                    
                                    <p>Math Open Lab: Open to students currently enrolled in MAT0018C, MAT0022C, MAT0028C,
                                       MAT1033C and STA 1001C.&nbsp; Uses interactive math programs and online content to emphasize
                                       what is learned in class.
                                    </p>
                                    
                                    <p> Math Connections: A learning community for students of Developmental Math I, DEvelopmental
                                       Math II, Developmental Math Combined,  Intermediate Algebra, and Introduction to Statistics.
                                       
                                    </p>
                                    
                                    <p>Hands On Learning: Assists students in understanding mathematical concepts using manipulatives
                                       such as board games and every day objects. 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Math Center (Open Lab) </strong><strong>Spring  hours : </strong></p>
                                    
                                    <p>Monday to Thursday: 9:00 am to 9:00 pm</p>
                                    
                                    <p>Friday: 9:00 am to 7:00pm</p>
                                    
                                    <p>Saturday: 10:00am to 3:00 pm</p>
                                    
                                    <p><strong>Tutoring Center</strong></p>
                                    
                                    <p>Monday to Thursday: 8:00 am to 8:00 pm</p>
                                    
                                    <p>Friday: 8:00am to 7:00pm</p>
                                    
                                    <p>Saturday: 10:00am to 3:00 pm</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>7- 240 </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>OST/Accounting</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Open to students in Office Systems Technology and Accounting Majors or courses. Reinforces
                                       skills in typing and use of desktop publishing software.
                                    </p>
                                    
                                    <p>*Schedule will change each semester and varies between rooms.</p>
                                    
                                    <p>Call the lab at 407-582-1730 for information </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M - R<br>
                                       8:00am - 8:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       8:00am - 12:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>7-144</p>
                                    
                                    <p>*Open lab times will vary each semester.</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Honors Resource Center Lab</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>This LAB is for students in the Honors program. Access is granted by the<br>
                                       Honors Department.
                                    </p>
                                    
                                    <p>Lab computers are managed by A/V services x1488<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Access is same as Library Hours of Operation</div>
                                 
                                 <div data-old-tag="td">6-201B</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/labs/west.pcf">©</a>
      </div>
   </body>
</html>