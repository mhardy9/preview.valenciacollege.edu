<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Lab Hours | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/labs/osceola.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/labs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Lab Hours</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/labs/">Labs</a></li>
               <li>Campus Lab Hours</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>Osceola Campus</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">LABS 
                                    OPEN TO ALL STUDENTS 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Lab</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Lab Manager</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Main Purpose</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Hours Sessions I &amp; II</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3>
                                          <strong><i>Building</i></strong>
                                          
                                          
                                       </h3>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <h3><strong><i>Webpage</i></strong></h3>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Atlas Access Lab</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Catherine Rivas</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to all students. <br>
                                       Open Lab for any    registered student. Cyber Adviser, Cyber Registration, Cyber Portfolio,
                                       CHOICES, FAFSA,&nbsp; Internet access, E-mail and Microsoft Office available.&nbsp;    Supports
                                       SLS1122.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>M-Th: 8:00am - 7:00pm<br>
                                       Fri: 9:00am – 5:00pm
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2-105G</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="https://atlas.valenciacollege.edu">Atlas</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Library</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Sarah Dockray </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to all students. Internet access and online databases    available for research
                                       purposes. Online databases can be accessed by remote    access. The Library provides
                                       students a large computer area    with access to general software, course specific
                                       software, scanning and    printing.&nbsp; 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">M-Th: &nbsp;7am – 9:45pm<br>
                                    Fri: 7am – 5pm<br>
                                    Sat: 8am– 12pm<br>
                                    <br>
                                    Call extension 4155 for summer hours.
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>4-202</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="../library/index.html">Osceola Campus Library</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Learning Center</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Marie Brady</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>The Learning Center houses the writing center, Language Lab and reading areas for
                                       the Osceola Campus. AS tutoring and SL sessions are help in this area as well. 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-Th: 8am–8pm<br>
                                       Fri: 8am – 5pm<br>
                                       Sat: 8am – 12pm
                                    </p>
                                    
                                    <p>(Sat. in Depot) <br>
                                       <br>
                                       <strong>Visit Learning Center web page for summer hours</strong> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>3-100</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="../../../osceola/learningcenter/index.html">Osceola Campus Learning Center</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>The DEPOT</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>James Coddington, 
                                       Dan Derosa,<br>
                                       Nelson Torres
                                       <br>
                                       
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>The Depot is the primary tutoring area for the campus and also houses our developmental
                                       math lab area. Group study rooms are available for checkout as well. 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Visit the DEPOT for current  hours</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>4-121</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="../../../osceola/math/mathdepot.html">Osceola Math Depot </a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Career Development Services Lab </strong></p>
                                 </div>
                                 
                                 
                                 <div data-old-tag="td">
                                    <p>Open Lab for any    registered or potential student to conduct educational/career
                                       related    research and planning. &nbsp;&nbsp; Cyber Adviser, Cyber Registration, Cyber    Portfolio,
                                       CHOICES, FAFSA,&nbsp; Internet access, E-mail and Microsoft    Office.&nbsp; Supports SLS1122.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>M-Th: 8am - 6pm<br>
                                       Fri: 9am - 5pm
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1-125</p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="#top">TOP</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> 
                                    <p>LABS 
                                       OPEN TO STUDENTS IN SPECIFIC DISCIPLINES
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Language Learning Lab</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Marie Brady </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Reserved    for students in EAP, Spanish, and French courses.&nbsp; We provide the tools
                                       you to be    successful.&nbsp; These include: computers    with language learning software,
                                       word processing capabilities, and Internet    access; reference books; and technical
                                       and academic assistance. For more information visit our    website at: <a href="../../../osceola/mainlab/language.html">Language Lab</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Visit Learning Center web page for hours of operation. </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>3-100</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="../../../osceola/learningcenter/index.html">Osceola Campus Learning Center</a> 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Academic Systems Lab    for Math.</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Andi Berry</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students    enrolled in math, Interactive Mathematics and AS Algebra sections.&nbsp;
                                       Instructional assistants and student tutors available for tutorial help.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Call extension 4835    for current open hours.</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2-142 &amp; 144</p>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Graphics Lab</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Todd Ribardo </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to students enrolled in Graphics Technology, Art and Design    courses. The lab
                                       is equipped with Apple Macintosh desktops, the latest    industry standard software
                                       such as Adobe Photoshop, Illustrator, InDesign,    Quark Xpress, Dreamweaver, Flash
                                       and Fireworks. The lab also provides an    oversized scanner, laser and photo quality
                                       inkjet printing. Instructional    assistance is provided. 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Call extension 4953 for current open hours.</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2-246</p>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/labs/osceola.pcf">©</a>
      </div>
   </body>
</html>