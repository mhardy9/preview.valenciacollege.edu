<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Lab Hours | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/labs/east.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/labs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Campus Lab Hours</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/labs/">Labs</a></li>
               <li>Campus Lab Hours</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="index.html">
                           </a>
                        
                        <h2>East Campus</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">LABS OPEN TO ALL STUDENTS</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Lab</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Main Purpose</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Hours Sessions I &amp; II</i></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong><i>Building</i></strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Communications<br>
                                       Support Center
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to all students.&nbsp; 
                                       Tutoring available by appointment.&nbsp; Software: Skills bank 
                                       (grammar, reading, vocabulary, etc), ELLIS (English as a 2<sup>nd</sup> language) Speed Reader, Microsoft Word, Internet Access, AZAR 
                                       (grammar program)
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-F<br>
                                       8:00am - 9:00pm
                                    </p>
                                    
                                    <p>S<br>
                                       8:00am - 2:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Bldg. 4-W</p>
                                    
                                    <p>Instr. Media Center</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Library Computer Lab </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>The Library Computer Lab  is the library's 
                                       academic research lab available for Valencia students.&nbsp; Access 
                                       to the Internet and specialized reference databases (EBSCOhost, 
                                       FirstSearch, NewsBank's InfoWeb, LINCCweb).&nbsp; Workstations 
                                       dedicated to check e-mail are available.&nbsp; Research assistance 
                                       and technical support available.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       7:00am-9:45pm
                                    </p>
                                    
                                    <p> F<br>
                                       7:00am-11:45am
                                    </p>
                                    
                                    <p>Sat<br>
                                       8:00am-1:45pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Bldg. 4</p>
                                    
                                    <p>2<sup>nd</sup> Floor
                                    </p>
                                    
                                    <p>LRC</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Math Support Center</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to all students.&nbsp; 
                                       Tutors available on a 1<sup>st</sup>-come 1<sup>st</sup>-served 
                                       basis, computer programs available for prep math and college 
                                       algebra.&nbsp; Info sheets associated with Prep I and II.&nbsp; Practice 
                                       State Competency tests, Videos available for checkouts Calculators 
                                       TI-82 and TI 83, Textbooks.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       7:30am - 8:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       7:30am - 6:00pm
                                    </p>
                                    
                                    <p>S<br>
                                       9:00am - 1:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>4-108</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Student Computer Center</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to all students.&nbsp; 
                                       
                                       Open use/including access to Internet.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       7:00am-9:45pm
                                    </p>
                                    
                                    <p> F<br>
                                       7:00am-11:45am
                                    </p>
                                    
                                    <p>Sat<br>
                                       8:00am-1:45pm
                                    </p>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>4-123</p>
                                    
                                    <p>4-123B</p>
                                    
                                    <p>4-122</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Atlas Access Lab</div>
                                 
                                 <div data-old-tag="td">Open Lab for any registered student to    conduct educational/career related research
                                    and planning. Open use includes    Access to the Internet and Atlas account Staff
                                    on hand to assist. Also    supports <strong>SLS1122</strong>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 6:00pm
                                    </p>
                                    
                                    <p>                      F <br>
                                       9:00am - 5:00pm<br>                  
                                       <br>
                                       <strong>Summer</strong><br>
                                       8am – 12 Noon
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">5-213</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"> 
                                    <h3>LABS 
                                       OPEN TO STUDENTS IN SPECIFIC DISCIPLINES
                                    </h3>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Business Service Lab</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>For Business Students.&nbsp; 
                                       Students have access to all software used in all business, 
                                       computer office systems, and accounting classes taught on 
                                       East Campus.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Fall &amp; Spring</strong><br>
                                       M - R<br> 
                                       8am - 9:45pm<br>
                                       F<br> 
                                       8am - 7pm<br>
                                       Saturday<br> 
                                       9am - 5pm
                                    </p>
                                    
                                    <p><strong>Summer</strong><br>
                                       M - R<br> 
                                       8:00am - 9:45 pm<br>
                                       F<br> 
                                       8:00am - 12:00pm<br>
                                       Saturday, closed
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Bldg. 4</p>
                                    
                                    <p>2<span><sup>nd</sup></span> Floor
                                    </p>
                                    
                                    <p>LRC</p>                  
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Foreign Language Labs</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Open to any student 
                                       enrolled in language courses.&nbsp; Audio programs, reference books, 
                                       Instructional assistance available. 
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       8:00am - 9:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       8:00am - 12:00pm
                                    </p>
                                    
                                    <p>S<br>
                                       8:00am - 12:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>4-104</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Graphics Technology 
                                       Lab
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Designed for AS Degree 
                                       students in the Graphics Technology Program.&nbsp; Students have 
                                       access to specialized software with lab assistance available.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       9:00am - 9:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       9:00am - 9:00pm
                                    </p>
                                    
                                    <p>S<br>
                                       9:00am - 12:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>1-213 &amp; 214</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Theater Tech CAD &amp; 
                                       Audio Lab
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Designed for AS Degree 
                                       Students in Arts &amp; Entertainment Graphics &amp; Audio 
                                       Technology disciplines only.&nbsp; Students have access to specialized 
                                       software.
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>M-R<br>
                                       9:00am - 5:00pm
                                    </p>
                                    
                                    <p>F<br>
                                       9:00am - 1:00pm
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>2-164</p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/labs/east.pcf">©</a>
      </div>
   </body>
</html>