<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Answer Center | Valencia College</title>
      <meta name="Description" content="Answer Center">
      <meta name="Keywords" content="answer center, valencia college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/answer-center/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/answer-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Answer Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Answer Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Answer Center</h2>
                        				
                        <p>The Answer Center should be your first stop in seeking answers to many of your questions.
                           At the Answer Center you will find cross-trained Student Services Advisors knowledgeable
                           with admissions, financial aid, advising and many other areas. They are available
                           to work with you on a walk-in basis.
                        </p>
                        				
                        <p>The Answer Center assists individual students in reaching their educational goals
                           by incorporating the <a href="../lifemap/index.php">LifeMap</a> model using a holistic, student-centered approach.
                        </p>
                        				
                        <p>Answer Center Advisors are there to guide students from application to graduation.
                           They have the ability to:
                        </p>
                        				
                        <ul>
                           					
                           <li>Clarify initial educational goals</li>
                           					
                           <li>Follow up on the admissions process</li>
                           					
                           <li>Review and determine financial aid needs</li>
                           					
                           <li>Describe the assessment process and direct students to preparation materials</li>
                           				
                        </ul>
                        				  
                        <hr class="styled_2">
                        				
                        <h3>Related Links</h3>
                        				
                        <ul>
                           					
                           <li>
                              						<a href="/students/catalog/index.php">Catalog</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="https://valenciacollege.edu/calendar/">College Calendar</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="/academics/competencies/index.php">Core Competencies</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="/students/offices-services/labs/index.php">Computer Labs</a>
                              					
                           </li>
                           					
                           <li>
                              						<a href="/students/offices-services/library/index.php">Library</a>
                              					
                           </li>
                           				
                        </ul>
                        				  
                        <hr class="styled_2">
                        				
                        <h3>Hours &amp; Locations</h3>
                        				
                        <p>Hours<br>
                           				Monday - Thursday: 8 a.m. to 6 p.m.<br>
                           				Friday: 9 a.m. to 5 p.m.
                        </p>
                        				
                        <p><strong>East Campus</strong><br>
                           				Bldg 5, Rm 211
                        </p>
                        				
                        <p><strong>Lake Nona Campus</strong><br>
                           				Bldg 1, Rm 149
                        </p>
                        				
                        <p><strong>Osceola Campus</strong><br>
                           				Bldg 2, Rm 105
                        </p>
                        				
                        <p><strong>West Campus</strong><br>
                           				SSB, Rm 106
                        </p>
                        				
                        <p><strong>Winter Park Campus</strong><br>
                           				Bldg 1, Rm 206
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/answer-center/index.pcf">©</a>
      </div>
   </body>
</html>