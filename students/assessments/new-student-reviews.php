<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>New Student Reviews  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/new-student-reviews.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>New Student Reviews </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Assessment Services</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_4">
                              
                              <li> Be sure to thoroughly read and complete every step in your Part 1: Online Orientation
                                 course.
                              </li>
                              
                              <li> Failure to submit ALL answers to your reviews will result in a delay in registering
                                 for Part 2: On-Campus Orientation.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>New Student Induction Process</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>Take a look at our&nbsp;<a href="/documents/students/assessments/new-student-induction/documents/6.18.15-enrollment-checklist-revise-final.pdf" target="_blank">Enrollment Checklist</a></strong> <i class="far fa-file-pdf-o" aria-hidden="true"></i>
                              
                           </p>
                           
                           <img src="/_resources/img/students/assessments/enrollment-checklist.png" alt="Enrollment Checklist Image"><br><br>
                           
                           
                           <p>You may be required to submit placement scores such as PERT, SAT or ACT if you do&nbsp;<u>not</u>&nbsp;meet the following criteria:
                           </p>
                           
                           <ul>
                              
                              <li>You are an Active Military Member.</li>
                              
                              <li>You started 9th grade in a Florida Public high school or charter school in 2003 or
                                 after&nbsp;<strong><u>AND</u></strong>&nbsp;did or will graduate with a&nbsp;<u>standard high school diploma</u>&nbsp;from a Florida Public High School or Charter School (GED does not qualify).
                              </li>
                              
                              <li>You are transferring college level coursework in English and Math with a grade of
                                 C or higher.
                              </li>
                              
                           </ul>
                           
                           <p>If you said “no” to any of the options above, you ARE&nbsp;<u>required</u>&nbsp;to provide placement scores.
                           </p>
                           
                           <p><em><strong>Dual Enrollment and Bridges candidates may be required to take the PERT for program
                                    eligibility.</strong></em></p>
                           
                           <ul>
                              
                              <li>If you have ACT or SAT scores that are less than two years old and they are high enough
                                 to use for placement, send them to Valencia.
                              </li>
                              
                           </ul>
                           
                           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Click&nbsp;<a href="https://valenciacollege.edu/assessments/placement-chart.cfm">here</a>&nbsp;to view the scores needed for SAT or ACT placement
                           </p>
                           
                           <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Click&nbsp;<a href="https://valenciacollege.edu/assessments/requesting-scores.cfm">here</a>&nbsp;to find out how to send your SAT/ACT scores.&nbsp;
                           </p>
                           
                           <p>Please note, it takes 5 weeks to receive and process SAT and ACT scores.</p>
                           
                           <ul>
                              
                              <li>If you do not wish to wait for your official ACT or SAT scores to arrive, you may
                                 take the&nbsp;<a href="https://valenciacollege.edu/assessments/pert/taking-the-exam.cfm" target="_blank">PERT</a>. The PERT is free of charge and can be taken at any campus&nbsp;<a href="/students/assessments/">Assessment Services</a>&nbsp;office (no appointment necessary).
                              </li>
                              
                           </ul>
                           
                        </div>         
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/new-student-reviews.pcf">©</a>
      </div>
   </body>
</html>