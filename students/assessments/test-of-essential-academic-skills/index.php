<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/test-of-essential-academic-skills/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Teas</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Test of Essential Academic Skills (TEAS)</h2>
                        
                        <p>**TEAS V will no longer be available after August 30, 2016!** </p>
                        
                        <p>Please be sure to purchase the new study material (6th edition) if you plan to test
                           <u>on or after</u> August 31, 2016.
                        </p>
                        
                        <h3>At a Glance</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Hours Available</h3>
                                    
                                    <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                                    Mon-Thurs: 8 AM -2 PM <br>
                                    Fridays: 9 AM - 1 PM            
                                    
                                    
                                    <p><strong>Note:</strong> Offered on East, West, and Osceola Campus between the above hours. Also available
                                       at the Lake Nona and Winter Park Campuses by appointment only.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Test Fees</h3>
                                    
                                    
                                    <p>**If you are testing <u>on or after</u> August 31, 2016, be sure to purchase the 6th edition study material. 
                                    </p>
                                    
                                    <p>              Valencia Student- $65 per test</p>
                                    
                                    <p>Non-Valencia Student- $85 </p>
                                    
                                    <p><a href="../../business-office/payments.html">Valencia's Payment Method</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Retake Wait Period</h3>
                                    
                                    <p>              30 days (May take on the 31st day) <br>
                                       Maximum of 3 times in 12 months; this includes tests taken at other institutions.
                                       Number of attempts is calculated from the anniversary of your first attempt. 
                                    </p>
                                    
                                    <p>TEAS V scores will not be accepted by Valencia's Health Sciences after September 15,
                                       2017. 
                                    </p>
                                    
                                    <p>**Testing for other institutions is allowed. However, any test scores that do not
                                       meet Valencia’s testing policy (above) will <em><strong>NOT</strong></em> be valid for Valencia Health Science admission. Students are advised to get approval
                                       from their institutions before testing at Valencia. &nbsp;
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Time Required</h3>
                                    209 minutes
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Technical difficulties inherent with the internet, software or transmission may occur
                                       during your testing experience and are unpredictable. If unexpected technical difficulties
                                       occur, Proctors will make every attempt to promptly resume your assessment. Extended
                                       delays in testing may require rescheduling.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>About the TEAS V- <span>TEAS V scores will not be accepted by Valencia's Health Sciences after September 15,
                              2017. </span>
                           
                        </h3>
                        
                        <p><span>**TEAS V will no longer be available after August 30, 2016**</span></p>
                        
                        <p>If you plan to test<u> on or after</u> August 31, 2016, please be sure to purchase the new 6th edition study guide <a href="https://www.atitesting.com/ati_store/product.aspx?zpid=1483" target="_blank">here</a>.
                        </p>
                        
                        
                        <p>For information regarding the<strong> new ATI TEAS</strong>, please review the content changes<span> <a href="http://atiteas.info/wp-content/uploads/2015/09/TEAS-V-Content-Comparison-Table.pdf" target="_blank">here</a>. </span></p>
                        
                        <p>Valencia Health Sciences information and website: <a href="../../west/health/admissionupdates.html">http://valenciacollege.edu/west/health/admissionupdates.cfm</a></p>
                        
                        <p>The Test of Essential Academic Skills (TEAS) is required to enter most  Health Sciences
                           programs.
                        </p>
                        
                        <ul>
                           
                           <li>Valencia College offers TEAS V (version five) for entrance into the  nursing and other
                              Allied Health Programs. <span>After August 30, 2016 the new ATI TEAS will be proctored. </span>
                              
                           </li>
                           
                        </ul>
                        
                        <p>The ATI TEAS is a computerized test consisting of 170 multiple choice questions  with
                           a time limit of 209 minutes for the test.
                        </p>
                        
                        <ul>
                           
                           <li>Reading contains 53  questions and is timed at 64 minutes</li>
                           
                           <li>Mathematics contains  36 questions and is timed at 54 minutes </li>
                           
                           <li>Science contains 53  questions and is timed at 63 minutes </li>
                           
                           <li>English and Language  Usage contains 28 questions and is timed for 28 minute.</li>
                           
                        </ul>        
                        
                        <p>The ATI TEAS starting after August 30, 2016 will have new content. Click <span><a href="http://atiteas.info/wp-content/uploads/2015/09/TEAS-V-Content-Comparison-Table.pdf" target="_blank">here</a> </span>for those changes. 
                        </p>        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/test-of-essential-academic-skills/index.pcf">©</a>
      </div>
   </body>
</html>