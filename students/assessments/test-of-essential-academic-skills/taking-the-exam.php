<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/test-of-essential-academic-skills/taking-the-exam.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/test-of-essential-academic-skills/">Teas</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Taking the TEAS</h2>
                        
                        <h2>TEAS Instructions</h2>
                        
                        <p> To take the test in the Assessment Center, please follow the steps below:</p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div><strong>Create a log in at ATItesting.com</strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Click on ”Create an account”</li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> Follow prompts </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>For <strong>&nbsp;Institution </strong>select: Valencia College ADN
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> Create a user name and password</li>
                                       
                                       <li>Do NOT select a test, the Assessment Center will load the test for you </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> You’ll need to remember or have the user ID and password you created on the date
                                          of the test, so you may want to write it down.
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p><strong>Pay for the test (You must pay the TEAS fee every time you take the test)</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> Bring a Valencia College id or current and original <u>government issued</u> photo ID 
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> You will be given a referral at the Assessment Center to take to the business office</li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> Pay for the test at the business office </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Payments are non refundable, we suggest you pay on the same day you plan to take the
                                          test. 
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Test Day (The test takes approximately 3.5 hours) </strong></div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> Bring a Valencia College id or current and original <u>government issued</u> photo ID 
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li> The Assessment Center will provide pencils and unlimited scratch paper </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <ul>
                                       
                                       <li>Take the test in the Assessment Center </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>The TEAS test is always proctored as a package, you <strong>cannot</strong> take specific sections of the test. 
                        </p>
                        
                        <p>You will recieve a copy of your scores immediately after testing.</p>
                        
                        <p>Your TEAS scores will be entered into your Valencia Account immediately after testing.</p>
                        
                        <p>The Assessment Center does <strong>NOT</strong> set the minimum scores needed for any Health Sciences programs. To know what the
                           scores needed for your program, please contact the Health Sciences department or attend
                           their Health Sciences Information Session. For more information about Health Sciences
                           please visit their website: http://valenciacollege.edu/west/health/
                        </p>
                        
                        <ul>
                           
                        </ul>        
                        <h3>Preparing for the TEAS V</h3>
                        
                        <ul>
                           
                           <li>Manuals and test packages are available at <a href="http://www.atitesting.com/ati_store/">ATI testing</a> online store.
                           </li>
                           
                           <li>The TEAS V manual is available at Valencia Campus Book stores.</li>
                           
                        </ul>       
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/test-of-essential-academic-skills/taking-the-exam.pcf">©</a>
      </div>
   </body>
</html>