<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Taking the CPT-I  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments, cpt, exam">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/cpt-i.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Taking the CPT-I </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>At a Glance</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              
                           </p>
                           
                           <h3>Hours Available</h3>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>All Assessment Centers Closed September 4. East Campus closed August 4.</strong><p></p>
                           Mon-Thurs: 8 AM - 5 PM<br>
                           Friday: 9 AM - 4 PM<br><br>
                           
                           
                           <h3>Retake Wait Period</h3>
                           
                           
                           <p>1 Day</p>
                           
                           
                           <h3>Test Fees</h3>
                           
                           Must pay application fees<br>
                           No Charge 1st attempt, $10 Retake fee<br><br>
                           <a href="/students/office-services/business-office/payments.cfm">Valencia's Payment Method</a><br>
                           
                           
                           <h3>Time Required</h3>
                           No Time Limit<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> Technical difficulties inherent with the internet, software or transmission may occur
                           during your testing experience and are unpredictable. If unexpected technical difficulties
                           occur, a proctor will make every attempt to promptly resume your assessment. Extended
                           delays in testing may require rescheduling.<br><br>
                           
                           
                           
                           
                           <p>The CPT-I is used to determine if a student is ready for a higher level math such
                              as Trigonometry, Calculus, Pre-Calculus, Statistics etc.
                           </p>
                           
                           <p>We must have the following scores or higher on file with Valencia to be eligible for
                              the CPT-I:
                           </p>
                           
                           <p><strong>Prior to March 1, 2016 SAT math</strong>: 500 or higher
                           </p>
                           
                           <p><strong>On or after March 1, 2016 SAT math</strong>: 26.5 or higher
                           </p>
                           
                           <p><strong>ACT math</strong>: 21 or higher
                           </p>
                           
                           <p><strong>PERT math</strong>: 123 or higher
                           </p>
                           
                           <p><strong>CPT math</strong>: 90 or higher
                           </p>
                           
                           <p>Please note: Valencia will only accept official scores for the SAT, ACT, PERT and
                              CPT. Please see&nbsp;<a href="requesting-scores.html" target="_blank">Requesting/Submitting Scores</a>&nbsp;for more information on how to submit your scores for our records prior to taking
                              the CPTI.
                           </p>
                           
                           <p>Please see the placement chart for the list of CPTI placement Math courses:</p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th colspan="2">CPTI Placement</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td scope="row">64 or less</td>
                                    
                                    <td>MAC 1105: College Algebra</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td scope="row">65 or more</td>
                                    
                                    <td>MAC 1114: College Trigonometry<br>
                                       MAC 1140: Pre-Calculus Algebra<br>
                                       MAC 2233: Calculus for Business and Social Science<br>
                                       MAE 2801: Elementary School Math<br>
                                       MHF 2300: Logic and Proof in Math
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td scope="row">89 or more</td>
                                    
                                    <td>MAC 2311: Calculus with Analytical Geometry I</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p><br>
                              
                           </p>
                           
                           <p>Need to Review? Click for the&nbsp;<a href="/documents/studets/offices-services/assessments/cpt-i/cpt-College-Math-Review.pdf">CPT-I Review Booklet</a></p>
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Steps for CPT-I Retake (1 retake per 24 months)</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>All CPTI retakes require a:</p>
                           
                           <ol>
                              
                           </ol>
                           
                           <ul>
                              
                              <li>1 day (24 hour) waiting period between tests</li>
                              
                              <li>CPT-I Retake Review Session per subtest</li>
                              
                              <li>CPT-I retake fee ($10 per subject retake)</li>
                              
                           </ul>
                           
                           
                           <p><strong>Steps:</strong><br>
                              1. Obtain your CPT-I scores and confirm retake eligibility with your Assessment Center<br>
                              2. Receive CPTI Retake Certificate from your Assessment Center<br>
                              3. Review the following link to see times, locations and process of a PERT/CPT-I Review
                              Session:&nbsp;<a href="/students/learning-support/pert.html">valenciacollege.edu/students/office-services/learning-support/pert.html</a><br>
                              4. Attend the CPT-I Retake Review Session(s) and get your certificate(s) signed<br>
                              5. Pay at your business office:&nbsp;<a href="/students/office-services/business-office/payments.html">http://valenciacollege.edu/students/office-services/business-office/payments.html</a>&nbsp;<br>
                              6. Return to the Assessment Center with your certificate(s) and payment receipt to
                              retake the CPT-I.
                           </p>
                           
                           <p align="center">CPT-I Review Booklet</p>
                           
                           <p align="center"><a href="/documents/studets/offices-services/assessments/cpt-i/cpt-College-Math-Review.pdf">COLLEGE LEVEL MATH REVIEW GUIDE (CPT I)</a></p>
                           
                           <p align="center">Bookstore Price: CPT-I $2.30 (Prices subject to change)<br>
                              <i class="far fa-info-circle" aria-hidden="true"></i> Be sure to work through the problems and bring in questions to the tutoring lab
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/cpt-i.pcf">©</a>
      </div>
   </body>
</html>