<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/learning-preferences.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2><strong>Valencia College Departments:</strong></h2>
                        
                        <p><strong><u>New Student Orientation</u></strong></p>
                        
                        <p>Students will complete Part 1: Online Orientation, including the Career Review, Academic
                           Reviews, and LASSI through their atlas account. After completing the Online Orientation
                           and Assessment requirements, students will wait 24 hours to sign up for Part 2: On-site
                           Orientation through their Atlas account.
                        </p>
                        
                        <p>Dual Enrollment students are only required to complete the Online Dual Enrollment
                           orientation. On-Campus Orientation is not required. 
                        </p>
                        
                        <p>For more information about orientation for non-Dual enrollment students, refer to
                           the <a href="../orientation/checklist.html">Orientation</a> home page. 
                        </p>
                        
                        <h2><img alt="Valencia Sign" height="-1" src="ValenciaPicture1.jpg" width="-1"></h2>
                        
                        
                        <p><strong><u>Testing Center</u></strong></p>
                        
                        <p>If you need to take an exam for a class, please refer to the Testing Center websites
                           for hours and additional information.
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div><a href="../east/academic-success/testing/index.html">East</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="../learning-support/testing/index.html">West</a></div>
                                 </div>
                                 
                                 <div>
                                    <div><a href="../osceola/testing/index.html">Osceola</a></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="../wp/testing.html">Winter Park</a> 
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       <a href="../lakenona/testingcenter.html">Lake Nona</a> 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>        
                        <br>
                        
                        <h2><img alt="Testing Center" height="-1" src="TestingCenterPicture1.jpg" width="-1"></h2>
                        
                        
                        <p><strong><u>Answer Center</u></strong></p>
                        
                        <p>For any general questions about financial aid, steps to enrollment, or if you need
                           to inquire about or follow up on your admission process, please refer to the <a href="../students/answer-center/index.html">Answer Center</a> home page.<br>
                           
                        </p>
                        
                        <p><img alt="Answer Center" height="-1" src="AnswerCenterPicture1.jpg" width="-1"></p>
                        
                        
                        <p><strong><u>Student Services Departments </u></strong></p>
                        
                        <p><strong>Advising Center</strong></p>
                        
                        <p>To speak with an advisor about person and/or academic development, please refer to
                           the <a href="../students/advising-counseling/index.html">Advising Center</a> home page for hours and locations. 
                        </p>
                        
                        <p><strong>Financial Aid Services</strong></p>
                        
                        <p>To learn more about your financial aid, please refer to the <a href="../finaid/index.html">Financial Aid Services</a> home page. 
                        </p>
                        
                        <p><strong>International Student Services</strong></p>
                        
                        <p>If you would like further information and assistance with the international admission
                           process, academic advising, immigration questions, and other needs please refer to
                           the <a href="../international/index.html">International Student Services</a> home page. 
                        </p>
                        
                        <p><strong>Office for Students with Disabilities</strong></p>
                        
                        <p>For any inquiries about what services we offer for students with disabilities, please
                           refer to the <a href="../office-for-students-with-disabilities/default.html">Office for Students with Disabilities</a> home page. 
                        </p>
                        
                        <p><strong>Office of Veteran Affairs</strong></p>
                        
                        <p>If you are a Veteran, Active-Duty, National Guard, Reserve military personnel, or
                           a dependent or spouse of the above, please refer to the <a href="../office-of-veterans-affairs/index.html">Office of Veteran Affairs</a> home page for more information about benefits and navigating through the college
                           experience. 
                        </p>
                        
                        <p><img alt="Valencia Students" height="207" src="ValenciaStudentsPicture1.jpg" width="490"></p>
                        
                        
                        <p><strong><u>Dual Enrollment Program </u></strong></p>
                        
                        <p>&nbsp;If you or somebody you know is interested in attending Valencia College as a Dual
                           Enrolled student please refer to the <a href="../admissions/dual-enrollment/index.html">Dual Enrollment</a> home page
                        </p>
                        
                        <p><img alt="Dual Enrollment Sign" height="-1" src="DESignPicture1.jpg" width="-1"></p>
                        
                        
                        <p><strong><u>Bridges to Success Program </u></strong></p>
                        
                        <p>If you or someone you know is interested in participating in the Bridges to Success
                           program please refer to the <a href="../bridges/index.html">Bridges</a> home page
                        </p>
                        
                        <p><img alt="Bridges Sign" height="-1" src="BridgestSignPicture1.jpg" width="-1"></p>
                        
                        
                        <p><strong><u>Criminal Justice Institute</u></strong></p>
                        
                        <p>For information on the Criminal Justice Institution admission process, academies,
                           and programs offered please refer to the <a href="../public-safety/criminal-justice-institute/index.html">Criminal Justice Institute</a> home page
                        </p>
                        
                        <p>*For information on the CJBAT exam please refer <a href="cjbat/index.html">here</a> 
                        </p>
                        
                        <p><img alt="Criminal Justice Students" height="169" src="CJIStudents.jpg" width="490"></p>
                        
                        
                        <p><strong><u>Health Sciences</u></strong></p>
                        
                        <p>To find out more about upcoming Health Science information sessions, program applications
                           and deadlines please visit our <a href="../west/health/index.html">Health Sciences</a> home page
                        </p>
                        
                        <p>*For information on the TEAS exam please refer <a href="teas/index.html">here</a> 
                        </p>
                        
                        <p><img alt="Health Sciences" height="-1" src="HealthSciencesLogo.jpg" width="-1"></p>
                        
                        
                        <p><strong><u>Atlas Lab</u></strong></p>
                        
                        <p> Atlas labs are open to all registered students to conduct educational and career
                           related research and planning . 
                        </p>
                        
                        <p>For hours of service and locations please visit the <a href="../student-services/atlas-access-labs.html">Atlas</a> home page
                        </p>
                        
                        <p><img alt="Atlas Lab Keyboard" height="-1" src="AtlasLabKeyboardPicture.jpg" width="-1"> 
                        </p>
                        
                        
                        <p><strong><u>Career Center</u></strong></p>
                        
                        <p>Career centers are located at each of our campuses and are there to help students
                           figure out what career choices and opportunities are cut out just for them. For hours
                           and contact information please visit the <a href="../careercenter/aboutus.html">Career Center</a> home page 
                        </p>
                        
                        <p><img alt="Career Center Cycle Picture" height="-1" src="CareerCenterCyclePicture1.jpg" width="-1"></p>
                        
                        
                        <h2>Learning Preferences Questionnaire</h2>
                        
                        <p>Knowing your learning preferences will allow you to take full advantage of your educational
                           
                           experience both in and out of the classroom. Take this online questionnaire and find
                           helpful strategies 
                           that will help you make the most of your class and study time.
                        </p>
                        
                        
                        <p><a href="http://www.vark-learn.com/english/page.asp?p=questionnaire" target="_blank">
                              Take the Learning Preferences Questionnaire!<br>
                              <br>
                              </a></p>
                        
                        
                        <h3>Disclaimer</h3>
                        
                        <p>Any links to external Web sites and/or non-Valencia College information provided on
                           Valencia Web pages or returned from Valencia Web search engines are provided as a
                           courtesy. They should not be construed as an endorsement by Valencia College of the
                           content or views of the linked materials.
                           
                           
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/learning-preferences.pcf">©</a>
      </div>
   </body>
</html>