<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessments  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Assessments</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Mission Statement</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>To provide and educate students about standardized testing in a secure and consistent
                              environment that supports all stages of their educational goals.
                           </p>
                           
                        </div>         
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Important Information</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p style="color:white; background-color: #bf311a">All Assessment Centers Closed September 4.</p>
                           You will be asked to have a valid and original&nbsp;<a href="../../academics/assessments/testing-rules.html">government issued</a>&nbsp;photo ID when testing at the Assessment Center.
                           
                           <p>In order to take the PERT at Valencia you must:</p>
                           
                           <ul>
                              
                              <li>
                                 <strong>Valencia Student&nbsp;</strong>- have an active application on file
                              </li>
                              
                              <ul>
                                 
                                 <ul>
                                    
                                    <li>the&nbsp;<a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Admissions application</a>&nbsp;must be completed at least 5 business days prior to testing.
                                    </li>
                                    
                                    <li>Valencia ID number found in your acceptance letter (V0******)</li>
                                    
                                    <li>Atlas username and password created. Activate your account @ the following link:&nbsp;<a href="https://atlas.valenciacollege.edu/">https://atlas.valenciacollege.edu/</a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </ul>
                              
                              <li>
                                 <strong>Future Dual Enrollment Student&nbsp;</strong>- have applied to Dual Enrollment but do not have to pay the application fee. Please
                                 see the&nbsp;<a href="../../academics/assessments/dual-enrollment.html">Dual Enrollment</a>&nbsp;page for more information.
                              </li>
                              
                              <ul type="circle">
                                 
                                 <li>Valencia ID number is required for testing (V0******)</li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>Non-Valencia Student</strong>- fill out Option 2 on the&nbsp;<a href="../../academics/assessments/remote">Additional Testing Options</a>&nbsp;page.
                              </li>
                              
                           </ul>
                           
                           <p>Please note: Children are not allowed in the testing room. Proper supervision of small
                              children should be arranged off site prior to testing.
                           </p>
                           
                        </div>
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Assessment Testing Hours</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>P.E.R.T.,&nbsp;<strong>ACCUPLACER&nbsp;</strong>ESL(LOEP), CPT-I
                           </h4>
                           
                           <ul>
                              
                              <li>
                                 <strong>Mon - Thurs:</strong>&nbsp;8 am - *5 pm
                              </li>
                              
                              <li>
                                 <strong>Friday</strong>: 9 am - 4 pm
                              </li>
                              
                           </ul>
                           
                           <p>* PERT takes 2.5 - 3 hours. Testing must be completed in the&nbsp;<strong>same day</strong>, unless a seven-day extension is needed.
                           </p>
                           
                           <h4>CJBAT</h4>
                           
                           <ul>
                              
                              <li>
                                 <strong>Mon - Thurs:</strong>&nbsp;8 am - 3 pm
                              </li>
                              
                              <li>
                                 <strong>F</strong><strong>riday:&nbsp;</strong>9am-2pm
                              </li>
                              
                           </ul>
                           
                           <h4>TEAS</h4>
                           
                           <ul>
                              
                              <li>
                                 <strong>Mon - Thurs:</strong>&nbsp;8 am - 2 pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong>&nbsp;9am - 1pm
                              </li>
                              
                           </ul>
                           
                           <h4>CLEP</h4>
                           
                           <ul>
                              
                              <li><strong>By appointment only</strong></li>
                              
                           </ul>
                           
                        </div>                               
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Locations &amp; Contact</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td><strong>West</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>SSB, Rm 171</td>
                                    
                                    <td>(407) 582-1101</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>East</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bldg 5, Rm 237</td>
                                    
                                    <td>(407) 582-2770</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Osceola</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bldg 4, Rm 248-250</td>
                                    
                                    <td>(407) 582-4149</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Lake Nona</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bldg 1, Rm 206</td>
                                    
                                    <td>(407) 582-7104</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>&nbsp;</td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Winter Park</strong></td>
                                    
                                    <td></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Bldg 1, Rm 104</td>
                                    
                                    <td>(407) 582-6086</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>&nbsp;</p>
                           
                        </div>  
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/index.pcf">©</a>
      </div>
   </body>
</html>