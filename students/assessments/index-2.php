<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/index-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h3>Mission Statement</h3>
                        
                        <p> To provide and educate students about standardized testing in a secure and consistent
                           environment that supports all stages of their educational goals.<br>
                           
                        </p>
                        
                        <h3>*Important Information* </h3>
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        <h3>You will be asked to have a valid and original <a href="testing-rules.html">government issued</a> photo ID when testing at  the Assessment Center.
                        </h3>
                        
                        <p>In order to take the PERT at Valencia you must:</p>
                        
                        <ul>
                           
                           <li>
                              <strong>Valencia Student </strong>- have an active application on file 
                           </li>
                           
                           <ul>
                              
                              <ul>
                                 
                                 <li>the <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm">Admissions application</a> must be completed at least 5 business days prior to testing. 
                                 </li>
                                 
                                 <li>Valencia ID number found in your acceptance letter (V0******)</li>
                                 
                                 <li>Atlas username and password created. Activate your account @ the following link: <a href="https://atlas.valenciacollege.edu/">https://atlas.valenciacollege.edu/</a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </ul>
                           
                           <li>
                              <strong>Future Dual Enrollment Student </strong>- have applied to Dual  Enrollment but do not have to pay the application fee. Please
                              see the <a href="dual-enrollment.html">Dual Enrollment</a> page for more information. 
                           </li>
                           
                           <ul type="circle">
                              
                              <li>Valencia ID number is required for        testing (V0******)</li>
                              
                           </ul>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <strong>Non-Valencia Student</strong>- fill out Option 2 on the <a href="remote/index.html">Additional Testing Options</a> page.
                           </li>
                           
                        </ul>
                        
                        <p>Please note: Children are not allowed in the testing room. Proper supervision of small
                           children should be arranged off site prior to testing. 
                        </p>
                        
                        
                        <p><img alt="Assessment Hours" height="68" src="clip_image002.png" width="338"></p>
                        
                        <h3>P.E.R.T., <strong>ACCUPLACER </strong> ESL(LOEP), CPT-I
                        </h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Mon - Thurs:</strong> 8 am - *5 pm
                           </li>
                           
                           <li>
                              <strong>Friday</strong>: 9 am - 4 pm 
                           </li>
                           
                        </ul>
                        
                        <p>* PERT takes 2.5 - 3 hours. Testing must be completed in the <span><strong>same day</strong></span>, unless a seven-day extension is needed. 
                        </p>
                        
                        <h3>CJBAT</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Mon - Thurs:</strong> 8 am - 3 pm 
                           </li>
                           
                           <li>
                              <strong>F</strong><strong>riday:&nbsp;</strong>9am-2pm
                           </li>
                           
                        </ul>
                        
                        <h3>TEAS</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Mon - Thurs:</strong> 8 am - 2 pm 
                           </li>
                           
                           <li>
                              <strong>Friday:</strong>&nbsp;9am  - 1pm
                           </li>
                           
                        </ul>
                        
                        <h3>CLEP</h3>
                        
                        <ul>
                           
                           <li><strong>By appointment only</strong></li>
                           
                        </ul>
                        
                        
                        <p>Quick Links</p>
                        
                        <p><a href="new-student-induction/index.html" target="_blank">New Student</a></p>
                        
                        <p><a href="pert/taking-the-exam.html" target="_blank">PERT</a></p>
                        
                        <p><a href="clep/index.html" target="_blank">CLEP</a></p>
                        
                        <p><a href="teas/index.html" target="_blank">TEAS</a></p>
                        
                        <p><a href="cjbat/index.html" target="_blank">CJBAT</a></p>
                        
                        <p><a href="compassesl/taking-the-exam.html" target="_blank">ACCUPLACER LOEP ESL</a></p>
                        
                        <p> <a href="placement-chart.html" target="_blank">Placement Chart</a></p>
                        
                        <p><a href="testing-rules.html" target="_blank">Testing Rules</a></p>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/assessments/contact.cfm" target="_blank">Contact Us </a></p>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/index-2.pcf">©</a>
      </div>
   </body>
</html>