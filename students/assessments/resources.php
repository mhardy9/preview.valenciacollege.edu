<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments, resources">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>New Student Orientation</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">           
                           
                           <p>Students will complete Part 1: Online Orientation, including the Career Review, Academic
                              Reviews, and LASSI through their atlas account. After completing the Online Orientation
                              and Assessment requirements, students will wait 24 hours to sign up for Part 2: On-site
                              Orientation through their Atlas account.
                           </p>
                           
                           <p>Dual Enrollment students are only required to complete the Online Dual Enrollment
                              orientation. On-Campus Orientation is not required.
                           </p>
                           
                           <p>For more information about orientation for non-Dual enrollment students, refer to
                              the&nbsp;<a href="https://valenciacollege.edu/orientation/checklist.cfm">Orientation</a> home page.
                           </p>
                           <img src="/_resources/img/students/assessments/ValenciaPicture1.jpg" width="-1" height="-1" alt="Valencia Sign">
                           
                        </div>         
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Testing Center</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you need to take an exam for a class, please refer to the Testing Center websites
                              for hours and additional information.
                           </p>
                           
                           <ul>
                              
                              <li><a href="https://valenciacollege.edu/east/academicsuccess/testing/">East</a></li>
                              
                              <li><a href="https://valenciacollege.edu/learning-support/testing/">West</a></li>
                              
                              <li><a href="https://valenciacollege.edu/osceola/testing/">Osceola</a></li>
                              
                              <li><a href="https://valenciacollege.edu/wp/testing.cfm">Winter Park</a></li>
                              
                              <li><a href="https://valenciacollege.edu/lakenona/testingcenter.cfm">Lake Nona</a></li>
                              
                           </ul>
                           
                           <br>
                           <img src="/_resources/img/students/assessments/TestingCenterPicture1.jpg" width="-1" height="-1" alt="Testing Center">
                           
                        </div> 
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Answer Center</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>For any general questions about financial aid, steps to enrollment, or if you need
                              to inquire about or follow up on your admission process, please refer to the&nbsp;<a href="https://valenciacollege.edu/answer-center/">Answer Center</a>&nbsp;home page.<br>
                              
                           </p>
                           
                           <p><img src="/_resources/img/students/assessments/AnswerCenterPicture1.jpg" width="-1" height="-1" alt="Answer Center"></p>
                           
                        </div> 
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Student Services Departments</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>Advising Center</strong></p>
                           
                           <p>To speak with an advisor about person and/or academic development, please refer to
                              the&nbsp;<a href="https://valenciacollege.edu/advising-center/">Advising Center</a>&nbsp;home page for hours and locations.
                           </p>
                           
                           <p><strong>Financial Aid Services</strong></p>
                           
                           <p>To learn more about your financial aid, please refer to the&nbsp;<a href="https://valenciacollege.edu/finaid/">Financial Aid Services</a>&nbsp;home page.
                           </p>
                           
                           <p><strong>International Student Services</strong></p>
                           
                           <p>If you would like further information and assistance with the international admission
                              process, academic advising, immigration questions, and other needs please refer to
                              the&nbsp;<a href="https://valenciacollege.edu/international/">International Student Services</a>&nbsp;home page.
                           </p>
                           
                           <p><strong>Office for Students with Disabilities</strong></p>
                           
                           <p>For any inquiries about what services we offer for students with disabilities, please
                              refer to the&nbsp;<a href="https://valenciacollege.edu/osd/">Office for Students with Disabilities</a>&nbsp;home page.
                           </p>
                           
                           <p><strong>Office of Veteran Affairs</strong></p>
                           
                           <p>If you are a Veteran, Active-Duty, National Guard, Reserve military personnel, or
                              a dependent or spouse of the above, please refer to the&nbsp;<a href="https://valenciacollege.edu/veterans-affairs/">Office of Veteran Affairs</a>&nbsp;home page for more information about benefits and navigating through the college
                              experience.
                           </p>
                           
                           <p><img src="/_resources/img/students/assessments/ValenciaStudentsPicture1.jpg" width="490" height="207" alt="Valencia Students"></p>
                           
                        </div>                                                            
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Dual Enrollment</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>&nbsp;If you or somebody you know is interested in attending Valencia College as a Dual
                              Enrolled student please refer to the&nbsp;<a href="https://valenciacollege.edu/dual/">Dual Enrollment</a>&nbsp;home page
                           </p>
                           
                           <p><img src="/_resources/img/students/assessments/DESignPicture1.jpg" width="-1" height="-1" alt="Dual Enrollment Sign"></p>
                           
                        </div>
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Bridges to Success Program</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you or someone you know is interested in participating in the Bridges to Success
                              program please refer to the&nbsp;<a href="https://valenciacollege.edu/bridges/">Bridges</a>&nbsp;home page
                           </p>
                           
                           <p><img src="/_resources/img/students/assessments/BridgestSignPicture1.jpg" width="-1" height="-1" alt="Bridges Sign"></p>
                           
                        </div>                 
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Criminal Justice Institute</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>For information on the Criminal Justice Institution admission process, academies,
                              and programs offered please refer to the&nbsp;<a href="https://valenciacollege.edu/cji/">Criminal Justice Institute</a>&nbsp;home page
                           </p>
                           
                           <p>*For information on the CJBAT exam please refer&nbsp;<a href="https://valenciacollege.edu/assessments/cjbat/">here</a></p>
                           
                           <p><img src="/_resources/img/students/assessments/CJIStudents.jpg" width="490" height="169" alt="Criminal Justice Students"></p>
                           
                        </div>                  
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Health Sciences</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>To find out more about upcoming Health Science information sessions, program applications
                              and deadlines please visit our&nbsp;<a href="https://valenciacollege.edu/west/health/">Health Sciences</a>&nbsp;home page
                           </p>
                           
                           <p>*For information on the TEAS exam please refer&nbsp;<a href="https://valenciacollege.edu/assessments/teas/">here</a></p>
                           
                           <p><img src="/_resources/img/students/assessments/HealthSciencesLogo.jpg" width="-1" height="-1" alt="Health Sciences"></p>
                           
                        </div>   
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Atlas Lab</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Atlas labs are open to all registered students to conduct educational and career related
                              research and planning .
                           </p>
                           
                           <p>For hours of service and locations please visit the&nbsp;<a href="https://valenciacollege.edu/student-services/atlas-access-labs.cfm">Atlas</a>&nbsp;home page
                           </p>
                           
                           <p><img src="/_resources/img/students/assessments/AtlasLabKeyboardPicture.jpg" width="-1" height="-1" alt="Atlas Lab Keyboard"></p>
                           
                        </div>                                                    
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Career Center</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Career centers are located at each of our campuses and are there to help students
                              figure out what career choices and opportunities are cut out just for them. For hours
                              and contact information please visit the&nbsp;<a href="https://valenciacollege.edu/careercenter/aboutus.cfm">Career Center</a>&nbsp;home page
                           </p>
                           
                           <p><img src="/_resources/img/students/assessments/CareerCenterCyclePicture1.jpg" width="-1" height="-1" alt="Career Center Cycle Picture"></p>
                           
                        </div>                                                                                 
                        
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Learning Preferences Questionnaire</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p align="left">Knowing your learning preferences will allow you to take full advantage of your educational
                              experience both in and out of the classroom. Take this online questionnaire and find
                              helpful strategies that will help you make the most of your class and study time.
                           </p>
                           
                           <p><a href="http://www.vark-learn.com/english/page.asp?p=questionnaire" target="_blank">Take the Learning Preferences Questionnaire!</a></p>
                           
                        </div>
                        
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Disclaimer</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p align="left">Any links to external Web sites and/or non-Valencia College information provided on
                              Valencia Web pages or returned from Valencia Web search engines are provided as a
                              courtesy. They should not be construed as an endorsement by Valencia College of the
                              content or views of the linked materials.
                           </p>
                           
                        </div>                    
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/resources.pcf">©</a>
      </div>
   </body>
</html>