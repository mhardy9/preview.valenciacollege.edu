<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice Basic Abilities Test  | Valencia College</title>
      <meta name="Description" content="College Level Examination Program">
      <meta name="Keywords" content="college, school, educational, assessments, cjbat, exam">
      <meta name="Author" content="Criminal Justice Basic Abilities Test | Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/cjbat.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Criminal Justice Basic Abilities Test </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>At a Glance</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              
                           </p>
                           
                           <h3>Hours Available</h3>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>All Assessment Centers Closed September 4.</strong>
                           
                           <p></p>
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>Walk-in basis at East, West, and Osceola campuses (view Note below)</strong>&nbsp;
                           <p></p>
                           <strong>Mon-Thurs</strong>: 8 AM - 3 PM <br>
                           <strong>Friday:&nbsp;</strong>9 AM - 2 PM <br>
                           
                           <p style="font-weight: bold">Note:&nbsp;Offered on East, West, and Osceola Campus between the above hours. Also available
                              at the Lake Nona and Winter Park Campuses by appointment only.
                           </p>
                           
                           
                           <h3>Retake Wait Period</h3>
                           
                           
                           <p>No wait period<br>
                              May only take 3 times within 12 months.
                           </p>
                           
                           
                           <h3>Test Fees</h3>
                           
                           <p>Valencia Student- $40 per test</p>
                           
                           <p>Non-Valencia Student- $50</p>
                           
                           <p><a href="/students/office-services/business-office/payments.cfm">Valencia's Payment Method</a></p>
                           
                           <p>*Prices subject to change</p>
                           
                           
                           
                           <h3>Time Required</h3>
                           2 hours, 30 minutes<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> Technical difficulties inherent with the internet, software or transmission may occur
                           during your testing experience and are unpredictable. If unexpected technical difficulties
                           occur, a proctor will make every attempt to promptly resume your assessment. Extended
                           delays in testing may require rescheduling.
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>About the CJBAT</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The CJBAT was developed to insure that trainees entering the Florida certified criminal
                              justice training centers possess the requisite abilities to master the curricula and
                              understand the materials that are presented to them in training.
                           </p>
                           
                           <p>View Florida Department of Law Enforcement website&nbsp;<a href="http://www.fdle.state.fl.us/cms/CJSTC/Officer-Requirements/Basic-Abilities-Test/Morris-and-McDaniel-Inc.aspx" target="_blank">here</a>.
                           </p>
                           
                           <p>Valencia offers the CJBAT in two criminal justice disciplines: Law Enforcement and
                              Corrections. The CJBAT series of exams were designed to select candidates for admission
                              into law enforcement officer or correctional officer training. Each of the tests includes
                              a measure of the same basic abilities, but they are customized for each criminal justice
                              discipline.
                           </p>
                           
                           <ul>
                              
                              <li>The CJBAT will assess your skills in the following areas:</li>
                              
                              <ul>
                                 
                                 <li>
                                    <strong>Deductive Reasoning</strong>&nbsp;- the ability to apply rules to specific problems to come up with logical answers
                                 </li>
                                 
                                 <li>
                                    <strong>Inductive Reasoning</strong>&nbsp;- the ability to combine separate pieces of information, or specific answers to problems
                                    to form general rules or conclusions
                                 </li>
                                 
                                 <li>
                                    <strong>Information Ordering</strong>&nbsp;- the ability to correctly follow a rule or set of rules in order to arrange things
                                    or actions in a certain order
                                 </li>
                                 
                                 <li>
                                    <strong>Memorization</strong>&nbsp;- the ability to remember information such as words, numbers, pictures and procedures
                                 </li>
                                 
                                 <li>
                                    <strong>Problem Sensitivity</strong>&nbsp;- the ability to tell if something is wrong or likely to go wrong
                                 </li>
                                 
                                 <li>
                                    <strong>Spatial Orientation</strong>&nbsp;- the ability to tell where you are in relation to the location of some objects
                                 </li>
                                 
                                 <li>
                                    <strong>Written Comprehension</strong>&nbsp;- involves reading &amp; understanding written words and sentences
                                 </li>
                                 
                                 <li>
                                    <strong>Written Expression</strong>&nbsp;- ability to write a paragraph using proper grammar and spelling
                                 </li>
                                 
                              </ul>
                              
                              <li>The CJBAT is a timed test - you will have 2 ½ hours to complete it.</li>
                              
                              <li>
                                 <strong>Passing Scores are as follows:</strong>
                                 
                                 <ul>
                                    
                                    <li>Corrections CJBAT: 70%</li>
                                    
                                    <li>Law Enforcment CJBAT: 70%</li>
                                    
                                    <li>
                                       <strong>NOTE</strong>: Starting February 15 2016, per a Florida Department of Law Enforcement (FDLE) directive,
                                       all numeric score information is to be removed from all law enforcement BAT grade
                                       reports. To meet this request, IOS has produced a pass / fail report for both the
                                       CJBAT – LEO and CJBAT – CO examinations.&nbsp;<u>Numeric scores will no longer be available on any CJBAT reports</u>. Please contact IOS at 708-410-0200 to speak with one of our representatives for
                                       more information, or you may contact the FDLE directly for more information regarding
                                       this policy change.
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <p>For a CJBAT Study Guide call 1-708-410-0200, or visit&nbsp;<a href="http://www.publicsafetyrecruitment.com/">publicsafetyrecruitment.com</a> or&nbsp;<a href="http://www.iosolutions.org/">iosolutions.org</a></p>
                              
                              <p><a href="/STUDENTS/offices-services/assessments/cjbat/taking-the-cjbat.html">How do I take the CJBAT?</a></p>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>CJBAT Transcripts</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you would like to request your CJBAT scores please complete the&nbsp;<a href="/documents/studets/offices-services/assessments/TestScoreRequestForm.pdf">Valencia College Test Score Request Form</a>. Make sure to fill out the form completely including your Date of Birth and VID number.
                              Attach a copy of a government issued picture ID that also includes your signature.
                              Request forms without a picture ID with signature will not be processed. Incomplete
                              requests will not be honored. Please fax the form to the Assessment Center of your
                              choice, specific details are located on the form.
                           </p>
                           
                           
                        </div>                                                                              
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/cjbat.pcf">©</a>
      </div>
   </body>
</html>