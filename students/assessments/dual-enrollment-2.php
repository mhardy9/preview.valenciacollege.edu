<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/dual-enrollment-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="index.html"></a>
                        
                        
                        <div>
                           
                           <h2><strong>Future Dual Enrollment Students
                                 </strong></h2>
                           
                        </div>
                        
                        <p>It is the student's responsibility to submit their scores in the appropriate time
                           needed for Valencia to receive and process. Do not wait until the last minute to submit
                           your scores. Refer to <a href="../admissions/dual-enrollment/index.html" target="_blank">Dual Enrollment</a> for deadline dates. SAT and ACT scores take <strong>5 weeks </strong>to receive and process. 
                        </p>
                        
                        <p>Minimum college-level scores required for eligibility into the Dual Enrollment Program:</p>
                        
                        <p><strong>PERT</strong>: Reading <strong>106</strong>/ Writing <strong>103</strong>/ Math <strong>114</strong></p>
                        
                        <p><strong>SAT(prior to 3/1/16):</strong> Critical Reading <strong>440</strong>/ Math <strong>440</strong> *SAT Writing score is not used for English placement.
                        </p>
                        
                        <p><strong>SAT (on or after 3/1/16):</strong> Reading Test Score <strong>24</strong>/ Writing and Language Test Score <strong>25</strong>/ Math Test Score <strong>24</strong></p>
                        
                        <p><strong>ACT:</strong> Reading <strong>19</strong>/ English <strong>17</strong>/ Math <strong>19</strong></p>
                        
                        
                        
                        <p> PERT Testing</p>
                        
                        <p><iframe frameborder="0" height="306" src="http://www.powtoon.com/embed/cbo9hBbKGvT/" width="490"></iframe></p>
                        
                        <p>You must have the following  when coming to the Assessment Center:<br>
                           <strong>Valencia Identification Number (VID)</strong>- This is found in your Dual  Enrollment email (V0_______).
                        </p>
                        
                        <p><strong>Identification</strong>- Acceptable forms of identification include: 
                        </p>
                        
                        <ul>
                           
                           <li>A current driver’s license </li>
                           
                           <li>A current Valencia College  student ID </li>
                           
                           <li>A current state or federal ID  card (such as a Green Card)</li>
                           
                           <li>A current passport </li>
                           
                           <li>A tribal ID card </li>
                           
                           <li>A naturalization card or  certificate of citizenship</li>
                           
                        </ul>
                        
                        <p>* If you do not have a  government issued photo ID, Dual Enrollment students may bring
                           their <u>original</u> Birth Certificate <strong>OR</strong> Social Security Card <strong>AND</strong> their high school ID. NO copies of Social Security Card or Birth Certificate will
                           be accepted. 
                        </p>
                        
                        <ul>
                           
                           <li>Testing is done on a walk-in basis. No appointment needed. </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>*PERT takes 2.5 - 3 hours. Testing must be completed in the  same day, unless a  seven-day
                              extension is needed. Please allow yourself at least 2 1/2 to 3 hours  for all 3 sections
                              of the PERT. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>NOTE</strong>: <em>Technical difficulties inherent with the  internet, software or transmission may occur
                              during your testing experience and  are unpredictable. If unexpected technical difficulties
                              occur, Proctors will  make every attempt to promptly resume your assessment. Extended
                              delays in  testing may require rescheduling. Do not wait last minute to complete the
                              PERT.</em></p>
                        
                        <ul>
                           
                           <li>A student is allowed 3 PERT attempts at Valencia in  a two-year period with a one-day
                              wait period between attempts. &nbsp;To be valid, test scores must have  been taken within
                              two years.
                           </li>
                           
                        </ul>
                        
                        <p><a href="pert/taking-the-exam.html"><strong>PERT Testing Times</strong></a></p>
                        
                        <ul>
                           
                           <li>Valencia  will only accept valid PERT scores from a student’s high school, no other
                              institution.             
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>PERT scores are immediately  input into your Valencia file after testing in Valencia's
                              Assessment Center. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Your initial PERT test at  Valencia is free <em>if</em> you have applied to Valencia’s Dual Enrollment Program and have received your  Valencia
                              ID number.             
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>You must wait at least 24 hours between any PERT  attempts.  Remediation is required
                              prior to your second attempt at Valencia. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>
                              <strong>PERT at High School Campus:</strong> Your Counselor should indicate on your application any college-level  PERT scores
                              received in High School. Those scores will be retrieved by  Assessment and reviewed
                              by Dual Enrollment. If you take the PERT again <u>after</u> you applied to Dual Enrollment, you must communicate to the Assessment office  through
                              the following link: 
                           </li>
                           
                        </ul>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">SUBMIT HIGH  SCHOOL PERT SCORES </a></p>
                        
                        
                        <p>PERT Retest at Valencia College </p>
                        
                        <p>Eligibility for PERT retakes:</p>
                        
                        <ul>
                           
                           <li>A student is allowed a total of 3 PERT attempts at  Valencia in a two-year period.</li>
                           
                           <li>There is a one-day wait period between attempts.</li>
                           
                           <li>Student must not have begun prep course sequence  prior to retest. </li>
                           
                        </ul>
                        
                        <p>Steps for completing PERT retakes (Students are required  to complete a PERT review
                           session prior to their <u>second</u> attempt): 
                        </p>
                        
                        <ul>
                           
                           <li>Obtain retake certificate from an Assessment  Proctor.&nbsp; </li>
                           
                           <li>Attend a required on-campus PERT review session in  the Support Center. Review  the
                              following link to see times, locations and process of a PERT Review  Session: <a href="../learning-support/PERT/index.html" target="_blank">http://valenciacollege.edu/learning-support/PERT/</a>
                              
                           </li>
                           
                           <li>Have Support Center staff sign retake certificate. </li>
                           
                           <li>Take certificate to the Business Office and pay  retake. <a href="../business-office/payments.html" target="_blank">http://valenciacollege.edu/business-office/payments.cfm</a>
                              
                           </li>
                           
                           <li>Return to Assessment Center with <u>signed  certificate</u> and <u>proof of payment</u>. 
                           </li>
                           
                        </ul>
                        
                        <p>* You may be asked to purchase a booklet from Valencia <a href="../students/locations-store/index.html" target="_blank">bookstore</a> or download and print the following booklet to review: 
                        </p>
                        
                        
                        <p><a href="documents/Reading.pdf">READING REVIEW GUIDE (PERT Reading) </a></p>
                        
                        <p><a href="documents/Writing.pdf">WRITING/SENTENCE SKILLS REVIEW GUIDE (PERT Writing)</a></p>
                        
                        <p><a href="pert/documents/PERTmathbookletUPDATE.pdf">MATH REVIEW GUIDE (PERT Math)</a></p>
                        
                        <p><a href="pert/documents/CPT-Ibooklet.pdf">COLLEGE LEVEL MATH REVIEW (CPT-I)</a></p>
                        
                        <p>Bookstore Prices: Reading $1.56, Writing $2.05, Math $4.00 </p>
                        
                        <p><u></u></p>
                        
                        <p> Please speak to an Assessment Center Specialist to make sure you qualify for a retake.
                           
                        </p>
                        
                        
                        
                        
                        <p>Submit SAT or ACT scores</p>
                        
                        <p>If you have taken the SAT or ACT in the past two years and they are at <a href="placement-chart.html" target="_blank"><u>college level</u></a>, please have your scores sent to us electronically or bring us the original score
                           report. <span>No printed, copied or scanned score reports will be accepted. </span></p>
                        
                        <p><strong> Please note: Sending SAT and ACT scores electronically will take 5 weeks to receive
                              and process. This includes requests made for rush delivery. </strong></p>
                        
                        <p><strong> It is the student's responsibility to submit their scores in the appropriate time
                              needed for Valencia to receive and process. Do not wait until the last minute to submit
                              your scores. </strong></p>
                        
                        <p>ACT scores can be sent electronically from your ACT account: <a href="http://www.actstudent.org/scores/send/" target="_blank">http://www.actstudent.org/scores/send/</a></p>
                        
                        <p>SAT scores can be sent electronically from your CollegeBoard Account: <a href="http://sat.collegeboard.org/scores/" target="_blank">http://sat.collegeboard.org/scores/</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/dual-enrollment-2.pcf">©</a>
      </div>
   </body>
</html>