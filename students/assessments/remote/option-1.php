<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Remote Testing  | Valencia College</title>
      <meta name="Description" content="Remote Testing">
      <meta name="Keywords" content="college, school, educational, assessments, remote, testing">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/remote/option-1.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/remote/">Remote</a></li>
               <li>Remote Testing </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Remote Testing for Valencia Students</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>(For Valencia students requesting to take a placement test at another institution)</h3>
                           
                           <p>The student is unable to test at Valencia because they live over 50 miles from the
                              nearest Valencia Assessment Center.
                           </p>
                           
                           <p><strong>The following tests are eligible to be taken at another institution through Valencia's
                                 Remote Testing process:</strong></p>
                           
                           <ul type="disc">
                              
                              <li>PERT</li>
                              
                              <li>CPTI (on a case-by-case basis only)</li>
                              
                              <li>ACCUPLACER ESL(LOEP)</li>
                              
                           </ul>
                           
                           <p>Visit the following links for test fee information</p>
                           
                           <p><a href="/STUDENTS/offices-services/assessments/pert.html" target="_blank">PERT Fee</a></p>
                           
                           <p><a href="/STUDENTS/offices-services/assessments/cpt-i.html" target="_blank">CPTI Fee</a></p>
                           
                           <p><a href="/STUDENTS/offices-services/assessments/loep.html" target="_blank">ACCUPLACER ESL(LOEP) Fee</a></p>
                           
                           <p><strong>Remote Placement Testing Policies:</strong></p>
                           
                           <ul type="disc">
                              
                              <li>Students must have a Valencia Student ID number and an active status prior to requesting
                                 remote testing.
                              </li>
                              
                              <li>All requests must be approved by the Institutional Test Administrator (ITA) or designee
                                 prior to taking the test.
                              </li>
                              
                              <li>Requests must be received at least 10 business days prior to the preferred test date.</li>
                              
                              <li>Testing must occur at a secure postsecondary test center, certified testing center
                                 or for those serving in the military or foreign services, a military/government educational
                                 facility.
                              </li>
                              
                              <li>Students are responsible for all remote related proctoring fees as well as any Valencia
                                 testing fees.
                              </li>
                              
                           </ul>
                           
                           <p><strong>Remote Placement Testing Steps:</strong>&nbsp;<br>
                              
                           </p>
                           
                           <ol>
                              
                              <li>Students must locate a testing facility in their geographic area with internet access.
                                 
                                 <ol type="a">
                                    
                                    <li>Look for community colleges, colleges, universities, certified testing centers or
                                       if military, military bases in your area.
                                       
                                       <ul>
                                          
                                          <li>To assist you in locating a college or university testing center, visit&nbsp;<a href="http://www.ncta-testing.org/cctc/find.php">www.ncta-testing.org/cctc/find.php</a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                 </ol>
                                 
                              </li>
                              
                              <li>Contact the testing facility and make sure they are willing to proctor an assessment
                                 for you prior to contacting Valencia. You should allow a minimum of 3.5 hours to complete
                                 the test.
                              </li>
                              
                              <li>Make sure that you have the following information before completing the&nbsp;<strong>Remote Placement Request Student Form</strong>.
                                 
                                 <ul type="disc">
                                    
                                    <li>Student’s Information:</li>
                                    
                                    <ul type="circle">
                                       
                                       <li>Full name</li>
                                       
                                       <li>Valencia Student ID#</li>
                                       
                                       <li>Contact phone number</li>
                                       
                                       <li>Email address</li>
                                       
                                       <li>Test date and time (confirmed with desired proctor institution or facility)</li>
                                       
                                    </ul>
                                    
                                    <li>Proctoring Institution Information:</li>
                                    
                                    <ul type="circle">
                                       
                                       <li>Institution name and their testing website address</li>
                                       
                                       <li>Proctor name and Proctor address</li>
                                       
                                       <li>Proctor email address</li>
                                       
                                       <li>Proctor phone number</li>
                                       
                                       <li>Proctor fax number</li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Click on the link below to complete the Remote Placement Request Student Form.</li>
                              
                              <li>Once you complete the student form, an email will be sent to the test proctor you
                                 have designated with a link to a second form they will complete.
                              </li>
                              
                              <li>The test proctor will complete the Remote Placement Request Proctor Form.</li>
                              
                              <li>Once both forms have been completed, your request will be processed by the Remote
                                 Testing Coordinator.
                              </li>
                              
                              <li>You will be contacted via the email address provided to confirm your test date.</li>
                              
                           </ol>
                           
                           <div align="center">
                              
                              <a href="http://net4.valenciacollege.edu/forms/assessments/remote/forms/option-1-agree.cfm" target="_blank" title="Remote Testing Request Form"><strong>Click here to complete the form</strong></a>
                              
                           </div>
                           
                           <p><br>
                              
                              
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/remote/option-1.pcf">©</a>
      </div>
   </body>
</html>