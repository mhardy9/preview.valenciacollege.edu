<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Remote Testing  | Valencia College</title>
      <meta name="Description" content="Remote Testing">
      <meta name="Keywords" content="college, school, educational, assessments, remote, testing">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/remote/option-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/remote/">Remote</a></li>
               <li>Remote Testing </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Other College Students</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>(For students from other institutions requesting to take a placement test at Valencia)</h3>
                           
                           <p>Valencia College has a process in place for allowing students from other institutions
                              to take the PERT, CPT or Accuplacer ESL(LOEP) test at Valencia. The information below
                              explains the request process.
                           </p>
                           
                           <p>Visit the following Valencia webpage for test fee information:</p>
                           
                           <p><a href="/STUDENTS/offices-services/assessments/assessments" target="_blank">http://www.valenciacollege.edu/students/assessments</a>&nbsp;(Other Fees)
                           </p>
                           
                           <p><strong>Remote Placement Testing Policies:</strong></p>
                           
                           <ul type="disc">
                              
                              <li>All requests&nbsp;<u>must</u>&nbsp;be approved by the student’s home institution prior to taking the test.
                              </li>
                              
                              <li>Requests must be received at least 10 business days prior to the preferred test date.</li>
                              
                              <li>Students are responsible for informing their institution of Valencia’s approval process.</li>
                              
                              <li>The Valencia preferred remote testing option is for students to test on&nbsp;<u>their institution’s account</u>&nbsp;using&nbsp;<u>their testing units</u>. This information must be shared with the other institution’s testing director prior
                                 to making any arrangements at Valencia.
                              </li>
                              
                           </ul>
                           
                           <p><strong>Remote Placement Testing Process:</strong></p>
                           
                           <ol>
                              
                              <li>Students must select a Valencia Testing Center location.</li>
                              
                           </ol>
                           
                           <ul type="disc">
                              
                              <li>To assist you in locating Valencia Campuses, visit&nbsp;<a href="http://valenciacollege.edu/aboutus/locations/">http://valenciacollege.edu/aboutus/locations/</a>
                                 
                              </li>
                              
                           </ul>
                           
                           <li>Complete Valencia's online form with your request to take a test at Valencia.</li>
                           
                           <li>Once you have completed the student form, an email will be sent to your home institution
                              to confirm your testing request with us.
                           </li>
                           
                           <li>Your home institution will complete a form.</li>
                           
                           <li>Once the forms have been completed, your request will be processed by Valencia.</li>
                           
                           <li>You will be contacted via the email address you provided to confirm your test date.</li>
                           
                           <div align="center">
                              
                              <a href="http://net4.valenciacollege.edu/forms/assessments/remote/forms/option-2-agree.cfm" target="_blank" title="Remote Testing Request Form"><strong>Click here to complete the form</strong></a>
                              
                           </div>
                           
                           
                           <p><br>
                              
                              
                           </p>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/remote/option-2.pcf">©</a>
      </div>
   </body>
</html>