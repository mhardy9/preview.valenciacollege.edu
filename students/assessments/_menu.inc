<ul>
<li><a href="/students/assessments/index.php">Assessment Services</a></li>
<li class="submenu"><a class="show-submenu" href="#">The Process <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
<li><a href="/students/assessments/testing-rules.php">Testing Policies</a></li>
<li><a href="/students/assessments/placement-chart.php">Placement Charts</a></li>
<li><a href="/students/assessments/new-student-induction/index.php">New Student Reviews</a></li>
<li><a href="/students/assessments/dual-enrollment.php">Dual Enrollment</a></li>
</ul>
</li>

   <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);">Assessments <span class="fas fa-chevron-down" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
                <div class="col-md-6">
                  <h3>PERT | LOEP | CPT-I</h3>
                  <ul>
                    <li><a href="/students/assessments/post-secondary-readiness-test/taking-the-exam.php">Taking the <strong>PERT</strong></a></li>
                    <li><a href="/students/assessments/compassesl/taking-the-exam.php">Taking the <strong>LOEP</strong></a></li>
                    <li><a href="/students/assessments/post-secondary-readiness-test/cpt-i.php">Taking the <strong>CPT-I</strong></a></li>
                  </ul>
                  <h3>College Level Examination Program (CLEP)</h3>
                  <ul>
                    <li><a href="/students/assessments/college-level-examination-program/index.php">College Level Examination Program (<strong>CLEP</strong>)</a></li>
                    <li><a href="/students/assessments/college-level-examination-program/exams-offered.php">CLEP Exams Offered</a></li>
                    <li><a href="/students/assessments/college-level-examination-program/taking-the-exam.php">Taking the CLEP</a></li>
                    <li><a href="/students/assessments/college-level-examination-program/policies.php">CLEP Testing Policies</a></li>
                    <li><a href="/students/assessments/college-level-examination-program/transcripts.php">CLEP Transcripts</a></li>
                    <li><a href="/students/assessments/college-level-examination-program/clepfaqs.php">CLEP FAQs</a></li>
                  </ul>
				<h3>Remote Testing</h3>	
					<ul>
					<li><a href="/students/assessments/remote/index.php">Remote Testing</a></li>
					<li><a href="/students/assessments/remote/option-1.php">Remote Testing Option 1 </a></li>
					<li><a href="/students/assessments/remote/option-2.php">Remote Testing Option 2</a></li>
					<li><a href="/students/assessments/remote/option-3.php">Remote Testing Option 3</a></li>
					</ul>
                </div>
                <div class="col-md-6">
                  <h3>Criminal Justice Basic Abilities Test 
					  <br />(CJBAT)</h3>
                  <ul>
                    <li><a href="/students/assessments/criminal-justice-basic-abilities-test/index.php">Criminal Justice Basic Abilities Test (<strong>CJBAT</strong>)</a></li>
                    <li><a href="/students/assessments/criminal-justice-basic-abilities-test/taking-the-exam.php">Taking the CJBAT</a></li>
                    <li><a href="/students/assessments/criminal-justice-basic-abilities-test/policies.php">CJBAT Testing Policies</a></li>
                    <li><a href="/students/assessments/criminal-justice-basic-abilities-test/transcripts.php">CJBAT Transcripts</a></li>
                    <li><a href="/students/assessments/criminal-justice-basic-abilities-test/frequently-asked-questions.php">CJBAT FAQs</a></li>
                  </ul>
                 <h3>Test of Essential Academic Skills (TEAS)</h3>
                  <ul>
                    <li><a href="/students/assessments/test-of-essential-academic-skills/index.php">Test of Essential Academic Skills (<strong>TEAS</strong>)</a></li>
                    <li><a href="/students/assessments/test-of-essential-academic-skills/taking-the-exam.php">Taking the TEAS</a></li>
                    <li><a href="/students/assessments/test-of-essential-academic-skills/policies.php">TEAS Testing Policies</a></li>
                    <li><a href="/students/assessments/test-of-essential-academic-skills/transcripts.php">TEAS Transcripts</a></li>
                    <li><a href="/students/assessments/test-of-essential-academic-skills/faqs.php">TEAS FAQs</a></li>
                  </ul>
                </div>
              </div>
              <!-- End menu-wrapper --></li>
               
<li><a href="/students/assessments/requesting-scores.php">Requesting/Submitting Scores</a></li>
<li><a href="/students/assessments/learning-preferences.php">Resources</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/assessments/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
