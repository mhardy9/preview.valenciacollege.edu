<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/placement-chart.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div>
                           <a href="index.html"></a>
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        <h2>Placement Charts</h2>
                        
                        <p><strong>If you are a prospective Dual Enrollment student -- Please click <a href="../admissions/dual-enrollment/index.html">here</a> for updated test score information. </strong></p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Reading Placement Scores</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Placement</strong></div>
                                 
                                 <div>
                                    <div><strong>PERT</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><strong>Accuplacer CPT </strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><strong>SAT</strong></p>
                                       
                                       <p><strong>Taken <em>prior</em> to March 1, 2016 </strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Redesigned SAT Reading Test Score </strong></p>
                                    
                                    <p><strong>Taken on or <em>after</em> March 1, 2016 </strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><strong>ACT</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>FCAT 2.0</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dev. Reading 1<br>
                                    ENC 0017 
                                 </div>
                                 
                                 <div>
                                    <div>50-83*</div>
                                 </div>
                                 
                                 <div>
                                    <div>&amp;lt; 59 </div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dev. Reading 2<br>
                                    REA 0017
                                 </div>
                                 
                                 <div>
                                    <div>84-105</div>
                                 </div>
                                 
                                 <div>
                                    <div>60-82</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Freshman Composition I **<br>
                                    ENC1101
                                 </div>
                                 
                                 <div>
                                    <div>106-150</div>
                                 </div>
                                 
                                 <div>
                                    <div>83 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    <div>440 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    <div>24 or &amp;gt; </div>
                                 </div>
                                 
                                 <div>
                                    <div>19 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    <div>262 &amp;gt;</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>*If you also score  90-102 on PERT Writing, you place into ENC 0027. Please refer
                                       to catalog for more information. 
                                    </p>
                                    
                                    <p><a href="http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/">http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/</a> 
                                    </p>
                                    
                                    <p>**Enrollment in ENC 1101 Freshman Composition I (the first college-level English course)
                                       requires college-level placement scores in both English and Reading for PERT, CPT
                                       and ACT or for students to be non-mandated into developmental education under Senate
                                       Bill 1720. The SAT and FCAT 2.0 only requires a college-level Reading Score. 
                                    </p>
                                    
                                    <p>Effective immediately, Valencia will also use the Redesigned SAT Reading Test score
                                       (taken <em>after</em> March 1, 2016) for placement until approval of new scores are in State Board Rule
                                       6A-10.0315. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <br>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Writing Placement Scores</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Placement</strong></div>
                                 
                                 <div>
                                    <div><strong>PERT</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Accuplacer CPT</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><strong>SAT </strong></p>
                                       
                                       <p><strong> Taken <em>prior</em> to March 1, 2016 </strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Redesigned SAT Writing Test Score </strong></p>
                                    
                                    <p><strong>Taken on or <em>after</em> March 1, 2016 </strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><strong>ACT English </strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>FCAT 2.0 Reading</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dev. Writing 1<br>
                                    ENC 0017
                                 </div>
                                 
                                 <div>
                                    <div>50-89*</div>
                                 </div>
                                 
                                 <div>
                                    <div>&amp;lt; 53</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>N/A</div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dev. Writing 2<br>
                                    ENC 0025
                                 </div>
                                 
                                 <div>
                                    <div>90-102*</div>
                                 </div>
                                 
                                 <div>
                                    <div>54-82</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>N/A</div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Freshman Composition I ** <br>
                                    ENC1101
                                 </div>
                                 
                                 <div>
                                    <div>103-150</div>
                                 </div>
                                 
                                 <div>
                                    <div>83 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Refer to SAT reading score  </p>
                                    
                                 </div>
                                 
                                 <div>25 or &amp;gt; </div>
                                 
                                 <div>
                                    <div>17 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    <div>Refer to FCAT 2.0 Reading score</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>*If you also score  84-105 on PERT Reading, you place into ENC 0027. Please refer
                                       to catalog for more information
                                    </p>
                                    
                                    <p><a href="http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/">http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/</a> 
                                    </p>
                                    
                                    <p>**Enrollment in ENC 1101 Freshman Composition I (the first college-level English course)
                                       requires college-level placement scores in both English and Reading for PERT, CPT
                                       and ACT or for students to be non-mandated into developmental education under Senate
                                       Bill 1720. The SAT and FCAT 2.0 only requires a college-level Reading Score. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <br>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Math Placement Scores</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Placement</strong></div>
                                 
                                 <div>
                                    <div><strong>PERT</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Accuplacer CPT</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p><strong>SAT</strong></p>
                                       
                                       <p><strong>Taken <em>prior</em> to March 1, 2016</strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 <div>
                                    
                                    <p><strong>Redesigned SAT</strong></p>
                                    
                                    <p><strong>Taken on or <em>after</em> March 1, 2016</strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div><strong>ACT</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dev. Mathematics I <br>
                                    MAT 0018C or MAT 0022C 
                                 </div>
                                 
                                 <div>
                                    <div>50-95</div>
                                 </div>
                                 
                                 <div>
                                    <div>&amp;lt; 41</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dev. Mathematics II<br>
                                    MAT 0022C - MAT 0028C 
                                 </div>
                                 
                                 <div>
                                    <div>96-113</div>
                                 </div>
                                 
                                 <div>
                                    <div>42-71</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                                 <div>
                                    <div>N/A</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Statistics for Undergraduates -STA1001C<br>
                                       <br>
                                       College Math - MGF 1106 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>96 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>42 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>440 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>24 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>19 or more </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Intermediate Algebra - MAT 1033C<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>114-122</div>
                                 </div>
                                 
                                 <div>
                                    <div>72-89</div>
                                 </div>
                                 
                                 <div>
                                    <div>440-499</div>
                                 </div>
                                 
                                 <div>
                                    <div>24-26</div>
                                 </div>
                                 
                                 <div>
                                    <div>19 or 20</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>College Level Math*</p>
                                    
                                    <p>MAC 1105 or STA 2023 </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>123-150</div>
                                 </div>
                                 
                                 <div>
                                    <div>90 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    <div>500 or &amp;gt;</div>
                                 </div>
                                 
                                 <div>
                                    <div>26.5 or &amp;gt; </div>
                                 </div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p>21 or &amp;gt;</p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>*Take CPT-I testing for possible higher placement (optional)
                                       
                                    </p>
                                    
                                    <p>Effective immediately, Valencia will also use the Redesigned SAT Math Test score (taken
                                       <em>after</em> March 1, 2016) for placement until approval of new scores are in State Board Rule
                                       6A-10.0315. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <br>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>CPTI Placement</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>64 or less</div>
                                 
                                 <div>
                                    <div>MAC 1105: College Algebra<br>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>65 or more</div>
                                 
                                 <div>
                                    <div>
                                       
                                       <p> MAC 1114: College Trigonometry<br>
                                          MAC 1140: Pre-Calculus Algebra<br>
                                          MAC 2233: Calculus for Business and Social Science<br>
                                          MAE 2801: Elementary School Math<br>
                                          MHF 2300: Logic and Proof in Math 
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>89 or more</div>
                                 
                                 <div>
                                    <div>MAC 2311: Calculus with Analytical Geometry I </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <br>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>English for Academic Purposes (EAP)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Assessment</strong></div>
                                 
                                 <div>
                                    <div><strong>Score</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Level Placement </strong></div>
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Course</strong></div>                      
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Compass ESL </div>
                                 
                                 <div>
                                    <div>12.4 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 0 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Admission delayed; referral to Adult Education ESOL </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>12.5 - 29.9</div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 2 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP 0281 </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>30.0 - 39.9</div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 3 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP0300C, EAP0320C, EAP0340C, EAP0360C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>40.0 - 49.9</div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 4 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP0400C, EAP0420C, EAP0440C, EAP0460C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>50.0 - 69.9</div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 5 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>70.0 - 97.4 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 6 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1620C, EAP1640C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>97.5 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 7 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Use PERT, ACT, or SAT for English and reading placements (not EAP) </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>LOEP</div>
                                 
                                 <div>
                                    <div>65 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 0 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Admission delayed; referral to Adult Education ESOL </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>66 - 75 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 2 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP 0281 </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>76 - 85 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 3 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP0300C, EAP0320C, EAP0340C, EAP0360C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>86 - 95 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 4 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP0400C, EAP0420C, EAP0440C, EAP0460C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>96 - 105 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 5 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>106 - 115 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 6 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1620C, EAP1640C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>116 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 7 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>TOEFL</div>
                                 
                                 <div>
                                    <div><strong>Internet Based </strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Computer Version </strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Paper Version </strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>44 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>132 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>449 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>Take Compass ESL for placement </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>45 - 53 </div>
                                 </div>
                                 
                                 <div>
                                    <div>133 - 156</div>
                                 </div>
                                 
                                 <div>
                                    <div>450 - 479 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP0400C, EAP0420C, EAP0440C, EAP0460C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>54 - 63 </div>
                                 </div>
                                 
                                 <div>
                                    <div>157 - 179 </div>
                                 </div>
                                 
                                 <div>
                                    <div>480 - 509 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>64 - 70 </div>
                                 </div>
                                 
                                 <div>
                                    <div>180 - 196 </div>
                                 </div>
                                 
                                 <div>
                                    <div>510 - 529 </div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1620C, EAP1640C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>71 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>197 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>530 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        <br>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>Non-Immigrants (International Students) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Non-Immigrants (international students) who do not speak English as a native language
                                    must submit a satisfactory score on the Compass ESL test, the LOEP and Essay <em>or</em> TOEFL. 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong>Assessment</strong></div>
                                 
                                 <div>
                                    <div><strong>Score</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Level Placement </strong></div>
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Course</strong></div>                      
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Compass ESL 
                                    
                                    <p><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>0 - 39.9 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Levels 0, 2, 3 </div>
                                 </div>
                                 
                                 <div>                      
                                    Not eligible for admission to Valencia 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>40.0 - 49.9 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 4 </div>
                                 </div>
                                 
                                 <div>                       
                                    EAP0400C, EAP0420C, EAP0440C, EAP0460C 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>50.0 - 69.9 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 5 </div>
                                 </div>
                                 
                                 <div> EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>70.0 - 97.4 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 6 </div>
                                 </div>
                                 
                                 <div> EAP1620C, EAP1640C </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>97.5 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 7 </div>
                                 </div>
                                 
                                 <div> Use PERT, ACT, or SAT for English and reading placements (not EAP) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>LOEP</p>
                                    
                                 </div>
                                 
                                 <div>
                                    <div>65 - 85 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Levels 0, 2, 3 </div>
                                 </div>
                                 
                                 <div>Not eligible for admission to Valencia </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>86 - 95 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 4 </div>
                                 </div>
                                 
                                 <div>EAP0400C, EAP0420C, EAP0440C, EAP0460C </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>96 - 105 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 5 </div>
                                 </div>
                                 
                                 <div>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>106 - 115 </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 6 </div>
                                 </div>
                                 
                                 <div>EAP1620C, EAP1640C </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>116 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>Level 7 </div>
                                 </div>
                                 
                                 <div>Use PERT, ACT, or SAT for English and reading placements (not EAP) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>TOEFL</div>
                                 
                                 <div>
                                    <div><strong>Internet Based</strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Computer Version </strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Paper Version </strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Course</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>44 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>132 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>449 or less </div>
                                 </div>
                                 
                                 <div>Not eligible for admission to Valencia</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>45 - 53 </div>
                                 </div>
                                 
                                 <div>
                                    <div>133 - 156 </div>
                                 </div>
                                 
                                 <div>
                                    <div>450 - 479 </div>
                                 </div>
                                 
                                 <div>EAP0400C, EAP0420C, EAP0440C, EAP0460C </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>54 - 63 </div>
                                 </div>
                                 
                                 <div>
                                    <div>157 - 179 </div>
                                 </div>
                                 
                                 <div>
                                    <div>480 - 509 </div>
                                 </div>
                                 
                                 <div>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>64 - 70 </div>
                                 </div>
                                 
                                 <div>
                                    <div>180 - 196</div>
                                 </div>
                                 
                                 <div>
                                    <div>510 - 529 </div>
                                 </div>
                                 
                                 <div>EAP1620C, EAP1640C </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>71 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>197 or more </div>
                                 </div>
                                 
                                 <div>
                                    <div>530 or more </div>
                                 </div>
                                 
                                 <div>Use PERT, ACT, or SAT for English and reading placements (not EAP) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>IELTS</div>
                                 
                                 <div>
                                    <div><strong>IELTS Score </strong></div>
                                 </div>
                                 
                                 <div>
                                    <div><strong>Level</strong></div>
                                 </div>
                                 
                                 <div>                      
                                    
                                    <div><strong>Course</strong></div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>5.0 or less </div>
                                 </div>
                                 
                                 <div>
                                    <div>Take Compass for placement </div>
                                 </div>
                                 
                                 <div>
                                    <div>Not eligible for admission to Valencia </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>5.5</div>
                                 </div>
                                 
                                 <div>
                                    <div>4</div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP0400C, EAP0420C, EAP0440C, EAP0460C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>6.0</div>
                                 </div>
                                 
                                 <div>
                                    <div>5</div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>6.5</div>
                                 </div>
                                 
                                 <div>
                                    <div>6</div>
                                 </div>
                                 
                                 <div>
                                    <div>EAP1620C, EAP1640C </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <div>7.0 or above </div>
                                 </div>
                                 
                                 <div>
                                    <div>7</div>
                                 </div>
                                 
                                 <div>
                                    <div>Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>        
                        
                        
                        <p><a href="Placement-Chart.html#">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/placement-chart.pcf">©</a>
      </div>
   </body>
</html>