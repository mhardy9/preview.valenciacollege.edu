<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Requesting and Submitting Scores  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments, dual, enrollment, requesting, submitting, scores">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/requesting-scores.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Requesting and Submitting Scores </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        
                        <p>
                           <i class="far fa-info-circle" aria-hidden="true"></i> Valencia only accepts scores within the past 2 years.
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>PERT (Post Secondary Readiness Test) Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>Retrieve PERT scores from a Florida school</strong>: If you would like to submit PERT scores taken in the state of Florida within the
                              last 2 years, please&nbsp;<a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">contact</a>&nbsp;us with the following information:
                           </p>
                           
                           <p><strong>Full name, VID#, email, phone and where you took the PERT.</strong></p>
                           
                           <p>We will use this information to retrieve your scores in the Florida PERT Repository.</p>
                           
                           <p><a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">PERT Scores Request Form</a></p>
                           
                           <p><strong>Send PERT scores to a different school:</strong>&nbsp;If you would like another Florida institution to receive your PERT scores, ask the
                              receiving institution to retrieve your scores from the Florida PERT Repository. If
                              your institution does not have access to the PERT Repository, please fill out the
                              following form:&nbsp;<a href="/documents/studets/offices-services/assessments/TestScoreRequestForm_000.pdf" target="_blank">Valencia Score Request Form</a>.
                           </p>
                           
                        </div>         
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>Accuplacer CPT (College Placement Test) Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you have taken the Accuplacer in the past 2 years, please have&nbsp;<strong>the institution</strong>&nbsp;fax, mail or email us your scores and call us to ensure we have recieved them.
                           </p>
                           
                           <p>We will&nbsp;<u>only accept</u>&nbsp;the following Accuplacer tests:
                           </p>
                           
                           <p><strong>CPT Reading (Comprehension)</strong></p>
                           
                           <p><strong>CPT Sentence Skills (Writing)</strong></p>
                           
                           <p><strong>CPT Elementary Algebra (Math)</strong></p>
                           
                           <p>Click&nbsp;<a href="https://valenciacollege.edu/assessments/placement-chart.cfm" target="_blank">here</a>&nbsp;to review the&nbsp;<strong>CPT</strong>&nbsp;score placement.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           </p>
                           
                           <p><strong>Assessment email:&nbsp;</strong>assessment@valenciacollege.edu
                           </p>
                           
                           <p><strong>Assessment fax:</strong>&nbsp;407-582-1682
                           </p>
                           
                           <p><strong>Assessment address</strong>: ATT: Assessment Services
                           </p>
                           
                           <p>1800 S Kirkman Rd</p>
                           
                           <p>Orlando, FL 32811</p>
                           
                           <p><strong>Assessment phone</strong>: 407-582-1101
                           </p>
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>Scores must be sent directly from the institution.</strong>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>ACT/SAT Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you have taken the ACT or SAT in the past two years and they are at&nbsp;<a href="placement-chart.cfm" target="_blank">college level</a>, please have your scores sent to us electronically or bring us the&nbsp;<u>original score report.</u>&nbsp;No copied or scanned score reports will be accepted.
                           </p>
                           
                           <p><strong>Please note: Sending SAT and ACT scores electronically will take 5 weeks to receive
                                 and process.</strong></p>
                           
                           <p>ACT scores can be sent electronically from your ACT account:&nbsp;<a href="http://www.actstudent.org/scores/send/" target="_blank">http://www.actstudent.org/scores/send/</a></p>
                           
                           <p>SAT scores can be sent electronically from your CollegeBoard Account:&nbsp;<a href="http://sat.collegeboard.org/scores/" target="_blank">http://sat.collegeboard.org/scores/</a></p>
                           
                           <p>**Please note, Valencia cannot provide official scores for this test. All request
                              must be done through CollegeBoard.
                           </p>
                           
                        </div>                               
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>TEAS Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you would like to transfer scores from tests taken at Valencia College, go to the&nbsp;<a href="http://www.atitesting.com/ati_store/" target="_blank">ATI testing online store</a>, click on TEAS/Discover Transcripts. You will be asked to fill out a form. The scores
                              will be sent electronically.
                           </p>
                           
                           <p>Valencia will accept your previous TEAS V scores from another institution. You may
                              request your scores by going to&nbsp;<a href="http://www.atitesting.com/ati_store/" target="_blank">ATI online stores</a>. Please make sure your scores are sent to Valencia College (Valencia ADN).
                           </p>
                           
                           <p>**Please note, Valencia cannot provide official scores for this test. All requests
                              must be done through ATItesting.com
                           </p>
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>CJBAT Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">If you would like to request your CJBAT scores please complete the&nbsp;<a href="/documents/students/assessments/TestScoreRequestForm.pdf" target="_blank">Valencia College Test Score Request Form</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>. Make sure to fill out the form completely including your Date of Birth and VID number.
                           Attach a copy of a government issued picture ID that also includes your signature.&nbsp;Request
                           forms without a picture ID with signature will not be processed. Incomplete requests
                           will not be honored.&nbsp;Please fax the form to the Assessment Center of your choice,
                           specific details are located on the form.
                           
                        </div>              
                        
                        
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>CLEP Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you plan to attend another college or university, your CLEP scores do not transfer
                              automatically with your Valencia College transcript. You will need to send your CLEP
                              scores in addition to your Valencia transcript. In order to send additional transcripts,
                              call 1-800-257-9558 or by completing a&nbsp;<a href="https://secure-media.collegeboard.org/digitalServices/pdf/clep/clep-transcript-request-form.pdf" target="_blank">CLEP Transcript Request Form</a> <i class="far fa-file-pdf-o" aria-hidden="true"></i>&nbsp;and mail it to:
                           </p>
                           
                           <blockquote>P.O. Box 6600<br>
                              Princeton, NJ<br>
                              08541-6600
                           </blockquote>
                           
                           <p>Click<a href="https://clep.collegeboard.org/about/score" target="_blank">&nbsp;here</a>&nbsp;to read more about CLEP scores.
                           </p>
                           
                           <p>**Please note the following:</p>
                           
                           <ul class="list_4">
                              
                              <li>Valencia cannot provide official scores for this test. All request must be done through
                                 CollegeBoard.
                              </li>
                              
                              <li>Valencia will ONLY accept CLEP transcripts through CollegeBoard.</li>
                              
                           </ul>
                           
                           <p><i class="far fa-info-circle" aria-hidden="true"></i> <strong>It takes 4 weeks to receive and process CLEP score reports.</strong></p>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>CLAST Scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Teacher certification candidates, who wish to submit CLAST scores to the&nbsp;&nbsp;<a href="http://www.fldoe.org/teaching/certification/" target="_blank">Bureau of Educator Certification</a>, will need to obtain an official score report that display their CLAST scores.
                           </p>
                           
                           <ul>
                              
                              <li>If the teacher certification candidate took the CLAST prior to July 1, 2002, then
                                 the Florida Teacher Certification Examination (FTCE) testing contractor, ES Pearson,
                                 will have their CLAST scores on file and can produce a duplicate score report, which
                                 will be accepted by the Bureau of Educator Certification.<a href="http://www.fl.nesinc.com/FL_RequestingScoreCopy.asp" target="_blank">&nbsp;Instructions from ES Pearson for obtaining a duplicate score report</a>.
                              </li>
                              
                              <li>If an individual took the CLAST before July 1, 2002, but did not pass the subtest(s)
                                 until after July 1, 2002, the passing score will not be on file and is not eligible
                                 for teacher certification purposes.
                              </li>
                              
                              <li>If an individual's CLAST scores are listed on their transcript as a 999(99), 998(98),
                                 997(97), or 996(96), then the individual was exempted from taking the CLAST and, therefore,
                                 does not have passing scores.
                              </li>
                              
                           </ul>
                           
                           <p><a href="/documents/studets/offices-services/assessments/CLASTscoreform_updated2015_FormOnly.pdf" target="_blank">Pearson Score Request Form</a>&nbsp;<i class="far fa-file-pdf-o" aria-hidden="true"></i> if taken prior to July 2002 or after the 60 day window has expired.
                           </p>
                           
                           <p>More information about the CLAST can be found&nbsp;<a href="http://www.fldoe.org/accountability/assessments/postsecondary-assessment/clast/" target="_blank">here</a>.
                           </p>
                           
                           <p>**Please note, Valencia cannot provide official scores for this test. All request
                              must be done through Pearson or&nbsp;<a href="http://www.fldoe.org/accountability/assessments/postsecondary-assessment/clast/" target="_blank">FLDOE</a>.
                           </p>
                           
                        </div>                                                                                 
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/requesting-scores.pcf">©</a>
      </div>
   </body>
</html>