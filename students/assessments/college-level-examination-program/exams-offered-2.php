<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/college-level-examination-program/exams-offered-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/college-level-examination-program/">Clep</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>CLEP Exams Offered</h2>
                        
                        <p>It takes 3-6 weeks to receive and process official CLEP scores. </p>
                        
                        <p>Credit will be awarded  using the following chart:</p>
                        
                        <div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>CLEP Test</div>
                                 
                                 <div>Score </div>
                                 
                                 <div>Course Credit </div>
                                 
                                 <div>Credit Hours </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>American Government </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>POS 2041 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>American Literature </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>AML 2000 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Calculus w/ Elementary Functions </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>MAC 2233 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>College Algebra </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>MAC 1105 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>College Mathematics </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>MGF1106 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>College Composition </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>ENC1101,1102</div>
                                 
                                 <div>6</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>English Literature </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>ENL 2000 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Financial Accounting</div>
                                 
                                 <div>50</div>
                                 
                                 <div>ACG 1001*</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>General Biology </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>BSC 1005 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>General Chemistry </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>CHM 1020 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>History of the United States I </div>
                                 
                                 <div>50</div>
                                 
                                 <div>AMH 2010</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>History of the United States II </div>
                                 
                                 <div>50</div>
                                 
                                 <div>AMH 2020</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Human Growth and Development </div>
                                 
                                 <div>50</div>
                                 
                                 <div>DEP 2004</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Humanities </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>HUM2250 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Information Systems</div>
                                 
                                 <div>50 </div>
                                 
                                 <div>CGS 1077 * </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Introduction to Educational Psych. </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>EDP 2002 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Introductory Business    Law </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>BUL 2241 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Introductory    Psychology </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>PSY 2012 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Introductory Sociology </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>SYG 2000 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Precalculus </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>MAC1140 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Principles of Management </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>MAN 2021 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Principles of    Macroeconomics </div>
                                 
                                 <div>50</div>
                                 
                                 <div>ECO 2013</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Principles of    Microeconomics </div>
                                 
                                 <div>50</div>
                                 
                                 <div>ECO 2023</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Principles of    Marketing </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>MAR2011 </div>
                                 
                                 <div>3 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Western Civilization I </div>
                                 
                                 <div>50</div>
                                 
                                 <div>EUH 2000</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Western Civilization    II </div>
                                 
                                 <div>50</div>
                                 
                                 <div>EUH 2001</div>
                                 
                                 <div>3</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>French </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>FRE 1120</div>
                                 
                                 <div>4</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>59</div>
                                 
                                 <div>FRE 1120, 1121</div>
                                 
                                 <div>8</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>German </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>GER 1120</div>
                                 
                                 <div>4</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>60</div>
                                 
                                 <div>GER 1120, 1121</div>
                                 
                                 <div>8</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Spanish </div>
                                 
                                 <div>50 </div>
                                 
                                 <div>SPN 1120</div>
                                 
                                 <div>4</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>63</div>
                                 
                                 <div>SPN 1120, 1121</div>
                                 
                                 <div>8</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>* These courses are not listed in the Valencia catalog. Please speak with an advisor
                           before taking this test. 
                        </p>
                        
                        <p><a href="exams-offered.html#">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/college-level-examination-program/exams-offered-2.pcf">©</a>
      </div>
   </body>
</html>