<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Level Examination Program  | Valencia College</title>
      <meta name="Description" content="College Level Examination Program">
      <meta name="Keywords" content="college, school, educational, assessments, clep, exam">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/college-level-examination-program/exams-offered.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/college-level-examination-program/">Clep</a></li>
               <li>College Level Examination Program </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>CLEP Exams Offered</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Credit will be awarded using the following chart:</p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr style="background-color: #bf311a; color:white">
                                    
                                    <th>CLEP Test</th>
                                    
                                    <th>Score</th>
                                    
                                    <th>Course Credit</th>
                                    
                                    <th>Credit Hours</th>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>American Government</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">POS 2041</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>American Literature</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">AML 2000</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Calculus w/ Elementary Functions</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">MAC 2233</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>College Algebra</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">MAC 1105</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>College Mathematics</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">MGF1106</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>College Composition</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">ENC1101,1102</td>
                                    
                                    <td align="center">6</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>English Literature</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">ENL 2000</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Financial Accounting</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">ACG 1001*</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>General Biology</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">BSC 1005</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>General Chemistry</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">CHM 1020</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>History of the United States I</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">AMH 2010</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>History of the United States II</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">AMH 2020</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Human Growth and Development</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">DEP 2004</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Humanities</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">HUM2250</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Information Systems</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">CGS 1077 *</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Introduction to Educational Psych.</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">EDP 2002</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Introductory Business Law</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">BUL 2241</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Introductory Psychology</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">PSY 2012</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Introductory Sociology</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">SYG 2000</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Precalculus</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">MAC1140</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Principles of Management</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">MAN 2021</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Principles of Macroeconomics</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">ECO 2013</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Principles of Microeconomics</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">ECO 2023</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Principles of Marketing</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">MAR2011</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Western Civilization I</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">EUH 2000</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Western Civilization II</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">EUH 2001</td>
                                    
                                    <td align="center">3</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="2">French</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">FRE 1120</td>
                                    
                                    <td align="center">4</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td align="center">59</td>
                                    
                                    <td align="center">FRE 1120, 1121</td>
                                    
                                    <td align="center">8</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="2">German</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">GER 1120</td>
                                    
                                    <td align="center">4</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td align="center">60</td>
                                    
                                    <td align="center">GER 1120, 1121</td>
                                    
                                    <td align="center">8</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="2">Spanish</td>
                                    
                                    <td align="center">50</td>
                                    
                                    <td align="center">SPN 1120</td>
                                    
                                    <td align="center">4</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td align="center">63</td>
                                    
                                    <td align="center">SPN 1120, 1121</td>
                                    
                                    <td align="center">8</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>* These courses are not listed in the Valencia catalog. Please speak with an advisor
                              before taking this test.
                           </p>
                           
                           <p><i class="far fa-info-circle" aria-hidden="true"></i> <strong>It takes 3-6 weeks to receive and process official CLEP scores.</strong></p>
                           
                           <p>&nbsp;</p>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/college-level-examination-program/exams-offered.pcf">©</a>
      </div>
   </body>
</html>