<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/college-level-examination-program/clepfaqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/college-level-examination-program/">Clep</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>CLEP FAQ </h2>
                        
                        
                        <p><strong>Can anyone take the CLEP exams at Valencia?</strong></p>
                        
                        <p>As Valencia is not an open testing institution, only students with an active Valencia
                           application on file may take the CLEP exams at Valencia.
                        </p>
                        
                        <p><strong>Can I take the CLEP exams at any Valencia Campus?</strong></p>
                        
                        <p>The CLEP exams are offered by appointment only at our East, West and Osceola Campuses.
                           Note: CLEP testing is not available at the Lake Nona or Winter Park campus. Paperwork
                           can be processed through these campuses, however the test can only be taken at West,
                           East and Osceola. 
                        </p>
                        
                        <p><strong>Can I get a receipt for my payment to CollegeBoard?</strong></p>
                        
                        <p>Unfortunately, we are not able to provide a receipt for the CLEP exam. The payment
                           for the CLEP is handled between the student and College Board. However, the Valencia
                           Business Office will provide a receipt for the $20 appointment fee.
                        </p>
                        
                        <p><strong>Is the CLEP exam timed?</strong></p>
                        
                        <p>The CLEP exams are timed at 90 minutes with the exception of the College Composition
                           which is timed at 120 minutes.
                        </p>
                        
                        <p><strong>What do I need to bring with me to take the CLEP exam?</strong></p>
                        
                        <ul>
                           
                           <li>
                              <u>You must hand in the printed registration ticket from </u><a href="http://www.collegeboard.org/">www.collegeboard.org</a> to the Assessment Center
                           </li>
                           
                           <li>
                              <u>You must have</u> a current government issued photo ID
                           </li>
                           
                        </ul>            
                        
                        
                        <p>Please refer to the <a href="policies.html">CLEP Policy</a> for more information on Prohibitied items. 
                        </p>
                        
                        
                        <p><strong>Can I send my CLEP exam scores to another institution?</strong></p>
                        
                        <p>If you plan to attend another college or university, your CLEP scores do not transfer
                           automatically with your Valencia College transcript. You will need to send your CLEP
                           scores in addition to your Valencia transcript. In order to send additional transcripts,
                           call 1-800-257-9558<a href="../requesting-scores.html"><img alt="If you plan to attend another college or university, your CLEP scores do not transfer automatically with your Valencia College transcript. You will need to send your CLEP scores in addition to your Valencia transcript. In order to send additional transcripts, call 1-800-257-9558 or by completing a CLEP Transcript Request Form and mail it to: " src="xEPMsFKs7YmM8bsAMlyf2OwTPszAMZMeayGCpJVAAAAAElFTkSuQmCC"></a> or by completing a <a href="https://secure-media.collegeboard.org/digitalServices/pdf/clep/clep-transcript-request-form.pdf">CLEP Transcript Request Form</a> and mail it to: 
                        </p>
                        P.O. Box 6600<br>
                        Princeton, NJ<br>
                        08541-6600
                        
                        <p>Click <a href="http://clep.collegeboard.org/about/score">here</a> to read more about CLEP scores. 
                        </p>
                        
                        <p>**Please note: Valencia cannot provide official scores for this test. All requests
                           must be done through CollegeBoard. 
                        </p>
                        
                        <p><strong>How long does it take for my CLEP exam scores to appear on my transcripts?</strong></p>
                        
                        <p>It can take up to 6 weeks for passing CLEP exam scores to appear on your record.</p>
                        
                        <p><strong>What is on the CLEP exam? Is there a study guide?</strong></p>
                        
                        <p>The CLEP exams encompass subjects that you would normally see on a final exam. For
                           specific information on a particular subject, please click <a href="https://store.collegeboard.org/sto/catalog.do?category=298&amp;categoryName=CLEP?">HERE</a>! . 
                        </p>
                        
                        <p>Please keep in mind that not all CLEP exams are accepted/available at Valencia. For
                           a list of tests we currently offer/accept please visit <a href="exams-offered.html">http://valenciacollege.edu/assessments/clep/exams-offered.cfm</a></p>
                        
                        
                        <p><strong>What subjects are offered through the CLEP exams?</strong></p>
                        
                        <p>The CLEP exams encompass many subjects; however, at Valencia we have a select list
                           of exams we offer. For the complete list, please visit <a href="exams-offered.html">http://valenciacollege.edu/assessments/clep/exams-offered.cfm</a>.
                        </p>
                        
                        
                        <p><strong>Will the CLEP exam change my GPA?</strong></p>
                        
                        <p>A passing score will only give you credit for the class. It will not have any impact
                           on your GPA.
                        </p>
                        
                        
                        
                        <p><strong>How do I register for the CLEP exam?</strong></p>
                        
                        <p>Please visit the <a href="taking-the-exam.html">Taking the CLEP</a> tab for the steps to register for a CLEP exam. 
                        </p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/college-level-examination-program/clepfaqs.pcf">©</a>
      </div>
   </body>
</html>