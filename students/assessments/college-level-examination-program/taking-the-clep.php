<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Level Examination Program  | Valencia College</title>
      <meta name="Description" content="College Level Examination Program">
      <meta name="Keywords" content="college, school, educational, assessments, clep, exam, taking">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/college-level-examination-program/taking-the-clep.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/college-level-examination-program/">Clep</a></li>
               <li>College Level Examination Program </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>3 steps to test at Valencia College</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>Step #1 – Register</h3>
                           
                           <ul>
                              
                              <li>Log into your&nbsp;<a href="https://atlas.valenciacollege.edu/">ATLAS account</a>&nbsp;(you must be enrolled at Valencia with an active status)
                              </li>
                              
                              <li>Click on the Students tab at the top of the page</li>
                              
                              <li>Under the Student forms, locate the CLEP Information and Registration Link</li>
                              
                              <li>Read and follow instructions provided on the CLEP Registration Form.</li>
                              
                              <li>Go to the campus Business Office to pay the $20 appointment fee.&nbsp;<a href="https://clep.collegeboard.org/military" target="_blank">DANTES</a>students do NOT need to pay the $20 appointment fee. Eligibility for DANTES- Click&nbsp;<a href="https://valenciacollege.edu/assessments/clep/documents/DANTESEligibilityChange.pdf" target="_blank">Here</a>
                                 
                              </li>
                              
                              <li>
                                 <strong>Return to the Assessment Office</strong>&nbsp;and present the following items to register for your CLEP :
                                 
                                 <ul>
                                    
                                    <li>Printed registration form from ATLAS</li>
                                    
                                    <li>Receipt from the Business Office (proof of $ 20.00 payment)<strong>&nbsp;**&nbsp;<a href="https://valenciacollege.edu/businessoffice/payments.cfm" target="_blank">Valencia's Payment Method.</a></strong>
                                       
                                    </li>
                                    
                                    <li>Government issued photo ID</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                           <h3>Step #2 – Payment</h3>
                           
                           <ul>
                              
                              <li>Go to&nbsp;<a href="http://www.collegeboard.org/" target="_blank">www.collegeboard.org</a><strong><u></u></strong>
                                 
                              </li>
                              
                              <li>Click Sign Up<strong><u></u></strong>
                                 
                              </li>
                              
                              <li>Select the CLEP tab<strong><u></u></strong>
                                 
                              </li>
                              
                              <li>Select the CLEP Exams tab<strong><u></u></strong>
                                 
                              </li>
                              
                              <li>Find the CLEP exam you want and select (check mark) the CLEP $85.00 box<strong><u></u></strong>
                                 
                              </li>
                              
                              <li>Select Add to cart button<strong><u></u></strong>
                                 
                              </li>
                              
                              <li>Select the Register and Checkout button<strong><u></u></strong>
                                 
                              </li>
                              
                              <li>
                                 <strong><u>Print&nbsp;</u></strong>your registration ticket (you must submit it on the day of the test)
                                 
                                 <ul>
                                    
                                    <li>CLEP Study Guide information&nbsp;<a href="https://store.collegeboard.org/sto/catalog.do?category=298&amp;categoryName=CLEP?" target="_blank">HERE</a>!
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                           <p><i class="far fa-info-circle" aria-hidden="true"></i> <strong> Click&nbsp;<a href="http://clep.collegeboard.org/clep-exam-registration-tutorial">here</a>&nbsp;for a video tutorial on CLEP Exam Registration and payment through CollegeBoard</strong></p>
                           
                           <h3>Step #3 – Testing</h3>
                           
                           <ul>
                              
                              <li>
                                 <strong><u>You must hand in the printed registration ticket from&nbsp;</u></strong><a href="http://www.collegeboard.org/" target="_blank">www.collegeboard.org</a>&nbsp;to the Assessment Center
                              </li>
                              
                              <li>
                                 <strong><u>You must have</u></strong>&nbsp;a current government issued photo ID
                              </li>
                              
                              <li>Arrive punctually at the Assessment Center for your appointment or you might need
                                 to be re-scheduled
                              </li>
                              
                           </ul>
                           
                           
                           <ul>
                              
                              <li>Due to limited schedule availability, it is important, to keep your appointment. You
                                 will be allowed to reschedule&nbsp;<strong><u>once</u></strong>. Any further rescheduling will require an additional $20 fee. After 3 months, the
                                 payment expires and you will be required to resubmit your CLEP registration and related
                                 fee.
                              </li>
                              
                           </ul>
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>NOTE: It takes 3-6 weeks to receive and process&nbsp;<strong>official</strong>&nbsp;CLEP scores.</strong>
                           
                           <hr class="styled_2">
                           
                           <h3>More Details</h3>
                           
                           <ul>
                              
                              <li>CLEP is offered on East, Osceola, and West Campuses by appointment only</li>
                              
                              <li>Test scores&nbsp;<strong>must</strong>&nbsp;be sent to Valencia.
                              </li>
                              
                              <li>You will need to show a form of ID to test:</li>
                              
                           </ul>
                           
                           <p>The test taker’s identification must:</p>
                           
                           <ul>
                              
                              <li>Be government-issued</li>
                              
                              <li>Be an original document —photocopied documents are not acceptable</li>
                              
                              <li>Be valid and current —expired documents (bearing expiration dates that have passed)
                                 are not acceptable, no matter how recently they may have expired
                              </li>
                              
                              <li>Bear the test-taker's full name, in English language characters, exactly as it appears
                                 on the Registration Ticket, including the order of the names
                              </li>
                              
                              <li>Middle initials are optional and only need to match the first letter of the middle
                                 name when present on both the ticket and the identification.
                              </li>
                              
                              <li>Bear a recent recognizable photograph that clearly matches the test-taker</li>
                              
                              <li>Include the test-taker’s signature</li>
                              
                              <li>Be in good condition, with clearly legible text and a clearly visible photograph</li>
                              
                           </ul>
                           
                           <p>Acceptable forms of identification&nbsp;<u>MUST</u>&nbsp;have&nbsp;<strong>name, photograph and signature</strong>&nbsp;on one of the following:
                           </p>
                           
                           <ul>
                              
                              <li>Government-issued passport</li>
                              
                              <li>Driver’s license</li>
                              
                              <li>State or Province ID issued by the motor vehicle agency</li>
                              
                              <li>Military ID (including electronic signatures)</li>
                              
                              <li>National ID</li>
                              
                              <li>Tribal ID card</li>
                              
                              <li>A naturalization card or certificate of citizenship</li>
                              
                              <li>A Permanent Resident Card (Green Card)</li>
                              
                           </ul>
                           
                           <p>&nbsp;</p>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/college-level-examination-program/taking-the-clep.pcf">©</a>
      </div>
   </body>
</html>