<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/college-level-examination-program/taking-the-exam.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/college-level-examination-program/">Clep</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Taking the CLEP</h2>
                        
                        <p><strong>3 steps to test at Valencia College</strong></p>
                        
                        <p><strong><u>Step #1 – Register</u></strong></p>
                        
                        <ul>
                           
                           <li>Log into your <a href="https://atlas.valenciacollege.edu/">ATLAS account</a>  (you must be enrolled at Valencia with an active status)
                           </li>
                           
                           <li>Click on the Students tab at the top of the page</li>
                           
                           <li>Under the Student forms, locate the CLEP Information and Registration Link </li>
                           
                           <li>Read and follow instructions provided on the CLEP Registration Form. </li>
                           
                           <li>Go to the campus Business Office to pay the $20 appointment fee. <a href="https://clep.collegeboard.org/military" target="_blank">DANTES</a> students do NOT need to pay the $20 appointment fee. Eligibility for DANTES- Click
                              <a href="documents/DANTESEligibilityChange.pdf" target="_blank">Here</a> 
                           </li>
                           
                           <li>
                              <strong>Return to the Assessment Office</strong> and present the following items to register for your CLEP :
                              
                              <ul>
                                 
                                 <li>Printed registration form from ATLAS</li>
                                 
                                 <li>Receipt from the Business Office (proof of $ 20.00 payment)<strong> ** <a href="../../business-office/payments.html" target="_blank">Valencia's Payment Method.</a></strong> 
                                 </li>
                                 
                                 <li>Government issued photo ID</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong><u>Step #2 – Payment</u></strong></p>
                        
                        <ul>
                           
                           <li>Go to <a href="http://www.collegeboard.org" target="_blank">www.collegeboard.org</a><strong><u> </u></strong>
                              
                           </li>
                           
                           <li>Click Sign Up<strong><u></u></strong>
                              
                           </li>
                           
                           <li>Select the CLEP tab<strong><u></u></strong>
                              
                           </li>
                           
                           <li>Select the CLEP Exams tab<strong><u></u></strong>
                              
                           </li>
                           
                           <li>Find the CLEP exam you want and select (check mark) the CLEP $85.00 box<strong><u></u></strong>
                              
                           </li>
                           
                           <li>Select Add to cart button<strong><u></u></strong>
                              
                           </li>
                           
                           <li>Select the Register and Checkout button<strong><u></u></strong>
                              
                           </li>
                           
                           <li>
                              <strong><u>Print </u></strong>your registration ticket (you must submit it on the day of the test)
                              
                              <ul>
                                 
                                 <li>CLEP Study Guide information <a href="https://store.collegeboard.org/sto/catalog.do?category=298&amp;categoryName=CLEP?" target="_blank">HERE</a>! 
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p>Click <a href="http://clep.collegeboard.org/clep-exam-registration-tutorial">here</a> for a video tutorial on CLEP Exam Registration and payment through CollegeBoard
                        </p>
                        
                        <p><strong><u>Step #3 – Testing</u></strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong><u>You must hand in the printed registration ticket from </u></strong><a href="http://www.collegeboard.org" target="_blank">www.collegeboard.org</a> to the Assessment Center
                           </li>
                           
                           <li>
                              <strong><u>You must have</u></strong> a current government issued photo ID
                           </li>
                           
                           <li>Arrive punctually at the Assessment Center for your appointment or you might need
                              to be re-scheduled
                           </li>
                           
                        </ul>
                        
                        <p><strong><em>We look forward to having you test with us. If you have further questions or need
                                 more information, please visit our website:</em></strong></p>
                        
                        <p><a href="index.html" target="_blank">http://valenciacollege.edu/assessments/clep/</a></p>
                        
                        <ul>
                           
                           <li>Due to limited schedule availability, it is important, to keep your appointment. You
                              will be allowed to reschedule <strong><u>once</u></strong>. Any further rescheduling will require an additional $20 fee. After 3 months, the
                              payment expires and you will be required to resubmit your CLEP registration and related
                              fee.
                           </li>
                           
                        </ul>
                        
                        <p><strong>NOTE: It takes 3-6 weeks to receive and process 
                              
                              
                              <strong>official</strong>  CLEP scores. </strong></p>
                        
                        <h2>More Details: </h2>
                        
                        <ul>
                           
                           <li> CLEP is offered on East, Osceola, and West Campuses by appointment only</li>
                           
                           <li>Test scores <strong>must</strong> be sent to Valencia.
                           </li>
                           
                           <li>You will need to show  a form of ID to test:                  </li>
                           
                        </ul>
                        
                        
                        <p>The test taker’s identification must:</p>
                        
                        <ul>
                           
                           <li>Be government-issued </li>
                           
                           <li>Be an original document —photocopied documents are not acceptable</li>
                           
                           <li>Be valid and current —expired documents (bearing expiration dates that have passed)
                              are not acceptable, no matter how recently they may have expired
                           </li>
                           
                           <li>Bear the test-taker's full name, in English language characters, exactly as it appears
                              on the Registration Ticket, including the order of the names
                           </li>
                           
                           <li>Middle initials are optional and only need to match the first letter of the middle
                              name when present on both the ticket and the identification.
                           </li>
                           
                           <li>Bear a recent recognizable photograph that clearly matches the test-taker</li>
                           
                           <li>Include the test-taker’s signature</li>
                           
                           <li>Be in good condition, with clearly legible text and a clearly visible photograph</li>
                           
                        </ul>
                        
                        <p>Acceptable forms of identification <u>MUST</u> have <strong>name, photograph and signature</strong> on one of the following: 
                        </p>
                        
                        <ul>
                           
                           <li>Government-issued passport </li>
                           
                           <li>Driver’s license </li>
                           
                           <li>State or Province ID issued by the motor vehicle agency </li>
                           
                           <li>Military ID (including electronic signatures)</li>
                           
                           <li>National ID </li>
                           
                           <li>Tribal ID card </li>
                           
                           <li>A naturalization card or certificate of citizenship </li>
                           
                           <li>A Permanent Resident Card (Green Card)
                              
                              
                           </li>
                           
                        </ul>            
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/college-level-examination-program/taking-the-exam.pcf">©</a>
      </div>
   </body>
</html>