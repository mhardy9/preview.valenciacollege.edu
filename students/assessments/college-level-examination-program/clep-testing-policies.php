<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Level Examination Program  | Valencia College</title>
      <meta name="Description" content="College Level Examination Program">
      <meta name="Keywords" content="college, school, educational, assessments, clep, testing, policies">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/college-level-examination-program/clep-testing-policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/college-level-examination-program/">Clep</a></li>
               <li>College Level Examination Program </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>CLEP Testing Policies</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>In order to meet all graduation requirements, you should<u><strong>&nbsp;take the test prior to the term in which you expect to graduate</strong></u>.
                              </li>
                              
                              <li>Students are required to follow the general assessment lab&nbsp;<a href="https://valenciacollege.edu/assessments/testing-rules.cfm">Testing Policies&nbsp;</a>.
                              </li>
                              
                              <li>Any infraction of the rules will be considered cheating, and you will be dismissed
                                 from the test.
                              </li>
                              
                              <li>The CLEP is a computerized test available to all students enrolled at Valencia.</li>
                              
                              <li>You are able to earn up to 45 credits through the CLEP.</li>
                              
                              <li>CLEP credit meets all graduation requirements, including Gordon Rule mandates if applicable.</li>
                              
                              <li>You will receive a copy of your&nbsp;<em>unofficial</em>&nbsp;scores immediately* after testing; official test scores will be processed 4-6 weeks
                                 after your test date and any applicable credit entered into your record at that time.
                                 * College Composition test scores will be available only as official scores 4-6 weeks
                                 after the test.
                              </li>
                              
                              <li>You can earn a varying number of college credits with CLEP, depending on the particular
                                 test and the score earned. Click&nbsp;<a href="https://valenciacollege.edu/assessments/clep/exams-offered.cfm">here</a>&nbsp;for CLEP Exams offered at Valencia.
                              </li>
                              
                              <li>You are able to retake a CLEP exam after a wait period of 3 months (90 days). Retaking
                                 a test before the wait period is over will invalidate your scores, and you will forfeit
                                 any fees paid.
                              </li>
                              
                              <li>You may use CLEP credit under the repeat course policy for D or F grades only. Within
                                 the guidelines of the repeat policy, all attempts for a course will be counted in
                                 your GPA until you earn a grade of C or better when only the last attempt will be
                                 counted. If you earn an acceptable CLEP score in a course in which you have earned
                                 a D or F, the CLEP credit will be recorded and the D or F will no longer be computed
                                 in your GPA. No letter grades or quality points will be assigned.
                              </li>
                              
                              <li>Passing scores are for credit granting purposes only and do not compute into your
                                 grade point average (GPA).
                              </li>
                              
                              <li>Appointment fees are non-refundable and non-transferable.</li>
                              
                              <li>Students in need of special testing accommodations must contact the office of Students
                                 with Disabilities.
                              </li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>CLEP Prohibited Items</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Food, beverages, or tobacco products</li>
                              
                              <li>Hats (unless worn as a religious requirement)</li>
                              
                              <li>Hooded sweatshirts or sweaters</li>
                              
                              <li>Any device capable of recording audio, photographic, or video content, or capable
                                 of viewing or playing back such content
                              </li>
                              
                              <li>Any other unauthorized testing aids</li>
                              
                              <li>Calculators (a calculator function is built into the software for some exams)</li>
                              
                              <li>Candidate-provided keyboards</li>
                              
                              <li>Cellular phones/pagers, smart-phones, beepers, walkie-talkies, PDAs, or wireless communication
                                 devices (e.g., Black Berry, iPad)
                              </li>
                              
                              <li>Dictionaries, books, pamphlets, or reference materials</li>
                              
                              <li>Digital cameras</li>
                              
                              <li>Digital watches (wrist or pocket), alarm watches, or wristwatch cameras</li>
                              
                              <li>Flash/thumb drives or any other portable electronic devices</li>
                              
                              <li>Listening devices such as radios, media players with headphones, or recorder</li>
                              
                              <li>Mechanical pencils or any type of pen or highlighter</li>
                              
                              <li>Nonmedical electronic devices</li>
                              
                              <li>Papers of any kind (scratch paper will be provided by the test center administrator)</li>
                              
                              <li>Slide rules, protractors, compasses, or rulers</li>
                              
                              <li>Weapons or firearms</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/college-level-examination-program/clep-testing-policies.pcf">©</a>
      </div>
   </body>
</html>