<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/post-secondary-readiness-test/taking-the-exam.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/post-secondary-readiness-test/">Pert</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Taking the PERT</h2>
                        
                        
                        <p>In order to  take the PERT at Valencia you must:</p>
                        
                        <ul>
                           
                           <li>
                              <strong>Valencia Student-</strong> have an active application on file - the <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Admissions  application</a> must be completed at least 5 business days prior to testing.
                           </li>
                           
                           <li> <strong>Future Dual Enrollment Student</strong>- do not have to pay the application fee. Please see the <a href="../dual-enrollment.html" target="_blank">Dual Enrollment</a> page for more information. 
                           </li>
                           
                           <li>
                              <strong>Non-Valencia Student</strong>- fill out Option 2 on the <a href="../remote/index.html" target="_blank">Additional Testing Options</a> page. 
                           </li>
                           
                        </ul>
                        
                        
                        <p>All students who test at Valencia MUST: </p>
                        
                        <ul>
                           
                           <li>Provide a  valid and original government-issued  photo ID, such as a Driver's License,
                              Passport, or Green Card. 
                           </li>
                           
                        </ul>
                        
                        <p>Please note: Children are not allowed in the testing room. Proper supervision of small
                           children should be arranged off site prior to testing. 
                        </p>
                        
                        <h3>At a Glance </h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Hours Available</h3>
                                    
                                    <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                                    
                                    <p> <strong>Mon-Thurs</strong>: 8 AM - *5 PM<br>
                                       <strong>Friday</strong>: 9 AM - 4 PM
                                    </p>
                                    
                                    <p>*PERT takes 2.5 - 3 hours. Testing must be completed in the <span>same day</span>, unless a seven-day extension is needed. 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Test Fees</h3>
                                    
                                    <p>        Must pay application fees <br>
                                       Valencia Student- No Charge 1st time, $10 Retake fee per section 
                                    </p>
                                    
                                    <p>Non-Valencia Student- $25 </p>
                                    
                                    <p> <a href="../../business-office/payments.html">Valencia's Payment Method</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Retake Wait Period</h3>
                                    1 Day 
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Time Required</h3>
                                    
                                    <p>No Time Limit </p>
                                    
                                    <p>*PERT takes 2.5 - 3 hours. Testing must be completed in the <span>same day</span>, unless a seven-day extension is needed. Please allow yourself at least 2 1/2 to
                                       3 hours for all 3 sections of the PERT. <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Technical difficulties inherent with the internet, software or transmission may occur
                                       during your testing experience and are unpredictable. If unexpected technical difficulties
                                       occur, Proctors will make every attempt to promptly resume your assessment. Extended
                                       delays in testing may require rescheduling.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>        
                        <br>
                        
                        <h3>About the PERT </h3>
                        
                        
                        <p>When you first come to Valencia College, you may be required to take Florida's Postsecondary
                           Education Readiness Test (PERT). 
                        </p>
                        
                        <blockquote>
                           
                           <p>The PERT is an entry-level placement test required of all degree seeking students
                              in the Florida College System (FCS) according to Florida Statutes, Section 1008.30,
                              Rule 6A-10.0315. This test will determine your initial placement in English, reading
                              and mathematics courses. 
                           </p>
                           
                           <p>You could be exempt from this placement test if you meet any of the following:</p>
                           
                        </blockquote>
                        
                        <ul>
                           
                           <li>Started the 9th grade in 2003 or after and graduated from a Florida High School with
                              a standard diploma 
                           </li>
                           
                           <li>Are an active duty member of any branch of the United States Armed Services</li>
                           
                           <li>Provide official ACT, CPT, and/or SAT scores less than two years old that place you
                              in college-level course work at Valencia (See <a href="../Placement-Chart.html">placement chart</a>)
                           </li>
                           
                           <li>Provide official transcripts of college-level coursework in English or mathematics
                              with a "C" or better from a regionally accredited institution.<br>
                              
                           </li>
                           
                        </ul>       
                        
                        <p>PERT Quick Testing Facts:</p>        
                        
                        <ul>
                           
                           <li>The test consists of three subtests: <strong>Mathematics, Reading and Writing</strong>.
                           </li>
                           
                           <li>Each subsection of the PERT consists of <strong>30 questions</strong>
                              
                           </li>
                           
                           <li>The PERT is <strong>not timed</strong>, so you will be able to work through the test at your own pace. <em>(We recommend you set aside between two and a half to three hours to take the test,
                                 but you may take as long as you need)</em>
                              
                           </li>
                           
                           <li>Calculators are not permitted in the lab, however there will be a computer calculator
                              available on certain math questions of the test<em>. </em>
                              
                           </li>
                           
                           <li>You<strong> cannot fail </strong>the test. The test is designed to determine the best placement for you based on your
                              academic preparation
                              
                              <p>Review sample PERT questions. NOTE: Score is not calculated.</p>
                              
                              <ol>
                                 
                              </ol>
                              
                              <ul>
                                 
                                 <li><a href="https://college.measuredsuccess.com/mscollege/practiceTest2/math/question1.html">Math Sample Test</a></li>
                                 
                                 <li><a href="https://college.measuredsuccess.com/mscollege/practiceTest2/reading/question1.html">Reading Sample Test</a></li>
                                 
                                 <li><a href="https://college.measuredsuccess.com/mscollege/practiceTest2/writing_mc/question1.html">Writing Sample Test</a></li>
                                 
                              </ul>
                              
                              
                           </li>
                           
                        </ul>        
                        
                        <p>Number of PERT attempts accepted at Valencia:</p>
                        
                        <ul>
                           
                           <li>A student is allowed 3 PERT attempts at Valencia in  a two-year period with a one-day
                              wait period between attempts. &nbsp;To be valid, test scores must have  been taken within
                              two years. 
                           </li>
                           
                        </ul>
                        
                        <ul>
                           
                           <li>Valencia  will only accept valid PERT/CPT scores from previously <em>enrolled </em>institutions. Request a sealed copy of  scores from the outside institution and submit
                              scores to Assessment staff or <a href="http://net4.valenciacollege.edu/forms/assessments/contact.cfm" target="_blank">contact</a> us and request scores to be retrieved from the PERT  repository (include Full Name,
                              VID#, Date of Birth and where you took the  PERT). Please Note: PERT scores are not
                              available in the PERT Repository  immediately after testing day. 
                           </li>
                           
                        </ul>
                        
                        <p>You must wait <u>at least 24 hours</u> between any PERT attempts. Remediation is required prior to your second retest at
                           Valencia.
                        </p>       
                        
                        <h3>PERT Retest at Valencia College </h3>
                        
                        <p>All PERT retakes require a:</p>
                        
                        <ul>
                           
                           <li>1 day (24 hour) wait period between tests</li>
                           
                           <li>PERT Retake Review Session per subtest</li>
                           
                           <li>PERT retake fee ($10 per subject retake) </li>
                           
                        </ul>
                        
                        <p><strong>Steps:</strong><br>
                           <strong>1. </strong>Obtain your PERT scores and confirm retake eligibility with your Assessment Center<br>
                           <strong>2. </strong>Receive PERT Retake Certificate  from your Assessment Center<br>
                           <strong>3.</strong> Review the following link to see times, locations and process of a PERT Review Session:
                           <a href="../../learning-support/PERT/index.html" target="_blank">http://valenciacollege.edu/learning-support/PERT/</a><br>
                           <strong>4. </strong>Attend the PERT Retake Review Session(s) and get your certificate(s) signed<br>
                           <strong>5. </strong>Pay at your business office: <a href="../../business-office/payments.html" target="_blank">http://valenciacollege.edu/business-office/payments.cfm</a> <br>
                           <strong>6. </strong>Return to the Assessment Center with your certificate(s) and payment receipt to retake
                           the PERT<br>
                           
                        </p>
                        
                        <p>*You may be asked to purchase a booklet from Valencia <a href="../../students/locations-store/index.html" target="_blank">bookstore</a> or download and print the following booklet to review: 
                        </p>
                        
                        <p><a href="documents/Reading.pdf">READING REVIEW GUIDE (PERT Reading) </a></p>
                        
                        <p><a href="documents/Writing.pdf">WRITING/SENTENCE SKILLS REVIEW GUIDE (PERT Writing)</a></p>
                        
                        <p><a href="documents/PERTmathbookletUPDATE.pdf">MATH REVIEW GUIDE (PERT Math)</a> 
                        </p>
                        
                        <p><a href="documents/CPT-Ibooklet.pdf">COLLEGE LEVEL MATH REVIEW (CPT-I)</a> 
                        </p>
                        
                        <p>Bookstore Prices: Reading $1.56, Writing $2.05, Math $4.00</p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/post-secondary-readiness-test/taking-the-exam.pcf">©</a>
      </div>
   </body>
</html>