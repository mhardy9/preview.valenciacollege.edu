<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/post-secondary-readiness-test/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Pert</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Placement Testing (PERT/COMPASS ESL/CPT-I)</h2>
                        
                        <h3>At a Glance</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h2>Hours Available</h2>
                                    
                                    <p>Walk-in basis. No appointment needed. <br>
                                       Mon-Thurs: 8 AM - 5 PM<br>
                                       Fridays: 9 AM - 10:30 AM 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h2>Test Fees</h2>
                                    
                                    <p><br>
                                       No fee for initial test<br>
                                       Retakes: $10 per sub-test
                                    </p>
                                    
                                    <p><a href="../../business-office/payments.html">Valencia's Payment Method</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h2><strong>Retake Wait Period</strong></h2>
                                    
                                    <p><br>
                                       <strong>PERT</strong>- 1 day, 1 retake per 24 months(2 yrs) Tutoring required prior to retake.<a href="taking-the-exam.html">PERT Review Book</a></p>
                                    
                                    <p><strong>COMPASS ESL</strong>- 90 days, unlimited retakes. Suggested English as a Second Language (<a href="http://preview.valenciacollege.edu/continuing-education/programs/english-as-a-second-language/">ESL classes)</a> prior to retake. 
                                    </p>
                                    
                                    <p><strong>CPT I-</strong> 1 day, 1 retake per 24 months (2 yrs) Tutoring required prior to retake. <a href="documents/cpt-College-Math-Review.pdf">CPT-I Review Book</a> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h2>Time Required</h2>
                                    
                                    <p><strong>PERT</strong>- Untimed
                                    </p>
                                    
                                    <p><br>
                                       <strong>COMPASS ESL</strong>- 1 hour essay, reading and listening sections are untimed
                                    </p>
                                    
                                    <p><strong>CPT I</strong>- Untimed 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <p><strong><u>Student Instructions for Viewing Test Scores in Atlas</u></strong></p>
                        
                        
                        <ol>
                           
                           <li>Log into your Atlas account.</li>
                           
                           <li>Click on the <strong>Courses</strong> tab.
                           </li>
                           
                           <li>Inside the Registration channel, click on <strong>Registration</strong>.&nbsp; <br>
                              <br>
                              <strong><img alt="Click on the Registration Link" height="202" src="step3.jpg" width="300"></strong><br>
                              
                           </li>
                           
                           <li>Click on <strong>Transcripts, Grades &amp; Holds</strong>.<br>
                              <br>
                              <img alt="Click on the Transcripts, Grades, and Holds Link" height="244" src="step4.jpg" width="200"><br>
                              
                           </li>
                           
                           <li>Click on <strong>View Test Scores</strong>.<br>
                              <br>
                              <img alt="Click on the View Test Scores Llink" height="311" src="step5.jpg" width="200">  <br>         
                              
                           </li>
                           
                        </ol>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/post-secondary-readiness-test/index.pcf">©</a>
      </div>
   </body>
</html>