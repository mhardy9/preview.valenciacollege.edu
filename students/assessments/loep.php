<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Taking the LOEP  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments, loep, exam">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/loep.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Taking the LOEP </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Taking the ACCUPLACER LOEP</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              
                           </p>
                           
                           <h3>Hours Available</h3>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>All Assessment Centers Closed September 4. East Campus closed August 4.</strong><p></p>
                           Mon-Thurs: 8 AM - 5 PM<br>
                           Friday: 9 AM - 4 PM<br><br>
                           
                           
                           <h3>Retake Wait Period</h3>
                           
                           
                           <p>You may take the LOEP two times in one year with a 1-day wait period between tests.
                              Number of attempts is calculated from the anniversary of your first attempt.
                           </p>
                           
                           
                           <h3>Test Fees</h3>
                           
                           Must pay application fees<br>
                           No Charge 1st attempt, $10 Retake fee<br><br>
                           Valencia's Payment Method<br><br>
                           
                           
                           <h3>Time Required</h3>
                           Part 1- Typing an Essay- 30 minutes<br>
                           Part 2- Sentence Meaning- No Time Limit<br>
                           Part 3- Reading- No Time Limit<br>
                           Part 4- Language Use- No Time Limit<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> Technical difficulties inherent with the internet, software or transmission may occur
                           during your testing experience and are unpredictable. If unexpected technical difficulties
                           occur, a proctor will make every attempt to promptly resume your assessment. Extended
                           delays in testing may require rescheduling.
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>About the ACCUPLACER LOEP</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If your native language is not English and you are seeking an associate degree or
                              career certificate, you will be required to take the ACCUPLACER LOEP. The LOEP is
                              an assessment that will test language proficiency before you can register for any
                              courses.
                           </p>
                           
                           <ul>
                              
                              <li>The LOEP is designed to assess your English skills if you have learned English as
                                 an additional language to your native language.
                              </li>
                              
                              <li>Tests are administered on a walk-in basis at Valencia's Assessment Centers.</li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>ACCUPLACER LOEP Test Breakdown</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The ACCUPLACER LOEP is comprised of 4 parts: Typing an Essay, Sentence Meaning, Reading
                              and Language Usage.
                           </p>
                           
                           <p>All parts aregiven on a computer.</p>
                           
                           
                           <p><img src="/_resources/img/students/assessments/computer_000.PNG" width="95" height="104" alt="picture of a computer and keyboard"></p>
                           
                           
                           <p><strong>Part 1: Typing&nbsp;</strong><strong>an Essay for 30 minutes&nbsp;</strong>(300-600 words)&nbsp;<br>
                              <em>This will be the only section of the ACCUPLACER LOEP that has a time limit. You will
                                 have up to 30 minutes to complete a typed essay with 300-600 words.</em></p>
                           
                           <p><strong>Part 2: Sentence Meaning&nbsp;</strong>(20 questions-multiple choice)<br>
                              <em>Verbs, nouns, basic phrases</em></p>
                           
                           <p><strong>Part 3: Reading</strong>&nbsp;(20 questions-multiple choice)<strong>&nbsp;<br>
                                 </strong><em>Questions on reading material</em></p>
                           
                           <p><strong>Part 4: Language Use&nbsp;</strong>(20 questions-multiple choice)&nbsp;<br>
                              <em>Sentence structure, subject-verb agreement</em></p>
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Are there any exemptions from the ACCUPLACER LOEP?</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You may be exempt from the ACCUPLACER LOEP if you:</p>
                           
                           <ul>
                              
                              <li>Provide official Test of English as a Foreign Language (TOEFL) or IELTS scores less
                                 than two years old that place you in course work at Valencia (See&nbsp;<a href="https://valenciacollege.edu/assessments/Placement-Chart.cfm">Placement chart</a>).
                              </li>
                              
                              <li>Provide official transcripts of college-level coursework in Freshman English Composition
                                 with a "C" or better.
                              </li>
                              
                              <li>Provide official transcripts indicating you have earned an Associate or Bachelor's
                                 degree for which English was the language of instruction.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Retaking the ACCUPLACER LOEP</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you would like to retake ACCUPLACER LOEP to possibly increase your score:</p>
                           
                           <ul>
                              
                              <li>You may be eligible to retake the test 1 day after the original test date (this option
                                 is available once per calendar year after initial test date)
                              </li>
                              
                              <li>You must pay a $10 retake fee before retesting</li>
                              
                              <li>All application fees must be paid prior to retesting</li>
                              
                              <li>Retakes are not allowed once you begin English for Academic Purpose (EAP) course work</li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>International Students</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">If you are an International student, your scores must place you at EAP Level 4 in
                           order to attend Valencia.
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Placement Charts</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>ACCUPLACER LOEP Placement Chart</h3>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%" bgcolor="#92D050">
                                       <h4 align="center">ACCUPLACER LOEP AVERAGE SCORE</h4>
                                    </td>
                                    
                                    <td width="14%" bgcolor="#92D050">
                                       <h4 align="center">LEVEL PLACEMENT</h4>
                                    </td>
                                    
                                    <td width="66%" bgcolor="#92D050">
                                       <h4 align="center">ELIGIBLE COURSES</h4>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">65 or less</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 0</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">Admission delayed; referral to Adult Education ESOL</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">66-75</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 2</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP0281</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">76-85</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 3</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP0300C, EAP0320C, EAP0340C, EAP0360C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">86-95</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 4</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP0400C, EAP0420C, EAP0440C, EAP0460C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">96-105</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 5</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">106-115</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 6</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP1620C, EAP1640C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">116 or more</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 7</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">(Use PERT scores for placement)</p>
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>Note:<br>
                              Immigrants, refugees and U.S. citizens scoring 65 or less will have their Admission
                              delayed.
                           </p>
                           
                           <p>Non-Immigrants&nbsp;<strong>(International students)</strong>&nbsp;scoring 0-85 (levels 0, 2, 3) will not be eligible for Admission.
                           </p>
                           
                           <h3>COMPASS ESL Placement Chart</h3>
                           (This chart is available to assist with placement for those who have&nbsp;<u>previously</u>&nbsp;tested in COMPASS ESL and have current scores. The COMPASS ESL is no longer offered
                           at Valencia)<br><br>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%" bgcolor="#92D050">
                                       <h4 align="center">COMPASS ESL AVERAGE SCORE</h4>
                                    </td>
                                    
                                    <td width="14%" bgcolor="#92D050">
                                       <h4 align="center">LEVEL PLACEMENT</h4>
                                    </td>
                                    
                                    <td width="66%" bgcolor="#92D050">
                                       <h4 align="center">ELIGIBLE COURSES</h4>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">12.4 or less</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 0</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">Admission delayed; referral to Adult Education ESOL</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">12.5-29.9</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 2</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP0281</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">30-39.9</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 3</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP0300C, EAP0320C, EAP0340C, EAP0360C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">40-49.9</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 4</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP0400C, EAP0420C, EAP0440C, EAP0460C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">50-69.9</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 5</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">70-97.4</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 6</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">EAP1620C, EAP1640C</p>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr valign="top">
                                    
                                    <td width="20%">
                                       <p align="center">97.5 or more</p>
                                    </td>
                                    
                                    <td width="14%">
                                       <p align="center">Level 7</p>
                                    </td>
                                    
                                    <td width="66%">
                                       <p align="center">(Use PERT scores for placement)</p>
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br><br>
                           <strong>Note:</strong> Immigrants, refugees and U.S. citizens scoring 12.4 or less will have their Admission
                           delayed. Non-Immigrants&nbsp;<strong>(International students)</strong>&nbsp;scoring 0-39.9 (levels 0, 2, 3) will not be eligible for Admission.
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/loep.pcf">©</a>
      </div>
   </body>
</html>