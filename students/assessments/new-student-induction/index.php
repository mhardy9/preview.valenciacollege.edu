<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/new-student-induction/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>New Student Induction</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h3>New Student Induction Process</h3>
                        
                        <p><strong> Take a look at our <a href="documents/6.18.15-enrollment-checklist-revise-final.pdf" target="_blank">Enrollment Checklist</a></strong></p>
                        
                        <p><img alt="Step 1 apply for admission and financial aid, step 2 create atlas account, step 3 complete part 1 orientation (you may be required to provide placement scores) and part 2 on campus orientation (register for classes at on-campus orientation." height="163" src="Part1and2orientationchart_001.PNG" width="713"></p>
                        
                        <p><img alt="Step 4 pay tuition, step 5 Get ID Card and Parking Decal, Step 6 Purchase Books and got to class!" height="155" src="Step45and6.PNG" width="485"></p>
                        
                        <p>You may be required to submit placement scores such as PERT, SAT or ACT if you do
                           <u>not</u> meet the following criteria:
                        </p>
                        
                        <ul>
                           
                           <li>You are an Active Military Member.</li>
                           
                           <li>You started 9th grade in a Florida Public high school or charter school in 2003 or
                              after <strong><u>AND</u></strong> did or will graduate with a <u>standard high school diploma</u> from a Florida Public High School or Charter School (GED does not qualify).
                           </li>
                           
                           <li>You are transferring college level coursework in English and Math with a grade of
                              C or higher. 
                           </li>
                           
                        </ul>
                        
                        <p>If you said “no” to any of the options above, you ARE <u>required</u> to provide placement scores. 
                        </p>
                        
                        <p><em><span><strong>Dual Enrollment and Bridges candidates may be required to take the PERT for program
                                    eligibility. </strong></span></em></p>
                        
                        <ul>
                           
                           <li>If you have ACT or SAT scores that are less than two years old and they are high enough
                              to use for placement, send them to Valencia. 
                           </li>
                           
                        </ul>
                        
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Click <a href="../placement-chart.html">here</a> to view the scores needed for SAT or ACT placement
                        </p>
                        
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Click <a href="../requesting-scores.html">here</a> to find out how to send your SAT/ACT scores.&nbsp; 
                        </p>
                        
                        <p>Please note, it takes 5 weeks to receive and process SAT and ACT scores. </p>
                        
                        <ul>
                           
                           <li>If you do not wish to wait for your official ACT or SAT scores to arrive, you may
                              take the <a href="../pert/taking-the-exam.html" target="_blank">PERT</a>. The PERT is free of charge and can be taken at any campus <a href="../index.html">Assessment Services</a> office (no appointment necessary). 
                           </li>
                           
                        </ul>        
                        
                        <p><strong>**Be sure to thoroughly read and complete every step in your Part 1: Online Orientation
                              course. </strong></p>
                        
                        <p><strong>** Failure to submit ALL answers to your reviews will result in a delay in registering
                              for Part 2: On-Campus Orientation.</strong></p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/new-student-induction/index.pcf">©</a>
      </div>
   </body>
</html>