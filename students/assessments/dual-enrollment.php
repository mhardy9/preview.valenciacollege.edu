<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dual Enrollment  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments, dual, enrollment">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/dual-enrollment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Dual Enrollment </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        
                        <p>
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Future Dual Enrollment Students</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>It is the student's responsibility to submit their scores in the appropriate time
                              needed for Valencia to receive and process. Do not wait until the last minute to submit
                              your scores. Refer to&nbsp;<a href="https://valenciacollege.edu/dual/" target="_blank">Dual Enrollment</a>&nbsp;for deadline dates. SAT and ACT scores take&nbsp;<strong>5 weeks&nbsp;</strong>to receive and process.
                           </p>
                           
                           <p>Minimum college-level scores required for eligibility into the Dual Enrollment Program:</p>
                           
                           <p><strong>PERT</strong>: Reading&nbsp;<strong>106</strong>/ Writing&nbsp;<strong>103</strong>/ Math&nbsp;<strong>114</strong></p>
                           
                           <p><strong>SAT(prior to 3/1/16):</strong>&nbsp;Critical Reading&nbsp;<strong>440</strong>/ Math&nbsp;<strong>440</strong>&nbsp;*SAT Writing score is not used for English placement.
                           </p>
                           
                           <p><strong>SAT (on or after 3/1/16):</strong>&nbsp;Reading Test Score&nbsp;<strong>24</strong>/ Writing and Language Test Score&nbsp;<strong>25</strong>/ Math Test Score&nbsp;<strong>24</strong></p>
                           
                           <p><strong>ACT:</strong>&nbsp;Reading&nbsp;<strong>19</strong>/ English&nbsp;<strong>17</strong>/ Math&nbsp;<strong>19</strong></p>
                           
                        </div>         
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>PERT Testing</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You must have the following when coming to the Assessment Center:<br>
                              <strong>Valencia Identification Number (VID)</strong>- This is found in your Dual Enrollment email (V0_______).
                           </p>
                           
                           <p><strong>Identification</strong>- Acceptable forms of identification include:
                           </p>
                           
                           <ul>
                              
                              <li>A current driver’s license</li>
                              
                              <li>A current Valencia College student ID</li>
                              
                              <li>A current state or federal ID card (such as a Green Card)</li>
                              
                              <li>A current passport</li>
                              
                              <li>A tribal ID card</li>
                              
                              <li>A naturalization card or certificate of citizenship</li>
                              
                           </ul>
                           
                           <p>* If you do not have a government issued photo ID, Dual Enrollment students may bring
                              their&nbsp;<u>original</u>&nbsp;Birth Certificate&nbsp;<strong>OR</strong>&nbsp;Social Security Card&nbsp;<strong>AND</strong>&nbsp;their high school ID. NO copies of Social Security Card or Birth Certificate will
                              be accepted.
                           </p>
                           
                           <ul>
                              
                              <li>Testing is done on a walk-in basis. No appointment needed.</li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>*PERT takes 2.5 - 3 hours. Testing must be completed in the same day, unless a seven-day
                                 extension is needed. Please allow yourself at least 2 1/2 to 3 hours for all 3 sections
                                 of the PERT.
                              </li>
                              
                           </ul>
                           
                           <p><strong>NOTE</strong>:&nbsp;<em>Technical difficulties inherent with the internet, software or transmission may occur
                                 during your testing experience and are unpredictable. If unexpected technical difficulties
                                 occur, Proctors will make every attempt to promptly resume your assessment. Extended
                                 delays in testing may require rescheduling. Do not wait last minute to complete the
                                 PERT.</em></p>
                           
                           <ul>
                              
                              <li>A student is allowed 3 PERT attempts at Valencia in a two-year period with a one-day
                                 wait period between attempts. &nbsp;To be valid, test scores must have been taken within
                                 two years.
                              </li>
                              
                           </ul>
                           
                           <p><a href="https://valenciacollege.edu/assessments/pert/taking-the-exam.cfm"><strong>PERT Testing Times</strong></a></p>
                           
                           <ul>
                              
                              <li>Valencia will only accept valid PERT scores from a student’s high school, no other
                                 institution.
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>PERT scores are immediately input into your Valencia file after testing in Valencia's
                                 Assessment Center.
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>Your initial PERT test at Valencia is free&nbsp;<em>if</em>&nbsp;you have applied to Valencia’s Dual Enrollment Program and have received your Valencia
                                 ID number.
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>You must wait at least 24 hours between any PERT attempts. Remediation is required
                                 prior to your second attempt at Valencia.
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>
                                 <strong>PERT at High School Campus:</strong>&nbsp;Your Counselor should indicate on your application any college-level PERT scores
                                 received in High School. Those scores will be retrieved by Assessment and reviewed
                                 by Dual Enrollment. If you take the PERT again&nbsp;<u>after</u>&nbsp;you applied to Dual Enrollment, you must communicate to the Assessment office through
                                 the following link:
                              </li>
                              
                           </ul>
                           
                           <p><a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">SUBMIT HIGH SCHOOL PERT SCORES</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>PERT Retest at Valencia College</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Eligibility for PERT retakes:</p>
                           
                           <ul>
                              
                              <li>A student is allowed a total of 3 PERT attempts at Valencia in a two-year period.</li>
                              
                              <li>There is a one-day wait period between attempts.</li>
                              
                              <li>Student must not have begun prep course sequence prior to retest.</li>
                              
                           </ul>
                           
                           <p>Steps for completing PERT retakes (Students are required to complete a PERT review
                              session prior to their&nbsp;<u>second</u>&nbsp;attempt):
                           </p>
                           
                           <ul>
                              
                              <li>Obtain retake certificate from an Assessment Proctor.&nbsp;</li>
                              
                              <li>Attend a required on-campus PERT review session in the Support Center. Review the
                                 following link to see times, locations and process of a PERT Review Session:&nbsp;<a href="http://valenciacollege.edu/learning-support/PERT/" target="_blank">http://valenciacollege.edu/learning-support/PERT/</a>
                                 
                              </li>
                              
                              <li>Have Support Center staff sign retake certificate.</li>
                              
                              <li>Take certificate to the Business Office and pay retake.&nbsp;<a href="http://valenciacollege.edu/businessoffice/payments.cfm" target="_blank">http://valenciacollege.edu/businessoffice/payments.cfm</a>
                                 
                              </li>
                              
                              <li>Return to Assessment Center with&nbsp;<u>signed certificate</u>&nbsp;and&nbsp;<u>proof of payment</u>.
                              </li>
                              
                           </ul>
                           
                           <p>* You may be asked to purchase a booklet from Valencia&nbsp;<a href="https://valenciacollege.edu/locations-store/" target="_blank">bookstore</a>&nbsp;or download and print the following booklet to review:
                           </p>
                           
                           <ul class="list_4">
                              
                              <li><a href="https://valenciacollege.edu/assessments/documents/Reading.pdf">READING REVIEW GUIDE (PERT Reading)</a></li>
                              
                              <li><a href="https://valenciacollege.edu/assessments/documents/Writing.pdf">WRITING/SENTENCE SKILLS REVIEW GUIDE (PERT Writing)</a></li>
                              
                              <li><a href="https://valenciacollege.edu/assessments/pert/documents/PERTmathbookletUPDATE.pdf">MATH REVIEW GUIDE (PERT Math)</a></li>
                              
                              <li><a href="https://valenciacollege.edu/assessments/pert/documents/CPT-Ibooklet.pdf">COLLEGE LEVEL MATH REVIEW (CPT-I)</a></li>
                              
                           </ul>
                           
                           <p>Bookstore Prices: Reading $1.56, Writing $2.05, Math $4.00</p>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>Please speak to an Assessment Center Specialist to make sure you qualify for a retake.</strong>
                           
                        </div>                               
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h2>Submit SAT or ACT scores</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>*Due to the new SAT changes, CollegeBoard will not be sending 2016 March or April
                                 SAT scores until Mid-May and may not be received by the Dual Enrollment Deadline date.
                                 Consider taking the PERT instead.</strong></p>
                           
                           <p>If you have taken the SAT or ACT in the past two years and they are at&nbsp;<a href="https://valenciacollege.edu/assessments/placement-chart.cfm" target="_blank"><u>college level</u></a>, please have your scores sent to us electronically or bring us the original score
                              report.&nbsp;No printed, copied or scanned score reports will be accepted.
                           </p>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> Please note: Sending SAT and ACT scores electronically will take 5 weeks to receive
                           and process. This includes requests made for rush delivery.
                           <p></p>
                           <i class="far fa-info-circle" aria-hidden="true"></i> It is the student's responsibility to submit their scores in the appropriate time
                           needed for Valencia to receive and process. Do not wait until the last minute to submit
                           your scores.
                           
                           
                           
                           <p>ACT scores can be sent electronically from your ACT account:&nbsp;<a href="http://www.actstudent.org/scores/send/" target="_blank">http://www.actstudent.org/scores/send/</a></p>
                           
                           <p>SAT scores can be sent electronically from your CollegeBoard Account:&nbsp;<a href="http://sat.collegeboard.org/scores/" target="_blank">http://sat.collegeboard.org/scores/</a></p>
                           
                        </div>  
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/dual-enrollment.pcf">©</a>
      </div>
   </body>
</html>