<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Placement Charts  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/placement-charts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Placement Charts </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Placement Charts</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p align="left"><strong>If you are a prospective Dual Enrollment student -- Please click&nbsp;<a href="https://valenciacollege.edu/dual/">here</a>&nbsp;for updated test score information.</strong></p>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td colspan="7" style="color:white; background-color: #bf311a; font-weight: bold">Reading Placement Scores</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="26%"><strong>Placement</strong></td>
                                    
                                    <td width="14%">
                                       <div align="center"><strong>PERT</strong></div>
                                    </td>
                                    
                                    <td width="14%">
                                       <div align="center">
                                          
                                          <p><strong>Accuplacer CPT</strong></p>
                                          
                                       </div>
                                    </td>
                                    
                                    <td width="13%">
                                       <div align="center">
                                          
                                          <p><strong>SAT</strong></p>
                                          
                                          <p><strong>Taken&nbsp;<em>prior</em>&nbsp;to March 1, 2016</strong></p>
                                          
                                       </div>
                                    </td>
                                    
                                    <td width="10%">
                                       
                                       <p align="center"><strong>Redesigned SAT Reading Test Score</strong></p>
                                       
                                       <p align="center"><strong>Taken on or&nbsp;<em>after</em>&nbsp;March 1, 2016</strong></p>
                                       
                                    </td>
                                    
                                    <td width="10%">
                                       <div align="center"><strong>ACT</strong></div>
                                    </td>
                                    
                                    <td width="13%">
                                       <div align="center"><strong>FCAT 2.0</strong></div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Dev. Reading 1<br>
                                       ENC 0017
                                    </td>
                                    
                                    <td>
                                       <div align="center">50-83*</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">&amp;lt; 59</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Dev. Reading 2<br>
                                       REA 0017
                                    </td>
                                    
                                    <td>
                                       <div align="center">84-105</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">60-82</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Freshman Composition I **<br>
                                       ENC1101
                                    </td>
                                    
                                    <td>
                                       <div align="center">106-150</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">83 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">440 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">24 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">19 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">262 &amp;gt;</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="76" colspan="7">
                                       
                                       <p>*If you also score 90-102 on PERT Writing, you place into ENC 0027. Please refer to
                                          catalog for more information.
                                       </p>
                                       
                                       <p><a href="http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/">http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/</a></p>
                                       
                                       <p>**Enrollment in ENC 1101 Freshman Composition I (the first college-level English course)
                                          requires college-level placement scores in both English and Reading for PERT, CPT
                                          and ACT or for students to be non-mandated into developmental education under Senate
                                          Bill 1720. The SAT and FCAT 2.0 only requires a college-level Reading Score.
                                       </p>
                                       
                                       <p>Effective immediately, Valencia will also use the Redesigned SAT Reading Test score
                                          (taken&nbsp;<em>after</em>&nbsp;March 1, 2016) for placement until approval of new scores are in State Board Rule
                                          6A-10.0315.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td colspan="7" style="color:white; background-color: #bf311a; font-weight: bold">Writing Placement Scores</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="23%"><strong>Placement</strong></td>
                                    
                                    <td width="10%">
                                       <div align="center"><strong>PERT</strong></div>
                                    </td>
                                    
                                    <td width="12%">
                                       <div align="center"><strong>Accuplacer CPT</strong></div>
                                    </td>
                                    
                                    <td width="20%">
                                       <div align="center">
                                          
                                          <p><strong>SAT</strong></p>
                                          
                                          <p><strong>Taken&nbsp;<em>prior</em>&nbsp;to March 1, 2016</strong></p>
                                          
                                       </div>
                                    </td>
                                    
                                    <td width="11%">
                                       
                                       <p align="center"><strong>Redesigned SAT Writing Test Score</strong></p>
                                       
                                       <p align="center"><strong>Taken on or&nbsp;<em>after</em>&nbsp;March 1, 2016</strong></p>
                                       
                                    </td>
                                    
                                    <td width="10%">
                                       <div align="center"><strong>ACT English</strong></div>
                                    </td>
                                    
                                    <td width="14%">
                                       <div align="center"><strong>FCAT 2.0 Reading</strong></div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Dev. Writing 1<br>
                                       ENC 0017
                                    </td>
                                    
                                    <td>
                                       <div align="center">50-89*</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">&amp;lt; 53</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Dev. Writing 2<br>
                                       ENC 0025
                                    </td>
                                    
                                    <td>
                                       <div align="center">90-102*</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">54-82</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Freshman Composition I **&nbsp;<br>
                                       ENC1101
                                    </td>
                                    
                                    <td>
                                       <div align="center">103-150</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">83 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Refer to SAT reading score</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">25 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">17 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Refer to FCAT 2.0 Reading score</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="76" colspan="7">
                                       
                                       <p>*If you also score 84-105 on PERT Reading, you place into ENC 0027. Please refer to
                                          catalog for more information
                                       </p>
                                       
                                       <p><a href="http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/">http://catalog.valenciacollege.edu/entrytestingplacementmandatorycourses/testingplacementcharts/</a></p>
                                       
                                       <p>**Enrollment in ENC 1101 Freshman Composition I (the first college-level English course)
                                          requires college-level placement scores in both English and Reading for PERT, CPT
                                          and ACT or for students to be non-mandated into developmental education under Senate
                                          Bill 1720. The SAT and FCAT 2.0 only requires a college-level Reading Score.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td colspan="6" style="color:white; background-color: #bf311a; font-weight: bold">Math Placement Scores</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="28%"><strong>Placement</strong></td>
                                    
                                    <td width="14%">
                                       <div align="center"><strong>PERT</strong></div>
                                    </td>
                                    
                                    <td width="13%">
                                       <div align="center"><strong>Accuplacer CPT</strong></div>
                                    </td>
                                    
                                    <td width="14%">
                                       <div align="center">
                                          
                                          <p><strong>SAT</strong></p>
                                          
                                          <p><strong>Taken&nbsp;<em>prior</em>&nbsp;to <br>March 1, 2016</strong></p>
                                          
                                       </div>
                                    </td>
                                    
                                    <td width="14%">
                                       
                                       <p align="center"><strong>Redesigned SAT</strong></p>
                                       
                                       <p align="center"><strong>Taken on or&nbsp;<em>after</em>&nbsp;<br>March 1, 2016</strong></p>
                                       
                                    </td>
                                    
                                    <td width="14%">
                                       <div align="center"><strong>ACT</strong></div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Dev. Mathematics I&nbsp;<br>
                                       MAT 0018C or MAT 0022C
                                    </td>
                                    
                                    <td>
                                       <div align="center">50-95</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">&amp;lt; 41</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>Dev. Mathematics II<br>
                                       MAT 0022C - MAT 0028C
                                    </td>
                                    
                                    <td>
                                       <div align="center">96-113</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">42-71</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">N/A</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <p>Statistics for Undergraduates -STA1001C<br>
                                          <br>
                                          College Math - MGF 1106
                                       </p>
                                    </td>
                                    
                                    <td>
                                       <div align="center">96 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">42 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">440 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">24 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">19 or more</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <p>Intermediate Algebra - MAT 1033C<br>
                                          
                                       </p>
                                    </td>
                                    
                                    <td>
                                       <div align="center">114-122</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">72-89</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">440-499</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">24-26</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">19 or 20</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       
                                       <p>College Level Math*</p>
                                       
                                       <p>MAC 1105 or STA 2023</p>
                                       
                                    </td>
                                    
                                    <td>
                                       <div align="center">123-150</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">90 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">500 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">26.5 or &amp;gt;</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">
                                          
                                          <p>21 or &amp;gt;</p>
                                          
                                       </div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="61" colspan="6">
                                       
                                       <p align="left">*Take CPT-I testing for possible higher placement (optional)</p>
                                       
                                       <p align="left">Effective immediately, Valencia will also use the Redesigned SAT Math Test score (taken&nbsp;<em>after</em>&nbsp;March 1, 2016) for placement until approval of new scores are in State Board Rule
                                          6A-10.0315.
                                       </p>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td colspan="5" style="color:white; background-color: #bf311a; font-weight: bold">CPTI Placement</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>64 or less</td>
                                    
                                    <td>
                                       <div align="left">MAC 1105: College Algebra<br>
                                          
                                       </div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="125">65 or more</td>
                                    
                                    <td>
                                       <div align="left">
                                          
                                          <p>MAC 1114: College Trigonometry<br>
                                             MAC 1140: Pre-Calculus Algebra<br>
                                             MAC 2233: Calculus for Business and Social Science<br>
                                             MAE 2801: Elementary School Math<br>
                                             MHF 2300: Logic and Proof in Math
                                          </p>
                                          
                                       </div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>89 or more</td>
                                    
                                    <td>
                                       <div align="left">MAC 2311: Calculus with Analytical Geometry I</div>
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td colspan="5" style="color:white; background-color: #bf311a; font-weight: bold">English for Academic Purposes (EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="22%"><strong>Assessment</strong></td>
                                    
                                    <td width="15%">
                                       <div align="center"><strong>Score</strong></div>
                                    </td>
                                    
                                    <td width="14%">
                                       <div align="center"><strong>Level Placement</strong></div>
                                    </td>
                                    
                                    <td colspan="2">
                                       
                                       <div align="center"><strong>Course</strong></div>
                                       
                                       <div align="center"></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="7">Compass ESL</td>
                                    
                                    <td>
                                       <div align="left">12.4 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 0</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">Admission delayed; referral to Adult Education ESOL</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">12.5 - 29.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 2</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP 0281</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">30.0 - 39.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 3</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP0300C, EAP0320C, EAP0340C, EAP0360C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">40.0 - 49.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 4</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP0400C, EAP0420C, EAP0440C, EAP0460C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">50.0 - 69.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 5</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">70.0 - 97.4</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 6</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP1620C, EAP1640C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">97.5 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 7</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="7">LOEP</td>
                                    
                                    <td>
                                       <div align="left">65 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 0</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">Admission delayed; referral to Adult Education ESOL</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">66 - 75</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 2</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP 0281</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">76 - 85</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 3</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP0300C, EAP0320C, EAP0340C, EAP0360C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">86 - 95</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 4</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP0400C, EAP0420C, EAP0440C, EAP0460C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">96 - 105</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 5</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">106 - 115</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 6</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">EAP1620C, EAP1640C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="left">116 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="left">Level 7</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="left">Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="6">TOEFL</td>
                                    
                                    <td>
                                       <div align="center"><strong>Internet Based</strong></div>
                                    </td>
                                    
                                    <td>
                                       <div align="center"><strong>Computer Version</strong></div>
                                    </td>
                                    
                                    <td width="18%">
                                       <div align="center"><strong>Paper Version</strong></div>
                                    </td>
                                    
                                    <td width="31%" colspan="-1">&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">44 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">132 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">449 or less</div>
                                    </td>
                                    
                                    <td colspan="-1">
                                       <div align="center">Take Compass ESL for placement</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">45 - 53</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">133 - 156</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">450 - 479</div>
                                    </td>
                                    
                                    <td colspan="-1">
                                       <div align="center">EAP0400C, EAP0420C, EAP0440C, EAP0460C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">54 - 63</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">157 - 179</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">480 - 509</div>
                                    </td>
                                    
                                    <td colspan="-1">
                                       <div align="center">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">64 - 70</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">180 - 196</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">510 - 529</div>
                                    </td>
                                    
                                    <td colspan="-1">
                                       <div align="center">EAP1620C, EAP1640C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">71 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">197 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">530 or more</div>
                                    </td>
                                    
                                    <td colspan="-1">
                                       <div align="center">Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           <br>
                           
                           <table class="table ">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td colspan="5" style="color:white; background-color: #bf311a; font-weight: bold">Non-Immigrants (International Students)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td colspan="5">Non-Immigrants (international students) who do not speak English as a native language
                                       must submit a satisfactory score on the Compass ESL test, the LOEP and Essay&nbsp;<em>or</em>&nbsp;TOEFL.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td width="22%"><strong>Assessment</strong></td>
                                    
                                    <td width="15%">
                                       <div align="center"><strong>Score</strong></div>
                                    </td>
                                    
                                    <td width="15%">
                                       <div align="center"><strong>Level Placement</strong></div>
                                    </td>
                                    
                                    <td colspan="2">
                                       
                                       <div align="center"><strong>Course</strong></div>
                                       
                                       <div align="center"></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="5">Compass ESL
                                       
                                       <p>&nbsp;</p>
                                       
                                       <p><br>
                                          
                                       </p>
                                       
                                    </td>
                                    
                                    <td>
                                       <div align="center">0 - 39.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Levels 0, 2, 3</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       
                                       <div align="center"></div>
                                       Not eligible for admission to Valencia
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">40.0 - 49.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 4</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       
                                       <div align="center"></div>
                                       EAP0400C, EAP0420C, EAP0440C, EAP0460C
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">50.0 - 69.9</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 5</div>
                                    </td>
                                    
                                    <td colspan="2">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="32">
                                       <div align="center">70.0 - 97.4</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 6</div>
                                    </td>
                                    
                                    <td colspan="2">EAP1620C, EAP1640C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td height="32">
                                       <div align="center">97.5 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 7</div>
                                    </td>
                                    
                                    <td colspan="2">Use PERT, ACT, or SAT for English and reading placements (not EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="5">
                                       <p>LOEP</p>
                                    </td>
                                    
                                    <td>
                                       <div align="center">65 - 85</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Levels 0, 2, 3</div>
                                    </td>
                                    
                                    <td colspan="2">Not eligible for admission to Valencia</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">86 - 95</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 4</div>
                                    </td>
                                    
                                    <td colspan="2">EAP0400C, EAP0420C, EAP0440C, EAP0460C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">96 - 105</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 5</div>
                                    </td>
                                    
                                    <td colspan="2">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">106 - 115</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 6</div>
                                    </td>
                                    
                                    <td colspan="2">EAP1620C, EAP1640C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">116 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Level 7</div>
                                    </td>
                                    
                                    <td colspan="2">Use PERT, ACT, or SAT for English and reading placements (not EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="6">TOEFL</td>
                                    
                                    <td>
                                       <div align="center"><strong>Internet Based</strong></div>
                                    </td>
                                    
                                    <td>
                                       <div align="center"><strong>Computer Version</strong></div>
                                    </td>
                                    
                                    <td width="17%">
                                       <div align="center"><strong>Paper Version</strong></div>
                                    </td>
                                    
                                    <td width="31%">
                                       <div align="center"><strong>Course</strong></div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">44 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">132 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">449 or less</div>
                                    </td>
                                    
                                    <td>Not eligible for admission to Valencia</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">45 - 53</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">133 - 156</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">450 - 479</div>
                                    </td>
                                    
                                    <td>EAP0400C, EAP0420C, EAP0440C, EAP0460C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">54 - 63</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">157 - 179</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">480 - 509</div>
                                    </td>
                                    
                                    <td>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">64 - 70</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">180 - 196</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">510 - 529</div>
                                    </td>
                                    
                                    <td>EAP1620C, EAP1640C</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">71 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">197 or more</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">530 or more</div>
                                    </td>
                                    
                                    <td>Use PERT, ACT, or SAT for English and reading placements (not EAP)</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td rowspan="6">IELTS</td>
                                    
                                    <td height="38">
                                       <div align="center"><strong>IELTS Score</strong></div>
                                    </td>
                                    
                                    <td>
                                       <div align="center"><strong>Level</strong></div>
                                    </td>
                                    
                                    <td colspan="2">
                                       
                                       <div align="center"></div>
                                       
                                       <div align="center"><strong>Course</strong></div>
                                       
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">5.0 or less</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">Take Compass for placement</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="center">Not eligible for admission to Valencia</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">5.5</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">4</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="center">EAP0400C, EAP0420C, EAP0440C, EAP0460C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">6.0</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">5</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="center">EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">6.5</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">6</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="center">EAP1620C, EAP1640C</div>
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <div align="center">7.0 or above</div>
                                    </td>
                                    
                                    <td>
                                       <div align="center">7</div>
                                    </td>
                                    
                                    <td colspan="2">
                                       <div align="center">Use PERT, ACT, or SAT for English and reading placements (not EAP)</div>
                                    </td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           <p>&nbsp;</p>
                           
                        </div>         
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/placement-charts.pcf">©</a>
      </div>
   </body>
</html>