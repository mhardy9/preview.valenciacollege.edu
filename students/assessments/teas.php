<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Test of Essential Academic Skills  | Valencia College</title>
      <meta name="Description" content="Test of Essential Academic Skills (TEAS)">
      <meta name="Keywords" content="college, school, educational, assessments, teas, exam">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/teas.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Test of Essential Academic Skills </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>At a Glance</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              
                           </p>
                           
                           <h3>Hours Available</h3>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>All Assessment Centers Closed September 4.</strong>
                           
                           <p><strong>Mon-Thurs</strong>: 8 AM -2 PM&nbsp;<br>
                              <strong>Fridays</strong>: 9 AM - 1 PM
                           </p>
                           
                           <p style="font-weight: bold">Note:&nbsp;Offered on East, West, and Osceola Campus between the above hours. Also available
                              at the Lake Nona and Winter Park Campuses by appointment only.
                           </p>
                           
                           
                           <h3>Retake Wait Period</h3>
                           
                           
                           <p>30 days (May take on the 31st day)&nbsp;<br>
                              Maximum of 3 times in 12 months; this includes tests taken at other institutions.
                              Number of attempts is calculated from the anniversary of your first attempt.
                           </p>
                           
                           
                           <h3>Test Fees</h3>
                           
                           <p>Valencia Student- $65 per test<br>
                              Non-Valencia Student- $85
                           </p>
                           
                           
                           <p><a href="/students/office-services/business-office/payments.cfm">Valencia's Payment Method</a></p>
                           
                           <p>*Prices subject to change</p>
                           
                           
                           
                           <h3>Time Required</h3>
                           209 minutes<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>TEAS V scores will not be accepted by Valencia's Health Sciences after September 15,
                              2017.</strong><br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> **Testing for other institutions is allowed. However, any test scores that do not
                           meet Valencia’s testing policy (above) will NOT be valid for Valencia Health Science
                           admission. Students are advised to get approval from their institutions before testing
                           at Valencia.<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> Technical difficulties inherent with the internet, software or transmission may occur
                           during your testing experience and are unpredictable. If unexpected technical difficulties
                           occur, a proctor will make every attempt to promptly resume your assessment. Extended
                           delays in testing may require rescheduling.<br><br>
                           
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>About the TEAS</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><i class="far fa-info-circle" aria-hidden="true"></i> If you plan to test <u>on or after</u>&nbsp;August 31, 2016, please be sure to purchase the new 6th edition study guide&nbsp;<a href="https://www.atitesting.com/ati_store/product.aspx?zpid=1483" target="_blank">here</a>.
                           </p>
                           
                           <p>For information regarding the<strong>&nbsp;new ATI TEAS</strong>, please review the content changes&nbsp;<a href="http://atiteas.info/wp-content/uploads/2015/09/TEAS-V-Content-Comparison-Table.pdf" target="_blank">here</a>.
                           </p>
                           
                           <p>Valencia Health Sciences information and website:&nbsp;<a href="/academics/west/health/admissionupdates.html">https://www.valenciacollege.edu/academics/west/health/admissionupdates.html</a></p>
                           
                           <p>The Test of Essential Academic Skills (TEAS) is required to enter most Health Sciences
                              programs.
                           </p>
                           
                           <ul>
                              
                              <li>Valencia College offers TEAS V (version five) for entrance into the nursing and other
                                 Allied Health Programs.&nbsp;After August 30, 2016 the new ATI TEAS will be proctored.
                              </li>
                              
                           </ul>
                           
                           <p>The ATI TEAS is a computerized test consisting of 170 multiple choice questions with
                              a time limit of 209 minutes for the test.
                           </p>
                           
                           <ul>
                              
                              <li>Reading contains 53 questions and is timed at 64 minutes</li>
                              
                              <li>Mathematics contains 36 questions and is timed at 54 minutes</li>
                              
                              <li>Science contains 53 questions and is timed at 63 minutes</li>
                              
                              <li>English and Language Usage contains 28 questions and is timed for 28 minute.</li>
                              
                           </ul>
                           
                           <p>The ATI TEAS starting after August 30, 2016 will have new content. Click&nbsp;<a href="http://atiteas.info/wp-content/uploads/2015/09/TEAS-V-Content-Comparison-Table.pdf" target="_blank">here</a>&nbsp;for those changes.
                           </p>
                           
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>TEAS Transcripts</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>
                              <i class="far fa-info-circle" aria-hidden="true"></i> TEAS V scores will NOT be accepted by Valencia's Health Sciences after September
                              15, 2017.
                           </h3>
                           
                           <p>If you would like to transfer scores from tests taken at Valencia College, go to the&nbsp;<a href="https://www.atitesting.com/ati_store/" target="_blank">ATI testing online store</a>, click on TEAS/Discover Transcripts. You will be asked to fill out a form. The scores
                              will be sent electronically.
                           </p>
                           
                           <p>Valencia will accept your previous ATI TEAS scores from another institution. TEAS
                              V scores will only be accepted&nbsp;<u>until September 15, 2017</u>. You may request your scores by going to ATI online stores. Please make sure your
                              scores are sent to Valencia College (Valencia ADN).
                           </p>
                           
                           <p>**Please note, Valencia cannot provide official scores for this test. All requests
                              must be done through ATItesting.com
                           </p>
                           
                           <p>&nbsp;</p>
                           
                           
                        </div>        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/teas.pcf">©</a>
      </div>
   </body>
</html>