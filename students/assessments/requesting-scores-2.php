<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/requesting-scores-2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Assessment Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Requesting/Submitting Scores</h2>
                        
                        <p>Valencia only accepts scores within the past 2 years </p>
                        
                        <h3>PERT (Post Secondary Readiness Test) Scores </h3>
                        
                        <p><strong>Retrieve PERT scores from a Florida school</strong>: If you would like to submit PERT scores taken in the state of Florida within the
                           last 2 years, please <a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">contact</a> us  with the following information:
                        </p>
                        
                        <p> <strong>Full name, VID#, email, phone and where you took the PERT. </strong></p>
                        
                        <p>We will use this information to retrieve your scores in the Florida PERT Repository.
                           
                        </p>
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/assessments/pert-score-request.cfm" target="_blank">PERT Scores Request Form</a></p>
                        
                        <p><strong>Send PERT scores to a different school:</strong> If you would like another Florida institution to receive your PERT scores, ask the
                           receiving institution to retrieve your scores from the Florida PERT Repository. If
                           your institution does not have access to the PERT Repository, please fill out the
                           following form: <a href="documents/TestScoreRequestForm_000.pdf" target="_blank">Valencia Score Request Form</a>. 
                        </p>
                        
                        <h3>Accuplacer CPT (College Placement Test) Scores</h3>
                        
                        <p> If you have taken the Accuplacer in the past 2 years, please have <strong>the institution</strong> fax, mail or email us your scores and call us to ensure we have received them. 
                        </p>
                        
                        <p>We will <u>only accept</u> the following Accuplacer tests:
                        </p>
                        
                        <p><strong>CPT Reading (Comprehension)</strong></p>
                        
                        <p><strong>CPT Sentence Skills (Writing) </strong></p>
                        
                        <p><strong>CPT Elementary Algebra (Math) </strong></p>
                        
                        <p>Click <a href="placement-chart.html" target="_blank">here</a> to review the <strong>CPT</strong> score placement.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                        </p>
                        
                        <h3>Scores must be sent directly from the institution.</h3>
                        
                        <p><strong>Assessment email: </strong>assessment@valenciacollege.edu 
                        </p>
                        
                        <p><strong>Assessment fax:</strong> 407-582-1682
                        </p>
                        
                        <p><strong>Assessment address</strong>: 
                        </p>
                        
                        <p>ATT: Assessment Services<br>
                           1800 S Kirkman Rd<br>
                           Orlando, FL 32811
                        </p>
                        
                        <p><strong>Assessment phone</strong>: 407-582-1101
                        </p>
                        
                        
                        <h3>ACT/SAT Scores</h3>
                        
                        <p> If you have taken the ACT or SAT in the past two years and they are at <a href="placement-chart.html" target="_blank">college level</a>, please have your scores sent to us electronically or bring us the <u>original score report.</u> No copied or scanned score reports will be accepted. 
                        </p>
                        
                        <p><strong> Please note: Sending SAT and ACT scores electronically will take 5 weeks to receive
                              and process. </strong></p>
                        
                        <p>ACT scores can be sent electronically from your ACT account: <a href="http://www.actstudent.org/scores/send/" target="_blank">http://www.actstudent.org/scores/send/</a></p>
                        
                        <p>SAT scores can be sent electronically from your CollegeBoard Account: <a href="http://sat.collegeboard.org/scores/" target="_blank">http://sat.collegeboard.org/scores/</a></p>
                        
                        <p>**Please note, Valencia cannot provide official scores for this test. All request
                           must be done through CollegeBoard. 
                        </p>
                        
                        <h3> TEAS  Scores</h3>
                        
                        <p>
                           If you would like to  transfer scores from tests taken at Valencia College, go to
                           the <a href="http://www.atitesting.com/ati_store/" target="_blank">ATI testing online store</a>, click on TEAS/Discover Transcripts. You will be asked to fill out a  form. The scores
                           will be sent electronically.
                           
                        </p>
                        
                        <p>
                           Valencia will accept your previous TEAS  V scores from another institution. You may
                           request your scores by going to <a href="http://www.atitesting.com/ati_store/" target="_blank">ATI online stores</a>. Please make sure your scores are sent to Valencia College  (Valencia ADN).
                        </p>
                        
                        <p>**Please note, Valencia cannot provide official scores for this test. All requests
                           must be done through ATItesting.com
                        </p>
                        
                        <h3>CJBAT Scores</h3>
                        
                        <p> If you would like to request your CJBAT scores please complete the <a href="documents/TestScoreRequestForm.pdf" target="_blank">Valencia College Test Score Request Form</a>. Make sure to fill out the form completely including your Date of Birth and VID number.
                           Attach a copy of a government issued picture ID that also includes your signature.
                           <span>Request forms without a picture ID with signature will not be processed. Incomplete
                              requests will not be honored. </span>Please fax the form to the Assessment Center of your choice, specific details are
                           located on the form. 
                        </p>
                        
                        <h3>CLEP Scores </h3>
                        
                        <p> If you plan to attend  another college or university, your CLEP scores do not transfer
                           automatically with your Valencia College transcript. You will need to send your CLEP
                           scores in addition to your Valencia transcript. In order to send additional transcripts,
                           call 1-800-257-9558 or by completing a <a href="https://secure-media.collegeboard.org/digitalServices/pdf/clep/clep-transcript-request-form.pdf" target="_blank">CLEP Transcript Request Form</a> and mail it to: 
                        </p>
                        
                        <blockquote> P.O. Box 6600<br>
                           Princeton, NJ<br>
                           08541-6600 
                        </blockquote>        
                        
                        
                        <p>Click<a href="https://clep.collegeboard.org/about/score" target="_blank"> here</a> to read more about CLEP scores. 
                        </p>
                        
                        <p>**Please note the following: </p>
                        
                        <p> Valencia cannot provide official scores for this test. All request must be done through
                           CollegeBoard. 
                        </p>
                        
                        <p>Valencia will ONLY accept CLEP transcripts through CollegeBoard. </p>
                        
                        <p>I<strong>t takes 4 weeks to receive and process CLEP score reports. </strong></p>
                        
                        <h3><strong>CLAST Scores </strong></h3>
                        
                        <p> Teacher certification candidates, who wish to submit CLAST scores to the&nbsp; <a href="http://www.fldoe.org/teaching/certification/" target="_blank">Bureau of Educator Certification</a>, will need to obtain an official score report that display their CLAST scores. 
                        </p>
                        
                        <ul>
                           
                           <li>If the teacher certification candidate took the CLAST prior to July 1, 2002, then
                              the Florida Teacher Certification Examination (FTCE) testing contractor, ES Pearson,
                              will have their CLAST scores on file and can produce a duplicate score report, which
                              will be accepted by the Bureau of Educator Certification.<a href="http://www.fl.nesinc.com/FL_RequestingScoreCopy.asp" target="_blank"> Instructions from ES Pearson for obtaining a duplicate score report</a>. 
                           </li>
                           
                           <li>If an individual took the CLAST before July 1, 2002, but did not pass the subtest(s)
                              until after July 1, 2002, the passing score will not be on file and is not eligible
                              for teacher certification purposes. 
                           </li>
                           
                           <li>If an individual's CLAST scores are listed on their transcript as a 999(99), 998(98),
                              997(97), or 996(96), then the individual was exempted from taking the CLAST and, therefore,
                              does not have passing scores. 
                           </li>
                           
                        </ul>
                        
                        <p> <a href="documents/CLASTscoreform_updated2015_FormOnly.pdf" target="_blank">Pearson Score Request Form</a> if taken prior to July 2002 or after the 60 day window has expired. 
                        </p>
                        
                        <p>More information about the CLAST can be found <a href="http://www.fldoe.org/accountability/assessments/postsecondary-assessment/clast/" target="_blank">here</a>. 
                        </p>
                        
                        <p>**Please note, Valencia cannot provide official scores for this test. All request
                           must be done through Pearson or <a href="http://www.fldoe.org/accountability/assessments/postsecondary-assessment/clast/" target="_blank">FLDOE</a>. 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/requesting-scores-2.pcf">©</a>
      </div>
   </body>
</html>