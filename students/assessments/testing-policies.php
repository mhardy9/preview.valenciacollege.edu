<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing Policies  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, assessments">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/testing-policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Testing Policies </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        
                        <p>
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>General Policies</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul>
                              
                              <li>Testing candidates must have an active Valencia application on file.</li>
                              
                              <li>Student must present an unexpired original photo ID acceptable to Assessment Services.&nbsp;<strong>Acceptable ID's for each test are described below&nbsp;</strong><strong>Testing Lab Regulations</strong>.
                              </li>
                              
                              <li>If there is a fee to take the test, it must be paid prior to testing.</li>
                              
                              <li>Children are not allowed in the testing room. Proper supervision of small children
                                 should be arranged off site prior to testing.
                              </li>
                              
                           </ul>
                           
                        </div>         
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Testing Dishonesty and Behavioral Guidelines</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The following guidelines must be adhered to before, during, or after taking a test
                              administered by the Assessment Department at Valencia College. These guidelines also
                              apply to Valencia students taking tests at other institutions. Test takers must refrain
                              from the following:
                           </p>
                           
                           <ul>
                              
                              <li>Giving or receiving assistance from other examinees.</li>
                              
                              <li>Submitting another person's work as your own, such as having your test taken by someone
                                 else.
                              </li>
                              
                              <li>Taking or attempting to take a test for another person.</li>
                              
                              <li>Working on or reading the test during a time not authorized by the test center staff.</li>
                              
                              <li>Removing or attempting to remove a test book, a test question, or a portion of a test
                                 in any format from the testing room.
                              </li>
                              
                              <li>Using or having books, notes, calculators, earplugs, headsets, rulers, listening devices
                                 (including Bluetooth), paging devices (beepers), cellular phones, recording or photographic
                                 devices, papers of any kind, or other aids unless specifically authorized.
                              </li>
                              
                              <li>Communicating with anyone other than testing personnel.</li>
                              
                              <li>Forging signatures, presenting false identification, or falsifying information on
                                 an official academic document, grade report, letter of authorization, transcript,
                                 test scores or any other official Valencia document.
                              </li>
                              
                              <li>Reproducing, circulating, or otherwise obtaining advance access to test materials.</li>
                              
                              <li>Creating a disturbance or any disruptive behavior.</li>
                              
                              <li>Attempting to bribe a member of the test center staff to obtain a copy of a test or
                                 any confidential information about the test.
                              </li>
                              
                              <li>Attempting to remove scratch paper or notes of any kind from the testing room.</li>
                              
                              <li>Attempting to tamper with a computer.</li>
                              
                              <li>Failing to follow directions issued by a member of the test center staff.</li>
                              
                              <li>Entering the testing room with food and/or drinks.</li>
                              
                              <li>Opening an unauthorized browser during a test administration.</li>
                              
                              <li>Refusing to adhere to any other regulation.</li>
                              
                           </ul>
                           
                           <p>Individuals who exhibit any of the above mentioned behaviors will be subject to appropriate
                              action as specified in the College’s&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>. Incidents will be reported to the appropriate Dean at the College for review and
                              to the department or agency that is associated with the testing program.
                           </p>
                           
                           <p>Please be advised, computer screens are monitored and testing sessions are video recorded
                              via surveillance cameras. Other detection devices may also be used.
                           </p>
                           
                        </div>
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Acceptable ID's - PERT, ACCUPLACER ESL(LOEP), CJBAT, and TEAS</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Acceptable forms of identification include:</p>
                           
                           <ul>
                              
                              <li>A current driver’s license</li>
                              
                              <li>A current Valencia College student ID</li>
                              
                              <li>A current state or federal ID card (such as a Green Card)</li>
                              
                              <li>A current passport</li>
                              
                              <li>A tribal ID card</li>
                              
                              <li>A naturalization card or certificate of citizenship</li>
                              
                           </ul>
                           
                        </div>                               
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>Acceptable ID - CLEP</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><strong>The ID must be a government-issued photo ID. The ID must include the candidate’s name,
                                 signature, and a recognizable photograph</strong>.
                           </p>
                           
                           <p>Acceptable forms of identification include:</p>
                           
                           <ul>
                              
                              <li>Be government-issued</li>
                              
                              <li>Be an original document —photocopied documents are not acceptable</li>
                              
                              <li>Be valid and current —expired documents (bearing expiration dates that have passed)
                                 are not acceptable, no matter how recently they may have expired
                              </li>
                              
                              <li>Bear the test-taker's full name, in English language characters, exactly as it appears
                                 on the Registration Ticket, including the order of the names
                              </li>
                              
                              <li>Middle initials are optional and only need to match the first letter of the middle
                                 name when present on both the ticket and the identification.
                              </li>
                              
                              <li>Bear a recent recognizable photograph that clearly matches the test-taker</li>
                              
                              <li>Include the test-taker’s signature</li>
                              
                              <li>Be in good condition, with clearly legible text and a clearly visible photograph</li>
                              
                           </ul>
                           
                           <p>Acceptable forms of identification&nbsp;<u>MUST</u>&nbsp;have name, photograph and signature on one of the following:
                           </p>
                           
                           <ul>
                              
                              <li>Government-issued passport</li>
                              
                              <li>Driver’s license</li>
                              
                              <li>State or Province ID issued by the motor vehicle agency</li>
                              
                              <li>Military ID (including electronic signatures)</li>
                              
                              <li>National ID</li>
                              
                              <li>Tribal ID card</li>
                              
                              <li>A naturalization card or certificate of citizenship</li>
                              
                              <li>A Permanent Resident Card (Green Card)</li>
                              
                           </ul>
                           
                        </div>  
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/testing-policies.pcf">©</a>
      </div>
   </body>
</html>