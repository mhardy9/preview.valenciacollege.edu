<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Criminal Justice Basic Abilities Test  | Valencia College</title>
      <meta name="Description" content="Criminal Justice Basic Abilities Test">
      <meta name="Keywords" content="college, school, educational, assessments, cjbat, exam, taking">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/criminal-justice-basic-abilities-test/taking-the-cjbat.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/criminal-justice-basic-abilities-test/">Cjbat</a></li>
               <li>Criminal Justice Basic Abilities Test </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>Taking the CJBAT</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h3>Corrections or Law Enforcement</h3>
                           
                           <ol>
                              
                              <li>All candidates applying for the Criminal Justice Institute must have an active application
                                 on file and have paid the $35 application fee. Applications take 3-5 business days
                                 to process. Non-Valencia applicants may start at step 4.
                              </li>
                              
                              <li>Wait to receive an email or letter of acceptance with an explanation on your next
                                 steps - Assessments and New Student Orientation. Note: You do not need to be enrolled
                                 in courses to take the CJBAT, however your application must be active in our system.
                              </li>
                              
                              <li>Create an&nbsp;<a href="https://atlas.valenciacollege.edu/">Atlas account</a>
                                 
                              </li>
                              
                              <li>Come to the Assessment Center on any campus with a government issued photo ID to obtain
                                 a Business Office Referral Form to pay the testing fee. Appointment required for Lake
                                 Nona and Winter Park Campuses.
                              </li>
                              
                              <li>Pay the fee in the Business Office.&nbsp;<em>Valencia College accepts cash, money order, checks, American Express, Discover, MasterCard
                                    and Visa. Checks must be made payable to Valencia College and all credit cards must
                                    be signed by the card holder or they will not be accepted if paying in person.</em>
                                 
                              </li>
                              
                              <li>Return with proof of payment to the Assessment Center.</li>
                              
                              <li>Take the CJBAT.</li>
                              
                              <li>Results will be printed and given to you immediately after testing.</li>
                              
                           </ol>
                           
                           <p>&nbsp;</p>
                           
                           <p>For a CJBAT Study Guide call 1-708-410-0200, or visit&nbsp;<a href="http://www.publicsafetyrecruitment.com/">publicsafetyrecruitment.com</a>&nbsp;or&nbsp;<a href="http://www.iosolutions.org/">iosolutions.org</a></p>
                           
                           <p>&nbsp;</p>
                           
                           <h3>&nbsp;</h3>
                           
                           <p>&nbsp;</p>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/criminal-justice-basic-abilities-test/taking-the-cjbat.pcf">©</a>
      </div>
   </body>
</html>