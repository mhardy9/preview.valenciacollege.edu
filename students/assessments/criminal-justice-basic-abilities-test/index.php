<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Assessment Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/criminal-justice-basic-abilities-test/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Assessment Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>Cjbat</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Criminal Justice Basic Abilities Test (CJBAT)</h2>
                        
                        <h2><strong>Corrections or Law Enforcement </strong></h2>
                        
                        <h3>At a Glance</h3>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Hours of Operation</h3> 
                                    
                                    <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>          
                                    
                                    <p> 
                                       Walk-in basis at East, West, and Osceola campuses (view Note below) <br>
                                       <strong>Mon-Thurs</strong>: 8 AM - 3 PM<br>
                                       <strong>Friday: </strong>9 AM - 2 PM
                                    </p>
                                    
                                    
                                    <p><strong>Note</strong>: Offered on East, West, and Osceola Campus between the above hours. 
                                       
                                       
                                       Also available at the Lake Nona and Winter Park Campuses by appointment only.
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Test Fees</h3>
                                    
                                    
                                    <p><br>
                                       Valencia Student- $40 per test
                                    </p>
                                    
                                    <p>Non-Valencia Student- $50 </p>
                                    
                                    <p><a href="../../business-office/payments.html">Valencia's Payment Method </a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Retake Wait Period</h3> 
                                    No wait period<br> 
                                    May only take 3 times within 12 months. 
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Time Required</h3>
                                    2 hours, 30 minutes
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <p>Technical difficulties inherent with the internet, software or transmission may occur
                                       during your testing experience and are unpredictable. If unexpected technical difficulties
                                       occur, Proctors will make every attempt to promptly resume your assessment. Extended
                                       delays in testing may require rescheduling.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h3><strong>About the CJBAT</strong></h3>
                        
                        <p>The  CJBAT was developed to insure that trainees entering the Florida certified  criminal
                           justice training centers possess the requisite abilities to master the  curricula
                           and understand the materials that are presented to them in training.
                        </p>
                        
                        <p>View Florida Department of Law Enforcement website <a href="http://www.fdle.state.fl.us/cms/CJSTC/Officer-Requirements/Basic-Abilities-Test/Morris-and-McDaniel-Inc.aspx" target="_blank">here</a>. 
                        </p>
                        
                        <p> Valencia offers  the CJBAT in two criminal justice disciplines: Law Enforcement 
                           and Corrections. The CJBAT series of exams were designed to select candidates  for
                           admission into law enforcement officer or correctional officer training.  Each of
                           the tests includes a measure of the same basic abilities, but they are  customized
                           for each criminal justice discipline. 
                        </p>
                        
                        
                        <ul>
                           
                           <li>The CJBAT will assess your skills in  the following areas:</li>
                           
                           <ul>
                              
                              <li>
                                 <strong>Deductive Reasoning</strong> - the ability to apply rules to specific  problems to come up with logical answers
                              </li>
                              
                              <li>
                                 <strong>Inductive Reasoning</strong> - the ability to combine separate pieces of  information, or specific answers to
                                 problems to form general rules or  conclusions
                              </li>
                              
                              <li>
                                 <strong>Information Ordering</strong> - the ability to correctly follow a  rule or set of rules in order to arrange things
                                 or actions in a certain order
                              </li>
                              
                              <li>
                                 <strong>Memorization</strong> - the ability to remember information such as  words, numbers, pictures and procedures
                              </li>
                              
                              <li>
                                 <strong>Problem Sensitivity</strong> - the ability to tell if something is wrong or  likely to go wrong
                              </li>
                              
                              <li>
                                 <strong>Spatial Orientation</strong> - the ability to tell where you are in  relation to the location of some objects
                              </li>
                              
                              <li>
                                 <strong>Written Comprehension</strong> - involves reading &amp; understanding written  words and sentences
                              </li>
                              
                              <li>
                                 <strong>Written Expression</strong> - ability to write a paragraph using  proper grammar and spelling
                              </li>
                              
                           </ul>
                           
                           <li>The CJBAT  is a timed test - you will have 2 ½ hours to complete it.</li>
                           
                           <li>
                              <strong>Passing Scores are as follows:</strong>              
                              
                              <ul>
                                 
                                 <li>Corrections CJBAT: 70%</li>
                                 
                                 <li>Law Enforcment CJBAT: 70%</li>
                                 
                                 <li>
                                    <strong>NOTE</strong>:
                                    Starting February 15 2016, per a Florida Department of Law Enforcement (FDLE) directive,
                                    all numeric score information is to be removed from all law enforcement BAT grade
                                    reports. To meet this request, IOS has produced a pass / fail report for both the
                                    CJBAT – LEO and CJBAT – CO examinations. <u>Numeric scores will no longer be available on any CJBAT reports</u>. Please contact IOS at 708-410-0200 to speak with one of our representatives for
                                    more information, or you may contact the FDLE directly for more information regarding
                                    this policy change.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <p> For a CJBAT Study Guide call 1-708-410-0200, or visit <a href="http://www.publicsafetyrecruitment.com/">publicsafetyrecruitment.com</a> or <a href="http://www.iosolutions.org/">iosolutions.org</a> 
                           </p>
                           
                           <p><a href="taking-the-exam.html">How do I take the CJBAT?</a></p>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/criminal-justice-basic-abilities-test/index.pcf">©</a>
      </div>
   </body>
</html>