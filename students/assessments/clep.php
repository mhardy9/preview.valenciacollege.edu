<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Level Examination Program  | Valencia College</title>
      <meta name="Description" content="College Level Examination Program">
      <meta name="Keywords" content="college, school, educational, assessments, clep, exam">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/clep.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li>College Level Examination Program </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <div class="box_style_1">
                        
                        <p>
                           
                           
                        </p>
                        
                        <div class="indent_title_in">
                           
                           <h2>At a Glance</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>
                              
                           </p>
                           
                           <h3>Hours Available</h3>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>All Assessment Centers Closed September 4. East Campus closed August 4.</strong><p></p>
                           
                           <h4 style="font-weight: bold">West, East, and Osceola Campus is by appointment only</h4>
                           
                           <p style="font-weight: bold">Note:&nbsp;CLEP testing is not available at the Lake Nona or Winter Park campus.&nbsp;Paperwork
                              can be processed through the Lake Nona and Winter Park campus, however the test can
                              only be taken at West, East, and Osceola campuses.
                           </p>
                           
                           
                           <h3>Retake Wait Period</h3>
                           
                           
                           <p>90 days</p>
                           
                           <p>As of October 17, 2014, the waiting period is 90 Days. Please note that this policy
                              is in effect only for students who test on October 17, 2014 or after. For students
                              who test prior to October 17, the waiting period to retake an exam is 180 days.
                           </p>
                           
                           
                           <h3>Test Fees</h3>
                           
                           <p>$20.00 appointment fee*<br>
                              $85.00 per test* (paid through CollegeBoard and subject to change by CollegeBoard)
                           </p>
                           
                           <p><a href="https://clep.collegeboard.org/military">DANTES</a>&nbsp;Tester- No exam fee or appointment fee for first attempt.
                           </p>
                           
                           <p><a href="/students/office-services/business-office/payments.cfm">Valencia's Payment Method</a></p>
                           
                           <p>*Prices subject to change</p>
                           
                           
                           
                           <h3>Time Required</h3>
                           Varies by subject<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> Technical difficulties inherent with the internet, software or transmission may occur
                           during your testing experience and are unpredictable. If unexpected technical difficulties
                           occur, a proctor will make every attempt to promptly resume your assessment. Extended
                           delays in testing may require rescheduling.<br><br>
                           
                           <i class="far fa-info-circle" aria-hidden="true"></i> <strong>It takes 4 weeks to receive and process official CLEP scores.</strong>  
                           
                           
                           
                        </div>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>About the CLEP</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The CLEP is a series of standardized tests that helps you receive college credit for
                              what you already know, for a fraction of the cost of a college course. Developed by
                              the College Board, CLEP is the most widely accepted credit-by-examination program,
                              available at more than 2,900 colleges and universities. If you have obtained knowledge
                              outside the classroom, the CLEP gives you an opportunity to demonstrate your proficiency
                              in the subject area and bypass undergraduate coursework.
                           </p>
                           
                           <ul>
                              
                              <li>In order to meet all graduation requirements,&nbsp;<u><strong>you should take the test prior to the term</strong></u>&nbsp;in which you expect to graduate.
                              </li>
                              
                              <li>The CLEP is a computerized test available to all students enrolled at Valencia.</li>
                              
                              <li>You are able to earn up to 45 credits through the CLEP.</li>
                              
                              <li>CLEP credit meets all graduation requirements, including Gordon Rule mandates if applicable.</li>
                              
                              <li>You will receive a copy of your&nbsp;<em>unofficial</em>&nbsp;scores immediately* after testing; official test scores will be processed 4-6 weeks
                                 after your test date and any applicable credit entered into your record at that time.
                              </li>
                              
                              <li>You can earn a varying number of college credits with CLEP, depending on the particular
                                 test and the score earned. Click&nbsp;<a href="https://valenciacollege.edu/assessments/clep/exams-offered.cfm">here</a>&nbsp;for CLEP Exams offered at Valencia.
                              </li>
                              
                              <li>You are able to retake a CLEP exam after a wait period of 3 months (90 days). Retaking
                                 a test before the wait period is over will invalidate your scores, and you will forfeit
                                 any fees paid.
                              </li>
                              
                           </ul>
                           
                           <ul>
                              
                              <li>You may use CLEP credit under the repeat course policy for D or F grades only. Within
                                 the guidelines of the repeat policy, all attempts for a course will be counted in
                                 your GPA until you earn a grade of C or better when only the last attempt will be
                                 counted. If you earn an acceptable CLEP score in a course in which you have earned
                                 a D or F, the CLEP credit will be recorded and the D or F will no longer be computed
                                 in your GPA. No letter grades or quality points will be assigned.
                              </li>
                              
                              <li>Passing scores are for credit granting purposes only and do not compute into your
                                 grade point average (GPA).
                              </li>
                              
                              <li>Appointment fees are non-refundable and non-transferable.</li>
                              
                              <li>Students in need of special testing accommodations must contact the Office of Students
                                 with Disabilities.
                              </li>
                              
                           </ul>
                           
                           
                           <p>*College Composition test scores will be available only as official scores 4-6 weeks
                              after the test.
                           </p>
                           
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h2>CLEP Transcripts</h2>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If you are planning on attending another college or university, your CLEP scores do
                              not transfer automatically with your Valencia College transcript. You will need to
                              send your CLEP scores in addition to your Valencia transcript. In order to send additional
                              transcripts, call 1-800-257-9558 or by completing a&nbsp;<a href="https://secure-media.collegeboard.org/digitalServices/pdf/clep/clep-transcript-request-form.pdf">CLEP Transcript Request Form</a>&nbsp;and mail it to:
                           </p>
                           
                           <blockquote>P.O. Box 6600<br>
                              Princeton, NJ<br>
                              08541-6600
                           </blockquote>
                           
                           
                        </div>                                                                              
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/clep.pcf">©</a>
      </div>
   </body>
</html>