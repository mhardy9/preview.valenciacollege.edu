<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/assessments/compassesl/taking-the-exam.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/assessments/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/assessments/">Assessments</a></li>
               <li><a href="/students/assessments/compassesl/">Compassesl</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"> 
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div> <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <div> 
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div> 
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   <li><a href="../../students/admissions-records/index.html">Admissions &amp; Records</a></li>
                                                   
                                                   <li><a href="../../students/student-services/index.html">Student Services</a></li>
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <div>
                                             
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured Image 1" border="0" height="260" src="featured-1.png" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                         
                                                      </div>
                                                      <br>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h2>Taking the ACCUPLACER LOEP </h2>
                                                      
                                                      
                                                      <p>In order to  take the ACCUPLACER <strong>LOEP</strong> at Valencia you must:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Have an active application on file - the <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" target="_blank">Admissions application</a>must  be completed at least 5 business days prior to testing.
                                                         </li>
                                                         
                                                         <li>Provide a currently valid government-issued  photo ID, such as a Driver's License,
                                                            Passport, or Green Card. High school IDs are not acceptable.
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <h3>Levels of English Proficiency (LOEP)</h3>
                                                      
                                                      <h3>At a Glance</h3>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <h3>Hours Available</h3>
                                                                  
                                                                  <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                                                                  
                                                                  <p><strong>Mon-Thurs</strong>: 8 AM - 5 PM<br>
                                                                     <strong>Friday</strong>: 9 AM - 4 PM
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <h3>Test Fees</h3>
                                                                  
                                                                  <p>Must pay application fees <br>
                                                                     No Charge 1st attempt, $10 Retake fee 
                                                                  </p>
                                                                  
                                                                  <p> <a href="../../business-office/payments.html">Valencia's Payment Method</a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  
                                                                  <h3>Retake Wait Period</h3>
                                                                  You may take the LOEP two times in one year with a 1-day wait period between tests.
                                                                  Number of attempts is calculated from the anniversary of your first attempt. <br>
                                                                  
                                                               </div>
                                                               
                                                               <div>
                                                                  
                                                                  <h3>Time Required</h3>
                                                                  
                                                                  <p>Part 1- <strong>Typing an Essay</strong>- 30 minutes <br>
                                                                     Part 2- <strong>Sentence Meaning</strong>- No Time Limit <br>
                                                                     Part 3- <strong>Reading</strong>- No Time Limit<br>
                                                                     Part 4-<strong> Language Use</strong>- No Time Limit <br>
                                                                     
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>Technical difficulties inherent with the internet, software or transmission may occur
                                                                     during your testing experience and are unpredictable. If unexpected technical difficulties
                                                                     occur, a proctor will make every attempt to promptly resume your assessment. Extended
                                                                     delays in testing may require rescheduling.
                                                                  </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      <h3>About the ACCUPLACER LOEP </h3>
                                                      
                                                      
                                                      
                                                      <p>If your  native language is not English and you are seeking an associate degree or
                                                         career  certificate, you will be required to take the ACCUPLACER LOEP. The LOEP is
                                                         an assessment that will test language proficiency before you can register for any
                                                         courses. 
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>The LOEP is designed to assess your English  skills if you have learned English as
                                                            an additional language to your native language.
                                                         </li>
                                                         
                                                         <li>Tests are administered on a walk-in basis at Valencia's Assessment Centers. </li>
                                                         
                                                      </ul>
                                                      
                                                      <h3>ACCUPLACER LOEP  Test Breakdown</h3>
                                                      
                                                      <p>The ACCUPLACER LOEP is comprised of 4 parts: Typing an Essay, Sentence Meaning, Reading
                                                         and Language Usage.
                                                      </p>
                                                      
                                                      <p>All  parts aregiven on a computer.</p>
                                                      
                                                      <blockquote>
                                                         
                                                         <blockquote>
                                                            
                                                            <blockquote>
                                                               
                                                               <p>  <img alt="picture of a computer and keyboard" height="104" src="computer_000.PNG" width="95"></p>
                                                               
                                                            </blockquote>
                                                            
                                                         </blockquote>
                                                         
                                                         <p><strong>Part 1: Typing </strong> <strong> an Essay for 30 minutes </strong>(300-600 words) <br>
                                                            <em>This will be the only section of the ACCUPLACER LOEP that has a time limit. You will
                                                               have up to 30 minutes to complete a typed essay with 300-600 words.</em></p>
                                                         
                                                         <p> <strong>Part  2: Sentence Meaning </strong>(20 questions-multiple choice)<br>
                                                            <em>Verbs, nouns, basic phrases </em></p>
                                                         
                                                         <p><strong>Part 3: Reading</strong> (20 questions-multiple choice)<strong> <br>
                                                               </strong><em>Questions on reading material </em> 
                                                         </p>
                                                         
                                                         <p><strong> Part 4: Language Use </strong>(20 questions-multiple choice) <br>
                                                            <em>Sentence structure, subject-verb agreement</em></p>
                                                         
                                                      </blockquote>        
                                                      <h3>Are there any exemptions from the ACCUPLACER LOEP? </h3>
                                                      
                                                      <p>You may be exempt from the ACCUPLACER LOEP if you: </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Provide official Test of English as a Foreign Language (TOEFL) or IELTS scores less
                                                            than two years old that place you in course work at Valencia (See <a href="../Placement-Chart.html">Placement chart</a>).
                                                         </li>
                                                         
                                                         <li>Provide official transcripts of college-level coursework in Freshman English Composition
                                                            with a "C" or better.
                                                         </li>
                                                         
                                                         <li>Provide official transcripts indicating you have earned an Associate or Bachelor's
                                                            degree for which English was the language of instruction. 
                                                         </li>
                                                         
                                                      </ul>        
                                                      
                                                      <h3>Retaking the ACCUPLACER LOEP </h3>
                                                      
                                                      <p>If you would like to retake ACCUPLACER LOEP to possibly increase your score: </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>You may be eligible to retake the test 1 day after the original test date (this option
                                                            is available once per calendar year after initial test date)
                                                         </li>
                                                         
                                                         <li>You must pay a $10 retake fee before retesting</li>
                                                         
                                                         <li>All application fees must be paid prior to retesting</li>
                                                         
                                                         <li>Retakes are not allowed once you begin English for Academic Purpose (EAP) course work</li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      <h3>International Students </h3>
                                                      
                                                      <p>If you are an International student, your scores must place you at EAP Level 4 in
                                                         order to attend Valencia.
                                                      </p>
                                                      
                                                      <p>ACCUPLACER LOEP Placement Chart </p>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <h4>ACCUPLACER LOEP  Average Score</h4>
                                                               </div>
                                                               
                                                               <div>
                                                                  <h4>Level Placement</h4>
                                                               </div>
                                                               
                                                               <div>
                                                                  <h4>Eligible Courses</h4>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>65 or less</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 0</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Admission delayed; referral to Adult Education ESOL </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>66-75</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 2</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP0281 </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>76-85</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 3</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP0381 or EAP0300C, EAP0320C, EAP0340C, EAP0360C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>86-95</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 4</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP0400C, EAP0420C, EAP0440C, EAP0460C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>96-105</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 5</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>106-115</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 6</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP1620C, EAP1640C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>116 or more</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 7</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>(Use PERT scores for placement)</p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p>Note:<br>
                                                         Immigrants, refugees and U.S. citizens scoring 65 or less will have their Admission
                                                         delayed. 
                                                      </p>
                                                      
                                                      <p>Non-Immigrants <strong>(International students)</strong> scoring 0-85 (levels 0, 2, 3) will not be eligible for Admission.
                                                      </p>
                                                      
                                                      <p>COMPASS ESL Placement Chart <span>(This chart is available to assist with placement for those who have <u>previously</u> tested in COMPASS ESL and have current scores. The COMPASS ESL is no longer offered
                                                            at Valencia)</span></p>
                                                      
                                                      <div>
                                                         
                                                         <div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <h4>COMPASS ESL  Average Score</h4>
                                                               </div>
                                                               
                                                               <div>
                                                                  <h4>Level Placement</h4>
                                                               </div>
                                                               
                                                               <div>
                                                                  <h4>Eligible Courses</h4>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>12.4 or less</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 0</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Admission delayed; referral to Adult Education ESOL </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>12.5-29.9</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 2</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP0281 </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>30-39.9</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 3</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP0381 or EAP0300C, EAP0320C, EAP0340C, EAP0360C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>40-49.9</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 4</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP0400C, EAP0420C, EAP0440C, EAP0460C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>50-69.9</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 5</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP1500C and EAP1520C and EAP1540C and EAP1560C or EAP1585C and EAP1586C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>70-97.4</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 6</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>EAP1620C, EAP1640C </p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div>
                                                               
                                                               <div>
                                                                  <p>97.5 or more</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>Level 7</p>
                                                               </div>
                                                               
                                                               <div>
                                                                  <p>(Use PERT scores for placement)</p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>        
                                                      
                                                      <p>Note:<br>
                                                         Immigrants, refugees and U.S. citizens scoring 12.4 or less will have their Admission
                                                         delayed. 
                                                      </p>
                                                      
                                                      <p>Non-Immigrants <strong>(International students)</strong> scoring 0-39.9 (levels 0, 2, 3) will not be eligible for Admission.
                                                      </p>
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="490"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                                   
                                                   <div><img alt="" height="1" src="spacer.gif" width="265"></div>
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="798"></div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>All Assessment Centers Closed November 13, 22-24, December 21-31</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>LOCATIONS &amp; CONTACT</h3>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>West</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 171</div>
                                    
                                    <div>(407) 582-1101</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>East</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 237</div>
                                    
                                    <div>(407) 582-2770</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><strong>Osceola</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248-250</div>
                                    
                                    <div>(407) 582-4149</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Lake Nona</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                    <div>(407) 582-7104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div><strong>Winter Park</strong></div>
                                    
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                    <div>(407) 582-6086</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/assessments/compassesl/taking-the-exam.pcf">©</a>
      </div>
   </body>
</html>