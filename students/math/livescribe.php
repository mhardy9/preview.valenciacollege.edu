<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Math Help 24/7MAT0018CMAT0028CMAT1033CMAC1105MGF1106MGF1107STA2023MAC2233MAC1140MAC1114MAC2311MAC2312MAC2313MAP2302Graphing
         Calculator | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/math/livescribe.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Math Help 24/7</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/math/">Math</a></li>
               <li>Math Help 24/7MAT0018CMAT0028CMAT1033CMAC1105MGF1106MGF1107STA2023MAC2233MAC1140MAC1114MAC2311MAC2312MAC2313MAP2302Graphing
                  Calculator
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="liveScribe.html"></a>
                        
                        <h2><img alt="24/7 Logo" height="86" src="24-7logo.png" width="417"></h2>
                        
                        <h2>Welcome to Math Help 24/7</h2>
                        
                        <p>Below you will find math courses that have links to video tutorials created by Valencia
                           math professors.  The videos use a variety of technologies, all simple to view,  but
                           some will require flash player. If you need flash player installed, you will be given
                           directions on how to install this at the time it is needed..
                        </p>
                        
                        <p><strong>Please answer a few questions using our </strong><a href="http://net4.valenciacollege.edu/forms/math/feedback.cfm" target="_blank"> feedback form</a><strong>. Any information will be helpful in improving the website and the content provided</strong>.
                        </p>
                        
                        <div>
                           
                           
                           <title>MAT0018C</title>                    
                           <div><a href="#">DEVELOPMENTAL MATH I - MAT0018</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>
                                          <u><strong>Integer Basics</strong></u>
                                          
                                          <ul>
                                             
                                             <li>Addition and Subtraction
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=5rqgmLk0vjo" target="_blank">with Julie Phelps</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Multiplication and Division
                                                
                                                <ul>
                                                   
                                                   <li> <a href="https://www.youtube.com/watch?v=FZWbYEwQCgY" target="_blank">with Julie Phelps</a>
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Pre-Algebra Skills</strong></u>
                                          
                                          <ul>
                                             
                                             <li>Translating word problems into symbols
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=R0Pmn9TXiIs" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Collecting like terms
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=i0Bw2FVXRrA" target="_blank">with Julie Phelps</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Order of operations
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=Ba3pISGi6jo" target="_blank">with Alison Hammack</a></li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=dx3JrDKd5p8" target="_blank">with Jennifer Lawhon</a> 
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=bIYBLi3jBPI" target="_blank">with Julie Phelps</a> (basic)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=6DHEE_aYlnI" target="_blank">with Julie Phelps</a> (with several grouping symbols)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Solving linear equations with variables on both sides
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=wT_U50Rnasw" target="_blank">with Joshua Guillemette</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Fraction Basics</strong></u>
                                          
                                          <ul>
                                             
                                             <li>Addition and Subtraction
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=DXA9BQa0XSQ" target="_blank">with Joshua Guillemette</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Multiplication and Division
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=2VQ10Vr1vX4" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Fraction Skills</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li>Solving equations with fractions
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=yXtPsx9QWpE" target="_blank">with Joshua Guillemette</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Applications with fractions
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/Cxsp32fzy8w" target="_blank">with Shalini Gopalkrishnan</a></li>
                                                   
                                                   <li><a href="http://youtu.be/6dnTFA0Qnas" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Decimal Basics</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li> Addition and Subtraction
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=bWojGPaRNys" target="_blank">with Darren Lacoste</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Multiplication and Division
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/T1SehfDH-uc" target="_blank">with Joshua Guillemette</a> (dividing decimals)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=JMW2zZwjo5I" target="_blank">with Darren Lacoste</a> (multiplying decimals)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/O8tbxbMVyNw" target="_blank">with Darren Lacoste</a> (dividing decimals)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Decimal Skills, Percent, and Applications</strong></u>
                                          
                                          <ul>
                                             
                                             <li>Solving equations with decimals
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=ox5Oxcs012Y" target="_blank">with Joshua Guillmette</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Applications with percents
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=nOUg3_oNKbg" target="_blank">with Joshua Guillemette</a> (part 1) 
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=1klb-K-XOIU" target="_blank">with Joshua Guillemette</a> (part 2)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=qaXGTl84jLI" target="_blank">with Darren Lacoste</a> (sale, discount, discount rate)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=evyEH1DRSho" target="_blank">with Darren Lacoste</a> (simple interest)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=BkNL778_yg8" target="_blank">with Darren Lacoste</a> (percent increase/decrease)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Comparing fractions with decimals
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=E5FjN-R1umI" target="_blank">with Shalini Gopalkrishnan</a></li>
                                                   
                                                   <li><a href="http://youtu.be/qBABeLPr39A" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Perimeter, Area, Volume</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li>Perimeter
                                                
                                                <ul>
                                                   
                                                   <li> <a href="http://youtu.be/frNO3O2HIQg" target="_blank">with Shalini Gopalkrishnan</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Area
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/MdpN5sS-b7Q" target="_blank">with Shalini Gopalkrishnan</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Volume
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=TfQHA0ADxxw" target="_blank">with Shalini Gopalkrishnan</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Measurement conversion
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://www.youtube.com/watch?v=ArQiDBXqJbE&amp;feature=share&amp;list=ULArQiDBXqJbE" target="_blank">with Joshua Guillemette</a> (introduction) 
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="http://www.youtube.com/watch?v=0AmimqQNzAw&amp;feature=share&amp;list=UL0AmimqQNzAw" target="_blank">with Joshua Guillemette</a> (examples)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <h3>Joshua Guillemette</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C 
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> NASCAR, Chess, and Reading Non-Fiction
                                    </p>
                                    
                                    
                                    
                                    <h3>Alison Hammack</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C 
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Parenting sons, Family adventures, Horses
                                    </p>
                                    
                                    
                                    
                                    <h3>Darren Lacoste </h3>
                                    
                                    <p><strong>Courses: </strong>MAT0018C, MAT0028C
                                    </p>
                                    
                                    <p><strong>Hobbies: </strong>Strategy Games, Singing
                                    </p>
                                    
                                    
                                    
                                    
                                    <h3>Shalini Gopalkrishnan </h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Julie Phelps</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C, MAC1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div><a href="#">DEVELOPMENTAL MATH II - MAT0028 </a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>
                                          <u><strong>Linear Equations</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li> Solving equations that contain fractions
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=yXtPsx9QWpE" target="_blank">with Joshua Guillemette</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Solving literal equations
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=MvEWCDMbneU" target="_blank">with Joshua Guillemette </a></li>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=9vaEdBQgYwM" target="_blank">with Darren Lacoste</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Solving linear inequalities
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=52_JiR_aytw" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Find equation of a line given a table or graph
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=_Loj90nb_zI" target="_blank">with Joshua Guillemette</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Linear Equation Applications</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li> Learning the four problem-solving steps
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=85yk8YW0rbE" target="_blank">with Darren Lacoste</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Dimensions of a rectangle </li>
                                             
                                             <ul>
                                                
                                                <li><a href="https://www.youtube.com/watch?v=rHQf6g_SzVA" target="_blank">with Darren Lacoste</a></li>
                                                
                                             </ul>
                                             
                                             <li>Distance, rate, time </li>
                                             
                                             <ul>
                                                
                                                <li><a href="https://www.youtube.com/watch?v=Diq91UU5PTA" target="_blank">with Darren Lacoste</a></li>
                                                
                                             </ul>
                                             
                                             <li>Sale, discount, discount rate </li>
                                             
                                             <ul>
                                                
                                                <li><a href="https://www.youtube.com/watch?v=qaXGTl84jLI" target="_blank">with Darren Lacoste</a></li>
                                                
                                             </ul>
                                             
                                             <li>Simple interest </li>
                                             
                                             <ul>
                                                
                                                <li><a href="https://www.youtube.com/watch?v=evyEH1DRSho" target="_blank">with Darren Lacoste</a></li>
                                                
                                                <li><a href="https://www.youtube.com/watch?v=RpnBzY2QCks" target="_blank">with Amanda Saxman</a></li>
                                                
                                             </ul>
                                             
                                             <li>Percent increase/decrease </li>
                                             
                                             <ul>
                                                
                                                <li><a href="https://www.youtube.com/watch?v=BkNL778_yg8" target="_blank">with Darren Lacoste</a></li>
                                                
                                             </ul>
                                             
                                             <li> Area/Perimeter/Tax
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://www.screencast.com/t/x9AEPoT0Efr" target="_blank">with Alison Hammack</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Mixture problems </li>
                                             
                                             <ul>
                                                
                                                <li>
                                                   <a href="http://youtu.be/zC7X1vAZ-SM" target="_blank">with Shalini Gopalkrishnan</a> 
                                                </li>
                                                
                                             </ul>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Graphs and Graph Information</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li>Interpreting slope
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=3kuUzL103Q4" target="_blank">with Jennifer Lawhon</a></li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=LN0rJzc1gJo" target="_blank">with Lisa Cohen</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Plotting points, writing ordered pairs
                                                
                                                <ul>
                                                   
                                                   <li> <a href="http://youtu.be/-Jpl5t1W_Ao" target="_blank">with Joel Berman</a>
                                                      
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Verifying solutions of equations
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/TfKuQST_prc" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Determining slope from points
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/ot9E2Yye2JU" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Graphing with intercepts
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/-II0OEUXYy4" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Graphing horizontal &amp; vertical lines
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/jU_mZQeHXvk" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Writing <em>y=mx+b</em> from graph
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/TYDVmnGC3uk" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Graphing from <em>y=mx+b</em> using slope
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/n6MjmwzDv4U" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Convert standard form to <em>y=mx+b</em>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/z-3k-vt3kDQ" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Completing tables to graph lines, <em>mx+b</em>
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/dGySINa0vK0" target="_blank">with Joel Berman</a> (video 1) 
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/pwqwKZXWLzc" target="_blank">with Joel Berman</a> (video 2)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Complete table to graph lines, <em>standard form</em>
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/LxV1e1XVOhY" target="_blank">with Joel Berman</a> (video 1)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/la3DjM2ItUA" target="_blank">with Joel Berman</a> (video 2)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=zv-Da6VHEH0" target="_blank">with Joshua Guillemette </a>(making a table of points)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Complete table to graph line, <em>standard=0</em>
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://youtu.be/z0CnywF6RmM" target="_blank">with Joel Berman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Setting a graphing window outside of the standard window
                                                
                                                <ul>
                                                   
                                                   <li> <a href="http://screencast.com/t/BFIpWDZf5k">with Jody DeVoe</a> (applications)
                                                   </li>
                                                   
                                                   <li> <a href="http://screencast.com/t/hv3RfDx8">with Jody DeVoe</a> (cubic functions)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Exponents and Polynomials</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li> Order of operations
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=Ba3pISGi6jo" target="_blank">with Alison Hammack</a></li>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=dx3JrDKd5p8" target="_blank">with Jennifer Lawhon</a></li>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=XQ9hIVSX89o" target="_blank">with Amanda Saxman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Negative exponents
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=rRzHWfSGCwA" target="_blank">with Joshua Guillemette</a></li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=yN45ArAgmjA" target="_blank">with Lisa Cohen</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Fractions
                                                
                                                <ul>
                                                   
                                                   <li> <a href="https://www.youtube.com/watch?v=2VQ10Vr1vX4" target="_blank">with Alison Hammack</a> (multiplying and dividing)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=DXA9BQa0XSQ" target="_blank">with Joshua Guillemette </a> (adding and subtracting) 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Decimals
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=bWojGPaRNys" target="_blank">with Darren Lacoste </a>(adding and subtracting) 
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=JMW2zZwjo5I" target="_blank">with Darren Lacoste</a> (multiplying decimals)
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="http://youtu.be/O8tbxbMVyNw" target="_blank">with Darren Lacoste</a> (dividing decimals)
                                                   </li>
                                                   
                                                   <li> <a href="http://youtu.be/T1SehfDH-uc" target="_blank">with Joshua Guillemette</a> (dividing decimals)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Factoring</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li>Factoring polynomial expressions
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=gG5Anu4angI" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Solving trinomials by factoring
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=_pWpQg-FUfc" target="_blank">with Amanda Saxman</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li> Factoring a trinomial using x-factor
                                                
                                                <ul>
                                                   
                                                   <li> <a href="https://www.youtube.com/watch?v=oM8IM02g0RY" target="_blank">with Joshua Guillemette</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Rational Expressions</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li>Addition and Subtraction
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=m9jcU1LRzok" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Multiplication and Division
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=ox-9CcZlLHM" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li> Simplifying rational expressions
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=dDqSnecxqWw" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Measurement conversion
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="http://www.youtube.com/watch?v=ArQiDBXqJbE&amp;feature=share&amp;list=ULArQiDBXqJbE" target="_blank">with Joshua Guillemette</a> (introduction) 
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="http://www.youtube.com/watch?v=0AmimqQNzAw&amp;feature=share&amp;list=UL0AmimqQNzAw" target="_blank">with Joshua Guillemette</a> (examples)
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u><strong>Radicals</strong></u>                              
                                          
                                          <ul>
                                             
                                             <li>Simplifying radicals
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=C_ias_iYo90" target="_blank">with Alison Hammack</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Addition and Subtraction
                                                
                                                <ul>
                                                   
                                                   <li>
                                                      <a href="https://www.youtube.com/watch?v=RPS9kUTrfH4" target="_blank">with Keri Siler</a> 
                                                   </li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>Rationalizing the denominator
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=C_DB0Zxwj4s" target="_blank">with Keri Siler</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>  Pythagorean theorem
                                                
                                                <ul>
                                                   
                                                   <li><a href="https://www.youtube.com/watch?v=KieuZpxfzVE" target="_blank">with Keri Siler</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Lisa Cohen</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028C, MGF1106, MAC2233
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Joshua Guillemette</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> NASCAR, Chess, Reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Alison Hammack</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Parenting, Family Adventures, Horses
                                    </p>
                                    
                                    
                                    
                                    <title>MAT0028C</title>
                                    
                                    <h3>Darren Lacoste </h3>
                                    
                                    <p><strong>Courses: </strong>MAT0018C, MAT0028C
                                    </p>
                                    
                                    <p><strong>Hobbies: </strong>Strategy Games, Singing
                                    </p>
                                    
                                    
                                    
                                    <h3>Jennifer Lawhon</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAC1105, MGF1106, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Running, Singing, Playing Piano
                                    </p>
                                    
                                    
                                    
                                    <h3>Julie Phelps</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018C, MAT0028C, MAT1033C, MAC1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Amanda Saxman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAT1033, MAC1105, MAE2801
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Dancing, Singing, Reading
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAT1033C</title>                    
                           <div><a href="#">INTERMEDIATE ALGEBRA  - MAT1033</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       
                                       <li>Exponent review - negative and rational exponents
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/PShWoO1hARk" target="_blank">with James Lang</a>  (basic laws; fractional and negative exponents)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations with rational expressions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_2zwXDciQWE" target="_blank">with Lisa Cohen</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/kG6VsbSmW9U" target="_blank">with Lisa Cohen</a> (example 2)  
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations using the quadratic formula
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/z5JhXP8VVqQ" target="_blank">with Amanda Saxman</a></li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=zxSrTiEXRsI">with Joel Berman</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Interpreting slope 
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=LN0rJzc1gJo" target="_blank">with Lisa Cohen</a></li>
                                             
                                             <li>
                                                <a href="https://www.youtube.com/watch?v=3kuUzL103Q4" target="_blank">with Jennifer Lawhon</a> (example 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/vJrBqLuvKX0" target="_blank">with Jennifer Lawhon</a> (example 2) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving inequalities graphically 
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=Z5reXOGLlH0" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Word problems
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/UG4ZikEqjsM" target="_blank">with Joel Berman</a> (linear application with two points)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tVeKsrULdfs" target="_blank">with Joel Berman</a> (distance, rate, time)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Factoring 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/uQIA3EdezKY" target="_blank">with Amanda Saxman</a> (using trial and error)
                                             </li>
                                             
                                             <li>
                                                <a href="https://www.youtube.com/watch?v=oM8IM02g0RY" target="_blank">with Joshua Guillemette</a> (using x-factor)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Area/Perimeter/Tax
                                          Word Problems
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.screencast.com/t/x9AEPoT0Efr" target="_blank">with Alison Hammack</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving linear inequalities 
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=52_JiR_aytw" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Addition and subtraction of rational expressions 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/h1ButfO8waE" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/iaFj9KSAKsA" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/QzWe_oGS9V4" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/s-v-PDRd-84" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/G6OBhpkVpVQ" target="_blank">with Abdellatif Dasser</a> (example 4)
                                             </li>
                                             
                                             
                                             
                                             <li><a href="https://www.youtube.com/watch?v=m9jcU1LRzok" target="_blank">with Alison Hammack</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Compound inequalities and interval notation 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/Za1E9AGRTqM" target="_blank">with Amanda Saxman</a></li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=7hfwlXgT6aI">with Joel Berman</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Quadratic max/min applications 
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=VFi1GrRiYKo" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving by extracting roots 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/SnGk5uklkKU" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving systems of equations by elimination 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/-VtVsqukB4E" target="_blank">with Amanda Saxman</a></li>
                                             
                                             <li><a href="http://youtu.be/2AIwZ4u9jXI" target="_blank">with Jennifer Lawhon</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Equations of lines from a table 
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=_Loj90nb_zI" target="_blank">with Joshua Guillemette</a></li>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=JEPKJnuch8Q" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Equations of lines from a graph 
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=G2rCeJ3Ix6Q" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Graphing inequalities 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=QLsSn9PyrAY" target="_blank">with Joel Berman</a> (linear in two variables)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding points of intersection on the calculator 
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=M-1QUYQbYHM" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Negative exponents
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=rRzHWfSGCwA" target="_blank">with Joshua Guillemette</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving systems of equations by substitution 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/CLe17IZx6cQ" target="_blank">with Jennifer Lawhon</a></li>
                                             
                                             <li><a href="http://youtu.be/RL2AFS0IhXA" target="_blank">with Amanda Saxman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Division and multiplication of rational expressions 
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=ox-9CcZlLHM" target="_blank">with Alison Hammack</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Vertex form 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/hJv3KGYk7aY" target="_blank">with Amanda Saxman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Completing tables on the calculator 
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=ChgykRauuaU" target="_blank">with Amanda Saxman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <u>Intermediate Algebra Videos</u> - <em>(this link goes to a series of videos presenting exercises  similar to those found
                                             in the current textbook by Mark Clark and Cynthia Anfinson) </em>                              
                                          
                                          <ul>
                                             
                                             <li> <a href="courses/ClarkLinks_000.html" target="_blank">with Joel Berman </a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Joel Berman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Golf, Knitting, Riding my motorcycle
                                    </p>
                                    
                                    
                                    
                                    <h3>Lisa Cohen</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MGF1106, MAC2233
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Abdellatif Dasser</h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311, 2312, 2313, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Playing soccer, swimming, and reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Joshua Guillemette</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> NASCAR, Chess, and Reading Non-Fiction
                                    </p>
                                    
                                    
                                    
                                    <h3>Alison Hammack</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Parenting sons, Family adventures, Horses
                                    </p>
                                    
                                    
                                    
                                    <h3>Jennifer Lawhon</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAC1105, MGF1106, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies: </strong>Running, Singing, Playing Piano
                                    </p>
                                    
                                    
                                    
                                    <h3>Amanda Saxman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAT1033, MAC1105, MAE2801
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> swing dancing, tap dancing, singing, reading
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC1105</title>
                           
                           <div><a href="#">COLLEGE ALGEBRA  - MAC1105</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       
                                       <li>Applications with exponential functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/B2ba1pcfdgc" target="_blank">with Jolene Rhodes</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Vs_r-uqy_hY" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/HKyJz5v5He4" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/5cZbgLkjP6w" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Average rate of change
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/EznWltGK27s" target="_blank">with Kurt Overhiser</a></li>
                                             
                                             <li> <a href="http://youtu.be/9STSqpQkCU0" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Completing the square
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/fop-M3T07fc" target="_blank">with Sidra Van De Car</a> (with no leading coefficient)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yFl30oBxhiI" target="_blank">with Sidra Van De Car</a> (with a leading coefficient)
                                             </li>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=-nM67fYeqI0&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=5&amp;feature=plpp_video">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/bleHQQv-HQk" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yDJDy0ZouXg" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/jbTmheLS8Jg" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/S2N4gaGrnDk" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Complex numbers
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/o4fg8KfDuJ0" target="_blank">with Abdellatif Dasser</a> (multiplying)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/35MWaHd02so" target="_blank">with Abdellatif Dasser</a> (dividing)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Composition of functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/avVfXfFSuN0" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/kw9Cr4mJ-Sg" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Rvrln_IaKi0" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/ik_WgGXe9ns" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Compound interest
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ysegZU6Hd8o" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/xHxwlZgcB2U" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/a8vajJBLgJs" target="_blank">with Abdellatif Dasser</a> (continuous compounding #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/V7xyypuimCg" target="_blank">with Abdellatif Dasser</a> (continuous compounding #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Continuous compounding vs compounding with k periods
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/pRE-XvIkjts" target="_blank">with Lisa Cohen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Converting back and forth between logs and exponentials
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/0BvFuJY3ITk" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul> 
                                          
                                       </li>
                                       
                                       <li>Domain
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/ZZ6FbuxkUd4" target="_blank">with Lisa Cohen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Exponent review - negative and rational exponents
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/PShWoO1hARk" target="_blank">with James Lang</a> (basic laws; fractional and negative exponents)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Factoring
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/5P-z3-1l3wQ" target="_blank">with Jennifer Lawhon</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding an appropriate viewing window
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/BFIpWDZf5k" target="_blank">with Jody DeVoe</a> (equation with words)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/rgI1ScfeC" target="_blank">with Jody DeVoe</a> (equation no words)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/hv3RfDx8" target="_blank">with Jody DeVoe</a> (cubic functions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/BFIpWDZf5k" target="_blank">with Jody DeVoe</a> (application problems)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the distance between two points
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/_JeTW_vpT4Q" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the midpoint of a segment
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/4uk2ym1Ke1o" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding points of intersection on the calculator
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/M-1QUYQbYHM" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Function notation
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/tc6pRuqnSGc" target="_blank">with Shalini Gopalkrishnan</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Graphing piecewise defined functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/vYmY3b_MEAw" target="_blank">with Lisa Cohen</a></li>
                                             
                                             <li><a href="http://youtu.be/HL3GvWAbFXQ" target="_blank">with Jennifer Lawhon</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EqPmClh4n24" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XCNDLoaFOP8" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Graphing a system of linear inequalities
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/LdAgOJJF0OA" target="_blank">with Jennifer Lawhon</a></li>
                                             
                                             <li> <a href="http://www.youtube.com/watch?v=QLsSn9PyrAY">with Joel Berman</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Interpreting slope
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=LN0rJzc1gJo" target="_blank">with Lisa Cohen</a></li>
                                             
                                             <li>
                                                <a href="https://www.youtube.com/watch?v=3kuUzL103Q4" target="_blank">with Jennifer Lawhon</a> (example #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/vJrBqLuvKX0" target="_blank">with Jennifer Lawhon</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Inverse Functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/PoKzkJaQ394" target="_blank">with Jolene Rhodes</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dBqaLaKDVuY" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/-9k6UoSuG7g" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/q3FKYzWj9OQ" target="_blank">with Abdellatif Dasser</a> (verifying inverse functions) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Quadratic Applications
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/n4Viuy8VW6A" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/LZozq7xyDdY" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://www.youtube.com/watch?v=VFi1GrRiYKo">with Joel Berman</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Quadratic Equations and the Vertex Formula
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/nQwjZrU5fR4" target="_blank">with Kurt Overhiser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Simplifying using powers of <em><strong>i</strong></em>                              
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/LTlKGtkUvpg" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving absolute value equations and inequalities
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/s8Zt2QhSI6M" target="_blank">with Luis Negron</a> (symbolically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/LaJSbJXj7Y8" target="_blank">with Luis Negron</a> (graphically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Ik3VWSUhfCw" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5SF-oy58V0k" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/CmIT_iJ8mQE" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Nydbvw6DnCA" target="_blank">with Abdellatif Dasser</a> (example #4) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving exponential equations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/9SayO-Dyfi0" target="_blank">with Kurt Overhiser</a></li>
                                             
                                             <li> <a href="http://youtu.be/1p0cJq9QpIQ">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                             <li><a href="http://youtu.be/nrjl8PnQxzg" target="_blank">with Jolene Rhodes</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XfQoeU_256c" target="_blank">with Wakeel Zainulabdeen</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving logarithmic equations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/NY5bFEq6giU" target="_blank">with Lisa Cohen</a></li>
                                             
                                             <li><a href="http://youtu.be/TzoeaK1EH_Y" target="_blank">with Kurt Overhiser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EpYHTfVlPEc" target="_blank">with Luis Negron</a> (solve basic log equations [base 2,10 and e])
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/AqqJYbKEK-M" target="_blank">with Luis Negron</a> (solve [base 2] log equations using properties of logs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wBmqBNPXhWI" target="_blank">with Luis Negron</a> (solve [base e] log equations using properties of logs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/cbTxKfXrRs4" target="_blank">with Luis Negron</a> (solve [base 10] log equations using properties of logs)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations using the quadratic formula 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/z5JhXP8VVqQ" target="_blank">with Amanda Saxman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations with rational expressions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_2zwXDciQWE">with Lisa Cohen</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/kG6VsbSmW9U">with Lisa Cohen</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving linear equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/RQgBpnLmrMY" target="_blank">with Abdellatif Dasser</a> (with fractions #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/aFeMUvoScCA" target="_blank">with Abdellatif Dasser</a> (with fractions #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/YgeWVSSfpwM" target="_blank">with Abdellatif Dasser</a> (without fractions)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving literal equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/LUN-3A5RD0Q" target="_blank">with Sylvana Vester</a>  
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving quadratic equations by square root method
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/5yR1QiQrwik" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/sDVFTS_hjtM" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving using extraction of roots  
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/SnGk5uklkKU" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Transformations of graphs
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ydU3AtJIR_U" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/2sseAGCxv-M" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/GqXdOT7j1Sg" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/185W3mTWc3I" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dWaWE69DKo8" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/PV9jmrRN6ZY" target="_blank">with Abdellatif Dasser</a> (example #6)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/8D-lZoOGnjA" target="_blank">with Abdellatif Dasser</a> (example #7)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Vertical and horizontal asymptotes
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/S-DT_pv6uYI" target="_blank">with Luis Negron</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Zeros of polynomial functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/oXJJrWN73rk" target="_blank">with Luis Negron</a> (using Descartes' Rule of Signs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GS0zKAJtxoY" target="_blank">with Luis Negron</a> (using the Rational Zeros test)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ojRk46tkSik" target="_blank">with Luis Negron</a> (using a graph)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/WJC7sEXp308" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/uGGi1l5gMIs" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/_PoBTip0x9M" target="_blank">with Abdellatif Dasser</a> (classifying zeroes)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GCn3VtGCGmA" target="_blank">with Abdellatif Dasser</a> (prescribed zeroes #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XwMLUpZLnb8" target="_blank">with Abdellatif Dasser</a> (prescribed zeroes #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/DkMYRa9tl44" target="_blank">with Abdellatif Dasser</a> (complete factorization) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Joel Berman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Golf, Knitting, Riding my motorcycle
                                    </p>
                                    
                                    
                                    
                                    <h3>Lisa Cohen</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MGF1106, MAC2233
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Abdellatif Dasser</h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2312, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Playing soccer, swimming, and reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jody DeVoe</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2313, MAP2302, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Kayaking, fishing, dogs, reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jennifer Lawhon</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAC1105, MGF1106, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Running, Singing, Playing Piano
                                    </p>
                                    
                                    
                                    
                                    <h3>Wakeel Zainulabdeen </h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2233, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Luis Negron</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Kurt Overhiser</h3>
                                    
                                    <p><strong>Courses:</strong> MAC 1105, MAC 2311, MAC 2312, MHF 2300
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> traveling, snorkeling, eating out at new trendy restaurants
                                    </p>
                                    
                                    
                                    
                                    <h3>Jolene Rhodes</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1140, MAC2312, Intensive Algebra
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Painting, Bowling, Crafting
                                    </p>
                                    
                                    
                                    
                                    <h3>Amanda Saxman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAT1033, MAC1105, MAE2801
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> swing dancing, tap dancing, singing, reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Sylvana Vester</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1105, MGF1107, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MGF1106</title>                    
                           <div><a href="#">COLLEGE MATH  - MGF1106</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Combinations vs permutations
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=adq05rsfBgA" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Survey problems with Venn diagrams
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=YW4YYtAxoM8" target="_blank">with Cathy Ferrer</a> (2 sets)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=2hSl1ma76dA" target="_blank">with Cathy Ferrer</a> (3 sets)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>How to use z-tables 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=dx3nzJ2fGl8" target="_blank">with Cathy Ferrer</a> (the 68-95-99.7 rule/empirical rule) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=cAc7V1TK08E" target="_blank">with Cathy Ferrer</a> (finding z - scores)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=oPGK_9pMdxI" target="_blank">with Cathy Ferrer</a> (using the z-table to find areas under normal curves)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=itkt9wKb5eE" target="_blank">with Cathy Ferrer</a> (applications with normal curves)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Truth tables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=cFgPWKH7AzE" target="_blank">with Cathy Ferrer</a> Basics of Truth Tables
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=SX4OWAiNqlc" target="_blank">with Cathy Ferrer</a> Using the Basics
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Geometry - when to use what. 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=t2ZgLymXcmQ" target="_blank">with Cathy Ferrer</a> (perimeter and area)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=KMeeFvtQihU" target="_blank">with Cathy Ferrer</a> (volume and surface area)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Distinguishing between counting problems and probability problems
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=YrltCJeSrUw" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Euler diagrams 
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=d4Xgt-XW6Xw" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Probabilities: And vs Or
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/G-ktAl0qxQM" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Shading Venn Diagrams 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/KB0AEafnFFY" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Union vs intersection
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/fLOkdG05sm8" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Negations of "all" and "some"
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/EX84lcgTv-g" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Cathy Ferrer</h3>
                                    
                                    <p><strong>Courses:</strong> MAT1033, MAC 1105, MAC1140, MAC1114, MAC2311,2312, MGF1106,1107, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Pottery, Painting, Card Making, Orchids, Bowling
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MGF1107</title>                    
                           <div><a href="#">MATH FOR LIBERAL ARTS - MGF1107</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Percents
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=9q8nsUZgN2I" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Simple Interest
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=FDyWYT-NR_Q" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Compound Interest
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=zpwbMiuIru0" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finance Problems Using the Finance Menu on the Calculator
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/Wc3f9S3X" target="_blank">with James Lang</a> (savings example part 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/0SrvKxnPP" target="_blank">with James Lang</a> (savings example part 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/k70bGDiHuBG" target="_blank">with James Lang</a> (savings example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/BjE888DTB" target="_blank">with James Lang</a> (loan  example part 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/MYskQplrD" target="_blank">with James Lang</a> (loan  example part 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Modular arithmetic
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=FswrYusJLm0" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Check digit problems
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/FBKBF_VrCns" target="_blank">with James Lang</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Intro to Graph Theory
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=COwTY2FfgDQ" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Greedy algorithm
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.screencast.com/t/WQfUUnR6Re" target="_blank">with James Lang</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Euler Paths and Circuits
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=mQyadp5eyRw" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Traveling Salesman and Hamilton Circuits
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=f5q2KPxFSio" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Brute Force Method and Nearest Neighbor Method
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=QrFEusLWdgE" target="_blank">with Cathy Ferrer</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Euler's formula: Like "How many V and E does an icosahedron have?
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Voting methods
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/plurality-voting-method-followed-by-a-run-off/1335137/" target="_blank">with Jennifer Lawhon</a> (plurality vote with runoff) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Cathy Ferrer</h3>
                                    
                                    <p><strong>Courses:</strong> MAT1033, MAC 1105, MAC1140, MAC1114, MAC2311,2312, MGF1106,1107, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> pottery, painting, card making, orchids, bowling
                                    </p>
                                    
                                    
                                    
                                    <h3>James Lang</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2312, 2313 MAP2302, STA2023, MAC1114, MGF1107
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Fishing, nerding, racquetball, basketball
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>STA2023</title>
                           
                           <div><a href="#">STATISTICS - STA2023</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>5-number summary </li>
                                       
                                       <ul>
                                          
                                          <li><a href="http://youtu.be/DZNTntZHKMo" target="_blank">with Abdellatif Dasser</a></li>
                                          
                                       </ul>
                                       
                                       <li>Box Plots
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/ytGBG99PDIg" target="_blank">with Abdellatif Dasser</a> (comparing distributions, ex. 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/7V3YG40c5HA" target="_blank">with Abdellatif Dasser</a> (comparing distributions, ex. 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/fPhdv75ZvwE" target="_blank">with Abdellatif Dasser</a> (comparing distributions, ex. 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Categorical vs Quantitative data
                                          
                                       </li>
                                       
                                       <ul>
                                          
                                          <li><a href="http://youtu.be/HEjfxZ48uvo" target="_blank">with Abdellatif Dasser</a></li>
                                          
                                       </ul>
                                       
                                       <li>Confidence interval interpretations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/to8rl7DWLTA" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Confidence intervals for proportions
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/7xRRQK0iM08" target="_blank">with Abdellatif Dasser</a> (example 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/78qmw5kkhXg" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/fYuxKwXwo0o" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Correlation and scatterplots
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/DrU4HNkXZNY" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Mklt67HiSiU" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Describing a distribution
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/FRx3IdwKPeI" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/aGpQYB39Vas" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/YUJCdyRAcpI" target="_blank">with Abdellatif Dasser</a> (calcuating percentages)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/klcVJmqbWew" target="_blank">with Abdellatif Dasser</a> (choosing summary statistics)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Distinguishing between proportion and mean problems
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=DSYPMs1jSN0" target="_blank">with Sylvana Vester</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Dot Plots
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/lFNk-YZBK9g" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Explanatory and Response variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/SK8hQAWRlbk" target="_blank">with Abdellatif Dasser</a>  
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding normal percentiles
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/DhPgYiSr2t8" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding an <em>x</em>-value given a percentile
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/gFtpC3L1PSY" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Histograms
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/qEqqKrYtIKc" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Hypothesis Testing (using <em>p</em>-values)
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ffhrSvCg1Ek" target="_blank">with Abdellatif Dasser</a> (left-tailed test)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/1PlPMrf7u4o" target="_blank">with Abdellatif Dasser</a> (right-tailed test)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/k3939XVjU64" target="_blank">with Abdellatif Dasser</a> (two-tailed test)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Making decisions with Hypothesis test
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/F-f9jxPY7Ek" target="_blank">with Sylvana Vester</a> (reject or not reject the null)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=4JligKJye1I" target="_blank">with Sylvana Vester</a> (writing the conclusion in context)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Marginal and Conditional distribution
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/FJEB_ZVZ39Q" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Regression Line
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/opqWNilGCRw" target="_blank">with Abdellatif Dasser</a> (example 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/nX31gDdjsfU" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Sampling distributions for proportions 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.screencast.com/t/UP0li2f2Pt9h" target="_blank">with James Lang</a> (using the proportions sampling applet proportions applet)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ql9j0zYndXI" target="_blank">with James Lang</a> (example problem)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/-AJXGw1E9Ww" target="_blank">with Abdellatif Dasser</a> (example 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/L5yE2voAT-E" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/BmXYJ3Bhbp4" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/f6IvtkLyduo" target="_blank">with Abdellatif Dasser</a> (example 4) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Sampling distributions for means
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.screencast.com/t/g4uw1xBoEuIr" target="_blank">with James Lang</a> (using the means applet)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/jAR_I3eI8Ao" target="_blank">with James Lang</a> (example problem)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/IMsygRr9CBI" target="_blank">with Abdellatif Dasser</a> (example 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/b4sn0Ok-VnY" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XCn3kfRre68" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/l-CWRUqLIsk" target="_blank">with Abdellatif Dasser</a> (example 4) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Scatter plots, correlation and describing associations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/HmMgoDq9Mt8" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Shifting and rescaling data
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/P3koeauekcw" target="_blank">with Abdellatif Dasser</a> (example 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/k51jR4AmALc" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Ni61JN1FVCU" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Stem and Leaf Plot
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/9pGU02Neol4" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Ddgy0smXo-8" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Summarizing the center and spread of data
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/VELJ-2R0Q0g" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/sI4CjCvcDLA" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>When to use correlation
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/G6lWydF039g" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/zAE2Lvw3nCg" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/7XkauLjPR-8" target="_blank">with Abdellatif Dasser</a> (presence of outliers)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>When to use s / √
                                          n
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Rhvw8N89OuY" target="_blank">with Shalini Gopalkrishnan</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Understanding/finding a proportion and/or percent 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/7D54k5YndR4" target="_blank">with Jody DeVoe</a> (finding proportions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/7L_9pKmo8fA" target="_blank">with Jody DeVoe</a> (converting between)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>
                                          <em>z</em>-scores
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Vy2yHVTrM5g" target="_blank">with Abdellatif Dasser</a> (comparing values of two variables, ex. 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/VOFDUi0GUxo" target="_blank">with Abdellatif Dasser</a> (comparing values of two variables, ex. 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/vfwVPIbHYuM" target="_blank">with Abdellatif Dasser</a> (comparing values of two variables, ex. 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/B-bULJ85M38" target="_blank">with Abdellatif Dasser</a> (interpreting <em>z</em>-scores)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/rS52mAWYZxY" target="_blank">with Abdellatif Dasser</a> (finding data values, ex. 1 &amp; 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tc9_rVFY4do" target="_blank">with Abdellatif Dasser</a> (finding data values, ex. 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <blockquote>
                                       
                                       <p><strong>Calculator Tutorials (TI-83/TI-84) </strong></p>
                                       
                                    </blockquote>
                                    
                                    <ul>
                                       
                                       <li> Entering data into the calculator
                                          
                                          <ul>
                                             
                                             <li><a href="http://screencast.com/t/rAMQ6vbpYuam" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Histograms
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/nJ6fDA4Z" target="_blank">with Jody DeVoe</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding <em>mean</em>, <em>sd</em>, and <em>5-number summary</em>
                                          
                                          <ul>
                                             
                                             <li><a href="http://screencast.com/t/mg5LqHxHN6" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Regular Boxplot
                                          
                                          <ul>
                                             
                                             <li> <a href="http://screencast.com/t/uPkWgtd2wb" target="_blank">with Jody DeVoe</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Modified Boxplot 
                                          
                                          <ul>
                                             
                                             <li><a href="http://screencast.com/t/YU0uSmZhLz" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Entering 2-variable data/scatter plot/least squares regression line/correlation coefficient
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/b47bEmVCE" target="_blank">with Jody DeVoe</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Using the <strong>normalcdf</strong> function on the calculator 
                                       </li>
                                       
                                       <ul>
                                          
                                          <li><a href="http://youtu.be/sf0IStd6n9I" target="_blank">with Sylvana Vester</a></li>
                                          
                                       </ul>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>James Lang</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2312, 2313 MAP2302, STA2023, MAC1114, MGF1107
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Fishing, nerding, racquetball, basketball
                                    </p>
                                    
                                    
                                    
                                    <h3>Sylvana Vester</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1105, MGF1107, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    <h3>Wakeel Zainulabdeen</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2233, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC2233</title>                    
                           <div><a href="#">BUSINESS CALCULUS  - MAC2233</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       
                                       <li>Algebraic manipulation skills
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/c3iM5y7yjNs" target="_blank">with Lisa Cohen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Applications of derivatives
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/_jhBBMKP5vg" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/nXJaycjAeOs" target="_blank">with Abdellatif Dasser</a> (marginal analysis)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Applications of exponential functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/vcDTXDZLyCA" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Applications of integrals
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/OiT_Wx7epNo" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ZpjQez0wTnA" target="_blank">with Abdellatif Dasser</a> (indefinite integrals) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Average rate of change
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/VJQqzBLTvNU" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Compound interest
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/y_R0o-dQFpo" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/_JKKwWcgrso" target="_blank">with Abdellatif Dasser</a> (continuous compounding)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Continuity of functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/JvtFgqVgrVM" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/f0FbaoItuvs" target="_blank">with Abdellatif Dasser</a> (piecewise functions
                                                
                                                #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/RZIItxWupPo" target="_blank">with Abdellatif Dasser</a> (piecewise functions
                                                
                                                #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Derivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ACfhKvSHDwU" target="_blank">with Abdellatif Dasser</a> (natural logarithmic functions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Kf0Xa_QqnHQ" target="_blank">with Abdellatif Dasser</a> (exponential functions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/75a0XsgeVas" target="_blank">with Abdellatif Dasser</a> (logarithmic and exponential functions
                                                
                                                #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wrQuIbKUgP8" target="_blank">with Abdellatif Dasser</a> (logarithmic and exponential functions
                                                
                                                #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Domain
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ZZ6FbuxkUd4" target="_blank">with Lisa Cohen</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Equations of lines
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ww2qqt5_spE" target="_blank">with Wakeel Zainulbadeen</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Exponent review with negative and rational exponents
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/PShWoO1hARk" target="_blank">with James Lang</a>  (basic laws; fractional and negative exponents)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/50_Guu8hl10" target="_blank">with James Lang</a>  (writing expressions in the form Ax^n)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Factoring methods
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/0oR2eXfOSu8" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding derivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_6B4j9IyLrI" target="_blank">with Abdellatif Dasser</a> (example #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/JeAB5CzNs5A" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/a4T0FQUd_VA" target="_blank">with Abdellatif Dasser</a> (product rule)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/mbXvUT_25zM" target="_blank">with Abdellatif Dasser</a> (quotient rule)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/vEKz09uwwVM" target="_blank">with Abdellatif Dasser</a> (chain rule)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding derivatives using limit definition
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/H4kCWtks8bM" target="_blank">with Abdellatif Dasser</a> (example #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/0IHLtXysHMA" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/WBnlDgRFDhc" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding equations for exponential curves
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/Kcltx9OAqHk" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding limits
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/HO8UTB0VAkg" target="_blank">with Abdellatif Dasser</a> (using graphs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/TR1fhpkbaEo" target="_blank">with Abdellatif Dasser</a> (simplifying)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding relative minimum or maximum using first derivative test
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/_Ah1xPjw_wU" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Indefinite integrals
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/tNzEteu95lo" target="_blank">with Abdellatif Dasser</a> (example #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/xbcaQmmK9ig" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/2SESi14vRBg" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5N-BU1Mi8l4" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Infinite limits
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/wPXgFgkJkM0" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Intervals
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/tEEPG67Eans" target="_blank">with Abdellatif Dasser</a> (increase/decrease)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/B0saD3pzr7k" target="_blank">with Abdellatif Dasser</a> (concavity)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Linear functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/r7lkACWGEfA" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/88-85T6_aAU" target="_blank">with Abdellatif Dasser</a> (example #2)  
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Obtaining demand or supply functions and relations to cost, revenue and profit.
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/eXbr3MrN2r8" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Quadratic functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/91DW_SWZIoE" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/gT-rgtYEl48" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/hrePgfOXONo" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving exponential equations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/VPUaZdofnUs" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> u-substitution for integrals
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/hQTztc2N18o" target="_blank">with Sidra Van De Car</a></li>
                                             
                                             <li><a href="http://youtu.be/f6Sx9xcsZvk" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Lisa Cohen</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MGF1106, MAC2233
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Sidra Van De Car</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2233, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Wakeel Zainulabdeen</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2233, STA2023 
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    <h3>Abdellatif Dasser </h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2312, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Playing soccer, swimming, and reading
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC1140</title>
                           
                           <div><a href="#">PRECALCULUS  - MAC1140</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Adding and subtracting fractions and rational expressions review
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/QaE8g5muLTw" target="_blank">with Joel Berman</a> (rational expressions)
                                             </li>
                                             
                                             <li>
                                                <a href="https://www.youtube.com/watch?v=m9jcU1LRzok" target="_blank">with Alison Hammack</a> (rational expressions)
                                             </li>
                                             
                                             <li>
                                                <a href="https://www.youtube.com/watch?v=DXA9BQa0XSQ" target="_blank">with Joshua Guillemette</a> (fractions)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Applications of exponential functions
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Vs_r-uqy_hY" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/HKyJz5v5He4" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/5cZbgLkjP6w" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Applications of linear functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/sKyzILYWvMw" target="_blank">with Luis Negron</a> (given initial value and slope)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/HKOTx_kzp68" target="_blank">with Luis Negron</a> (given two points #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/VGe05MAJd3k" target="_blank">with Luis Negron</a> (given two points #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Completing the square 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/fop-M3T07fc" target="_blank">with Sidra Van De Car</a>  (with no leading coefficient)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yFl30oBxhiI" target="_blank">with Sidra Van De Car</a>  (with a leading coefficient)
                                             </li>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=-nM67fYeqI0&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=5&amp;feature=plpp_video" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/bleHQQv-HQk" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yDJDy0ZouXg" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/jbTmheLS8Jg" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/S2N4gaGrnDk" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Complex numbers
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/o4fg8KfDuJ0" target="_blank">with Abdellatif Dasser</a> (multiplying)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/35MWaHd02so" target="_blank">with Abdellatif Dasser</a> (dividing)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Composition of functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=avVfXfFSuN0&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=34&amp;feature=plpp_video" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/kw9Cr4mJ-Sg" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Rvrln_IaKi0" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/ik_WgGXe9ns" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Compound interest
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ysegZU6Hd8o" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/xHxwlZgcB2U" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/a8vajJBLgJs" target="_blank">with Abdellatif Dasser</a> (continious compounding #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/V7xyypuimCg" target="_blank">with Abdellatif Dasser</a> (continious compounding #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Deciding whether (x-k) is a factor of a polynomial
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/zxtouje5S88" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Determinant of 3x3
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/m5yel1nWxno" target="_blank">with Jolene Rhodes</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Determining odd and even functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/pP5rDvFBWic" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yf7kWAU1gMw" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Difference quotient/average rate of change
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/VeCymmsU1Jw" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tcamwppIr_A" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Nz4AWbDwd7c" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/VJQqzBLTvNU" target="_blank">with Abdellatif Dasser</a> (average rate of change)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Evaluating logarithms
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/ctKHKezu61k" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Exponent review with negative and rational exponents
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/PShWoO1hARk" target="_blank">with James Lang</a> (basic laws; fractional and negative exponents)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/50_Guu8hl10" target="_blank">with James Lang</a> (writing expressions in the form Ax^n)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Exponential functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=vlUZiJ1bwVU&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=40&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (compound interest)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=j5g-kjzLEI0&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=41&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (sketch exponential function and inverse)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=bTB1EHGDv1Y&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=42&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (writing an exponential function)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Factoring
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/5P-z3-1l3wQ" target="_blank">with Jennifer Lawhon</a></li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=prh5ViwdPbc&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=13&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (graphing to factor)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding a polynomial that satisfies given conditions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/_HYjD_UdxOk" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding equations for exponential curves
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/CEBYoN4Uvbw" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the distance between two points
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/_JeTW_vpT4Q" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the equation of a circle
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/pAcfXQm5pDU" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the midpoint of a segment
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/4uk2ym1Ke1o" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Gaussian elimination
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/yrbxTdDDVoY" target="_blank">with Jolene Rhodes</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Graphing rational functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ZwZGqBnvf5M" target="_blank">with Luis Negron</a> (asymptotes and intercepts)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tF6WqK3IpPw" target="_blank">with Luis Negron</a> (asymptotes and intercepts and a "hole")
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Dk-hHiJaZTA" target="_blank">with Luis Negron</a> (slant asymptotes)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=aV2aV9SXjaE&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=20&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (with a "hole" #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=7qrLLuM6BFs&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=35&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (with a "hole" #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=-Hw3Ny4kN_I&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=21&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (with slant asymptotes)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=QhpTb68YyEk&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=32&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rational function that crosses horizontal asymptote)
                                             </li>
                                             
                                          </ul> 
                                          
                                       </li>
                                       
                                       <li>Inequalities
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/gePgWttyTLQ" target="_blank">with Jolene Rhodes</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/vhDUkjDigr8" target="_blank">with Luis Negron</a>  (quadratic inequalities symbolically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/jpJcndFn9kc" target="_blank">with Luis Negron</a>  (quadratic inequalities graphically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/s8Zt2QhSI6M" target="_blank">with Luis Negron</a>  (absolute value inequalities symbolically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/LaJSbJXj7Y8" target="_blank">with Luis Negron</a>  (absolute value inequalities graphically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/44IQODNnpIA" target="_blank">with Luis Negron</a>  (linear inequalities graphically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=r9RyUr4j3M0&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=1&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (solving absolute value equations and inequalities)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=Sh3t1TWptJc&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=6&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (solve inequality graphically)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Intervals of increasing/decreasing
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/N30ierzvQ" target="_blank">with Jody DeVoe</a> (understanding increasing/decreasing)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/BiPxkzimVQ8" target="_blank">with Jody DeVoe</a> (writing intervals of increasing/decreasing)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Inverse functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=tmz7eWrzJMI&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=36&amp;feature=plpp_video" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=aycPcDm5X3E&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=46&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (find <em>f</em> inverse <em>2</em>)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dBqaLaKDVuY" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/-9k6UoSuG7g" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/q3FKYzWj9OQ" target="_blank">with Abdellatif Dasser</a> (verifying inverse functions) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Logarithmic functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=p3-IQPsqktQ&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=43&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (sketch log functions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=h1jzwW_gcxE&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=44&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (condensing logarithms)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/zmL0qfWhw_8" target="_blank">with Abdellatif Dasser</a> (condensing logarithms #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/unYDr0pIP9w" target="_blank">with Abdellatif Dasser</a> (condensing logarithms #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=_wSyFuIeg8k&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=45&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (expanding logarithms)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/8ThEO_p0g4c" target="_blank">with Abdellatif Dasser</a> (expanding logarithms #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/02Dh06H14xM" target="_blank">with Abdellatif Dasser</a> (expanding logarithms #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>One-to-one functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=uAK4vdhUNwg&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=39&amp;feature=plpp_video" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul> 
                                          
                                       </li>
                                       
                                       <li>Optimization
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=ZM_uU4_fVbc&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=10&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=eEUG9ND76T4&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=14&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=KIEXSYVUIhk&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=17&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=gr7FNBKySwQ&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=15&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=mkgN4vWTYM0&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=16&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Piecewise functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=ehqPIzKEcpw&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=7&amp;feature=plpp_video" target="_blank">with Joel Berman</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Polynomials
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=11HUgUsmpqE&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=8&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (graph and interpret polynomial w/ calculator)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=84w8E5TxfzE&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=9&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (determine degree of polynomial from graph)
                                             </li>
                                             
                                             <li> <a href="http://www.youtube.com/watch?v=Y3yXVnmKuAE&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=12&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (long division)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XH7DRFQ8UPg" target="_blank">with Abdellatif Dasser</a> (long division) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=9No2sd52sFI&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=11&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (completely factored form of polynomial #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=aa6TeKA80Ro&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=18&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (completely factored form of polynomial #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=4gi747VbtkY&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=19&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (using graphing to factor a polynomial)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=Z_QJpAQGvvA&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=26&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (solve polynomial inequality graphically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=DtRqvt3NEGU&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=38&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (solve polynomial inequality symbolically)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Quadratics
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=h626p9sPcmk&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=2&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (finding quadratic model from a graph)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=QZ4XCHTZYn8&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=3&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (quadratic formula with fractional coefficients)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=Isri8LCMKHk&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=4&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (convert quadratic to vertex)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Radicals
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=aGEzKvazGVQ&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=27&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (solving radical equations #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=LfL5u8hLmko&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=31&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (solving radical equations #2) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=fK_-SvG0R3Q&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=28&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rewrite using radical notation)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Rational zero theorem
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ehEEAmm5WTc" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/otksIRU-Uc4" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/pNozNLIJenM" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Remainder theorem
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/UhrUJkfMTh0" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Simplifying using powers of <strong><em>i</em></strong>
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/LTlKGtkUvpg" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving absolute value equations and inequalities
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Ik3VWSUhfCw" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5SF-oy58V0k" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/CmIT_iJ8mQE" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Nydbvw6DnCA" target="_blank">with Abdellatif Dasser</a> (example #4) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations using substitution
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=EplO0qulhyM&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=29&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=JkpTZ20Y0pU&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=30&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=HnRF6eG5V60&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=33&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (problem #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving exponential equations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/1p0cJq9QpIQ" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                             <li><a href="http://youtu.be/VPUaZdofnUs" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                             <li><a href="http://youtu.be/9SayO-Dyfi0">with Kurt Overhiser</a></li>
                                             
                                             <li><a href="http://youtu.be/nrjl8PnQxzg">with Jolene Rhodes</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving linear equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/RQgBpnLmrMY" target="_blank">with Abdellatif Dasser</a> (with fractions #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/aFeMUvoScCA" target="_blank">with Abdellatif Dasser</a> (with fractions #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/YgeWVSSfpwM" target="_blank">with Abdellatif Dasser</a> (without fractions)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving logarithmic equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/EpYHTfVlPEc" target="_blank">with Luis Negron</a>  (basic log equations [base 2,10 and e])
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/AqqJYbKEK-M" target="_blank">with Luis Negron</a>  ([base 2] log equations using properties of logs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wBmqBNPXhWI" target="_blank">with Luis Negron</a>  ([base e] log equations using properties of logs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/cbTxKfXrRs4" target="_blank">with Luis Negron</a>  ([base 10] log equations using properties of logs)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving quadratic equations by square root method
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/5yR1QiQrwik" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/sDVFTS_hjtM" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving rational equations/inequalities 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/x5tlhAeo6cw" target="_blank">with Luis Negron</a>  (rational inequalities symbolically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GnRKW9cxhOo" target="_blank">with Luis Negron</a>  (rational inequalities graphically)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=UbBeBBfS7ug&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=22&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rational equations)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=TA2qy1oMRAE&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=24&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rational inequalities symbolically #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=5w1kBnAwjmo&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=25&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rational inequalities symbolically #2)
                                             </li>
                                             
                                             <li> <a href="http://www.youtube.com/watch?v=KjNc64C79IQ&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=37&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rational inequalities symbolically #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=frFZmD8dG2k&amp;list=PLxSBUPOc1pY5Zf40dMjnv7druSuI59CUt&amp;index=23&amp;feature=plpp_video" target="_blank">with Joel Berman</a> (rational inequalities graphically)  
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations with non-integer exponents like x2/3 or x-2 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/RWr51X2dwTk" target="_blank">with Shalini Gopalkrishnan</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Synthetic division
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/QS7C4pfrV4I" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Transformations of graphs
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ydU3AtJIR_U" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/2sseAGCxv-M" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/GqXdOT7j1Sg" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/185W3mTWc3I" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dWaWE69DKo8" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/PV9jmrRN6ZY" target="_blank">with Abdellatif Dasser</a> (example #6)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/8D-lZoOGnjA" target="_blank">with Abdellatif Dasser</a> (example #7)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Writing the complete factored form of f(x) given that k is a zero
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/zvbWEG6ogJ0" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Writing the equation of a circle in standard form
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/aQhLEa6NQLc" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Zeros of polynomial functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/oXJJrWN73rk">with Luis Negron</a> (using Descartes' Rule of Signs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GS0zKAJtxoY">with Luis Negron</a> (using the Rational Zeros test)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ojRk46tkSik">with Luis Negron</a> (using a graph)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/WJC7sEXp308">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/uGGi1l5gMIs">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/_PoBTip0x9M">with Abdellatif Dasser</a> (classifying zeroes)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GCn3VtGCGmA">with Abdellatif Dasser</a> (prescribed zeroes #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XwMLUpZLnb8">with Abdellatif Dasser</a> (prescribed zeroes #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/DkMYRa9tl44">with Abdellatif Dasser</a> (complete factorization) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Joel Berman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Golf, Knitting, Riding my motorcycle
                                    </p>
                                    
                                    
                                    
                                    <h3>Abdellatif Dasser</h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2312, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Playing soccer, swimming, and reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Joshua Guillemette</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033 
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> NASCAR, Chess, and Reading Non-Fiction
                                    </p>
                                    
                                    
                                    
                                    <h3>Alison Hammack</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033 
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Parenting sons, Family adventures, Horses
                                    </p>
                                    
                                    
                                    
                                    <h3>James Lang </h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2312, 2313, MAP2302, STA2023, MAC1114, MGF1107
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Fishing, nerding, racquetball, basketball
                                    </p>
                                    
                                    
                                    
                                    <h3>Luis Negron</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028 
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Jody De Voe</h3>
                                    
                                    <p><strong>Courses: </strong>MAC2311,2313, MAP2302, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Kayaking, fishing, dogs, reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jolene Rhodes</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1140, MAC2312, Intensive Algebra
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Painting, Bowling, Crafting
                                    </p>
                                    
                                    
                                    
                                    <h3>Sidra Van De Car</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2233, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    <h3>Wakeel Zainulabdeen</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2233, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC1114</title>                    
                           <div><a href="#">TRIGONOMETRY - MAC1114</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Converting degrees to degrees-minutes-seconds
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/e7vdF0K6fzU" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Converting degrees-minutes-seconds to degrees
                                          
                                          <ul>
                                             
                                             <li>  <a href="http://youtu.be/DGHe-nbHd-c" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Degrees vs. Revolutions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/NnlhiJ5rzDM" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/9Sgs4MLAmHo" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Double-Angle Identities
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/bpUS_irZ2BQ" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Evaluating trigonometric functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/LdB9mnE_DIg" target="_blank">with Joel Berman</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Hp6M7R3Gs0g" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/77NatrvD_XM" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Evaluating inverse trigonometric functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/WLxwapa72Hk" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/f5Ok_PM_tHw" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/KAKqEUvyGVA" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/fAkMkTFHR_E" target="_blank">with Abdellatif Dasser</a> (example 4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Expressing infinite solutions for trigonometric equations
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/K3W9nNaDRuM" target="_blank">with Sylvana Vester</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding exact values of trigonometric functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/4-uzH3xXajM" target="_blank">with Paul Flores</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding the equation given a sine or cosine graph
                                          
                                          <ul>
                                             
                                             <li><a href="http://edcr8.co/Nb7qHM" target="_blank">with James Lang</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the measure of the central angle given the circumference
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/lfS1TO5INkU" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding the other trigonometric solution
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/hTa1cRrn5ek" target="_blank">with Sylvana Vester</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the radian measure of a central angle subtended by an arc
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/EAFDAtpAR1c" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Graphing  y =  Asin(Bx + C) + k 
                                          or y = Acos(Bx + C) + k
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=c0XgXIA-bD8&amp;feature=feedu" target="_blank">with Sylvana Vester</a> (sine) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=0OXTEthianc&amp;feature=related" target="_blank">with Sylvana Vester</a> (cosine)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/6QBikhJCwt4" target="_blank">with Abdellatif Dasser</a> (sine, ex. 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/QqQAtWj4yRQ" target="_blank">with Abdellatif Dasser</a> (sine, ex. 2) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/WhghNgVs7s0" target="_blank">with Abdellatif Dasser</a> (cosine) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Laws of Sines: The Ambiguous Case
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/-MR9l5CXmOA" target="_blank">with Jody DeVoe</a> (one triangle)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/sSeGVmPXXHU" target="_blank">with Jody DeVoe</a> (two triangles)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/OWGbwwhzniI" target="_blank">with Jody DeVoe</a> (no triangles)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Linear and angular velocity
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/e5278xl5EfA" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Locating angles greater than 1 revolution (like 15π/7 or -1020°)
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/3pWXCfa0TJk" target="_blank">with Sylvana Vester</a> (part 1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/UVfpW0ITQsw" target="_blank">with Sylvana Vester</a> (part 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Polar Coordinates
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/polar-coordinates-plotting-points/19075923/?ref=appemail" target="_blank">with Jody DeVoe</a> (plotting points) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/polar-coordinates-converting-points/19076358/?ref=appemail" target="_blank">with Jody DeVoe</a> (converting points) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/graphing-equations-in-polar/19076647/?ref=appemail" target="_blank">with Jody DeVoe</a> (graphing equations in polar)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/converting-equations-polar-coordinates/19077431/?ref=appemail" target="_blank">with Jody DeVoe</a> (converting equations into polar)
                                             </li>
                                             
                                          </ul> 
                                          
                                       </li>
                                       
                                       <li>Radians to Degrees conversion
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_v3ArUAlrtE" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/gGZb0xN6l_E" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Reference angles
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/N5ux6nZBufc" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Reducing fractions with π
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Signs of sine and cosine
                                          
                                          <ul>
                                             
                                             <li>with Wakeel Zainulabdeen</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Simplifying expressions using fundamental identities
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/YmdxdqMelEg" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/SPrpT8D96PI" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/jZ16xl4IrDQ" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ILIIGehSLH8" target="_blank">with Abdellatif Dasser</a> (example 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/RrbgrMFDboU" target="_blank">with Abdellatif Dasser</a> (example 5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/A1s0kVkaQ3g" target="_blank">with Abdellatif Dasser</a> (example 6)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/N3bCsYL8d_8" target="_blank">with Abdellatif Dasser</a> (example 7)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/bQb8qQWXUAc" target="_blank">with Abdellatif Dasser</a> (example 8)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Simplifying inverse functions such as cos(sin^1(¼)) exactly
                                          
                                          <ul>
                                             
                                             <li><a href="http://www.youtube.com/watch?v=qwztywnewMo" target="_blank">with Sylvana Vester</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Sketching angles in standard position
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/U34WxRMVJHI" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>SOHCAHTOA
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/To0n_ZM29Ig" target="_blank">with Paul Flores</a> 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving trigonometric equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/IhJI9J-6KIc" target="_blank">with Sylvana Vester</a> (basic equations)
                                             </li>
                                             
                                             <li><a href="http://youtu.be/NTA9E_0E45s" target="_blank">with Sylvana Vester</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/FYO6OMC88cc" target="_blank">with Wakeel Zainulabdeen</a> (basic equations) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving trigonometric equations that require using an identity  
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/tCHCB4AGoWM" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/hhSeX6qjrgo" target="_blank">with Abdellatif Dasser</a> (introduction)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dhwqT_d62c8" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/hdbMtMZ-YFM" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/rYmyqSt_2_k" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             
                                             <li><a href="http://youtu.be/F5QcrIwYE6A" target="_blank">with Sidra Van De Car</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving trigonometric equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/QOWRFDyIyZA" target="_blank">with Jody DeVoe</a>   (of the form y = Asin(Bx +C))
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yuBJ-wWWR4o" target="_blank">with Jody DeVoe</a>   (factoring)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wJRReIuVXfY" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/9zfu4VS3if0" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/c8nJT3QFQNg" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving trigonometric equations that require the quadratic formula or factoring
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/uz1s1H_AuCQ" target="_blank">with Paul Flores</a></li>
                                             
                                             <li><a href="http://youtu.be/6l5hM8VJqok" target="_blank">with Kurt Overhiser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving applied triangle problems
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/nhC1MN8ZY9U" target="_blank">with James Lang</a>  (basic right triangles)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/MvJxfSD3O6w" target="_blank">with James Lang</a>  (two right triangles; two equations)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving equations with 2θ, 3θ  etc.
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Sum and Difference trigonometric identities
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/SfLCIXc8bzY" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Aq5MslBnkhs" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Unit circle-special angles
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/tBNid5U9fVU" target="_blank">with Jody DeVoe</a> (in quadrant I)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ZWsOjsWINd4" target="_blank">with Jody DeVoe</a> (outside quadrant I)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/q0e46uoaN1w" target="_blank">with Jody DeVoe</a> (where do those coordinates come from?)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Using the fundamental identities to find the exact values of the remaining trigonometric
                                          functions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/GuJcuHBg-ag" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/mWRpyTvWerM" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Using general (coordinate) definition for non-special angles
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/ozisB9J8VnA" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Vectors
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/UzFVtA5tl3Y">Abdelatif Dasser</a> (dot product)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/K0TT4I2YhYE">Abdelatif Dasser</a> (cross product)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/pPdUnEZvslw">Abdelatif Dasser</a> (angle between two vectors)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5-nVnAibfzA">Abdelatif Dasser</a> (resolving a vector into components)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/G4tI779rBOE">with Jim Franklin</a> (vector projections)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Verifying basic identities
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=CgddAydFPKs" target="_blank">with James Lang</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Verifying double or half angle identities  
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=YKpnTgKf9Ek" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Verifying if a point lies on the unit circle
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=0BvW8Dh1MOU" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Jody De Voe</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2313, MAP2302, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Kayaking, fishing, dogs, reading
                                    </p>
                                    
                                    
                                    
                                    <h3>James Lang</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2312, 2313, MAP2302, STA2023, MAC1114, MGF1107
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Fishing, nerding, racquetball, basketball
                                    </p>
                                    
                                    
                                    
                                    <h3>Abdellatif Dasser </h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2313, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong>Playing soccer, swimming, and reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jim Franklin</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2311, 2312, 2313, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Joel Berman </h3>
                                    
                                    <p><strong>Courses: </strong>MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Golf, Knitting, Riding my motorcycle
                                    </p>
                                    
                                    
                                    
                                    <h3>Kurt Overhiser</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1105, MAC2311, 2312, MHF2300
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> traveling, snorkeling, eating out at new trendy restaurants
                                    </p>
                                    
                                    
                                    
                                    <h3>Sidra Van De Car</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2233, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Sylvana Vester</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1105, MGF1107, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Wakeel Zainulabdeen</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2233, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC2311</title>                    
                           <div><a href="#">CALCULUS I  - MAC2311</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Area between curves
                                          
                                          <ul>
                                             
                                             <li><a href="https://www.youtube.com/watch?v=-b2PB_iNPMk" target="_blank">with Kurt Overhiser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/KrAbu894W1I" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/fUkE9RfvYUA" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Chain Rule
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/ge2V8AQLWzA" target="_blank">with Kurt Overhiser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/8lsqrEXJodM" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/RElNdEDX1WM" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EYB06Rz3dG4" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/_00n_fX0LvY" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/X2k7stLpcoY" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/-pg_wa8XZ6k" target="_blank">with Abdellatif Dasser</a> (example #6)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/fcLWb7L7_AA" target="_blank">with Abdellatif Dasser</a> (example #7)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/93egdMnUoHk" target="_blank">with Abdellatif Dasser</a> (example #8)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Continuity of functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/JvtFgqVgrVM" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/f0FbaoItuvs" target="_blank">with Abdellatif Dasser</a> (piecewise functions #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/RZIItxWupPo" target="_blank">with Abdellatif Dasser</a> (piecewise functions #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Derivatives and Concavity
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/j2QC87ejMGgu" target="_blank">with Jody DeVoe</a> (1st derivative)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/TzVMAaNdf2" target="_blank">with Jody DeVoe</a> (2nd derivative)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Evaluating limits using squeeze theorem
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/pZXJ6ryNJRI" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/vGIURICyoGk" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Exponent review - negative and rational exponents
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/PShWoO1hARk" target="_blank">with James Lang</a> (basic laws; fractional and negative exponents)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/50_Guu8hl10" target="_blank">with James Lang</a> (writing expressions in the form Ax^n)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding areas using antiderivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/gkP8I2IWKzs" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/73GAPtUezFY" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/irUeTack-uc" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/hrI995yE8MM" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding derivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_6B4j9IyLrI" target="_blank">with Abdellatif Dasser</a> (power rule, ex. #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/JeAB5CzNs5A" target="_blank">with Abdellatif Dasser</a> (power rule, ex. #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/a4T0FQUd_VA" target="_blank">with Abdellatif Dasser</a> (product rule)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/mbXvUT_25zM" target="_blank">with Abdellatif Dasser</a> (quotient rule)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/uHkXHy7nZQw" target="_blank">with Abdellatif Dasser</a> (combined rules)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/6E_LHW1tW4M" target="_blank">with Kurt Overhiser</a> (combined rules)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding derivatives using the limit definition
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/vuAdeALjbcQ" target="_blank">with Kurt Overhiser</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/msexvX2j0NE" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/0xUVp_JsIF0" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/slTaYPux-F4" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/q1pYKs4-ai8" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding limits 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/YBSbWPDll-s" target="_blank">with Paul Flores</a> (using graphs and tables) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/HO8UTB0VAkg">with Abdellatif Dasser</a> (using graphs)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/TR1fhpkbaEo">with Abdellatif Dasser</a> (simplifying)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/D9oBTTyMI2o" target="_blank">with Abdellatif Dasser</a> (fractions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/nyf8QQyzSaQ" target="_blank">with Abdellatif Dasser</a> (radicals)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/4JxepW3UNzw" target="_blank">with Abdellatif Dasser</a> (absolute value, ex. #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ZzrozQAwTKo" target="_blank">with Abdellatif Dasser</a> (absolute value, ex. #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding relative minimum or maximum
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_Ah1xPjw_wU" target="_blank">with Abdellatif Dasser</a> (1st derivative test)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/1CqQjx54elo" target="_blank">with Abdellatif Dasser</a> (2nd derivative test)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Horizontal asymptotes
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/0jCuigzARr4" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Implicit Differentiation
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/eiCjptWOYsA" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Indefinite integrals
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/tNzEteu95lo">with Abdellatif Dasser</a> (example #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/xbcaQmmK9ig">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/2SESi14vRBg">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5N-BU1Mi8l4">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/OSWXIjE1rYw" target="_blank">with Abdellatif Dasser</a> (using Riemann Sum, ex. #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dL7LMLtaDRU" target="_blank">with Abdellatif Dasser</a> (using Riemann Sum, ex. #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/-zmWJLo2tOQ" target="_blank">with Abdellatif Dasser</a> (using Riemann Sum, ex. #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Infinite limits
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/-GQsyLAI-EE" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/cH_4QEE8MWw" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Y6mBWRIYcgg" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wwy1gD_A22U" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wwKahPdKUBA" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/w4f0k2Xui4A" target="_blank">with Abdellatif Dasser</a> (example #6)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Instantaneous Rate of Change (1st and 2nd derivatives)
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/iiVETRagCA" target="_blank">with Jody DeVoe</a> (video #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/cJJMvDxsm" target="_blank">with Jody DeVoe</a> (video #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Max/Min Applications
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/ar1tCXDxzzw" target="_blank">with Jody DeVoe</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Related Rates 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/goceIkekHnI" target="_blank">with Kurt Overhiser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Review of Riemann Sum
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/2EdOn8-YChA" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/8sZrLqXfZps" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/AoT3zaExAmY" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>u-substitution
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-3/18905835/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #1) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-4/18906211/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #2) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-5/18906478/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #3) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-6/18906702/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #4) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-7/18906903/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #5) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-8/18907614/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #6) 
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-9/18908030/?ref=appemail" target="_blank">with Jody DeVoe</a> (example #7)
                                             </li>
                                             
                                             <li><a href="http://youtu.be/hQTztc2N18o" target="_blank">with Sidra Van De Car</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/f6Sx9xcsZvk" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wsCQ-qX2BOg" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W0MA-QkTEBk" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/mRSnDN5zuYw" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W6R2mOk1p7Q" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Volumes with Cylindrical shells
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/bxmruKsguv0" target="_blank">with Jolene Rhodes</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Volumes with Disks and Washers
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/MRb9A3thvVs" target="_blank">with Jolene Rhodes</a></li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/volumes-horizontal-discs/19261820/?ref=appemail" target="_blank">with Jody DeVoe</a> (horizontal slices)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/volumes-vertical-discs/19261523/?ref=appemail" target="_blank">with Jody DeVoe</a> (vertical slices)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/aTRYXzHSBBM" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tLAXIRxF8-8" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/FEXqUIxjrb0" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ScTz7c3VSzg" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/dQfcYXTAqwQ" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Vertical asymptotes
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/lYb-OfdQ5nc" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5LI4X_goZ2Q" target="_blank">with Abdellatif Dasser</a> (example #2) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Abdellatif Dasser</h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2313, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Playing soccer, swimming, and reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jody De Voe</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2313, MAP2302, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Kayaking, fishing, dogs, reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jim Franklin</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2311, 2312, 2313, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Kurt Overhiser</h3>
                                    
                                    <p><strong>Courses:</strong> MAC 1105, MAC 2311, MAC 2312, MHF 2300
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> traveling, snorkeling, eating out at new trendy restaurants
                                    </p>
                                    
                                    
                                    
                                    <h3>Jolene Rhodes</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1140, MAC2312, Intensive Algebra
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Painting, Bowling, Crafting
                                    </p>
                                    
                                    
                                    
                                    <h3>Sidra Van De Car</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2233, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC2312</title>                    
                           <div><a href="#">CALCULUS II  - MAC2312</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Application of integration to fluid pressure
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Area in polar coordinates
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul> 
                                          
                                       </li>
                                       
                                       <li>Comparison test
                                          
                                          <ul>
                                             
                                             <li>with teacher </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Conditional convergence vs absolute convergence
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Derivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ACfhKvSHDwU" target="_blank">with Abdellatif Dasser</a> (natural logarithmic functions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Kf0Xa_QqnHQ" target="_blank">with Abdellatif Dasser</a> (exponential functions)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/75a0XsgeVas" target="_blank">with Abdellatif Dasser</a> (logarithmic and exponential functions #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wrQuIbKUgP8" target="_blank">with Abdellatif Dasser</a> (logarithmic and exponential functions #2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Exponential functions
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/WGkdjuhWLCA" target="_blank">with Abdellatif Dasser</a> (domain)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/JNSPDqwEBsg" target="_blank">with Abdellatif Dasser</a> (limit, ex #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/VyoEtncs0lg" target="_blank">with Abdellatif Dasser</a> (limit, ex #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/MNfsEZ75OLU" target="_blank">with Abdellatif Dasser</a> (limit, ex #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/9XC_dufgK2E" target="_blank">with Abdellatif Dasser</a> (limit, ex #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/IRK3Ly8OZVs" target="_blank">with Abdellatif Dasser</a> (limit, ex #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding a formula for the inverse function
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Yee0ODzHBEM" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wQZs7aEGIQM" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/eKwYb_NL76E" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tFBF1qUJdgo" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/yJOSvVKg56g" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding a Maclaurin series for a function
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/UBgI7F1bidc" target="_blank">with Jolene Rhodes</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Integration with trignometric substitution
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/aUKq7lRCP-4" target="_blank">with Sidra Van De Car</a>   (using secant)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/iHUbRUnni-w" target="_blank">with Sidra Van De Car</a>   (using secant and completing the square)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/cKKH5OUz1tQ" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/p34aUf_1k_U" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/I2Tqen77wag" target="_blank">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             
                                             <li>
                                                <a href="http://youtu.be/2RW6wP32BHs" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ycieEg14Er8" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EwyeZR62XeU" target="_blank">with Abdellatif Dasser</a> (example #6)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/q7o5QTQHqmc" target="_blank">with Abdellatif Dasser</a> (example #7)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/SzdnmYJUwCg" target="_blank">with Abdellatif Dasser</a> (example #8)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/917A89YdSBM" target="_blank">with Abdellatif Dasser</a> (example #9)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Integration using Partial Fractions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/SWXlB7NpaSE" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/_1uK_OCxj58" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/bNnZNyDMysE" target="_blank">with Abdellatif Dasser</a> (example #3 part a)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/FifxxuSlaK4" target="_blank">with Abdellatif Dasser</a> (example #3 part b)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/rQ0pFhRtXA4" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/NEyi5GVbn3U" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Sequences vs series
                                          
                                          <ul>
                                             
                                             <li>with teacher </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>u-Substitution
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-3/18905835/?ref=appemail">with Jody DeVoe</a> (example #1)
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-4/18906211/?ref=appemail">with Jody DeVoe</a> (example #2)
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-5/18906478/?ref=appemail">with Jody DeVoe</a> (example #3)
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-6/18906702/?ref=appemail">with Jody DeVoe</a> (example #4)
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-7/18906903/?ref=appemail">with Jody DeVoe</a> (example #5)
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-8/18907614/?ref=appemail">with Jody DeVoe</a> (example #6)
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.educreations.com/lesson/view/u-sub-practice-9/18908030/?ref=appemail">with Jody DeVoe</a> (example #7)
                                             </li>
                                             
                                             <li><a href="http://youtu.be/hQTztc2N18o" target="_blank">with Sidra Van De Car</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/f6Sx9xcsZvk" target="_blank">with Abdellatif Dasser</a> (example #1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wsCQ-qX2BOg" target="_blank">with Abdellatif Dasser</a> (example #2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W0MA-QkTEBk">with Abdellatif Dasser</a> (example #3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/mRSnDN5zuYw" target="_blank">with Abdellatif Dasser</a> (example #4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W6R2mOk1p7Q" target="_blank">with Abdellatif Dasser</a> (example #5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Use power series to estimate an integral
                                          
                                          <ul>
                                             
                                             <li>with teacher </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Write a power series using combinations of basic series
                                          
                                          <ul>
                                             
                                             <li>with teacher </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Writing Taylor polynomials 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/8nMFZTc0Wqo" target="_blank">with Wakeel Zainulabdeen</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Abdellatif Dasser</h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2312, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Playing soccer, swimming, and reading
                                    </p>
                                    
                                    
                                    
                                    <h3>James Lang</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2312, 2313 MAP2302, STA2023, MAC1114, MGF1107
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Fishing, nerding, racquetball, basketball
                                    </p>
                                    
                                    
                                    
                                    <h3>Jim McCloskey</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1105, MAC1114, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Jolene Rhodes</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1140, MAC2312, Intensive Algebra
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Painting, Bowling, Crafting
                                    </p>
                                    
                                    
                                    
                                    <h3>Sidra Van De Car</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2233, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Wakeel Zainulabdeen</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2233, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAC2313</title>                    
                           <div><a href="#">CALCULUS III  - MAC2313</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Applications of triple integrals
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/RQHwY2oCi2I" target="_blank">with Jim Franklin</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Chain rule for functions of two variables w/ one independent variable
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/d4lNiZXSgSc" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/uQl-ykobJls" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Chain rule for functions of two variables w/ two independent variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ykKUTB-mwZo" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/xO6btCQn_l8" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Computing partial derivatives algebraically
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/86Dq03FdST4" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/b2TXJpQfpP4" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/IB5p4wTfXKg" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Continuous functions of two variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/UVYnvYCy9T8" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/oytMCMBOYF4" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Converting equations from rectangular to cylindrical form
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/MuDR5UW588Y" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/WCmjx01j7nk" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Converting equations from cylindrical to rectangular form
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/VEMwiRVwQew" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Converting equations from rectangular to spherical form
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/xJ0MOpv-9Bs" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/3hAq-yORsVU" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/drEFKedAoCQ" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/QSPTUDoXSdg" target="_blank">Abdellatif Dasser</a> (video 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/gx1niSB-uO0" target="_blank">Abdellatif Dasser</a> (video 5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Converting equations from spherical to rectangular
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/nkc3GftMdPo" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Differential of a function of two variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Pq3h6SpdXrg" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/yH7fVr2E874" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Directional derivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ZI_d4QdtlKM" target="_blank">Abdellatif Dasser</a> (find using the limit function)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/zN-JpgVNGx0" target="_blank">Abdellatif Dasser</a> (gradient vectors)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/rLjvsYsVRAM" target="_blank">Abdellatif Dasser</a> (estimating)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Domain of a function of two variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/0g1gQeXtZIg" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/2BaLRsNNbc0" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/HllwVbKiPsU" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Double integrals
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/DPo85nsd6lg" target="_blank">Abdellatif Dasser</a> (over a general region - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/cQzYtixQphc" target="_blank">Abdellatif Dasser</a> (over a general region - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/8muY25ZkCcM" target="_blank">Abdellatif Dasser</a> (reversing the order of integration
                                                
                                                - video 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/iMm1IfSE_Ho" target="_blank">Abdellatif Dasser</a> (reversing the order of integration
                                                
                                                - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/laGBy6M4fOw" target="_blank">Abdellatif Dasser</a> (reversing the order of integration
                                                
                                                - video 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/X8FMF7cRmFA" target="_blank">Abdellatif Dasser</a> (volume over a general region - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/NopBM6m8ivQ" target="_blank">Abdellatif Dasser</a> (volume over a general region - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/H-2ClxcnKlo" target="_blank">Abdellatif Dasser</a> (polar coordinates - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/HKbZsdhef44" target="_blank">Abdellatif Dasser</a> (polar coordinates - video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Equation of the tangent plane to a surface of a function of two variables
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/jFFD3lUKAds" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Estimating partial derivatives
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/xg3qZGaSwVo" target="_blank">Abdellatif Dasser</a> (using a contour diagram)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/f-UT5WOQ5Us" target="_blank">Abdellatif Dasser</a> (using the algebraic formula of a function)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Evaluating a line integral over a curve
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/z0qSxuVjNpM" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/yB5AYgB3CS0" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Evaluating the double integrals by converting to polar coordinates
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/uK4JUCsYWWY" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/ZO_GGe25RVg" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/DtaxfwadEbw" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/XXWHXyel2QA" target="_blank">Abdellatif Dasser</a> (video 4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Finding directions of maximum, minimum, and zero rate of change
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/dEfVkETNsos" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding the direction of steepest ascent
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/ikBEPyA6rdY" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Finding an equation of a plane
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ATgmwZeSx00" target="_blank">Abdellatif Dasser</a> (given a point and a normal vector)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/u3bd4s7l5sM" target="_blank">Abdellatif Dasser</a> (given three points)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Functions of two variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/o41vt_YgVu0" target="_blank">with Paul Flores</a> 
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/2mbPHydsGfE" target="_blank">with Paul Flores</a> (verifying solutions to Laplace’s Equation)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GnJ9kyk9X7o" target="_blank">Abdellatif Dasser</a> (max, min, &amp; saddle points - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/DWKCREOJmVw" target="_blank">Abdellatif Dasser</a> (max, min, &amp; saddle points - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/nXESGHEZVO4" target="_blank">Abdellatif Dasser</a> (absolute extrema - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/X8Px5VUW0xM" target="_blank">Abdellatif Dasser</a> (absolute extrema - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W9yjNxpIQag" target="_blank">Abdellatif Dasser</a> (absolute extrema - video 3)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/XeyFAoYXwCI" target="_blank">Abdellatif Dasser</a> (absolute extrema - video 4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Gradient vector and all of its properties 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/dIHlu8gcdIw" target="_blank">with Jim Franklin</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Graphing functions of two variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/ltgmLlunFgc" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/NQMO5zjJ77c" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/0X6cm7Bs37Y" target="_blank">Abdellatif Dasser</a> (sections of functions)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Level surfaces
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/GIXAHM488Gk" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/qRj22y4mD3I" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/US5YAWx9Ssc" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Limit of a function of two variables
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/xzuwwKfrXWw" target="_blank">with Paul Flores</a></li>
                                             
                                             <li>
                                                <a href="http://youtu.be/GVCtIxGyHuU" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/soERGHzh0TU" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/LlrZY0scCL8" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Linear approximation to a function of two variables
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/OKlLu-TcQeM" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Linear functions of two variables
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/jhNfnZZcYAM" target="_blank">Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Making contour diagrams 
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/sS1RNZy6dKA" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/xomxNqYPQDc" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/G_GFWbSqjBA" target="_blank">Abdellatif Dasser</a> (video 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EpcJbqQnnfw" target="_blank">Abdellatif Dasser</a> (video 4)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Parametric equations
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_SjFVpt9KA0" target="_blank">Abdellatif Dasser</a> (video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/CnN7Wn60T4k" target="_blank">Abdellatif Dasser</a> (video 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Spherical coordinates
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/x1NyPWUjF" target="_blank">with Jody DeVoe</a> (defining the coordinates)
                                             </li>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/NV2UiaV0m0p7" target="_blank">with Jody DeVoe</a> (using the spherical coordinates applet)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.math.uri.edu/~bkaskosz/flashmo/tools/sphcoords/" target="_blank">with Jody DeVoe</a> (spherical coordinates applet)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/FOjE1Ioftj0" target="_blank">with Jody DeVoe</a> (volume of a sphere)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EXW_ZDCdNLI" target="_blank">with Jody DeVoe</a> (volume of a snow cone)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/3l440S47uOg" target="_blank">with Jody DeVoe</a> (rectangular to spherical coordinates)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Triple integrals
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/AIb1TpPWogA" target="_blank">Abdellatif Dasser</a> (cylindrical coordinates
                                                
                                                - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/YJicRGdwqkI" target="_blank">Abdellatif Dasser</a> (cylindrical coordinates - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wUWH_6Jb4hw" target="_blank">Abdellatif Dasser</a> (cylindrical coordinates - video 3)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/iN4RvqOn6rA" target="_blank">Abdellatif Dasser</a> (cylindrical coordinates - video 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/zl1pHLkBK8s" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 1)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/lHlPaPZJ5aA" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/ciBkP-Z2o1Y" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 3)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/gvrMPBulc60" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/-485W6ClzAs" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 5)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/ThojEtFMRZg" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 6)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/FHmwxulcPnY" target="_blank">Abdellatif Dasser</a> (spherical coordinates - video 7)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Vectors
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/UzFVtA5tl3Y" target="_blank">Abdellatif Dasser</a> (dot product)
                                             </li>
                                             
                                             <li> <a href="http://youtu.be/K0TT4I2YhYE" target="_blank">Abdellatif Dasser</a> (cross product)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/pPdUnEZvslw" target="_blank">Abdellatif Dasser</a> (angle between two vectors)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/5-nVnAibfzA" target="_blank">Abdellatif Dasser</a> (resolving a vector into components)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/G4tI779rBOE" target="_blank">with Jim Franklin</a> (vector projections) 
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Jody DeVoe</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2313, MAP2302, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Kayaking, fishing, dogs, reading
                                    </p>
                                    
                                    
                                    
                                    <h3>Jim Franklin</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2311, 2312, 2313, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>MAP2302</title>                    
                           <div><a href="#">DIFFERENTIAL EQUATIONS  - MAP2302</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li>Applied first order
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Bernoulli differential equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/cWqxZURPHbA" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/j9zIrD5FmHg" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Cauchy-Euler equations
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/veTP21etWYY" target="_blank">with Abdellatif Dasser</a> (distinct real roots)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/DdJeLGkXFpE" target="_blank">with Abdellatif Dasser</a> (repeated real roots)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/1QzHt1p0JII" target="_blank">with Abdellatif Dasser</a> (conjugate complex roots)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/sb8gT5racvY" target="_blank">with Abdellatif Dasser</a> (3rd order equation)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/8YPkkXYdWCU" target="_blank">with Abdellatif Dasser</a> (variation of parameters)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Determining the form of <em>yp</em>                             
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/mh1PBmiJmpU" target="_blank">with Jim Franklin</a> (annihilator method)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Determine behavior of solution in 2nd order linear without solving 
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/9o0nvJrc2Lk" target="_blank">with Jim Franklin</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Determine if the differential equation is <u>exact</u>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/qd9nKkGoxuo" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/RwM1zrMejTo" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Determine if the differential equation is <u>homogenous</u>
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/LoTvTybaJYw" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/S5dWW68JU04" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Euler's method
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://screencast.com/t/MWM1OTIxM" target="_blank">with Jody DeVoe</a> (graphing calculator)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Existence and uniqueness theorem for first order IVPs
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/aR3AMffsPCI" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Homogeneous linear equations with constant coefficients
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/FLR3pYEBCmg" target="_blank">with Abdellatif Dasser</a> (2nd order, distinct real roots)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/0cCv21N-w1M" target="_blank">with Abdellatif Dasser</a> (2nd order, repeated real roots)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/tkXnNqcm5f4" target="_blank">with Abdellatif Dasser</a> (2nd order, conjugate roots)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EPhnjrkUDtM" target="_blank">with Abdellatif Dasser</a> (3rd order, ex. 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Yl5JTcJqqvI" target="_blank">with Abdellatif Dasser</a> (3rd order, ex. 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/78PTt-iyj20" target="_blank">with Abdellatif Dasser</a> (4th order, ex. 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/3M7E3hNuF7I" target="_blank">with Abdellatif Dasser</a> (4th order, ex. 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Indefinite integrals
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/IyWuXMAfmSE" target="_blank">with Abdellatif Dasser</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Inverse Laplace resulting in unit step functions
                                          
                                          <ul>
                                             
                                             <li>with Sidra Van De Car</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Laplace of unit step functions 
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Linear dependence using Wronskian
                                          
                                          <ul>
                                             
                                             <li> <a href="http://youtu.be/qvTi28mxKqU" target="_blank">with Abdellatif Dasser</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Linear dependence/independence
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/p0otbuTxRjo" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/y70mW1mH5uc" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/CV0_93xGpD4" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Mixture problems
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/mUtuPDBCjNs" target="_blank">with Jim Franklin</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Partial fractions
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=T64XVCfUirA" target="_blank">with Abdellatif Dasser</a> (linear factors)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=fbTe4YU_XJ0" target="_blank">with Abdellatif Dasser</a> (repeated linear factors)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=kQcuzRbc9Qk" target="_blank">with Abdellatif Dasser</a> (multiple linear factors)
                                             </li>
                                             
                                             <li>
                                                <a href="http://www.youtube.com/watch?v=usFT1soxtdk" target="_blank">with Abdellatif Dasser</a> (quadratic factors)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Phase line diagrams given a sketch of y vs dy/dx
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Reduction of order differential equations
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/F9I5GQUb_UA" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/6qvz3Ja-s_0" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Reduction to separation of variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/_Z2y3wQjSto" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/8jucshGhq-4" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/D-1lh90S9Uk" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Set up DE from a verbal statement  
                                          
                                          <ul>
                                             
                                             <li>with teacher</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving differential equations by separation of variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/qYKegNoxgGk" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/sA08KEdZPAA" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/l25kt7DZW6k" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving differential equations using undetermined coefficients method: (annihilator
                                          method)
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/-QYwnMpkSTc" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/L8fkL4itLz4" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving differential equations using variation of parameters
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/Bnb-uPDglQM" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/eGfN4IGShDQ" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Solving initial value problems by separation of variables
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/OSIyruBH_KE" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/MH8gDZzyObQ" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/SHWwmRCZbps" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/3o032em66oU" target="_blank">with Abdellatif Dasser</a> (example 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/UNq3Y3dMSGY" target="_blank">with Abdellatif Dasser</a> (example 5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving initial value problems using Laplace transforms
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/asTGRsyqTyU" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/Wd6U9tTFGOU" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving non homogeneous differential equations using the undetermined coefficients
                                          method
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/owi2Hf4pi70" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/96JBUkX7pYw" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/UdZoEhxtl7M" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/EtF_PkpB9-E" target="_blank">with Abdellatif Dasser</a> (example 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/qdtas-dtfeU" target="_blank">with Abdellatif Dasser</a> (example 5)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/p6oNh_h88GY" target="_blank">with Abdellatif Dasser</a> (example 6)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> Solving the first order differential equations using the integrating factor
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/oWfYzYAN6w0" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/rR7YwzJqQ8A" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/_5LBCAQkq3E" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li> u-substitution for integrals
                                          <ul>
                                             
                                             <li>
                                                <a href="http://youtu.be/f6Sx9xcsZvk" target="_blank">with Abdellatif Dasser</a> (example 1)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/wsCQ-qX2BOg" target="_blank">with Abdellatif Dasser</a> (example 2)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W0MA-QkTEBk" target="_blank">with Abdellatif Dasser</a> (example 3)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/mRSnDN5zuYw" target="_blank">with Abdellatif Dasser</a> (example 4)
                                             </li>
                                             
                                             <li>
                                                <a href="http://youtu.be/W6R2mOk1p7Q" target="_blank">with Abdellatif Dasser</a> (example 5)
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li>Writing a piece-wise defined function with unit step functions
                                          
                                          <ul>
                                             
                                             <li><a href="http://youtu.be/WEYSqD_C9Cw" target="_blank">with Sidra Van De Car</a></li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <h3>Jim Franklin</h3>
                                    
                                    <p><strong>Courses:</strong> MAC1114, MAC2311, 2312, 2313, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>James Lang</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2312, 2313 MAP2302, STA2023, MAC1114, MGF1107
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Fishing, nerding, racquetball, basketball
                                    </p>
                                    
                                    
                                    
                                    <h3>Sidra Van De Car</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2233, MAC2311, 2312, MAP2302
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    
                                    
                                    <h3>Abdellatif Dasser </h3>
                                    
                                    <p><strong>Courses:</strong> MAP2302, MAC 2311,2312,2313, MAC2233, MAC 1105
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong>Playing soccer, swimming, and reading
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <title>Graphing Calculator</title>
                           
                           <div><a href="#">GRAPHING CALCULATOR VIDEOS ON VARIOUS TOPICS</a></div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <ul>
                                       
                                       <li><strong><u>Statistics</u></strong></li>
                                       
                                       <ul>
                                          
                                          <li> Entering data into the calculator
                                             
                                             <ul>
                                                
                                                <li><a href="http://screencast.com/t/rAMQ6vbpYuam" target="_blank">with Jody DeVoe</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li>Histograms
                                             
                                             <ul>
                                                
                                                <li><a href="http://screencast.com/t/nJ6fDA4Z" target="_blank">with Jody DeVoe</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li> Finding <em>mean, sd</em>, and <em>5-number summary</em>
                                             
                                             <ul>
                                                
                                                <li><a href="http://screencast.com/t/mg5LqHxHN6" target="_blank">with Jody DeVoe</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li>Regular Boxplot
                                             
                                             <ul>
                                                
                                                <li><a href="http://screencast.com/t/uPkWgtd2wb" target="_blank">with Jody DeVoe</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li>Modified Boxplot
                                             
                                             <ul>
                                                
                                                <li>
                                                   <a href="http://screencast.com/t/YU0uSmZhLz" target="_blank">with Jody DeVoe</a> 
                                                </li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li> Entering 2-variable data/scatter plot/least squares regression line/correlation coefficient
                                             
                                             <ul>
                                                
                                                <li> <a href="http://screencast.com/t/b47bEmVCE">with Jody DeVoe</a> 
                                                </li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <li><strong><u>Algebra</u></strong></li>
                                       
                                       <ul>
                                          
                                          <li>Linear Regression
                                             
                                             <ul>
                                                
                                                <li><a href="http://www.youtube.com/watch?v=NqZuZbQihqk" target="_blank">with Lisa Potchen</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li>Graphing windows
                                             
                                             <ul>
                                                
                                                <li>
                                                   <a href="http://screencast.com/t/hv3RfDx8" target="_blank">with Jody DeVoe</a> (cubic functions)
                                                </li>
                                                
                                                <li>
                                                   <a href="http://screencast.com/t/BFIpWDZf5k" target="_blank">with Jody DeVoe</a> (applications)
                                                </li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                       <li> <u><strong>Differential Equations</strong></u>
                                          
                                          <ul>
                                             
                                             <li>Euler's Method
                                                
                                                <ul>
                                                   
                                                   <li><a href="http://screencast.com/t/MWM1OTIxM" target="_blank">with Jody DeVoe</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                       <li><u><strong><a href="courses/CalculatorLinks.html" target="_blank">Various Calculator Help Links (external websites)</a></strong></u></li>
                                       
                                       <ul>
                                          
                                          
                                          <li>Videos by Cathy Ferrer</li>
                                          
                                          <ul>
                                             
                                             <li><a href="https://youtu.be/SqC9qxBaWlA" target="_blank">Making a table for a function</a></li>
                                             
                                             <li><a href="https://youtu.be/vSAc0ynz71k" target="_blank">Creating a Scatterplot on  the Calculator</a></li>
                                             
                                             <li><a href="https://youtu.be/hjFPvDcsAF4" target="_blank">Numerical and Graphical  Representations of Linear Functions</a></li>
                                             
                                             <li><a href="https://youtu.be/gpraetoPhdk" target="_blank">Solving a linear equation  graphically</a></li>
                                             
                                             <li><a href="https://youtu.be/u2EweIsPPL8" target="_blank">Solving Linear Inequalities  Graphically</a></li>
                                             
                                             <li><a href="https://youtu.be/2FeD8SFSr3s" target="_blank">Graphical Solution of Systems of  Equations</a></li>
                                             
                                             <li><a href="https://youtu.be/5RpMxjR7PB4" target="_blank">Solving a System of Linear  Inequalities with Technology</a></li>
                                             
                                             <li><a href="https://youtu.be/6Hvfw_74SBc" target="_blank">Solving Quadratic Equations  Graphically</a></li>
                                             
                                             <li><a href="https://youtu.be/iHakh8UV63Q" target="_blank">Applications of Quadratic  Equations Geometry</a></li>
                                             
                                             <li><a href="https://youtu.be/5tO9GOplAQQ" target="_blank">Applications of Quadratic  Equations Speed</a></li>
                                             
                                             <li><a href="https://youtu.be/LOA5m9j274g" target="_blank">Applications of Quadratic  Equations Box Dimensions</a></li>
                                             
                                             <li><a href="https://youtu.be/QU04nFgHbVQ" target="_blank">Solving Rational Equations  Graphically</a></li>
                                             
                                             <li><a href="https://youtu.be/MDIIoHTKAlo" target="_blank">Applications of Rational  Functions</a></li>
                                             
                                             <li><a href="https://youtu.be/u6PewE_nNF4" target="_blank">Solving Rational Equations  Graphically</a></li>
                                             
                                             <li><a href="https://youtu.be/_Zh7DULPKzw" target="_blank">Solving  Rational Equations Wind Speed</a></li>
                                             
                                             <li><a href="https://youtu.be/K3cJ_H4ltuA" target="_blank">Solving Rational Equations Speed</a></li>
                                             
                                             <li><a href="https://youtu.be/DfAUCQCI_aE" target="_blank">Solving an Equations with  Rational Exponents Graphically</a></li>
                                             
                                             <li><a href="https://youtu.be/36lAsV53qKg" target="_blank">Solving Quadratic Equations  Graphically</a></li>
                                             
                                          </ul>
                                          
                                       </ul>
                                       
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <h3>Jody DeVoe</h3>
                                    
                                    <p><strong>Courses:</strong> MAC2311,2313, MAP2302, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies: </strong>Kayaking, fishing, dogs, reading
                                    </p>
                                    
                                    
                                    
                                    
                                    <h3>Joel Berman</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0018, MAT0028, MAT1033
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> Golf, Knitting, Motorcycling 
                                    </p>
                                    
                                    
                                    
                                    
                                    <h3>Lisa Potchen</h3>
                                    
                                    <p><strong>Courses:</strong> MAT0028, MAT1033, MAC1105, MAC1114, MGF1106
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> 
                                    </p>
                                    
                                    
                                    
                                    <h3>Cathy Ferrer</h3>
                                    
                                    <p><strong>Courses:</strong> MAT1033, MAC 1105, MAC1140, MAC1114, MAC2311,2312, MGF1106,1107, STA2023
                                    </p>
                                    
                                    <p><strong>Hobbies:</strong> pottery, painting, card making, orchids, bowling
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>  
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/math/livescribe.pcf">©</a>
      </div>
   </body>
</html>