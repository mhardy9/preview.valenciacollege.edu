<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Policies  | Valencia College</title>
      <meta name="Description" content="Information on the rules, policies, and procedures of the West Campus Math Center and its labs.">
      <meta name="Keywords" content="policies, rules, math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/math/west/policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/math/">Math</a></li>
               <li><a href="/students/math/west/">West</a></li>
               <li>Policies </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     
                     <h2>Policies</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Math Center Policies</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <ul class="list_style_1">
                           
                           <li>The Math Open Lab, Math Connections Lab and Hands-On Learning Lab serve students enrolled
                              in MAT
                              0018C, MAT 0022C, MAT 0028C, MAT 1033C and STA 1001C only.
                              
                           </li>
                           
                           <li>Cell phones must be set to silent or vibrate and all personal electronic devices must
                              be put away at
                              all times.
                              
                           </li>
                           
                           <li>Calculator use permitted for MAT 1033C students only. Cell phones are not permitted
                              to be used as
                              calculators.
                              
                           </li>
                           
                           <li>Food and drinks are not permitted in the Math Open Lab.</li>
                           
                           <li>Math Center is a child free zone.</li>
                           
                           <li>Valid physical photo identification card is required for all password protected quizzes
                              and
                              assessments in the Math Open Lab.
                              
                           </li>
                           
                           <li>
                              <strong>Absolutely no assistance</strong> is allowed on assessments and quizzes in the Math Open Lab.
                              That means no notes, no notebooks, no textbooks, no calculators, no personal electronic
                              devices, or any
                              other assistance from anyone or anything on quizzes.
                              
                           </li>
                           
                           <li>Academic dishonest/cheating of any sort may result in a grade of F on the assignment
                              and referral of
                              the incident to professor.
                              
                           </li>
                           
                           <li>Loose sheets of paper may be used for scratch work during quizzes.</li>
                           
                           <li>Student computers are monitored to make sure students adhere to all Math Open Lab
                              policies and
                              procedures.
                              
                           </li>
                           
                           <li>Students must check with a lab instructor for appropriate seating area in the Math
                              Open Lab.
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/math/west/policies.pcf">©</a>
      </div>
   </body>
</html>