<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Group Study Rooms  | Valencia College</title>
      <meta name="Description" content="Study rooms are available at the Math Center for small groups of students to reserve.">
      <meta name="Keywords" content="group, study, room, reservation, math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/math/west/group-study.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/math/">Math</a></li>
               <li><a href="/students/math/west/">West</a></li>
               <li>Group Study Rooms </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     
                     <h2>Group Study Rooms</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Room Reservations</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>How To Reserve A Room</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Students must visit the front desk in the Math Center 7-240 to make a reservation
                              for a group study
                              room. Reservations are made on a first come first served basis.
                              
                           </li>
                           
                           <li>Faculty and Staff may visit the front desk in the Math Center 7-240 to make a reservation
                              for a group
                              study room.
                              
                           </li>
                           
                        </ul>
                        
                        
                        <h4>Room Reservation Rules</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Only reserved to students, staff, and faculty members.</li>
                           
                           <li>Valencia ID required for students.</li>
                           
                           <li>Faculty and Staff may book a room for an entire semester when reoccurring appointments
                              are needed (SL
                              Leaders, Group Tutoring, OSD Tutoring).
                              
                           </li>
                           
                           <li>Students may only book up to 2 weeks in advance.</li>
                           
                           <li>A maximum of 2 hours of reservation time available per reservation, unless special
                              permission granted
                              by manager.
                              
                           </li>
                           
                           <li>Rooms must be booked to use, even if not currently occupied. Students, Staff, or Faculty
                              need to see
                              front desk staff to book a room.
                              
                           </li>
                           
                           <li>The person reserving the room is responsible for any damages.</li>
                           
                           <li>Rooms empty for 15 minutes after the reservation time will be open for reservations
                              to other students,
                              staff, or faculty.
                              
                           </li>
                           
                           <li>After 3 No Shows students, staff, or faculty member will need to see manager for approval
                              to book
                              rooms.
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Available Rooms</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <table class="table table table-striped">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th>Room</th>
                                 
                                 <th>Capacity</th>
                                 
                                 <th>Equipment</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>7-240 A Study Room</td>
                                 
                                 <td>8</td>
                                 
                                 <td>Whiteboard</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>7-240 C Study Room</td>
                                 
                                 <td>8</td>
                                 
                                 <td>Whiteboard</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>7-240 E Study Room</td>
                                 
                                 <td>8</td>
                                 
                                 <td>Whiteboard</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>7-240 H Study Room</td>
                                 
                                 <td>8</td>
                                 
                                 <td>Whiteboard</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>7-240 I Study Room</td>
                                 
                                 <td>8</td>
                                 
                                 <td>Whiteboard</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/math/west/group-study.pcf">©</a>
      </div>
   </body>
</html>