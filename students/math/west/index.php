<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Math Center  | Valencia College</title>
      <meta name="Description" content="The West Campus Math Center (7-240) provides learning support services for all levels of mathematics. Students seeking to improve their skills in mathematics have access to a variety of resources within the Math Center, including interactive software, calculators for check-out, instructional videos, and knowledgeable staff.">
      <meta name="Keywords" content="math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/math/west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/math/">Math</a></li>
               <li>West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <h2>Math Center</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>About The Math Center</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>The West Campus Math Center (7-240) provides learning support services for all levels
                           of mathematics.
                           Students seeking to improve their skills in mathematics have access to a variety of
                           resources within the
                           Math Center, including interactive software, calculators for check-out, instructional
                           videos, and
                           knowledgeable staff.
                        </p>
                        
                        
                        <p>The Math Center houses:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>the <a href="/students/tutoring/west/">Tutoring Center</a>, where walk-in tutoring is
                              available for mathematics and appointments are made on an as-available basis for one-on-one
                              tutoring for
                              a variety of other subjects
                              
                           </li>
                           
                           <li>the <a href="open-lab.html">Math Open Lab</a>, where students complete their required labs for
                              Developmental Math I, Developmental Math II, Developmental Math Combined, and Intermediate
                              Algebra
                              
                           </li>
                           
                           <li>the <a href="connections.html">Math Connections</a>, a learning community for students of
                              Developmental Math I, Developmental Math II, Developmental Math Combined, and Intermediate
                              Algebra
                              
                           </li>
                           
                           <li>
                              <a href="pert.html">PERT review sessions</a>, conducted with assistance from lab instructors
                           </li>
                           
                           <li>a <a href="hands-on-math.html">Hands-on Math</a> classroom for active learning of math skills and
                              concepts
                              
                           </li>
                           
                           <li>group study rooms, available on an as-available basis to all students, staff, and
                              faculty members
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Hours &amp; Location</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p><strong>Location</strong>: Building 7, Room 241
                        </p>
                        
                        
                        <h4>Summer 2017 Math Open Lab Hours of Operation</h4>
                        
                        <ul class="list-unstyled">
                           
                           <li>
                              <strong>Monday to Thursday</strong>: 9 am to 7 pm
                           </li>
                           
                           <li>
                              <strong>Friday</strong>: 9am to 12pm
                           </li>
                           
                           <li>
                              <strong>Saturday</strong>: 10 am to 3 pm
                           </li>
                           
                           <li>
                              <strong>Questions</strong>: Call 407-582-1720 or 407-582-1780
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/math/west/index.pcf">©</a>
      </div>
   </body>
</html>