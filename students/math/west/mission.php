<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Mission &amp; Staff  | Valencia College</title>
      <meta name="Description" content="&amp;amp;lt;li&amp;amp;gt;The Math Center will provide a welcoming, inviting, energizing space to create a learning community in mathematics. The Math Center will accommodate all learning styles. The design team for the Math Center Initiative will collaborate with all stakeholders (math department students, faculty, staff, and administration).">
      <meta name="Keywords" content="mission, staff, math, center, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/math/west/mission.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/math/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/math/">Math</a></li>
               <li><a href="/students/math/west/">West</a></li>
               <li>Mission &amp; Staff </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-12">
                     
                     <h2>Mission &amp; Staff</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Mission Statement</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Support, enhance, and celebrate teaching and learning in Mathematics.</p>
                        
                        
                        <h4>Design Principles</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>The Math Center will provide a welcoming, inviting, energizing space to create a learning
                              community in
                              mathematics.
                              
                           </li>
                           
                           <li>The Math Center will accommodate all learning styles.</li>
                           
                           <li>The design team for the Math Center Initiative will collaborate with all stakeholders
                              (math department
                              students, faculty, staff, and administration).
                              
                           </li>
                           
                           <li>The Math Center will use the most effective technology available, using standard interfaces
                              when
                              possible.
                              
                           </li>
                           
                           <li>The Math Center’s learning space and resources will allow for flexibility and adaptability.</li>
                           
                           <li>All stakeholders will be aware of the mission, resources, and operations of the Math
                              Center.
                           </li>
                           
                           <li>The Math Center will be staffed by trained individuals with appropriate expertise.</li>
                           
                           <li>The Math Center will make effective use of available resources.</li>
                           
                           <li>The Math Center’s resources, operations and effectiveness in accomplishing its mission
                              will be
                              assessed annually.
                              
                           </li>
                           
                           <li>The Math Center’s layout will conform to all college safety and access standards.</li>
                           
                        </ul>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Meet the Team</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <h4>Instructional Lab Supervisors</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Aditi Batra, BS</li>
                           
                           <li>Nicolas Navarro, MBA</li>
                           
                           <li>Courtney Watson, MA Ed</li>
                           
                        </ul>
                        
                        
                        <h4>Instructional Lab Assistant, Sr.</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Ann Gabelle, BS (Adjunct Professor)</li>
                           
                           <li>Denise Breit, BS</li>
                           
                           <li>Fara Fridoonfar, MS</li>
                           
                           <li>James Ellerbrock, MS (Adjunct Professor)</li>
                           
                           <li>Jeanette Blagg, MBA</li>
                           
                           <li>Keith Noone, BS (Adjunct Professor)</li>
                           
                           <li>Krystal Perea, BS</li>
                           
                           <li>Kim Ziegler, BS (Adjunct Professor)</li>
                           
                           <li>Lee Matheny, BS</li>
                           
                           <li>Lisa Martin, BS (Adjunct Professor)</li>
                           
                           <li>Mark Nasser, BS</li>
                           
                           <li>Marvin Campbell, BS (Adjunct Professor)</li>
                           
                           <li>Mona Moorjani, BS (Adjunct Professor)</li>
                           
                           <li>Oscar Cartaya-Sainz, BS</li>
                           
                           <li>Narjes Ammar, BS (Adjunct Professor)</li>
                           
                           <li>Ray Tripp, MA (Adjunct Professor)</li>
                           
                           <li>Robert Crabbs, MA</li>
                           
                           <li>Rudrani Sewsankar, MS (Adjunct Professor)</li>
                           
                           <li>Sergio Torres, BS</li>
                           
                           <li>Deon Muschette, MA</li>
                           
                        </ul>
                        
                        
                        <h4>Instructional Lab Assistant</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>Rene Rodriguez</li>
                           
                           <li>Bob Arciero, BS</li>
                           
                           <li>Shannon Cook, BS</li>
                           
                           <li>Win Peregrino, MA</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/math/west/mission.pcf">©</a>
      </div>
   </body>
</html>