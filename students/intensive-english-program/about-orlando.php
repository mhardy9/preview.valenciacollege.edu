<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/about-orlando.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Intensive English Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li>Intensive English Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>About Orlando, Florida USA</h2>
                        
                        
                        
                        <p>Ever changing and growing, Orlando possesses many diverse features that seamlessly
                           mesh in one ideal destination. Theme parks and first-rate tourist attractions stand
                           alongside a wide array of cultural venues, promising something for everyone.   Orlando
                           is known as "The City Beautiful." Lush landscaping and pristine natural areas are
                           accompanied by more than 300 lakes and rivers, wildlife-filled preserves and parks,
                           and well-manicured public spaces and gardens. Dining options are plentiful as well,
                           and restaurants range from casual to upscale, economical to pricey, and fast food
                           to gourmet.
                        </p>
                        
                        
                        
                        
                        <div>
                           
                           
                           <h3>Location</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Orange County is the region's most populous county and includes the city of Orlando
                                    as well as 12 other incorporated cities. The county sits in the approximate center
                                    of the state, midway between Jacksonville and Miami. The St. Petersburg-Tampa area
                                    on the Gulf of Mexico and Daytona Beach on the Atlantic Ocean are easy drives. Two
                                    of the state's major highways, Interstate 4 (east-west) and the Florida Turnpike (north-south),
                                    intersect just outside Orlando.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Education and Business</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Orange County is home to the second largest university in the nation (one of the top
                                    research universities), one of the top ten research parks in the country, one of only
                                    two National Entrepreneur Centers, a world-class international airport, and an all
                                    new Medical City which includes the UCF College of Medicine that opened in 2009. Metro
                                    Orlando has become a hub for corporate divisions, association and U.S. operations
                                    headquarters, and has a strong international presence. In addition to Mitsubishi and
                                    Siemens divisions, there are over 120 other foreign-based companies in Orlando.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Demographics</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The greater Orlando area has a population of just over two million residents. Two-thirds
                                    of our residents are under the age of 44, and nearly one- third have obtained a college
                                    degree. People from all backgrounds and walks of life call Orlando home.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Weather</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The weather in Orlando can be cold in the winter (low 30 degrees) to hot in the summer
                                    (high 95 degrees).  The coldest months are usually December through February, but
                                    temperatures average about 50 degrees.  Humidity can still be high in the winter months,
                                    but rainfall is at a minimum.  The summer brings lots of rain, with frequent afternoon
                                    thunderstorms.  Weather can be very unpredictable during the summer, so always carry
                                    an umbrella!  Ironically, you should also always carry a sweater or jacket because
                                    classrooms can be very cold due to the air conditioning.
                                 </p>
                                 
                                 <p>June through December is the official hurricane season, but September is the peak
                                    time for <a href="http://www.nhc.noaa.gov/" target="_blank" title="Hurricanes in Florida">hurricanes in Florida</a>. While they do not hit every year, you should be aware that there is always the possibility
                                    of a hurricane and know how to prepare.
                                 </p>
                                 
                                 <p>Another thing to keep in mind is allergy season.  Pollen counts can get very high
                                    in Orlando, so watch the <a href="http://www.weather.com/outlook/health/allergies/weather/32803" target="_blank" title="Allergy Alerts">Allergy Alerts</a> if needed.
                                 </p>
                                 
                                 
                                 <div>
                                    
                                    <h3>Current Weather Forcast in Orlando, FL</h3>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3"> 
                        
                        <h3>Intensive English Program</h3>
                        
                        <p>Now offered at two locations: West Campus &amp; Osceola Campus</p>
                        
                        <hr>
                        <a href="apply.html" target="_blank" title="How to Apply">
                           
                           <div class="button-action">How To Apply</div>
                           </a>
                        
                        <div>
                           
                           <h3>START DATES</h3>
                           
                           <p>Classes start monthly.</p>
                           <a href="../iep-dates-and-deadlines.html" title="Steps to Register">
                              
                              <div class="button-action_outline">View all start dates and holidays.</div>
                              </a>
                           
                        </div>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">
                           
                           <div class="button-action_outline">CONTACT US</div>
                           </a> 
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/about-orlando.pcf">©</a>
      </div>
   </body>
</html>