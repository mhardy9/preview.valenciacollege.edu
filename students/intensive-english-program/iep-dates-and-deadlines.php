<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Programs | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep-dates-and-deadlines.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Intensive English Programs</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li>Intensive English Programs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        <h2>Dates and Deadlines</h2>
                        
                        <div>
                           
                           <div>
                              
                              <h3>2017 DATES</h3>
                              
                              <h4><strong>September - December 2017</strong></h4>
                              
                              <table class="table table">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <th scope="col">REGISTRATION DEADLINE</th>
                                       
                                       <th scope="col">PLACEMENT TEST</th>
                                       
                                       <th scope="col">SESSION START DATE</th>
                                       
                                       <th scope="col">SESSION END DATE</th>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4&amp;gt;">Labor Day Break: Monday, September 4 - Friday, September 8 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, August 23</td>
                                       
                                       <td>Tuesday, September 5 at 8:00 a.m.</td>
                                       
                                       <td><strong>Tuesday, September 12*</strong></td>
                                       
                                       <td>Thursday, October 5</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, September 27</td>
                                       
                                       <td>Monday, October 2 at 1:00 p.m.</td>
                                       
                                       <td><strong>Tuesday, October 10*</strong></td>
                                       
                                       <td>Thursday, November 2</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, October 25</td>
                                       
                                       <td>Monday, October 30 at 1:00 p.m.</td>
                                       
                                       <td><strong>Tuesday, November 7*</strong></td>
                                       
                                       <td>Thursday, December 7</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4">Thanksgiving Break: Monday, November 20 - Friday, November 24, 2017<br>
                                          IEP Winter Break: Friday, December 8, 2017 - Friday, January 5, 2018
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              
                              <div><a href="teen-program/iep-dates-and-deadlines-for-teens.cfm/index.html">Click here for ESL for Teens Dates</a></div>
                              
                           </div>
                           
                           <div>
                              
                              <h3>2018 DATES</h3>
                              
                              <h4><strong>January - December 2018</strong></h4>
                              
                              <table class="table table">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <th scope="col"><strong>Registration Deadline </strong></th>
                                       
                                       <th scope="col"><strong>Placement Test</strong></th>
                                       
                                       <th scope="col"><strong>Session Start Date </strong></th>
                                       
                                       <th scope="col"><strong>Session End Date </strong></th>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, December 13 </td>
                                       
                                       <td>Wednesday, January 3 </td>
                                       
                                       <td>Tuesday, January 9 </td>
                                       
                                       <td>Friday, February 2 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4"><strong>Monday, January 15: Martin    Luther King, Jr. Holiday </strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, January 17 </td>
                                       
                                       <td>Monday, January 29 </td>
                                       
                                       <td>Tuesday, February 6 </td>
                                       
                                       <td>Friday, March 2 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4"><strong>Friday, February 9:    Valencia College Learning Day (No Classes) </strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, February 14 </td>
                                       
                                       <td>Monday, February 26 </td>
                                       
                                       <td>Tuesday, March 6 </td>
                                       
                                       <td>Thursday, April 5 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4"><strong>Monday, March 12 to    Friday, March 16: Spring Break </strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, March 21 </td>
                                       
                                       <td>Monday, April 2 </td>
                                       
                                       <td>Tuesday, April 10 </td>
                                       
                                       <td>Thursday, May 3 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, April 11 </td>
                                       
                                       <td>Monday, April 30 </td>
                                       
                                       <td>Tuesday, May 15 </td>
                                       
                                       <td>Friday, June 8 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4">
                                          <strong>Monday, May 28: Memorial    Day Holiday </strong> <br>
                                          <strong>Monday, May 7 to Friday,    May 11: Summer Break </strong>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, May 23 </td>
                                       
                                       <td>Monday, June 4 </td>
                                       
                                       <td>Tuesday, June 12 </td>
                                       
                                       <td>Friday, July 6 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4"><strong>Wednesday, July 4:    Independence Day Holiday </strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, June 20 </td>
                                       
                                       <td>Monday, July 2 </td>
                                       
                                       <td>Tuesday, July 10 </td>
                                       
                                       <td>Thursday, August 2 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, July 11 </td>
                                       
                                       <td>Monday, July 30 </td>
                                       
                                       <td>Tuesday, August 7 </td>
                                       
                                       <td>Thursday, August 30 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4"><strong>Monday, September 3 to    Friday, September 7: Labor Day Holiday &amp; Fall Break </strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, August 22 </td>
                                       
                                       <td>Monday, August 27 </td>
                                       
                                       <td>Tuesday, September 11 </td>
                                       
                                       <td>Thursday, October 4 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, September 26 </td>
                                       
                                       <td>Monday, October 1 </td>
                                       
                                       <td>Tuesday, October 9 </td>
                                       
                                       <td>Thursday, November 1 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Wednesday, October 24 </td>
                                       
                                       <td>Monday, October 29 </td>
                                       
                                       <td>Tuesday, November 6 </td>
                                       
                                       <td>Thursday, December 6 </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td colspan="4">
                                          <strong>Monday, November 19 to    Friday, November 23 Thanksgiving Break </strong> <br>
                                          <strong>Friday, December 7 to    Friday, January 7: Winter Holiday </strong>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              
                              <div><a href="teen-program/iep-dates-and-deadlines-for-teens.cfm/index.html">Click here for ESL for Teens Dates</a></div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Intensive English Program</h3>
                        
                        <p>Now offered at two locations: West Campus &amp; Osceola Campus</p>
                        
                        <hr>
                        <a href="/STUDENTS/international/intensive-english-program/iep/apply.html" target="_blank" title="How to Apply">
                           
                           <div class="button-action">How To Apply</div>
                           </a>
                        
                        <div>
                           
                           <h3>START DATES</h3>
                           
                           <p>Classes start monthly.</p>
                           <a href="/STUDENTS/international/intensive-english-program/iep-dates-and-deadlines.html" title="Steps to Register">
                              
                              <div class="button-action_outline">View all start dates and holidays.</div>
                              </a>
                           
                        </div>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">
                           
                           <div class="button-action_outline">CONTACT US</div>
                           </a> 
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep-dates-and-deadlines.pcf">©</a>
      </div>
   </body>
</html>