<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Educational Partners | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/educational-partners/partners.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Educational Partners</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/educational-partners/">Educational Partners</a></li>
               <li>Educational Partners</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        
                        <h2>Directory of Partners</h2>
                        
                        
                        <div>
                           
                           <h3>Brazil</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       <a href="http://www.travelintercambio.com.br/" target="_blank">Learning &amp; Fun</a> 
                                    </li>
                                    
                                    <li><a href="http://www.calypsoviagens.com.br" target="_blank">Calypso Viagens</a></li>
                                    
                                    <li><a href="http://www.cogintercambio.com.br/" target="_blank">COG Intercambio</a></li>
                                    
                                    <li>
                                       <a href="http://www.cia-agency.com/" target="_blank">Cultural Interchange Agency</a> 
                                    </li>
                                    
                                    <li><a href="http://www.dreamsintercambios.com.br/" target="_blank">Dreams Intercambios</a></li>
                                    
                                    <li><a href="http://www.flowamerica.com/" target="_blank">Flow America</a></li>
                                    
                                    <li><a href="http://www.accesschool.com/site/" target="_blank">Access International </a></li>
                                    
                                    <li><a href="http://www.braunturismo.com.br/portal/" target="_blank">Braun Turismo</a></li>
                                    
                                    <li><a href="http://ibl-idiomas.com.br/" target="_blank">Instituto Brasileiro de LÃ­nguas</a></li>
                                    
                                    <li><a href="http://www.travelmate.com.br/" target="_blank">TravelMate</a></li>
                                    
                                    <li>
                                       <a href="http://www.niceviaapia.com.br" target="_blank">Nice Via Apia Tourismo</a> 
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Colombia</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.aeaexplorer.com/" target="_blank">American Explorer Academy</a></li>
                                    
                                    <li><a href="http://kefss.com/" target="_blank">KEF USA </a></li>
                                    
                                    <li><a href="http://www.uppcolleges.com/" target="_blank">Universal Placement Program</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Italy</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li> <a href="http://www.istitutonobile.it/" target="_blank">Istituto Aeronautico Umberto Nobile</a>
                                       
                                    </li>
                                    
                                    <li> <a href="http://kefss.com/" target="_blank">KEF USA </a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Morocco</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="http://m.mobile.global-exchange.org/" target="_blank">Global Student Exchange</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3> Venezuela </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.idiomasyviajes.com/" target="_blank">Idiomas y Viajes</a></li>
                                    
                                    <li><a href="http://intercambioestudiantil.com/" target="_blank">G.R. Academic Exchange Programs </a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>South Korea</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.uhaknet.co.kr/" target="_blank">Uhaknet Co., Ltd</a></li>
                                    
                                    <li><a href="http://www.uhakgoing.com" target="_blank">Han Education Consulting</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Spain</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li> <a href="http://www.spaineduprograms.es/" target="_blank">Spain Education Programs</a> 
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>United States</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li> <a href="http://www.aeaexplorer.com/" target="_blank">American Explorer Academy</a> 
                                    </li>
                                    
                                    <li><a href="http://www.cia-agency.com/" target="_blank">Cultural Interchange Agency</a></li>
                                    
                                    <li><a href="http://kefss.com/">KEF USA </a></li>
                                    
                                    <li><a href="http://www.uppcolleges.com/" target="_blank">Universal Placement Program</a></li>
                                    
                                    <li><a href="http://www.flowamerica.com/">Flow America</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Vietnam</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.chongroyuhak.com/">Chongro Overseas Educational</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../iep/apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/educational-partners/partners.pcf">©</a>
      </div>
   </body>
</html>