<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ESL for Teens | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/teen-program/program-description.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ESL for Teens</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/teen-program/">Teen Program</a></li>
               <li>ESL for Teens</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>Program Description</h2>
                        
                        
                        
                        <p>This program is located at Valencia's West Campus and Osceola Campus. The classrooms
                           are in close proximity to each other and they are near the advisor's office, the student
                           lounge, and the Client Service Center. Our attentive staff creates a comfortable 
                           learning environment for students and makes them feel at home right away. With supportive
                           staff, highly trained instructors,  state of the art facilities, and many interesting
                           social and cultural events,  students will start learning and improving their English
                           from day one. <br>
                           
                        </p>
                        
                        
                        <p>Features</p>
                        
                        <ul>
                           
                           <li>F-1  Student Visas</li>
                           
                           <li>Five levels: beginner through advanced</li>
                           
                           <li>Students  can study two weeks or four weeks</li>
                           
                           <li>Small  class sizes</li>
                           
                           <li>Classes  meet Tuesday - Friday, 9:00 a.m. - 1:00 p.m.</li>
                           
                           <li>Afternoon  Language lab </li>
                           
                           <li>Free  internet access throughout the building</li>
                           
                        </ul>
                        
                        
                        <br>
                        
                        <div>
                           
                           <div>
                              
                              
                              
                              
                              
                              
                              
                              <div>
                                 
                                 <h3>Program Placement</h3>
                                 
                                 
                                 <p>
                                    Students are placed in the program based on the results of a language  assessment
                                    completed before the start of class.  
                                 </p>
                                 
                              </div>    
                              
                              
                              <div>
                                 
                                 <h3>Program Structure</h3>
                                 
                                 
                                 <ol>
                                    
                                    <li>There are two courses  within a level <em>.</em>
                                       
                                    </li>
                                    
                                    <li>Each  course in a level is the same level of difficulty. For example, all beginner
                                       courses are at a beginner level.
                                    </li>
                                    
                                    <li>Each  course within a level covers different objectives and topics.</li>
                                    
                                    <li>All courses  within a level MUST BE COMPLETED before moving up to the next level.</li>
                                    
                                 </ol>
                                 
                                 
                                 <p><strong>ESL for Teens - Beginner Level</strong></p>
                                 
                                 <p>________ ESL for    Teens Beginner A<br>
                                    ________ ESL for    Teens Beginner B
                                 </p>
                                 
                                 <p><strong>ESL for Teens -  Intermediate 1 Level</strong></p>
                                 
                                 <p>________ ESL for    Teens Intermediate A<br>
                                    ________ ESL for    Teens Intermediate B
                                 </p>
                                 
                                 <p><strong>ESL for Teens -  Intermediate 2 Level</strong></p>
                                 
                                 <p>________ ESL for    Teens Intermediate A<br>
                                    ________ ESL for    Teens Intermediate B                   
                                 </p>
                                 
                                 <p><strong>ESL  for Teens - Advanced 1 Level</strong></p>
                                 
                                 <p>
                                    ________ ESL for Teens  Advanced 1A<br>
                                    ________ ESL for Teens  Advanced 1B
                                 </p>
                                 
                                 <p><strong>ESL for Teens - Advanced 2 Level</strong></p>
                                 
                                 <p>________ ESL for Teens Advanced 2A<br>
                                    ________ ESL for Teens Advanced 2B
                                 </p>
                                 
                              </div>  
                              
                              
                              
                              <div>
                                 
                                 <h3>Grading</h3>
                                 
                                 
                                 <p>
                                    Students will be assessed formally through a variety of means including but not limited
                                    to homework, quizzes, tests, role-plays, dialogues, and general instructor observation.
                                    These assessments will measure language learning in speaking, listening, reading,
                                    and writing.  Unit achievement tests, which are to be administered at the end of every
                                    two units, will serve as the main tool to measure students' language progress.  
                                    
                                 </p>
                                 
                                 <p>
                                    Grades are based on a letter grade scale of A-F.  
                                    
                                 </p>
                                 
                                 <p>
                                    A = 100-90% <br>
                                    B = 89-80% <br>
                                    C = 79-70% <br>
                                    D = 69-60% <br>
                                    F = 59-0% <br>
                                    
                                 </p>
                                 
                              </div> 
                              
                              
                              <div>
                                 
                                 <h3>Course Materials</h3>
                                 
                                 
                                 <p>
                                    Each level has its own combination textbook-workbook . Your teacher will give you
                                    the  book in class. Please make sure to bring your book with you to class each day
                                    and take it with you at the end of each day in order to complete your homework. Students
                                    also must bring a pen or pencil and paper or a notebook each day to class. 
                                 </p>
                                 
                                 
                                 
                              </div>  
                              
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="apply.html" target="_blank" title="How to Apply"></a>
                        
                        <hr>
                        
                        <h3>START DATES</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Summer 2017</div>
                                 
                                 <div data-old-tag="td">June - August</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Spring 2018</div>
                                 
                                 <div data-old-tag="td">January</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Summer 2018</div>
                                 
                                 <div data-old-tag="td">June - August</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr>
                        
                        <h3>Classes start monthly</h3>
                        <a href="iep-dates-and-deadlines-for-teens.html" title="Steps to Register"> View all start dates and holidays</a>
                        <a href="../educational-partners/partners.html"> Find an educational partner in your country</a>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a>    
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/teen-program/program-description.pcf">©</a>
      </div>
   </body>
</html>