<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ESL for Teens | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/teen-program/policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ESL for Teens</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/teen-program/">Teen Program</a></li>
               <li>ESL for Teens</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>School Policies</h2>
                        
                        
                        
                        <p>The ESL for Teens program  follows the policies established by Valencia College as
                           outlined in the  college's <a href="../../../about/general-counsel/index.html">Policy  Manual</a>. The policies below are for the ESL for Teens program  and they conform to college
                           policy, as well as immigration regulations and  regulations of other governing agencies.
                        </p>
                        
                        
                        
                        <div>
                           
                           <h3>Attendance Policy</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>
                                    Attendance for both F-1 and  non F-1 students is essential because we feel that you
                                    will not learn English if you do not attend class. 
                                    Class  attendance is also a requirement for F-1 students by the U.S. Citizenship and
                                    Immigration Services (USCIS). 
                                    Any F-1  student who violates this attendance policy may be terminated from the program.
                                    
                                 </p>
                                 
                                 <ul type="disc">
                                    
                                    <li>
                                       <strong>Absences:</strong> Attendance is mandatory and it is important to attend class on time. 
                                       Administration will be notified immediately of any student absences.
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Classroom Expectations</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>Be respectful of your classmates!</li>
                                    
                                    <li>Show respect for the property of others and school property.</li>
                                    
                                    <li>Pay attention in class. Students may not sleep during class.</li>
                                    
                                    <li>Use "inside voices" when in the classroom, student lounge, and walking in the hallways.</li>
                                    
                                    <li>Raise your hand if you have a question or if you need to use the restroom.</li>
                                    
                                    <li>No running.</li>
                                    
                                    <li>No inappropriate language or cursing in any language.</li>
                                    
                                    <li>Clean up after yourself.</li>
                                    
                                    <li>No chewing gum.</li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Cheating</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>
                                       Cheating is a form of academic dishonesty. It is  the use of ideas, language, images
                                       or work of 
                                       another without sufficient reference to show that the material is from another person.
                                       
                                    </li>
                                    
                                    <li>
                                       If a student is caught cheating at any time, the instructor may give the student a
                                       zero and 
                                       contact the appropriate person about the matter. This can cause the student to fail
                                       and be put on academic probation.
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Homework</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>You are required to complete all assignments.</li>
                                    
                                    <li>Assignments are to be completed legibly, neatly, and completely, or points may be
                                       deducted from any given assignment or the assignment may not be graded.
                                    </li>
                                    
                                    <li>Homework is due on the announced date at the beginning of class, and all assignments
                                       should be completed and prepared to turn in at the beginning of class, NOT during
                                       class.
                                    </li>
                                    
                                    <li>No late work will be accepted once assignments have been graded and returned to the
                                       class.
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Unacceptable Behavior</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>Socializing while the instructor or other students are talking.</li>
                                    
                                    <li>Regularly getting up and leaving the classroom.</li>
                                    
                                    <li>Making loud noises in the classroom, hallway or student lounge </li>
                                    
                                    <li>Chewing gum.</li>
                                    
                                    <li>Throwing cigarettes on the floor.</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Use of Electronic Devices</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>The use of cell phones and other electronic devices is prohibited during class and
                                       
                                       labs as it is disrespectful and a distraction to your instructors and other classmates.
                                       Please turn off all electronic devices. 
                                       This includes:
                                    </li>
                                    
                                    <ul>
                                       
                                       <li>Cell phones</li>
                                       
                                       <li>Pagers</li>
                                       
                                       <li>MP3 players or iPods</li>
                                       
                                       <li>Laptops</li>
                                       
                                       <li>Game devices</li>
                                       
                                    </ul>
                                    
                                    <li>This includes text messaging.</li>
                                    
                                    <li>Grade points may be taken off the final grade due to cell phone usage.</li>
                                    
                                    <li>Audio-visual recording of the class is not allowed.</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="apply.html" target="_blank" title="How to Apply"></a>
                        
                        <hr>
                        
                        <h3>START DATES</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Summer 2017</div>
                                 
                                 <div data-old-tag="td">June - August</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Spring 2018</div>
                                 
                                 <div data-old-tag="td">January</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Summer 2018</div>
                                 
                                 <div data-old-tag="td">June - August</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr>
                        
                        <h3>Classes start monthly</h3>
                        <a href="iep-dates-and-deadlines-for-teens.html" title="Steps to Register"> View all start dates and holidays</a>
                        <a href="../educational-partners/partners.html"> Find an educational partner in your country</a>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a>    
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/teen-program/policies.pcf">©</a>
      </div>
   </body>
</html>