<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ESL for Teens | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/teen-program/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ESL for Teens</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/teen-program/">Teen Program</a></li>
               <li>ESL for Teens</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>How to Apply</h2>
                        
                        
                        
                        <h3>Information  for Parents</h3>
                        
                        <p>
                           This program is intended for  international students who do not reside in the United
                           States and who wish to visit the United States during a school break. Students generally
                           arrive in groups and are accompanied by chaperones throughout the entire visit.  It
                           is expected that these groups will be coordinated through one of our student  recruiters
                           in your region. 
                        </p>
                        
                        <p>If you are a parent of a  student and are interested in having your child participate
                           in this program  please contact a 
                           <a href="../educational-partners/partners.html">student  recruiter</a> that supports your  area. Please note that in order to be eligible to receive a Form
                           I-20 for studying in this program, students may only enroll in the June - August timeframe.
                        </p>
                        
                        <p>Student  recruiters can provide you with complete information on the program including
                           housing and transportation. They will also assist you with the registration process.
                           If  there is no student recruiter in your area, please contact Izabella Vianna at:
                           <a href="mailto:ivianna@valenciacollege.edu">ivianna@valenciacollege.edu</a>.
                        </p>
                        
                        <h3>Information for Student  Recruiters</h3>
                        
                        <p>
                           If you are a student  recruiter and would like to coordinate an ESL for Teens program
                           for students in  
                           your country, please review the steps on <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/educational-partners/become-a-partner.cfm" target="_blank">How to Become an Educational Partner</a>.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="apply.html" target="_blank" title="How to Apply"></a>
                        
                        <hr>
                        
                        <h3>START DATES</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Summer 2017</div>
                                 
                                 <div data-old-tag="td">June - August</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Spring 2018</div>
                                 
                                 <div data-old-tag="td">January</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Summer 2018</div>
                                 
                                 <div data-old-tag="td">June - August</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr>
                        
                        <h3>Classes start monthly</h3>
                        <a href="iep-dates-and-deadlines-for-teens.html" title="Steps to Register"> View all start dates and holidays</a>
                        <a href="../educational-partners/partners.html"> Find an educational partner in your country</a>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a>    
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/teen-program/apply.pcf">©</a>
      </div>
   </body>
</html>