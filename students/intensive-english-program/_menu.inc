<ul>
	<li><a href="index.php">Intensive English Programs</a></li>
	<li class="submenu"><a class="show-submenu" href="javascript:void(0);">Information  <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="about-orlando.php">About Orlando</a></li>
			<li><a href="about-valencia.php">About Valencia College</a></li>
		</ul> 
	</li>
	<li class="submenu"><a class="show-submenu" href="iep/index.php">IEP<i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="iep/program-description.php">Program Description</a></li>
			<li><a href="iep/costs.php">Costs</a></li>
			<li><a href="iep/apply.php">How to Apply</a></li>
			<li><a href="iep/pre-arrival/default.php">Pre-Arrival</a></li>
			<li><a href="iep/housing.php">Housing</a></li>
			<li><a href="iep/arrival.php">Arrival</a></li>
			<li><a href="iep/current-students/default.php">Current Students</a></li>
			<li><a href="iep/testimonials.php">Testimonials</a></li>
		</ul>  


	<li class="submenu"><a class="show-submenu" href="teen-program/index.php">ESL for Teens<i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="teen-program/program-description.php">Program Description</a></li>
			<li><a href="teen-program/apply.php">How to Apply</a></li>
			<li><a href="teen-program/testimonials.php">Testimonials</a></li>
			<li><a href="teen-program/policies.php">School Policies </a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="f1-visa/index.php">Maintaining F-1 Student<br />Visa Status<i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="f1-visa/forms.php">Forms</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="educational-partners/index.php">Educational Partners<i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="educational-partners/partners.php">Directory of Partners</a></li>
			<li><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/educational-partners/become-a-partner.cfm" target="_blank">Become a Partner</a></li>
			<li><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">Contact Us</a></li>
		</ul>
	</li>
</ul>
