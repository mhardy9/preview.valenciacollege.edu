<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Maintaining F-1 Student Visa Status | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/f1-visa/forms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Maintaining F-1 Student Visa Status</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/f1-visa/">F1 Visa</a></li>
               <li>Maintaining F-1 Student Visa Status</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>Forms</h2>
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p>Form</p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Description</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/Valencia_College_Program_Extension.pdf">I-20 Program Extension Form</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>For students    who require additional time to complete academic program requirements
                                       and are    requesting an extended I-20 use this form.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/Valencia_College_F-1_Transfer-Out_Request.pdf">F-1 Transfer-Out Request Form</a><br> &amp; <br><a href="documents/Valencia_College_F-1_Transfer-Out_Checklist.pdf">F-1 Transfer-Out Checklist</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>The transfer-out request  form is used to notify Valencia of your intent to transfer
                                       your F-1    immigration status to another school. The International Advisor uses the
                                       information to update your <acronym>SEVIS</acronym> record with the name of the school to which you will transfer and your    transfer
                                       release date. The checklist guides you through the process of    officially entering
                                       and transferring from IEP-Valencia to another educational institution    in the <acronym>U.S.</acronym></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/Valencia_College_Vacation_Request.pdf">Vacation Request Form</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>This  form is for F-1 students who need to request vacation from the program.</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/Valencia_College_Departure.pdf">Departure Form</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>This form is used by F1 students for departure including completion of program, early
                                       withdraw from classes, and a break from studies due to an emergency.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="documents/Valencia_College_Update_Personal_Information.pdf">Update Personal Information</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>This form is used by  students who have a change or correction in their personal information
                                       (name, address,   citizenship) and wish to be issued an updated I-20 reflecting this
                                       change.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../iep/apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/f1-visa/forms.pcf">©</a>
      </div>
   </body>
</html>