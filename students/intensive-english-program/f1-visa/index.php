<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Maintaining F-1 Student Visa Status | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/f1-visa/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Maintaining F-1 Student Visa Status</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li>F1 Visa</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h1>&nbsp;</h1>
                        
                        
                        
                        <p>All F-1 students are responsible for learning, understanding, and complying with United
                           States 
                           federal laws and regulations governing the F -1 student visa. Failure to do so will
                           violate your legal 
                           status in the U.S. If you have any questions, please see your international student
                           advisor. 
                        </p>
                        
                        
                        
                        
                        <div>
                           
                           <h3>Checking In</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>You must check in with the school Designated School Official within 30 days of your
                                    arrival. 
                                    The official check in day will be during the mandatory orientation you will meet your
                                    advisor, 
                                    who is also the Designated School Official for immigration.<br><br>Please make sure to bring the following documents to orientation:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>I-20</li>
                                    
                                    <li>Passport  and visa</li>
                                    
                                    <li>Insurance</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Economic Hardship </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    Off-campus employment is allowed if the student is going through severe economic hardship.
                                    
                                    Please visit the <a href="http://www.uscis.gov/portal/site/uscis">U.S. Citizenship and Immigration Services</a> website for more 
                                    information and schedule a meeting with your advisor. 
                                    
                                 </p>
                                 
                                 <p>Social  Security</p>
                                 
                                 <p>
                                    Social Security numbers are generally assigned to people who are authorized to work
                                    in the United States. 
                                    Social Security numbers are used to report your wages to the government and to determine
                                    eligibility for 
                                    Social Security  benefits. Social Security will not assign you a number just to enroll
                                    in a college or school. 
                                    If you want to get a job on campus, you should contact your DSO to find out if you
                                    are eligible to work on campus. 
                                 </p>
                                 
                                 <p><strong>Language school students are not permitted to work unless it is due to economic hardship.</strong></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Program Completion</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You  have 60 days after your program completion and then you must leave the country.
                                    
                                 </p>
                                 It  is the completion of your program that determines when the course of study ends
                                 and the 60-day grace period begins, NOT the end-day of the I-20.
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Program Extension</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>Program  extensions are granted for <strong>study ONLY</strong>.
                                    </li>
                                    
                                    <li>You  must complete the Program Extension Form.</li>
                                    
                                    <li>Inform  your International Student Advisor at least <strong>21 days</strong> <u>prior to</u> your program end date. <strong><em><u>Failure to do so will cause  you to be out of status</u>.</em></strong>
                                       
                                    </li>
                                    
                                    <li>Provide  an updated original bank statement if the one on file is 5 months or older.</li>
                                    
                                    <li>Provide  proof of health insurance coverage for the new program duration.</li>
                                    
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Reinstatement</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Reinstatement is the  process by which a student who has failed to maintain his or
                                    her status may be  “reinstated” to lawful F-1 status at the discretion of USCIS. Students
                                    become  out of status when they: 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Enroll  for less than a full course load.</li>
                                    
                                    <li>Are  not registered during the academic year as required.</li>
                                    
                                    <li>Fail  to extend their stay prior to the expiration date on the SEVIS I-20. </li>
                                    
                                    <li>Violate  one of the school policies. </li>
                                    
                                    <li>Are  not maintaining good academic standing. </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Change of Status</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Students Who Meet the Following Criteria Are  Authorized To Apply For a Change of
                                       Status</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Has  not been out of status for more than five months prior to filing for  reinstatement.</li>
                                    
                                    <li>Does  not have a record of repeated status violations.</li>
                                    
                                    <li>Is  pursuing or will pursue a full course of study in the next available term.</li>
                                    
                                    <li>Has  not engaged in unauthorized employment.</li>
                                    
                                    <li>The  violation of the student’s status resulted from circumstances beyond the  student’s
                                       control.
                                    </li>
                                    
                                 </ul>
                                 <strong>If you have failed to maintain your status  and would like to be reinstated, please
                                    contact the International Student  Advisor immediately.</strong> 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Taxes</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>International students are  not required to pay Social Security Tax, Medicare, or
                                    Federal Unemployment Tax. They ARE required to pay Federal  Income Tax and State Income
                                    Tax in Florida. 
                                 </p>
                                 
                                 <p>Federal law requires <u>ALL</u> students in F-1 status to submit Tax Form 8843 - Statement for Exempt  Individuals,
                                    <u>EVEN IF THEY DID NOT  WORK</u> in that last year. If you <u>did</u> work the previous year, you are required to submit <u>both</u> Tax Form 8843 - Statement for Exempt Individuals <u>and</u> Tax Form 1040NR - US Tax  Return for Certain Nonresident Aliens.<br>
                                    <br>
                                    Please note that there are  existing treaties between the U.S. and certain countries
                                    that serve the purpose  of eliminating double taxation of income. If your country
                                    has an agreement with the U.S., please provide the  proper forms in order to take
                                    advantage of applicable tax treaty  exemptions. If you are not sure that  your country
                                    has an existing treating, visit <a href="http://www.irs.gov">www.irs.gov</a> for more information.
                                 </p>
                                 
                                 <p>The U.S government  agency that deals with taxes is the Internal Revenue Service (IRS).&nbsp;
                                    According to the IRS, <strong>all  international students are required to file INS form 8843</strong>. You  can view and print out these instructions from the <a href="http://www.irs.gov/" target="_blank">IRS web site</a> under the Forms  &amp; Publications link, by choosing the correct form number from the
                                    list  provided.<strong> </strong></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           <h3>Transfer Out</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>The  International Student Advisor will assist you with the transfer out process to
                                       any college/university.
                                    </li>
                                    
                                    <li>As  an IEP student, you have conditional acceptance to Valencia College. </li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           <h3>Travel and Re-Entry</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <p>Students must consult their Designated School Official (DSO)  prior to traveling within
                                    the U.S. or abroad.
                                    
                                 </p>
                                 
                                 <p>Emergency Travel</p>
                                 
                                 <ul>
                                    
                                    <li>If you are leaving the U.S. and have not  completed the 12-week program, you must
                                       leave the U.S. within 10 days.
                                    </li>
                                    
                                    <li>If you leave the country due to an unexpected emergency and you do  not get your I-20
                                       signed for travel, the school has the option of signing the  I-20 and sending it overnight
                                       to you. There should be good explanation for the  sudden departure, as well as appropriate
                                       documentation.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Travel or Vacation</p>
                                 
                                 <ul>
                                    
                                    <li>International  students are eligible to take two months of vacation per year. </li>
                                    
                                    <li>You  must attend class full-time for twenty weeks.</li>
                                    
                                    <li>You  must have completed your current level. </li>
                                    
                                    <li>You  must be registered for the session upon your return. This might require a program
                                       extension. 
                                    </li>
                                    
                                    <li>You  must provide proof of travel.</li>
                                    
                                    <li>The  I-20 must be signed. </li>
                                    
                                 </ul>
                                 
                                 <p>Re-Entry Requirements</p>
                                 
                                 <p>Below  are the basic requirements for an F-1  student to reenter the United States
                                    after traveling abroad on pleasure or  personal business?
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>A SEVIS Form I-20, endorsed for travel and signed by your DSO </li>
                                    
                                    <li>You have been out of the United States for less than five months </li>
                                    
                                    <li>A current passport valid for at least six months after the date of  your reentry or,
                                       if you are from <a href="http://www.ice.gov/sevis/travel/faq_f2.htm#_Toc81222004%23_Toc81222004">one of the countries  listed below</a>, a passport that is current through the date of entry 
                                    </li>
                                    
                                    <li>A valid, current visa or you traveled to <a href="http://www.ice.gov/sevis/travel/faq_f2.htm#_Toc81222003%23_Toc81222003">contiguous country or  adjacent island</a> for less than thirty days 
                                    </li>
                                    
                                    <li>Financial information showing proof of necessary funds to cover  tuition and living
                                       expenses 
                                    </li>
                                    
                                    <li>Bringing  your most recent I-94, Departure Card, will facilitate your reentry, if
                                       reentering  through a land Port of Entry. If you are flying, the airline will collect
                                       your  I-94 prior to departure and you will complete a new one upon re-entry.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../iep/apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/f1-visa/index.pcf">©</a>
      </div>
   </body>
</html>