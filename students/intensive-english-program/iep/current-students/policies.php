<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/current-students/policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/intensive-english-program/iep/current-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li><a href="/students/intensive-english-program/iep/current-students/">Current Students</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>School Policies </h2>
                        
                        
                        
                        <p>The Intensive English program follows the policies established by Valencia College
                           as outlined in the college's <a href="../../../../about/general-counsel/index.html">Policy  Manual</a>.The policies below are for the ESL for Teens program and they conform to college
                           policy, as well as immigration regulations and regulations of other governing agencies.
                        </p>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           
                           <h3>Attendance &amp; Lateness</h3>
                           
                           <div>
                              
                              <div>                   
                                 The Intensive English Program (IEP) requires attendance for both F-1 and non F-1 students
                                 because we feel that students will not learn English if they do not attend class.
                                 Class attendance is also a requirement for F-1 students by the U.S. Citizenship and
                                 Immigration Services (USCIS). Any F-1 student who violates this attendance policy
                                 may be terminated from the program.
                                 
                                 <p><strong>Absences</strong> 
                                 </p>
                                 
                                 <p>To help students succeed in learning English, the IEP requires that all students must
                                    attend at least 70% of classes.  Students will be placed on attendance probation if
                                    absent for more than 30% of a course for a maximum of 3 sessions. This may result
                                    in termination from the program.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Important information is given at the beginning of class. If you miss that information,
                                       you risk falling behind. If you are late or absent, it is YOUR RESPONSIBILITY to find
                                       out what you missed in class.
                                    </li>
                                    
                                    <li>If you leave the room and do not return, you will be marked absent.</li>
                                    
                                    <li>
                                       <strong>Emergencies are defined as severe illness, death of a family member, or a national
                                          emergency.</strong> Students must see the advisor immediately regarding any absences.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Lateness </strong><br>
                                    It is important to attend class on time.  If a student is late 3 times in one session,
                                    it will be accounted as one absence.
                                 </p>
                                 
                                 <p><strong>Attendance Probation</strong></p>
                                 
                                 <p>Below is the progression of communication regarding our attendance probation policy.</p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Initial Notice:</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Student will receive an email with policy reminder</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Second Warning:</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Meet with Advisor and receive first official letter</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Third Warning:</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Student will receive second official letter</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Notice of Termination:</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Student will receive an official letter stating that they will be terminated in days
                                                and will need to leave the country immediately.
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           
                           <h3>Classroom Expectations</h3>
                           
                           <div>
                              
                              <div>    
                                 
                                 
                                 
                                 <ul>
                                    
                                    <li>Active participation is essential for succeeding in this course. </li>
                                    
                                    <li>Arrive on time and prepared.</li>
                                    
                                    <li>Listen and respect everyoneâ€™s input and ideas.</li>
                                    
                                    <li>Do not waste class time.</li>
                                    
                                    <li>Stay on task during group work and class discussion.</li>
                                    
                                    <li>Follow instructions.</li>
                                    
                                    <li>Use English only in the classroom.</li>
                                    
                                    <li>Do not disturb other students or disrupt the classroom by talking off topic. If you
                                       choose to talk to another student during class, you will be asked to separate your
                                       seats.
                                    </li>
                                    
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Homework</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>You are required to complete <strong>ALL</strong> assignments. 
                                    </li>
                                    
                                    <li>Assignments are to be completed legibly, neatly, and completely, or points may be
                                       deducted from any given assignment or the assignment may not be graded.
                                    </li>
                                    
                                    <li>Homework is due on the announced date at the beginning of class, and all assignments
                                       should be completed and prepared to turn in at the beginning of class, NOT during
                                       class.
                                    </li>
                                    
                                    <li>No late work will be accepted once assignments have been graded and returned to the
                                       class.
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Unacceptable Behavior</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>Socializing while the instructor or other students are talking.</li>
                                    
                                    <li>Regularly getting up and leaving the classroom.</li>
                                    
                                    <li>Making loud noises in the classroom, hallway or student lounge.</li>
                                    
                                    <li>Chewing gum.</li>
                                    
                                    <li>Smoking on campus.</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <h3>Use of Electronic Devices</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>The use of cell phones and other electronic devices is prohibited during class and
                                       labs as it is disrespectful and a distraction to your instructors and other classmates.
                                       Please turn off all electronic devices. 
                                       This includes:
                                    </li>
                                    
                                    <ul>
                                       
                                       <li>Cell phones</li>
                                       
                                       <li>Pagers</li>
                                       
                                       <li>MP3 players or iPods</li>
                                       
                                       <li>Laptops</li>
                                       
                                       <li>Game devices</li>
                                       
                                    </ul>
                                    
                                    <li>This includes speaking and text messaging.</li>
                                    
                                    <li>Electronic translators are not allowed in class.</li> 
                                    
                                    <li>Grade points may be taken off the final grade due to cell phone usage.</li>
                                    
                                    <li>If you have a special need, please discuss limited use of the cell phone with your
                                       instructor prior to class
                                    </li>
                                    
                                 </ul>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Academic Probation </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 
                                 <ul>
                                    
                                    <li>Students who receive a grade of 'F' areautomatically placed on academic          
                                       probation.
                                    </li>
                                    
                                    <li>Students are not permitted to move up  to the next level and must remain in      
                                       the same level until they receive a  minimum grade of 'D'.
                                    </li>
                                    
                                    <li>Students will be given the opportunity  to take up to two more courses           
                                       (four weeks) in order to raise their grade.
                                    </li>
                                    
                                    <li>Once the student receives a grade of 'D' they will be removed from academic probation.
                                       
                                    </li>
                                    
                                    <li>After taking two additional courses,  the student still has not received a grade of
                                       'D' they will be terminated from  the program.
                                    </li>
                                    
                                    <li>Students on an F-1 (student) visa will be given 10 days to transfer to another school
                                       or to leave the country
                                    </li>
                                    
                                    <li>Students can only be placed on an academic probation one time.</li>
                                    
                                    <li><u>Any relapse is cause for immediate termination from the program</u></li>
                                    
                                 </ul> 
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>
                              <a href="#">Cheating</a>
                              
                           </h3>
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li> Cheating is a form of academic dishonesty. It is the use of ideas, language, images
                                       or work of another without sufficient reference to show that the material is from
                                       another person. 
                                    </li>
                                    
                                    <li> If a student is caught cheating at any time, the instructor may give the student
                                       a zero and contact the appropriate person about the matter. This can cause the student
                                       to fail and be put on academic probation.
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/current-students/policies.pcf">©</a>
      </div>
   </body>
</html>