<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/current-students/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/intensive-english-program/iep/current-students/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li><a href="/students/intensive-english-program/iep/current-students/">Current Students</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Current Students</h2>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>COLLEGE CONTACTS</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>ASSISTANCE</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Daniel Eshcol </strong><br> 
                                       Advising Manager
                                       
                                    </p>
                                    
                                    <p><strong>Tatiana    Tyler</strong><br>
                                       International Student    Advisor <br>
                                       
                                    </p>
                                    
                                    <p><strong>West Campus Building  10</strong></p>
                                    
                                    <p>or</p>
                                    
                                    <p><strong>Osceola Campus International Village </strong></p>
                                    
                                    <p>Check-in at the Front Desk  </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>Course level changes</li>
                                       
                                       <li>F-1 and non F-1 student advising</li>
                                       
                                       <li>Immigration questions </li>
                                       
                                       <li>Attendance issues (tardiness, absenteeism)</li>
                                       
                                       <li>Academic Probation</li>
                                       
                                       <li>Social/Activity information</li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>Vacation requests</li>
                                       
                                       <li>Cyber Lab </li>
                                       
                                       <li>F-1 transfer out process to another institution or the    credit side</li>
                                       
                                       <li>Economic Hardship</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Client    Service Center</strong><br>
                                       
                                    </p>
                                    
                                    <p><strong>West Campus Building 10</strong></p>
                                    
                                    <p>or</p>
                                    
                                    <p><strong>Osceola Campus International Village</strong></p>
                                    
                                    <p> Check-in at the Front Desk    or call 407-582-6688</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>Register and pay for classes</li>
                                       
                                       <li>Program Extensions</li>
                                       
                                       <li>Sign up and pay for field trips </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Security    Desk</strong><br>
                                       Lobby / 407-582-1000<strong></strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>Emergencies </li>
                                       
                                       <li>Classroom location</li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        The Commission on English Language Program Accreditation (CEA) requires that all accredited
                        programs publicy post how to<a href="documents/Filing_a_Complaint_Against_an_Accredited_Program.pdf"> file a complaint with the CEA</a>. Please know that this is completely separate from the Intensive English Program's
                        process to file a complaint. <br>
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <h3>Community</h3>
                              
                              
                              <h2>Community Resources  &amp; Local Attractions</h2>
                              
                              <p>While you are living  in Orlando, Florida, we want to make sure that you have easy
                                 access to the  resources you need for everyday life, as well as information on things
                                 to do  and local attractions. The <a href="../../../exchange/whyvalencia/resources.html" target="_blank">community  resources</a><br>
                                 page has information on transportation,  insurance, medical care, libraries and other
                                 public services, as well as links  to event calendars, shopping malls and outlets,
                                 theme parks, festivals,  performing arts, museums, and galleries.&nbsp;<strong> </strong></p>
                              
                           </div>  
                           
                           
                           <div>
                              
                              <h3>Driver's License</h3>
                              
                              
                              <h2>Driver's License </h2>
                              
                              <p>The best place to  find out detailed information on where and how to get a driver’s
                                 license is  directly from the <a href="http://www.hsmv.state.fl.us/" target="_blank">Florida Department of  Motor Vehicles (DMV) </a>web  page.&nbsp; Check the <a href="http://www.hsmv.state.fl.us/offices/" target="_blank">Driver’s  License Office Directory</a> for addresses, telephone numbers, days and operating  hours of nearby offices. The
                                 city of Orlando is in Orange County. You will need  the following in order to apply
                                 for a license: 
                              </p>
                              
                              <ul>
                                 
                                 <li>Current I-20 </li>
                                 
                                 <li>Valid passport </li>
                                 
                                 <li>I-94 Departure Record </li>
                                 
                              </ul>
                              
                              <p>Other services  provided by the Department of Motor Vehicle (DMV) include: issuance
                                 of Florida  ID card, license renewals, license plate renewals, and finding a Florida
                                 DMV-approved traffic school.
                              </p> <br><br>
                              
                              <blockquote> NOTE:  Florida law defines a bicycle as a vehicle so bicyclists are subject to many
                                 of  the same responsibilities as operators of motor vehicles and subject to the  same
                                 citations and fines for violating traffic law. 
                              </blockquote>       
                              
                           </div>  
                           
                           
                           <div>
                              
                              <h3>Benefits</h3>
                              
                              
                              <h2>Valencia Benefits &amp; Resources</h2>
                              
                              
                              <p><strong>Valencia Student ID</strong></p>
                              
                              <ul>
                                 
                                 <li>As  an F-1 student, you will receive a Valencia Student ID card.Â&nbsp; This identification
                                    card will give you access  to all Valencia events, the library, computer lab, and
                                    writing center at any  college campus (see below).
                                 </li>
                                 
                                 <li>You  can also get discounts on transportation, restaurants and movie theaters.</li>
                                 
                                 <li>To  get your ID, you will receive an identification number within four to six weeks
                                    along  with instructions.<strong><u></u></strong>
                                    
                                 </li>
                                 
                              </ul>
                              
                              
                           </div>  
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <p><a href="#">TOP</a></p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/current-students/default.pcf">©</a>
      </div>
   </body>
</html>