<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/pre-arrival/visa-application-interview.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li><a href="/students/intensive-english-program/iep/pre-arrival/">Pre Arrival</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        <h2>Steps to Apply for a Visa</h2>
                        
                        
                        
                        <p>Please review the steps below for details on applying for a visa. Once you are done
                           reviewing the information below, please refer to the Embassy Interview Checklist to
                           help prepare for your interview.
                        </p>
                        
                        
                        
                        
                        <ol>
                           
                           <li>
                              <strong>Find out how to  obtain a visa</strong>by visiting, the <a href="http://travel.state.gov/visa/visa_1750.html" target="_blank">U.S. Department of State.</a>
                              
                           </li>
                           <br>
                           
                           <li>
                              <strong>Review the Form 1-20</strong> (this form is also  known as “Certificate of Eligibility for Nonimmigrant (F-1) Student”
                           </li>
                           
                        </ol>
                        
                        <ul>
                           
                           <li>Check your name, date of birth, and all other personal information for accuracy. If
                              there are any errors, let us know  immediately. 
                           </li>
                           
                           <li>Sign at the bottom where it says your name and date.</li>
                           
                           <li>Make a copy of this for your records to leave at home with a family member along with
                              a copy of your passport.
                           </li>
                           
                        </ul>
                        <br>
                        
                        <li>
                           <strong>Make an appointment at your nearest U.S. Embassy or Consulate</strong>. Please note that they get extremely busy around the holidays.
                        </li>
                        <br>
                        
                        <li>
                           <strong>Pay the SEVIS  fee. </strong>Foreign  citizens applying to become students must pay the <a href="https://www.fmjfee.com" target="_blank" title="SEVIS I-901 fee">SEVIS I-901 fee</a> of $200 for F-1 visa applicants. 
                        </li>
                        <br>
                        
                        <li>
                           <strong>Complete the</strong> <a href="https://ceac.state.gov/genniv" target="_blank" title="DS-160: Online Nonimmigrant Visa Application">DS-160: Online Nonimmigrant Visa Application</a>
                           
                        </li>
                        <br>
                        
                        <li>
                           <strong>Find out if any  additional security clearance screenings are needed. </strong>There are some  countries that require additional security clearance screenings regardless
                           of  gender, field of study, or country of citizenship. If a security clearance is
                           required, it could  take several months, so plan accordingly. Visit the <a href="http://www.ice.gov" target="_blank" title="U.S. Immigration and Customs Enforcement">U.S. Immigration and Customs Enforcement</a> for more information.
                        </li>
                        <br>
                        
                        <li>
                           <strong>Consulate Appointment.</strong> Make  sure that you have the following items ready for your consulate appointment.
                           
                        </li>
                        
                        <ul>
                           
                           <li>I-20 signed and dated. </li>
                           
                           <li>Acceptance  letter. </li>
                           
                           <li>Make sure that your passport is valid for at least six months beyond the date of 
                              your expected stay. 
                           </li>
                           
                           <li>Financial  Support such as bank and/or sponsor letter. </li>
                           
                           <li>Any proof that you are planning to return to your country after you complete your
                              studies. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/pre-arrival/visa-application-interview.pcf">©</a>
      </div>
   </body>
</html>