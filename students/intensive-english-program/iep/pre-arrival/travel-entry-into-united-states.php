<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/pre-arrival/travel-entry-into-united-states.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li><a href="/students/intensive-english-program/iep/pre-arrival/">Pre Arrival</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>Travel and Entry into the United States</h2>
                        
                        
                        
                        <h3>Entry  Point/Arrival in Orlando</h3>
                        
                        <ul>
                           
                           <li>During  your flight, you will be asked to fill out some forms. If you do not understand
                              a form, ask for assistance:
                           </li>
                           
                           
                           
                           <ul>
                              
                              <li><strong>CF-6059 Customs  Declaration Form</strong></li>
                              
                              <li>
                                 <strong>I-94 Arrival-Departure  Record Form</strong> for immigration. The Form I-94 should reflect the address where you will  reside,
                                 not the school address.
                              </li>
                              
                           </ul>
                           
                           <li>If  you are an initial student entering to attend school for the first time, you 
                              cannot enter the United States more than 30 days before the program start date  indicated
                              on your I-20.
                           </li>
                           
                        </ul>
                        
                        <h3>Immigration Inspection  Upon Arrival</h3>
                        
                        <ul>
                           
                           <li>During  your inspection, it is important that you tell the Customs and Border  Protection
                              Officer that you will be attending school as an F-1 student.Â&nbsp; Once your inspection
                              is complete, the officer  will stamp your SEVIS Form for duration of status (D/S),
                              stamp the I-94, and  staple it in your passport.
                           </li>
                           
                           <li>In  the case that your admission to the U.S. needs to be verified at the port of 
                              entry, be sure to have the name and phone number of the school’s program  advisor
                              on hand.
                           </li>
                           
                           <li>You  will go through the US-VISIT registration, which involves obtaining a scan of
                              two index fingerprints and a digital photograph. It is conducted for all  nonimmigrant
                              visa holders.
                           </li>
                           
                           <li>Some  individuals may be asked to provide additional information under the National
                              Security Entry-Exit Registration System. Follow all instructions given by the  Customs
                              and Border Protection Officer.
                           </li>
                           
                        </ul>
                        
                        <h3>Orlando International Airport</h3>
                        
                        <p>
                           Students  will arrive to the <a href="http://www.orlandoairports.net/index.htm">Orlando International  Airport</a>.&nbsp;Going  through customs can add an additional hour on to your arrival time, so be
                           prepared. After you go through customs, you will be required to pass through  another
                           security and baggage checkpoint.&nbsp;Here are some useful links to  help you:<br>
                           <a href="http://www.orlandoairports.net/arrive/index.htm">Airport Arrivals Guide</a><br>
                           <a href="http://www.orlandoairports.net/gt.htm">Airport Parking</a><br>
                           <a href="http://www.orlandoairports.net/transport/directions.htm">Driving Distances and  Maps </a></p>
                        
                        <h3>Transportation</h3>
                        
                        <p>
                           Due to college  policy, Valencia cannot provide airport pick up. You can take a <a href="http://www.orlandoairports.net/transport/local_transport.htm">Taxi Cab</a> to your  accommodations if you do not have someone meeting you.&nbsp;Taxis use metered
                           rates in Orlando. There are many <a href="http://www.orlandoairports.net/transport/rental_cars.htm">rental car companies</a> in Orlando, but you  must be at least 21 years old to rent a car&nbsp;with most companies.
                           Check the  latest <a href="http://www.traffic.com/Orlando-Traffic/Orlando-Traffic-Reports.html">Orlando Travel  Reports</a> to see current delays.
                        </p>
                        Most  hotels will provide airport pick up for free or additional cost. Contact your
                        hotel for more information.
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/pre-arrival/travel-entry-into-united-states.pcf">©</a>
      </div>
   </body>
</html>