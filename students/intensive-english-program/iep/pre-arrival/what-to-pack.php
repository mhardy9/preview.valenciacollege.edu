<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/pre-arrival/what-to-pack.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li><a href="/students/intensive-english-program/iep/pre-arrival/">Pre Arrival</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2><strong>What to Pack</strong></h2>
                        
                        <p><strong>Important Documents to Carry with You:</strong></p>
                        
                        
                        <ul>
                           
                           <li>Valid  passport and visa</li>
                           
                           <li>I-20</li>
                           
                           <li>Evidence  of financial resources</li>
                           
                           <li>School  registration forms or receipts</li>
                           
                           <li>Name  and contact number of the school program advisor</li>
                           
                           
                        </ul>
                        
                        <p><strong>Find out what is  permitted and what is not:</strong></p>
                        
                        <p> It is very  important that you understand the items that are permitted and not permitted
                           on  the airplane if arriving by air. Verify this information at your airline’s  website
                           or the <a href="http://www.tsa.gov/">Transportation Security Administration’s</a> prior to arriving to  the airport.
                        </p>
                        
                        <p><strong>Here are additional suggestions of important  items to bring with you:</strong></p>
                        
                        <ul>
                           
                           <li>One carry-on bag with reading  material and snacks for the plane</li>
                           
                           <li>Daypack for regular day-to-day  travel</li>
                           
                           <li>Money belt</li>
                           
                           <li>Small umbrella</li>
                           
                           <li>Medical prescriptions for the  duration of your stay</li>
                           
                           <li>Medical records (if you have any  serious or chronic medical problems)</li>
                           
                           <li>Over-the-counter medications for  headache, upset stomach, etc.</li>
                           
                           <li>Eye  glasses and contact lenses</li>
                           
                           <li>Toiletries</li>
                           
                           <li>Layers of clothing for the colder  months </li>
                           
                           <li>Comfortable walking shoes</li>
                           
                           <li>At least one dressy outfit for  special occasions</li>
                           
                           <li>Hat, sunglasses, bathing suit, beach  towel, flip-flops, and sunscreen</li>
                           
                           <li>Laptop  computer</li>
                           
                           <li>Camera</li>
                           
                           <li>Do not bring any valuable jewelry</li>
                           
                           <li>Home food recipes</li>
                           
                           <li>Unique cooking tools</li>
                           
                           <li>Favorite  food or ingredients from home (make sure to check what <a href="http://www.tsa.gov/traveler-information/traveling-food-or-gifts">types of foods</a>  you are allowed to enter the USA) 
                           </li>
                           
                           
                        </ul>
                        
                        <p><strong>Appropriate clothing  for class:</strong></p>
                        
                        <p>You are expected to  demonstrate professionalism and good judgment at all times related
                           to your  appearance. For this reason, if you are ever in doubt about the appropriateness
                           of your appearance, please keep in mind that anything that could cause a  disruption
                           of the learning environment or intimidation of others in the learning  environment
                           will not be permitted.
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           
                           <div><a href="../apply.html" target="_blank" title="How to Apply"><span>How To Apply</span></a></div>
                           
                        </div>
                        <br>
                        <br>
                        
                        
                        <h3>START DATES</h3>
                        
                        <p>Classes start monthly.</p>
                        <a href="../../iep-dates-and-deadlines.html" title="Steps to Register">View all start dates and holidays.</a>
                        
                        <hr>
                        
                        <div><a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">CONTACT US</a></div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/pre-arrival/what-to-pack.pcf">©</a>
      </div>
   </body>
</html>