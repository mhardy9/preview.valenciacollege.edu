<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/arrival.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        <h2>Arrival</h2>
                        
                        
                        
                        <p>During your first few  days in Orlando, you will be very busy getting settled into
                           your housing, taking  the placement test, attending orientation, and getting prepared
                           for your  courses. Below is a list of activities  you will undertake during your first
                           week. The order of these events may change  but they are listed here to give you an
                           approximate idea of what to expect.<strong> </strong></p>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">First Week </div>
                                 
                                 <div data-old-tag="td"> 
                                    <ul>
                                       
                                       <li>Check into your apartment, hotel, or homestay</li>
                                       
                                       <li>Become familiar with your neighborhood</li>
                                       
                                       <li>Go grocery shopping</li>
                                       
                                       <li>Set up a checking account</li>
                                       
                                       <li>Take the  placement test one week before course begins either at <strong>West Campus in Building 10</strong> or at <strong>Osceola Campus in the International Village </strong>
                                          
                                       </li>
                                       
                                       <li>Attend Orientation</li>
                                       
                                       <li>Meet other new students</li>
                                       
                                       <li>Buy your books</li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Tuesday</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       <li>Start the Intensive English Program 
                                          
                                          (make sure to  bring your books!)
                                          and Cyberlab
                                       </li>
                                    </ul> 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Wednesday</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>Class </li>
                                       
                                    </ul>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Thursday</div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       <li>Class and Cyberlab</li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Friday</div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       <li>Class</li>
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Saturday-Sunday</div>
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>Buy any personal items as needed</li>
                                       
                                       <li>Free time</li>
                                       
                                    </ul>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday</div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>Begin the second week of class </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Intensive English Program</h3>
                        
                        <p>Now offered at two locations: West Campus &amp; Osceola Campus</p>
                        
                        <hr>
                        <a href="/STUDENTS/international/intensive-english-program/iep/apply.html" target="_blank" title="How to Apply">
                           
                           <div class="button-action">How To Apply</div>
                           </a>
                        
                        <div>
                           
                           <h3>START DATES</h3>
                           
                           <p>Classes start monthly.</p>
                           <a href="/STUDENTS/international/intensive-english-program/iep-dates-and-deadlines.html" title="Steps to Register">
                              
                              <div class="button-action_outline">View all start dates and holidays.</div>
                              </a>
                           
                        </div>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">
                           
                           <div class="button-action_outline">CONTACT US</div>
                           </a> 
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/arrival.pcf">©</a>
      </div>
   </body>
</html>