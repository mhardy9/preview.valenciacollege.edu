<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/program-description.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        
                        
                        <h2>Program Description</h2>
                        
                        <p>This program is located at the West Campus and Osceola Campus.   Our attentive staff
                           creates a comfortable learning environment for students and makes them feel at home
                           right away.  With supportive staff, highly trained instructors, state of the art facilities,
                           and many interesting social and cultural events, students will start learning and
                           improving their English from day one. 
                        </p>
                        
                        <p>Features</p>
                        
                        <ul>
                           
                           <li>F-1 Student Visas</li>
                           
                           <li>Nine levels from low beginner through high advanced</li>
                           
                           <li>Monthly start dates</li>
                           
                           <li>Based on their level, students can study anywhere from one month to over one year</li>
                           
                           <li>Small class sizes</li>
                           
                           <li>Classes meet daily</li>
                           
                           <li>Hours can be either 9:00 a.m. - 1:00 p.m. or 2:00 p.m. - 6:00 p.m. </li>
                           
                           <li>Free Cyber Language lab </li>
                           
                           <li>Free internet access </li>
                           
                           <li>A variety of social and cultural activities</li>
                           
                        </ul>
                        
                        <div>
                           
                           <div>
                              
                              
                              
                              
                              
                              <div>
                                 
                                 <h3>Program Placement</h3>
                                 
                                 
                                 <p>Students are placed in the program based on the  results of a computer-based assessment.
                                    This placement test includes listening, speaking, short answer, and written portions
                                    and is taken the week before class starts.
                                 </p>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Program Structure</h3>
                                 
                                 
                                 <ol>
                                    
                                    <li>Each level has two courses (A and B), and each course lasts four weeks for a total
                                       of eight weeks per level. 
                                    </li>
                                    
                                    <li>Courses within a level must be taken in consecutive order. </li>
                                    
                                    <li>Each course within a level covers different objectives and topics.</li>
                                    
                                    <li>Both courses within a level MUST BE COMPLETED before moving up to the next level.</li>
                                    
                                 </ol>
                                 
                                 
                                 <p><a href="documents/IEP-Grading-Standards-and-Proficiency-Scale.pdf" target="_blank">IEP Grading Standards and Proficiency Scale </a></p>
                                 
                                 <p><strong>Level 1: Low Beginner</strong><br>
                                    Level 1:  Low Beginner English A<br>
                                    Level 1:  Low Beginner English B<br>
                                    
                                 </p>
                                 <br>
                                 
                                 <p><strong>Level 2:  Mid Beginner</strong><br>
                                    Level 2:  Mid Beginner English A<br>
                                    Level 2:  Mid Beginner English B 
                                 </p>
                                 <br>
                                 
                                 <p><strong>Level 3:  High Beginner</strong><br>
                                    Level 3:  High Beginner English A<br>
                                    Level 3:  High Beginner English B
                                 </p>
                                 <br>
                                 
                                 <p><strong>Level 4:  Low Intermediate</strong><br>
                                    Level 4:  Low Intermediate English A<br>
                                    Level 4:  Low Intermediate English B
                                 </p>
                                 <br>
                                 
                                 <p><strong>Level 5:  Mid Intermediate</strong><br>
                                    Level 5:  Mid Intermediate English A<br>
                                    Level 5:  Mid Intermediate English B
                                 </p>
                                 <br>
                                 
                                 <p><strong>LEVEL 6:  HIGH INTERMEDIATE</strong><br>
                                    Level 6:  High Intermediate English A<br>
                                    Level 6:  High Intermediate English B
                                 </p>
                                 <br>
                                 
                                 <p><strong>LEVEL 7:  LOW ADVANCED</strong><br>
                                    Level 7:  Low Advanced English A<br>
                                    Level 7:  Low Advanced English B<br>
                                    
                                 </p>
                                 
                                 
                                 <p><strong>LEVEL 8:  MID ADVANCED</strong><br>
                                    Level 8:  High Advanced English A<br>
                                    Level 8:  High Advanced English B<br>
                                    
                                 </p>
                                 
                                 
                                 <p><strong>LEVEL 9:  HIGH ADVANCED</strong><br>
                                    Level 9:  High Advanced English A<br>
                                    Level 9:  High Advanced English B<br>
                                    <br>
                                    
                                 </p>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Grading</h3>
                                 
                                 
                                 <p>Grades are based on a letter grade scale of A-F.</p>
                                 
                                 <ul>
                                    
                                    <li>A = 100-90% </li>
                                    
                                    <li>B = 89-80% </li>
                                    
                                    <li>C = 79-70% </li>
                                    
                                    <li>F = 69-0% </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <h3>Course Materials</h3>
                                 
                                 
                                 <p>Please note that books must be purchased before the start of classes and are not included
                                    in the tuition. They are available for sale at the Campus Bookstore. 
                                 </p>
                                 
                                 <p>All of the information you need in order to buy books will be provided to you in your
                                    Registration Confirmation, including course title, book title, and store location.
                                    Please take this confirmation with you when you go to the bookstore so you know what
                                    books to buy.
                                 </p>
                                 
                                 <p> Campus Bookstore Hours: </p>
                                 
                                 <ul>
                                    
                                    <li> Monday - Thursday: 7am to 7pm</li>
                                    <br>
                                    
                                    <li> Friday: 7am to 5pm</li>
                                    
                                 </ul>
                                 
                                 
                                 <p>Please make sure to have your books by the first day of class.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Intensive English Program</h3>
                        
                        <p>Now offered at two locations: West Campus &amp; Osceola Campus</p>
                        
                        <hr>
                        <a href="/STUDENTS/international/intensive-english-program/iep/apply.html" target="_blank" title="How to Apply">
                           
                           <div class="button-action">How To Apply</div>
                           </a>
                        
                        <div>
                           
                           <h3>START DATES</h3>
                           
                           <p>Classes start monthly.</p>
                           <a href="/STUDENTS/international/intensive-english-program/iep-dates-and-deadlines.html" title="Steps to Register">
                              
                              <div class="button-action_outline">View all start dates and holidays.</div>
                              </a>
                           
                        </div>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">
                           
                           <div class="button-action_outline">CONTACT US</div>
                           </a> 
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/program-description.pcf">©</a>
      </div>
   </body>
</html>