<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/housing.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        
                        <h2>Housing</h2>
                        
                        <p>One of the most important things you will need to take care of before you start your
                           studies in the United States is finding a place to live. In general, the amount you
                           spend for housing should be limited to one-fourth or one-third of the total amount
                           you have planned to spend on living expenses. If the cost is one-half of your budget
                           or more, you may be spending too much. It is especially important to consider location
                           in selecting your housing. If you live farther than walking distance from the campus,
                           it may prove to be inconvenient unless you are close to public transportation or you
                           have a car. While Valencia College does not offer housing, students can choose from
                           a variety of options, including a homestay, hotel, or apartment. Below is information
                           on each of these options:
                        </p>
                        
                        <ul>
                           
                           <li>Homestays</li>
                           
                        </ul>
                        
                        
                        <ul>
                           
                           <li>Hotels*</li>
                           
                        </ul>
                        
                        
                        <ul>
                           
                           <li>Apartments</li>
                           
                        </ul>
                        
                        <p>Another option for students is to make arrangements for homestays, we have included
                           some company websites below. Their emphasis is on safe, quality, and affordable housing
                           with hosts who are pre-screened, interested in cultural exchange and happy to open
                           their home to a student.
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://www.homestayfinder.com/SearchHost.aspx?page=3&amp;country=US&amp;city=Orlando#.UoY7hyewd8E%23.UoY7hyewd8E">Homestay Finder</a></li>
                           
                           <li><a href="http://www.homestaybooking.com/homestay-in-orlando">Homestay Booking</a></li>
                           
                        </ul>
                        
                        <p>All companies listed  are privately owned  and are not associated with Valencia College;
                           the contact is provided here only as information about alternative housing possibilities
                           and any arrangements you choose to make are strictly between you (the student) and
                           the housing company.
                        </p>
                        
                        <p><strong>Below is a list of affordable hotels:</strong> 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Hotel Name &amp; Address</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Hotel Contact Information</strong></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Residence Inn<br>
                                          Orlando Convention Center</strong><br>
                                       8800 Universal Blvd.<br>
                                       Orlando, FL 32819
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Jami Mattei</p>
                                    
                                    <p>407-597-1880</p>
                                    
                                    <p><a href="http://www.marriott.com/hotels/travel/mcoic-residence-inn-orlando-international-drive/">www.orlandoresidenceinn.com</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Courtyard by Marriott<br>
                                          Orlando International Drive</strong><br>
                                       8600 Austrian Court<br>
                                       Orlando, FL 32819
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>407-351-2244<br>
                                       407-581-0508<br>
                                       <a href="http://www.internationaldrivecourtyard.com/">www.internationaldrivecourtyard.com</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Florida Mall Hotel</strong><br>
                                       1500 Sand Lake Rd<br>
                                       Orlando, FL, 32809
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>407-859-1500<br>
                                       <a href="http://www.thefloridahotelorlando.com/">www.thefloridahotelorlando.com</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Homestead Studio Suites Hotel</strong><br>
                                       4101 Equity Row<br>
                                       Orlando, FL 32819
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>407-352-5577<br>
                                       <a href="http://www.homesteadhotels.com/">www.homesteadhotels.com</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Extended Stay America</strong><br>
                                       8687 Commodity Circle<br>
                                       Orlando, FL 32819
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>407-248-8010<br>
                                       <a href="http://www.extendedstayamerica.com/">www.extendedstayamerica.com</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Holiday Inn Express</strong><br>
                                       7900 S. Conway Rd<br>
                                       Orlando, FL 32812
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>407-851-7900<br>
                                       <a href="http://www.hiexpress.com/">www.hiexpress.com</a></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>* Students are responsible for making their own hotel reservations. All hotels are
                           run independent of Valencia College. Valencia College acts only as an agent providing
                           information about nearby hotels and accommodations. It is understood and agreed that
                           Valencia College assumes no responsibility for, nor guarantees performance of, and
                           in no event shall be liable for, any loss, damage, or claims associated with your
                           stay at these nearby accommodations.
                        </p>
                        
                        <p><a href="http://www.mckinley.com/apartments/florida/orlando/">McKinley</a>is a website that may help you find apartments and other housing options . When conducting
                           your search, enter our street address and zip code to locate the nearest housing to
                           either the West Campus or Osceola Campus.
                        </p>
                        
                        <p>West Campus: 1800 South Kirkman Road, Orlando, FL 32811</p>
                        
                        <p>or</p>
                        
                        <p>Osceola Campus: 1800 Denn John Lane, Kissimmee, FL 34744</p>
                        
                        <p>McKinley is a private company and is not associated with Valencia College; the contact
                           is provided here only as information about alternative housing possibilities and any
                           arrangements you choose to make are strictly between you (the student) and Mckinley,
                           as well as the apartments listed on their website.
                        </p>
                        
                        <p>All partners are privately owned companies and are not associated with Valencia College;
                           the contact is provided here only as information about alternative housing possibilities
                           and any arrangements you choose to make are strictly between you (the student) and
                           the housing company.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Intensive English Program</h3>
                        
                        <p>Now offered at two locations: West Campus &amp; Osceola Campus</p>
                        
                        <hr>
                        <a href="/STUDENTS/international/intensive-english-program/iep/apply.html" target="_blank" title="How to Apply">
                           
                           <div class="button-action">How To Apply</div>
                           </a>
                        
                        <div>
                           
                           <h3>START DATES</h3>
                           
                           <p>Classes start monthly.</p>
                           <a href="/STUDENTS/international/intensive-english-program/iep-dates-and-deadlines.html" title="Steps to Register">
                              
                              <div class="button-action_outline">View all start dates and holidays.</div>
                              </a>
                           
                        </div>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">
                           
                           <div class="button-action_outline">CONTACT US</div>
                           </a> 
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/housing.pcf">©</a>
      </div>
   </body>
</html>