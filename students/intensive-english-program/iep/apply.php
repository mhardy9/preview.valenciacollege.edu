<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intensive English Program | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/intensive-english-program/iep/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/intensive-english-program/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1>Intensive English Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/intensive-english-program/">Intensive English Program</a></li>
               <li><a href="/students/intensive-english-program/iep/">Iep</a></li>
               <li>Intensive English Program</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height:40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element" class="google-translate"></div>
                        
                        
                        <h2>How to Apply</h2>
                        
                        <p>Contact the Client Service Center at 407-582-6688 and they will guide you through
                           the registration process.
                        </p>
                        
                        <h3>If You Do Not Require an F-1 Student Visa</h3>
                        
                        <ol>
                           
                           <li>Select a <a href="../iep-dates-and-deadlines.html">start date.</a> 
                           </li>
                           
                           <li>Download and complete the<a href="documents/Non-F1Application100615.pdf"> Non F-1 Student Application. </a>
                              
                           </li>
                           
                           <li>Submit the completed application to ce_info@valenciacollege.edu </li>
                           
                           <li>Pay the program fees.</li>
                           
                           <li>Arrive one week before to complete the  placement test.</li>
                           
                           <li>Attend the orientation.</li>
                           
                           <li>Buy your books.</li>
                           
                        </ol>
                        
                        <h3>If You Require an F-1 Student Visa</h3>
                        
                        <ol>
                           
                           <li>Download and read the<a href="documents/IEPApplicationInstructions.pdf"> F-1 Student Instructions<strong>.</strong></a>
                              
                           </li>
                           
                           <li>Download and complete the<a href="documents/F1App100515.pdf"> F-1 Student Application. </a>
                              
                           </li>
                           
                           <li>Select a <a href="../iep-dates-and-deadlines.html">start date.</a>
                              
                           </li>
                           
                           <li>Submit the completed application, along with the  application fee, a copy of your
                              passport and document of financial support to the Client Service Center via email
                              to ce_info@valenciacollege.edu. 
                           </li>
                           
                           <li>Receive an Acceptance Letter and I-20</li>
                           
                           <li>Go to embassy appointment.</li>
                           
                           <li>Submit Payment Balance three weeks before course start date. </li>
                           
                           <li>Arrive one week before to complete the  placement test.</li>
                           
                           <li>Attend the orientation. </li>
                           
                           <li>Buy your books.</li>
                           
                        </ol>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Intensive English Program</h3>
                        
                        <p>Now offered at two locations: West Campus &amp; Osceola Campus</p>
                        
                        <hr>
                        <a href="/STUDENTS/international/intensive-english-program/iep/apply.html" target="_blank" title="How to Apply">
                           
                           <div class="button-action">How To Apply</div>
                           </a>
                        
                        <div>
                           
                           <h3>START DATES</h3>
                           
                           <p>Classes start monthly.</p>
                           <a href="/STUDENTS/international/intensive-english-program/iep-dates-and-deadlines.html" title="Steps to Register">
                              
                              <div class="button-action_outline">View all start dates and holidays.</div>
                              </a>
                           
                        </div>
                        
                        <hr>
                        <a href="http://net4.valenciacollege.edu/forms/international/intensive-english-program/contact.cfm" target="_blank">
                           
                           <div class="button-action_outline">CONTACT US</div>
                           </a> 
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/intensive-english-program/iep/apply.pcf">©</a>
      </div>
   </body>
</html>