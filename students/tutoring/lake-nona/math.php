<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Mathematics Tutoring  | Valencia College</title>
      <meta name="Description" content="Tutoring for a variety of Math courses is available from the Lake Nona Tutoring staff. Check this page for scheduling, lab manuals, and helpful resources.">
      <meta name="Keywords" content="math, mathematics, algebra, statistics, tutoring, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/lake-nona/math.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg_interior">
         <div id="intro-txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li><a href="/students/tutoring/lake-nona/">Lake Nona</a></li>
               <li>Mathematics Tutoring </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Mathematics Tutoring</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Goals</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Promote positive beliefs and attitudes toward mathematics.</li>
                              
                              <li>Value prior knowledge.</li>
                              
                              <li>Make connections between prior knowledge, the world of the student, and the strands
                                 and actions of
                                 mathematics.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Summer 2017 Hours</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>MAT0018C, MAT0022C, MAT0028C, MAT1033C, MAC1105, MAC1114, MAC1140, MAC1147, MAC2233,
                              MAC2311, MAC2312,
                              MGF1106, and MGF1107
                           </h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday - Thursday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 8:00am - 12:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>MAP2302 and MAC2313</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 3:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>STA1001C and STA2023</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 4:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 8:00am - 12:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <p><strong>NOTE: Tutor availability is not guaranteed!</strong> If we receive advance notice of tutor
                              absence, we will post the notice on the appropriate subject page. We encourage you
                              to call if you need
                              last-minute details (408-582-7106 between 9:00am and 5:00pm).
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Recommended Math Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>These are some additional resources we've found helpful.</p>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="https://valenciacollege.edu/east/academicsuccess/math/calculators.cfm">Graphing Calculator
                                    (TI-83/TI-84) Tips and Hints</a></li>
                              
                              <li><a href="https://valenciacollege.edu/math/liveScribe.cfm">Valencia College's Math Help 24/7</a></li>
                              
                              <li><a href="http://patrickjmt.com/">Patrick JMT - Math Video Tutorials</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>MAT1033C Intermediate Algebra Lab Materials</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <p><a href="/documents/students/tutoring/lake-nona/MAT1033C-Lab-Manual.pdf" class="button-download hvr-sweep-to-right" target="_blank">Download Lab Manual PDF</a></p>
                           
                           
                           <h4>Lab Week 8 - Factoring Trinomials</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/tutoring/lake-nona/Method1-TrialandError.pdf" target="_blank">Method 1 - Trial and Error</a></li>
                              
                              <li><a href="/documents/students/tutoring/lake-nona/Method2-ACMethod.pdf" target="_blank">Method 2 - AC Method</a></li>
                              
                              <li><a href="/documents/students/tutoring/lake-nona/Method3-SlideandDivide.pdf" target="_blank">Method 3 - Slide and Divide</a></li>
                              
                              <li><a href="/documents/students/tutoring/lake-nona/Method4-BoxMethod.pdf" target="_blank">Method 4 - Box Method</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button btn-block text-center">Apply Now</a> <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865" class="button btn-block text-center">Schedule a Tour</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        
                        <br>
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" class="button btn-block text-center">Directions</a> <a href="http://valenciacollege.edu/map/lake-nona.cfm" class="button btn-block text-center">Campus Map</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                        
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/"><i class="social_facebook"></i>&nbsp;Lake Nona
                        on Facebook</a>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/lake-nona/math.pcf">©</a>
      </div>
   </body>
</html>