<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Lake Nona Tutoring Center  | Valencia College</title>
      <meta name="Description" content="Welcome to the Lake Nona Campus Tutoring website. We want you to be successful in your current classes, but most importantly help obtain the skills necessary to be successful in all of your future classes. Walk-in tutoring is available in a variety of subjects.">
      <meta name="Keywords" content="tutoring, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/lake-nona/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li>Lake Nona</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Tutoring Services</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Welcome</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Welcome to the Lake Nona Campus Tutoring website. We want you to be successful in
                              your current classes,
                              but most importantly help obtain the skills necessary to be successful in all of your
                              future classes.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Walk-In Tutoring</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Walk-in tutoring is available in a variety of subjects. For informations on hours
                              of operation, courses
                              covered, and additional resources, please check the subject pages below.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="business.html">Business</a></li>
                              
                              <li><a href="math.html">Mathematics</a></li>
                              
                              <li><a href="science.html">Science</a></li>
                              
                              <li><a href="/students/writing/lake-nona/">Writing Center (English/Reading)</a></li>
                              
                              <li><a href="pert.html">PERT Review</a></li>
                              
                           </ul>
                           
                           <p><strong>NOTE: Tutor availability is not guaranteed!</strong> If we receive advance notice of tutor
                              absence, we will post the notice on the appropriate subject page. We encourage you
                              to call if you need
                              last-minute details (408-582-7106 between 9:00am and 5:00pm).
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Online Tutoring</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Need additional help outside of our tutoring hours? Valencia College provides <em>free</em> online
                              tutoring for all students through SmartThinking. You can access SmartThinking from
                              Atlas
                              .
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>In Atlas, click on the Courses Tab, then look for the "Tutoring (online) - Smart Thinking"
                                 link under
                                 the "My Courses" section.
                                 
                              </li>
                              
                              
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Becoming a Tutor</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Candidates interested in becoming a tutor should submit a resume to <a href="mailto:kbeers1@valenciacollege.edu">Kevin Beers</a> and/or <a href="mailto:tkoehler@valenciacollege.edu">Theresa Koehler</a> with the following information in the body
                              of the email:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Subject area(s) that you would like to tutor</li>
                              
                              <li>Any tutoring experience</li>
                              
                              <li>Your career goals</li>
                              
                              <li>Why you would like to become a tutor</li>
                              
                              <li>A copy of your college transcripts</li>
                              
                           </ul>
                           
                           <p>Resumes will be kept on file for two semesters. Candidates will be contacted if selected
                              for an
                              interview. Please contact <a href="mailto:kbeers1@valenciacollege.edu">Kevin Beers</a> at 407-582-7145 or
                              <a href="mailto:tkoehler@valenciacollege.edu">Theresa Koehler</a> at 407-582-7109 if you have any
                              questions or concerns.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Contact Us</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <ul class="list_teachers">
                                    
                                    <li>
                                       <a href="mailto:kbeers1@valenciacollege.edu">
                                          
                                          <figure><img src="/_resources/img/teacher_1_thumb.jpg" alt="Valencia image description" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>Kevin Beers</h5>
                                          
                                          <p>Phone: 407-582-7145</p>
                                          <i class="pe-7s-angle-right-circle"></i> </a>
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="mailto:tkoehler@valenciacollege.edu">
                                          
                                          <figure><img src="/_resources/img/teacher_1_thumb.jpg" alt="Valencia image description" class="img-rounded">
                                             
                                          </figure>
                                          
                                          <h5>Theresa Koehler</h5>
                                          
                                          <p>Phone: 407-582-7109</p>
                                          <i class="pe-7s-angle-right-circle"></i> </a>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button btn-block text-center">Apply Now</a> <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865" class="button btn-block text-center">Schedule a Tour</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        
                        <br>
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" class="button btn-block text-center">Directions</a> <a href="http://valenciacollege.edu/map/lake-nona.cfm" class="button btn-block text-center">Campus Map</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                        
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/"><i class="social_facebook"></i>&nbsp;Lake Nona
                        on Facebook</a>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/lake-nona/index.pcf">©</a>
      </div>
   </body>
</html>