<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Science Tutoring  | Valencia College</title>
      <meta name="Description" content="Tutoring for a variety of Science courses is available from the Lake Nona Tutoring staff. Check this page for scheduling and helpful resources.">
      <meta name="Keywords" content="science, biology, chemistry, tutoring, lake, nona, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/lake-nona/science.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/lake-nona/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li><a href="/students/tutoring/lake-nona/">Lake Nona</a></li>
               <li>Science Tutoring </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Science Tutoring</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Summer 2017 Hours</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>BSC1005C, BSC101C, and BSC1011C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 9:00am - 5:30pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 8:00am - 12:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>BSC1020</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 4:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 4:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 10:30am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 4:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>BSC2093C and BSC2094C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 2:30pm - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 10:30am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 10:30am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 2:30pm - 7:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>MCB2010C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 4:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 2:00pm - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 9:00am - 7:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>CHM1020, CHM1025C, CHM1045C and CHM1046C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Friday:</strong> 8:00am - 12:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>CHM2210C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 9:00am - 7:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>CHM2210C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 1:00pm - 7:00pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 2:30pm - 5:30pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 3:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 2:30pm - 5:30pm
                              </li>
                              
                           </ul>
                           
                           
                           <h4>CHM2211C</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <strong>Monday:</strong> 9:00am - 3:30pm
                              </li>
                              
                              <li>
                                 <strong>Tuesday:</strong> 9:00am - 3:00pm
                              </li>
                              
                              <li>
                                 <strong>Wednesday:</strong> 9:00am - 3:00pm
                              </li>
                              
                              <li>
                                 <strong>Thursday:</strong> 9:00am - 3:00pm
                              </li>
                              
                           </ul>
                           
                           
                           <p><strong>NOTE: Tutor availability is not guaranteed!</strong> If we receive advance notice of tutor
                              absence, we will post the notice on the appropriate subject page. We encourage you
                              to call if you need
                              last-minute details (408-582-7106 between 9:00am and 5:00pm).
                           </p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Biology</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://www.zygotebody.com/">Google Body/Zygote Body</a></li>
                              
                           </ul>
                           
                           
                           <h4>Chemistry</h4>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://www.ptable.com/">Periodic Table</a></li>
                              
                              <li><a href="http://molview.org/">MolView - Draw structures and export as .png files</a></li>
                              
                              <li><a href="https://web.chemdoodle.com/demos/sketcher/">ChemDoodle - Draw structures and screenshot to
                                    save</a></li>
                              
                              <li><a href="https://web.chemdoodle.com/demos/simulate-nmr-and-ms/">ChemDoodle - NMR &amp; MS Prediction
                                    Tools</a></li>
                              
                              <li><a href="http://www.nmrdb.org/new_predictor/index.shtml?v=v2.71.3">nmrdb.org - NMR Prediction
                                    Tools</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://net4.valenciacollege.edu/promos/internal/admissions-apply-now-button.cfm" class="button btn-block text-center">Apply Now</a> <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=1.109424450.67023854.1466708865" class="button btn-block text-center">Schedule a Tour</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Contact</h3>
                        
                        
                        
                        <p>campusID = 2 campus_fields = "phone, phone_2, address, email"</p>
                        
                        <br>
                        
                        <a href="http://maps.google.com/maps?q=12350+Narcoosee+Rd+Orlando,+FL+32832&amp;hl=en&amp;sll=28.523908,-81.46225&amp;sspn=0.070812,0.104628&amp;hnear=12350+Narcoossee+Rd,+Orlando,+Florida+32832&amp;t=m&amp;z=17" class="button btn-block text-center">Directions</a> <a href="http://valenciacollege.edu/map/lake-nona.cfm" class="button btn-block text-center">Campus Map</a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Social Media</h3>
                        
                        
                        
                     </div>
                     <a href="https://www.facebook.com/ValenciaCollegeLakeNonaCampus/"><i class="social_facebook"></i>&nbsp;Lake Nona
                        on Facebook</a>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/lake-nona/science.pcf">©</a>
      </div>
   </body>
</html>