<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Services We Offer  | Valencia College</title>
      <meta name="Description" content="The West Campus tutoring center offers walk-in tutoring for math courses, small group tutoring for other subjects on select days and times, and offers math textbooks and calculators for short check-out.">
      <meta name="Keywords" content="services, textbooks, calculators, borrowing, tutoring, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/west/services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li><a href="/students/tutoring/west/">West</a></li>
               <li>Services We Offer </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Services We Offer</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Tutoring and Materials</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Math tutoring is available on a walk-in basis during operation hours. Small group
                                 tutoring is
                                 available for all other subjects on select days and times, <a href="#">check the schedule page</a> to
                                 see which subjects are offered and tutor availability
                                 
                              </li>
                              
                              <li>Math textbooks and solution manuals are available for 2 hour check out</li>
                              
                              <li>TI-84 graphing calculators are available for 5 hour check out</li>
                              
                              <li>Anything returned late is subject to a $1.20/hour fine</li>
                              
                              <li>
                                 <a href="#">Group study rooms</a> are available for students, staff, and faculty members when not in
                                 use by tutors
                                 
                              </li>
                              
                              <li>To check out materials and rooms students MUST have a valid Valencia ID</li>
                              
                           </ul>
                           
                           <p><strong>All materials are subject to a $1.20 per hour late fee!</strong></p>
                           
                           
                           <p>Please remember:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Treat the Tutoring Center like a classroom and be respectful</li>
                              
                              <li>No mobile phone conversations in the Tutoring Center, please take calls in the atrium</li>
                              
                              <li>Food and smokeless tobacco are not permitted in the Tutoring Center</li>
                              
                              <li>Students are required to show their Valencia ID in order to check out materials from
                                 the Information
                                 Desk
                                 
                              </li>
                              
                              <li>No children are permitted per Valencia policy 6Hx28:04-10</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Group Study Rooms</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Students must visit the Information Desk in the Tutoring Center to make a reservation
                                 for a group
                                 study room
                                 
                              </li>
                              
                              <li>Reservations are made on a first come-first serve basis</li>
                              
                              <li>There are eight study rooms available for reservation when not in use for tutoring
                                 sessions
                              </li>
                              
                              <li>A valid Valencia ID is required to book a group study room</li>
                              
                              <li>Study rooms come equipped with white boards and select rooms have monitors. Dry erase
                                 markers can be
                                 checked out for two hours with a valid VID card.
                                 
                              </li>
                              
                              <li>The rooms can be booked for a maximum of two hours at a time</li>
                              
                              <li>Students may book up to two weeks in advance</li>
                              
                              <li>The person reserving the room is responsible for any damages</li>
                              
                              <li>If empty for 15 minutes, students forfeit their reservation</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 17 campusID = 3 department_fields = "none" campus_fields = "none"
                        office_fields = "location,hours,phone,website"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" class="button btn-block text-center">Tutoring Satisfaction Survey</a>
                        
                     </div>
                     
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/west/services.pcf">©</a>
      </div>
   </body>
</html>