<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Tutoring Center  | Valencia College</title>
      <meta name="Description" content="The West Campus Tutoring Center offers free academic assistance for Valencia and UCF students with a valid Atlas account or Valencia ID.">
      <meta name="Keywords" content="tutoring, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li>West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>West Campus Tutoring Center</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>About the Tutoring Center</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The West Campus Tutoring Center offers free academic assistance for Valencia and UCF
                              students with a
                              valid Atlas account or Valencia ID.
                           </p>
                           
                           
                           <p>Math tutoring is available on a walk-in basis during operating hours while all other
                              subjects are by
                              appointment and conducted in small groups. Check out Smarthinking online tutoring
                              through the Courses tab
                              in Atlas for times when the Tutoring Center is full or closed.
                           </p>
                           
                           
                           <p>You can also check out math textbooks (2 hours), TI-84+ graphing calculators (5 hours),
                              and group study
                              rooms (2 hours), with a valid Valencia ID. <a href="services.html">Visit the Services page</a> for more
                              information.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Announcements</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li><a href="/documents/students/tutoring/west/Tutor-Schedules-2017-Summer.pdf" target="_blank">Summer 2017 Tutoring Schedule</a></li>
                              
                              <li>You can make Tutoring Center appointments by stopping by 7-240, or through <a href="https://atlas.valenciacollege.edu/">Atlas</a>. Make sure to select "Tutoring Center Schedule."
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Check Out Tutors on the Tube</h4>
                           
                           <p>We are proud to present Tutors on the Tube, a new initiative by the West Campus Tutoring
                              Center to bring
                              a little slice of the Tutoring Center to you, wherever you are. On our YouTube channel,
                              <a href="https://www.youtube.com/channel/UCUWiglQTpOTttt56VOyZFgA">Valencia West LS</a>, you can watch our
                              math tutors work out problems for some of the more challenging concepts you’ll face
                              in class.
                              
                           </p>
                           
                           <p>Ken talks about implicit differentiation, intercept of a function, and out of order
                              quadratic formulas.
                              Linda tackles sum and difference of cubes, plus the AC method of factoring. John covers
                              Professor Krise’s
                              tricky pear orchard and fish stocking problems.
                           </p>
                           
                           
                           <h4>Smarthinking Update</h4>
                           
                           <p><a href="http://atlas.valenciacollege.edu/">Log into Atlas</a> or click the button below to check out
                              Smarthinking, Valencia's online tutoring program. You can access this completely free
                              service on your
                              Courses tab. You have 8 hours of tutoring each semester, <a href="/documents/students/tutoring/west/smarthinking-schedule.pdf" target="_blank">access the schedule for Smarthinking tutors</a>.
                           </p>
                           
                           <p>
                              <a href="https://ptl5-cas-prod.valenciacollege.edu:8443/cas-web/login?service=http%3A//gcp.valenciacollege.edu/CPIP/?sys=smTh" class="button">Smarthinking</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Tutoring Center Staff</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Front Desk Staff</h4>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Caroline McCoy</li>
                              
                              <li>Carolyn Keglar</li>
                              
                              <li>CeCe Hill</li>
                              
                              <li>Liz Aquino</li>
                              
                              <li>Luis Nolasco</li>
                              
                           </ul>
                           
                           
                           <h4>Math Tutors</h4>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Andrew Farquaharson</li>
                              
                              <li>Erik Carillo</li>
                              
                              <li>Hector Medina-Reyes</li>
                              
                              <li>Ivan Ordonez</li>
                              
                              <li>Jennifer Nelson</li>
                              
                              <li>Luigi Garcia</li>
                              
                              <li>Maite Frometa</li>
                              
                              <li>Miguel Ramirez</li>
                              
                              <li>Mithil Patel</li>
                              
                              <li>Monica Garcia</li>
                              
                              <li>Nimit Patel</li>
                              
                              <li>Niraj Patel</li>
                              
                              <li>Rob Walls</li>
                              
                              <li>Scott Adams</li>
                              
                              <li>Stephen Cox</li>
                              
                              <li>Thanapat Phoolsuk</li>
                              
                           </ul>
                           
                           
                           <h4>Subject Tutors</h4>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Abby Marchetti (Macro- and Microeconomics)</li>
                              
                              <li>Ali Khalil (Biology, Anatomy &amp; Physiology)</li>
                              
                              <li>Andrew Farquaharson (Chemistry 1 &amp; 2, Organic Chemistry 1)</li>
                              
                              <li>Carlos Hernandez Pavon (Biology, Microbiology)</li>
                              
                              <li>Fortuné Tabouna (Network Engineering)</li>
                              
                              <li>Hani Alayoubi (Organic Chemistry)</li>
                              
                              <li>Heather Heberly (Anatomy &amp; Physiology, TEAS)</li>
                              
                              <li>Jared Hansraj (Financial and Managerial Accounting)</li>
                              
                              <li>Jase Smalley (US Government)</li>
                              
                              <li>Joao Pedro dos Santos (Portuguese)</li>
                              
                              <li>Lester Bulosan (Physics with Medical Applications &amp; Psychology)</li>
                              
                              <li>Michelle Cohen (EAP)</li>
                              
                              <li>Vladimir Perez (Spanish)</li>
                              
                           </ul>
                           
                           
                           <h4>Tutorial Center Support Specialists</h4>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Teresa Gallagher (Tutoring Coordinator)</li>
                              
                              <li>Amanda Forth (Smarthinking and Prescriptive Learning)</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 17 campusID = 3 department_fields = "none" campus_fields = "none"
                        office_fields = "location,hours,phone,website"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" class="button btn-block text-center">Tutoring Satisfaction Survey</a>
                        
                     </div>
                     
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/west/index.pcf">©</a>
      </div>
   </body>
</html>