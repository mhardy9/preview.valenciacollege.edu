<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Online Resources  | Valencia College</title>
      <meta name="Description" content="A list of links to useful resources for students to supplement visits to the tutoring center.">
      <meta name="Keywords" content="resources, math, tutoring, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/west/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li><a href="/students/tutoring/west/">West</a></li>
               <li>Online Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Online Resources</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Study Resources</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Want to brush up on your note taking, time management, goal setting, and test taking
                              skills? Check out
                              UCF's <a href="http://sarconline.sdes.ucf.edu/?page_id=344">SARC website for Learning Resources</a>.
                           </p>
                           
                           
                           
                           <p>Additional resources to help with Reading, Writing, and ESL are provided by the <a href="#">Writing
                                 Center.</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Instructional Videos</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="https://valenciacollege.edu/math/liveScribe.cfm">Math Help 24/7</a>: Valencia professors work
                                 through problems and concepts from Developmental Math through Differential Equations.
                                 
                              </li>
                              
                              <li>
                                 <a href="https://www.khanacademy.org/">Khan Academy</a>: Instructional videos for math, science,
                                 economics, and humanities.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://ed.ted.com/lessons">TED Ed</a>: Explanatory videos from You Tube and TED in a variety
                                 of subjects, like science, math, technology, health, psychology, the arts, and much
                                 more.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://youtube.com/user/crashcourse">Crash Course</a>: You Tube channel that covers US
                                 history, world history, chemistry, biology, and more.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://youtube.com/user/patrickJMT">Patrick JMT</a>: You Tube channel that covers mostly math
                                 concepts, like derivatives and integrals in calculus, graphing functions and the unit
                                 circle in
                                 trigonometry, etc.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Reference</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://www.studygeek.org/">Study Geek</a>: A math reference website with definitions, mini
                                 lessons, and online calculators; applicable to College Algebra, Trigonometry, Statistics,
                                 and Calculus.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://sdbs.db.aist.go.jp/sdbs/cgi-bin/direct_frame_top.cgi">Organic Compound Database</a>: A
                                 spectral database (NMR, IR, MS, etc.) for organic compounds, useful for lab reports
                                 and compound
                                 identification.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Simulations For Math &amp; Science</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://mw.concord.org/modeler/">The Concord Consortium</a>: Simulations in physics,
                                 chemistry, and biology that help visualize concepts including chemical equilibrium,
                                 mechanics,
                                 diffusion, and much more. Most simulations have mini lessons included for further
                                 explanation.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://phet.colorado.edu/en/simulations/category/new">PhET Interactive Simulations</a>:
                                 Another great resource for simulations. These simulations focus more on user manipulation
                                 than
                                 explanation.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Language Websites</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://www.busuu.com/enc">busuu</a>: A language website with online lessons and practice with
                                 native speakers. Free registration required for this site.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.signingsavvy.com/">Signing Savvy</a>: Website specifically for American Sign
                                 Language. Resources include ASL dictionary, videos, guides, and more.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Practice Problems</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://www.businessbookmall.com/accounting%20internet%20library2.htm">Accounting</a>: Videos,
                                 Power Points, and problem sets for cost, business accounting, managerial accounting,
                                 finance, and more.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.textbooksfree.org/Economics%20Notes.htm">Economics</a>: Notes, videos, and
                                 practice problems for both macro and microeconomics.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.solvephysics.com/problems.shtml">Physics</a>: Problems for kinematics, dynamics,
                                 waves, electricity, magnetism, and more.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://science.widener.edu/svb/tutorial/">General Chemistry</a>: Problems for conversions,
                                 scientific notation, empirical formula, gas laws, rate laws, and more.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www2.chemistry.msu.edu/faculty/reusch/VirtTxtJml/Questions/problems.htm">Organic
                                    Chemistry</a>: Problems for nomenclature, reactions, functional groups, stereochemistry, mechanisms,
                                 and
                                 more.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.biologycorner.com/">Biology</a>: Worksheets and quizzes for general topics in
                                 Biology I &amp; II.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.wiley.com/college/apcentral/anatomydrill/">Anatomy &amp; Physiology</a>: Interactive
                                 quizzes for topics covered in A&amp;P I &amp; II.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://highered.mcgraw-hill.com/sites/0072320419/student_view0/">Microbiology</a>: Companion
                                 website for a different Microbiology text book that offers free study guides, practice,
                                 and quizzes.
                                 
                              </li>
                              
                              <li>
                                 <a href="http://quizlet.com/">Quizlet</a>: Flash card generator for all subjects. You can browse
                                 flashcards made by other students or create your own.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 17 campusID = 3 department_fields = "none" campus_fields = "none"
                        office_fields = "location,hours,phone,website"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" class="button btn-block text-center">Tutoring Satisfaction Survey</a>
                        
                     </div>
                     
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/west/resources.pcf">©</a>
      </div>
   </body>
</html>