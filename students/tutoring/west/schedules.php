<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tutoring Schedules  | Valencia College</title>
      <meta name="Description" content="Schedules for when tutoring is available for various subjects, and tips to get the most out of a tutoring session.">
      <meta name="Keywords" content="schedules, math, tutoring, west, campus, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/tutoring/west/schedules.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/tutoring/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/tutoring/">Tutoring</a></li>
               <li><a href="/students/tutoring/west/">West</a></li>
               <li>Tutoring Schedules </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     
                     <div class="box_style_1">
                        
                        <h2>Tutoring Schedules</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Tutoring Information</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="/documents/students/tutoring/west/Tutor-Schedules-2017-Summer.pdf" target="_blank" class="button">Summer 2017 Tutoring Schedule</a></p>
                           
                           
                           <h4>Math Tutoring</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Math tutoring is available through walk-in basis every day the Tutoring Center is
                                 open, no
                                 appointments needed
                                 
                              </li>
                              
                              <li>One-on-one appointments are no longer available due to high student volume</li>
                              
                              <li>All math tutors are qualified to assist with College Algebra, Trigonometry, Pre-Calculus,
                                 and
                                 College/Liberal Arts Math.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Small Group Subject Tutoring</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Tutoring for non-math subjects is in a small group format with 1 tutor working with
                                 a maximum of 4
                                 students at a time
                                 
                              </li>
                              
                              <li>All currently enrolled Valencia students can make appointments to work with a tutor
                                 on their available
                                 days--you are allowed up to 4 appointments per week
                                 
                              </li>
                              
                              <li>You can sign up for an appointment up to 2 weeks in advance, book early, reserve your
                                 spot.
                              </li>
                              
                              <li>No-show appointments may be given to other students after 15 minutes</li>
                              
                              <li>Appointments can be made at kiosks in the Tutoring Center (7-240)</li>
                              
                              <li>No appointments are needed on Walk in Wednesdays</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Tips and Tricks</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Make sure you come prepared--bring your notes, textbooks, homework, Power Point slides,
                                 reviews, etc.
                                 so the tutors can see exactly where you are and help you get where you need to go
                                 
                              </li>
                              
                              <li>Math tutoring tends be less busy during the morning (we open at 8 am) and on Fridays
                                 and Saturdays, so
                                 visit us during those times to get even more time with our math tutors
                                 
                              </li>
                              
                              <li>Wednesday is walk-in for the small group subject tutoring, take advantage of these
                                 hours plus the
                                 appointment hours
                                 
                              </li>
                              
                              <li>Forgot to print something out? Need to work on online homework? We have you covered!
                                 There are 12
                                 computers in the Tutoring Center that have internet access, print, and Microsoft Office
                                 
                              </li>
                              
                              <li>When there is no scheduled tutoring happening in the smaller rooms you can check them
                                 out for quiet or
                                 group study for 2 hour blocks (with a valid Valencia ID)
                                 
                              </li>
                              
                              <li>You can check out math textbooks, solution manuals, and calculators if you forget
                                 yours at the
                                 Information Desk (with a valid Valencia ID)
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 17 campusID = 3 department_fields = "none" campus_fields = "none"
                        office_fields = "location,hours,phone,website"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        
                        <a href="http://valenciacollege.qualtrics.com/SE/?SID=SV_416C2TqWpUCjM5T" class="button btn-block text-center">Tutoring Satisfaction Survey</a>
                        
                     </div>
                     
                     
                     
                  </aside>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/tutoring/west/schedules.pcf">©</a>
      </div>
   </body>
</html>