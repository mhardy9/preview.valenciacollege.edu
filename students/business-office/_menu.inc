<ul>
   <li><a href="/students/business-office/index.php">Business Office</a></li>

<li class="submenu"><a class="show-submenu" href="#">Dates|Fees|Policies <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
<li><a href="/students/business-office/important-deadlines.php">Important Deadlines</a></li>
<li><a href="/students/business-office/tuition-fees.php">Tuition &amp; Fee Schedule</a></li>
<li><a href="/students/business-office/payments.php">Payments</a></li>
<li><a href="/students/business-office/policies.php">Policies</a></li>
<li><a href="/students/business-office/forms.php">Forms</a></li>
</ul>
</li>


    <li class="submenu"><a class="show-submenu" href="#">Tuition  <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
<li><a href="/students/business-office/tuition-installment-plan/index.php">Tuition Installment Plan</a></li>
<li><a href="/students/business-office/tuition-installment-plan/enrollment.php"> Enrollment</a></li>
<li><a href="/students/business-office/tuition-installment-plan/payments.php"> TIP Payments</a></li>
<li><a href="/students/business-office/tuition-installment-plan/books.php"> Book Purchases</a></li>
<li><a href="/students/business-office/tuition-installment-plan/manage-tip-account.php"> Manage My TIP Account</a></li>
<li><a href="/students/business-office/tuition-installment-plan/valencia-terms.php"> Valencia Terms</a></li>
<li><a href="/students/business-office/tuition-installment-plan/nelnet-terms.php"> Nelnet Terms</a></li>
        </ul>   
    </li>  
<li class="submenu"><a class="show-submenu" href="#">Fin-Aid  <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
<li><a href="/students/business-office/florida-prepaid.php">Florida Prepaid</a></li>
<li><a href="/students/business-office/financial-aid-authorization.php">Financial Aid Authorization</a></li>
<li><a href="/students/business-office/student-enrollment-agreement.php">Student Enrollment Agreement</a></li>
        </ul>   



    <li class="submenu"><a class="show-submenu" href="/students/business-office/frequently-asked-questions/index.php">Frequently Asked Questions <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
        <ul>
         <li><a href="/students/business-office/frequently-asked-questions/payments.php">Payments</a></li>
<li><a href="/students/business-office/frequently-asked-questions/third-party-payments.php">Third Party Payments</a></li>
<li><a href="/students/business-office/frequently-asked-questions/holds.php">Holds</a></li>
<li><a href="/admissions/refund/frequently-asked-questions.php">Refunds</a></li>
<li><a href="/students/business-office/frequently-asked-questions/1098t.php">1098-T</a></li>

        </ul>   
    </li>       
<li><a href="http://net4.valenciacollege.edu/forms/businessoffice/contact.cfm" target="_blank">Contact Us</a></li>
</ul>

