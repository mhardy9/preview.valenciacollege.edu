<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Business Office | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Business Office</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <p>The Business Office at Valencia College is the department to inquire and resolve any
                           questions pertaining to your financial obligations with the college. We strive to
                           provide you with accurate information, to allow a seamless transition through the
                           registration process.
                        </p>
                        
                        <ul>
                           
                           <li><a href="/students/business-office/tuition-installment-plan/manage-tip-account.php">MANAGE MY TIP ACCOUNT</a></li>
                           
                           <li><a href="/students/refund/index.php">REFUND INFORMATION</a></li>
                           
                        </ul>
                        
                        <p>Paying for Classes</p>
                        
                        <p>Valencia College accepts cash, money order, checks, American Express, Discover, MasterCard
                           and Visa. For more information on authorizing third-party payments or student loans,
                           visit the <a href="/students/business-office/payments.php">Payments</a> page.
                        </p>
                        
                        <p>For a summary of tuition and fees, visit the <a href="/students/business-office/tuition-fees.php">Tuition &amp; Fee Schedule</a> page.
                        </p>
                        
                        <p>IMPORTANT MESSAGE FOR HIGHER ONE ACCOUNT HOLDERS</p>
                        
                        <p>Effective May 4, 2016 Higher One ATMs will be taken out of service at all Valencia
                           campus locations. However, you will be able to access your funds surcharge-free at
                           any Allpoint network ATM location using your Valencia debit card. To find the ATM
                           most convenient for you:
                        </p>
                        
                        <ul>
                           
                           <li>Visit <a href="http://www.allpointnetwork.com" target="_blank">www.allpointnetwork.com</a></li>
                           
                           <li>Call 800-809-0308, option 2</li>
                           
                           <li>Download Allpoint ATM Locator app from <a href="https://itunes.apple.com/us/app/allpoint-global-surcharge/id307308511?mt=8" target="_blank">Apple iTunes</a>, <a href="https://play.google.com/store/apps/details?id=com.allpoint&amp;hl=en" target="_blank">Google Play</a>, or <a href="https://www.microsoft.com/en-us/store/apps/allpoint/9wzdncrdqtfj" target="_blank">Microsoft Store</a></li>
                           
                        </ul>
                        
                        <p>The Central Florida Educator (CFE) Credit Union ATMs on the East, Osceola, West, and
                           Winter Park Campuses will remain operational, but they are NOT part of the Allpoint
                           network. Therefore, you will incur a fee if you use this out of network ATM to access
                           your Higher One account with your Valencia debit card.
                        </p>
                        
                        <p>If you have any additional questions, please Higher One Customer care directly at
                           877-327-9515 or login to your account using EasyHelp.
                        </p>
                        
                        <p>IMPORTANT TAX RETURN DOCUMENT AVAILABLE</p>
                        
                        <p>The IRS 1098-T Form is now available. Go to Atlas 'Students' tab under 'Student Resources'
                           expand the Business Office link and click on <strong>IRS Form 1098-T (Electronic Delivery).</strong></p>
                        
                        <p>In order to receive Form 1098-T in an electronic format, recipient must have access
                           to the internet and have a current/active ATLAS account. To print the statement, recipient
                           must have access to a printer. If you don't have access to a printer, you can visit
                           the Atlas Lab at any campus to log in to the ATLAS and print your statement.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     <h3>IMPORTANT DATES &amp; DEADLINES</h3>
                     There are no events at this time. <a href="/students/business-office/important-deadlines.php">More</a>
                     
                     <hr class="styled_2">	
                     
                     <h3>HOURS &amp; LOCATIONS</h3>
                     
                     <div>
                        
                        <p><strong>Hours</strong><br> <span> Monday - Thursday: 8 AM to 6 PM<br> Friday: 9 AM to 5 PM <br> <i>Summer Fridays: 9 AM to Noon</i> <br> </span></p>
                        
                        <p><strong>Locations</strong></p>
                        
                        
                        <p>
                           West Campus<br>
                           SSB, Rm 101<br>
                           East Campus<br>
                           Bldg 5, Rm 214<br>
                           Osceola Campus<br>
                           Bldg 2, Rm 110<br>
                           
                        </p>
                        
                        
                        <p><strong>Phone:</strong><br> <a href="tel:407-582-1200">407-582-1200</a> &nbsp;//&nbsp; <a href="tel:407-582-2387">407-582-2387</a></p>
                        
                        
                        <p><strong>E-mail:</strong><br><a href="mailto:BusinessOffice@valenciacollege.edu">BusinessOffice@valenciacollege.edu</a></p>
                        
                        
                        
                     </div>
                     	
                  </div>
                  	
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/index.pcf">©</a>
      </div>
   </body>
</html>