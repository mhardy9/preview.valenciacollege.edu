<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia College Student Enrollment Agreement | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/student-enrollment-agreement.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Valencia College Student Enrollment Agreement</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Valencia College Student Enrollment Agreement</h2>
                        
                        <p>In addition to all Valencia College academic standards and policies, I hereby agree
                           to comply with the terms and conditions specified in this enrollment agreement in
                           order to enroll for credit courses at Valencia College.&nbsp; If I am a financial aid recipient,
                           I understand that I am responsible for adhering to all of the <a href="/admissions/finaid/gettingstarted/fin_deadlines.php">Terms and Conditions</a> set forth by the financial aid administering agency/agencies.&nbsp; I also understand
                           that information pertaining to the above statement can be found on Valencia’s website
                           and it is solely my responsibility to read this information.I also agree to abide
                           by all rules and regulations described in the <a href="/students/catalog/index.php">Valencia Academic Course Catalog</a><u>, </u>Academic Course Catalog Addenda, and <a href="/students/pdf/studenthandbook.pdf">Valencia Student Handbook</a><a href="/students/pdf/studenthandbook.pdf"><u>.</u></a></p>
                        	
                        <p><strong>Registration and Registration Changes</strong></p>
                        
                        <p>I understand I must be officially registered prior to or on the start date of course(s)
                           in order to participate in and receive academic credit for those courses. I am responsible
                           for knowing and complying with any and all registration deadline dates.&nbsp; I am responsible
                           for all requests to change, add, drop, or withdraw course registration made through
                           my Atlas account or by a Valencia employee on my behalf. I understand that I am responsible
                           for reviewing my registration and academic record each term for accuracy.
                        </p>
                        	
                        <p><strong>Financial Liability Statement</strong></p>
                        
                        <p>I agree to pay all Valencia student account balances and charges pursuant to Valencia
                           policies. I understand that the college is advancing value to me in the form of education
                           services, and that my right to register is expressly conditioned upon my agreement
                           to pay institutional costs including, but not limited to, tuition, fees, books, and
                           any additional costs, when those charges become due. It is my responsibility to view
                           and pay my student account balance in Valencia’s student portal referred to as “Atlas.”
                        </p>
                        
                        <p>I understand that a past due student account balance will result in a financial “hold,”
                           which prevents future registrations as well as other services being offered in accordance
                           with college policy. A delinquent student account balance may be reported to a credit
                           bureau and referred to collection agencies or litigated. I agree to pay any cost associated
                           with the collection of unpaid charges, including late fee assessments, collection
                           fees up to 30%, attorney fees and court costs. This agreement shall be construed in
                           accordance with Florida law, and any lawsuit to collect unpaid fees may be brought
                           in a court of competent jurisdiction in Orange County, Florida, regardless of my domicile
                           at the time of bringing such action.
                        </p>
                        	
                        <p><strong>Suspension of Services</strong></p>
                        
                        <p>I understand and agree that Valencia will withhold grade reports, transcripts, diplomas,
                           and other services if I fail to pay tuition, fees, books, and any additional costs
                           or otherwise fail to abide by the provisions of this agreement.&nbsp; Valencia will prevent
                           me from further registration activity until all outstanding balances on my student
                           account and/or to third party vendors have been satisfied in full.
                        </p>
                        	
                        <p><strong>Change in Name, Address, or Phone Number</strong></p>
                        
                        <p>I am responsible for updating my Valencia records with any changes in my name, address,
                           or phone number within seven (7) days of any such change.&nbsp; Personal information should
                           be updated via Atlas.
                        </p>
                        	
                        <p><strong>Communications</strong></p>
                        
                        <p>I understand that email<s>s</s> from my Atlas account are Valencia’s primary and official means of communication,
                           and serve to provide the means for delivering the College’s official notices. &nbsp;I am
                           responsible for reading the information and notices that are sent to me through my
                           assigned Valencia Atlas email.
                        </p>
                        
                        <p>I further acknowledge and understand that Valencia and/or third parties may contact
                           me regarding outstanding debt via my Atlas email, regular mail, home and mobile phones.&nbsp;
                           By accepting and agreeing below, I consent to Valencia and/or third parties doing
                           so.
                        </p>
                        	
                        <p><strong>Course Add/Drop and Withdrawal Procedures</strong></p>
                        
                        <p>I understand that non-attendance does not constitute a drop or a withdrawal.* I also
                           understand that notifying my professor does not constitute a withdrawal.&nbsp; I understand
                           that dropping or withdrawing from courses may affect my Satisfactory Academic Progress
                           (SAP) standing as set forth by the U.S. government or other agency’s (i.e. Bright
                           Futures).
                        </p>
                        
                        <p>If I drop my course(s) prior to the refund deadline, my tuition and fees will be refunded
                           in accordance with my <a href="/students/refund/index.php">refund choice</a> as determined by the refund schedule published in my Valencia Academic Course Catalog
                           .&nbsp; If I withdraw from my course(s) after the refund deadline, I understand that I
                           am financially obligated to pay for the total cost of the course.&nbsp; I understand that
                           if I am a financial aid recipient and I withdraw from my course(s), I will be subject
                           to and responsible for repaying the Return of Title IV Funds Calculation regulations
                           and Bright Futures.
                        </p>
                        	
                        <p><strong>Enrollment Agreement Renewal</strong></p>
                        
                        <p>I understand and agree that this Agreement is executed at the time of my initial enrollment
                           for each term at Valencia.&nbsp; I further understand that the college will notify me through
                           Atlas email a copy of any changes or modifications Valencia makes to this Agreement
                           during a term in which I am enrolled.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/student-enrollment-agreement.pcf">©</a>
      </div>
   </body>
</html>