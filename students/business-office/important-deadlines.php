<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Important Deadlines | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/important-deadlines.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Important Deadlines</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Important Dates &amp; Deadlines</h2>
                        
                        <p><a href="/students/calendar/index.php">Important Dates &amp; Deadlines</a></p>
                        
                        <div>
                           
                           <h3>Fall 2017</h3>
                           
                           <div class="table-responsive">
                              
                              <table class="table table table-striped add_bottom_30">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <th scope="col" width="100"><a title="Sort on “REGISTRATION”" href="#">Registration Date</a></th>
                                       
                                       <th scope="col" width="100"><a title="Sort on “PAYDATE”" href="#">Pay Date</a></th>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                                 <tbody>
                                    
                                    
                                    <tr>
                                       
                                       <td>May 23 to August 11</td>
                                       
                                       <td><strong>Friday, August 11 (5:00 PM)</strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>August 12 to End of Term</td>
                                       
                                       <td><strong>Due Upon Registration</strong></td>
                                       
                                    </tr>
                                    
                                    
                                    <tr> 
                                       <td></td>
                                       	
                                       <td>
                                          <div>***Fees will not be assessed until July 1st***</div>
                                       </td>
                                    </tr>
                                    
                                    
                                 </tbody>
                                 
                              </table>
                              
                           </div>	
                           	
                           	
                           
                           <tr>
                              
                              <td>July 3</td>
                              
                              <td>TIP registration begins</td>
                           </tr>
                           
                           <tr>
                              
                              <td>September 5</td>
                              
                              <td>TIP registration ends</td>
                           </tr>
                           
                           <tr>
                              
                              <td>September 5</td>
                              
                              <td>
                                 
                                 <p>Drop/Refund Deadline (<strong>Drop classes with 100% refund)</strong></p>
                                 
                              </td>
                           </tr>
                           
                           <tr>
                              
                              <td>September 21</td>
                              
                              <td>
                                 
                                 <p>Business Office refund begins</p>
                                 
                              </td>
                           </tr>
                           
                           <tr>
                              
                              <td>September 26</td>
                              
                              <td><a href="/admissions/finaid/gettingstarted/using_finaid.php">Financial Aid Disbursement begins</a></td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>November 3</td>
                              
                              <td>VA Deferments due</td>
                              
                           </tr>
                           
                           
                           
                        </div>
                        	
                        <hr clas="styled_2">
                        
                        <h3>Spring 2018</h3>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “RegistrationDate”" href="#">Registration Date</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “PaymentDue”" href="#">Payment Due</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>February 20 to April 20 </td>
                                    
                                    <td><strong>Friday, April 20 (5:00PM)</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>April 21  to End of Term</td>
                                    
                                    <td><strong>Due Upon Registration</strong></td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>late registration fee will be charged from April 21 to end of term.</td>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <td>February 20</td>
                                    
                                    <td>TIP registration begins</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>May 4</td>
                                    
                                    <td>TIP registration ends</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>May 14</td>
                                    
                                    <td>Drop/Refund Deadline (<strong>Drop classes with 100% refund)</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June  4 </td>
                                    
                                    <td> Business Office refund begins </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June 5 </td>
                                    
                                    <td><a href="/finaid/gettingstarted/using_finaid.php">Financial Aid Disbursement begins</a> (Pell Grants July 10, 2018)
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>July 6 </td>
                                    
                                    <td>VA Deferments due</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        	
                        	
                        
                        <hr class="styled_2">
                        
                        <h3>Summer (A) 2018</h3>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “RegistrationDate”" href="#">Registration Date</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “PaymentDue”" href="#">Payment Due</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>February 20 to April 20</td>
                                    
                                    <td>April 21 to End of Term</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Friday, April 20 (5:00PM)</strong></td>
                                    
                                    <td><strong>Due Upon Registration</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>A late registration fee will be charged from April 22 to end of Term</td>
                                    	
                                    <td>&nbsp;</td>
                                 </tr>
                                 	
                                 
                                 <tr>
                                    
                                    <td>February 20 -TIP registration begins</td>
                                    	
                                    <td>&nbsp;</td>
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>May 4 - TIP registration ends</td>
                                    	
                                    <td>&nbsp;</td>
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>May 14 - Drop/Refund Deadline (<strong>Drop classes with 100% refund)</strong></td>
                                    	
                                    <td>&nbsp;</td>
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June 4 - Business Office refund begins</td>
                                    	
                                    <td>&nbsp;</td>
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June 5 - <a href="/admissions/finaid/gettingstarted/using_finaid.php">Financial Aid Disbursement begins </a>(Pell Grants July 11, 2017)
                                    </td>
                                    	
                                    <td>&nbsp;</td>
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>July 6 - VA Deferments due</td>
                                    
                                    <td>&nbsp;</td>
                                    	
                                 </tr>
                                 
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>	
                        	
                        	
                        
                        <hr class="styled_2">
                        
                        <h3>Summer (B) 2018</h3>
                        
                        <div class="table-responsive">
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col" width="100"><a title="Sort on “RegistrationDate”" href="#">Registration Date</a></th>
                                    
                                    <th scope="col" width="100"><a title="Sort on “PaymentDue”" href="#">Payment Due</a></th>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <td>February 20 to April 20</td>
                                    
                                    <td>April 21 to End of Term</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td><strong>Friday, April 20 (5:00PM)</strong></td>
                                    
                                    <td><strong>Due Upon Registration</strong></td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>A late registration fee will be charged from June 16 to end of Term</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>February 20 -TIP registration begins</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>May 4 - TIP registration ends</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June 11 - Drop/Refund Deadline (<strong>Drop classes with 100% refund)</strong></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June 4 - Business Office refund begins</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>June 26 - <a href="/admissions/finaid/gettingstarted/using_finaid.php">Financial Aid Disbursement begins</a></td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>July 6 - VA Deferments due</td>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                        </div>
                        
                        
                        <div>
                           
                           <p><span>Note:</span> Fee payment deadlines for Parts of Term, coincide with the beginning of those terms.
                           </p>
                           
                        </div>
                        	
                        <hr class="styled_2">
                        	
                        <h3>Important Notes</h3>
                        
                        <p>Students enrolling at Valencia College must follow <strong>Withdrawal and Forgiveness Rules</strong> that restrict the number of times a student may withdraw from or repeat a course.&nbsp;
                           These rules were mandated by the State Board of Education for all Florida community
                           colleges.
                        </p>
                        
                        <p>More details can be found in the <a href="http://catalog.valenciacollege.edu/academicpoliciesprocedures/">Academic Policies &amp; Procedures</a> section of the Valencia Catalog.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  		
               </div>
               	
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/important-deadlines.pcf">©</a>
      </div>
   </body>
</html>