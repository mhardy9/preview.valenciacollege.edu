<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Florida Prepaid | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/florida-prepaid.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Florida Prepaid</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Florida Prepaid</h2>
                        
                        <p>
                           The Florida Prepaid Tuition Plan at Valencia is administered by the Business Office.
                           
                           Listed below are the processing procedures for participants:
                           
                        </p>
                        
                        
                        <p>
                           Once you have registered for classes and fees have been assessed we will 
                           <strong>automatically </strong>defer the maximum amount that can be billed 
                           for all participants who are in <strong>good standing</strong> with Florida Prepaid. 
                           <u>You are <strong>not </strong>required to notify the Business Office that you want 
                              to use your prepaid plan.</u>&nbsp; 
                           
                        </p>
                        
                        <p>
                           Please review your account on Atlas to determine 
                           that your Florida Prepaid has been applied. Please contact the Business Office in
                           the 
                           event that your account has not been updated.&nbsp; Your deferral and the Florida 
                           Prepaid payment are limited to the amount of credits remaining in your prepaid plan,
                           
                           which is reflected on your annual statement from Florida Prepaid.
                           
                        </p>
                        
                        
                        <p>
                           At the time we invoice, near the end of each semester, your Florida Prepaid account
                           must 
                           be in good standing and have sufficient credit hours available.&nbsp; If necessary, please
                           
                           notify Florida Prepaid to release any holds.&nbsp; If for any reason we are unable to 
                           complete the billing process we will reverse the credit and payment for your classes
                           will 
                           become due to Valencia <strong>immediately</strong>.
                           
                        </p>
                        
                        
                        
                        <p>
                           Should you have any questions concerning  procedures, please come by the Business
                           Office. 
                           Your Florida Prepaid Tuition Plan representative may be reached at 800-552-GRAD.
                           
                        </p>
                        
                        
                        <p>
                           If for any reason you do not want Valencia College to bill Florida Prepaid on your
                           behalf <strong>or</strong> 
                           would like to reduce the number of hours billed, please complete and sign the 
                           <a href="/students/business-office/documents/Florida_Prepaid_Authorization_Form_000.pdf" target="_blank">authorization form</a> and return 
                           to the Business Office by the <a href="/students/business-office/important-deadlines.php">payment deadline</a>.
                           
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>Important Notes</h3>
                        
                        
                        <p>
                           If you are a beneficiary of a scholarship please complete 
                           the survey that is required from Florida Prepaid each year.
                           
                        </p>
                        
                        
                        <p>
                           <strong>Please be aware that there are fees that are not covered by your Florida Prepaid Plan.
                              
                              </strong>Check your Atlas account to determine these fees and make arrangements to pay for
                           them 
                           by the payment deadline. &nbsp;Payments can be made through the web, by mail or in person
                           at the Business Office.
                           
                        </p>
                        
                        
                        <h3>Fees Not Paid by Florida Prepaid</h3>
                        
                        
                        <ul>
                           
                           <li>
                              Any <strong>special fees</strong> associated with your classes.
                              
                           </li>
                           
                           
                           <li>
                              The <strong>student activity fee</strong> portion of your tuition.
                              
                           </li>
                           
                           
                           <li>
                              The <strong>technology fee</strong> portion of your tuition. Unless you are enrolled in the 
                              local fee program with Florida Prepaid and have separately purchased the TDF plan.
                              
                           </li>
                           
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/florida-prepaid.pcf">©</a>
      </div>
   </body>
</html>