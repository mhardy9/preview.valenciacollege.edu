<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Holds | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/frequently-asked-questions/holds.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/frequently-asked-questions/">Frequently Asked Questions</a></li>
               <li>Holds</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="container margin-60">
                           <div class="main-title">
                              								
                              <h2>Holds</h2>
                              								
                              <p>Frequently Asked Questions</p>
                              							
                           </div>
                           								
                           <div class="row">
                              <div class="col-md-6">
                                 <div class="box_style_2">
                                    												
                                    <h3>My records have a hold that prevents me from registering. I want to go back to school
                                       and register. What do I need to do?
                                    </h3>
                                    												
                                    <p>On your Atlas account, you can view the description of any holds that you may have.
                                       Contact the originating office that placed the hold and speak with them directly.
                                       However, you must settle any financial obligations before you will be allowed to register.
                                       You can pay online via your Atlas account if your account has not be submitted to
                                       a collection agency. If your account has been submitted, you would need to pay the
                                       collection agency directly.
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-6">
                                 <div class="box_style_2">
                                    												
                                    <h3>Will my hold be removed if I pay my debt through Atlas?</h3>
                                    												
                                    <p>Financial related holds are removed from your account over night by paying online
                                       through your Atlas account. However, you may also email the Business Office at <a href="mailto:businessoffice@valenciacollege.edu">businessoffice@valenciacollege.edu</a> (Please be sure to include your student identification number in your e-mail) or
                                       visit your campus Business Office to have the hold removed from your account.
                                    </p>
                                    											
                                 </div>
                              </div>
                           </div>
                           							
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/frequently-asked-questions/holds.pcf">©</a>
      </div>
   </body>
</html>