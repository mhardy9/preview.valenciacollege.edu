<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>1098-T Payment Statements | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/frequently-asked-questions/1098t.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/frequently-asked-questions/">Frequently Asked Questions</a></li>
               <li>1098-T Payment Statements</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="container margin-60">
                           <div class="main-title">
                              								
                              <h2>1098-T Payment Statements</h2>
                              								
                              <p>Frequently Asked Questions</p>
                              							
                           </div>
                           								
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>What is the 1098-T Tuition Payment Statement?</h3>
                                    												
                                    <p>The Taxpayer Relief Act of 1997 states that eligible educational institutions, such
                                       as Valencia College, are required to file with the Internal Revenue Service (IRS)
                                       and furnish a Form 1098-T to students to assist them in determining eligibility for
                                       the American Opportunity Credit, Lifetime Learning Credit, or the Tuition and Fees
                                       deduction. &nbsp;Valencia College has elected to report amounts billed for qualified tuition
                                       and related expenses instead of total payments received. Therefore, the amounts reported
                                       on the 1098-T may not be the amounts you would report on your tax return.
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>When and where will the Form 1098- be provided? Can I “GO GREEN!” and consent to access
                                       my Form 1098-T online?
                                    </h3>
                                    												
                                    <p>Students at Valencia College can receive their 1098-T tuition statement form electronically
                                       by logging into your ATLAS account, and clicking on <strong>Students</strong> Tab. Under <strong>Business Office</strong> click on <strong>IRS Form 1098-T (Electronic Delivery).</strong> 1098-T Tuition Statement form will be available by end of January of each year.
                                    </p>
                                    												
                                    <p>Benefits to receiving the Form 1098-T electronically vs. through regular postal mail:</p>
                                    												
                                    <ul>
                                       													
                                       <li>Online delivery provides 24/7 access to the Form 1098-T.</li>
                                       													
                                       <li>Online delivery eliminates the chance that the Form 1098-T will get lost, misdirected
                                          or delayed during delivery, or misplaced once the student receives it.
                                       </li>
                                       													
                                       <li>Students can receive their Form 1098-T even while traveling or on assignment away
                                          from their home address.
                                       </li>
                                       												
                                    </ul>
                                    												
                                    <p></p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>Why didn’t I receive my 1098-T?</h3>
                                    												
                                    <p></p>
                                    												
                                    <ul>
                                       													
                                       <li>Valencia College is not required to file Form 1098-T or furnish a statement for:</li>
                                       													
                                       <li>Non-credit program courses for which no academic credit is offered, even if the student
                                          is otherwise enrolled in a degree program
                                       </li>
                                       													
                                       <li>Nonresident alien students, unless requested by the student</li>
                                       													
                                       <li>Students whose qualified tuition and related expenses are entirely waived or paid
                                          entirely with scholarships and grants.
                                       </li>
                                       													
                                       <li>Students whose qualified tuition and related expenses are covered by a formal billing
                                          arrangement between an institution and the student's employer or a government entity,
                                          such as the Department of Veterans' Affairs or the Department of Defense.
                                       </li>
                                       													
                                       <li><strong>Am I eligible to receive the</strong> <strong>American Opportunity</strong>, <strong>Lifetime Learning credit or Tuition and Fees deduction?</strong></li>
                                       													
                                       <li>Refer to <a href="http://www.irs.gov/pub/irs-pdf/p970.pdf" target="_blank">IRS publication 970</a> and <a href="http://www.irs.gov/newsroom/article/0,,id=213044,00.html" target="_blank">Tax Benefits for Education: Information Center</a> for further information regarding the 1098-T, or consult your tax advisor. Please
                                          note that the taxpayer is responsible for determining eligible educational expenses,
                                          and should not request from the college instructions related to the eligibility requirements
                                          or calculation of allowable education credits or tuition and fees deduction.
                                          													
                                       </li>
                                       												
                                    </ul>
                                    												
                                    <p></p>
                                    											
                                 </div>
                              </div>
                           </div>
                           							
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/frequently-asked-questions/1098t.pcf">©</a>
      </div>
   </body>
</html>