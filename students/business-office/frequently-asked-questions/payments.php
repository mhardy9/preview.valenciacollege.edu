<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Payments | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/frequently-asked-questions/payments.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/frequently-asked-questions/">Frequently Asked Questions</a></li>
               <li>Payments</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="container margin-60">
                           <div class="main-title">
                              								
                              <h2>Payment Questions</h2>
                              								
                              <p>Frequently Asked Questions</p>
                              								
                              <div>
                                 <p><strong>Please Note:</strong> If there is any balance remaining on your account, after the last day to pay, and
                                    arrangements have 
                                    not been made to defer your fees, you may be deleted from ALL registered course work.
                                 </p>
                              </div>
                              							
                           </div>
                           								
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>How can I pay for classes?</h3>
                                    												
                                    <p></p>
                                    												
                                    <ul>
                                       													
                                       <li>
                                          														<strong>Pay online through your Atlas account</strong><br>
                                          														Log into <a href="https://atlas.valenciacollege.edu">Atlas</a><br>
                                          														Click on Students<br>
                                          														Click on Account Summary and Tuition Payment Options<br>
                                          														Click on Submit your Credit Card Payment Online or Sign up for a TIP
                                          Payment Plan.
                                          													
                                       </li>
                                       													
                                       <li style="list-style: none"><br></li>
                                       													
                                       <li><strong>Mail your payment to address</strong><br>
                                          													Post Office Box 4913, Orlando, Florida 32802. Make sure it will be received
                                          (not postmarked) prior to the last day to pay.
                                       </li>
                                       													
                                       <li style="list-style: none"><br></li>
                                       													
                                       <li><strong>Pay in person</strong><br>
                                          													Go to any of the Business Office locations.
                                       </li>
                                       													
                                       <li style="list-style: none"><br></li>
                                       													
                                       <li>
                                          														<strong>Parents and authorized users</strong><br>
                                          														Go to <a href="https://commerce.cashnet.com/cashnetb/selfserve/ebillLogin.aspx?&amp;client=VCC_PROD&amp;PV=N" target="_blank">Valencia ePayment</a> login with your user name and password.
                                          													
                                       </li>
                                       												
                                    </ul>
                                    												
                                    <p></p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>Where can I find out how much money I owe the college?</h3>
                                    												
                                    <p>You can get that information on your <a href="https://atlas.valenciacollege.edu">Atlas</a> account.<br></p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>I'm a parent and I want to pay for my child's classes before he/she loses them. How
                                       can I pay?
                                    </h3>
                                    												
                                    <p>Payment is ultimately the responsibility of the student. Due to federal regulation,
                                       the college is unable to release information without the students' permission. For
                                       further details, click on the following link: <a href="http://www.ed.gov/offices/OM/fpco/ferpa/" target="_blank">http://www.ed.gov/offices/OM/fpco/ferpa/</a></p>
                                    												
                                    <p>The student may create a Parent PIN for any person he/she wishes to provide the ability
                                       to view his/her current balance and make secure online payments by electronic check
                                       or credit card. Only the student can create Parent PINs and reset passwords for these
                                       accounts. The student may also inactivate a Parent PIN at any time at his/her discretion.<br></p>
                                    												
                                    <p>To create a Parent PIN:</p>
                                    												
                                    <ol>
                                       												
                                       <li>Login to Atlas</li>
                                       												
                                       <li>Click on Student tab</li>
                                       												
                                       <li>Under the “Student Resources” click to expand "Business Office"</li>
                                       												
                                       <li>Click on "Account Summary and Tuition Payment Options"</li>
                                       												
                                       <li>Click on "Submit Your Payment Online"</li>
                                       												
                                       <li>Click on "Add New" under Parent PINs to create an account</li>
                                       													
                                       <li>If a Parent or Authorized User has been setup by the Student, log onto <a href="https://commerce.cashnet.com/vccpay" target="_blank">Valencia ePayment</a> to make a payment.
                                       </li>
                                    </ol>
                                    											
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>I know my tuition check has been returned, and I want to pay my tuition now so I will
                                       not lose my classes (Valencia shows no record of this check being returned).
                                    </h3>
                                    												
                                    <p>Wait for notification from Valencia before you proceed to pay for your classes. Checks
                                       are automatically redeposited by our bank. If the check is returned to us, we will
                                       void the initial payment and send you notification of when the account must be settled.
                                       There will be an additional charge for the returned check added to your account, and
                                       you must pay either by credit card or cash. No personal checks will be accepted for
                                       payment of this debt.
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>What is the last day for me to drop classes and get a 100% refund?</h3>
                                    												
                                    <p>Click <a href="/students/business-office/important-deadlines.php">here</a> to view current schedule for payment and refund dates.
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>I registered for classes on the Internet and the charges are too high. Why?</h3>
                                    												
                                    <p>In-state fees are $99.06 per credit hour and the out of state fees are $375.22 per
                                       credit hour. Any special fees associated with a class are designated by the department
                                       and the amount is listed in the <a href="/students/catalog/index.php">college catalog</a> by the course description. You can learn more about the fee schedule by visit our
                                       <a href="/students/business-office/important-deadlines.php">Important Dates</a> page. If you are attempting a class for the third time, you could be getting charged
                                       the full cost of instruction. If residency is your problem, you must visit the Answer
                                       Center for clarification.
                                    </p>
                                    											
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>Why do I have to pay a Financial Aid fee if I don't get Financial Aid?</h3>
                                    												
                                    <p>All students pay these fees. They are mandated by the State of Florida legislature.
                                       Go to <a href="http://www.leg.state.fl.us/statutes" target="_blank">http://www.leg.state.fl.us/statutes</a>. Title XLVIII Chapter 1009-23. See #8A (Financial Aid).
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>Why do I have to pay Student Activities fees? I don't participate in any clubs.</h3>
                                    												
                                    <p>All students pay these fees. They are mandated by the State of Florida legislature.
                                       Go to <a href="http://www.leg.state.fl.us/statutes" target="_blank">http://www.leg.state.fl.us/statutes</a>. Title XLVIII Chapter 1009-23. See #7 (Student Activities).
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>What forms of payment are acceptable for payment of classes?</h3>
                                    												
                                    <p>Valencia College accepts cash, American Express, Discover, MasterCard, Visa, money
                                       orders and personal checks. Checks must be made payable to Valencia College.
                                    </p>
                                    											
                                 </div>
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>My employer wants to pay for my classes. What do I need to do?</h3>
                                    												
                                    <p>If all or part of your tuition is being paid by an approved external agency (ex. Vocational
                                       Rehabilitation or Workforce of Central Florida), you must present your current term
                                       form of authorization (letter, voucher, etc) within the payment timeframe (see <a href="/students/business-office/important-deadlines.php">Payments and Refund Dates</a>). If your employer is not an approved agency, they will need to contact Valencia's
                                       accounts receivable department to set up an agreement.
                                    </p>
                                    											
                                    												
                                    <h3>Where do I get a VA deferment?</h3>
                                    												
                                    <p>Visit the <a href="/admissions/finaid/index.php">Financial Aid Services Office</a> at any campus.
                                    </p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>If I register late, what is the charge?</h3>
                                    												
                                    <p>Students who register for the first time after payment deadline or dropped for all
                                       classes for non-payment will be assessed a $50.00 Late Registration Fee. <a href="calendar/index.php">Important Dates &amp; Deadlines</a></p>
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												
                                    <h3>My employer needs a detailed receipt of classes paid. Where can I get one?</h3>
                                    												
                                    <p>It can be requested through any campus Business Office.</p>
                                    											
                                 </div>
                              </div>
                           </div>
                           							
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/frequently-asked-questions/payments.pcf">©</a>
      </div>
   </body>
</html>