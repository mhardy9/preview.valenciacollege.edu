<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Third Party Payments | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/frequently-asked-questions/third-party-payments.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/frequently-asked-questions/">Frequently Asked Questions</a></li>
               <li>Third Party Payments</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="container margin-60">
                           <div class="main-title">
                              								
                              <h2>Third Party Payments</h2>
                              								
                              <p>Frequently Asked Questions</p>
                              							
                           </div>
                           								
                           <div class="row">
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    											
                                    
                                    <h3>I have Florida Pre Paid College Plan. What do I have to do?</h3>
                                    
                                    <p>
                                       The Florida Pre Paid College Plan will cover your fees automatically. 
                                       After registering for classes, please check your Atlas account 
                                       to ensure that the charges were covered by the Plan.&nbsp; If 
                                       not, please visit the Business Office. Your fees will 
                                       be credited automatically. Please allow  24-48 hours for processing. Call 1-800-552-4723
                                       for more 
                                       information or visit their website at <a href="http://www.myfloridaprepaid.com/" target="_blank">
                                          www.myfloridaprepaid.com</a>
                                       
                                    </p>
                                    											
                                    											
                                    <h3>Vocational Rehabilitation is paying for my classes. What do I need to do?</h3>
                                    
                                    <p>
                                       Your counselor should submit your authorization form to Valencia's accounts receivable
                                       
                                       department to credit your account. Check the account balance page of your Atlas account
                                       
                                       to ensure proper credit has been given. You may also visit the Business Office at
                                       any campus.
                                       
                                    </p>
                                    	
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    												   
                                    <h3>If I have Florida Pre Paid, why do I have to pay anything?</h3>
                                    
                                    <p>
                                       The first plans issued for Florida Pre-Paid did not cover the Student Activity Fee
                                       , Technology Fee  
                                       nor did it cover any special fees. If you have any question regarding your Florida
                                       Prepaid program, 
                                       contact 1-800-552-4723 or you may visit their website at <a href="http://www.myfloridaprepaid.com/" target="_blank">
                                          www.myfloridaprepaid.com</a>
                                       
                                    </p>
                                    											
                                    											
                                 </div>
                              </div>
                              <div class="col-md-4">
                                 <div class="box_style_2">
                                    											
                                    <h3>What do I do if I don’t want to use my Florida Prepaid?</h3>
                                    
                                    <p>
                                       If for any reason you do not want Valencia College to bill Florida Prepaid on your
                                       behalf or 
                                       would like to reduce the number of hours billed, please complete and sign the 
                                       <a href="/students/business-office/documents/florida-prepaid-authorization-form.pdf" target="_blank">authorization form</a> 
                                       and return to the Business Office by the <a href="/students/business-office/important-deadlines.php">payment deadline</a>. 
                                       
                                    </p>
                                    												
                                    											
                                    											
                                 </div>
                              </div>
                           </div>
                           							
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/frequently-asked-questions/third-party-payments.pcf">©</a>
      </div>
   </body>
</html>