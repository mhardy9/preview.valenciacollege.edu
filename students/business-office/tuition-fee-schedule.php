<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tuition &amp; Fee Schedule  | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-fee-schedule.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Tuition &amp; Fee Schedule </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Tuition &amp; Fee Schedule</h2>
                        				
                        <p>Valencia has flexible payment options, for more information visit the <a href="tuition-installment-plan/">Tuition Installment Plan</a> site.
                        </p>
                        				
                        <p>If you interested in financial aid opportunities, visit the <a href="/finaid/programs/grants.php">Financial Aid Grants</a> site. 				Financial aid assistance is offered on any Valencia campus at the <a href="/answer-center/">Answer Center</a>.
                        </p>
                        				
                        <p>For detailed tuition and fees information, read the <a href="http://catalog.valenciacollege.edu/financialinformationfees/" target="_blank">Financial Information &amp; Fees</a> section of the Valencia Catalog.
                        </p>
                        				
                        <h3>Registration Fees</h3>
                        				
                        <p>Associate Fees Baccalaureate Fees <strong> In State Tuition</strong> $103.06 per credit hour* $112.19 per credit hour* <strong>Out of State Tuition</strong> $390.96 per credit hour* $427.59 per credit hour*
                        </p>
                        				
                        <p>*Registration fees are subject to change without notice. Please check your Atlas account
                           for your current balance.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Non-Refundable Fees</h3>
                        				
                        <div class="table-responsive">
                           					
                           <table class="table table table-striped add_bottom_30">
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <th scope="col" width="50">
                                       									<a href="#" title="Sort on “APPLICATION”">APPLICATIONT</a>
                                       								
                                    </th>
                                    								
                                    <th scope="col" width="50">
                                       									<a href="#" title="Sort on “FEES”">FEES</a>
                                       								
                                    </th>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <td>Associate's Application</td>
                                    								
                                    <td>$35.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Bachelor's Application</td>
                                    								
                                    <td>$35.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Readmit Application</td>
                                    								
                                    <td>$35.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Official Transcript Request</td>
                                    								
                                    <td>$3.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Late Registration</td>
                                    								
                                    <td>$50.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Returned Check - Face Value of Check</td>
                                    								
                                    <td>NSF Fee</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>$50.00 or less</td>
                                    								
                                    <td>$25.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>$50.01 through $300.00</td>
                                    								
                                    <td>$30.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>$300.01 or more</td>
                                    								
                                    <td>$40.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>VA - Deferment Late</td>
                                    								
                                    <td>$25.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Bad Debt Collection</td>
                                    								
                                    <td>25% of outstanding balance plus fees</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Independent Study Fee</td>
                                    								
                                    <td>$25.00 per credit hour plus cost of class</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Health Related Program Application</td>
                                    								
                                    <td>$15.00</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>School of Public Safety Application</td>
                                    								
                                    <td>$100.00</td>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              					
                           </table>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-fee-schedule.pcf">©</a>
      </div>
   </body>
</html>