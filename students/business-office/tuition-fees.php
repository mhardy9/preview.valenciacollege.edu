<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tuition &amp; Fee Schedule | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-fees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Tuition &amp; Fee Schedule</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Tuition &amp; Fee Schedule</h2>
                        
                        
                        
                        <p>
                           Valencia has flexible payment options, for more information visit the <a href="tuition-installment-plan/index.php">Tuition Installment Plan</a> site. 
                           
                        </p>
                        
                        
                        <p>
                           If you interested in financial aid opportunities, visit the <a href="../finaid/programs/grants.php">Financial Aid Grants</a> site. 
                           Financial aid assistance is offered on any Valencia campus at the <a href="../answer-center/index.php">Answer Center</a>. 
                           
                        </p>
                        
                        
                        <p>
                           For detailed tuition and fees information, read the 
                           <a href="http://catalog.valenciacollege.edu/financialinformationfees/" target="_blank">Financial Information &amp; Fees</a> 
                           section of the Valencia Catalog.
                           
                        </p>
                        
                        
                        <h3>Registration Fees</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">Associate Fees</div>
                                 
                                 <div data-old-tag="td">Baccalaureate Fees</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>In State Tuition</strong></div>
                                 
                                 <div data-old-tag="td">$103.06 per credit hour*</div>
                                 
                                 <div data-old-tag="td">$112.19 per credit hour*</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Out of State Tuition</strong></div>
                                 
                                 <div data-old-tag="td">$390.96 per credit hour*</div>
                                 
                                 <div data-old-tag="td">$427.59 per credit hour*</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>
                           *Registration fees are subject to change without notice. Please check your Atlas account
                           for your current balance. 
                           
                        </p>
                        
                        
                        
                        
                        <h3>Non-Refundable Fees</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>Associate's Application</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$35.00</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>Bachelor's  Application</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$35.00</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Readmit Application </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$35.00</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Official Transcript Request </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$3.00</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>Late Registration</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$50.00</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>Returned Check - Face Value 
                                       of Check
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NSF Fee</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">$50.00 or less</div>
                                 
                                 <div data-old-tag="td">
                                    <p>$25.00</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> $50.01 through $300.00</div>
                                 
                                 <div data-old-tag="td">
                                    <p>$30.00</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td"> $300.01 or more</div>
                                 
                                 <div data-old-tag="td">
                                    <p>$40.00</p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>VA - Deferment Late</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$25.00</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>Bad Debt Collection</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>25% of outstanding 
                                       balance plus fees
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>Independent Study 
                                       Fee
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$25.00 per credit 
                                       hour plus cost of class
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>Health Related Program Application</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$15.00 </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <div>School of Public Safety Application </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$100.00</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-fees.pcf">©</a>
      </div>
   </body>
</html>