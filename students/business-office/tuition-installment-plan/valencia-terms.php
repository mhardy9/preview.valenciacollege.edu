<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Valencia Terms &amp; Conditions | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-installment-plan/valencia-terms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/tuition-installment-plan/">Tuition Installment Plan</a></li>
               <li>Valencia Terms &amp; Conditions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Valencia Terms &amp; Conditions</h2>
                        				
                        <p>Acceptance of the following terms and conditions are being set forth as an additional
                           requirement to use the Valencia College, Tuition Installment Plan.
                        </p>
                        				
                        <p><strong>1.</strong> I authorize Valencia College to alter my TIP agreement, to equal the total of all
                           tuition, fees and bookstore charges due on my account for the current term.
                        </p>
                        				
                        <p><strong>2</strong>. I understand that if Financial Aid is awarded before or after my agreement has been
                           established, adjustments and changes will be made to the account. If Financial Aid
                           is insufficient to cover the remaining balance, my payment obligation will continue
                           with Nelnet Business Solutions. If Financial aid is sufficient to cover my entire
                           balance, my Tuition Installment Plan will be reduced to zero ($0.00) and no payments
                           will be due to Nelnet Business Solutions (<u>Effective Fall 2009</u> ).&nbsp; I also understand that after a certain date during the semester, my TIP agreement
                           will not change upon receiving financial aid.&nbsp; I will receive a refund in the full
                           amount of my financial aid when posted to my account and will make payments to Nelnet
                           Business Solutions as scheduled.&nbsp;
                        </p>
                        				
                        <p><strong>3.</strong> I am personally liable to Valencia College for any tuition, fee and bookstore charge
                           debts that remain on my account as a result of the following:
                        </p>
                        				
                        <ul>
                           					
                           <li>I am in default of my TIP agreement with Nelnet Business Solutions and all monies
                              due have been transferred back to my Valencia account.
                           </li>
                           					
                           <li>I have added a class(es) after the Increment deadline and my TIP agreement could not
                              be updated for the new balance.
                           </li>
                           					
                           <li>I have charged books against my financial aid instead of my TIP agreement.</li>
                           					
                           <li>I terminated my TIP agreement and I must pay my tuition and fees to Valencia College
                              by the payment deadline or my registration will be deleted.
                           </li>
                           				
                        </ul>
                        				
                        <p><strong>4.</strong> A refund of payments made to my TIP agreement will be returned to Valencia College
                           within 45 - 60 days from the Add/Drop deadline. Valencia College will then refund
                           the student, not the responsible party on the TIP agreement (if the two are not the
                           same) through HigherOne <u></u> based upon the college's existing refund policy.
                        </p>
                        				
                        <p><strong>5.</strong> Failure to pay fees owed on my TIP agreement, or otherwise defaulting on the agreement,
                           will result in a financial hold placed on my Valencia account. This action will prevent
                           me from receiving grades or transcripts and will also prevent me from registering
                           in a future term at Valencia.
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-installment-plan/valencia-terms.pcf">©</a>
      </div>
   </body>
</html>