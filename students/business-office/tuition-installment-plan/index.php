<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Tuition Installment Plan (TIP) | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-installment-plan/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Tuition Installment Plan</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Tuition Installment Plan (TIP)</h2>
                        				
                        <p>Valencia College offers a <strong>T</strong>uition <strong>I</strong>nstallment <strong>P</strong>lan (<strong>TIP</strong>) through Nelnet Business Solutions (NBS) for all students requiring assistance with
                           managing their Educational Expenses. Since the cost of higher education has increased
                           in recent years, the TIP program will allow students to pay for tuition and book expenses
                           over a period of time rather than one lump sum payment during the first few weeks
                           of school. Valencia College has contracted with Nelnet Business Solutions to administer
                           this program. This company will be responsible for the enrollment, as well as receiving
                           the monthly payments.
                        </p><a href="http://www.mycollegepaymentplan.com/valencia" target="_blank">2016-2017 TIP Brochure</a>
                        				
                        <h3>Fall 2017 Tuition Installment Plan Important Dates</h3>
                        				
                        <div class="table-responsive">
                           					
                           <table class="table table table-striped add_bottom_30">
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <th scope="col" width="100">
                                       									<a href="#" title="Sort on “PAYMENT”">PAYMENT</a>
                                       								
                                    </th>
                                    								
                                    <th scope="col" width="100">
                                       									<a href="#" title="Sort on “PHONENUMBER”">FEES</a>
                                       								
                                    </th>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <td>TIP registration begins</td>
                                    								
                                    <td>July 3</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>First day to purchase books ($400 credit limit)</td>
                                    								
                                    <td>August 1</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td><strong>TIP registration ends</strong></td>
                                    								
                                    <td><strong>September 5</strong></td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Last day to purchase books</td>
                                    								
                                    <td>September 6</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Last day to <strong><em>increase,</em></strong> <strong><em>decrease</em></strong> or <strong><em>terminate</em></strong> a contract
                                    </td>
                                    								
                                    <td>September 6</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td><strong>1st Refund</strong> - Terminated Contracts/Not Guaranteed
                                    </td>
                                    								
                                    <td>September 22</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td><strong>2nd Refund</strong> - Tuition Overpayment/Guaranteed
                                    </td>
                                    								
                                    <td>October 13</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Date Financial Aid must be <strong>Authorized</strong> on your account to affect/change your TIP agreement
                                    </td>
                                    								
                                    <td>September 4</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Date that agreement must be current by in order to prevent Termination</td>
                                    								
                                    <td>September 14</td>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              					
                           </table>
                           				
                        </div>
                        				
                        <p>For more important dates take a look at the Business Office <a href="../important-deadlines.php">Important Deadlines</a>, or Valencia's Collegewide <a href="/academics/calendar/">Important Dates &amp; Deadlines</a></p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Financial Aid Students</h3>
                        				
                        <p>If Financial Aid is posted on your Valencia’s account <strong>before</strong> you enroll in the Tuition Installment Plan (TIP) program, the TIP account will be
                           set-up for the remaining balance due since financial aid will cover a portion of your
                           tuition fees.&nbsp;
                        </p>
                        				
                        <p>If Financial Aid is posted on your Valencia’s account <strong>after</strong> you enroll in the Tuition Installment Plan (TIP) program, one of the following will
                           occur:
                        </p>
                        				
                        <ul>
                           					
                           <li>If your Financial Aid is sufficient to cover the remaining balance on the Tuition
                              Installment Plan account, your balance will be reduced to zero and there will be no
                              other payments due to Nelnet Business Solutions.
                           </li>
                           				
                        </ul>
                        				
                        <ul>
                           					
                           <li>If your Financial Aid award is not sufficient to cover the remaining balance, your
                              Tuition Installment Plan account will be reduced by the amount of the Financial Aid
                              awarded and a lesser payment will be due monthly. You will receive a notice via email
                              or by mail each time there is a change on your account.
                           </li>
                           				
                        </ul>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Agency Sponsored (3rd Party) Students</h3>
                        				
                        <p>If you enroll in the Tuition Installment Plan before receiving authorization, please
                           be aware that the authorization amount may only cover part or none of the tuition
                           balance. Students will be responsible for paying any monies owed to Nelnet Business
                           Solutions.
                        </p>
                        				
                        <p><strong>IMPORTANT:</strong> Valencia College will not be able to make any changes or adjustments after a certain
                           date during the semester. Please check the deadline dates for adjustments by reviewing
                           TIP - Important Dates.
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-installment-plan/index.pcf">©</a>
      </div>
   </body>
</html>