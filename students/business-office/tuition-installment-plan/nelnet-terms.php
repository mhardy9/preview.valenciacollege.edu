<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Nelnet Business Solutions Terms &amp; Conditions | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-installment-plan/nelnet-terms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/tuition-installment-plan/">Tuition Installment Plan</a></li>
               <li>Nelnet Business Solutions Terms &amp; Conditions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Nelnet Business Solutions Terms &amp; Conditions</h2>
                        				
                        <p><strong>AUTHORIZATION</strong> : TIP is administered for Valencia College (Institution) by Nelnet Business Solutions
                           of Lincoln , NE. Nelnet has contracted with the Institution to act as its agent for
                           the collection of tuition and/or fees. As the Responsible Party who has submitted
                           this agreement, you accept and agree to be bound by the agreement's terms and conditions
                           until the total amount owed is paid in full. Additionally, you authorize Nelnet to
                           process payments and fees from the account provided or any subsequent account provided.
                           Your authorization will terminate when the total balance due has been paid (including
                           fees, unless waived).
                        </p>
                        				
                        <p><strong>DOWN AND FULL PAYMENTS</strong> : If you elect to make a down payment, that payment will be processed by Nelnet <strong><u>immediately</u></strong> according to the payment method you have selected. Should your down payment fail,
                           Nelnet will notify you that your agreement has been terminated. You will be instructed
                           to re-enroll through Nelnet or make payment arrangements directly with your community
                           college.
                        </p>
                        				
                        <p><strong>ENROLLMENT FEE</strong> : The nonrefundable enrollment fee will be deducted from the account provided within
                           14 days of the agreement being posted to the Nelnet system. The nonrefundable enrollment
                           fee is based upon the number of payments selected for each Nelnet agreement period.
                           If the entire balance due is not paid within the agreement period or twelve (12) months,
                           whichever period is shorter, an agreement is in renewal. On the renewal date of such
                           an agreement, Nelnet may assess a new enrollment fee. Fees are subject to change in
                           future academic years. The nonrefundable enrollment fee(s) applicable to this agreement
                           is (are) shown at the bottom of this page.
                        </p>
                        				
                        <p><strong>RETURNED PAYMENTS</strong> : In the event that your financial institution returns a monthly payment, a $25 returned
                           payment fee will be automatically deducted from the account provided within 20 days.
                           A returned payment fee will be assessed for each payment attempt that is returned.
                           If any fees are returned, they will be reattempted. Fees are subject to change in
                           future academic years.
                        </p>
                        				
                        <p><strong>PAYMENT METHOD</strong> : If you wish to change from a checking or savings account to a credit card, or vice
                           versa, you will need to complete a new agreement and an additional nonrefundable enrollment
                           fee will be automatically deducted.
                        </p>
                        				
                        <p><strong>PAYMENT DATES</strong> : If your payment date falls on a weekend or a banking holiday observed by the Federal
                           Reserve, the payment will be attempted on the next business day. Although Nelnet specifies
                           the date each payment will occur, your financial institution determines the time of
                           day the payment is debited to the account.
                        </p>
                        				
                        <p><strong>CUSTODIAL ACCOUNT</strong> : Collected funds shall be held by Nelnet as your agent until remitted to the Institution.
                           Depending upon the institution's policy, payments returned by your financial institution
                           will be automatically reattempted. Refunds of any money paid to Nelnet, except for
                           any applicable fees, will be handled by the Institution according to its refund policy.
                           Interest earned on custodial funds is payable to Nelnet.
                        </p>
                        				
                        <p><strong>CHANGES TO AGREEMENTS</strong> :
                        </p>
                        				
                        <p>A. You may make changes to the information you provided in this agreement by contacting
                           the Institution. The timely application of changes depends on when they are received
                           by Nelnet; Nelnet may refuse to apply changes prior to the next scheduled payment
                           date if Nelnet determines, for whatever reason, that it does not have sufficient time
                           to act on them. In the event that you authorize additional services from the Institution,
                           or in the event that additional fees are assessed by the Institution in accordance
                           with its policies, you understand that the total balance due and/or payment amount
                           will change. You agree that your authorization of any such change shall constitute
                           your authorization to change the payment amount, and/or to continue payments until
                           the total balance due is paid in full. If you, as the Responsible Party, are not the
                           student, you authorize the student to make changes to his or her schedule or activities
                           and agree to be bound by any such changes. You do not require Nelnet or the Institution
                           to send advance notice of any adjustments resulting from any such changed authorization,
                           which includes any reduction in the balance due and/or payment as a result of financial
                           aid, or any other similar cause. However, a copy of any such changed authorization
                           is to be provided to you by the Institution.
                        </p>
                        				
                        <p>B. If there will be any change in the preauthorized payment amount other than a change
                           made by you, as described above, the Institution will give you notice of such changed
                           payment amount at least ten (10) days in advance of the next scheduled payment.
                        </p>
                        				
                        <p>C. You may revoke your authorization by sending Nelnet a signed, written notification
                           or an e-mail; upon receipt, Nelnet will immediately terminate your agreement. However,
                           terminating your agreement with Nelnet in no way affects your obligation to pay Nelnet,
                           and you will be charged another nonrefundable enrollment fee if you need to begin
                           a new agreement.
                        </p>
                        				
                        <p><strong>CONFIRMATION</strong> : Any and all inconsistencies in the information provided will be resolved in the
                           confirmation notification sent to you from Nelnet. Changes made by the Institution
                           that are received by Nelnet before the notification is sent may also be included.
                           In either event, the confirmation notification shall be controlling.
                        </p>
                        				
                        <p><strong>REFUND/TERMINATION DISCLOSURE</strong> : Nelnet guarantees remittance of tuition and/or fees it collects from you. Nelnet
                           reserves the right to terminate this agreement at any time if you are not current
                           on payments or fees due Nelnet. If Nelnet exercises this right, the student on this
                           agreement may be dropped from class(es). Failure to pay any debt to Nelnet could also
                           result in holds being placed on transcript requests, registration for subsequent academic
                           terms, and graduation. If this agreement is terminated for any reason, money collected
                           prior to termination (if any) will be remitted to the Institution, less any fees due
                           Nelnet. Refunds, if any, will be handled by the Institution and will not be issued
                           until 60 days after the last day of the add-and-drop period. As noted above, enrollment
                           fees and returned payment fees are nonrefundable. If this agreement is terminated,
                           Nelnet will attempt to notify you immediately via the e-mail address Nelnet has on
                           file. If the e-mail address is invalid, a letter will be mailed.
                        </p>
                        				
                        <p><strong>GOVERNING LAW</strong> : You acknowledge that the origination of ACH transactions to your account must comply
                           with the provisions of U.S. law. This agreement shall be governed by the laws of the
                           State of Nebraska . The District Court of Lancaster County, NE, shall be the sole
                           venue for filing any action. This agreement should in no way be construed to be a
                           lender-borrower agreement between Nelnet and the Institution or Nelnet and you.
                        </p>
                        				
                        <p>Arbitration: Upon the demand of you or Nelnet, any dispute concerning the parties'
                           duties or liabilities under this agreement shall be resolved by binding arbitration
                           in accordance with the terms of this agreement. Arbitration proceedings shall be administered
                           by the American Arbitration Association ("AAA") or such other administrator as the
                           parties shall mutually agree upon in accordance with the AAA Commercial Arbitration
                           Rules. All disputes submitted to arbitration shall be resolved in accordance with
                           the Federal Arbitration Act (Title 9 of the United States Code), notwithstanding any
                           conflicting choice of law provision. The arbitration shall be conducted at a location
                           in Lincoln , Nebraska selected by the AAA or other administrator. All statutes of
                           limitation applicable to any dispute shall apply to any arbitration proceeding. All
                           discovery activities shall be expressly limited to matters directly relevant to the
                           dispute being arbitrated. Judgment upon any award rendered in arbitration may be entered
                           in any court having jurisdiction.
                        </p>
                        				
                        <p><strong>Notice of Privacy Statement</strong> : Nelnet acts as agent for institutions in administering their payment plans. In
                           the course of providing payment plan services for these institutions, Nelnet receives
                           personal and financial information from their customers. It is the policy of Nelnet
                           and its subsidiaries to protect all information provided to us by all participants
                           in the Nelnet payment plans. From time to time Nelnet may share this information with
                           you and with the institution for which it is acting as an agent. Nelnet does not sell
                           or share any nonpublic personal information or client lists to any third party, except
                           as agreed to by you, or as may be necessary to complete a transaction in the ordinary
                           course of business, or as required under an applicable law. To protect the information
                           from access by unauthorized parties, Nelnet maintains physical, procedural, and electronic
                           safeguards.
                        </p>
                        				
                        <p><strong>SPECIAL NOTICE REGARDING FINANCIAL AID</strong> : Please do not assume your balance will automatically be adjusted if you receive
                           financial aid or a class is added or dropped. You should review your agreement balance
                           online through My Nelnet Account or the Institution's website, if applicable, to confirm
                           the change.
                        </p>
                        				
                        <p>The nonrefundable enrollment fee to budget payment(s) through Nelnet:</p>
                        				
                        <div class="table-responsive">
                           					
                           <table class="table table table-striped add_bottom_30">
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <th scope="col" width="100">
                                       									<a href="#" title="Sort on “PAYMENT”">PAYMENT</a>
                                       								
                                    </th>
                                    								
                                    <th scope="col" width="100">
                                       									<a href="#" title="Sort on “PHONENUMBER”">FEES</a>
                                       								
                                    </th>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              						
                              <tbody>
                                 							
                                 <tr>
                                    								
                                    <td>Down payment plus 4 monthly payments</td>
                                    								
                                    <td>$30</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Down payment plus 3 monthly payments</td>
                                    								
                                    <td>$35</td>
                                    							
                                 </tr>
                                 							
                                 <tr>
                                    								
                                    <td>Down payment plus 2 monthly payments</td>
                                    								
                                    <td>$40</td>
                                    							
                                 </tr>
                                 						
                              </tbody>
                              					
                           </table>
                           				
                        </div>
                        				
                        <p>The enrollment fee will be attempted within 14 days.</p>
                        				
                        <p><strong>REFUND/TERMINATION DISCLOSURE</strong> : Nelnet guarantees remittance of tuition and/or fees it collects from you. Nelnet
                           reserves the right to terminate this agreement at any time if you are not current
                           on payments or fees due Nelnet. If Nelnet exercises this right, the student on this
                           agreement may be dropped from class(es). Failure to pay any debt to Nelnet could also
                           result in holds being placed on transcript requests, registration for subsequent academic
                           terms, and graduation. If this agreement is terminated for any reason, money collected
                           prior to termination (if any) will be remitted to the Institution, less any fees due
                           Nelnet. Refunds, if any, will be handled by the Institution and will not be issued
                           until 60 days after the last day of the add-and-drop period. As noted above, enrollment
                           fees and returned payment fees are nonrefundable. If this agreement is terminated,
                           Nelnet will attempt to notify you immediately via the e-mail address Nelnet has on
                           file. If the e-mail address is invalid, a letter will be mailed.
                        </p>
                        				
                        <p>Note: A $30.00 FACTS Returned Payment Fee will be assessed if an automatic payment
                           is returned.
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-installment-plan/nelnet-terms.pcf">©</a>
      </div>
   </body>
</html>