<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Manage My TIP Account | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-installment-plan/manage-tip-account.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/tuition-installment-plan/">Tuition Installment Plan</a></li>
               <li>Manage My TIP Account</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Manage My TIP Account</h2>
                        				
                        <p><strong>Accessing Your TIP Account</strong></p>
                        				
                        <p>If you have already created a TIP agreement for the current registration term, you
                           can access your personal account information at <a href="http://www.mypaymentplan.com" target="_blank">www.mypaymentplan.com</a>. This site allows you to:
                        </p>
                        				
                        <ul>
                           					
                           <li>View agreement history</li>
                           					
                           <li>View remaining balance and payment amount</li>
                           					
                           <li>View returned payments</li>
                           					
                           <li>Change financial information (payment method)</li>
                           					
                           <li>Update demographic information</li>
                           					
                           <li>Update email address</li>
                           					
                           <li>Change Access Code</li>
                           					
                           <li>Add additional uuthorized parties</li>
                           				
                        </ul>
                        				
                        <p><strong>First Time User Account Setup</strong></p>
                        				
                        <p>Registration for an account requires three pieces of information provided in your
                           confirmation letter you received from FACTS/ Nelnet Business Solutions (NBS).
                        </p>
                        				
                        <p>1- FACTS/NBS Agreement Number</p>
                        				
                        <p>2- Access Code</p>
                        				
                        <p>3- Institution Identification</p>
                        				
                        <p>If you have any questions or concerns regarding the Account Setup process, please
                           contact Nelnet Business Solutions
                        </p>
                        				
                        <p>(NBS) at <a href="tel:800-609-8056">1-800-609-8056</a>.<br>
                           				Business Hours: Monday - Thursday 7:30am - 7:00pm (CST)<br>
                           				Friday 7:30am - 5:00pm (CST)
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-installment-plan/manage-tip-account.pcf">©</a>
      </div>
   </body>
</html>