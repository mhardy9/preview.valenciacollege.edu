<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>TIP Payments | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/tuition-installment-plan/payments.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li><a href="/students/business-office/tuition-installment-plan/">Tuition Installment Plan</a></li>
               <li>TIP Payments</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>TIP Payments</h2>
                        				
                        <p><strong><u>Down Payment</u></strong></p>
                        				
                        <p>Along with the enrollment fee, a down payment amount will also be deducted from the
                           bank or credit card account that was provided during enrollment. The down payment
                           will be processed immediately upon establishing the plan. The down payment will be
                           between 10 and 50 % of the tuition balance. If the down payment is not processed successfully
                           as a result of Non-Sufficient Funds (NSF) or incorrect account information, the plan
                           will be terminated immediately and you will owe the full tuition balance to Valencia
                           .
                        </p>
                        				
                        <p><strong><u>TIP Payment</u></strong></p>
                        				
                        <p>Tuition Installment Plan (TIP) charges will be processed on the 20th of each month.&nbsp;
                           You do not have to make any payments to Valencia College or Nelnet Business Solutions.&nbsp;
                           The payments will be deducted from the bank/credit card account automatically.&nbsp; If
                           the payment is returned as a result of Non-Sufficient Funds, Nelnet Business Solutions
                           <u>may</u> re-attempt to collect the payment on the 5th of the following month.&nbsp; Nelnet Business
                           Solutions may re-attempt to collect payment up to 3 times.&nbsp; It is important that you
                           contact the company immediately to determine the status of the TIP account in the
                           event you default on a payment.&nbsp; There is a $30 fee for each missed payment.&nbsp; In general,
                           your TIP account should remain current at all times.&nbsp; Nelnet Business Solutions has
                           the rights to terminate your TIP account at anytime for missed payments. <span><strong>To access your Tuition Installment Plan, please</strong></span> <strong><a href="manage-tip-account.php">click here</a>.&nbsp;</strong></p>
                        				
                        <p><strong><u>TIP Payment Default</u></strong></p>
                        				
                        <p>Make sure your TIP account with Nelnet Business Solutions is current at all times.
                           Nelnet has the right to terminate your agreement at anytime due to missed payments.
                           You will then be obligated to make full payment directly to Valencia College with
                           no other payment deferment options.
                        </p>
                        				
                        <p>A financial hold will be placed on your Valencia's account once you have defaulted
                           on your TIP agreement.
                        </p>
                        				
                        <p>Beginning August 2011, anyone completing a TIP agreement (for a future term) will
                           be required to bring oustanding return fees current before a new agreement can be
                           established.
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/tuition-installment-plan/payments.pcf">©</a>
      </div>
   </body>
</html>