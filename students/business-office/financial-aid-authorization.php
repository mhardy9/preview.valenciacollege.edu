<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Financial Aid Authorization | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/financial-aid-authorization.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Financial Aid Authorization</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Financial Aid Authorization</h2>
                        
                        <p>Federal Student Aid Payment Authorization Instructions</p>
                        
                        <p>Federal financial aid is automatically authorized to pay for your tuition and fees,
                           but you must submit a one-time authorization if you plan to use federal financial
                           aid to pay for your books, supplies and other charges at Valencia College. Valencia
                           must pay the resulting credit balance directly to the student or parent if applicable.
                           
                        </p>
                        
                        
                        
                        <h3>Submitting Your Financial Aid Authorization</h3>
                        
                        <p>If you would like for the college to deduct payment for non-tuition charges from your
                           financial aid, use <a href="https://atlas.valenciacollege.edu/">Atlas</a> to submit your authorization as follows: 
                        </p>
                        
                        <ul>
                           
                           <li>Click on the Students tab </li>
                           
                           <li>Click on "Financial Aid" found under STUDENT RESOURCES </li>
                           
                           <li>Click on Federal Student Aid Payment Authorization</li>
                           
                        </ul>
                        
                        <p>Please check your Atlas email account for correspondence from the college. For more
                           information, visit the <a href="/admissions/finaid/index.php">Financial Aid</a> site.
                        </p>
                        
                        
                        <h3>Non-Tuition Expenses</h3>
                        
                        <p>If you plan to use your federal financial aid to pay for charges other than tuition
                           and fees at Valencia, please read the information below: 
                        </p>
                        
                        <ul>
                           
                           <li>Tuition and fees will automatically be deducted from your financial aid before any
                              refund will be made. 
                           </li>
                           
                           <li>If you plan to charge your books and supplies at the Valencia bookstore against your
                              federal financial aid, you must submit your authorization as shown above. 
                           </li>
                           
                           <li>By submitting your authorization, Valencia may deduct payment from federal financial
                              aid for all non-tuition and fee charges. This includes books and supplies charged
                              at the Valencia bookstore, parking fines, etc. 
                           </li>
                           
                           <li>Once submitted, your authorization will remain valid for the duration of your attendance
                              at Valencia, unless you rescind your authorization.
                           </li>
                           
                           <li>You may rescind your authorization by completing  
                              the Rescission of Authorization Form. By rescinding the authorization you will be
                              unable to charge books, supplies, parking fines, etc. from your financial aid. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>Important Note:</strong> You are not required to submit an authorization for your non-tuition expenses. But,
                           if you do not authorize Valencia to deduct these payments, you will not be eligible
                           to charge your books and supplies in the Valencia bookstore.
                        </p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/financial-aid-authorization.pcf">©</a>
      </div>
   </body>
</html>