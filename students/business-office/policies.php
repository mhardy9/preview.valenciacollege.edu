<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Policies | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/policies.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Policies</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Policies</h2>
                        				
                        <p>The majority of funding for continued operations at Valencia is determined by the
                           Florida Legislature. Therefore, required fees and other financial policies are dictated
                           by Florida statute.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Class Deletion, Drop, or Withdrawal</h3>
                        				
                        <p>Administrative Withdrawal</p>If you owe the college any money after the end of the Drop/Add period, the college
                        has the option to withdraw you from all registered classes. You will still owe the
                        balance plus any other fees if they are charged. <strong>You are not eligible for a refund.</strong>
                        				
                        <p>Student Initiated Drop</p>You can drop a class or all of your classes before the end of the Drop/Add period
                        and <strong>are entitled to a 100% refund of the refundable fees.</strong> Your Valencia account will be credited, and after all fees have been paid, a check
                        or charge card credit will be issued. Refer to the college <a href="/students/catalog/index.php">catalog</a> for further information.
                        				
                        <p>Student Initiated Withdrawal</p>You may withdraw from a class or all of your classes after the end of the official
                        Drop/Add period, but before the mid-session withdrawal deadline. However, <strong>you are not eligible for a refund</strong>. If you owe the college any money, you will still have to pay that balance. Refer
                        to the college <a href="/students/catalog/index.php">catalog</a> for further information.
                        				
                        <p>Class Deletion</p><strong><span>You&nbsp;are responsible for payment&nbsp;for all classes</span> <u>not dropped</u> <span>from your schedule by the Drop/Refund Deadline&nbsp;listed in the Important College Calendar
                              Dates section of the catalog&nbsp;and in the Credit Class Schedule.</span> Non-attendance in any course(s) will not qualify you&nbsp;for a waiver of your financial
                           responsibility for the course(s). &nbsp;</strong>If you do not pay by the specified date and time, you may lose <strong>all</strong> of your classes and will have to attempt to re-register. It is <strong>your</strong> responsibility to review your account with the college.
                        				
                        <h3>Refund Policy</h3>
                        				
                        <p>To receive a 100% refund of the refundable fees, students must <strong>drop from a course or all courses before the last day of the Drop/Add period</strong> as listed in the credit class schedule.
                        </p>
                        				
                        <p>If you withdraw from a class after the Add/Drop period but before the withdrawal deadline,
                           you will receive a grade of "W" and will not be entitled to a refund of fees.
                        </p>
                        				
                        <p>If you withdraw from a course after the withdrawal deadline, you will be issued a
                           grade of "W", and a refund will not be granted.
                        </p>
                        				
                        <p>If you have recieved financial aid, or if your fees were paid by an approved agency,
                           you might be entitled to a refund after proper credits are first issued to the awarding
                           agency. Refund policies are established by and subject to change by the Legislature
                           of the State of Florida.
                        </p>
                        				
                        <p>For refund purposes, the first day of classes is the one designated "Classes Begin"
                           as published in the College's catalog and the class schedule, and does not refer to
                           the first scheduled class day for any particular student.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Collection of Debt</h3>
                        				
                        <p>If you are currently enrolled and have a returned check, delinquent loan, delinquent
                           VA deferment or other financial obligation, you will be notified. If you do not respond
                           within the allowed time, the college reserves the right to financially withdraw a
                           student from all classes. All fees on the account remain. This applies even if you
                           do not attend any classes. Until your indebtedness is paid, you will not be allowed
                           to enroll in a subsequent term or receive your college records. Please be advised
                           that checks returned unpaid to the College's bank will be automatically redeposited
                           for collection. Consequently, you may be assessed returned check charges by your banking
                           institution, in addition to a returned check fee from Valencia. The college reserves
                           the right to submit for collection all accounts deemed delinquent to an external collection
                           service. The College will notify all students before this action will be taken. However,
                           once an account is submitted, up to 30% fee will be assessed to the outstanding balance
                           of your Valencia account and payment must be rendered to the collections agency directly
                           after submission.
                        </p>
                        				
                        <p><strong>Collection of Debt refers to:</strong></p>Collection of Returned Checks<br>
                        				Rejected Credit Cards or Loans<br>
                        				VA Deferments<br>
                        				Miscellaneous Financial Obligations
                        				
                        <hr class="styled_2">
                        				
                        <h3>Course Repeat Policy</h3>
                        				
                        <p>Students enrolled in the same college-preparatory and/or college-level (credit) course
                           more than two times beginning Session 1, 1999, shall pay the full cost of instruction
                           which is $375.22 per credit hour.
                        </p>
                        				
                        <h3>Important Dates and Deadlines</h3>
                        				
                        <p>There are no events at this time. <a href="/students/business-office/important-deadlines.php">More</a></p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/policies.pcf">©</a>
      </div>
   </body>
</html>