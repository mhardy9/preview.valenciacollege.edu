<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Payments | Valencia College</title>
      <meta name="Description" content="The Business Office at Valencia College is the department to inquire and resolve any questions pertaining to your financial obligations with the college.">
      <meta name="Keywords" content="business office, payments, policies, tuition installments, important deadline, florida prepaid">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/business-office/payments.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/business-office/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Business Office</h1>
            <p>				</p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/business-office/">Business Office</a></li>
               <li>Payments</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Payments</h2>
                        				
                        <p>You can make online payments by logging into <a href="https://atlas.valenciacollege.edu" target="_blank">Atlas</a>. Once logged in please go to the Students tab then click on "Account Summary and
                           Tuition Payment Option" within the Business Office channel then click on "Submit Your
                           Payment Online". This will open a new page which allows to make payments.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Methods of Payment</h3>
                        				
                        <p>Valencia College accepts cash, money order, checks, American Express, Discover, MasterCard
                           and Visa. Checks must be made payable to Valencia College and all credit cards must
                           be signed by the card holder or they will not be accepted if paying in person.
                        </p>
                        				
                        <p>If you are a Financial Aid recipient, eligible award will automatically be credited
                           to your account. It is your responsibility to pay any remainder owed on the account
                           not covered by aid.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Parent PIN</h3>
                        				
                        <p>Payment is ultimately the responsibility of the student. Due to federal regulation,
                           the college is unable to release information without the students' permission. For
                           further details, click on the following link: <a href="http://www.ed.gov/offices/OM/fpco/ferpa/">http://www.ed.gov/offices/OM/fpco/ferpa/</a></p>
                        				
                        <p>The student may create a Parent PIN for any person he/she wishes to provide the ability
                           to view his/her current balance and make secure online payments by electronic check
                           or credit card. Only the student can create Parent PINs and reset passwords for these
                           accounts. The student may also inactivate a Parent PIN at any time at his/her discretion.
                        </p>
                        				
                        <p>To create a Parent PIN:</p>
                        				
                        <p>Login to <strong>Atlas</strong></p>
                        				
                        <p>Click on the <strong>"Students"</strong> tab
                        </p>
                        				
                        <p>Under “<strong>Student Resources”</strong> click to expand <strong>"Business Office</strong>"
                        </p>
                        				
                        <p>Click on "<strong>Account Summary and Tuition Payment Options"</strong></p>
                        				
                        <p>Click on "<strong>Submit Your Payment Online</strong>"
                        </p>
                        				
                        <p>Click on "<strong>Add New</strong>" under Parent PINs to create an account
                        </p>
                        				
                        <p>If a Parent or Authorized User has been setup by the Student, log onto <a href="https://commerce.cashnet.com/vccpay">Valencia ePayment</a> to make a payment.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Third Party Authorization</h3>
                        				
                        <p>If all or part of your tuition is being paid by some other agency, (ex. your employer
                           or government agency), you must present your current form of authorization (letter,
                           voucher, etc.) within the payment time frame (see <a href="/students/business-office/important-deadlines.html">Important Deadline dates</a>).
                        </p>
                        				
                        <p>This form must be for the current term and must explain if all or part of the fees
                           will be paid by that source. Bring this authorization to the Business Office to ensure
                           proper credit is posted to your account. If not presented, a balance will remain on
                           your account and you could be deleted from all classes.
                        </p>
                        				
                        <p>Third Party Authorizations will not be accepted after the last day to drop classes.</p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Financial Aid Programs</h3>
                        				
                        <p>Valencia College offers a variety of programs geared towards Financial Aid assistance.</p>
                        				
                        <p>Visit the <a href="/admissions/finaid/programs/index.php">Financial Aid Services</a> for more information.
                        </p>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/business-office/payments.pcf">©</a>
      </div>
   </body>
</html>