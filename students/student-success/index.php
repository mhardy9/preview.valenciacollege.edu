<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Success  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/student-success/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/student-success/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Student Success</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>The Basics</h3>
                           
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>If you're looking for a way to survive college, take SLS 1122 Student Success. This
                              course is designed to
                              introduce you to college resources, help you map out your education plan, select a
                              major when you're
                              ready, set some goals for yourself and dust off those study skills so necessary for
                              your success!
                           </p>
                           
                           <p>Besides that, you'll be able to connect with your peers and make some friends. Who
                              says you need to go it
                              alone?
                           </p>
                           
                           <h3>You might want to take Student Success if:</h3>
                           
                           <ul>
                              
                              <li>You tend to be a procrastinator and you'd like to change your ways.</li>
                              
                              <li>You got through high school without studying much and now college is looking scary.</li>
                              
                              <li>You need to improve your skills in memory, test taking, or note taking.</li>
                              
                              <li>You'd like to make better use of your 24/7…that is, become a time management whiz.</li>
                              
                              <li>You have some doubts about your major or you're not sure what your next steps will
                                 be after life at
                                 Valencia.
                                 
                              </li>
                              
                              <li>You feel lost when you step on campus…so many resources, so little direction!</li>
                              
                              <li>When people ask you what you want to be when you grow up, your answer is "Ummmm…still
                                 looking for good
                                 ideas."
                                 
                              </li>
                              
                              <li>You're in search of meaningful goals for yourself.</li>
                              
                           </ul>
                           
                           
                           
                        </div>
                        
                        
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           
                           <h3>Student Success Team</h3>
                           
                           
                        </div>
                        
                        
                        <div class="wrapper_indent">
                           
                           <table class="table ">
                              
                              <tr>
                                 
                                 <td valign="top">
                                    
                                    <table class="table ">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <th>East Campus</th>
                                             
                                             <th>West Campus</th>
                                             
                                             <th>Osceola Campus</th>
                                             
                                          </tr>
                                          
                                          <tr valign="top">
                                             
                                             <td>
                                                
                                                <p align="left"><strong>Leonard C. Bass, Ed.D.&nbsp;</strong><br> Campus Dean for Learning Support
                                                   
                                                </p>
                                                
                                                <p align="left">Phone: 407.582.2745</p>
                                                
                                                <p align="left">Email:&nbsp;<a href="mailto:lbass11@valenciacollege.edu">lbass11@valenciacollege.edu</a></p>
                                                
                                                <p align="left">&nbsp;</p>
                                                
                                                <p align="left"><strong>Stacy Ramos</strong><br> Adminstrative Assistant to the Campus Dean for
                                                   Learning Support
                                                </p>
                                                
                                                <p align="left">Phone: 407.582.2823</p>
                                                
                                                <p align="left">Email:&nbsp;<a href="mailto:sramos14@valenciacollege.edu">sramos14@valenciacollege.edu</a>
                                                   
                                                </p>
                                                
                                             </td>
                                             
                                             <td>
                                                
                                                <p dir="auto"><strong>Karen Reilly, Ph.D</strong><br> Campus Dean of Learning Support
                                                </p>
                                                
                                                <p dir="auto">Phone: 407-582-1810</p>
                                                
                                                <p dir="auto">Email:&nbsp;<a href="mailto:kreilly5@valenciacollege.edu" rel="noreferrer">kreilly5@valenciacollege.edu</a>
                                                   
                                                </p>
                                                
                                                <p align="left">&nbsp;</p>
                                                
                                                <p dir="auto"><strong>Anthony Ards</strong><br> Administrative Assistant to the Dean of Learning
                                                   Support
                                                </p>
                                                
                                                <p dir="auto">Phone: 407-582-1516</p>
                                                
                                                <p dir="auto">Email:&nbsp;<a href="mailto:aards1@valenciacollege.edu" rel="noreferrer">aards1@valenciacollege.edu</a>
                                                   
                                                </p>
                                                
                                             </td>
                                             
                                             <td>
                                                
                                                <p align="left"><strong>Landon Shephard, Ph.D.</strong><br> Campus Dean of Learning Support
                                                   
                                                </p>
                                                
                                                <p align="left">Phone: 321.582.4877</p>
                                                
                                                <p align="left">Email:&nbsp;<a href="mailto:lshephard@valenciacollege.edu">lshephard@valenciacollege.edu</a>
                                                   
                                                </p>
                                                
                                                <p align="left">&nbsp;</p>
                                                
                                                <p align="left"><strong>Leslie Leadbeater</strong><br> Assistant to the Dean of Learning Support
                                                   
                                                </p>
                                                
                                                <p align="left">Phone: 407-582-4915</p>
                                                
                                                <p align="left">Email:&nbsp;<a href="mailto:lleadbeater@valenciacollege.edu">lleadbeater@valenciacollege.edu</a>
                                                   
                                                </p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p><a href="https://valenciacollege.edu/studentsuccess/#top">TOP</a></p>
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td valign="top"><img src="https://valenciacollege.edu/images/spacer.gif" width="15" height="1" alt="Valencia image description"></td>
                                 
                                 <td valign="top"><img src="https://valenciacollege.edu/images/spacer.gif" width="770" height="1" alt="Valencia image description"></td>
                                 
                              </tr>
                              
                           </table>
                           
                           <p>&nbsp;</p>
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="banner">
                        <i class=" iconcustom-school"></i>
                        
                        
                        <h3>Take a campus tour</h3>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.</p>
                        <a href="tour.html" class="banner_bt">Start tour</a>
                        
                     </div>
                     
                     
                     <h4><strong>How to apply</strong></h4>
                     
                     
                     <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                        Te pri facete latine
                        salutandi, scripta mediocrem et sed, cum.
                     </p>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Phone</h5>
                        <i class="icon-phone"></i>
                        
                        
                        <p><a href="tel://033284322">+ 0043 848293 43</a>
                           
                           <br>
                           <small>Monday to Friday 9.00am - 6.00pm</small>
                           
                        </p>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>By Postal Mail</h5>
                        <i class="icon_pencil-edit"></i>
                        
                        
                        <p><a href="#0"><strong>Download the application form</strong></a>,
                           
                           <br>and send it to this address:
                           
                           <br>
                           
                           <br><em>4 West 31st Street New York <br> 10001 United States</em></p>
                        
                     </div>
                     
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>Apply Online</h5>
                        <i class="icon_desktop"></i>
                        
                        
                        <p>Eam admodum pertinacia an, essent nostro audiam ad qui, pro ex habeo aeterno maluisset.
                           Aeque apeirian duo
                           et, ad ludus denique tincidunt usu.
                        </p>
                        
                        
                        <p><a href="apply_online.html" class="button small">Apply online</a></p>
                        
                     </div>
                     
                  </aside>
                  
               </div>
               
               
               
               <div class="main-title">
                  
                  <h2>Frequently questions</h2>
                  
                  
                  <p>Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.</p>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Et ius tota recusabo democritum?</h4>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Posse exerci volutpat has?</h4>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Te pri facete latine salutandi?</h4>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Et ius tota recusabo democritum?</h4>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Mediocritatem sea ex, nec id agam?</h4>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="col-md-4">
                     
                     <div class="box_style_2">
                        
                        <h4>Te pri facete latine salutandi?</h4>
                        
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/student-success/index.pcf">©</a>
      </div>
   </body>
</html>