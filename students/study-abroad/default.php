<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div> <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 
                                 
                                 <div>
                                    
                                    
                                    <div>
                                       
                                       
                                       <div> 
                                          
                                          <div>
                                             
                                             
                                             <link href="../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                             
                                             
                                             
                                             
                                             
                                             
                                             
                                             <div> 
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-1.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-3.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-4.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-5.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-6.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-7.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-8.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   <div>
                                                      <img alt="SAGE" border="0" height="279" src="featured-9.jpg" width="930">
                                                      
                                                   </div>
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h3>For Students</h3>
                                       <a href="students/short-term/index.html">Short-Term Study Abroad Programs</a>
                                       <a href="students/semester/index.html">Semester Study Abroad Programs</a>
                                       <a href="students/events/internationaleducationweek.html">International Education Week</a>
                                       <a href="students/events/globaldistinction.html">Global Distinction</a>          
                                       <a href="students/contact.html">Contact Us</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h3>SAGE Events </h3>
                                       
                                       
                                       <div data-id="international_education_week_2017">
                                          
                                          
                                          <div>
                                             Nov<br>
                                             <span>13</span>
                                             
                                          </div>
                                          
                                          
                                          <div>        
                                             <a href="http://events.valenciacollege.edu/event/international_education_week_2017" target="_blank">International Education Week 2017</a><br>
                                             
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                       <a href="students/events/index.html"><strong>View More</strong></a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h3>For Faculty and Staff</h3>
                                       <a href="faculty-staff/leadstudyabroad/index.html">Lead a Study Abroad Program</a>
                                       <a href="faculty-staff/curriculum/exchanges.html">Faculty and Staff Exchanges</a>
                                       <a href="faculty-staff/internationaltravel.html">International Travel &amp; Conferences</a>
                                       <a href="faculty-staff/curriculum/index.html">Internationalizing the Curriculum</a>          
                                       <a href="faculty-staff/sagecontact.html">Contact Us</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>          
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/default.pcf">©</a>
      </div>
   </body>
</html>