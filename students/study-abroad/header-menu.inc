<div class="header header-site">
   <div class="container">
      <div class="row">
         <nav class="col-xs-12"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in"><i class="fal fa-window-close"></i></a><ul>
                  <li class=""><a href="/_resources/includes/&#34;/academics/" target="">Academics</a></li>
                  <li class="megamenu submenu "><a href="#" target="" class="show-submenu-mega">Admissions <i class="fas fa-chevron-down" aria-hidden="true"></i></a><div class="menu-wrapper">
                        <div class="col-md-6">
                           
                           <ul>
                              
                              <li><a href="/admissions/admissions-records/">Admission &amp; Records</a></li>
                              
                              <li><a href="/admissions/admissions-records/admission-details/index.php">Admission Details</a></li>
                              
                              <li><a href="/admissions/admissions-records/admission-details/forms.php">Applications &amp; Forms</a></li>
                              
                              <li><a href="/admissions/admissions-records/change-major.php">Change of Program/Major</a></li>
                              
                              <li><a href="/admissions/admissions-records/degree-verification.php">Degree Verification</a></li>
                              
                           </ul>
                           
                        </div>
                        <div class="col-md-6">
                           
                           <ul>
                              
                              <li><a href="/admissions/admissions-records/enrollment-verification.php">Enrollment Verification</a></li>
                              
                              <li><a href="/admissions/admissions-records/florida-residency/index.php">Florida Residency</a></li>
                              
                              <li><a href="/admissions/admissions-records/paying-for-classes.php">Paying for Classes</a></li>
                              
                              <li><a href="/admissions/admissions-records/registration-details/index.php">Registration Details</a></li>
                              
                              <li><a href="/admissions/admissions-records/steps.php">Steps to Enroll</a></li>
                              
                           </ul>
                           
                        </div>
                     </div>
                  </li>
                  <li class="megamenu submenu "><a href="#" target="" class="show-submenu-mega">Programs &amp; Degrees <i class="fas fa-chevron-down" aria-hidden="true"></i></a><div class="menu-wrapper c3">
                        <div class="col-md-4">
                           
                           <h3>Programs</h3>
                           
                           <ul>
                              
                              <li><a href="/icon-pack-1.html">General Studies</a></li>
                              
                              <li><a href="/icon-pack-2.html">Arts &amp; Entertainment</a></li>
                              
                              <li><a href="/icon-pack-3.html">Business</a></li>
                              
                              <li><a href="/icon-pack-4.html">Communications</a></li>
                              
                              <li><a href="/shortcodes.html">Education</a></li>
                              
                              <li><a href="/shortcodes.html">Engineering</a></li>
                              
                              <li><a href="/shortcodes.html">Computer Programming</a></li>
                              
                              <li><a href="/shortcodes.html">Health Sciences</a></li>
                              
                           </ul>
                           
                        </div>
                        <div class="col-md-4">
                           
                           <h3>Areas</h3>
                           
                           <ul>
                              
                              <li><a href="/academic_single_course.html">Hospitality &amp; Culinary</a></li>
                              
                              <li><a href="/blog.html">Information Technology</a></li>
                              
                              <li><a href="/contacts.html">Landscape &amp; Horticulture</a></li>
                              
                              <li><a href="/agenda_calendar.html">Public Safety &amp; Paralegal Studies</a></li>
                              
                              <li><a href="/gallery.html">Science &amp; Mathematics</a></li>
                              
                              <li><a href="/shortcodes.html">Social Sciences</a></li>
                              
                           </ul>
                           
                        </div>
                        <div class="col-md-4">
                           
                           <h3>Degrees</h3>
                           
                           <ul>
                              
                              <li><a href="/shortcodes.html">Associate in Arts</a></li>
                              
                              <li><a href="/shortcodes.html">Associate in Science</a></li>
                              
                              <li><a href="/shortcodes.html">Bachelor of Science</a></li>
                              
                              <li><a href="/shortcodes.html">Certificate Programs</a></li>
                              
                              <li><a href="/shortcodes.html">Advanced Technical Certificates</a></li>
                              
                           </ul>
                           
                        </div>
                     </div>
                  </li>
                  <li class=""><a href="/ABOUT/" target="">About</a></li>
                  <li class="site-menu-outline"><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/" target="">Request Info</a></li>
                  <li class="site-menu-outline apply"><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwskalog.p_disploginnew?in_id=&amp;cpbl=&amp;newid=" target="">Apply</a></li>
               </ul>
            </div>
         </nav>
      </div>
   </div>
</div>