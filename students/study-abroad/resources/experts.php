<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/resources/experts.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/resources/">Resources</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        <h2>International Resource Experts at Valencia</h2>
                        
                        <p> The following faculty members  have identified themselves as subject matter experts
                           for the countries listed below. Feel free to contact them if you need assistance for
                           study abroad information or internationalizing your curriculum. Campuses are listed
                           by first letter with telephone extensions.<br>
                           
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    <div>COUNTRY</div>
                                 </div>
                                 
                                 <div>
                                    <div>FACULTY/STAFF CONTACT LIST </div>
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Argentina</div>
                                 
                                 <div>Karen Cowden (W-1960), Raul Valery (E-2882), Ana Caldero (W-1431) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Australia</div>
                                 
                                 <div>Mary Beck (W-1882), Gina Dalle Molle (W-1503), 
                                    
                                    
                                    Eric  Wallman (E-2814), Gina M. Dalle Molle (W-1503) 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Austria</div>
                                 
                                 <div>Andrew Ray (W-1847), Carl E Creasman Jr (E-2009), Ed Frame (W-1189), Marjorie Karwowski
                                    (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Bahamas</div>
                                 
                                 <div>Debbie Hall (W-1963) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Belgium</div>
                                 
                                 <div>Andrew Ray (W-1847), Dr. George Brooks (E-2721)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Brazil</div>
                                 
                                 <div>Tammi Gitto-Kania (O-4925), Christopher D'Urso (E-2804), Sylvana Vester (E-2231) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Canada</div>
                                 
                                 <div>Jada D. Kearns (E-2736), Debbie Hall (W-1963), Aaron Powell (W-1356) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Caribbean Islands </div>
                                 
                                 <div>Deymond Hoyte (E-2118) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>China</div>
                                 
                                 <div>Deymond Hoyte (E-2118), Ed Frame (W-1189) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Colombia</div>
                                 
                                 <div>Aida E. Diaz (E-4092), Angela Cortes (E-2479) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Costa Rica </div>
                                 
                                 <div>Christie Pickeral (O-4092), Karen Cowden (W-1960), Debbie Hall (W-1963)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Cuba</div>
                                 
                                 <div>Tammi Gitto-Kania (O-4925), Eric  Wallman (E-2814)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Czech Republic</div>
                                 
                                 <div>Andrew Ray (W-1847), Carl E Creasman Jr (E-2009), Dr. George Brooks (E-2721), Marcelle
                                    Cohen (2362), Debbie Hall (W-1963), Eric  Wallman (E-2814), Bonnie Oliver (E-2214),
                                    Marjorie Karwowski (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Denmark</div>
                                 
                                 <div>Andrew Ray (W-1847), Lana Powell (W-1322), Lee McCain (E-2489) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Dominican Republic </div>
                                 
                                 <div>Christie Pickeral (O-4092), Lana Powell (W-1322),  Raul Valery (E-2882) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ecuador</div>
                                 
                                 <div>Marcelle Cohen (2362), Ana Caldero (W-1431) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>England</div>
                                 
                                 <div>Andrew Ray (W-1847), Carl E Creasman Jr (E-2009), Dr. George Brooks (E-2721), Deymond
                                    Hoyte (E-2118), Marcelle Cohen (2362), Jada Kearns (E-2736), Sara Melanson (E-2457),
                                    Debbie Hall (W-1963),  Raul Valery (E-2882), Eric  Wallman (E-2814), Ed Frame (W-1189),
                                    Marjorie Karwowski (E-2502), Aaron Powell (W-1356), Gina M. Dalle Molle (W-1503) 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Estonia</div>
                                 
                                 <div>Wendy Wish-Bogue (W-1338)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ethiopia</div>
                                 
                                 <div>Melissa Schreiber (E-2246) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>France</div>
                                 
                                 <div>
                                    
                                    <p>Andrew Ray (W-1847), Carl E Creasman Jr (E-2009), Dr. George Brooks (E-2721), Deymond
                                       Hoyte (E-2118), Jada D. Kearns (E-2736) Hatim Boustique (E-2693), Gina&nbsp;Dalle Molle(W-1503),
                                       Brenda Schumpert (W-1232), Catherine Gaughan (E-2643), Debbie Hall (W-1963), Sylvana
                                       Vester (E-2231), Eric  Wallman (E-2814), Marjorie Karwowski (E-2502), 
                                       
                                       
                                       Anna Sezonenko (W-1084), 
                                       
                                       
                                       Gina M. Dalle Molle (W-1503) 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Germany</div>
                                 
                                 <div>Andrew Ray (W-1847), Carl E Creasman Jr (E-2009), Dr. George Brooks (E-2721), Sara
                                    Melanson (E-2457), Karen Murray (W-1485), Robert McCaffrey (E-2784), Debbie Hall (W-1963),
                                    Eric  Wallman (E-2814)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Gibraltar</div>
                                 
                                 <div>Andrew Ray (W-1847)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Greece</div>
                                 
                                 <div>Carl E Creasman Jr (E-2009), Debbie Hall (W-1963), Karen Murray (W-1485), Marjorie
                                    Karwowski (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Guyana</div>
                                 
                                 <div>Deymond Hoyte (E-2118), Steve Myers (E-2205) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Hungary</div>
                                 
                                 <div>Yasmeen Qadi (E-2624),  Debbie Hall (W-1963), Eric  Wallman (E-2814), Marjorie Karwowski
                                    (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Iceland</div>
                                 
                                 <div>Marjorie Karwowski (E-2502)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>India</div>
                                 
                                 <div>Yasmeen Qadi (E-2624), Aida E. Diaz (E-2336), Steve Myers (E-2205), Ed Frame (W-1189)
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ireland</div>
                                 
                                 <div>Andrew Ray (W-1847), Dr. George Brooks (E-2721), Eric  Wallman (E-2814)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Israel</div>
                                 
                                 <div>Richard Gair (E-2641), Anna Sezonenko (W-1084) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Italy</div>
                                 
                                 <div>Andrew Ray (W-1847), Carl E Creasman Jr (E-2009), Dr. George Brooks (E-2721), Aida
                                    E. Diaz (E-2336), Tammi Gitto-Kania (O-4925), Karen Murray (W-1485), Debbie Hall (W-1963),
                                    
                                    
                                    
                                    Diane Brown (O-4119), Bonnie Oliver (E-2214), Marjorie Karwowski (E-2502), 
                                    
                                    
                                    Diane Brown (O-4119)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Japan</div>
                                 
                                 <div>Irina Struganova (W-1947), Brenda Schumpert (W-1232), Kathleen McOwen (W- 1246) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Jordan</div>
                                 
                                 <div>Areej Zufari (E-2349)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Korea</div>
                                 
                                 <div>James May (E-2047), Eric  Wallman (E-2814), Aaron Powell (W-1356) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Kyrgyzstan</div>
                                 
                                 <div>Mary Beck (W-1882)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Lebanon</div>
                                 
                                 <div>Areej Zufari (E-2349)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Luxembourg</div>
                                 
                                 <div>Andrew Ray (W-1847), Dr. George Brooks (E-2721)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Malaysia</div>
                                 
                                 <div>Yasmeen Qadi (E-2624), Deymond Hoyte (E-2118), Ed Frame (W-1189) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Mexico</div>
                                 
                                 <div>James May (E-2047), Debbie Hall (W-1963), Marie Trone (O-4914), Wendy Wish-Bogue (W-1338),
                                    Raul Valery (E-2882), Eric  Wallman (E-2814)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Monaco</div>
                                 
                                 <div>Andrew Ray (W-1847), Debbie Hall (W-1963), Marjorie Karwowski (E-2502)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Mongolia</div>
                                 
                                 <div>Mary Beck (W-1882) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Morocco</div>
                                 
                                 <div>Karen Cowden (W-1960), Areej Zufari (E-2349), Hatim Boustique (E-2693), Ed Frame (W-1189)
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Namibia </div>
                                 
                                 <div>Eric Wallman (E-2814)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Netherlands</div>
                                 
                                 <div>Andrew Ray (W-1847), Dr. George Brooks (E-2721), Debbie Hall (W-1963), Lana Powell
                                    (W-1322), Marjorie Karwowski (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>New Zealand </div>
                                 
                                 <div>Mary Beck (W-1882) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Nicaragua</div>
                                 
                                 <div>Kitty Harkleroad (W-1570) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Northern Ireland </div>
                                 
                                 <div>Eric Wallman (E-2814) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Panama</div>
                                 
                                 <div>Aida E. Diaz (E-2336), Eric  Wallman (E-2814)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Peru</div>
                                 
                                 <div>Dr. George Brooks (E-2721), 
                                    
                                    
                                    Ed Frame (W-1189) 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Poland</div>
                                 
                                 <div>Richard Gair (E-2641), Eric  Wallman (E-2814), 
                                    
                                    
                                    Marjorie Karwowski (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Portugal</div>
                                 
                                 <div>Andrew Ray (W-1847), Marjorie Karwowski (E-2502)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Puerto Rico </div>
                                 
                                 <div>Aida E. Diaz (E-2336), Ana Caldero (W-1431) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Romania</div>
                                 
                                 <div>Debbie Hall (W-1963), Eric  Wallman (E-2814)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Russia</div>
                                 
                                 <div>Irina Struganova (W-1947), Eric  Wallman (E-2814), Steve Cunningham (O-4827), Anna
                                    Sezonenko (W-1084) 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Saudi Arabia </div>
                                 
                                 <div>Areej Zufari (E-2349), Yasmeen Qadi (E-2624)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Scotland</div>
                                 
                                 <div>Carl E Creasman Jr (E-2009), Eric  Wallman (E-2814)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Spain</div>
                                 
                                 <div>Andrew Ray (W-1847), Dr. George Brooks (E-2721), Karen Cowden (W-1960), Aida E. Diaz
                                    (E-2336), Sarah Melanson (E-2457), Karen Murray (W-1485), Jeremy Bassetti (E-2367),
                                    Eric  Wallman (E-2814), Bonnie Oliver (E-2214), Marjorie Karwowski (E-2502), 
                                    
                                    
                                    Aaron Powell (W-1356) 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Sweden</div>
                                 
                                 <div>Wendy Wish-Bogue (W-1338)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Switzerland</div>
                                 
                                 <div>Andrew Ray (W-1847), Dr. George Brooks (E-2721), Debbie Hall (W-1963), Marjorie Karwowski
                                    (E-2502)
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Syria</div>
                                 
                                 <div>Areej Zufari (E-2349)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> Tanzania </div>
                                 
                                 <div>Ed Frame (W-1189) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Togo</div>
                                 
                                 <div>Judy Chubb (E-2763) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Turkey</div>
                                 
                                 <div>Areej Zufari (E-2349)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Ukraine</div>
                                 
                                 <div>Anna Sezonenko (W-1084) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> United Arab Emirates </div>
                                 
                                 <div>Marjorie Karwowski (E-2502)</div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Venezuela</div>
                                 
                                 <div>Steve Myers (E-2205) ,  Raul Valery (E-2882) </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>Zambia</div>
                                 
                                 <div>Melissa Schreiber (E-2246), Eric  Wallman (E-2814)</div>  
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <br>
                        <br>
                        
                        
                        
                        
                        <p><a href="experts.html#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/resources/experts.pcf">©</a>
      </div>
   </body>
</html>