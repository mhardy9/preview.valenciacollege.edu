<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/resources/foreignvisitors.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/resources/">Resources</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        <h2>Foreign Visitors </h2>
                        
                        <p>If you are coming to visit us at Valencia  College, you will find the following information
                           helpful:
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>ARRIVALS AND DEPARTURES<br>
                                          <br>
                                          </strong><a href="http://orlandoairports.net/guide.htm" target="_blank"> Guide to the Orlando International Airport<br>
                                          </a> <a href="http://orlandoairports.net/transport/local_transport.htm" target="_blank">Bus, Shuttle, and Taxi Rates</a><br>
                                       <a href="http://www.bing.com/local/default.aspx?what=rental+cars&amp;where=Orlando,+Florida&amp;s_cid=ansPhBkYp01&amp;mkt=en-us&amp;ac=false&amp;q=rental+cars+in+orlando+florida&amp;qpvt=rental+cars+in+orlando+florida" target="_blank">Rental Cars</a>                <br>
                                       <a href="http://orlandoairports.net/flights.htm" target="_blank">Flight Information</a><br>
                                       <a href="http://www.worldtimezone.com/index24.html" target="_blank">World Time Zone Map<br>
                                          </a>                  <a href="http://finance.yahoo.com/currency-converter/#from=USD;to=EUR;amt=1%23from=USD;to=EUR;amt=1">Currency Converter</a><br>
                                       <br>
                                       <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>VALENCIA  COLLEGE<br>
                                          <br>
                                          </strong><a href="../../../map/index.html" target="_blank">Valencia Campus Locations and Driving Directions</a><a href="../../../about-valencia/administration.html" target="_blank"><br>
                                          College Administration<br>
                                          </a><a href="../../../contact/directory.html" target="_blank">Phone Directory</a> <strong><br>
                                          </strong><a href="../../../about-valencia/index.html" target="_blank">About Valencia  College</a> <br>
                                       <a href="../../../academics/programs/index.html" target="_blank">Degrees and Programs<br>
                                          </a><a href="../../exchange/index.html" target="_blank">J Exchange Visitor Program</a> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>IN AND AROUND ORLANDO<br>
                                          <br>
                                          </strong><a href="http://www.weather.com/weather/today/Orlando+FL+USFL0372" target="_blank">Weather in Orlando</a> <br>
                                       <a href="http://www.bing.com/local/default.aspx?what=hotels++the++airport&amp;where=Orlando,+Florida&amp;s_cid=ansPhBkYp01&amp;mkt=en-us&amp;ac=false&amp;q=hotels+near+the+orlando+airport&amp;qpvt=hotels+near+the+orlando+airport" target="_blank">Hotels Near the Orlando Airport</a><br>
                                       <a href="http://www.frommers.com/destinations/orlando/" target="_blank">Orlando Attractions</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>IMPORTANT PHONE NUMBERS<br>
                                       <br>
                                       </strong>Fire/Police/Ambulance: 911<br>
                                    Valencia  College: 407-582-5000 <br>
                                    
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/resources/foreignvisitors.pcf">©</a>
      </div>
   </body>
</html>