<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/resources/associationsandorganizations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/resources/">Resources</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>Associations &amp; Organizations</h2>
                        
                        <p>The following associations and organizations offer study abroad resources 
                           for faculty, staff, and students:
                        </p>
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://ccid.cc/index.htm" target="_blank"><strong>CCID: Community Colleges for International Development</strong></a><br>
                                       POB 2068
                                       <br>
                                       Cedar Rapids, IA 52406-2068<br>
                                       Tel: 319-398-1257<br>
                                       Fax: 319-398-7113<br>
                                       Email: 
                                       <a href="mailto:ccid@kirkwood.edu">ccid@kirkwood.edu</a><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>The mission of CCID is to provide opportunities for building global relationships
                                       that strengthen educational programs, and promote economic development. The organization
                                       achieves this goal by providing technical training, economic development, partnerships,
                                       leadership, and professional development opportunities to members. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <a href="http://www.ccisabroad.org/" target="_blank"><strong>CCIS: College Consortium for International Studies</strong><br>
                                       </a>2000 P Street, NW, Suite 503 <br>
                                    Washington, DC 20036 <br>
                                    Tel: 202-223-0330 <br>
                                    Fax: 202-223-0999 <br>
                                    Contact: Harlan N. Henson                
                                 </div>
                                 
                                 <div>CCIS is a partnership of two and four-year colleges and universities. CCIS members
                                    sponsor a variety of programs, notably study abroad programs and professional development
                                    seminars for faculty and administrators, which are designed to enhance international/
                                    intercultural perspectives within the academic community. <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.onlinecolleges.net/" target="_blank">Center for Online Education</a> <br>
                                       PO Box 52755 <br>
                                       Houston, TX 77052<br>
                                       Tel: 202-367-9383<br>
                                       Contact: Katherine Harper - Outreach Coordinator 
                                    </p>
                                    
                                 </div>
                                 
                                 <div> The Center for Online Education is a collection of online educators and experts committed
                                    to providing informed, research-based guides for students pursuing college degrees
                                    online.
                                    
                                    
                                    Formerly OnlineColleges.net, the Center for Online Education was created to reflect
                                    the extensive team behind the resources, rankings and original research on the site.
                                    We have recently published a <a href="http://www.onlinecolleges.net/for-students/study-abroad-guide/" target="_blank">Step-by-Steb Guide to Studying Abroad</a> to ensure students are fully prepared to embark on a memorable experience. 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.ciee.org/" target="_blank"><strong>CIEE: Council on International Educational Exchange</strong><br>
                                          </a>205 East 42nd Street <br>
                                       New York, NY 10017 <br>
                                       Tel: 888-268-6245 <br>
                                       Email: <a href="mailto:info@ciee.org">info@ciee.org</a> <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>This Council is divided into three interrelated but very independent operating entities:
                                       Council-International Study Programs, Council Exchanges, and Council Travel. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><a href="http://www.fulbrightteacherexchange.org/" target="_blank"><strong>Distinguished Fulbright Awards in Teaching Program</strong></a></div>
                                 
                                 <div>
                                    
                                    <p>Sponsored by the Bureau of Educational and Cultural Affairs, U.S. Department of State,
                                       the Distinguished Fulbright Awards in Teaching program recognizes and encourages excellence
                                       in teaching in the&nbsp;U.S.and abroad. It is part of the overall Fulbright Program, named
                                       in honor of Senator William Fulbright, which promotes mutual understanding among people
                                       of the&nbsp;United States&nbsp;and other countries. The program sends highly accomplished primary
                                       and secondary teachers from the U.S.&nbsp;abroad and brings international teachers to the
                                       U.S for a three to six month long program. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="http://elf.georgetown.edu/" target="_blank">EL Fellow English Teaching Program</a> </strong></div>
                                 
                                 <div>Faculty with master's degrees in TEFL/TESL, Applied Linguistics, or related fields
                                    should take advantage of the chance to experience 10 months living and teaching English
                                    in one of 80 countries. This program is funded by the U.S. Department of State (DOS)
                                    within the Bureau of Educational and Cultural Affairs, Office of English Language
                                    Programs and is administered by the Center for Intercultural Education and Development
                                    at Georgetown University, Washington, DC. <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <strong><a href="http://www.fcie.org" target="_blank">Florida Consortium for International Education</a></strong><br>
                                    Tel: 407-582-3188 <br>
                                    Fax: 407-582-3003 <br>
                                    Email: <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 <div>This is a group of  Florida colleges and universities committeed to improving the
                                    quality of international education in the State of Florida. Meetings are held once
                                    or twice a year to network and share ideas in the areas of international student recruitment,
                                    partnerships, and study abroad.<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <a href="http://www.cies.org/IEA/" target="_blank"><strong>Fulbright International Education Administrators Program </strong></a>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>The International Education Administrators (IEA) seminars program helps international
                                       education professionals and senior higher education officials from the United States
                                       create empowering connections with the societal, cultural and higher education systems
                                       of other countries.&nbsp; Grantees have the opportunity to learn about the host country’s
                                       education system from the inside out as well as establish networks of U.S. and international
                                       colleagues.&nbsp; Grantees return with enhanced ability to serve and encourage international
                                       students and prospective study-abroad students. See the<a href="http://catalog.cies.org/viewOpenAwards.aspx" target="_blank"> Fulbright Scholar Program Awards Catalog</a> for more information. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div><strong><a href="http://www.fulbrightteacherexchange.org/" target="_blank">Fulbright Teacher Exchange Program</a></strong></div>
                                 
                                 <div>This <a href="http://www.fulbrightteacherexchange.org/cte.cfm"></a> provides opportunities for teachers to participate in direct exchanges of positions
                                    with colleagues from other countries for a semester or a year. By living and working
                                    abroad, exchange teachers gain an understanding and appreciation of different educational
                                    systems and cultures, and enrich their schools and communities by providing students
                                    with new perspectives about the world in which they live. <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.nafsa.org/" target="_blank"><strong>NAFSA: Association of International Educators</strong><br>
                                          </a>1875 Connecticut Avenue, NW, Suite 1000 <br>
                                       Washington, DC 20009-5728 <br>
                                       Tel: 202-939-3103 <br>
                                       Fax: 202-667-3419 <br>
                                       Email: <a href="mailto:inbox@nafsa.org%20">inbox@nafsa.org </a>                  
                                    </p>
                                    
                                 </div>
                                 
                                 <div>                  
                                    <p>NAFSA: Association of International Educators promotes the exchange of students and
                                       scholars to and from the United States. Our members share a belief that international
                                       educational exchange advances learning and scholarship, builds respect among different
                                       peoples and encourages constructive leadership in a global community. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><a href="http://www.neh.gov/" target="_blank"><strong>NEH Endowment for the Humanities</strong></a></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>NEH is an independent grant-making agency of the United States government dedicated
                                       to supporting research, education, preservation, and public programs in the humanities.
                                       Valencia faculty apply every year for grants that provide funding for international
                                       experiences. <a href="http://www.neh.gov/grants/index.html" target="_blank">Click here</a> for information on how to apply for a grant.
                                    </p>
                                    
                                    <p><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    <a href="http://www.ucfglobalperspectives.org/" target="_blank"><strong>UCF's Office for Global Perspectives</strong></a> <br>
                                    Howard Phillips Hall 202<br>
                                    4000 Central Florida Blvd.<br>
                                    POB 160003<br>
                                    Orlando, FL 32816-0003<br>
                                    Tel: 407-823-0688 <br>
                                    Email: <a href="mailto:freeman@mail.ucf.edu">freeman@mail.ucf.edu</a>
                                    
                                 </div>
                                 
                                 <div> The mission of the Office of the Special Assistant to the President for Global Perspectives
                                    is to sharpen UCF's international focus. The office helps advance UCF's goal of providing
                                    international emphasis to curricula and research. In addition, it works to expand
                                    the university's efforts to enlarge Central Florida's awareness and understanding
                                    of the interconnectedness of the global community. <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p><strong>Valencia's International Faculty Exchange Opportunities </strong></p>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <p>Valencia has written agreements with the following institutions to do faculty exchange
                                       programs. Please note that the <a href="../faculty-staff/committees.html" target="_blank">SAGE Advisory Committee</a> is currently creating an application process for these programs. The incoming faculty
                                       from overseas must be brought in as a <a href="http://international.valenciacollege.edu/" target="_blank">J Exchange Visitor</a>. 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>Koning Willem I, The Netherlands<br>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        <p><br>
                           
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/resources/associationsandorganizations.pcf">©</a>
      </div>
   </body>
</html>