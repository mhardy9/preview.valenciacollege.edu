<ul>
	<li><a href="/students/study-abroad">Study Abroad</a></li>
	
<li class="submenu"><a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
	<ul>
    <li><a href="/students/study-abroad/students/welcome.php">Program Advisors</a></li>					
    <li><a href="/students/study-abroad/students/shortterm.php">Short-Term</a></li>					
    <li><a href="/students/study-abroad/students/semester.php">Semester</a></li>					
    <li><a href="/students/study-abroad/students/exchanges.php">Exchanges</a></li>					
    <li><a href="/students/study-abroad/students/internships_new.php">Internships</a></li>					
    <li><a href="https://www.flickr.com/photos/valencia_sage/sets/">Photo Gallery</a></li>					   
    <li><a href="/students/study-abroad/students/shorttermprograms.php">Short-Term Study Abroad Programs</a></li>
    <li><a href="/students/study-abroad/students/shortterm.php">Short-Term How to Apply</a></li>
    <li><a href="/students/study-abroad/students/scholarshipeligibility.php">Short-Term Scholarship Elegibility</a></li>
    <li><a href="/students/study-abroad/students/shorttermfundingstudyabroad_new.php">Short Term Funding Study Abroad</a></li>
    <li><a href="/students/study-abroad/students/financialaid.php">Short Term Financial Aid</a></li>
    <li><a href="/students/study-abroad/students/beforeyougo.php">Before You Go</a></li>
    <li><a href="/students/study-abroad/students/whenyoureturn.php">When You Return</a></li>
    <li><a href="/students/study-abroad/students/semesterprograms.php">Semester Program Providers</a></li>    
    <li><a href="/students/study-abroad/students/semester.php">Semester How to Apply</a></li>					
    <li><a href="/students/study-abroad/students/scholarshipeligibilitySSA.php">Semester Scholarship Elegibility</a></li>
    <li><a href="/students/study-abroad/students/semesterfundingstudyabroad_new.php">Semester Funding Study Abroad</a></li>
    <li><a href="/students/study-abroad/students/financialaidSSA.php">Semster Financial Aid</a></li>
    <li><a href="/students/study-abroad/students/semesterprograms.php">Semester Program Providers</a></li>
    <li><a href="/students/study-abroad/students/internationaltravel.php">Register Your Trip</a></li>
    <li><a href="/students/study-abroad/students/studentexchangeprograms.php">Exchange Program Providers</a></li>
    <li><a href="/students/study-abroad/students/exchanges.php">Exchange How to Apply</a></li>
    <li><a href="/students/study-abroad/students/exchangefundingstudyabroad_new.php">Exchange Funding Study Abroad</a></li>
    <li><a href="http://net4.valenciacollege.edu/forms/international/study-abroad/register_request.php" target="_blank">Register Your Exchange Trip</a></li>
    <li><a href="/students/study-abroad/students/internationalinternshipapply.php">International Internships How to Apply</a></li>
    <li><a href="/students/study-abroad/students/">Program Information</a></li>
    <li><a href="/students/study-abroad/students/islprograminfo.php">International Service Learning Program Information</a></li>
    <li><a href="/students/study-abroad/students/islhowtoapply.php">International Service Learning How to Apply</a></li>
    <li><a href="/students/study-abroad/students/internationaleducationweek.php" class="Option">International Education Week</a></li>
    <li><a href="/students/study-abroad/resources/" class="Option">International Education Week Calendar of Events</a></li>
    <li><a href="/students/study-abroad/students/iew-photocontest.php" class="Option">International Education Week Photo Contest</a></li>
		</ul>
	</li>
	
	<li class="submenu"><a href="javascript:void(0);" class="show-submenu">Faculy &amp; Staff <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
	<ul>
      <li><a href="/students/study-abroad/staff/submitproposal.php">Lead a Study Abroad Program</a></li>						
      <li><a href="/students/study-abroad/staff/">Program Leader-in-Training</a></li>
      <li><a href="/students/study-abroad/staff/">Program Providers</a></li>
      <li><a href="/students/study-abroad/staff/programleaders.php">SAGE Program Leaders</a></li>    
      <li><a href="/students/study-abroad/staff/exchanges.php">Faculty and Staff Exchanges</a></li>
      <li><a href="/students/study-abroad/staff/">How to Apply</a></li>
      <li><a href="/students/study-abroad/staff/international-education-conference-request.php">Exchange Program Partners</a></li>
      <li><a href="http://valenciacollege.edu/international/study-abroad/faculty-staff/curriculum/fulbright.php">Fullbright Programs</a></li>
      <li><a href="/international/exchange/">J Exchange Visitor Program</a></li>
          <li><a href="/students/study-abroad/staff/internationaltravel.php">International Travel Checklist</a></li>							
      <li><a href="http://net4.valenciacollege.edu/forms/international/study-abroad/register_request.php" target="_blank">Register Your International Trip</a></li>	
		</ul>
	</li>
<li class="submenu"><a href="javascript:void(0);" class="show-submenu">Resources <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
	<ul>
    <li><a href="/students/study-abroad/resources/travelandsafety.php">Safety &amp; Emergency Preparedness</a></li>
    <li><a href="/students/study-abroad/resources/">When You Return</a></li>				
      <li><a href="/students/study-abroad/resources/travelandsafety.php">Travel and Safety</a></li>	
      <li><a href="/students/study-abroad/resources/foreignvisitors.php">Foreign Visitors</a></li>										
      <li><a href="/students/study-abroad/resources/languageambassadors.php">Valencia Language Ambassadors</a></li>	
		</ul>
</li>
	</ul>	   