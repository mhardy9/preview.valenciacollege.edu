<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/internationaltravel.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>Register Your International Travel with SAGE </h2>
                        
                        <p>This webpage has been created to assist you with your overseas travel. You can print
                           this page and use it as a checklist:
                        </p>
                        
                        <ul>
                           
                           <li>Per <a href="../../../about/general-counsel/policy/documents/5-08-Travel-by-Authorized-Personnel.pdf.html" target="_blank">College policy 6Hx28:5-08</a>, all Valencia staff and faculty traveling overseas must register with the SAGE office
                              no later than 15 days prior to departure.<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <u><strong>You do NOT need to complete this process if</strong></u>: 1) you are leading a study abroad program with students or 2) you are traveling
                              to Canada.<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <u><strong>GROUP TRAVELERS (i.e., NEH Grant)</strong></u><strong>:</strong> One person should be the communication point person with the SAGE office. Please
                              complete the <a href="documents/TravelersInformationWorksheet_InternationalGroupTravel.xlsx">Traveler's Information Worksheet - Group Travel</a> for the entire group and send it to the SAGE office. <br>
                              <br>
                              
                           </li>
                           
                           <li> Email the SAGE office at <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a> or call 407-582-3188 if you have any questions on filling out these forms. 
                           </li>
                           
                        </ul>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong>STEP</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>WHO</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>TASK</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 1: <br>
                                       Check for a Travel Warning </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">Check for a <a href="https://travel.state.gov/content/passports/en/alertswarnings.html" target="_blank">Department of State Travel Warning</a> for the destination country. If you see that your destination is listed on the warning
                                    list, please contact the SAGE office. The College may not fund any trips going to
                                    a country with an active travel warning. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 2: <br>
                                       Request for International Travel Form </strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Faculty exchanges</p>
                                    
                                    <p>International conferences</p>
                                    
                                    <p>Program Leader-in-Training </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>You do NOT need to complete this step if you are traveling on an<span><strong> Endowed Chair</strong></span>, a <span><strong>sabbatica</strong></span>l, or an <span><strong>NEH grant</strong></span>. 
                                    </p>
                                    
                                    <p>Complete the <a href="documents/IntlTravel_RequestforInternationalTravelForm_000.pdf" target="_blank">Valencia Request for International Travel Form</a> by <strong>October 1</strong> for the academic year in which the travel will take place. This form requires dean/supervisor
                                       and campus president/vice president signatures. Send the form with signatures to the
                                       SAGE office DO-335 or email it to <a href="mailto:jrobertson@valenciacollege.edu">rbrighton1@valenciacollege.edu</a>.
                                    </p>
                                    
                                    <p><span>INTERNATIONAL EDUCATION BUDGET FUNDING REQUESTS:</span> If you are requesting funding from the International Education (SAGE) budget, you
                                       must submit a <a href="documents/IntlTravel_PlannedProgramItineraryforInternationalTravel.docx">Planned Program Itinerary for International Travel</a> and a <a href="documents/IntlTravel_ProposedBudgetforInternationalTravel.xlsx">Proposed Budget for International Travel</a> with the request form. PLEASE NOTE THAT THE COLLEGE NO LONGER REIMBURSES FOR MEALS
                                       FOR INTERNATIONAL TRAVEL. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 3: <br>
                                       Proposal Form </strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Faculty exchanges</p>
                                    
                                    <p>International conferences</p>
                                    
                                    <p>Program Leader-in-Training </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Complete any additional paperwork as applicable. Ask the SAGE office if you are exempt
                                       from this step or which forms apply if you are unsure. Obtain the required signatures
                                       and send the completed forms to the SAGE office. <span>Sabbaticals and Endowed Chairs: These have their own application/proposal process.
                                          You can skip this step. </span></p>
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="documents/Faculty-StaffInternationalProgramProposalInstructions.docx">Faculty-Staff International Program Proposal Instructions</a>, <a href="documents/Faculty-StaffInternationalProgramProposal.docx">Faculty-Staff International Program Proposal</a> (use for faculty exchange programs - one way or reciprocal) <br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="documents/StudyAbroadProgramLeader-in-TrainingProposalForm.docx">Program Leader-in-Training Mendorship</a>, <a href="documents/PLITProgramExpectations.docx">Program Expectations</a> <br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="documents/SAGESiteVisitQuestionnaireforOverseasColleges.docx">Questionnaire for Overseas Colleges</a> (use for site visits for Endowed Chairs) 
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 4: <br>
                                       Passport and Visa </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Ensure that your passport is up to date and apply for a <a href="http://www.visahq.com/" target="_blank">visa</a> if applicable (be sure to verify the visa requirements). Some countries require your
                                       passport to be valid at least 6 months after your return to the United States. <strong>Please send a clear copy of your passport to the SAGE office. </strong>
                                       
                                    </p>
                                    
                                    <p>
                                       
                                       <a href="http://net4.valenciacollege.edu/forms/international/studyabroad/faculty-staff/passportsubmission.cfm" target="_blank">Submit Proof of Passport</a><br>
                                       <br>
                                       If you need 
                                       to get information on applying for a passport for the first time or to know about
                                       a particular place to go and get your passport with to no appointments, no wait. Please
                                       follow the links: 
                                       
                                       
                                       <a href="https://spot.ucf.edu/index.html">The Spot at UCF</a> - 
                                       
                                       
                                       <a href="http://cdn.ucf.edu/map/printable/UCF-Campus-Map-2014.pdf">UCF Map </a></p>
                                    <br>
                                    For additional information, visit this page: <a href="../students/travelsafety.html" target="_blank">Travel and Safety</a> 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 5: Airline Reservations </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><u>INDIVIDUAL TRAVELERS</u>: You can make your reservations online. Ask your department for assistance. You can
                                       request a cash advance on your authorization form for the airline ticket. Accounts
                                       Payable will send you a check one week prior to departure. <strong>If the money is coming from the SAGE budget, you must contact the SAGE director to
                                          get approval on your ticket before making the purchase. </strong></p>
                                    
                                    <p><u>GROUP TRAVELERS (i.e., NEH grant)</u>: Contact Robert Reed at Travel Max (407-704-7850). He will need you to complete the
                                       <em>Traveler's Information Worksheet - Group Travel</em> in order to purchase the tickets. <br>
                                       <br>
                                       Ask your supervisor if you can use the department credit card (P-card) to pay for
                                       the flights. The cardholder will need to charge the amount to the approved budget
                                       in PaymentNet. The budget manager will need to be informed of the purchase and will
                                       require all receipts. Contact Accounts Payable if you do not know the budget index
                                       number. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 6: Accommodations Reservations </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    <p> Make your hotel or other accommodations arrangments. You can either: 1) pay for the
                                       hotel and get reimbursed after your trip, or 2) get the hotel set up in the system
                                       as a vendor and Valencia will pay the hotel directly. Please contact Accounts Payable
                                       for more details (ext. 3312). 
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 7: Authorization for International Travel Form </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Faculty and staff must complete their own form. Contact your department assistant
                                       for help. <strong>For detailed instructions on how to complete this form, click <a href="http://valenciastudyabroad.pbworks.com/w/page/31749225/Authorization-for-International-Travel" target="_blank">HERE</a>.</strong></p>
                                    
                                    <p><a href="documents/IntlTravel_AuthorizationforInternationalTravelForm.pdf">Authorization for International Travel Form</a>  
                                    </p>
                                    
                                    <p><strong>Per the requirements of Accounts Payable, you MUST provide supporting documentation</strong> <strong>for ALL costs related to the international travel for which Valencia is paying.</strong> <strong> Example supporting documents....</strong> 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>airline quote or receipt from the travel agent or a website (please provide the days
                                          and times of departure and return) 
                                       </li>
                                       
                                       <li>tour operator quote or receipt </li>
                                       
                                       <li>insurance quote or receipt</li>
                                       
                                       <li>hotel website pages with prices (if you cannot obtain hotel costs, we will use the
                                          Dept. of State'smaximum lodging rate on the Per Diem by Country website page below)
                                       </li>
                                       
                                       <li>cultural activities webpages with prices</li>
                                       
                                       <li> supporting documentation for any other costs associated with the trip</li>
                                       
                                    </ul>
                                    
                                    <p><strong>The following is a list of expenses that SAGE funds will NOT cover:</strong> meals per diem, airport parking, airline upgrades, entrance fees to cultural or social
                                       activities that are not directly related to the internationalized curriculum project,
                                       and expenses for incoming participants from the overseas institution 
                                    </p>
                                    
                                    <p><strong>Please forward your signed paperwork to the SAGE office at DO-335. We will stamp it
                                          received and forward it to Accounts Payable. Accounts Payable will not process your
                                          form without the SAGE stamp on it. </strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 8:<br>
                                       Register Your Travel Plans with SAGE </strong></div>
                                 
                                 <div data-old-tag="td">All travelers EXCEPT NEH Grant groups </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Please complete the travel registration at least <strong>15 days prior to your trip</strong>. <strong>You will need your destination itinerary, passport, airline, and accommodations information
                                          in order to complete your registration. </strong></p>
                                    
                                    
                                    <p><strong><u>NEH Grant Participants</u>:</strong> The coordinator will need to complete the<a href="documents/TravelersInformationWorksheet_InternationalGroupTravel.xlsx"> Travelers Information Worksheet </a>and send to the SAGE Office in lieu of completing the individual online form. 
                                    </p>
                                    
                                    <p><strong>IMPORTANT: </strong>SAGE must receive specific accommodation locations in order to register you with the
                                       <a href="https://step.state.gov/step/" target="_blank">Department of State's STEP Program</a>. Failure to provide this information will mean that you will not be registered and
                                       we will not receive important embassy updates in the event that something occurs in
                                       your country destination. 
                                    </p>
                                    
                                    <ul>
                                       
                                       <li>If you will need an international cell phone, you can request one through the SAGE
                                          office. Please note that we do not always have phones available. International calling
                                          is VERY expensive, so the SAGE phone should only be used for emergency purposes. Please
                                          see the <a href="documents/TelestialPhoneCountryRates.docx">Telestial Phone Country Rates</a>. If you are not traveling as part of a study abroad program, you must provide the
                                          SAGE office with the budget information to charge your department. <span>NOTE: You will be responsible for payment of all non-emergency phone calls</span>.<br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>If you would like to have an iPad instead of an international phone, the SAGE office
                                          can issue you one if available at the time of your travel (study abroad program leaders
                                          get priority). The iPad is set up with lots of travel apps and Facetime to make video
                                          calls. IF YOU LOSE ANY OF THE ACCESSORIES, YOU WILL BE REQUIRED TO REPLACE THEM. <br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>If you have your own smartphone or iPad, we also have portable travel batteries that
                                          hold up to four charges and electrical adaptors for different countries that we can
                                          issue you. <strong><br>
                                             </strong>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 9: <br>
                                       Medical/Trip Insurance</strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Valencia requires all international travelers who are working during their trip to
                                       carry medical/trip insurance.  Starting June 2015, the cost is approximately <strong>$36.69</strong> per person for every <strong>15 days.</strong> Please note that this is additional coverage that includes overseas emergency assistance.
                                       See the <a href="documents/PremierGroupDocPlan1FrontierMEDEXFaculty.pdf">coverage plan</a> for details. 
                                    </p>
                                    
                                    <p> Once you have registered your trip online (step 8), contact the SAGE Office with
                                       the budget number that the insurance will be paid out of. If you are paying with personal
                                       funds, you can pay at the campus Business Office. Contact the SAGE Office for details
                                       on how to complete this process.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 10: Department of State Registration </strong></div>
                                 
                                 <div data-old-tag="td">All travelers</div>
                                 
                                 <div data-old-tag="td">
                                    <p> Upon receipt of your registration (STEP 8), the SAGE office will email you a copy
                                       of <em>Valencia's Emergency-Crisis Management Plan for International Travel, </em>register you with the <a href="https://step.state.gov/step/" target="_blank">Department of State's STEP (Smart Travelers Enrollment Program)</a>, and will inform Valencia's Emergency Response Team of your upcoming travel. <br>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 11:<br>
                                       Finances </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>It is very important to keep a copy of all your receipts that are related to the costs
                                       of your travel. Please use the <a href="documents/InternationalExpenseReportv2.xlsx" target="_blank">International Expense Report</a> to make it easier to figure out the expenses for the final per diem.
                                    </p>
                                    
                                    <p>Our program providers suggest that you exchange money PRIOR to leaving the United
                                       States in order to get the best rates. There is a currency exchange booth at the Florida
                                       Mall (407-854-0860). 
                                    </p>
                                    
                                    <p>If you will be using a personal credit card overseas, be sure to contact the credit
                                       card company in advance to let them know so that they do not freeze your account for
                                       suspicious activity. 
                                    </p>
                                    
                                    <p>If you are traveling to Europe, be aware that some countries are in the process of
                                       using a new credit card system that uses a chip and pin. The magnetic swipe credit
                                       cards will not work. You should find out what system is in use at your destination
                                       and plan accordingly. You may need to have access to cash, which can be obtained at
                                       the bank or ATM. Be sure to take a debit card for cash access. Here is a copy that
                                       offers the new chip and pin credit card: <a href="https://www.andrewsfcu.org/credit_cards_and_loans/credit_cards/globetrek_rewards.html" target="_blank">Andrews Federal Credit Union</a>. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 12:<br>
                                       Know Before You Go </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The following is a list of other important information for your international travel:
                                       
                                    </p>
                                    
                                    <p>You must contact CMI <strong>before</strong> you request or pay for any medical services. They may not reimburse you if you do
                                       not contact them first. Call the 24-7 number on your card.<br>
                                       <br>
                                       <br>
                                       Please visit the <a href="../resources/travelandsafety.html">Travel and Safety</a> webpage for lots of useful links. 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 13: <br>
                                       Upon Your Return </strong></div>
                                 
                                 <div data-old-tag="td">All travelers </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Please complete the<a href="documents/IntlTravel_PerDiemForm.pdf"> Per Diem Form for International Travel</a>. Include a copy of ALL receipts (airfare, hotel, ground transportation, entrance
                                       fees, etc.) with your per diem form for close out. <strong>For detailed instructions on how to complete this form, click <a href="http://valenciastudyabroad.pbworks.com/w/page/53088451/Per-Diem-for-International-Travel" target="_blank">HERE</a>. </strong>Send the Per Diem, receipts, and international cell phone (if applicable) to the SAGE
                                       office at DO-335.
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>STEP 14: <br>
                                       Faculty Exchange Close Out </strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Faculty Exchanges</p>
                                    
                                    <p>Program Leader-in-Training</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>Contact the SAGE office to let us know how you will be meeting the "sharing" requirement
                                          of your program (Cafe Conversations, department presentation, conference presentation,
                                          International Education Week, Skillshop, etc.). <br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>
                                          <u>OPTIONAL</u>: Upload photos to the SAGE Flickr (create a set for your program): <a href="http://valenciastudyabroad.pbworks.com/w/page/31158209/Upload-Photos-to-SAGE-Flickr" target="_blank">http://valenciastudyabroad.pbworks.com/w/page/31158209/Upload-Photos-to-SAGE-Flickr</a> OR send us a link to your blog to post to the SAGE website. <br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>
                                          <u>EXCHANGES</u>: Submit your "Internationalizing the Curriculum" project to the SAGE office by the
                                          end of the semester after you return. <br>
                                          <br>
                                          
                                       </li>
                                       
                                       <li>
                                          <u>PLIT</u>: Submit your final report within 30 days upon your return. <br>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/internationaltravel.pcf">©</a>
      </div>
   </body>
</html>