<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/leadstudyabroad/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/leadstudyabroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li>Leadstudyabroad</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        
                        <h2>Submit a Proposal to Lead a Study Abroad Program in 2016</h2>
                        
                        
                        <p>In order to receive important information about study abroad, international education
                           conferences, faculty exchanges, internationalizing the curriculum, and international
                           travel from SAGE, please subscribe to the <a href="http://feedburner.google.com/fb/a/mailverify?uri=TheGroveValenciaCollegeGlobalExperiences&amp;loc=en_US" target="_blank">SAGE blog</a>. Email blasts will no longer be sent out college-wide. 
                        </p>
                        
                        <p>The first step for anyone interested in leading a study abroad program, participating
                           as a program leader-in-training on a study abroad program, participating in a faculty
                           exchange program, or attending an international conference is to complete the following
                           form. This form is due to the SAGE office by <strong>OCTOBER 1</strong> of each year for travel during the academic year (fall, spring, summer). This form
                           gives you approval to go to the next step in the process which is to complete the
                           proposal form. NOTE: You are NOT required to complete this form for sabbaticals or
                           Endowed Chair travel. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong><a href="documents/IntlTravel_RequestforInternationalTravelForm.pdf" target="_blank">REQUEST FOR INTERNATIONAL TRAVEL FORM</a></strong><a href="documents/IntlTravel_RequestforInternationalTravelForm.pdf"><strong> </strong></a>
                              
                           </li>
                           
                        </ul>
                        
                        <p><br>
                           Click on <strong>SUBMIT A PROPOSAL </strong>below in order to complete the process to apply to lead short-term study abroad in
                           2018. The proposal deadline date is <span><strong>February 28, 2018 at 11:59 pm. </strong></span></p>
                        
                        <div>
                           
                           <h3>Submit a Proposal</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Thank you for your interest in submitting a proposal to lead or co-lead a short-term
                                    study abroad program at Valencia! Proposals are submitted the year prior to running
                                    the program. This year's <strong>call for proposals is January 6 - February 24, 2017</strong> to lead a study abroad program in the spring or summer term of <strong>2018</strong>. You must be a full-time faculty member in order to apply to lead or co-lead a study
                                    abroad program.
                                 </p>
                                 
                                 <p><strong>STEP 1: </strong>Complete the <a href="documents/IntlTravel_RequestforInternationalTravelForm.pdf" target="_blank">Request for International Travel Form</a><a href="../documents/IntlTravel_RequestforInternationalTravelForm.pdf">,</a> get your dean and campus president signatures, and submit to the SAGE office by the
                                    date listed above. 
                                 </p>
                                 
                                 <p> <strong>STEP 2: </strong>Attend the <a href="leadstudyabroadprogram.html">Study Abroad Program Leader Certificate</a> classes in the <strong>FALL TERM</strong>. Please note that this is a mandatory requirement in order to lead or co-lead a study
                                    abroad program. See the<a href="../events.html"> Faculty and Staff Events</a> page for more details. 
                                 </p>
                                 
                                 <p><strong>STEP 3: </strong>Complete any of the  Program Proposals below to Lead Short-Term Study Abroad that
                                    apply to your program: <strong>Coming Soon</strong></p>
                                 
                                 <ul>
                                    
                                    <li><a href="documents/2019-Proposal-Packet.docx" target="_blank">2019 Program Proposal Packet</a></li>
                                    
                                    <li><a href="documents/Program-Proposal-Expense-Worksheet.xlsx" target="_blank">Program Proposal Expense Worksheet</a></li>
                                    
                                 </ul>
                                 
                                 <p>Please submit your proposal by the deadline date listed above. In order to submit
                                    a completed application, you will need the following documents to submit as part of
                                    your proposal:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li> official course outline</li>
                                    
                                    <li> program proposal packet </li>
                                    
                                    <li>detailed program itinerary (follow the guidelines provided in the workshop)</li>
                                    
                                    <li>program proposal expense worksheet </li>
                                    
                                    <li>program provider proposal (the one you are using)</li>
                                    
                                 </ul>
                                 
                                 <p><strong>STEP 4: </strong>Print out the application packet and all attachments,<strong> SINGLE-SIDED</strong>, <strong>NO STAPLES</strong> and obtain signatures and submit as outlined below. Please note that this is a competitive
                                    process and not all proposals will be approved. Incomplete or late applications will
                                    not be accepted. The following dates are guidelines to help you ensure that you complete
                                    the proposal on time.
                                 </p>
                                 
                                 <p><strong>STEP 5: </strong>The proposal will be reviewed by the <a href="../committees.html">Study Abroad Committee</a> and recommendations will be given to the<a href="../committees.html"> International Education Connections Team</a> for final approval. Notification for approval will be given by early April. At that
                                    time, you can begin marketing your program. The following is the list of possible
                                    outcomes:   (1) Recommended and funded; (2) Recommended and waitlisted for funding;
                                    (3) Recommended and not funded; (4) Not recommended or funded.
                                 </p>
                                 
                                 <p><strong>STEP 6:</strong> Once approved, you must create a program booklet or update your existing booklet
                                    if this is a repeat program. Contact the SAGE Office for the template to get started.
                                    The booklet is due by <strong>August 1</strong>.
                                 </p>
                                 
                                 <p><span><strong>STEP 7:</strong></span> If you have been funded, you will be informed of the scholarships awarded to your
                                    program after <strong>August 1</strong> and after your program booklet is received in the SAGE Office. 
                                 </p>
                                 
                                 <p><strong>Please note the following:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Incomplete and late applications will not be accepted.</li>
                                    
                                    <li>You must have submitted a <em>Request for International Travel Form</em> by <strong>October 1</strong> in order to qualify to apply. 
                                    </li>
                                    
                                    <li>You can apply to lead two study abroad programs per year. However, for the second
                                       trip to be approved, it cannot bump a new program that has not been approved before.
                                       
                                    </li>
                                    
                                    <li>To ensure a program that meets Valencia's standards for academic excellence and does
                                       not put additional financial burdens on students, program leaders are only allowed
                                       to visit a maximum of two different countries. 
                                    </li>
                                    
                                    <li>You must read the <strong>Safety &amp; Emergency-Crisis Management Plan for Study Abroad</strong> prior to submitting your proposal. If you have not received a copy of this document,
                                       contact the SAGE Office. 
                                    </li>
                                    
                                    <li>Apply for a Valencia Foundation <a href="http://www.valencia.org/forms.cfm" target="_blank"><strong>Endowed Chair</strong></a> in <strong>APRIL</strong> to help fund your program.
                                    </li>
                                    
                                    <li>Meals that are not included in the program fee for the program leader/s will not be
                                       covered for study abroad. 
                                    </li>
                                    
                                    <li>This course must be paid as an overload by your department.</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Co-Program Leader Policy: </strong>If you have a co-program leader, you both must recruit at least 16 students. If the
                                    program has less than 16 students and there is a financial impact on students (the
                                    price has to increase), then the co-program leader will not be able to participate
                                    unless additional funding is identified. If you cannot achieve the minimum ratio after
                                    all efforts have been exhausted, and there is no financial impact to the students
                                    (i.e., free ticket for 12 students from EF Tours), then the co-program leader can
                                    participate.
                                 </p>
                                 
                                 <p><strong>Family Policy:</strong> If you wish to take a spouse/significant other on study abroad, you can do so outside
                                    of the Valencia process.&nbsp; He/She does not have to enroll in the course.&nbsp; You will
                                    have to make the arrangements on your own and pay all associated fees. &nbsp;Please note
                                    that the participation of your spouse/significant other cannot impact the cost of
                                    the trip in any way. &nbsp;For example, if you are at the maximum allowed for a bus, and
                                    adding another person would incur the cost of a second bus. If you wish to take your
                                    child, he/she must be at least 18 years old and enroll in the course as a student.&nbsp;
                                    Minors are not permitted.
                                 </p>
                                 
                                 <p><strong>NEW  POLICIES APPROVED BY THE STUDY ABROAD COMMITTEE FOR 2016:</strong></p>
                                 
                                 <ol>
                                    
                                    <li>The minimum number of students is now <strong><u>10</u></strong> for repeat programs at year 3 and beyond in order to lock in airfare rates and keep
                                       costs down for students. The minimum number of students for new programs is <u><strong>8</strong></u> students. 
                                    </li>
                                    
                                    <li>Final proposal scores will not be published for faculty applying to lead study abroad.
                                       
                                    </li>
                                    
                                    <li>There will be a $250 stipend for all program leaders after close out.</li>
                                    
                                    <li>There will no longer be a $200 discretionary amount added to budgets. You can budget
                                       in additional expenses to be paid in-country, but they must be identified on the budget.
                                    </li>
                                    
                                    <li>All programs must budget a $500 emergency fund.  All program leaders are required
                                       to take a $500 cash advance on the Pcard prior to departing and exchange some funds
                                       in the foreign currency.
                                    </li>
                                    
                                    <li>If you break any of the Pcard procedures, you will not be issued a Pcard in the future.
                                       You will have to take a co-program leader that qualifies for a Pcard on any future
                                       programs. 
                                    </li>
                                    
                                    <li>Faculty who are on 8-month contracts can be program leaders, but they will need to
                                       get a sponsor who is tenure or tenure-track to agree to take over in the event he
                                       or she cannot fulfill the obligations of the program. This will go into effect 2017.
                                    </li>
                                    
                                 </ol>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <h3>Program Providers</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> If you are looking for a third-party program provider, here is a list of companies
                                    that offer great tours at reasonable prices. You select the tour and build the study
                                    component around the locations visited. You can also build a custom tour at a higher
                                    price and with a minimum amount of students. Please note that you are not limited
                                    to these companies, and you can use any program provider that your dean and campus
                                    president approve. 
                                 </p>
                                 
                                 <p><span><strong>NOTE</strong></span><strong>: Preference is given to program providers with in-country tour operators or contacts
                                       who will be available in the event of an emergency. </strong></p>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.aifspartnerships.com/" target="_blank">American Institute for Foreign Study (AIFS)</a></li>
                                    
                                    <li><a href="http://www.appina.travel/main.php?site=start&amp;lang=en" target="_blank">Appina Travel </a></li>
                                    
                                    <li><a href="http://www.capa.org/" target="_blank">CAPA International Education </a></li>
                                    
                                    <li><a href="http://www.casterbridgetours.com/" target="_blank">Casterbridge Tours </a></li>
                                    
                                    <li><a href="http://www.spanishstudies.org/customizedprograms" target="_blank">CC-CS: Study Abroad in Spain and Argentina</a></li>
                                    
                                    <li>
                                       <a href="http://www.ciee.org/international/studyabroad/advisors/custom/" target="_blank">CIEE Study Abroad</a> 
                                    </li>
                                    
                                    <li>
                                       <a href="http://www.cepa-europe.com/faculty-led-programs/destinations/europe/" target="_blank">CEPA Europe</a> 
                                    </li>
                                    
                                    <li><a href="http://www.efcollegestudytours.com/" target="_blank">EF College Study Tours</a></li>
                                    
                                    <li>
                                       <a href="http://www.europeandestinations.com/" target="_blank">European Destinations</a> (airfare)
                                    </li>
                                    
                                    <li><a href="http://www.explorica.com/" target="_blank">Explorica</a></li>
                                    
                                    <li><a href="http://www.fellowship.com/tours/" target="_blank">Fellowship Travel International </a></li>
                                    
                                    <li>
                                       <a href="../documents/IAUResidentFellows_FacultyLed.pdf" target="_blank"> IAU College - France</a> - (they coordinate programs throughout Europe including Spain and Morocco)
                                    </li>
                                    
                                    <li><a href="http://www.keiabroad.org/" target="_blank">Knowledge Exchange Institute</a></li>
                                    
                                    <li>
                                       <a href="http://www.odysseys-unlimited.com/" target="_blank">Odysseys Unlimited (The small group travel experience)</a> 
                                    </li>
                                    
                                    <li><a href="http://www.passports.com/" target="_blank">Passports Educational Group Travel </a></li>
                                    
                                    <li>Places to stay: <a href="https://www.airbnb.com/" target="_blank">www.airbnb.com </a>
                                       
                                    </li>
                                    
                                    <li>
                                       <u><a href="http://selecttravelstudy.com/" target="_blank">Select Travel Study</a></u> 
                                    </li>
                                    
                                    <li>Service Learning Opportunities: <a href="http://www.heifer.org/site/c.edJRKQNiFiG/b.201457/" target="_blank">Heifer International </a>
                                       
                                    </li>
                                    
                                    <li><a href="http://www.teachbytravel.com/" target="_blank">Teach by Travel</a></li>
                                    
                                    <li>
                                       <a href="http://www.teacherslovetravel.com" target="_blank">Teachers Love Travel</a> 
                                    </li>
                                    
                                    <li><a href="http://www.untours.com/" target="_blank">Untours</a></li>
                                    
                                    <li>
                                       <a href="http://www.worldculturaltours.com" target="_blank">World Cultural Tours (Custom Tours)</a>                    
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>Using any program provider will require a signed agreement each year. Once you have
                                    selected a program provider, you must contact the SAGE office to review the logistics
                                    and obtain a signed agreement. 
                                 </p>
                                 
                                 <p><a href="#top">TOP</a></p>
                                 
                                 
                                 
                                 <h3>COMMUNITY COLLEGES FOR INTERNATIONAL DEVELOPMENT: </h3>
                                 
                                 <p><a href="http://www.ccidinc.org/" target="_blank">CCID</a> has developed a consortium <a href="http://ccid.studioabroad.com/" target="_blank">Troika</a> programming model for short-term, faculty-led study abroad programs. A minimum of
                                    three CCID colleges collaborate to provide community college students with greater
                                    access to study abroad opportunities. A three- to four-year program agreement is required.
                                    CCID currently has the following collaboration opportunities which you could apply
                                    for:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Transcultural Nursing in Ecuador</li>
                                    
                                    <li>Architecture, Art History, and Design in Estonia, Latvia, Finland or Russia (TBD)
                                       
                                    </li>
                                    
                                    <li>International Engineering in Germany and France</li>
                                    
                                    <li>Hospitality Management and Culinary Arts in Germany, France, and Switzerland </li>
                                    
                                    <li>Global Business in An Emerging Democracy in the Ukraine </li>
                                    
                                    <li> Valencia currently runs the following Troika Programs:</li>
                                    
                                    <li>China's Economic, Social, and Cultural Impact Due to Globalization (Business) </li>
                                    
                                    <li>Global Perspectives in Event Management (Business) </li>
                                    
                                 </ul>
                                 
                                 <p><a href="#top">TOP</a></p>
                                 
                                 
                                 
                                 <h3>FLORIDA CONSORTIUM FOR INTERNATIONAL EDUCATION:</h3>
                                 
                                 <p>FCIE (<a href="http://www.fcie.org" target="_blank">www.fcie.org</a>) has 22 member institutions that collaborate and share study abroad opportunities
                                    for students. Program Leaders are welcome to contact an institution to see if they
                                    can work together on a program. Please note that <a href="http://www.broward.edu/internationalEducation/InternationalEducation/StudyAbroad/page2487.html">Broward College</a> and <a href="http://www.international.fsu.edu/">Florida State University</a> have international partnerships that will allow you to use their facilities for a
                                    fee to create your own program. Facilities for Broward College are located in Spain,
                                    Peru, and Germany. Facilities for Florida State University are located in Spain, England,
                                    London, Italy, and Panama. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li><a href="../../students/shorttermprograms.html">Short-Term Study Abroad Opportunities</a></li>
                                    
                                    <li><a href="../../students/semester/index.html">Semester Study Abroad Opportunities</a></li>
                                    
                                 </ul>
                                 
                                 <p><a href="#top">TOP</a></p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div> 
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/leadstudyabroad/index.pcf">©</a>
      </div>
   </body>
</html>