<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/leadstudyabroad/programleaders.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/leadstudyabroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li><a href="/students/study-abroad/faculty-staff/leadstudyabroad/">Leadstudyabroad</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        <h2>SAGE Program Leaders &amp; PLIT Participants </h2>
                        
                        <p>The following Valencia faculty have led a short-term study abroad program or will
                           be leading a program in the future. They have a WEALTH of information that they are
                           always willing to share! 
                        </p>
                        
                        
                        <h3>Business &amp; Technology</h3>
                        
                        <p><img alt="Professor Hensel" height="251" src="ProfessorGeraldHensel.jpg" width="200">Computer Fundamentals Study Abroad Program
                        </p>
                        
                        <p><span><strong>Jerry Hensel</strong></span>, professor of Information Technology, has been highly involved in study abroad at
                           Valencia. He has led  study abroad trips to Spain and Brazil, and facilitated faculty
                           workshops. It was very exciting for him to see how other people use technology around
                           the world, and how technology plays a role in European and South American businesses.
                           Many of the students that participated in these trips returned home with a new appreciation
                           for the technology available in the United States. 
                        </p>
                        
                        
                        
                        
                        
                        
                        <p><img alt="Professor Hoyte" height="251" src="ProfessorDeymondHoyte.jpg" width="200">(Entrepreneurship) Business &amp; Technology Study Abroad Program
                        </p>
                        
                        <p><span><strong>Deymond Hoyte</strong></span>, Professor of Computer Engineering and Business, has traveled to Barcelona, Spain;
                           Stugart, Germany; and Strasbourg and Paris, France on a site visit to research and
                           arrange   the Sustainability and Alternate Energy study abroad program. Deymond  
                           visited several alternative energy companies, as well as the European Union in Strasbourg.
                           During his visit, he had the opportunitity to speak with a member of parliament about
                           the EU nations plan for sustainability. In addition to his work as a professor, Deymond
                           has led several study abroad programs with Valencia including the Immersion in Global
                           Business program in the countries of China, France, and Germany. His passion for traveling
                           (47 countries) and the love of people, history and culture provides him with a global
                           perspective. His vision is to share his experience and develop  students' entrepreneurial
                           skills.
                        </p>
                        
                        <p>Visit the following link to see the China 2012 program in action, including first-hand
                           accounts from Deymond and students: <a href="https://www.youtube.com/watch?v=Wvo2DYdrH3I">https://www.youtube.com/watch?v=Wvo2DYdrH3I</a> .
                        </p>
                        
                        
                        
                        <p><img alt="Professor Heith Hennel" height="278" src="heith-hennel09_000.jpg" width="200">Computer Fundamentals Study Abroad Program
                        </p>
                        
                        <p><span><strong>Heith Hennel</strong></span>, professor of Information Technology, is a veteran traveler. He proposed to his wife
                           under the Eiffel Tower in Paris, then, he spent a 100 days traveling through Southeast
                           Asia for his honeymoon. He also lived in Greece for a year and has travelled to over
                           30 countries. In addition to his time at Valencia, Heith has  worked in the IT industry
                           with clients such as Google, Motorola and Visa. He enjoys running especially while
                           traveling abroad to see a city/country from a different viewpoint. He has led Valencia
                           trips to Spain, the Dominican Republic, and Brazil.  During these programs, students
                           examined how technology is used in business, as it is technology that can really connect
                           us in a global environment. 
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        <p><img alt="Professor Charlotte Mortimer" height="239" src="photo-Charlotte-Mortimer.jpg" width="189">Mathematics Study Abroad Program
                        </p>
                        
                        <p> Charlotte Mortimer is a professor of Mathematics at Valencia College West Campus.
                           She has been at Valencia since 2012. Charlotte completed her Bachelor of Science at
                           the University of Tampa in Mathematics and Secondary Education in Mathematics and
                           Masters of Arts at UCF in Educational Leadership – Community College. She's had a
                           passion for travel and anything international her whole life. She studied abroad as
                           an undergraduate in Australia. She has traveled to Australia, Italy, France, England,
                           Germany, Netherlands, Denmark, Czech Republic, Spain, Belgium, Mexico, Bahamas, Cayman
                           Islands, Canada, and Jamaica so far. Her MGF 1106 classes are internationalized and
                           quality for Valencia's Global Distinction program.
                        </p>
                        
                        <p>She is a short term study abroad program leader with MGF 1106 College Mathematics.
                           In this class, instead of tests, students will collect information while abroad and
                           use the 5 lenses of the class (Sets, Logic, Probability, Geometry, and Statistics)
                           to organize and analyze the information.
                        </p>
                        
                        
                        
                        <h3>Health</h3>
                        
                        <p><img alt="Professor Schreiber" height="251" src="ProfessorMelissaScheiber.jpg" width="200">Health Education Study Abroad Program
                        </p>
                        
                        <p><span><strong>Mellisa Schreiber</strong></span>, professor of Biology, has traveled to Germany in order to compare US/German health
                           care systems. After that trip she went to Panama to research a tropical parasite called
                           Leishmania , and then traveled to Ethiopia to study about HIV/AIDS. During her studies
                           at the University fo Central Florida (UCF), she had the opportunity to travel to China
                           to present the findings of her graduate research in the development of an oral vaccine
                           for malaria. She has been all over the world: Germany, Netherlands, Panama, Ethiopia,
                           Zambia, South Africa, Mexico, Cayman Islands, Bahamas, Jamaica, Belize, China, Italy,
                           France, Costa Rica, Puerto Ricao, Spain, Beligum, Korea, Ireland, Poland, Hungary,
                           Czech Republic, Thailand, Turkey, Romania, Greece,  St. Maarten, and Russia.
                        </p>
                        
                        <p>Professor Schreiber has also led the  Health Education short-term program with Valencia.
                           
                        </p>
                        
                        
                        
                        
                        
                        <h3>Honors</h3>
                        
                        <p><img alt="Professor Frame" height="227" src="ProfessorEdFrame.jpg" width="200">Honors Humanities Study Abroad Program
                        </p>
                        
                        <p><span><strong>Ed Frame</strong></span>, professor of Humanities, was in the Peace Corps and served in Sabah, Malaysia for
                           two years. He returned to Malaysia on a Fulbright-Hays doctoral research grant for
                           9 months and was hired by a University in Penang Malaysia as a professor of Asian
                           and Western Music. His wife is from Malaysia. They have travelled extensively during
                           their marriage and have visited 35 countries. In addition, their children travel extensively.
                           His oldest daughter is has taught in South Korea and his youngest daughter received
                           a Fulbright-Hays doctoral research grant in Tanzania. His wife speaks two dialects
                           of Chinese and he speaks Malay. His elder daughter is fluent in Mandarin Chinese and
                           his middle daughter is fluent in German. His youngest speaks Hindi and Swahili. 
                        </p>
                        
                        <p>He has led or been a part of multiple  honors overseas trips with Valencia. These
                           include Peru, Morocco and Spain,  Vienna, and Paris.  It has been an honor to him
                           to lead the honors  trips. The students are absolutely thrilled to be able to travel
                           overseas. He considers it a real blessing that Valencia is interested in exposing
                           students to the eye-opening experience of study abroad in both the SAGE and the honors
                           programs.
                        </p>
                        
                        
                        
                        <p><img alt="Professor Jane Maguire" height="267" src="Jane-Maguire.jpg" width="200">Honors Study Abroad Program
                        </p>
                        
                        <p><strong>Jane Maguire</strong>, Communications Professor teaching ENC0017,  ENC0027, and Honors IDH2911, has been
                           at Valencia since January 2004. Jane  completed her Bachelors and Masters degrees
                           at the University of  Missouri-Columbia. Jane is the Seneff Honors Coordinator for
                           the Undergraduate  Research Track on East Campus. She enjoys traveling to conferences
                           with the  Honors students, which led her to become interested in the study abroad
                           program. Jane has traveled as a program leader-in-training during the Spring  Break
                           IDH2955 Honors study abroad trip to the Netherlands. During the 2018 Spring  break,
                           she will travel to Spain with the IDH2955 Honors students visiting  Barcelona, Valencia
                           and Madrid. The students studying abroad will visit a  refugee center in Madrid for
                           service learning hours teaching basic English as  one of the highlights of the cultural
                           experiences the students will have during  this trip.
                        </p>
                        
                        
                        
                        
                        
                        
                        <h3>Humanities            </h3>
                        
                        <p><img alt="Professor Kevin Mulholland" height="252" src="ProfessorKevinMulholland.jpg" width="200">Humanities Study Abroad Program
                        </p>
                        
                        <p><span><strong>Kevin Mulholland</strong></span>, professor of Humanities, was born in England and has lived and taught in his home
                           country,  Canada and the USA. He has had a long career teaching, and has always appreciated
                           the fact that his job has given him the time and resources to study other cultures.
                           Kevin has traveled extensively in North Africa, Europe, Latin America, Canada and
                           the USA. He was the co-leader for a student service learning trip to the Dominican
                           republic, and is looking forward to taking a group of students to Ireland in May 2016.
                        </p>
                        
                        
                        
                        
                        
                        
                        <p><img alt="Professor Jeremy Bassetti" height="250" src="Jeremy-Bassetti.jpg" width="200">Humanities Study Abroad Program
                        </p>
                        
                        <p><strong>Dr. Jeremy Bassetti</strong>, professor of Humanities at Valencia College East Campus, completed his Ph. D. at
                           Florida State University in 2014. He performed a bulk of his primary doctoral research
                           at the Royal Academy of History in Spain, where he lived and studied from 2010 to
                           2011. He has been a study abroad program leader at Valencia College since 2016 and
                           is training to be a SAGE Program Leader Certificate facilitator.
                        </p>
                        
                        
                        
                        
                        
                        
                        <p><img alt="Professor Lisa Cole" height="205" src="Lisa-Cole.JPG" width="200">Humanities Study Abroad Program
                        </p>
                        
                        <p><strong>Lisa Cole</strong>, professor of Humanities and Art History at Valencia  College West Campus, has been
                           involved with study abroad since 2014. She has  served&nbsp;on the Study Abroad Committee
                           and as a Program Leader in Training  to Spain. She has traveled to Italy, France,
                           Mexico, Brazil, Canada,&nbsp;and  Spain.&nbsp;
                        </p>
                        
                        
                        
                        
                        
                        
                        <h3>Political Science </h3>
                        
                        <p><img alt="Professor Heather Ramsier" height="281" src="ProfessorHeatherRamsier.jpg" width="200">Political Science Study Abroad Program
                        </p>
                        
                        <p><span><strong>Heather Ramsier</strong></span>, professor of Student Life Skills and Political Science, has a firm background in
                           education.&nbsp; Her career prior to joining Valencia College focused on teaching social
                           science courses in the secondary classroom. In 2011, she began instructing face to
                           face and online political science and student success classes at the Osceola Campus
                           and teaching unique populations at the Adult Learning Center of Osceola and Liberty
                           High School.&nbsp; Her experience in classroom instruction was enhanced in 2013 when she
                           participated in a college preparatory program for students in Osceola County public
                           high schools and middle schools. Heather is a seasoned classroom instructor that is
                           also trained in academic advising.
                        </p>
                        
                        <p>Heather enjoys traveling and has visited Canada, Mexico, the Bahamas, Spain, France,
                           Italy, and Iceland. Her interest in learning about the cultures of others is rooted
                           in her lifelong passion for the social sciences.&nbsp; Heather studied at the University
                           of Central Florida, earning both her bachelor's degree and master's degree in Social
                           Science Education. Heather is the interim program leader of the study abroad trip
                           for  International Politics in France and Belgium, visiting the cities of Paris, Strasbourg,
                           and Brussels.
                        </p>
                        
                        
                        
                        <p><img alt="Dr. Creamer" height="251" src="DrScottCreamer.jpg" width="200">International Politics Study Abroad Program
                        </p>
                        
                        <p><strong>Dr. Scott Creamer</strong> is a professor of political science at Valencia College on the Osceola campus teaching
                           courses in U.S. government and international politics. He completed his Ph.D. and
                           Master of Arts degrees in political science at the University of Connecticut, and
                           earned his Bachelor of Arts degree in international studies at  Johns Hopkins University.
                           He has been a professor of political science for over 10 years, teaching at The University
                           of Connecticut, Southern Connecticut State University, and Naugatuck Valley Community
                           College. He has been at Valencia College since 2011.
                        </p>
                        Dr. Creamer has been a study abroad program leader for the International Politics
                        in France and Belgium program several times since 2012, visiting the cities of Paris,
                        Strasbourg, and Brussels.
                        
                        
                        
                        
                        
                        <h3>Science</h3>
                        
                        <p><img alt="Professor Myers" height="251" src="ProfessorSteveMyers.jpg" width="200">Field Biology Study Abroad Program
                        </p>
                        
                        <p><span><strong>Steve Myers</strong></span>, professor of biology on the East Campus, has found, after 28 years of teaching at
                           Valencia, that some of his most rewarding experiences have been traveling abroad with
                           students. He first began taking students on self-designed field courses 20 years ago.
                           Later, these field courses turned into full-scale study abroad programs and has now
                           led multiple trips  to India and Guyana to study and learn together. During his programs,
                           students learn basic science skills in field work methodology, including field observations,
                           note taking, data collection, and taxonomic identification of important species. Furthermore,
                           students have the opportunity to meet and work with some of the world's finest biologists,
                           conservationists and herpetologists.
                        </p>
                        
                        
                        
                        
                        
                        <p><img alt="Professor Marie Trone" height="198" src="Marie-Trone.jpg" width="200">Environmental Science Study Abroad Program
                        </p>
                        
                        <p>Marie Trone is a Professor of Biology.&nbsp; Marie participated as a student in a tropical
                           marine biology course that took her class to Belize during her sophomore year  in
                           college, and her life has never been the same since!&nbsp; Marie spent 13 years working
                           as a dolphin  trainer, three of which were spent living in the Mayan Riviera in Mexico.&nbsp;
                           As a result of this employment she drove her  Ford Escort to the Yucatan Peninsula
                           from the United States twice.&nbsp; In addition, she has established the Amazon  Dolphin
                           Acoustics Lab in the Peruvian Amazon and consequently has spent the  past 4 summers
                           investigating river dolphin acoustics and behavior.&nbsp; Her work in the Amazon has led
                           to  collaborations with the University of Toulon in France and the Instituto de  Investigaciones
                           de la Amazonía Peruana (IIAP).&nbsp;  Marie conducted her Master's thesis work investigating
                           fish biodiversity  in Tobago in conjunction with the Smithsonian Institute.&nbsp; She has
                           also traveled to other international  destinations in Chile, Costa Rica, Honduras,
                           the Caribbean and Japan for  science conferences or visiting friends.            
                        </p>
                        
                        
                        <p><img alt="Professor Brantley" height="251" src="ProfessorBetsyBrantley.jpg" width="200">Environmental Conservation Study Abroad Program
                        </p>
                        
                        <p><span><strong>Betsy Brantley</strong></span>, professor of biology, has traveled across Europe and to several countries in West
                           Africa (where her favorite “fun-sounding-name” location was Ouagadougou). She was
                           privileged to participate in Valencia's first Study Abroad Program-Leader-in-Training
                           course in summer 2013. As part of that program, she spent two weeks in Ecuador studying
                           transcultural nursing, a course which included visits to hospitals and health clinics
                           in both the capital city and in rural rain forest communities. 
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        <h3>Service Learning</h3>
                        
                        <p><img alt="Professor Christy Cheney" height="288" src="ProfessorChristyCheney.jpg" width="200">Service Learning Study Abroad Program
                        </p>
                        
                        <p><span><strong>Christy Cheney</strong></span>, professor of Student Life Skills, began teaching at Valencia in 1997 and, in her
                           first term, she served in a <a href="../../../../linc/index.html"><u>Learning Community course (LinC)</u></a>. With the help of various grants, including <a href="http://www.achievingthedream.org/"><u>Achieving the Dream</u></a>&nbsp;and the Developmental Education Initiative, her involvement with LinC became a part
                           of her workload every term.&nbsp; Since 2010, Christy has been a part of the&nbsp;<a href="../../../../students/reach/index.html"><u>REACH (Reaching Every Academic Challenge Head on) program</u></a>, a relatively new learning community on Osceola Campus, which consists of a small
                           group of students who take their first year of college courses together with the same
                           instructors.&nbsp; Working with students in a learning community setting gives students
                           and faculty the opportunity to work more closely together through the development
                           of cross-disciplinary lessons and activities.&nbsp; The integration between student's courses
                           helps them to directly apply key concepts and outcomes, and build connections between
                           students, faculty, and counselors.
                        </p>
                        
                        <p>Her passion for travel, meeting new people and learning more about the powerful impact
                           the immersion of cultural experiences can have on all people motivated her to complete
                           the Study Abroad Leader Certification, and become involved with Valencia's Study Abroad
                           Program.&nbsp; In March 2015, Christy was a Co-Leader in the Study Abroad to Italy and
                           Greece, and will lead an International Service Learning course to Italy in May 2016.&nbsp;
                           Christy recently was awarded an Endowed Chair to provide study abroad scholarship
                           opportunities for REACH students!&nbsp;&nbsp;She continues to develop and create opportunities
                           for students to gain exposure and awareness of the world.&nbsp; With the partnership of
                           University of Central Florida (UCF), Christy plans to co-facilitate Global Distinction
                           co-curricular programs for students to enhance their knowledge of global citizenship.
                        </p>
                        
                        <p>Christy has a bachelor's degree in Organizational Communication and a master's degree
                           in Higher Education, both from the University of Central Florida.
                        </p>
                        
                        
                        <p><img alt="Professor Liz Earle" height="247" src="photo-Liz-Earle.jpg" width="200">Service Learning Study Abroad Program
                        </p>
                        
                        <p>Liz Earle is a Communications Professor on the Osceola Campus and has been with Valencia
                           College since 2000. Always interested in the study of people and cultures, as an undergraduate
                           Liz participated in an Anthropology Study Abroad program in San Salvador, an out island
                           in the Bahamas. She has traveled extensively throughout Europe, and in South Africa
                           and Peru, and lived in England for three years. In 2017, Liz traveled to Italy with
                           the Greek/Roman Humanities Study Abroad in Italy trip as a Program-Leader-in-Training.
                           She will co-lead Study Abroad to Italy/Sicily in the summer of 2018, and is excited
                           to incorporate a home stay and service learning experience in Sicily.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/leadstudyabroad/programleaders.pcf">©</a>
      </div>
   </body>
</html>