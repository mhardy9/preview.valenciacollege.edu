<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/leadstudyabroad/leadstudyabroadprogram.cfm/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/leadstudyabroad/leadstudyabroadprogram.cfm/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li><a href="/students/study-abroad/faculty-staff/leadstudyabroad/">Leadstudyabroad</a></li>
               <li>Leadstudyabroadprogram.cfm</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../../index.html"></a>
                        
                        
                        
                        <h2>Submit a Proposal to Lead a Study Abroad Program in 2016</h2>
                        
                        
                        
                        <p>In order to receive important information about study abroad, international education
                           conferences, faculty exchanges, internationalizing the curriculum, and international
                           travel from SAGE, please subscribe to the <a href="http://feedburner.google.com/fb/a/mailverify?uri=TheGroveValenciaCollegeGlobalExperiences&amp;loc=en_US" target="_blank">SAGE blog</a>. Email blasts will no longer be sent out college-wide. 
                        </p>
                        
                        <p>The first step for anyone interested in leading a study abroad program, participating
                           as a program leader-in-training on a study abroad program, participating in a faculty
                           exchange program, or attending an international conference is to complete the following
                           form. This form is due to the SAGE office by <strong>OCTOBER 1</strong> of each year for travel during the academic year (fall, spring, summer). This form
                           gives you approval to go to the next step in the process which is to complete the
                           proposal form. NOTE: You are NOT required to complete this form for sabbaticals or
                           Endowed Chair travel. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="../../documents/IntlTravel_RequestforInternationalTravelForm.pdf"><strong>REQUEST FOR INTERNATIONAL TRAVEL FORM</strong></a><strong> </strong>
                              
                           </li>
                           
                        </ul>          
                        
                        <p>STUDY ABROAD PROGRAM LEADER CERTIFICATE </p>
                        
                        <p>All faculty who are planning to submit a proposal for study abroad must enroll in
                           the <strong><a href="http://valenciacollege.edu/faculty/development/programs/curriculumDevelopment/studyabroadplcert.cfm">Study Abroad Program Leader Certificate</a></strong>, which includes <strong>The Roles and Responsibilities of the Study Abroad Program Leader </strong>and <strong>Designing a Study Abroad Experience for Students. </strong>This program is designed to culminate in the certification of full-time faculty as
                           a study abroad program leader. The designation will indicate completion of 24 hours
                           of professional development in the area of study abroad and global experiences and
                           will prepare faculty members to develop and lead a meaningful short-term study abroad
                           experience for Valencia’s students. To successfully complete the <strong>Study Abroad Program Leader Certification</strong>, the faculty member must be full-time and complete both courses in any order. To
                           view the course descriptions and upcoming schedules, <a href="../../../students/events/index.html">click here</a>. You will see two options for the Roles &amp; Responsibilities workshop, but there is
                           only one course offering for the Designing workshop each fall. 
                        </p>
                        
                        <p>Workshops are presented in partnership with <a href="../../../../../faculty/development/index.html"><strong>Faculty Development</strong></a><strong>. </strong>Visit the following link for instructions on how to register in Atlas: <a href="../../../../../faculty/development/howToRegisterForCourses.html">Faculty Development Registration Instructions</a>. For more information or to request other workshop topics, email us at <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>. 
                        </p>
                        
                        
                        <p>Valencia has a number of different opportunities for staff and faculty to get involved
                           in international education. Click on a tab below for details.
                        </p>
                        
                        <div>
                           
                           <ul>
                              
                              <li><a href="#timeline" title="Timeline">Timeline</a></li>
                              
                              <li><a href="#form" title="International Travel Request Form">International Travel Request Form</a></li>
                              
                              <li><a href="#certificate" title="Study Abroad Program Leader Certificate">Study Abroad Program Leader Certificate</a></li>
                              
                              <li><a href="#proposal" title="Proposal Process">Proposal Process</a></li>
                              
                              
                           </ul>
                           
                           
                           <div>  
                              
                              
                              <div>
                                 
                                 
                                 <h3><a href="#timeline">Timeline</a></h3>
                                 
                                 <p>Thank you for your interest in submitting a proposal to lead or co-lead a short-term
                                    study abroad program at Valencia! Proposals are submitted the year prior to running
                                    the program. This year's <strong>call for proposals is January 2 - February 28, 2013</strong> to lead a study abroad program in the spring or summer term of <strong>2015</strong>. You must be a full-time faculty member in order to apply to lead or co-lead a study
                                    abroad program.
                                 </p>
                                 
                                 <p><strong>STEP 1: </strong>Complete the <strong><a href="../../documents/IntlTravel_RequestforInternationalTravelForm.pdf">Request for International Travel </a></strong><a href="../../documents/IntlTravel_RequestforInternationalTravelForm.pdf"><strong>Form,</strong></a> get your dean and campus president signatures, and submit to the SAGE office by the
                                    date listed above. 
                                 </p>
                                 
                                 <p>                          <strong>STEP 2: </strong>Attend the <a href="http://valenciacollege.edu/faculty/development/programs/curriculumDevelopment/studyabroadplcert.cfm"><strong>Study Abroad Program Leader Certificate</strong></a> classes in the <strong>FALL TERM</strong>. Please note that this is a mandatory requirement in order to lead or co-lead a study
                                    abroad program. See the <a href="../../events.html">Faculty and Staff Events</a> page for more details. 
                                 </p>
                                 
                                 <p><span><strong>STEP 3:</strong></span> The primary program leader must create a <a href="https://dynamicforms.ngwebsolutions.com/Login.aspx" target="_blank"><strong>Dynamic Forms</strong></a> account if you do not already have one. If you have a co-program leader, you will
                                    only submit one application and use one Dynamic Forms account. Please see the Co-Program
                                    Leader Policy listed at the bottom of this page. 
                                 </p>
                                 
                                 <p><strong>STEP 4: </strong>Complete the online<a href="https://dynamicforms.ngwebsolutions.com/ShowForm.aspx?RequestedDynamicFormTemplate=2a507ce8-117f-454f-a992-02a44c6b8fba" target="_blank"><strong> Short-Term Study Abroad Proposal Application</strong></a> by the dates listed below in step 5. <strong>YOU MUST USE THIS LINK TO ACCESS THE PROPOSAL FORM. </strong>This form will be made active on <strong>October 1, 2013</strong>.  In order to submit a completed application, you will need upload the following
                                    documents: course outline, program provider proposal (the one you are using), <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/SAGEProgramItineraryBlankTemplate.docx">program itinerary</a>, and a copy of your Faculty Development transcript or certificate showing that you
                                    completed the required coursework.
                                 </p>
                                 
                                 <p><strong>IMPORTANT!</strong> You can start a proposal and save it, but you must be sure to log into Dynamic Forms
                                    first (step 3 link) and then go to the link in step 4. You will know that you are
                                    logged in if you see the "Save Progress" button at the bottom of the screen. 
                                 </p>
                                 
                                 <p><strong>STEP 5: </strong>Print out the application packet and all attachments,<strong> SINGLE-SIDED</strong>, <strong>NO STAPLES</strong> and obtain signatures and submit as outlined below. Please note that this is a competitive
                                    process and not all proposals will be approved. Incomplete or late applications will
                                    not be accepted. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Get the completed application packet to your dean/s for signature by <strong>Monday, February 6, 2015</strong>. 
                                    </li>
                                    
                                    <li>Get the completed application with the dean's signature to the campus president for
                                       signature by <strong>Friday, February 13, 2015</strong>.
                                    </li>
                                    
                                    <li>Send the final packet to the SAGE Office (DTC-2) by <strong>Friday, February 27, 2015</strong>.  
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>STEP 6: </strong>The proposal will be reviewed by the <a href="../../committees.html"><strong>Study Abroad Committee</strong></a> and recommendations will be given to the<a href="../../committees.html"><strong> International Education Connections Team</strong></a> for final approval using the following <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/SAGEProgramProposalEvaluationandRankingInstructions2015_011414FINAL.docx"><strong>proposal guidelines</strong></a> and <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/SAGEProgramProposalEvaluationRubric2015_010714FINAL.docx"><strong>rubric</strong></a>. Notification for approval will be given by early April 2014. At that time, you can
                                    begin marketing your program. The following is the list of possible outcomes:   (1)
                                    Recommended and funded; (2) Recommended and waitlisted for funding; (3) Recommended
                                    and not funded; (4) Not recommended or funded.
                                 </p>
                                 
                                 <p><strong>STEP 7:</strong> Once approved, you must create a program booklet or update your existing booklet
                                    if this is a repeat program. Contact the SAGE Office for the template to get started.
                                    The booklet is due by <strong>JULY 1</strong>.
                                 </p>
                                 
                                 <p><span><strong>STEP 8:</strong></span> If you have been funded, you will be informed of the scholarships awarded to your
                                    program after <strong>July 1</strong> and after your program booklet is received in the SAGE Office. 
                                 </p>
                                 
                                 <p><strong>Please note the following:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Incomplete and late applications will not be accepted.</li>
                                    
                                    <li>You must have submitted a <em>Request for International Travel Form</em> by <strong>October 1</strong> in order to qualify to apply. 
                                    </li>
                                    
                                    <li>You can only apply to lead one study abroad program per year. </li>
                                    
                                    <li>To ensure a program that meets Valencia's standards for academic excellence and does
                                       not put additional financial burdens on students, program leaders are only allowed
                                       to visit a maximum of two different countries. 
                                    </li>
                                    
                                    <li>You must read the <strong>Safety &amp; Emergency-Crisis Management Plan for Study Abroad</strong> prior to submitting your proposal. If you have not received a copy of this document,
                                       contact the SAGE Office. 
                                    </li>
                                    
                                    <li>Apply for a Valencia Foundation <a href="http://www.valencia.org/forms.cfm" target="_blank"><strong>Endowed Chair</strong></a> in <strong>APRIL</strong> to help fund your program.
                                    </li>
                                    
                                    <li>Meals that are not included in the program fee for the program leader/s will not be
                                       covered for study abroad. 
                                    </li>
                                    
                                    <li>This course must be paid as an overload.</li>
                                    
                                 </ul>
                                 
                                 <p><strong>Co-Program Leader Policy: </strong>If you have a co-program leader, you both must recruit at least 16 students. If the
                                    program has less than 16 students and there is a financial impact on students (the
                                    price has to increase), then the co-program leader will not be able to participate
                                    unless additional funding is identified. If you cannot achieve the minimum ratio after
                                    all efforts have been exhausted, and there is no financial impact to the students
                                    (i.e., free ticket for 12 students from EF Tours), then the co-program leader can
                                    participate.<br>
                                    
                                 </p>
                                 
                                 <p><strong>Family Policy:</strong> If you wish to take a spouse/significant other on study abroad, you can do so outside
                                    of the Valencia process.&nbsp; He/She does not have to enroll in the course.&nbsp; You will
                                    have to make the arrangements on your own and pay all associated fees. &nbsp;Please note
                                    that the participation of your spouse/significant other cannot impact the cost of
                                    the trip in any way. &nbsp;For example, if you are at the maximum allowed for a bus, and
                                    adding another person would incur the cost of a second bus. If you wish to take your
                                    child, he/she must be at least 18 years old and enroll in the course as a student.&nbsp;
                                    Minors are not permitted.
                                 </p>
                                 
                                 <p><strong>Application and Payment Deadlines: </strong>All programs  have standardized deadlines. Please plan on marketing heavily up until
                                    the application deadline date. <br>
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <div data-old-tag="table">
                                          
                                          <div data-old-tag="tbody">
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td"><strong>2015 DEADLINES: </strong></div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>Spring Break</strong></div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>Summer A</strong></div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div><strong>Summer B</strong></div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Student Application Deadline</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>09/26/14</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>11/07/14</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>01/16/15</div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Deposit Deadline</strong><strong>
                                                         </strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>10/24/14</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>01/30/15</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>02/13/15</div>
                                                </div>
                                                
                                             </div>
                                             
                                             <div data-old-tag="tr">
                                                
                                                <div data-old-tag="td">
                                                   
                                                   <p><strong>Student Program Balances Due</strong></p>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div> 12/12/14</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>                                    03/13/15</div>
                                                </div>
                                                
                                                <div data-old-tag="td">
                                                   <div>                                    04/03/15</div>
                                                </div>  
                                                
                                             </div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong><br>
                                       PROGRAM DEVELOPMENT RESOURCES: </strong></p>
                                 
                                 <p><a href="http://valenciastudyabroad.pbworks.com/w/page/31268916/Program-Leaders-Toolkit" target="_blank">Study Abroad Program Leader's Toolkit</a></p>
                                 
                                 <p><a href="http://valenciasagelibrary.pbworks.com/w/page/53349589/FrontPage" target="_blank">SAGE eLibrary</a> 
                                 </p>
                                 
                                 <p><a href="#providers">Program Providers</a> 
                                 </p>
                                 
                                 <p><a href="http://valenciacollege.edu/international/studyabroad/resources/associationsandorganizations.cfm">Associations and Organizations</a></p>
                                 
                                 <p><a href="http://valenciacollege.edu/international/studyabroad/resources/experts.cfm">International Resource Experts </a></p>
                                 
                                 <p><a href="../../../students/travelsafety.html">Travel and Safety</a> 
                                 </p>
                                 
                                 <p><a href="../../../resources/languageambassadors.html">Valencia Language Ambassadors </a></p>
                                 
                                 
                                 
                              </div> 
                              
                              
                              <div>
                                 
                                 
                                 <h3><a href="#form">International Travel Request Form</a></h3>
                                 <a href="#form">
                                    </a>
                                 
                              </div> 
                              
                              
                              <div>
                                 
                                 
                                 <h3>Study Abroad Program Leader Certificate</h3>
                                 
                                 
                              </div> 
                              
                              
                              <div>
                                 
                                 
                                 <h3>Program Proposal</h3>
                                 
                              </div> 
                              
                              
                           </div>  
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/leadstudyabroad/leadstudyabroadprogram.cfm/index.pcf">©</a>
      </div>
   </body>
</html>