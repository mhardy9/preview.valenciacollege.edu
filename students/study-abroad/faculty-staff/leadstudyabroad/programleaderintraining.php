<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/leadstudyabroad/programleaderintraining.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/leadstudyabroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li><a href="/students/study-abroad/faculty-staff/leadstudyabroad/">Leadstudyabroad</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        
                        <h2>Program Leader-in-Training (PLIT) Mentorship</h2>
                        
                        
                        Check out Suzette Dohany's advice on what it's like to be a mentee on study abroad:
                        <a href="http://www.youtube.com/watch?feature=player_embedded&amp;v=vj77bXnLLps" target="_blank">CLICK HERE</a>
                        
                        <p>This program is designed for full-time faculty who have never led a study abroad program
                           before but who would like to gain experience in order to apply to lead a short-term
                           program with Valencia students in the future. The following outlines the steps to
                           apply and the requirements of program participation. Participants will select a short-term
                           study abroad program to go on the first year as a mentee to learn what is involved
                           in leading a study abroad program the following year. Watch this<a href="http://youtu.be/sZdNigNiI-s" target="_blank"> video on one professor's perspective</a> on what it is like to mentor. 
                           
                        </p>
                        
                        <p><u><strong>APPLICATION PROCESS: </strong></u></p>
                        
                        <ul>
                           
                           <li>Identify a <a href="../../students/shorttermprograms.html" target="_blank">short-term program</a> in which  you want to participate.<br>
                              
                           </li>
                           
                           <li>Ask the faculty program leader if they are willing to be your mentor. Here is a list
                              of the<a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/leadstudyabroad/documents/PLITProgramExpectations.docx" target="_blank"> <strong>PLIT Expectations</strong></a>. Be sure that you both understand the expectations.<br>
                              
                           </li>
                           
                           <li>Once you receive your mentor's approval, complete the<a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/leadstudyabroad/documents/Study-Abroad-Program-Leader-in-Training-Proposal-Form-2018.pdf" target="_blank"> <strong>Program Leader-in-Training Proposal Form</strong>.</a><br>
                              
                           </li>
                           
                           <li>Get your proposal signed by all parties identified on the form.<br>
                              
                           </li>
                           
                           <li>Submit a scanned copy of the the signed proposal the Campus Dean of Academic Affairs
                              AND the SAGE office (DTC-2) by email by <strong>September 1</strong>. 
                              
                              
                              Late applications may be accepted, but funds may not be available after that date.
                              
                              You do NOT have to complete the <em>Request for International Travel Form</em>.  <br>
                              
                           </li>
                           
                           <li>Receive approval from your campus contact.</li>
                           
                        </ul>
                        
                        <p><span><strong><u>SELECTION PROCESS</u></strong></span><strong>: </strong>Each campus will determine their own selection criteria and process. The SAGE office
                           should be informed as soon as a candidate is selected. Applicants for this program
                           may be competing with other applicants and faculty exchange program applicants. 
                        </p>
                        
                        <p><span><strong><u>FUNDING</u></strong></span><strong>:</strong> Each main campus  has been allotted $4000 from the SAGE budget. You may be fully
                           or particially funded for this program depending upon what your campus committee decides.
                           The SAGE office will pay  up to a maximum of $3000. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Costs that are covered by SAGE: </strong>airline tickets, luggage fees (one bag), hotel accommodations (up to 6 nights max),
                              airport/hotel transfers, in-country ground transportation, program registration fees,
                              entrance fees to cultural activities, and medical/trip insurance<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong> Costs that are NOT covered by SAGE: 
                                 
                                 
                                 </strong>meals per diem, airport parking, airline upgrades, entrance fees to cultural or social
                              activities that are not directly related to the internationalized curriculum project,
                              and expenses for incoming participants from the overseas institution 
                           </li>
                           
                        </ul>
                        
                        <p><span><strong><u>PARTICIPATION REQUIREMENTS</u></strong></span><strong>: </strong>Program participants must: (1) commit to attend the next fall workshops (Study Abroad
                           Program Leader Certificate); (2) submit a final report upon his/her return to the
                           discipline dean and the SAGE office highlighting what you learned as a result of this
                           experience and what your future plans are related to study abroad; and (3) submit
                           a proposal to lead a program within two years.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/leadstudyabroad/programleaderintraining.pcf">©</a>
      </div>
   </body>
</html>