<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/committees.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>International Education Committees 2015-2016</h2>
                        
                        In 2008, the  SAGE Advisory Committee was formed to provide input and guidance all
                        aspects of study abroad. In 2010 the composition and scope of that group changed with
                        the implementation of the <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/InternationalEducationStrategicPlan_2011-2016_FINAL.pdf">Strategic Plan for International Education</a>. The  International Education Steering Committee was formed to get the work of the
                        strategic plan underway. In September 2012, the College determined that the subcommittees
                        would report directly to the <a href="../../../governance/llc/index.html">College Learning Council</a>. 
                        
                        <p>The goals of the <em>Strategic Plan for International Education </em> are as follows:
                        </p>
                        
                        <ul>
                           
                           <li>GOAL 1:&nbsp; Review and Enhance (Internationalize) the Curricula</li>
                           
                           <li>GOAL 2:&nbsp; Increase Short-Term Study Abroad Experiences</li>
                           
                           <li>GOAL 3:&nbsp; Increase Semester Study Abroad Experiences</li>
                           
                           <li>GOAL 4:&nbsp; Increase Student, Scholar, and Faculty Exchange Opportunities</li>
                           
                           <li>GOAL 5:&nbsp; Increase International Student Enrollment </li>
                           
                           <li>GOAL 6:&nbsp; Integrate International Students into the College and Local Community</li>
                           
                        </ul>            
                        
                        <p>As part of our work in international education, we surveyed Valencia faculty, staff,
                           and students. Here is a copy of our working draft of the <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/GlobalCitizensFlyer-01.pdf">Competencies of a Global Citizen</a>. You can read more about <a href="http://thegrove.valenciacollege.edu/internationalizing-at-valencia/" target="_top">Internationalization at Valencia</a> in The Grove! 
                           
                           
                        </p>
                        
                        <div>
                           
                           
                           <h3>Internationalizing  the Curriculum Committee</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <div>NAME</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>TITLE</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>CAMPUS</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Aby Boumarate </div>
                                          
                                          <div data-old-tag="td">Professor, English </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Adrienne Trier-Bieniek </div>
                                          
                                          <div data-old-tag="td">Professor, Sociology </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Aryan Ashkani </div>
                                          
                                          <div data-old-tag="td">Professor, Mathematics </div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Beth King </div>
                                          
                                          <div data-old-tag="td">Librarian</div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Cheryl Bollinger </div>
                                          
                                          <div data-old-tag="td">Professor, Manufacturing </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Heather Bryson </div>
                                          
                                          <div data-old-tag="td">Professor, History </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"> Dr. Karen Borglum (Co-Chair) </div>
                                          
                                          <div data-old-tag="td"> AVP, Curriculum and Articulation </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Marsha Butler </div>
                                          
                                          <div data-old-tag="td">Faculty, New Student Experience </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Maryke Lee </div>
                                          
                                          <div data-old-tag="td">Dean, Mathematics </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Nely Hristova </div>
                                          
                                          <div data-old-tag="td">Professor, Mathematics </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Nichole Jackson </div>
                                          
                                          <div data-old-tag="td">Assit Director, Learning Assessment</div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Nicholas Bekas </div>
                                          
                                          <div data-old-tag="td">Campus Dean, Academic Affairs </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Robyn Brighton </div>
                                          
                                          <div data-old-tag="td"> Director, Curriculum Initiatives</div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Tammy Gitto </div>
                                          
                                          <div data-old-tag="td">Professor, Humanities </div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Yasmeen Qadri </div>
                                          
                                          <div data-old-tag="td">Professor, Education </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div> 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Study Abroad Committee</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <div>NAME</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>TITLE</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>CAMPUS</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Betsy Brantley </div>
                                          
                                          <div data-old-tag="td">Professor, Biology </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Eric B. Wallman </div>
                                          
                                          <div data-old-tag="td">Professor, Humanities </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Jennifer Tomlinson </div>
                                          
                                          <div data-old-tag="td">Faculty, New Student Experience </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Jenni Campbell </div>
                                          
                                          <div data-old-tag="td">Dean, Communication/Language</div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Jerry Hensel </div>
                                          
                                          <div data-old-tag="td">Professor, Information Technology </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Jocelyn Morales </div>
                                          
                                          <div data-old-tag="td">Counselor</div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Kara Parsons </div>
                                          
                                          <div data-old-tag="td">Manager, SAGE </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Lisa Cole </div>
                                          
                                          <div data-old-tag="td">Professor, Humanities </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"> Michelle Foster (Co-Chair) </div>
                                          
                                          <div data-old-tag="td"> Campus Dean,  Academic Affairs</div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Michael Robbins </div>
                                          
                                          <div data-old-tag="td">Professor, English </div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Natali Shulterbrondt </div>
                                          
                                          <div data-old-tag="td">Couselor</div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Nicholas Bekas </div>
                                          
                                          <div data-old-tag="td">Campus Dean,  Academic Affairs </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"> Robyn Brighton (Co-Chair) </div>
                                          
                                          <div data-old-tag="td"> Director, Curriculum Initiatives</div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Steve Mammino </div>
                                          
                                          <div data-old-tag="td">Manager, Risk Management </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>    
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Safety in Study Abroad Committee</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <div>NAME</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>TITLE</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>CAMPUS</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"> Robyn Brighton</div>
                                          
                                          <div data-old-tag="td"> Director, Curriculum Initiatives, Service Learning </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Leslie Golden </div>
                                          
                                          <div data-old-tag="td">Assistant General Council, Legal Services </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Paul Rooney </div>
                                          
                                          <div data-old-tag="td">AVP of Security, Safety, and Risk Management</div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>International Education Connections Team</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <div>NAME</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>TITLE</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>CAMPUS</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Falecia Williams </div>
                                          
                                          <div data-old-tag="td">Campus President</div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Jenni Campbell</div>
                                          
                                          <div data-old-tag="td">Dean, Communications and Humanities </div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Robyn Brighton </div>
                                          
                                          <div data-old-tag="td">Director, Curriculum Initiatives, Service Learning </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Joyce Romano </div>
                                          
                                          <div data-old-tag="td"> Vice President, Student Affairs </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Kathleen Plinske </div>
                                          
                                          <div data-old-tag="td">Campus President </div>
                                          
                                          <div data-old-tag="td">
                                             <div>Osceola</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Karen Marie Borglum</div>
                                          
                                          <div data-old-tag="td"> Assistant VP, Curriculum Dev &amp; Art </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Lisa Eli </div>
                                          
                                          <div data-old-tag="td"> Mgng Director, Continuing Education International </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"> Michelle Foster </div>
                                          
                                          <div data-old-tag="td"> Campus Dean, Academic Affairs</div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Nick Bekas </div>
                                          
                                          <div data-old-tag="td">Campus Dean, Academic Affairs </div>
                                          
                                          <div data-old-tag="td">
                                             <div>West</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Stacey Johnson </div>
                                          
                                          <div data-old-tag="td">Campus President </div>
                                          
                                          <div data-old-tag="td">
                                             <div>East</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">Dr. Susan Ledlow </div>
                                          
                                          <div data-old-tag="td">Vice President, Academic Affairs and Planning </div>
                                          
                                          <div data-old-tag="td">
                                             <div>DO</div>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>                        
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Meeting Dates, Times, and Topics</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <div>DATE</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>TIME</div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div>LOCATION</div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">October 28, 2016 </div>
                                          
                                          <div data-old-tag="td">2:00 - 4:00 p.m. </div>
                                          
                                          <div data-old-tag="td">West, HSB 107 </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">*December 2, 2016 </div>
                                          
                                          <div data-old-tag="td">1:00 - 3:00 p.m.</div>
                                          
                                          <div data-old-tag="td">Lake Nona, TBD </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">January 20, 2017 </div>
                                          
                                          <div data-old-tag="td">9:00 - 11:00 a.m. </div>
                                          
                                          <div data-old-tag="td">East, 3-113 </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">February 17, 2017 </div>
                                          
                                          <div data-old-tag="td">2:00 - 4:00 p.m. </div>
                                          
                                          <div data-old-tag="td">West, TBD </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">March 24, 2017 </div>
                                          
                                          <div data-old-tag="td">9:00 - 11:00 a.m. </div>
                                          
                                          <div data-old-tag="td">Lake Nona, 148 </div>
                                          
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                       
                                    </div>
                                    
                                 </div> 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>International Education  Annual Reports</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/InternationalEducationStrategicPlan_AnnualReport2011-2012_FINAL.pdf">2011-2012</a> 
                                 </p>
                                 
                                 <p><a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/InternationalEducationAnnualReportandAddendums2012-2013.pdf" target="_blank">2012-2013</a></p>
                                 
                                 <p><a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/InternationalEducationAnnualReport2013-2014.pdf">2013-2014</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/committees.pcf">©</a>
      </div>
   </body>
</html>