<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/curriculum/exchanges.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/curriculum/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li><a href="/students/study-abroad/faculty-staff/curriculum/">Curriculum</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        
                        <h2>Faculty and Staff Exchange Opportunities </h2>
                        
                        <div>
                           
                           <ul>
                              
                              <li>
                                 <a href="#Opportunities">Exchange Opportunities</a> 
                              </li>
                              
                           </ul>
                           
                           <p>The SAGE office has funding for faculty to travel overseas to gain a greater understanding
                              and appreciation for another country and culture in order to   internationalize their
                              curriculum. The overseas experience may involve any of the following activities:
                           </p>
                           
                           <ul>
                              
                              <li>job shadowing and interviews </li>
                              
                              <li>guest speaking / teaching</li>
                              
                              <li>research</li>
                              
                              <li>classroom observations </li>
                              
                              <li>meetings on best practices</li>
                              
                              <li>curriculum review </li>
                              
                              <li>conference attendance </li>
                              
                              <li>social and cultural tours</li>
                              
                           </ul>
                           
                           <p><strong>APPLICATION DEADLINEs</strong>: <strong>OCTOBER 1 AND MARCH 1 <br>
                                 </strong><em>Send proposal form to <strong>studyabroad@valenciacollege.edu</strong>. </em></p>
                           
                           <p><strong>FUNDING AMOUNT:</strong> From $3500 t0 $4000 depending upon the costs of your program
                           </p>
                           
                           <p><strong>PROGRAM APPROVALS: </strong>Proposals will be submitted to the INZ Committee for final approval. Not all projects
                              may be approved. Funding is limited. 
                           </p>
                           
                           <p><strong>PARTICIPATION REQUIREMENT: </strong>You must enroll in the Internationalizing the Curriculum workshop and develop a course
                              internationalization toolkit. You can enroll in the course either prior to or after
                              travel but it must be in the same academic year. 
                           </p>
                           
                           <p><strong>RECIPROCAL EXCHANGES: </strong>You must complete the<a href="documents/JEVProgram_HostingDepartmentSign-OffForm_2012-05-15.pdf"><strong> Hosting Department Sign-Off Form</strong></a> in advance of applying.
                           </p>
                           
                           <p><strong>BEFORE YOU APPLY: </strong> Read these detailed instructions - <strong><a href="documents/Faculty-StaffInternationalExchangeProgramApplicationInstructions_2016.docx">Faculty/Staff International Exchange Program Instructions</a></strong></p>
                           
                           <p><strong>FORMS YOU WILL NEED: </strong></p>
                           
                           <p><strong><a href="documents/Faculty-StaffInternationalExchangeProgramProposalForm_2016.docx">Faculty/Staff International Exchange Program Proposal Form</a></strong></p>
                           
                           <p><a href="documents/JEVProgram_HostingDepartmentSign-OffForm_2012-05-15.pdf"><strong>Hosting Department Sign-Off Form</strong></a></p>
                           
                           <p><strong><a href="documents/BuildingaCulturalComponentIntotheEVP.docx">Building Cross-Cultural Components into the Exchange Visitor Program</a> </strong></p>
                           
                           <h3>In order to receive important information about study abroad, international education
                              conferences, faculty exchanges, internationalizing the curriculum, and international
                              travel from SAGE, please subscribe to the <a href="http://feedburner.google.com/fb/a/mailverify?uri=TheGroveValenciaCollegeGlobalExperiences&amp;loc=en_US" target="_blank">SAGE blog</a>. Email blasts will no longer be sent out college-wide. 
                           </h3>
                           
                           <h3>Faculty/Staff Exchange Opportunities<a name="Opportunities" id="Opportunities"></a> 
                           </h3>
                           
                        </div>
                        
                        <p>The list below is only for programs options for exchanges. Faculty and staff can also
                           identify other international partners to create a program. We have other institutional
                           partnerships in England, Ecuador, and the Dominican Republic to create MOUs for exchanges
                           if you are interested.
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>PROGRAM </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>DESCRIPTION</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>CONTACT INFORMATION </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Koning Willem I Faculty/Staff Exchange Program</strong></p>
                                    
                                    <p><strong>s-Hertogenbosch, NETHERLANDS </strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>RADIX (ROC Association for Dutch International ConneXions) has the pleasure in offering
                                       a two-week professional and cultural exchange with Valencia. A <strong>faculty member or administrator</strong> from the Netherlands will be matched with an American counterpart with similar job
                                       responsibilities. However, other match factors such as age, gender, personal interests
                                       and family situation will also be taken into account, if necessary. As a rule, the
                                       two weeks’ exchange, will consist of <strong>nine days of professional exchange and three days of leisure time</strong> (weekend) plus an orientation day upon arrival. The common language is <strong>English</strong>, which will add an extra dimension to the personal and professional aspects of the
                                       exchange.&nbsp; The deadline to apply for this program is <span><strong>March 2 </strong></span> each year. 
                                    </p>
                                    
                                    <p>Valencia faculty and staff can apply for funding for spring break or summer programs
                                       at Koning Willem as well. These programs are not reciprocal, do not have a 2-week
                                       minimum, and do not involve homestay. The deadline to apply for SAGE funding is <strong>October 1</strong>.<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>              <a href="mailto:jrobertson@valenciacollege.edu">studyabroad@valenciacollege.edu</a><br>
                                       407-582-3188
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Zealand Institute of Business and Technology</strong></p>
                                    
                                    <p><strong>Koge, Denmark </strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Valencia faculty and staff can apply for funding for this exchange opportunity during
                                    spring break or summer. Please note that no Valencia faculty or staff have participated
                                    in an exchange with this institution to date. The deadline to apply for SAGE funding
                                    is <strong>October 1</strong>.
                                 </div>
                                 
                                 <div data-old-tag="td">            <a href="mailto:jrobertson@valenciacollege.edu">studyabroad@valenciacollege.edu</a><br>
                                    407-582-3188
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>IAU College Faculty Fellows Program</strong></p>
                                    
                                    <p><strong> Aix-en-Provence, FRANCE </strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">IAU College in Aix-en-Provence, France is pleased to announce its Faculty Fellows
                                    Program, designed for visiting professors on sabbatical leave from their home institutions
                                    for an academic year. Collaborative teaching opportunities between home institution
                                    and IAU College are also possible. The Fellow can choose to participate in the IAU
                                    College program of their choice for a semester or the full academic year (fall and
                                    spring semesters), promote intellectual and cultural exchange through a series of
                                    public lectures in their field of expertise, conduct smaller seminars and discussions
                                    with students and faculty and develop their own research. They will have full access
                                    to all IAU resources including library, classrooms, WIFI and an affordable Resident
                                    Fellow apartment in Aix. Stipends and housing subsidies are available to qualified
                                    faculty from IAU partner institutions. Interested U.S. faculty may find additional
                                    information, including the IAU Resident Fellows Application, at <a href="http://www.iaufrance.org/Programs/ResidentFellows">www.iaufrance.org/Programs/ ResidentFellows</a>. The deadline to apply for SAGE funding is <strong>October 1</strong>.<br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Any questions can be directed to Professor Alan Roberts, Director of IAU College’s
                                       Marchutz School of Fine Arts and Chair of the Resident Fellows selection committee,
                                       at <a href="mailto:Alan.Roberts@iaufrance.org">Alan.Roberts@iaufrance.org</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Fulbright Scholar Program </strong></div>
                                 
                                 <div data-old-tag="td">There are a variety of programs for faculty and administrators. Visit the website
                                    for details: <a href="http://www.cies.org/us_scholars/" target="_blank">http://www.cies.org/us_scholars</a>/. The deadline to apply is <strong>August 1</strong> each year. The deadline to apply for SAGE funding is <strong>October 1</strong>. 
                                 </div>
                                 
                                 <div data-old-tag="td"><a href="mailto:scholars@iie.org">scholars@iie.org</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>      
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/curriculum/exchanges.pcf">©</a>
      </div>
   </body>
</html>