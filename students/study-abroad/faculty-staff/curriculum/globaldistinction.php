<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/curriculum/globaldistinction.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/curriculum/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li><a href="/students/study-abroad/faculty-staff/curriculum/">Curriculum</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        
                        <h2>Global Distinction</h2>
                        
                        <h6>Valencia students can now earn a global distinction on their transcript with a medallion
                           at graduation! visit the student <a href="../../students/events/globaldistinction.html">global distinction</a> (VGD) webpage for details. read below for information on how you can add your course
                           to the global distinction approved list!
                        </h6>
                        
                        <p><strong>Degrees of Internationalization (INZ)</strong></p>
                        
                        <p>All Valencia courses are now categorized by their degree of internationalization.
                           The current VGD course list has all 4th degree courses, but individual course sections
                           can be added that are 2nd and 3rd degree. Visit the following webpage for complete
                           list: <a href="../../students/globaldistinction/courselist.html">VGD Course List</a>. If you believe that a master course qualfies for the VGD, please contact Joanna
                           Branham (jbranham@valenciacollege.edu) to add it to the next INZ Committee agenda.
                           
                        </p>            
                        <ul>
                           
                           <li>
                              <u><strong>1st Degree:</strong></u> The course contains occasional international and/or intercultural events.
                           </li>
                           
                           <li>
                              <u><strong>2nd Degree:</strong></u> One unit in the course is internationally and/or interculturally-oriented (e.g.,
                              there is a unit on international marketing in the introduction to Marketing course.)
                           </li>
                           
                           <li>
                              <u><strong>3rd Degree:</strong></u> International and/or intercultural elements are intergrated throughout the course
                              (e.g., students are required to consider international/intercultural viewpoints in
                              all writing assignments.)
                           </li>
                           
                           <li>
                              <u><strong>4th Degree:</strong></u> The entire course has an international and/or intercultural orientation (e.g., World
                              History, Asian Studies, language and culture courses).
                           </li>
                           
                        </ul>            
                        
                        <p><strong>Steps for Course Section Inclusion</strong></p>
                        
                        <p>If you would like your course section added to the VGD course list, follow the steps
                           below: 
                        </p>
                        
                        <ol>
                           
                           <li>Start with a conversation with your dean</li>
                           
                           <li>Enroll in the<a href="events.html"> Internationalizing the Curriculum</a> workshop 
                           </li>
                           
                           <li>Complete a Course INZ Toolkit </li>
                           
                           <li>Complete the <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/curriculum/documents/VGDCourseSectionAdditionRequestFormv2.docx">VGD Course Section Addition Request Form v2</a>
                              
                           </li>
                           
                           <li>Obtain committee approval </li>
                           
                        </ol>            
                        
                        <p><strong>NOTE: </strong>The SAGE office has funding for faculty involved in this process to participate in
                           an international <a href="exchanges.html">exchange program</a>. You must have attended or be enrolled in the Internationalizing the Curriculum workshop
                           in order to be funded. Preference will go to those faculty who have never been funded
                           by the SAGE office for either an international exchange or study abroad in the past.
                           
                        </p>            
                        
                        <p><strong>INZ Toolkit</strong></p>
                        
                        <ul>
                           
                           <li>Check out the current Course INZ Toolkits on the <a href="http://site.valenciacollege.edu/inz/toolkits/default.aspx" target="_blank">SAGE SharePoint!</a>
                              
                           </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/curriculum/globaldistinction.pcf">©</a>
      </div>
   </body>
</html>