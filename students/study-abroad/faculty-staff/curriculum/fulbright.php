<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/faculty-staff/curriculum/fulbright.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/faculty-staff/curriculum/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/faculty-staff/">Faculty Staff</a></li>
               <li><a href="/students/study-abroad/faculty-staff/curriculum/">Curriculum</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../../index.html"></a>
                        
                        
                        
                        <h2>Fulbright Programs </h2>
                        
                        <p><strong> The 2015-2016 Fulbright U.S. Scholar Program Core Competition is now OPEN.  
                              The application deadline is AUGUST 1, 2015. Reviewers are needed if you are interested.
                              Contact
                              PeerReview@iie.org for more information.</strong></p>
                        
                        <p><strong>Did you know...</strong></p>
                        
                        <ul>
                           
                           <li>there are Fulbright Programs that are focused for community college faculty?</li>
                           
                           <li>there are Fulbright Programs that do not require a doctoral degree?</li>
                           
                           <li> adjunct faculty are eligible for Fulbright programs?</li>
                           
                           <li>advisors and administrators are eligible for Fulbright programs? </li>
                           
                           <li>you can bring in visiting scholars to internationalize your classes?</li>
                           
                           <li>the SAGE Director  is your Fulbright Campus Representative?</li>
                           
                        </ul>      
                        
                        <p>For program options, deadline dates, and website information, click here - <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/FulbrightProgramsSummary.docx">Fulbright Programs Summary</a> 
                        </p>
                        
                        <p><strong>FULBRIGHT WORKSHOP MATERIALS: </strong>If you were not able to attend the workshop, please see below for the workshop materials:<br>
                           <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/FulbrightWorkshop_14-15_Community_Colleges.ppt" target="_blank">Power Point Presentation</a><br>
                           <a href="http://valenciacollege.edu/international/studyabroad/faculty-staff/documents/FulbrightProgramsOverviewHO.pdf">Fulbright Programs Overview</a><br>
                           <a href="http://youtu.be/SR6A_4FMr3I">Steve Cunningham's Fulbright Experience in Russia </a><br>
                           <a href="http://youtu.be/ZbTPhDXjDH0">How to Apply for a Fulbright
                              </a> 
                        </p>
                        
                        <p><strong>IF YOU ARE PLANNING TO APPLY: </strong>Valencia's Grants Department will assist you by answering questions you have on the
                           writing of the application OR by providing you with feedback on your completed application
                           draft. Be sure to build time in for review and feedback prior to the deadline date.
                           Contact <strong>Kristeen Christian</strong> at <a href="mailto:kchristian6@valenciacollege.edu">kchristian6@valenciacollege.edu</a> for an appointment. 
                        </p>
                        
                        <p><strong>REMEMBER!</strong> When you complete your application, you must solve a problem or add value to something
                           to set yourself apart from others. Focus on what you plan to do, not what you have
                           already done. 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div>SESSION TITLE &amp; DESCRIPTION </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>DATE / TIME / LOCATION</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Fulbright Workshop for Faculty &amp; Professionals <br>
                                          <br>
                                          </strong>Please join us for a workshop on all Fulbright programs and get a chance to speak
                                       with Professor Steve Cunningham, Valencia College’s very first Fulbright Scholar!&nbsp;
                                       Snacks and beverages will be served.<strong>          </strong></p>
                                    
                                    <ul>
                                       
                                       <li>
                                          <strong> Learn </strong>about teaching and research opportunities in more than 125 countries
                                       </li>
                                       
                                       <li>
                                          <strong>Get Advice</strong> on selecting countries for application &amp; making contacts abroad
                                       </li>
                                       
                                       <li>
                                          <strong>Explore</strong> how your campus can host a visiting foreign Fulbright scholar
                                       </li>
                                       
                                       <li>
                                          <strong>Get Tips</strong> on how to prepare the Fulbright application
                                       </li>
                                       
                                    </ul>            
                                    
                                    <p>Read about Steve's experience in Russia on his <a href="http://steve-cunningham.blogspot.com/" target="_blank">BLOG</a>! 
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Friday, Feb. 12, 2016 <br>
                                       Lake Nona - Room TBD<br>
                                       10:00 am - 12:00 pm                  <br>
                                       <br>
                                       Space is limited; please RSVP to <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a><br>
                                       <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>      
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/faculty-staff/curriculum/fulbright.pcf">©</a>
      </div>
   </body>
</html>