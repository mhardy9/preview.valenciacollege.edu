<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/exchanges/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/exchanges/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>Exchanges</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Exchanges</h2>
                        
                        <div>
                           
                           
                           <h3>Program Providers</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia is currently working with the following institution on a Student Exchange
                                    Programs. In this arrangement, there is a one-for-one exchange where each student
                                    pays the tuition of his or her home institution, but studies for one semester at the
                                    host institution. 
                                 </p>
                                 
                                 <p>Zealand Institute of Business and Technology, DENMARK</p>
                                 
                                 <p><strong><a href="http://zibat.dk/about-zibat/" target="_blank">ABOUT ZEALAND INSTITUTE OF BUSINESS AND TECHNOLOGY</a> <img alt="Roskilde" height="168" src="campus_roskilde.jpg" width="274"></strong></p>
                                 
                                 <p><strong><a href="http://zibat.dk/oversigt-over-campusser/" target="_blank">CAMPUS LOCATIONS IN DENMARK</a> </strong></p>
                                 
                                 <p><strong><a href="http://zibat.dk/programmes-2/" target="_blank">PROGRAMS OF STUDY</a>: </strong>Marketing Management, Service Hospitality and Tourism Management, Multimedia Design,
                                    Computer Science, Logistics Management
                                 </p>
                                 
                                 <p><strong>SCHOLARSHIP FUNDING:</strong> The SAGE office has up to <span><strong>$3,500</strong></span> to support this program. You must meet eligibility requirements.
                                 </p>
                                 
                                 <p><strong>APPLICATION DEADLINES:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <strong>Spring 2017</strong> (February 1 - end of June) - The application deadline is December 1.
                                    </li>
                                    
                                    <li>
                                       <strong>Summer 2017</strong> - Not available
                                    </li>
                                    
                                    <li>
                                       <strong>Fall 2017</strong> (beginning of August - end of January) - The application deadline is May 1
                                    </li>
                                    
                                 </ul>              
                                 
                                 <p><strong>APPLICATION REQUIREMENTS: </strong></p>
                                 
                                 <ul>
                                    
                                    <li>Have at least one year of college studies in the relevant program area. </li>
                                    
                                    <li>Have a valid passport up to 6 months past the program end date.</li>
                                    
                                    <li>Pay tuition fees to Valencia College.&nbsp;&nbsp; There is no ZIBAT application fee to pay.</li>
                                    
                                    <li>Be able to show financial support for the program duration for accommodations, air
                                       and ground transportation, books, meals, activities, insurance, and incidentals.&nbsp;
                                       Estimated costs per semester are $7000. 
                                    </li>
                                    
                                    <li>Report one week before the term begins but do not arrive any earlier. </li>
                                    
                                    <li>The TOEFL test is not required.</li>
                                    
                                 </ul>              
                                 
                                 <p><strong>HOW TO APPLY: </strong></p>
                                 
                                 <ul>
                                    
                                    <li>Contact the SAGE office at 407-582-3188 or by email at <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>.      
                                    </li>
                                    
                                    <li>When you have been cleared to apply by the SAGE office, go to the following website
                                       and complete the program application form:&nbsp; <a href="http://zibat.dk/how-to-apply-and-deadlines/">http://zibat.dk/how-to-apply-and-deadlines/</a> &nbsp;&nbsp;Follow the instructions on how and where to send your application. Be sure to copy
                                       the SAGE office on your application.&nbsp;&nbsp; When you submit your application you must indicate
                                       on the top of the first page of the application form that you are applying as:&nbsp; <em>Valencia College Exchange Program</em>. 
                                    </li>
                                    
                                    <li> You must complete <em>the Course Pre-Approval / Consortium Agreement Form</em> for credit transfer and get the appropriate signatures.
                                    </li>
                                    
                                 </ul>              
                                 
                                 <p><strong>COURSE INFORMATION: </strong></p>
                                 
                                 <ul>
                                    
                                    <li>15 U.S. credits equals 30 ECTS credits. This equates to approximately 40 hours of
                                       work per week including class time, homework, projects, etc.
                                    </li>
                                    
                                    <li>You will need to plan out the courses that you want to take in advance. Be sure to
                                       talk to an academic adviser and a financial aid adviser to ensure that the courses
                                       you select fit into your degree program, otherwise, you will not get sign off. 
                                    </li>
                                    
                                    <li>You must indicate the semester and program you will be attending on the application
                                       form.&nbsp;
                                    </li>
                                    
                                 </ul>              
                                 
                                 <p><strong>ACCOMMODATIONS: </strong></p>
                                 
                                 <ul>
                                    
                                    <li>ZIBAT does not offer student housing, but in most cases you will get assistance with
                                       finding accommodation. In some of the cities where you find a ZIBAT Campus, finding
                                       housing is relatively easy, but in others, not as easy (in general – the closer to
                                       Copenhagen the more difficult and more expensive)
                                    </li>
                                    
                                    <li>Normally you have to pay the first month’s rent plus three month’s rent as a deposit.&nbsp;
                                       The monthly rent for a single room with a small toilet and bath ranges from $430 to
                                       $650 per month.&nbsp; 
                                    </li>
                                    
                                    <li>For housing assistance, contact the campus contact person indicated on <a href="http://www.zibat.dk">www.zibat.dk</a> or in your information package received from your ZIBAT Campus. Be sure to let them
                                       know that you are a Valencia College exchange student.
                                    </li>
                                    
                                 </ul>              
                                 
                                 <p><strong>TRANSPORTATION:</strong> Students will arrive to Coppenhagen Airport:&nbsp; <a href="http://www.cph.dk/CPH/UK/ABOUT+CPH/">http://www.cph.dk/CPH/UK/ABOUT+CPH/</a> and will be picked up by a ZIBAT representative from your campus.&nbsp; ZIBAT has campuses
                                    in 5 cities in the Copenhagen capital region (island of Zealand). All cities are not
                                    a big city in a US context but mid sized cities in a Danish context..&nbsp; If you are
                                    traveling from Copenhagen to one of the ZIBAT campus cities, you should take the train.&nbsp;
                                    The ride is about 20 minutes to 1 hour long (depending on ZIBAT Campus/City location
                                    your chose/study in).  Most Danes ride bikes, and in 2008 Copenhagen was declared
                                    the world’s first bike city.&nbsp; They have free city bikes, bicycle taxis, guided bicycle
                                    tours, and bike rentals.&nbsp; You can also rent a bike yourself to get around.
                                 </p>
                                 
                                 <p><strong>OTHER IMPORTANT INFORMATION:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <p>Upon arrival students will be paired up with a Danish “buddy” to help acquaint them
                                          with the college, the city, and getting used to life in Denmark.&nbsp; Students will receive
                                          an orientation as well as a guide with basic “need to know” information.
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <p>Students are allowed to work 15 hours per week.&nbsp; The salary is approximately DKK (Danish
                                          Krone) 80-100 per hour ($1.00 = 5.20 KDD).&nbsp; Students often can find work in the service
                                          industry.&nbsp; You must pay income tax on all income earned which is approximately 40
                                          percent.
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <p>Approximately 5.4 million inhabitants of Denmark speak Danish, a Germanic language
                                          related to both English and German.&nbsp; Danish uses the same alphabet as English but
                                          with three additional letters.&nbsp; Free&nbsp; Danish language courses are offered by the College
                                          as a supplement to help in managing day-to-day life in Denmark.&nbsp; About 80 percent
                                          of the Danes speak English. 
                                       </p>
                                       
                                    </li>
                                    
                                    <li> Students must be sure to get their original transcript sent to the SAGE office.&nbsp;
                                       A copy will be kept on file and the original will be sent to the Admissions Department.
                                       
                                    </li>
                                    
                                 </ul>            
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Scholarship Elegibility</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>
                                    
                                    
                                    Verify that you meet the scholarship eligibility requirements below. You must meet
                                    the requirements <u><strong>at the application deadline date</strong></u> above. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <strong>Students  must apply by the application deadline date (above) in order to receive
                                          the FULL scholarship amount. If you apply after the deadline date, you will only be
                                          eligible for a PARTIAL scholarship.</strong> <strong>ELIGIBILITY IS FACTORED BY THE APPLICATION DEADLINE DATE, NOT THE TRAVEL DATE. <br>
                                          </strong>
                                       
                                    </li>
                                    
                                    <li> Be a degree-seeking student at Valencia. You do not qualify for a scholarship if
                                       you are a transient, audit, or dual enrollment student. For students who are currently
                                       enrolled at Valencia and graduating in the term (semester) prior to travel, you can
                                       get a scholarship, but you will not have access to financial aid. <br>
                                       
                                    </li>
                                    
                                    <li>Be enrolled in the minimum number of credits in the term of your study abroad program:
                                       3 for spring (not including the study abroad program), 3 for fall (not includingthe
                                       study abroad program), or 2 for summer (can include the study abroad program). Please
                                       note that financial aid may have different requirements.<br>
                                       
                                    </li>
                                    
                                    <li>Have completed at least one semester with Valencia  within the last year. <br>
                                       
                                    </li>
                                    
                                    <li>Receive only one SAGE scholarship for study abroad within the academic year (fall-spring-summer).<br>
                                       
                                    </li>
                                    
                                    <li>Meet all course requirements (attend all pre- and post-trip meetings, participate
                                       in all in-country activities, turn in all academic assignments, obtain a grade of
                                       a C or better).<br>
                                       
                                    </li>
                                    
                                    <li>Submit a thank you card to the donor addressed "To My Esteemed Donor."<br>
                                       
                                    </li>
                                    
                                    <li>Complete the online course evaluation within two weeks upon your return.<br>
                                       
                                    </li>
                                    
                                    <li>Upon your return, you must contact the  SAGE Office within 30 days to coordinate your
                                       scholarship project. The project will involve  promoting study abroad to future students
                                       by assisting at an event, facilitating an information session, or conducting a classroom
                                       presentation. Instructions will be provided and documentation of the project completion
                                       will be necessary in order to retain your scholarship. <br>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>(4) <u>STUDY ABROAD / FINANCIAL AID ADVISING</u>: </strong>Meet with a study abroad advisor on your campus (click on the WELCOME tab above for
                                    contact names) to ensure that the course fits into your academic plan and there are
                                    no financial aid issues. The program must count towards your degree in order to use
                                    your financial aid. 
                                 </p>
                                 
                                 <p><strong>(5) <u>APPLICATION FEE</u>: </strong>Pay the application fee to the Business Office. DO THIS BEFORE YOU COMPLETE THE ONLINE
                                    APPLICATION! <br>
                                    <br>
                                    <strong>(6) <u>ONLINE APPLICATION</u>: </strong>Complete the program online application process and do not skip any pages. Each application
                                    packet contains the application form, student agreement, risk and release waiver,
                                    and medical form. <strong>You can find the link on the program webpage. </strong>Please upload the following documents with your application: (1) UNOFFICIAL TRANSCRIPT;
                                    (2) COPY OF A VALID PASSPORT UP TO 6 MONTHS AFTER THE PROGRAM END DATE.
                                 </p>
                                 
                                 <p><strong>If you do not have a passport, you will need to apply as SOON as you are accepted
                                       into the program (within 5 days). You will need to show proof of application (receipt)
                                       or a copy of your passport when the program deposit is due or you will not be enrolled
                                       in the course and your spot will be given to a waitlisted student. </strong></p>
                                 
                                 <p><strong>*********************************************************************************************************************************************************</strong></p>
                                 
                                 <p>  Here are some tools to help you plan:</p>
                                 
                                 <ul>
                                    
                                    <li> <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/SemesterStudyAbroadPlannerv5.pdf">Semester Study Abroad Planner</a>
                                       
                                    </li>
                                    
                                    <li><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/04BudgetWorksheetforStudyAbroad.pdf">Budget Worksheet for Study Abroad</a></li>
                                    
                                    
                                    <li><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/05RecommendedPackingList.pdf">Recommended Packing List</a></li>
                                    
                                    <li>
                                       <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/PropertyDocumentForm.pdf">Property Document Form</a> (for insurance claims)
                                    </li>
                                    
                                    <li><a href="http://valenciacollege.edu/international/studyabroad/students/resources/travelandsafety.cfm">Travel and Safety Resources </a></li>
                                    
                                 </ul>
                                 
                                 <p><strong>FIRST-SEMESTER COLLEGE STUDENTS:</strong><br>
                                    If this is your first semester in college, you will need to upload a copy of your
                                    reading and writing PERT scores with your application. 
                                 </p>
                                 
                                 <p><strong>NON-U.S. PASSPORT HOLDERS (CHINA PROGRAM EXCLUDED!!):<br>
                                       </strong>1. Before you apply, verify if you need a visa (it is usually a tourist visa that
                                    you will need) and what the requirements are at <a href="http://www.visahq.com" target="_blank">www.visahq.com</a>.<br>
                                    2. Contact the program country embassy
                                    to confirm the requirements.<br>
                                    3. You can only get conditional acceptance into a program. A place may be held for
                                    you up to the program payment deadline, but you must secure the visa before we can
                                    enroll you.<br>
                                    4. Please note that not all visas are approved.  You will have to do this at your
                                    own expense and risk. <br>
                                    5.  If you are at Valencia on an F or J visa, you must meet with an international
                                    advisor to see if there are any travel restrictions on your visa. 
                                 </p>
                                 
                                 <p>Neither the SAGE office nor the International Student Services office is trained to
                                    handle tourist visa questions. You must contact the resources listed above. 
                                 </p>
                                 
                                 <p><strong>DUAL ENROLLMENT STUDENTS:<br>
                                       </strong>1. You must check in with the Dual Enrollment office prior to applying: <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> or call 407-582-1600.<br>
                                    2.  The course must be 3 credits and your high school must accept it for credit. This
                                    should be done BEFORE you apply. <br>
                                    3. You must be 18 years old at the time of travel.<br>
                                    4. You do not qualify for a scholarship.<br>
                                    5. You must pay the program fee in full.
                                    
                                 </p>
                                 
                                 <p><strong>IF YOU ARE WAITLISTED:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>Be prepared to attend all predeparture meetings.</li>
                                    
                                    <li>Have a valid passport ready to go.</li>
                                    
                                    <li>Have your program payment ready to submit to the College.</li>
                                    
                                 </ul>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Funding Study Abroad</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Finding the money to support a short-term or semester study abroad program can be
                                    challenging for some. The first step in the process is to create a budget. Use the
                                    <em>Budget Worksheet for Study Abroad</em> form on the <strong>How to Apply </strong> pages to help you. If you are applying for any scholarship money or plan on using
                                    your financial aid, this is a required form.
                                    The following is a list of resources to help fund your study abroad program: 
                                 </p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Generation Study Abroad </a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>The <a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Institute of International Education</a> has just announced the launch of <strong>Generation Study Abroad</strong>, a five-years campaign to double the number of American students who study abroad
                                                by the end of the decade.
                                             </p>
                                             
                                             <p>AIFS is the first study abroad organization to become a committed partner of Generation
                                                Study Abroad. We are now offering <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">Generation Study Abroad Scholarship for semester and summer programs</a></p>
                                             
                                             <p>Every student should have the opportunity to study abroad. Our goal is to have 600,000
                                                U.S. students studying abroad in credit and non-credit programs. Over the next 5 years,
                                                <strong>Generation Study Abroad</strong> will reach out to educators at all levels and stakeholders in the public and private
                                                sectors to encourage purposeful, innovative action to get more people interested to
                                                undertake an international experience. 
                                             </p>
                                             
                                             <p><strong>Why? Because international experience is one of the most important components of a
                                                   21st. century education. Read more about the Generation Study Abroad challenge </strong></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             
                                             <p>SAGE Scholarships</p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>The SAGE office currently has funding from the Valencia Foundation and from Student
                                                Development to help offset the costs of study abroad. The amount for short-term study
                                                abroad is listed on the country website page. You must submit a separate application
                                                for semester study abroad. Awards range from $1000 to $4000 depending upon the number
                                                of applicants and the contents of your application. <strong>BE SURE TO READ THE SCHOLARSHIP REQUIREMENTS BEFORE APPLYING. </strong><br>
                                                
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div>
                                                <a href="../../../../finaid/gettingstarted/getting_started.html" target="_blank">Financial Aid</a> 
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Full-time, Valencia students who are eligible for financial aid can use these funds
                                                for  study abroad programs that award academic credit for study.&nbsp; Read more about
                                                <a href="http://www.nafsa.org/Explore_International_Education/For_Students/Financial_Aid_For_Study_Abroad/Financial_Aid_for_Study_Abroad__An_Undergraduate_Student_s_Resource/" target="_blank">Using Financial Aid for Study Abroad</a>.                    <br>
                                                
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.iie.org/gilman" target="_blank">Benjamin A. Gilman International Scholarship Program</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>The Gilman Scholarship offers awards up to $5,000 to study abroad to U.S. citizen
                                                undergraduates receiving federal <strong>Pell Grant</strong> funding at 2-year or 4-year colleges or universities. Students studying a Critical
                                                Need Language may receive a $3,000 supplement for a total possible award of $8,000.
                                             </p>
                                             
                                             <p><strong>NOTE: For your application and essay, please be sure to include the following details
                                                   IF they apply to you.</strong></p>
                                             
                                             <ul>
                                                
                                                <li>You're a current/past student at community college</li>
                                                
                                                <li>You're studying in a field underrepresented in study abroad (sciences and engineering)</li>
                                                
                                                <li> You're a first generation college student</li>
                                                
                                                <li>You're a student of diverse ethnic backgrounds</li>
                                                
                                                <li>You're a student with disabilities  </li>
                                                
                                                <li>You're traveling to a country outside of Western Europe, Australia or New Zealand
                                                   
                                                </li>
                                                
                                                <li>You intend to study a critical need language during your study abroad experience
                                                   
                                                   <ul>
                                                      
                                                      <li>Arabic (all dialects)</li>
                                                      
                                                      <li>Chinese (all dialects)</li>
                                                      
                                                      <li>Bahasa Indonesia</li>
                                                      
                                                      <li>Japanese</li>
                                                      
                                                      <li>Turkic (Azerbaijani, Kazakh, Kyrgz, Turkish, Turkmen, Uzbek)</li>
                                                      
                                                      <li>Persian (Farsi, Dari, Kurdish, Pashto, Tajiki)</li>
                                                      
                                                      <li>Indic (Hindi, Urdu, Nepali, Sinhala, Bengali, Punjabi, Marathi, Gujarati, Sindhi)</li>
                                                      
                                                      <li>Korean</li>
                                                      
                                                      <li>Russian</li>
                                                      
                                                      <li>Swahili</li>
                                                      
                                                   </ul>
                                                   
                                                </li>
                                                
                                             </ul>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.borenawards.org" target="_blank">Boren Scholarships</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">Boren Scholarships offer&nbsp;unique opportunities for U.S. undergraduates to study abroad
                                             for a semester or a year in&nbsp;world regions critical to U.S. interests (including Africa,
                                             Asia, Eastern Europe, Eurasia, Latin American &amp; the Caribbean, and the Middle East).
                                             In exchange for scholarship funding, all Boren Scholars must agree to work for the
                                             Federal Government in a position with national security responsibilities. Refer to
                                             "Boren Scholarship Basics" for complete details. Funding varies by length of program
                                             up to $20,000. Read the <a href="http://www.borenawards.org/boren_scholarship/faq">Boren FAQs</a> for more information. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div>
                                                <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">AIFS Study Abroad</a> 
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="td">Each year, more than <strong>$600,000 in scholarships</strong>, grants and financial support is awarded to deserving students &amp; institutions. Funds
                                             are available for both summer and semester programs. Please check the website for
                                             more details and how to apply. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             
                                             <p><a href="http://www.myfloridaprepaid.com/" target="_blank">Florida Prepaid College</a></p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">If you have a Florida Prepaid College program, you can use those funds to pay for
                                             the regular tuition rate for a study abroad course. These funds cannot be used for
                                             any additional program costs or lab fees.
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.fundforeducationabroad.com/applicants/" target="_blank">Fund for Education Abroad</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td"> The FEA is committed to increasing the opportunities for dedicated American students
                                             to participate in high-quality, rigorous education abroad programs by reducing financial
                                             restrictions through the provision of grants and scholarships. Participants must be
                                             studying at least 4 weeks in country. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://us.fulbrightonline.org" target="_blank">U. S. Fulbright Student Program</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">Sponsored by the U.S. Department of State, Bureau of Educational and Cultural Affairs.
                                             Fulbright is the largest U.S. international exchange program offering opportunities
                                             for students, scholars and professionals to undertake international graduate study,
                                             advanced research, university teaching, and teaching in elementary and secondary schools
                                             worldwide.                  
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div>
                                                <a href="http://www.ccisabroad.org/" target="_blank">College Consortium for International Studies</a> 
                                             </div>
                                          </div>
                                          
                                          <div data-old-tag="td">The College Consortium for International Studies offers scholarships each term to
                                             students at CCIS member institutions. Valenica maintains a membership to CCIS. If
                                             you are participating in a CCIS Program, contact the Study Abroad Office for CCIS
                                             Scholarship information. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.caorc.org/" target="_blank">Critical Language Scholarships for Intensive Summer Institutes</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">Scholarships for U.S. citizen undergraduate, master's and Ph.D. students to participate
                                             in beginning, intermediate and advanced level intensive summer language programs at
                                             American Overseas Research Centers.                  
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.floridastudentfinancialaid.org/SSFAD/home/uamain.htm" target="_blank">Florida Student Scholarship and Grant Programs</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">The Office of Student Financial Assistance (OSFA) State Programs, within the Florida
                                             Department of Education, administers a variety of postsecondary educational state-funded
                                             grants and scholarships, and provides information to students, parents, and high school
                                             and postsecondary professionals.
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.iie.org/programs/Freeman-Asia" target="_blank">Freeman-Asia: Awards for Study in Asia</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">Open to undergraduate students to study in East and Southeast Asia. Fall, Spring and
                                             Summer cycles. Awards amounts vary from $3,000 for summer term $5,000 for semester
                                             and $7,000 for academic year programs. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.studyabroadfunding.org/" target="_blank">Institute of International Education </a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">With hundreds of scholarship and grant listings, IIE's online directory Study Abroad
                                             Funding, is among the most comprehensive directories for U.S. students to find funding
                                             for studying abroad. It contains thousands of study abroad listings offered by U.S.
                                             and foreign universities and study abroad providers, along with key information on
                                             funding opportunities for study abroad experiences. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.studyabroadscholars.org/index.html" target="_blank">Hispanic Study Abroad Scholars (HSAS)</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <strong>                </strong>Over $8 Million in scholarships for Hispanic study abroad. The HSAS Scholarship offers
                                             $1,000 per semester to students whose college or university is a member of the Hispanic
                                             Association of Colleges and Universities (HACU) and are attending Global Learning
                                             Semesters programs. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.rotary.org/en/StudentsAndYouth/Pages/ridefault.aspx">Rotary International</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">The Rotary Club offers ambassadorial scholarships for students and grants for university
                                             professors. These programs can change the lives of those who participate. Through
                                             these programs, participants can travel on cultural exchanges, or help a community
                                             through a service project. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="../../../../finaid/Scholarship_bulletin.html#ScholarshipSearch" target="_blank">Scholarship Bulletin Board</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">This Scholarship Bulletin Board offers students information on available scholarships.
                                             Scholarship advertisements include application requirements, locations of applications,
                                             and specific deadlines. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.sit.edu/studyabroad/scholarships" target="_blank">SIT Study Abroad Financial Aid and Scholarships</a></div>
                                          </div>
                                          
                                          <div data-old-tag="td">The <strong>S</strong>chool for <strong>I</strong>nternational <strong>T</strong>raining and World Learning offer a variety of study abroad scholarships ranging from
                                             $500 to $5,000. Awards are based on need, merit, and the inclusion of students from
                                             a variety of institutions, from across the U.S. and from around the world. Most scholarship
                                             funds are available for semester or summer programs.                  
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             
                                             <p><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/ScholarshipstoJapan.doc">Scholarships to Japan</a> <br>
                                                (Word document)
                                             </p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><a href="http://www.aatj.org/studyabroad/scholarships.html">http://www.aatj.org/studyabroad/scholarships.html</a><br>
                                                <a href="http://www.aatj.org/studyabroad/morgan.html">http://www.aatj.org/studyabroad/morgan.html</a><br>
                                                <a href="http://www.hashi.org/scholarshiphp.html">http://www.hashi.org/scholarshiphp.html</a>                  <br>
                                                
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <div><a href="http://www.studyabroadfunding.org/" target="_blank">IIE Funding Study Abroad </a></div>
                                          </div>
                                          
                                          <div data-old-tag="td"> This directory features detailed descriptions of hundreds of study abroad scholarships,
                                             fellowships, grants, and paid internships for U.S. undergraduate, graduate and post-graduate
                                             students, and professionals. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             
                                             <p><a href="http://www.nafsa.org/students.sec/financial_aid_for_study/" target="_blank">NAFSA: Association of International Educators</a></p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td"> Financial aid for undergraduate study abroad consists mainly of federal grants and
                                             federal and private loans. However, scholarship money is also available from organizations
                                             and sponsoring companies. You likely have many questions about the cost of studying
                                             abroad and about how you can fund your abroad experience. Be sure to speak with your
                                             campus financial aid officer and study abroad adviser to learn more about specific
                                             funding options available at—and required procedures for—your college or university.
                                             
                                          </div>  
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <h3>&nbsp;</h3>
                                 
                                 <h3>How To Apply</h3>
                                 
                                 <p> The process to research, plan, and prepare for a semester study abroad program is
                                    a long one, so we recommend that you start at least one year prior to your program
                                    start date and no less than 6 months prior. 
                                 </p>
                                 
                                 <p><strong>How to Apply:</strong></p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <strong>            </strong>Contact the SAGE office at 407-582-3188 or <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>. Let us know what your plans are and be sure to check in along the way. <br>
                                       <br>
                                       
                                    </li>
                                    
                                    <li>
                                       <strong>REQUIRED! </strong>Print out and follow the instructions on the <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/SemesterStudyAbroadPlannerv5.pdf" target="_blank">Semester Study Abroad Planner</a> and use the checklist to help you navigate this process.<br>
                                       <br>
                                       
                                    </li>
                                    
                                    <li>Search for a <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/semesterprograms.cfm">program provider</a>.
                                    </li>
                                    
                                 </ul>            
                                 
                                 <p><span>IT IS VERY IMPORTANT THAT YOU SELECT A PROGRAM THAT WILL PROVIDE YOU WITH A TRANSCRIPT
                                       ISSUED BY A U.S. INSTITUTION TO OBTAIN TRANSFER CREDIT. IF YOU DIRECTLY ENROLL IN
                                       AN OVERSEAS INSTITUTION THAT HAS NO U.S. AFFILIATION, YOU CANNOT USE YOUR FINANCIAL
                                       AID, YOU CANNOT GET A SAGE SCHOLARSHIP, YOU WILL NEED TO PAY FOR A NACES TRANSCRIPT
                                       EVALUATION UPON PROGRAM COMPLETION, AND THERE IS NO GUARANTEE THAT THE CREDITS WILL
                                       TRANSFER BACK TO VALENCIA. </span><br>
                                    
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Print out the following forms which have all been referenced in the <em>Semester Study Abroad Planner</em> above.<br>
                                       
                                       <ul>
                                          
                                          <li>
                                             <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/04BudgetWorksheetforStudyAbroad.pdf">Budget Worksheet for Study Abroad</a> 
                                          </li>
                                          
                                          <li><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/ConsortiumAgreementFormforStudyAbroadv2.pdf"> Consortium Agreement Form for Study Abroad</a></li>
                                          
                                          <li><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/SAGEScholarshipInstructionsandApplicationForm.doc">SAGE Scholarship Instructions and Application Form</a></li>
                                          
                                          <li><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/05RecommendedPackingList.pdf">Recommended Packing List</a></li>
                                          
                                          <li>
                                             <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/05PropertyDocumentForm.pdf">Property Document Form</a><br>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>SAGE Scholarships: </strong>The SAGE office has  semester study abroad scholarships for up to $4000 (depending
                                    upon the contents of your application) for which  you can apply for the 2013-2014
                                    academic year. You do not have to be accepted into a program at the time of applying,
                                    but you must provide proof of acceptance prior to receiving the scholarship. Scholarships
                                    will be issued to your student account. In order to apply, you must:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Be a degree-seeking student at Valencia:
                                       
                                       <ul>
                                          
                                          <li>You cannot go on short-term study abroad with financial aid or a scholarship if you
                                             are already graduated.
                                          </li>
                                          
                                          <li>You do not qualify for a scholarship if you are a transient or audit student. </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li>Have a minimum 2.5 GPA at the time of applying to the program.</li>
                                    
                                    <li>Submit a <em>SAGE Scholarship Application Form</em> and supporting documents (above).  
                                    </li>
                                    
                                    <li>Submit proof of program acceptance. </li>
                                    
                                    <li>Have completed at least one semester with Valencia prior to travel within the last
                                       year.
                                    </li>
                                    
                                    <li>Receive only one scholarship for study abroad within the academic year.</li>
                                    
                                    <li>Meet all course requirements (attend all pre- and post-trip meetings, participate
                                       in all in-country activities, turn in all academic assignments, obtain a grade of
                                       a C or better).
                                    </li>
                                    
                                    <li>
                                       <strong><a href="http://net4.valenciacollege.edu/forms/international/studyabroad/register_request.cfm" target="_blank">Register Your Trip</a></strong> with the SAGE Office. <span>IMPORTANT!</span> 
                                    </li>
                                    
                                    <li>Submit a thank you card to the donor addressed "To My Esteemed Donor."</li>
                                    
                                    <li>Complete the online course evaluation within two weeks upon your return.</li>
                                    
                                    <li>Be willing to participate in a SAGE event to help promote study abroad to future students.
                                       
                                    </li>
                                    
                                 </ul>
                                 
                                 <p><strong>Important Websites: </strong>The following are some important website links for you. They are all referenced in
                                    the <em>Semester Study Abroad Planner</em> above.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>
                                       <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/fundingstudyabroad.cfm" target="_blank">Study Abroad Scholarships</a>                
                                    </li>
                                    
                                    <li>
                                       <a href="http://www.usps.com/passport/" target="_blank">Passport</a> (your passport should be valid for up to 90 days after your program end date) 
                                    </li>
                                    
                                    <li>
                                       <a href="http://www.visahq.com/" target="_blank">Visa</a> (verify if you need a visa and apply 45-120 days prior) 
                                    </li>
                                    
                                    <li><a href="http://www.cmiinsurance.com" target="_blank">Medical/trip insurance</a></li>
                                    
                                    <li><a href="http://www.isic.org/" target="_blank">International Student ID Card</a></li>
                                    
                                 </ul>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Register Your Trip</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>This information will assist you with your overseas travel. You can print this page
                                    and use it as a checklist:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Per <a href="../../../../about/general-counsel/policy/documents/5-08-Travel-by-Authorized-Personnel.pdf.html" target="_blank">College policy 6Hx28:5-08</a>, all Valencia staff and faculty traveling overseas must register with the SAGE office
                                       no later than 15 days prior to departure.<br>
                                       <br>
                                       
                                    </li>
                                    
                                    <li>
                                       <u><strong>You do NOT need to complete this process if</strong></u>: 1) you are leading a study abroad program with students or 2) you are traveling
                                       to Canada or Puerto Rico.<br>
                                       <br>
                                       
                                    </li>
                                    
                                    <li>
                                       <u><strong>GROUP TRAVELERS (i.e., NEH Grant)</strong></u><strong>:</strong> One person should be the communication point person with the SAGE office. Please
                                       complete the <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/TravelersInformationWorksheet_InternationalGroupTravel.xlsx">Traveler's Information Worksheet - Group Travel</a> for the entire group and send it to the SAGE office. <br>
                                       <br>
                                       
                                    </li>
                                    
                                    <li> Email the SAGE office at <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a> or call 407-582-3188 if you have any questions on filling out these forms. 
                                    </li>
                                    
                                 </ul>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <div><strong>STEP</strong></div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div><strong>WHO</strong></div>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <div><strong>TASK</strong></div>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 1: <br>
                                                Check for a Travel Warning </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">Check for a <a href="http://travel.state.gov/travel/cis_pa_tw/tw/tw_1764.html" target="_blank">Department of State Travel Warning</a> for the destination country. If you see that your destination is listed on the warning
                                             list, please contact the SAGE office. The College may not fund any trips going to
                                             a country with an active travel warning. 
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 2: <br>
                                                Request for International Travel Form </strong></div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Faculty exchanges</p>
                                             
                                             <p>International conferences</p>
                                             
                                             <p>Program Leader-in-Training </p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>You do NOT need to complete this step if you are traveling on an Endowed Chair, a
                                                sabbatical, or an NEH grant. 
                                             </p>
                                             
                                             <p>Complete the <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/IntlTravel_RequestforInternationalTravelForm.docx">Valencia Request for International Travel Form</a> by <strong>October 1</strong> for the academic year in which the travel will take place. This form requires dean/supervisor
                                                and campus president/vice president signatures. Send the form with signatures to the
                                                SAGE office DTC-2 or email it to <a href="mailto:jrobertson@valenciacollege.edu">jrobertson@valenciacollege.edu</a>.
                                             </p>
                                             
                                             <p><span>INTERNATIONAL EDUCATION BUDGET FUNDING REQUESTS:</span> If you are requesting funding from the International Education (SAGE) budget, you
                                                must submit a <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/IntlTravel_PlannedProgramItineraryforInternationalTravel.docx">Planned Program Itinerary for International Travel</a> and a <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/IntlTravel_ProposedBudgetforInternationalTravel.xlsx">Proposed Budget for International Travel</a> with the request form. PLEASE NOTE THAT THE COLLEGE NO LONGER REIMBURSES FOR MEALS
                                                FOR INTERNATIONAL TRAVEL. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 3: <br>
                                                Proposal Form </strong></div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Faculty exchanges</p>
                                             
                                             <p>International conferences</p>
                                             
                                             <p>Program Leader-in-Training </p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Complete any additional paperwork as applicable. Ask the SAGE office if you are exempt
                                                from this step or which forms apply if you are unsure. Obtain the required signatures
                                                and send the completed forms to the SAGE office. <span>Sabbaticals and Endowed Chairs: These have their own application/proposal process.
                                                   You can skip this step. </span></p>
                                             
                                             <ul>
                                                
                                                <li>
                                                   <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/Faculty-StaffInternationalProgramProposalInstructions.docx">Faculty-Staff International Program Proposal Instructions</a>, <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/Faculty-StaffInternationalProgramProposal.docx">Faculty-Staff International Program Proposal</a> (use for faculty exchange programs - one way or reciprocal) <br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>
                                                   <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/StudyAbroadProgramLeader-in-TrainingProposalForm.docx">Program Leader-in-Training Mendorship</a>, <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/PLITProgramExpectations.docx">Program Expectations</a> <br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>
                                                   <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/SAGESiteVisitQuestionnaireforOverseasColleges.docx">Questionnaire for Overseas Colleges</a> (use for site visits for Endowed Chairs) 
                                                </li>
                                                
                                             </ul>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 4: <br>
                                                Passport and Visa </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Ensure that your passport is up to date and apply for a <a href="http://www.visahq.com/" target="_blank">visa</a> if applicable (be sure to verify the visa requirements). Some countries require your
                                                passport to be valid at least 6 months after your return to the United States. <strong>Please send a clear copy of your passport to the SAGE office. </strong></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 5: Airline Reservations </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p><u>INDIVIDUAL TRAVELERS</u>: You can make your reservations online. Ask your department for assistance. You can
                                                request a cash advance on your authorization form for the airline ticket. Accounts
                                                Payable will send you a check one week prior to departure. <strong>If the money is coming from the SAGE budget, you must contact the SAGE director to
                                                   get approval on your ticket before making the purchase. </strong></p>
                                             
                                             <p><u>GROUP TRAVELERS (i.e., NEH grant)</u>: Contact Robert Reed at Travel Max (407-704-7850). He will need you to complete the
                                                <em>Traveler's Information Worksheet - Group Travel</em> in order to purchase the tickets. <br>
                                                <br>
                                                Ask your supervisor if you can use the department credit card (P-card) to pay for
                                                the flights. The cardholder will need to charge the amount to the approved budget
                                                in PaymentNet. The budget manager will need to be informed of the purchase and will
                                                require all receipts. Contact Accounts Payable if you do not know the budget index
                                                number. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 6: Accommodations Reservations </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             <p> Make your hotel or other accommodations arrangments. You can either: 1) pay for the
                                                hotel and get reimbursed after your trip, or 2) get the hotel set up in the system
                                                as a vendor and Valencia will pay the hotel directly. Please contact Accounts Payable
                                                for more details (ext. 3312). 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 7: Authorization for International Travel Form </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Faculty and staff must complete their own form. Contact your department assistant
                                                for help. <strong>For detailed instructions on how to complete this form, click <a href="http://valenciastudyabroad.pbworks.com/w/page/31749225/Authorization-for-International-Travel" target="_blank">HERE</a>.</strong></p>
                                             
                                             <p><a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/IntlTravel_AuthorizationforInternationalTravelForm.pdf">Authorization for International Travel Form</a>  
                                             </p>
                                             
                                             <p><strong>Per the requirements of Accounts Payable, you MUST provide supporting documentation</strong> <strong>for ALL costs related to the international travel for which Valencia is paying.</strong> <strong> Example supporting documents....</strong> 
                                             </p>
                                             
                                             <ul>
                                                
                                                <li>airline quote or receipt from the travel agent or a website (please provide the days
                                                   and times of departure and return) 
                                                </li>
                                                
                                                <li>tour operator quote or receipt </li>
                                                
                                                <li>insurance quote or receipt</li>
                                                
                                                <li>hotel website pages with prices (if you cannot obtain hotel costs, we will use the
                                                   Dept. of State'smaximum lodging rate on the Per Diem by Country website page below)
                                                </li>
                                                
                                                <li>cultural activities webpages with prices</li>
                                                
                                                <li> supporting documentation for any other costs associated with the trip</li>
                                                
                                             </ul>
                                             
                                             <p><strong>The following is a list of expenses that SAGE funds will NOT cover:</strong> meals per diem, airport parking, airline upgrades, entrance fees to cultural or social
                                                activities that are not directly related to the internationalized curriculum project,
                                                and expenses for incoming participants from the overseas institution 
                                             </p>
                                             
                                             <p><strong>Please forward your signed paperwork to the SAGE office at DO-335. We will stamp it
                                                   received and forward it to Accounts Payable. Accounts Payable will not process your
                                                   form without the SAGE stamp on it. </strong></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 8:<br>
                                                Register Your Travel Plans with SAGE </strong></div>
                                          
                                          <div data-old-tag="td">All travelers EXCEPT NEH Grant groups </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Please complete the travel registration at least <strong>15 days prior to your trip</strong>. <strong>You will need your destination itinerary, passport, airline, and accommodations information
                                                   in order to complete your registration. </strong></p>
                                             
                                             
                                             <p><strong><u>NEH Grant Participants</u>:</strong> The coordinator will need to complete the<a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/TravelersInformationWorksheet_InternationalGroupTravel.xlsx"> Travelers Information Worksheet </a>and send to the SAGE Office in lieu of completing the individual online form. 
                                             </p>
                                             
                                             <p><strong>IMPORTANT: </strong>SAGE must receive specific accommodation locations in order to register you with the
                                                <a href="https://step.state.gov/step/" target="_blank">Department of State's STEP Program</a>. Failure to provide this information will mean that you will not be registered and
                                                we will not receive important embassy updates in the event that something occurs in
                                                your country destination. 
                                             </p>
                                             
                                             <ul>
                                                
                                                <li>If you will need an international cell phone, you can request one through the SAGE
                                                   office. Please note that we do not always have phones available. International calling
                                                   is VERY expensive, so the SAGE phone should only be used for emergency purposes. Please
                                                   see the <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/TelestialPhoneCountryRates.docx">Telestial Phone Country Rates</a>. If you are not traveling as part of a study abroad program, you must provide the
                                                   SAGE office with the budget information to charge your department. <span>NOTE: You will be responsible for payment of all non-emergency phone calls</span>.<br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>If you would like to have an iPad instead of an international phone, the SAGE office
                                                   can issue you one if available at the time of your travel (study abroad program leaders
                                                   get priority). The iPad is set up with lots of travel apps and Facetime to make video
                                                   calls. IF YOU LOSE ANY OF THE ACCESSORIES, YOU WILL BE REQUIRED TO REPLACE THEM. <br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>If you have your own smartphone or iPad, we also have portable travel batteries that
                                                   hold up to four charges and electrical adaptors for different countries that we can
                                                   issue you. <strong><br>
                                                      </strong>
                                                   
                                                </li>
                                                
                                             </ul>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 9: <br>
                                                Medical/Trip Insurance</strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Valencia requires all international travelers who are working during their trip to
                                                carry medical/trip insurance. The cost is approximately <strong>$30.89</strong> per person for every <strong>15 days </strong>.Please note that this is additional coverage that includes overseas emergency assistance.
                                                See the <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/PremierGroupDocPlan1FrontierMEDEXFaculty.pdf">coverage plan</a> for details. 
                                             </p>
                                             
                                             <p> Once you have registered your trip online (step 8), contact Ashley Metz in the SAGE
                                                Office with the budget number that the insurance will be paid out of. If you are paying
                                                with personal funds, you can pay at the campus Business Office. Contact the SAGE Office
                                                for details on how to complete this process.
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 10: Department of State Registration </strong></div>
                                          
                                          <div data-old-tag="td">All travelers</div>
                                          
                                          <div data-old-tag="td">
                                             <p> Upon receipt of your registration (STEP 8), the SAGE office will email you a copy
                                                of <em>Valencia's Emergency-Crisis Management Plan for International Travel, </em>register you with the <a href="https://step.state.gov/step/" target="_blank">Department of State's STEP (Smart Travelers Enrollment Program)</a>, and will inform Valencia's Emergency Response Team of your upcoming travel. <br>
                                                
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 11:<br>
                                                Finances </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>It is very important to keep a copy of all your receipts that are related to the costs
                                                of your travel. Please use the <a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/InternationalExpenseReport.xlsx">International E xpense Report</a> to make it easier to figure out the expenses for the final per diem.
                                             </p>
                                             
                                             <p>Our program providers suggest that you exchange money PRIOR to leaving the United
                                                States in order to get the best rates. There is a currency exchange booth at the Florida
                                                Mall (407-854-0860). 
                                             </p>
                                             
                                             <p>If you will be using a personal credit card overseas, be sure to contact the credit
                                                card company in advance to let them know so that they do not freeze your account for
                                                suspicious activity. 
                                             </p>
                                             
                                             <p>If you are traveling to Europe, be aware that some countries are in the process of
                                                using a new credit card system that uses a chip and pin. The magnetic swipe credit
                                                cards will not work. You should find out what system is in use at your destination
                                                and plan accordingly. You may need to have access to cash, which can be obtained at
                                                the bank or ATM. Be sure to take a debit card for cash access. Here is a copy that
                                                offers the new chip and pin credit card: <a href="https://www.andrewsfcu.org/credit_cards_and_loans/credit_cards/globetrek_rewards.html" target="_blank">Andrews Federal Credit Union</a>. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 12:<br>
                                                Know Before You Go </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>The following is a list of other important information for your international travel:
                                                
                                             </p>
                                             
                                             <p>You must contact CMI <strong>before</strong> you request or pay for any medical services. They may not reimburse you if you do
                                                not contact them first. Call the 24-7 number on your card.<br>
                                                <br>
                                                <br>
                                                Please visit the <a href="http://valenciacollege.edu/international/studyabroad/students/resources/travelandsafety.cfm">Travel and Safety</a> webpage for lots of useful links. 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 13: <br>
                                                Upon Your Return </strong></div>
                                          
                                          <div data-old-tag="td">All travelers </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Please complete the<a href="http://valenciacollege.edu/international/studyabroad/students/exchanges/documents/IntlTravel_PerDiemForm.pdf"> Per Diem Form for International Travel</a>. Include a copy of ALL receipts (airfare, hotel, ground transportation, entrance
                                                fees, etc.) with your per diem form for close out. <strong>For detailed instructions on how to complete this form, click <a href="http://valenciastudyabroad.pbworks.com/w/page/53088451/Per-Diem-for-International-Travel" target="_blank">HERE</a>. </strong>Send the Per Diem, receipts, and international cell phone (if applicable) to the SAGE
                                                office at DTC-2.
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td"><strong>STEP 14: <br>
                                                Faculty Exchange Close Out </strong></div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Faculty Exchanges</p>
                                             
                                             <p>Program Leader-in-Training</p>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <ul>
                                                
                                                <li>Contact the SAGE office to let us know how you will be meeting the "sharing" requirement
                                                   of your program (Cafe Conversations, department presentation, conference presentation,
                                                   International Education Week, Skillshop, etc.). <br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>
                                                   <u>OPTIONAL</u>: Upload photos to the SAGE Flickr (create a set for your program): <a href="http://valenciastudyabroad.pbworks.com/w/page/31158209/Upload-Photos-to-SAGE-Flickr" target="_blank">http://valenciastudyabroad.pbworks.com/w/page/31158209/Upload-Photos-to-SAGE-Flickr</a> OR send us a link to your blog to post to the SAGE website. <br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>
                                                   <u>EXCHANGES</u>: Submit your "Internationalizing the Curriculum" project to the SAGE office by the
                                                   end of the semester after you return. <br>
                                                   <br>
                                                   
                                                </li>
                                                
                                                <li>
                                                   <u>PLIT</u>: Submit your final report within 30 days upon your return. <br>
                                                   
                                                </li>
                                                
                                             </ul>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/exchanges/index.pcf">©</a>
      </div>
   </body>
</html>