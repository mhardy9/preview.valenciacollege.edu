<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/exchanges.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <div>  
                           
                           
                           <div>
                              
                              <div> <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div> 
                                             
                                             
                                             
                                             
                                             
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../exchange/index.html">JEV</a></li>
                                                   
                                                   
                                                   <li><a href="http://international.valenciacollege.edu">ISS/CIE</a></li>
                                                   
                                                   
                                                   <li><a href="../../../faculty/development/programs/curriculumDevelopment/index.html">Faculty Development</a></li>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                          </div> 
                                          
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a name="content" id="content"></a>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      <link href="../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      
                                                      <h2>Student Exchange Programs</h2>
                                                      
                                                      <p> Valencia is currently working with the following institution on a student exchange
                                                         program. In this arrangement, there is a one-for-one exchange where each student pays
                                                         the tuition of his or her home institution, but studies for one semester at the host
                                                         institution. 
                                                      </p>
                                                      
                                                      <p> The process to research, plan, and prepare for a semester exchange program is almost
                                                         exactly like planning for a regular semester abroad program with just a few minor
                                                         differences. We recommend that you start at least two semesters prior to your program
                                                         start date. Follow the institution-specific instructions below and use the&nbsp;<u><a href="http://valenciacollege.edu/international/studyabroad/students/semester/documents/SemesterStudyAbroadPlannerv6a.pdf" target="_blank">Semester Study Abroad Planner</a></u> &nbsp;to help you navigate the application process. 
                                                         
                                                         
                                                         <strong>Since this is an exchange program, you will pay tuition to Valencia but not to the
                                                            host institution.&nbsp; </strong></p>
                                                      
                                                      <p>Zealand Institute of Technology </p>
                                                      
                                                      <p><a href="http://zibat.dk/about-zibat/"><u>ABOUT ZEALAND INSTITUTE OF BUSINESS AND TECHNOLOGY</u></a></p>
                                                      
                                                      
                                                      <p><a href="http://zibat.dk/oversigt-over-campusser/"><u>CAMPUS LOCATIONS IN DENMARK</u></a></p>
                                                      
                                                      <p><a href="http://zibat.dk/programmes-2/"><u>PROGRAMS OF STUDY</u></a>:&nbsp;Marketing Management, Service Hospitality and Tourism Management, Multimedia Design,
                                                         Computer Science, Logistics Management
                                                      </p>
                                                      
                                                      <p>SCHOLARSHIP FUNDING:&nbsp;The SAGE office has up to&nbsp;<span><strong>$3000</strong></span>&nbsp;to support this program. You must meet eligibility requirements.
                                                      </p>
                                                      
                                                      <p>APPLICATION DEADLINES:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            <strong>Fall 2015</strong>&nbsp; - The application deadline is May 1, 2015
                                                         </li>
                                                         
                                                         <li>
                                                            <strong>Spring 2016</strong>&nbsp; - The application deadline is November 1, 2015
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>APPLICATION REQUIREMENTS:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Have at least one year of college studies in the relevant program area.</li>
                                                         
                                                         <li>Have a valid passport up to 6 months past the program end date.</li>
                                                         
                                                         <li>Pay tuition fees to Valencia College.&nbsp;&nbsp; There is no ZIBAT application fee to pay.</li>
                                                         
                                                         <li>Be able to show financial support for the program duration for accommodations, air
                                                            and ground transportation, books, meals, activities, insurance, and incidentals.&nbsp;
                                                            Estimated costs per semester are $7000.
                                                         </li>
                                                         
                                                         <li>Report one week before the term begins but do not arrive any earlier.</li>
                                                         
                                                         <li>The TOEFL test is not required.</li>
                                                         
                                                      </ul>
                                                      
                                                      <p>HOW TO APPLY:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Contact the SAGE office at 407-582-3188 or by email at&nbsp;<a href="mailto:studyabroad@valenciacollege.edu"><u>studyabroad@valenciacollege.edu</u></a>.
                                                         </li>
                                                         
                                                         <li>When you have been cleared to apply by the SAGE office, go to the following website
                                                            and complete the program application form:&nbsp;<a href="http://zibat.dk/international-application-procedures/" target="_blank">http://zibat.dk/international-application-procedures/</a><u> </u>Follow the instructions on how and where to send your application. Be sure to copy
                                                            the SAGE office on your application.&nbsp;&nbsp; When you submit your application you must indicate
                                                            on the top of the first page of the application form that you are applying as:&nbsp;&nbsp;Valencia
                                                            College Exchange Program.
                                                         </li>
                                                         
                                                         <li>You must complete&nbsp;the Course Pre-Approval / Consortium Agreement Form&nbsp;for credit transfer
                                                            and get the appropriate signatures.
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>COURSE INFORMATION:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>15 U.S. credits equals 30 ECTS credits. This equates to approximately 40 hours of
                                                            work per week including class time, homework, projects, etc.
                                                         </li>
                                                         
                                                         <li>You will need to plan out the courses that you want to take in advance. Be sure to
                                                            talk to an academic adviser and a financial aid adviser to ensure that the courses
                                                            you select fit into your degree program, otherwise, you will not get sign off.
                                                         </li>
                                                         
                                                         <li>You must indicate the semester and program you will be attending on the application
                                                            form.&nbsp;
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>ACCOMMODATIONS:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>ZIBAT does not offer student housing, but in most cases you will get assistance with
                                                            finding accommodation. In some of the cities where you find a ZIBAT Campus, finding
                                                            housing is relatively easy, but in others, not as easy (in general – the closer to
                                                            Copenhagen the more difficult and more expensive)
                                                         </li>
                                                         
                                                         <li>Normally you have to pay the first month’s rent plus three month’s rent as a deposit.&nbsp;
                                                            The monthly rent for a single room with a small toilet and bath ranges from $430 to
                                                            $650 per month.&nbsp;
                                                         </li>
                                                         
                                                         <li>For housing assistance, contact the campus contact person indicated on&nbsp;<a href="http://www.zibat.dk/"><u>www.zibat.dk</u></a>&nbsp;or in your information package received from your ZIBAT Campus. Be sure to let them
                                                            know that you are a Valencia College exchange student.
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <p>TRANSPORTATION:&nbsp;Students will arrive to Coppenhagen Airport:&nbsp;&nbsp;<a href="http://www.cph.dk/en/about-cph/" target="_blank">http://www.cph.dk/en/about-cph/</a> &nbsp;and will be picked up by a ZIBAT representative from your campus.&nbsp; ZIBAT has campuses
                                                         in 5 cities in the Copenhagen capital region (island of Zealand). All cities are not
                                                         a big city in a US context but mid sized cities in a Danish context..&nbsp; If you are
                                                         traveling from Copenhagen to one of the ZIBAT campus cities, you should take the train.&nbsp;
                                                         The ride is about 20 minutes to 1 hour long (depending on ZIBAT Campus/City location
                                                         your chose/study in). Most Danes ride bikes, and in 2008 Copenhagen was declared the
                                                         world’s first bike city.&nbsp; They have free city bikes, bicycle taxis, guided bicycle
                                                         tours, and bike rentals.&nbsp; You can also rent a bike yourself to get around.
                                                      </p>
                                                      
                                                      <p>OTHER IMPORTANT INFORMATION:</p>
                                                      
                                                      <ul>
                                                         
                                                         <li>
                                                            
                                                            <p>Upon arrival students will be paired up with a Danish “buddy” to help acquaint them
                                                               with the college, the city, and getting used to life in Denmark.&nbsp; Students will receive
                                                               an orientation as well as a guide with basic “need to know” information.
                                                            </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Students are allowed to work 15 hours per week.&nbsp; The salary is approximately DKK (Danish
                                                               Krone) 80-100 per hour ($1.00 = 5.20 KDD).&nbsp; Students often can find work in the service
                                                               industry.&nbsp; You must pay income tax on all income earned which is approximately 40
                                                               percent.
                                                            </p>
                                                            
                                                         </li>
                                                         
                                                         <li>
                                                            
                                                            <p>Approximately 5.4 million inhabitants of Denmark speak Danish, a Germanic language
                                                               related to both English and German.&nbsp; Danish uses the same alphabet as English but
                                                               with three additional letters.&nbsp; Free&nbsp; Danish language courses are offered by the College
                                                               as a supplement to help in managing day-to-day life in Denmark.&nbsp; About 80 percent
                                                               of the Danes speak English.
                                                            </p>
                                                            
                                                         </li>
                                                         
                                                         <li>Students must be sure to get their original transcript sent to the SAGE office.&nbsp; A
                                                            copy will be kept on file and the original will be sent to the Admissions Department.
                                                         </li>
                                                         
                                                      </ul>                    
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div> 
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/exchanges.pcf">©</a>
      </div>
   </body>
</html>