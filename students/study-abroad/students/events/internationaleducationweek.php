<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/events/internationaleducationweek.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/events/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/events/">Events</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div> <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../../exchange/index.html">JEV</a></li>
                                                   
                                                   
                                                   <li><a href="http://international.valenciacollege.edu">ISS/CIE</a></li>
                                                   
                                                   
                                                   <li><a href="../../../../faculty/development/programs/curriculumDevelopment/index.html">Faculty Development</a></li>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      <h2> November 13th-17th</h2>
                                                      
                                                      <h3>IEW 2017 Schedule - Coming soon!</h3>
                                                      
                                                      <p>We are currently accepting proposals for International  Education Week.&nbsp; Check back
                                                         soon for the  2017 schedule of events!
                                                      </p>
                                                      
                                                      
                                                      <h3>Event Overview</h3>
                                                      
                                                      <p>International Education Week is an opportunity to celebrate  the benefits of international
                                                         education and exchange worldwide.&nbsp; This  joint initiative of the U.S. Dept. of State
                                                         and the U.S. Dept. of Education is  part of their efforts to promote programs that
                                                         prepare Americans for a global  environment and attract future leaders from abroad
                                                         to study, learn, and  exchange experiences in the United States.
                                                      </p>
                                                      
                                                      <p> The purpose of this event is to promote the concept of  global citizenship and cross-cultural
                                                         awareness. This is the perfect  opportunity to continue to learn about the world around
                                                         you! Valencia faculty,  staff, and students will work together to host a variety of
                                                         activities at all  the Valencia campuses: West Campus, East Campus, Winter Park Campus,
                                                         Osceola  Campus, Lake Nona Campus, and the new Poinciana Campus. All activities will
                                                         be  developed around the following learning outcomes:
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Promote the concept of global citizenship and  cross-cultural awareness</li>
                                                         
                                                         <li>Gain a greater level of understanding and  appreciation for different world cultures</li>
                                                         
                                                         <li>Promote study abroad opportunities to students</li>
                                                         
                                                         <li>Create cultural opportunities for international  and domestic students to interact</li>
                                                         
                                                         <li>Allow international students and study abroad  students to share experiences of their
                                                            home/host country
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <h3>Submit a Proposal</h3>
                                                      
                                                      <p>If you are interested in participating in International  Education Week, please submit
                                                         a <a href="https://valenciacc.ut1.qualtrics.com/jfe/form/SV_3dTmiKp2O3LG3s1">proposal  form</a>.&nbsp; 
                                                      </p>
                                                      
                                                      <p>Questions about the form or  participating in IEW?&nbsp; </p>
                                                      
                                                      <ul>
                                                         
                                                         <li>Contact: Lindi Kourtellis at  <a href="mailto:lkourtellis@valenciacollege.edu">lkourtellis@valenciacollege.edu</a>
                                                            
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      
                                                      <h3>Event Flyer and Photos</h3>
                                                      
                                                      <p><a href="documents/IEW-2017-Faculty-Staff-Information.pdf" target="_blank">2017 International Education Week Faculty/Staff Flyer</a></p>
                                                      
                                                      
                                                      <h3>Social Media</h3>
                                                      
                                                      <p><a href="https://www.instagram.com/ValenciaIEW/" target="_blank">Follow us on Instagram and post your event photos:  #ValenciaIEW</a></p>
                                                      
                                                      <p><a href="https://www.facebook.com/InternationalEdWeek" target="_blank">Check out the Dept.  of State's IEW page on Facebook.</a></p>
                                                      
                                                      <p><a href="http://libguides.valenciacollege.edu/iew-speech" target="_blank">SPEECH CONTEST</a></p>
                                                      
                                                      <p><a href="https://www.flickr.com/photos/valencia_sage/sets/72157629837045994/" target="_blank">Check out pictures of past International Education Week Celebrations</a></p>
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/events/internationaleducationweek.pcf">©</a>
      </div>
   </body>
</html>