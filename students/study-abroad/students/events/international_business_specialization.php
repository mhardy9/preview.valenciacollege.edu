<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/events/international_business_specialization.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/events/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/events/">Events</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../../exchange/index.html">JEV</a></li>
                                                   
                                                   
                                                   <li><a href="http://international.valenciacollege.edu">ISS/CIE</a></li>
                                                   
                                                   
                                                   <li><a href="../../../../faculty/development/programs/curriculumDevelopment/index.html">Faculty Development</a></li>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      <link href="../../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      <h2>International Business Specialization</h2>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      <h2>Program Outcomes</h2>
                                                      
                                                      <ul>
                                                         
                                                         <li>Describe the impact of language, culture, religion and local government regulations
                                                            on the conduct of international business.
                                                         </li>
                                                         
                                                         <li>Explain the demographics, market segmentation and the selection of target markets
                                                            as applied to the global business environment.
                                                         </li>
                                                         
                                                         <li>Demonstrate strategies for opening foreign markets, including pure exporting, use
                                                            of local distributors, global manufacturing, operating wholly-owned subsidiaries and
                                                            foreign direct investment.
                                                         </li>
                                                         
                                                         <li>Apply the planning process and develop marketing strategies for the international
                                                            marketplace.
                                                         </li>
                                                         
                                                         <li>Identify and interpret relevant international financial documents, and evaluate financial
                                                            strategies that support an organization’s integrative trade initiatives.
                                                         </li>
                                                         
                                                      </ul>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=ECO%202013">ECO&nbsp;2013</a></div>
                                                               
                                                               <div data-old-tag="td">PRINCIPLES OF ECONOMICS-MACRO ~</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">or&nbsp;<a href="http://catalog.valenciacollege.edu/search/?P=ECO%202023">ECO&nbsp;2023</a>
                                                                  
                                                               </div>
                                                               
                                                               <div data-old-tag="td"> PRINCIPLES OF ECONOMICS-MICRO</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=GEB%201350">GEB&nbsp;1350</a></div>
                                                               
                                                               <div data-old-tag="td">INTRODUCTION TO INTERNATIONAL BUSINESS +</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=MAR%202150">MAR&nbsp;2150</a></div>
                                                               
                                                               <div data-old-tag="td">INTERNATIONAL MARKETING</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=FIN%202051">FIN&nbsp;2051</a></div>
                                                               
                                                               <div data-old-tag="td">INTERNATIONAL FINANCE *</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=CGS%202510C">CGS&nbsp;2510C</a></div>
                                                               
                                                               <div data-old-tag="td">SPREADSHEET APPLICATIONS FOR BUSINESS</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=MTB%201103">MTB&nbsp;1103</a></div>
                                                               
                                                               <div data-old-tag="td">BUSINESS MATHEMATICS 1</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td"><a href="http://catalog.valenciacollege.edu/search/?P=OST%201335C">OST&nbsp;1335C</a></div>
                                                               
                                                               <div data-old-tag="td">BUSINESS COMMUNICATIONS</div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Science or Math</div>
                                                               
                                                               <div data-old-tag="td">See <a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#">Gen. Ed.</a> Core Requirement +*~
                                                               </div>
                                                               
                                                               <div data-old-tag="td">3</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Internship(s) and/or Business Electives 2</div>
                                                               
                                                               <div data-old-tag="td">6</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">Total Credit Hours</div>
                                                               
                                                               <div data-old-tag="td">30</div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">+</div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>This course must be completed with a grade of C or better.</p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">*</div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>This course has a prerequisite; check description in Valencia catalog.</p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">~</div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>&nbsp;This is a general education course.</p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">1</div>
                                                               
                                                               <div data-old-tag="td">
                                                                  <p>MAT 1033C or higher mathematics course&nbsp;may be substituted for MTB 1103.&nbsp;</p>
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">2</div>
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p>Business Electives: Courses with the following subject prefixes may be used if not
                                                                     being used as a program requirement: ACG, APA, BUL, CGS, CIS, COP, CTS, FIN, GEB,
                                                                     INP, MAN, MAR, MKA, MNA, MTB, OST, REE, RMI, SBM, TAX, SLS 1303 JOB SEARCH.
                                                                  </p>
                                                                  
                                                                  <p>If a student is going for the Global Distinction, pick two of the following courses:
                                                                     &nbsp;<br>
                                                                     GEA 1000 World Geography, GEB 2955 Immersion in Global Business, HUM 2403 Middle Eastern
                                                                     Humanities, HUM 2410 Asian Humanities, HUM 2454 African-American Humanities, HUM 2461
                                                                     Latin American Humanities, INR 2002 International Politics or INR 2002H International
                                                                     Politics Honors, SPC 1700 Cross-Cultural Communication
                                                                  </p>
                                                                  
                                                                  <p><a href="http://catalog.valenciacollege.edu/degrees/associateinscience/businessaccountingofficerelated/businessadministration/#programrequirementstext%23programrequirementstext">#programrequirementstext</a></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/events/international_business_specialization.pcf">©</a>
      </div>
   </body>
</html>