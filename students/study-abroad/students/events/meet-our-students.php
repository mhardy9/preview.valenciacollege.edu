<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/events/meet-our-students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/events/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/events/">Events</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../../exchange/index.html">JEV</a></li>
                                                   
                                                   
                                                   <li><a href="http://international.valenciacollege.edu">ISS/CIE</a></li>
                                                   
                                                   
                                                   <li><a href="../../../../faculty/development/programs/curriculumDevelopment/index.html">Faculty Development</a></li>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      <link href="../../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      <p>MEET OUR STUDENTS </p>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><strong><em>NAKITA CAMACHO</em></strong> - For her Capstone project Nakita created a video on how Anime Conventions bring
                                                                     diverse people together.
                                                                  </p>
                                                                  
                                                                  <p>Nakita, holding her medallion and Valencia degree in this picture, deeply believes
                                                                     that “the world would be a better place when everyone finds common bridges of friendship,
                                                                     respect and tolerance — the tenet principles of Anime conventions all over the world.”
                                                                  </p>
                                                                  
                                                                  <p><strong>SPRING 2015 Graduate </strong></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><span>JOSSIE MORALES </span>- As a graphic designer who is hoping to go out to other countries and help small
                                                                     businesses grow,” Jossie, from the Osceola Campus, believes in empowering small businesses
                                                                     to preserve a sense of place. Just recently, for instance, she crafted a new logo
                                                                     for the Orange County Animal Shelter and will soon be approaching them with her idea.
                                                                  </p>
                                                                  
                                                                  <p><strong>SPRING 2015 Graduate </strong></p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  
                                                                  
                                                                  <p> [No picture]</p>
                                                                  
                                                               </div>
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><span>MELISSA THOMAS-YARDE</span> - Melissa, a Caribbean native and a computer programmer, has become an expert on
                                                                     the Everglades preservation in only a matter of weeks. “When I began this journey,
                                                                     I did not expect this capstone project to change me the way it did,” Melissa asserts,
                                                                     and she has become an ardent voice calling for “a commitment that demands global attention
                                                                     to the Everglades — any weak effort will be lost among the tides and seas of growing
                                                                     world problems; we need a worldwide thrust to teach people to appreciate, value and
                                                                     preserve the Everglades for all mankind.” 
                                                                  </p>
                                                                  
                                                                  <p><strong>SPRING 2015 Graduate </strong></p>
                                                                  
                                                                  <h3>&nbsp;</h3>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               
                                                               <div data-old-tag="td">
                                                                  
                                                                  <p><strong>VGD Graduates - SPRING 2016</strong></p>
                                                                  
                                                                  <p><strong>Congratulations To</strong>:<br>Rachel Shuck<br>
                                                                     Alicia Stinnett<br>
                                                                     Laura Marcela Agudelo-Paez<br>
                                                                     Darin Starks<br>
                                                                     Melissa Elliott <br>
                                                                     Pamela Hernandez-Rivera<br>
                                                                     Rashidamonique Marine      
                                                                  </p>
                                                                  
                                                               </div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>            
                                                      
                                                      <h2>&nbsp;</h2>
                                                      
                                                      <h2><strong>  </strong></h2>
                                                      
                                                      <h2>&nbsp;</h2>
                                                      
                                                      
                                                      <blockquote>
                                                         
                                                         
                                                      </blockquote>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/events/meet-our-students.pcf">©</a>
      </div>
   </body>
</html>