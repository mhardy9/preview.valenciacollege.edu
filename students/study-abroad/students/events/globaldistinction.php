<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/events/globaldistinction.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/events/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/events/">Events</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../../exchange/index.html">JEV</a></li>
                                                   
                                                   
                                                   <li><a href="http://international.valenciacollege.edu">ISS/CIE</a></li>
                                                   
                                                   
                                                   <li><a href="../../../../faculty/development/programs/curriculumDevelopment/index.html">Faculty Development</a></li>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <a name="content" id="content"></a>
                                          
                                          <div data-old-tag="table">
                                             
                                             <div data-old-tag="tbody">
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      
                                                      <link href="../../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div data-old-tag="tr">
                                                   
                                                   
                                                   <div data-old-tag="td">
                                                      
                                                      <h2>Global Distinction</h2>
                                                      
                                                      <p><strong>SAVE THE DATE - UPCOMING VGD ORIENTATIONS: </strong></p>
                                                      
                                                      <div data-old-tag="table">
                                                         
                                                         <div data-old-tag="tbody">
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="th">Description</div>
                                                               
                                                               <div data-old-tag="th">Date</div>
                                                               
                                                               <div data-old-tag="th">Time</div>
                                                               
                                                               <div data-old-tag="th">Campus</div>
                                                               
                                                               <div data-old-tag="th">Room</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">VGD Orientation </div>
                                                               
                                                               <div data-old-tag="td">9/5</div>
                                                               
                                                               <div data-old-tag="td">2:30 to 3:45</div>
                                                               
                                                               <div data-old-tag="td">West</div>
                                                               
                                                               <div data-old-tag="td">1-131</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">VGD Orientation </div>
                                                               
                                                               <div data-old-tag="td">9/6</div>
                                                               
                                                               <div data-old-tag="td">2:30 to 3:45</div>
                                                               
                                                               <div data-old-tag="td">West</div>
                                                               
                                                               <div data-old-tag="td">1-131</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">VGD Orientation </div>
                                                               
                                                               <div data-old-tag="td">9/19</div>
                                                               
                                                               <div data-old-tag="td">1-3pm</div>
                                                               
                                                               <div data-old-tag="td">East</div>
                                                               
                                                               <div data-old-tag="td">1-377</div>
                                                               
                                                            </div>
                                                            
                                                            <div data-old-tag="tr">
                                                               
                                                               <div data-old-tag="td">VGD Orientation </div>
                                                               
                                                               <div data-old-tag="td">10/18</div>
                                                               
                                                               <div data-old-tag="td">12-2pm</div>
                                                               
                                                               <div data-old-tag="td">East</div>
                                                               
                                                               <div data-old-tag="td">3-113</div>
                                                               
                                                            </div>
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      <p><a href="https://www.facebook.com/ValenciaGlobalDistinction/">Check out our Global Distinction Facebook</a> 
                                                      </p>
                                                      
                                                      
                                                      <p>Thank you for your interest in <span><a href="documents/VGDPostcard_FINAL.pdf">Valencia College's Global Distinction (VGD)!</a></span> 
                                                      </p>
                                                      
                                                      <p>A global citizen is curious and eager to learn about the dynamics of a growing world
                                                         community. The goal of Valencia's Global Distinction (VGD) is to prepare students
                                                         to live and work in an interdependent and multicultural world, while having the knowledge,
                                                         skills, and attitudes of a competent global citizen. This preparation includes: 
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li><strong>Studying global current and historical events from multiple perspectives</strong></li>
                                                         
                                                         <li><strong>Achieving a greater degree of cross-cultural competency through multidisciplinary
                                                               study</strong></li>
                                                         
                                                         <li><strong>Appreciating diversity in others</strong></li>
                                                         
                                                         <li><strong>Articulating self-awareness from a cross-cultural perspective</strong></li>
                                                         
                                                         <li><strong>Fostering curiosity about the world and different groups of people.<br>
                                                               </strong></li>
                                                         
                                                      </ul>
                                                      
                                                      <p><strong>Make sure you sign up for an orientation - that is your first step in moving forward
                                                            in the program! </strong></p>
                                                      
                                                      <p>In order to successfully earn the Valencia Global Distinction, students must complete
                                                         the following: 
                                                      </p>
                                                      
                                                      <ul>
                                                         
                                                         <li><strong>Valencia's Global Distinction Orientation Seminar</strong></li>
                                                         
                                                         <li><strong>15 credit hours from any of the courses on the approved VGD course list</strong></li>
                                                         
                                                         <li><strong>15 co-curricular hours with a global/intercultural theme OR participate in a study
                                                               abroad, international internship, or international service learning program</strong></li>
                                                         
                                                         <li><strong>Capstone project showcasing the knowledge, skills, and attitudes of a global citizen</strong></li>
                                                         
                                                      </ul>                
                                                      
                                                      <p>All students will receive a medallion, an acknowledgement at graduation, and a Global
                                                         Distinction notation on their official transcript.
                                                      </p>
                                                      
                                                      <p><strong>CLICK THE ENROLL NOW! LINK TO THE LEFT TO GET STARTED. FOR QUESTIONS, CONTACT: </strong></p>
                                                      
                                                      <p>STUDY ABROAD AND GLOBAL EXPERIENCES<br>
                                                         1768 Park Center Drive - Orlando, FL 32835<br>
                                                         Phone: 407-582-3608<br>
                                                         Email: 
                                                         
                                                         
                                                         <a href="mailto:globaldistinction@valenciacollege.edu">globaldistinction@valenciacollege.edu</a> 
                                                      </p>
                                                      
                                                      <h3>Global Studies - Winter Park Campus </h3>
                                                      
                                                      <p>The Global Studies Track is designed to provide highly-motivated students a well-rounded
                                                         general education curriculum with thematically-integrated courses. At Winter Park,
                                                         the focus is on creating opportunities for students to become active, global citizens.
                                                         Students will have the opportunity to Skype with students at Koning Willem I College
                                                         and cap-off their coursework with a study abroad trip to the Netherlands. Students
                                                         completing this track will graduate with both the<a href="../../../../students/service-learning/index.html"> Service Learning</a> and <a href="globaldistinction.html">Valencia Global Distinction</a> recognitions. 
                                                      </p>
                                                      
                                                      
                                                      <blockquote>
                                                         
                                                         
                                                      </blockquote>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/events/globaldistinction.pcf">©</a>
      </div>
   </body>
</html>