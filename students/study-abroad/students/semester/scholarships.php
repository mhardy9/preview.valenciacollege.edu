<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/semester/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/semester/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/semester/">Semester</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Semester Programs - Scholarships &amp; Financial Aid </h2>
                        
                        <ul>
                           
                           <li><a href="#FinancialAid">Financial Aid</a></li>
                           
                           <li>
                              <a href="#Other">Other Scholarship Funding Sources</a> 
                           </li>
                           
                        </ul>                  
                        
                        <p>The SAGE office has scholarship funding from the Valencia Foundation and Student Development
                           to help offset the costs of study abroad. You must submit a separate application for
                           semester study abroad. Awards range from $1000 to $3500 depending upon the number
                           of applicants, the contents of your application, and the amount of scholarship dollars
                           available for the current academic year. 
                           
                        </p>
                        
                        <p>Finding the money to support a semester study abroad program can be challenging for
                           some. The first step in the process is to create a budget. Use the <em><a href="http://valenciacollege.edu/international/studyabroad/students/semester/documents/BudgetWorksheetforStudyAbroad.xlsx">Budget Worksheet for Study Abroad</a></em> form to help you. If you are applying for a SAGE scholarship or plan on using your
                           financial aid, this is a required form.                                    
                           
                        </p>
                        
                        <p><strong>SCHOLARSHIP ELIGIBILITY REQUIREMENTS</strong>
                           
                        </p>
                        
                        <ul>
                           
                           <li>Have a minimum 2.5 GPA at the time of applying to the program.</li>
                           
                           <li>Submit a <em>SAGE Scholarship Application Form</em> and supporting documents (see <a href="apply.html">How to Apply</a>). 
                           </li>
                           
                           <li>Submit proof of program acceptance. </li>
                           
                           <li>Be a degree-seeking students at Valencia College:
                              
                              <ul>
                                 
                                 <li> you can get a scholarship after graduation as long as the program runs by the following
                                    semester so, for example, students who graduate in the fall term can get a scholarship
                                    for the spring term but not the summer term
                                 </li>
                                 
                                 <li>you do not qualify for a scholarship if you are a transient or audit student </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Have not received a study abroad scholarship within the current academic year</li>
                           
                           <li>Completed at least one semester with Valencia within the last year</li>
                           
                           <li>Submit a thank you card to your donor addressed to "My Esteemed Donor"</li>
                           
                           <li>Complete the online course evaluation at the end of the program </li>
                           
                           <li>
                              <strong><a href="http://net4.valenciacollege.edu/forms/international/studyabroad/register_request.cfm" target="_blank">Register Your Trip</a></strong> with the SAGE Office once you have your flight, passport, and living arrangements
                              confirmed. <span><strong>IMPORTANT!</strong></span> 
                           </li>
                           
                           <li>Be willing to participate in a SAGE event to help promote study abroad to future students.
                              
                           </li>
                           
                        </ul>                  
                        
                        <p><strong>FINANCIAL AID<a name="FinancialAid" id="FinancialAid"></a> </strong></p>
                        
                        <p> Recent changes in federal law have made it possible for students to use some or all
                           of their federal financial aid for semester study abroad. Full-time, Valencia students
                           who are eligible for <a href="../../../../finaid/gettingstarted/index.html">Financial Aid</a> can use these funds for study abroad programs that award academic credit and grades
                           counted towards your cumulative GPA for study.&nbsp; Eligibility is determined by the type
                           of study abroad program you attend, as well as the type of financial aid you receive.
                           Only students earning a degree at Valencia are eligible. It is important that you
                           begin this process at least one year prior to your program start date. Any program
                           for which you will not receive transfer credits or grades is not eligible for any
                           financial aid funds. Financial aid can be used to support summer study, as long as
                           you are enrolled for academic credit and you meet all other requirements. 
                        </p>
                        
                        <p>  For semester programs, check with your academic advisor to ensure that the courses
                           you are going to take will transfer back to Valencia. You will need to fill out the
                           <a href="http://valenciacollege.edu/international/studyabroad/students/semester/documents/ConsortiumAgreementFormforStudyAbroadv2.pdf">Course Pre-Approval and Consortium Agreement Form for Study Abroad </a> before you go. These forms will be required when you meet with the Financial Aid
                           Services Department.
                        </p>
                        
                        <p> NOTE: You cannot use financial aid for study abroad after you graduate from Valencia!
                           It is important to meet with a Financial Aid Services advisor prior to accepting any
                           scholarship money to determine if it will affect your overall financial aid package.
                           Take your budget and the course approval forms to your Financial Aid Services advisor
                           for him or her to review to determine your eligibility. Eligibility is determined
                           by the type of program you attend, as well as the type of financial aid you receive.&nbsp;
                           
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong> Financial Aid Funding Source </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong> Valencia Study Abroad </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong> Affiliate Study Abroad </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Federal Pell Grant </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Federal SEOG </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Federal Work-Study </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Federal Stafford Loans </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Federal PLUS Loans </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Valencia Student Development Funding </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Valencia Foundation </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Valencia SAGE Grants </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://www.iie.org/en/Programs/Gilman-Scholarship-Program">Gilman International Scholarship Program </a></div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://www.borenawards.org/">David L. Boren Scholarships </a></div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Florida Student Assistance Grant </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="http://www.floridastudentfinancialaid.org/ssfad/bf/">Florida Bright Futures</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>YES</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>NO</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p> For more information, check out the following resources: </p>
                        
                        <ul>
                           
                           <li><a href="http://www.finaid.org/">FinAid: The SmartStudent Guide to Financial Aid</a></li>
                           
                           <li>
                              <a href="http://nafsa.org/students.sec/financial_aid_for_study">NAFSA</a><a href="http://nafsa.org/students.sec/financial_aid_for_study"> - Financial Aid for Study Abroad: An Undergraduate Student's Resource</a>
                              
                           </li>
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/semester/documents/FinancialAidforStudyAbroadStudents.pptx">Financial Aid for Study Abroad Students - SAGE Powerpoint Presentation </a></li>
                           
                        </ul>
                        
                        <p><strong>OTHER SCHOLARSHIP FUNDING SOURCES<a name="Other" id="Other"></a> </strong></p>
                        
                        <p>The following is a list of resources to help fund your study abroad program: 
                           
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Generation Study Abroad </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The <a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Institute of International Education</a> has just announced the launch of <strong>Generation Study Abroad</strong>, a five-years campaign to double the number of American students who study abroad
                                       by the end of the decade.
                                    </p>
                                    
                                    <p>AIFS is the first study abroad organization to become a committed partner of Generation
                                       Study Abroad. We are now offering <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">Generation Study Abroad Scholarship for semester and summer programs</a></p>
                                    
                                    <p>Every student should have the opportunity to study abroad. Our goal is to have 600,000
                                       U.S. students studying abroad in credit and non-credit programs. Over the next 5 years,
                                       <strong>Generation Study Abroad</strong> will reach out to educators at all levels and stakeholders in the public and private
                                       sectors to encourage purposeful, innovative action to get more people interested to
                                       undertake an international experience. 
                                    </p>
                                    
                                    <p><strong>Why? Because international experience is one of the most important components of a
                                          21st. century education. Read more about the Generation Study Abroad challenge </strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/gilman" target="_blank">Benjamin A. Gilman International Scholarship Program</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The Gilman Scholarship offers awards up to $5,000 to study abroad to U.S. citizen
                                       undergraduates receiving federal <strong>Pell Grant</strong> funding at 2-year or 4-year colleges or universities. Students studying a Critical
                                       Need Language may receive a $3,000 supplement for a total possible award of $8,000.
                                    </p>
                                    
                                    <p><strong>NOTE: For your application and essay, please be sure to include the following details
                                          IF they apply to you.</strong></p>
                                    
                                    <ul>
                                       
                                       <li>You're a current/past student at community college</li>
                                       
                                       <li>You're studying in a field underrepresented in study abroad (sciences and engineering)</li>
                                       
                                       <li> You're a first generation college student</li>
                                       
                                       <li>You're a student of diverse ethnic backgrounds</li>
                                       
                                       <li>You're a student with disabilities </li>
                                       
                                       <li>You're traveling to a country outside of Western Europe, Australia or New Zealand
                                          
                                       </li>
                                       
                                       <li>You intend to study a critical need language during your study abroad experience
                                          
                                          <ul>
                                             
                                             <li>Arabic (all dialects)</li>
                                             
                                             <li>Chinese (all dialects)</li>
                                             
                                             <li>Bahasa Indonesia</li>
                                             
                                             <li>Japanese</li>
                                             
                                             <li>Turkic (Azerbaijani, Kazakh, Kyrgz, Turkish, Turkmen, Uzbek)</li>
                                             
                                             <li>Persian (Farsi, Dari, Kurdish, Pashto, Tajiki)</li>
                                             
                                             <li>Indic (Hindi, Urdu, Nepali, Sinhala, Bengali, Punjabi, Marathi, Gujarati, Sindhi)</li>
                                             
                                             <li>Korean</li>
                                             
                                             <li>Russian</li>
                                             
                                             <li>Swahili</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.borenawards.org" target="_blank">Boren Scholarships</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Boren Scholarships offer&nbsp;unique opportunities for U.S. undergraduates to study abroad
                                    for a semester or a year in&nbsp;world regions critical to U.S. interests (including Africa,
                                    Asia, Eastern Europe, Eurasia, Latin American &amp; the Caribbean, and the Middle East).
                                    In exchange for scholarship funding, all Boren Scholars must agree to work for the
                                    Federal Government in a position with national security responsibilities. Refer to
                                    "Boren Scholarship Basics" for complete details. Funding varies by length of program
                                    up to $20,000. Read the <a href="http://www.borenawards.org/boren_scholarship/faq">Boren FAQs</a> for more information. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">AIFS Study Abroad</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">Each year, more than <strong>$600,000 in scholarships</strong>, grants and financial support is awarded to deserving students &amp; institutions. Funds
                                    are available for both summer and semester programs. Please check the website for
                                    more details and how to apply. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p><a href="http://www.myfloridaprepaid.com/" target="_blank">Florida Prepaid College</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">If you have a Florida Prepaid College program, you can use those funds to pay for
                                    the regular tuition rate for a study abroad course. These funds cannot be used for
                                    any additional program costs or lab fees.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.fundforeducationabroad.com/applicants/" target="_blank">Fund for Education Abroad</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> The FEA is committed to increasing the opportunities for dedicated American students
                                    to participate in high-quality, rigorous education abroad programs by reducing financial
                                    restrictions through the provision of grants and scholarships. Participants must be
                                    studying at least 4 weeks in country. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://us.fulbrightonline.org" target="_blank">U. S. Fulbright Student Program</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Sponsored by the U.S. Department of State, Bureau of Educational and Cultural Affairs.
                                    Fulbright is the largest U.S. international exchange program offering opportunities
                                    for students, scholars and professionals to undertake international graduate study,
                                    advanced research, university teaching, and teaching in elementary and secondary schools
                                    worldwide. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="http://www.ccisabroad.org/" target="_blank">College Consortium for International Studies</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">The College Consortium for International Studies offers scholarships each term to
                                    students at CCIS member institutions. Valenica maintains a membership to CCIS. If
                                    you are participating in a CCIS Program, contact the Study Abroad Office for CCIS
                                    Scholarship information. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.caorc.org/" target="_blank">Critical Language Scholarships for Intensive Summer Institutes</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Scholarships for U.S. citizen undergraduate, master's and Ph.D. students to participate
                                    in beginning, intermediate and advanced level intensive summer language programs at
                                    American Overseas Research Centers. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.floridastudentfinancialaid.org/SSFAD/home/uamain.htm" target="_blank">Florida Student Scholarship and Grant Programs</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The Office of Student Financial Assistance (OSFA) State Programs, within the Florida
                                    Department of Education, administers a variety of postsecondary educational state-funded
                                    grants and scholarships, and provides information to students, parents, and high school
                                    and postsecondary professionals.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/programs/Freeman-Asia" target="_blank">Freeman-Asia: Awards for Study in Asia</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Open to undergraduate students to study in East and Southeast Asia. Fall, Spring and
                                    Summer cycles. Awards amounts vary from $3,000 for summer term $5,000 for semester
                                    and $7,000 for academic year programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <a href="http://www.ibs-sp.com.br/index.php?lang=EN" target="_blank">IBS-Americas (International Business School)</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">Open to all students, second year and up or graduate students to apply for their programs
                                    and scholarships (<strong>$4,000</strong>). International Business School IBS-Americas has been offering international programs
                                    to students, young professionals and executives since 2003.  No background in Portuguese
                                    language is required, all courses are taught exclusively in English. This program
                                    will offer participants the opportunity for intercultural integration, international
                                    engagement and managerial development. Held in Brazil, in the United States and in
                                    Europe, their short-term programs are offered in January and  July, with full-time
                                    classes and activities, allowing the participants to have a deep contact with knowledge
                                    and the business real situation of the chosen country without needing to break professional
                                    and academic bonds. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadfunding.org/" target="_blank">Institute of International Education </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">With hundreds of scholarship and grant listings, IIE's online directory Study Abroad
                                    Funding, is among the most comprehensive directories for U.S. students to find funding
                                    for studying abroad. It contains thousands of study abroad listings offered by U.S.
                                    and foreign universities and study abroad providers, along with key information on
                                    funding opportunities for study abroad experiences. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.internationalscholarships.com/1271/Hispanic-Study-Abroad-Scholars-%28hsas%29-Scholarship" target="_blank">Hispanic Study Abroad Scholars (HSAS)</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong> </strong>Over $8 Million in scholarships for Hispanic study abroad. The HSAS Scholarship offers
                                    $1,000 per semester to students whose college or university is a member of the Hispanic
                                    Association of Colleges and Universities (HACU) and are attending Global Learning
                                    Semesters programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://valenciacollege.edu/international/studyabroad/students/semester/documents/facts_about_Rotary_scholarships_en.pdf" target="_blank">Rotary International</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The Rotary Club offers ambassadorial scholarships for students and grants for university
                                    professors. These programs can change the lives of those who participate. Through
                                    these programs, participants can travel on cultural exchanges, or help a community
                                    through a service project. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="../../../../finaid/Scholarship_bulletin.html#ScholarshipSearch" target="_blank">Scholarship Bulletin Board</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">This Scholarship Bulletin Board offers students information on available scholarships.
                                    Scholarship advertisements include application requirements, locations of applications,
                                    and specific deadlines. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.sit.edu/studyabroad/scholarships" target="_blank">SIT Study Abroad Financial Aid and Scholarships</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The <strong>S</strong>chool for <strong>I</strong>nternational <strong>T</strong>raining and World Learning offer a variety of study abroad scholarships ranging from
                                    $500 to $5,000. Awards are based on need, merit, and the inclusion of students from
                                    a variety of institutions, from across the U.S. and from around the world. Most scholarship
                                    funds are available for semester or summer programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p><a href="http://valenciacollege.edu/international/studyabroad/students/semester/documents/ScholarshipstoJapan.doc" target="_blank">Scholarships to Japan </a><br>
                                       (Word document)
                                    </p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://www.aatj.org/studyabroad/japan-bridging-scholarships" target="_blank">http://www.aatj.org/studyabroad/japan-bridging-scholarships</a></p>
                                    
                                    <p><a href="http://www.aatj.org/studyabroad/morgan-stanley" target="_blank">http://www.aatj.org/studyabroad/morgan-stanley</a><br>
                                       <br>
                                       <a href="http://www.hashi.org/scholarshiphp.html" target="_blank">http://www.hashi.org/scholarshiphp.html</a> <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadfunding.org/" target="_blank">IIE Funding Study Abroad </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> This directory features detailed descriptions of hundreds of study abroad scholarships,
                                    fellowships, grants, and paid internships for U.S. undergraduate, graduate and post-graduate
                                    students, and professionals. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p><a href="http://www.nafsa.org/students.sec/financial_aid_for_study/" target="_blank">NAFSA: Association of International Educators</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td"> Financial aid for undergraduate study abroad consists mainly of federal grants and
                                    federal and private loans. However, scholarship money is also available from organizations
                                    and sponsoring companies. You likely have many questions about the cost of studying
                                    abroad and about how you can fund your abroad experience. Be sure to speak with your
                                    campus financial aid officer and study abroad adviser to learn more about specific
                                    funding options available at—and required procedures for—your college or university.
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th"><a href="http://tc-america.org/scholarships/minority.htm" target="_blank">Turkish Coalition of American - TCA </a></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The Turkish Coalition of American (TCA), a Washington, DC-based nonprofit that aims
                                       to foster a better understanding of U.S.-Turkey relations and Turkish Americans here
                                       in the U.S.
                                    </p>
                                    
                                    <p>TCA has the funding for 100 scholarships per year for African American, Armenian American,
                                       Bosnian American, Filipino American, Hispanic American, Macedonian American, and Native
                                       American students who have been accepted to a study abroad or language program in
                                       Turkey, the Turkish Republic of Northern Cyprus (TRNC), and Bosnia and Herzegovina
                                       (BiH). Eligible students will receive between $500 and $2,000.
                                    </p>
                                    
                                    <p>Please visit their website for more information.</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/semester/scholarships.pcf">©</a>
      </div>
   </body>
</html>