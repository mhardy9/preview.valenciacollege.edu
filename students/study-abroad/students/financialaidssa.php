<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/financialaidssa.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>Using Financial Aid</h2>
                        
                        
                        
                        
                        <p><strong>Recent changes in federal law have made it possible for students to use some or all
                              of their federal financial aid for BOTH short-term and semester study abroad. </strong></p>
                        
                        <p>Full-time, Valencia students who are eligible for <a href="../../../finaid/gettingstarted/index.html">Financial Aid</a> can use these funds for study abroad programs that award academic credit and grades
                           counted towards your cumulative GPA for study.&nbsp; Eligibility is determined by the type
                           of study abroad program you attend, as well as the type of financial aid you receive.
                           Only students earning a degree at Valencia are eligible. It is important that you
                           begin this process at least one year prior to your program start date. Any program
                           for which you will not receive transfer credits or grades is not eligible for any
                           financial aid funds. Financial aid can be used to support summer study, as long as
                           you are enrolled for academic credit and you meet all other requirements. 
                        </p>
                        
                        <p>The first step in the process is to determine which study abroad program you will
                           participate in and what the costs are. Put together a <strong><a href="http://valenciacollege.edu/international/studyabroad/students/documents/BudgetWorksheetforStudyAbroad.xlsx">Budget </a></strong><a href="http://valenciacollege.edu/international/studyabroad/students/documents/BudgetWorksheetforStudyAbroad.xlsx"><strong>Worksheet</strong></a> to ensure you have sufficient funds for the study abroad program.
                        </p>
                        
                        <p><strong>SHORT-TERM STUDY ABROAD<br>
                              </strong>Student should verify with an academic advisor and a financial aid advisor to determine
                           how participation in this study abroad program will affect their overall financial
                           aid package. Also, courses must fit within the student's education plan in order for
                           financial aid to cover it.
                        </p>
                        
                        <ul>
                           
                           <li>SAGE scholarships will be authorized prior to the program balance due date so students
                              know what their balance is.
                           </li>
                           
                           <li>Financial aid is  disbursed two weeks after the start of the LAST class of the semester.</li>
                           
                        </ul>            
                        
                        <p><strong>SEMESTER STUDY ABROAD </strong><br>
                           For semester programs, check with your academic dean to ensure that the courses you
                           are going to take will transfer back to Valencia. You will need to fill out the <strong><a href="http://valenciacollege.edu/international/studyabroad/students/documents/CoursePre-ApprandConsortforSemesterAbroad.docx">Course Pre-Approval and Consortium Agreement Form for Study Abroad</a> </strong> before you go. These forms will be required when you meet with the Financial Aid
                           Services Department. NOTE: You must have at least one semester of coursework behind
                           you, and you must be registered as a Valencia student in order to participate. You
                           cannot attend a program AFTER your graduation date using financial aid or scholarship
                           funds. <strong><span>NOTE: It is important to meet with a Financial Aid Services advisor prior to accepting
                                 any scholarship money to determine if it will affect your overall financial aid package.
                                 </span></strong><br>
                           <br>
                           Take your budget and the course approval forms to your Financial Aid Services advisor
                           for him or her to review to determine your eligibility. Eligibility is determined
                           by the type of program you attend, as well as the type of financial aid you receive.&nbsp;
                           <br>
                           
                        </p>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Financial Aid Funding Source </strong></div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong> Valencia Study Abroad </strong></div>
                                    </div>
                                    
                                    <div data-old-tag="td">
                                       <div><strong> Affiliate Study Abroad </strong></div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Federal Pell Grant </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Federal SEOG </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Federal Work-Study </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Federal Stafford Loans </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Federal PLUS Loans </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Valencia Student Development Funding </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Valencia Foundation </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Valencia SAGE Grants </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="http://www.iie.org/en/Programs/Gilman-Scholarship-Program" target="_blank">Gilman International Scholarship Program </a></div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="http://www.borenawards.org/" target="_blank">David L. Boren Scholarships </a></div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> Florida Student Assistance Grant </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"> <a href="http://www.floridastudentfinancialaid.org/ssfad/bf/" target="_blank">Florida Bright Futures</a> 
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>YES</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="td"> 
                                       <div>NO</div>
                                       
                                    </div>  
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p> For more information, check out the following resources: </p>
                        
                        <ul>
                           
                           <li><a href="http://www.finaid.org/" target="_blank">FinAid: The SmartStudent Guide to Financial Aid</a></li>
                           
                           <li>
                              <a href="http://nafsa.org/students.sec/financial_aid_for_study" target="_blank">NAFSA</a><a href="http://nafsa.org/students.sec/financial_aid_for_study"> - Financial Aid for Study Abroad: An Undergraduate Student's Resource</a>
                              
                           </li>
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/FinancialAidforStudyAbroadStudents.pptx">Financial Aid for Study Abroad Students - SAGE Powerpoint Presentation </a></li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/financialaidssa.pcf">©</a>
      </div>
   </body>
</html>