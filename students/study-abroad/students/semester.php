<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/semester.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <h2>Semester Programs</h2>
                        
                        <p> The process to research, plan, and prepare for a semester study abroad program is
                           a long one, so we recommend that you start at least one year prior to your program
                           start date and no less than 6 months prior. 
                        </p>
                        
                        <p><strong>How to Apply:</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>            </strong>Contact the SAGE office at 407-582-3188 or <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>. Let us know what your plans are and be sure to check in along the way. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>REQUIRED! </strong>Print out and follow the instructions on the <a href="http://valenciacollege.edu/international/studyabroad/students/documents/SemesterStudyAbroadPlannerv5.pdf" target="_blank">Semester Study Abroad Planner</a> and use the checklist to help you navigate this process.<br>
                              <br>
                              
                           </li>
                           
                           <li>Search for a <a href="semesterprograms.html">program provider</a>.
                           </li>
                           
                        </ul>            
                        
                        <p><span>IT IS VERY IMPORTANT THAT YOU SELECT A PROGRAM THAT WILL PROVIDE YOU WITH A TRANSCRIPT
                              ISSUED BY A U.S. INSTITUTION TO OBTAIN TRANSFER CREDIT. IF YOU DIRECTLY ENROLL IN
                              AN OVERSEAS INSTITUTION THAT HAS NO U.S. AFFILIATION, YOU CANNOT USE YOUR FINANCIAL
                              AID, YOU CANNOT GET A SAGE SCHOLARSHIP, YOU WILL NEED TO PAY FOR A NACES TRANSCRIPT
                              EVALUATION UPON PROGRAM COMPLETION, AND THERE IS NO GUARANTEE THAT THE CREDITS WILL
                              TRANSFER BACK TO VALENCIA. </span><br>
                           
                        </p>
                        
                        <ul>
                           
                           <li>Print out the following forms which have all been referenced in the <em>Semester Study Abroad Planner</em> above.<br>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/international/studyabroad/students/documents/04BudgetWorksheetforStudyAbroad.pdf">Budget Worksheet for Study Abroad</a> 
                                 </li>
                                 
                                 <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/ConsortiumAgreementFormforStudyAbroadv2.pdf"> Consortium Agreement Form for Study Abroad</a></li>
                                 
                                 <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/SAGEScholarshipInstructionsandApplicationForm.doc">SAGE Scholarship Instructions and Application Form</a></li>
                                 
                                 <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/05RecommendedPackingList.pdf">Recommended Packing List</a></li>
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/international/studyabroad/students/documents/05PropertyDocumentForm.pdf">Property Document Form</a><br>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>SAGE Scholarships: </strong>The SAGE office has  semester study abroad scholarships for up to $4000 (depending
                           upon the contents of your application) for which  you can apply for the 2013-2014
                           academic year. You do not have to be accepted into a program at the time of applying,
                           but you must provide proof of acceptance prior to receiving the scholarship. Scholarships
                           will be issued to your student account. In order to apply, you must:
                        </p>
                        
                        <ul>
                           
                           <li>Be a degree-seeking student at Valencia:
                              
                              <ul>
                                 
                                 <li>You cannot go on short-term study abroad with financial aid or a scholarship if you
                                    are already graduated.
                                 </li>
                                 
                                 <li>You do not qualify for a scholarship if you are a transient or audit student. </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Have a minimum 2.5 GPA at the time of applying to the program.</li>
                           
                           <li>Submit a <em>SAGE Scholarship Application Form</em> and supporting documents (above).  
                           </li>
                           
                           <li>Submit proof of program acceptance. </li>
                           
                           <li>Have completed at least one semester with Valencia prior to travel within the last
                              year.
                           </li>
                           
                           <li>Receive only one scholarship for study abroad within the academic year.</li>
                           
                           <li>Meet all course requirements (attend all pre- and post-trip meetings, participate
                              in all in-country activities, turn in all academic assignments, obtain a grade of
                              a C or better).
                           </li>
                           
                           <li>
                              <strong><a href="http://net4.valenciacollege.edu/forms/international/studyabroad/register_request.cfm" target="_blank">Register Your Trip</a></strong> with the SAGE Office. <span>IMPORTANT!</span> 
                           </li>
                           
                           <li>Submit a thank you card to the donor addressed "To My Esteemed Donor."</li>
                           
                           <li>Complete the online course evaluation within two weeks upon your return.</li>
                           
                           <li>Be willing to participate in a SAGE event to help promote study abroad to future students.
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Important Websites: </strong>The following are some important website links for you. They are all referenced in
                           the <em>Semester Study Abroad Planner</em> above.
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.studyabroad.com/scholarships.aspx" target="_blank">Study Abroad Scholarships</a>                
                           </li>
                           
                           <li>
                              <a href="http://www.usps.com/passport/" target="_blank">Passport</a> (your passport should be valid for up to 90 days after your program end date) 
                           </li>
                           
                           <li>
                              <a href="http://www.visahq.com/" target="_blank">Visa</a> (verify if you need a visa and apply 45-120 days prior) 
                           </li>
                           
                           <li><a href="http://www.cmiinsurance.com" target="_blank">Medical/trip insurance</a></li>
                           
                           <li><a href="http://www.isic.org/" target="_blank">International Student ID Card</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/semester.pcf">©</a>
      </div>
   </body>
</html>