<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/semesterprograms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Semester Study Abroad Programs<a name="TOP" id="TOP"></a> 
                        </h2>
                        
                        <ul>
                           
                           <li>
                              <a href="#Florida">Florida Educational Institutions</a> 
                           </li>
                           
                           <li><a href="#Programs">Study Abroad Programs and Agencies</a></li>
                           
                           <li><a href="#Engines">Study Abroad Program Search Engines                </a></li>
                           
                           <li>
                              <a href="#Service">Service Learning and Volunteer Programs</a> 
                           </li>
                           
                        </ul>            
                        
                        <p><strong>GENERAL  INFORMATION</strong></p>
                        
                        <ul>
                           
                           <li>The SAGE office has up to $4000 scholarships available for the 2012-2013 academic
                              year. Get your application in early! 
                           </li>
                           
                           <li>
                              <a href="http://www.iie.org/Programs/Gilman-Scholarship-Program/Deadlines-and-Timeline" target="_blank">                Gilman Scholarship Application Deadline</a>: Fall - March / Spring - October
                           </li>
                           
                           <li>
                              <a href="http://www.borenawards.org/boren_scholarship/how_apply.html" target="_blank"> Boren Application Deadline</a> for next academic year: February (SAGE office deadline is January 31) 
                           </li>
                           
                        </ul>            
                        
                        <p>You can earn college credit at an institution in another country for a semester. Since
                           Valencia does not offer semester programs in other countries, you would need to enroll
                           in a program offered by another university/college or organization and get transfer
                           credit.  You must be a full-time Valencia student and have completed a semester of
                           coursework at Valencia in order to participate. <strong>You must use the Semester Study Abroad Planner and contact the SAGE office to get
                              started. See the <a href="http://valenciacc.edu/international/studyabroad/students/apply.cfm" target="_blank">How to Apply</a> page and click the Semester tab to obtain this form. </strong></p>
                        
                        <p>IT IS VERY IMPORTANT THAT YOU SELECT A PROGRAM THAT WILL PROVIDE YOU WITH A TRANSCRIPT
                           ISSUED BY  A U.S. INSTITUTION TO OBTAIN TRANSFER CREDIT. IF YOU DIRECTLY ENROLL IN
                           AN OVERSEAS INSTITUTION THAT HAS NO U.S. AFFILIATION, YOU CANNOT USE YOUR FINANCIAL
                           AID, YOU CANNOT GET A SAGE SCHOLARSHIP, YOU WILL NEED TO PAY FOR A NACES TRANSCRIPT
                           EVALUATION UPON PROGRAM COMPLETION, AND THERE IS NO GUARANTEE THAT THE CREDITS WILL
                           TRANSFER BACK TO VALENCIA. 
                        </p>
                        
                        <p>Each of the program providers listed below has different requirements so check closely
                           to see that you qualify before applying.&nbsp; We ask that you research the organizations
                           to find a program that will fit your needs as far as coursework, budget, and most
                           importantly, a country that you would enjoy visiting.&nbsp;
                        </p>
                        
                        <p>Check out these useful "How To" videos produced by one of our study abroad students:
                           
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.youtube.com/watch?v=SivWdWqQV-c&amp;list=HL1343586986&amp;feature=mh_lol" target="_blank">How to Study Abroad - Step 1</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.youtube.com/watch?v=pBQW0PCWops">How to Study Abroad - Step 2</a> 
                           </li>
                           
                        </ul>            
                        
                        <p><em>Please be sure to check with the SAGE office to discuss the course credits you will
                              need to transfer back to Valencia. </em></p>
                        
                        <p><strong>AFFILIATE PARTNERS (additional scholarship funds available)</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.keiabroad.org" target="_blank">KEI: Knowledge Exchange Institute</a></li>
                           
                        </ul>            
                        
                        <p><strong>COUNTRY-SPECIFIC PROGRAMS </strong></p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.studyinbuenosaires.edu.ar/english/abroad.php" target="_blank">ARGENTINA: Spanish Language, Humanities, Business, Agribusiness, and More</a> 
                           </li>
                           
                           <li><a href="http://www.spanishstudies.org/" target="_blank">ARGENTINA: Spanish Studies in Cordoba </a></li>
                           
                           <li><a href="http://www.bli.ca/" target="_blank">CANADA: French Immersion in Quebec City</a></li>
                           
                           <li><a href="http://www.cosi.co.cr/index.php" target="_blank">COSTA RICA: Spanish Institue in San Jose </a></li>
                           
                           <li><a href="http://www.abroad.ufic.ufl.edu/index.cfm?Program_Type_ID=O&amp;Program_Name=&amp;pt=%7F&amp;pi=%7F&amp;pc=China%7F&amp;pr=%7F&amp;FuseAction=Programs.SearchResults&amp;SimpleSearch=1" target="_blank">CHINA: Topics Vary (through the University of Florida) </a></li>
                           
                           <li><a href="http://www.studyintaiwan.org/en/index.html" target="_blank">CHINA: Study in Taiwan </a></li>
                           
                           <li><a href="http://english.web.ncku.edu.tw/files/13-1005-75521.php" target="_blank">CHINA: Engineering, Industry, Space, Research, and More </a></li>
                           
                           <li>
                              <a href="http://global.usf.edu/studyabroad/cambridge/" target="_blank">ENGLAND: USF - Cambridge University Summer School</a> 
                           </li>
                           
                           <li><a href="http://www.actilangue.com/AccueilEN/EN_learn_french_france.html" target="_blank">FRANCE: French Learning School </a></li>
                           
                           <li><a href="http://www.iaufrance.org/" target="_blank">FRANCE: Study Abroad in the South of France </a></li>
                           
                           <li><a href="http://www.daad.org/" target="_blank">GERMANY: Academic Exchange Service </a></li>
                           
                           <li>
                              <a href="http://www.arcadia.edu/abroad/default.aspx?id=6725" target="_blank">GREECE: Greek Life and Culture</a> 
                           </li>
                           
                           <li><a href="http://www.foylelanguageschool.com/" target="_blank">IRELAND: Irish Cultural and Vocational School </a></li>
                           
                           <li><a href="http://www.acd.ie/" target="_blank">IRELAND: Discover Dublin </a></li>
                           
                           <li><a href="http://www.arcadia.edu/abroad/default.aspx?id=6731" target="_blank">ITALY: Italian Language and Culture </a></li>
                           
                           <li><a href="http://www.johncabot.edu/" target="_blank">ITALY: American University in the heart of Rome, Italy </a></li>
                           
                           <li><a href="http://www.ldminstitute.com/en/home" target="_blank">ITALY: Florence, Rome, and Tuscany </a></li>
                           
                           <li><a href="http://www.tuj.ac.jp/newsite/main/undergrad/about_tuj/index.html" target="_blank">JAPAN: Temple University Japan Campus</a></li>
                           
                           <li><a href="http://www.sophia.ac.jp/eng/admissions/undergraduate_p" target="_blank">JAPAN: Sophia University </a></li>
                           
                           <li><a href="http://www.icomexico.com" target="_blank">MEXICO: Spanish Language School </a></li>
                           
                           <li><a href="http://www.amideast.org/" target="_blank">MIDDLE EAST: Egypt, Jorden, Morocco, Tunisia </a></li>
                           
                           <li>
                              <a href="http://www.ccisabroad.org/program.php?link=namibia_windhoek" target="New Window">NAMIBIA: Service Learning and More in South Africa</a> 
                           </li>
                           
                           <li><a href="http://www.broward.edu/internationalEducation/InternationalEducation/peru/page27628.html" target="_blank">PERU: Discover Lima, Peru</a></li>
                           
                           <li>
                              <a href="http://www.barcelonasae.com/" target="_blank">SPAIN: Discover Barcelona, Spain</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.ccisabroad.org/summspain_semester.html" target="_blank">SPAIN: Discover Seville, Spain</a> 
                           </li>
                           
                           <li><a href="http://www.spanishstudies.org/" target="_blank">SPAIN: Spanish Studies in Seville or Alcante </a></li>
                           
                           <li>
                              <a href="http://www.spainstudyabroad.com/" target="_blank">SPAIN: Alicante, Barcelona, Cadiz, Madrid, San Sebastian, and Sevilla</a> 
                           </li>
                           
                        </ul>            
                        
                        <p><br>
                           <strong>FLORIDA EDUCATIONAL INSTITUTIONS <a name="Florida" id="Florida"></a></strong></p>
                        
                        <p><strong>The <em>Florida Consortium for International Education</em> now has a website for member schools to submit their study abroad programs that are
                              open to transient students. </strong><strong>Check out the all new <a href="http://fcie.org/?s=Search...&amp;property-search-submit=Search" target="_blank">FCIE Program Finder</a> for details!</strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.broward.edu/internationalEducation/InternationalEducation/StudyAbroad/page2487.html" target="_blank">Broward College</a></li>
                           
                           <li><a href="http://www.famu.edu/index.cfm?oied&amp;StudyAbroadPrograms" target="_blank">Florida A&amp;M University </a></li>
                           
                           <li><a href="http://www.fau.edu/goabroad/" target="_blank">Florida Atlantic University </a></li>
                           
                           <li>
                              <a href="http://www.international.fsu.edu/" target="_blank">Florida State University</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.seminolestate.edu/internationalstudies/study-abroad/seminole-state-travel-programs.php" target="_blank">Seminole State College</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.spcollege.edu/studyabroad/" target="_blank">St. Petersburg College</a> 
                           </li>
                           
                           <li>
                              <a href="http://studyabroad.ucf.edu/" target="_blank">University of Central Florida</a>
                              
                              <ul>
                                 
                                 <li><a href="http://www.cecs.ucf.edu/minors/internationalengineering/where" target="_blank">International Engineering </a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="https://www.abroad.ufic.ufl.edu/" target="_blank">University of Florida</a></li>
                           
                           <li>
                              <a href="http://www.unf.edu/intlctr" target="_blank">University of North Florida</a> 
                           </li>
                           
                           <li><a href="http://global.usf.edu/educationabroad/" target="_blank">University of South Florida</a></li>
                           
                           <li>
                              <a href="http://uwf.edu/studyabroad/" target="_blank">University of West Florida</a> 
                           </li>
                           
                        </ul>
                        
                        <p><strong><br>
                              STUDY ABROAD PROGRAMS AND AGENTS<a name="Programs" id="Programs"></a></strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.antioch.edu/aea/" target="_blank">Antioch University </a></li>
                           
                           <li>
                              <a href="http://www.aifsabroad.com/?source=AIFS" target="_blank">AIFS: American Institute of Foreign Study</a> 
                           </li>
                           
                           <li><a href="http://www.amerispan.com/" target="_blank">Amerispan</a></li>
                           
                           <li><a href="http://www.academicstudies.com" target="_blank">ASA: Acedemic Studies Abroad</a></li>
                           
                           <li><a href="http://www.athenaabroad.com" target="_blank">Athena Study Abroad</a></li>
                           
                           <li><a href="http://www.ice.cam.ac.uk/intsummer" target="_blank">Cambridge University </a></li>
                           
                           <li>
                              <a href="http://www.capa.org/" target="_blank">CAPA International Education</a> 
                           </li>
                           
                           <li><a href="http://ccid.kirkwood.cc.ia.us/" target="_blank">CCID: Community Colleges for International Developtment</a></li>
                           
                           <li>
                              <a href="http://www.ccisabroad.org/" target="_blank">CCIS: College Consortium for International Studies</a> 
                           </li>
                           
                           <li><a href="http://www.gowithcea.com/explore_programs.html" target="_blank">CEA Global Education </a></li>
                           
                           <li><a href="http://www.cepa-europe.com" target="_blank">CEPA Europe </a></li>
                           
                           <li>
                              <a href="http://ciee.org/study.aspx" target="_blank">CIEE: Council on International Educational Exchange</a> 
                           </li>
                           
                           <li><a href="http://www.studyabroad-cis.com/index.php" target="_blank">CIS: Center for International Studies</a></li>
                           
                           <li><a href="http://www.clic.es/" target="_blank">CLIC: Centre for Languages and Intercultural Exchange</a></li>
                           
                           <li><a href="http://www.intercoined.org" target="_blank">Coined: Spanish Courses </a></li>
                           
                           <li><a href="http://www.csi.cuny.edu/international/study_abroad.html" target="_blank">CSI: Center for International Service</a></li>
                           
                           <li><a href="http://www.cupa-paris.org" target="_blank">CUPA: Center for University Programs Abroad </a></li>
                           
                           <li><a href="http://www.diversityabroad.com/" target="_blank">Diversityabroad</a></li>
                           
                           <li><a href="http://www.donquijote.org" target="_blank">Don Quijote: Spanish Studies in Spain and Latin America </a></li>
                           
                           <li><a href="http://www.earthwatch.org/aboutus/education/studentopp/" target="_blank">Earth Watch Institute: Enviornmental Education </a></li>
                           
                           <li><a href="http://www.eclee.eu" target="_blank">ECLEE: European Center for Leadership and Entrepreneurship Education</a></li>
                           
                           <li><a href="http://www.educationabroadnetwork.org" target="_blank">Education Abroad Network </a></li>
                           
                           <li><a href="http://www.enforex.com" target="_blank">Enforex: Spanish Studies in the Spanish World</a></li>
                           
                           <li><a href="http://www.euroscholars.eu/" target="_blank">Euro Scholars</a></li>
                           
                           <li><a href="http://www.eusa-edu.com/index.php" target="_blank">EUSA International Education </a></li>
                           
                           <li><a href="http://www.explorica.com" target="_blank">Explorica: Travel and Learn </a></li>
                           
                           <li><a href="http://www.gooverseas.com/study-abroad" target="_blank">GoOverseas</a></li>
                           
                           <li><a href="http://www.globalinksabroad.org/DefaultGlobalinksAbroad.aspx" target="_blank">GlobalLinks Abroad</a></li>
                           
                           <li>
                              <a href="http://www.icads.org/" target="_blank">ICADS: Institute for Central American Development Studies</a> 
                           </li>
                           
                           <li><a href="http://www.iesabroad.org" target="_blank">IES Abroad </a></li>
                           
                           <li><a href="http://www.iiepassport.org/" target="_blank">IIE Passport</a></li>
                           
                           <li><a href="http://www.ihp.edu/" target="_blank">IHP: International Honors Program</a></li>
                           
                           <li><a href="http://www.studiesabroad.com" target="_blank">ISA: International Studies Abroad </a></li>
                           
                           <li><a href="http://www.interstudy.org" target="_blank">Interstudy</a></li>
                           
                           <li><a href="http://www.studiesabroad.com/">ISA: International Studies Abroad </a></li>
                           
                           <li><a href="http://www2.uwrf.edu/itc/" target="_blank">ITC: International Traveling Classroom</a></li>
                           
                           <li><a href="http://www.keiabroad.org" target="_blank">KEI: Knowledge Exchange Institute</a></li>
                           
                           <li><a href="http://www.miusa.org/" target="_blank">MIUSA Mobility International USA</a></li>
                           
                           <li><a href="http://www.ole.edu.mx" target="_blank">OLE Center for Spanish Language and Culture </a></li>
                           
                           <li><a href="http://www.oxbridgeprograms.com" target="_blank">Oxbridge Acedemic Programs</a></li>
                           
                           <li><a href="http://www.passports.com" target="_blank">Passports; An Educational Travel Institute </a></li>
                           
                           <li><a href="http://www.saiprograms.com" target="_blank">SAI: Study Abroad Italy </a></li>
                           
                           <li><a href="http://www.sea.edu" target="_blank">Sea Education Association, Inc.</a></li>
                           
                           <li>
                              <a href="http://www.semesteratsea.org/" target="_blank">Semester at Sea</a> 
                           </li>
                           
                           <li><a href="http://www.sit.edu" target="_blank">SIT Study Abroad </a></li>
                           
                           <li><a href="http://www.fieldstudies.org/pages/517_sfs_at_a_glance.cfm" target="_blank">SFS: The School for Field Studies</a></li>
                           
                           <li>
                              <a href="http://www.sit.edu/studyabroad/" target="_blank">SIT Study Abroad: School for International Training</a> 
                           </li>
                           
                           <li><a href="http://www.steinhardt.nyu.edu/global" target="_blank">Steinhardt NYC </a></li>
                           
                           <li><a href="http://suabroad.syr.edu/" target="_blank">Syracuse University</a></li>
                           
                           <li><a href="http://www.brockport.edu/studyabroad" target="_blank">The College at Brockport</a></li>
                           
                           <li><a href="http://www.cerge-ei.cz/abroad" target="_blank">UPCES: Undergraduate Program in Central European Studies </a></li>
                           
                           <li><a href="http://www.usil.edu.pe" target="_blank">USIL: Universidaad San Ignacio De Loyola</a></li>
                           
                        </ul>            
                        
                        <p><strong>Study Abroad Program Search Engines:<a name="Engines" id="Engines"></a></strong></p>
                        
                        <ul>
                           
                           <li> <a href="http://main.abet.org/aps/Accreditedprogramsearch.aspx" target="_blank">ABET Accredited Programs for Engineering</a>
                              
                           </li>
                           
                           <li><a href="http://fcie.org/?s=Search&amp;property-search-submit=Search" target="_blank">Florida Consortium for International Education (FCIE) Program Finder </a></li>
                           
                           <li><a href="http://www.studyabroad.com" target="_blank">www.studyabroad.com&nbsp; </a></li>
                           
                           <li>
                              <a href="http://www.goabroad.com/teach-abroad" target="_blank">Teach Abroad</a> 
                           </li>
                           
                           <li><a href="http://www.studiesabroad.com/" target="_blank">www.studiesabroad.com</a></li>
                           
                           <li><a href="http://www.goabroad.com/intern-abroad" target="_blank">Intern Abroad</a></li>
                           
                           <li><a href="http://www.teachabroad.com/" target="_blank">www.teachabroad.com</a></li>
                           
                           <li>
                              <a href="http://www.transitionsabroad.com/" target="_blank">www.transitionsabroad.com</a> 
                           </li>
                           
                        </ul>            
                        
                        <p><strong>Service Learning and Volunteer Programs<a name="Service" id="Service"></a></strong></p>
                        
                        <ul>
                           
                           <li><a href="http://www.360studenttravel.com" target="_blank">360 Travel by West Coast Connection</a></li>
                           
                           <li><a href="http://www.afsusa.org" target="_blank">AFS Intercultural Programs</a></li>
                           
                           <li><a href="http://www.amerispan.com" target="_blank">Amerispan</a></li>
                           
                           <li><a href="http://amideast.org/" target="_blank">Amideast Abroad</a></li>
                           
                           <li><a href="http://www.bunac.org/usa" target="_blank">BUNAC: Working Adventures Worldwide</a></li>
                           
                           <li>
                              <a href="http://www.intercoined.org" target="_blank">Coined</a> 
                           </li>
                           
                           <li><a href="http://www.cosi.co.cr" target="_blank">COSI: Costa Rica Spanish Institute </a></li>
                           
                           <li><a href="http://www.crossculturalsolutions.org/" target="_blank">Cross-Cultural Solutions</a></li>
                           
                           <li><a href="http://www.cqtours.com" target="_blank">Culture Quest</a></li>
                           
                           <li><a href="http://www.earthwatch.org" target="_blank">Earth Watch Institute</a></li>
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/steveslodge.jpg" target="_blank">Horticulture in Ecuador </a></li>
                           
                           <li><a href="http://www.orlandodiocese.org/en/mission-about-us" target="_blank">Mission Office Diocese of Orlando</a></li>
                           
                           <li><a href="http://www.servicelearning.org/" target="_blank">National Service-Learning Clearinghouse</a></li>
                           
                           <li><a href="http://www.orphanage-outreach.org" target="_blank">Orphanage Outreach</a></li>
                           
                           <li><a href="http://www.peacecorps.gov" target="_blank">Peace Corps </a></li>
                           
                           <li> <a href="http://valenciacollege.edu/international/studyabroad/students/documents/Quest_Brochure.pdf">Quest – Volunteer Abroad Program </a>
                              
                           </li>
                           
                           <li><a href="http://www.spiabroad.com" target="_blank">SPI Abroad </a></li>
                           
                           <li><a href="http://www.studyintaiwan.org" target="_blank">Study In Taiwan </a></li>
                           
                           <li><a href="http://www.spanishstudies.org" target="_blank">Spanish Studies Abroad</a></li>
                           
                           <li><a href="http://www.brockport.edu/studyabroad" target="_blank">The College at Brockport</a></li>
                           
                           <li><a href="http://www.ipsl.org/" target="_blank">The International Partnership for Service-Learning and Leadership</a></li>
                           
                           <li><a href="http://www.unitedplanet.org/" target="_blank">United Planet</a></li>
                           
                           <li><a href="http://www.partnershipvolunteers.org" target="_blank">VIP: Volunteer for International Partnerships</a></li>
                           
                           <li><a href="http://www.volunteeringinafrica.org/ghana.htm" target="_blank">Volunteer in Africa</a></li>
                           
                        </ul>            
                        
                        
                        <p><a href="#TOP">TOP&nbsp;</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <br>
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/semesterprograms.pcf">©</a>
      </div>
   </body>
</html>