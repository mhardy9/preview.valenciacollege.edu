<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/shorttermfundingstudyabroad_new.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>Funding Study Abroad<a name="top" id="top"></a>
                           
                        </h2>
                        
                        
                        <p>Finding the money to support a short-term or semester study abroad program can be
                           challenging for some. The first step in the process is to create a budget. Use the
                           <em>Budget Worksheet for Study Abroad</em> form on the <strong>How to Apply </strong> pages to help you. If you are applying for any scholarship money or plan on using
                           your financial aid, this is a required form.
                           The following is a list of resources to help fund your study abroad program: 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Generation Study Abroad </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The <a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Institute of International Education</a> has just announced the launch of <strong>Generation Study Abroad</strong>, a five-years campaign to double the number of American students who study abroad
                                       by the end of the decade.
                                    </p>
                                    
                                    <p>AIFS is the first study abroad organization to become a committed partner of Generation
                                       Study Abroad. We are now offering <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">Generation Study Abroad Scholarship for semester and summer programs</a></p>
                                    
                                    <p>Every student should have the opportunity to study abroad. Our goal is to have 600,000
                                       U.S. students studying abroad in credit and non-credit programs. Over the next 5 years,
                                       <strong>Generation Study Abroad</strong> will reach out to educators at all levels and stakeholders in the public and private
                                       sectors to encourage purposeful, innovative action to get more people interested to
                                       undertake an international experience. 
                                    </p>
                                    
                                    <p><strong>Why? Because international experience is one of the most important components of a
                                          21st. century education. Read more about the Generation Study Abroad challenge </strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p>SAGE Scholarships</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The SAGE office currently has funding from the Valencia Foundation and from Student
                                       Development to help offset the costs of study abroad. The amount for short-term study
                                       abroad is listed on the country website page. You must submit a separate application
                                       for semester study abroad. Awards range from $1000 to $4000 depending upon the number
                                       of applicants and the contents of your application. <strong>BE SURE TO READ THE SCHOLARSHIP REQUIREMENTS BEFORE APPLYING. </strong><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="../../../finaid/gettingstarted/getting_started.html" target="_blank">Financial Aid</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Full-time, Valencia students who are eligible for financial aid can use these funds
                                       for  study abroad programs that award academic credit for study.&nbsp; Read more about
                                       <a href="http://www.nafsa.org/Explore_International_Education/For_Students/Financial_Aid_For_Study_Abroad/Financial_Aid_for_Study_Abroad__An_Undergraduate_Student_s_Resource/" target="_blank">Using Financial Aid for Study Abroad</a>.                    <br>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/gilman" target="_blank">Benjamin A. Gilman International Scholarship Program</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The Gilman Scholarship offers awards up to $5,000 to study abroad to U.S. citizen
                                       undergraduates receiving federal <strong>Pell Grant</strong> funding at 2-year or 4-year colleges or universities. Students studying a Critical
                                       Need Language may receive a $3,000 supplement for a total possible award of $8,000.
                                    </p>
                                    
                                    <p><strong>NOTE: For your application and essay, please be sure to include the following details
                                          IF they apply to you.</strong></p>
                                    
                                    <ul>
                                       
                                       <li>You're a current/past student at community college</li>
                                       
                                       <li>You're studying in a field underrepresented in study abroad (sciences and engineering)</li>
                                       
                                       <li> You're a first generation college student</li>
                                       
                                       <li>You're a student of diverse ethnic backgrounds</li>
                                       
                                       <li>You're a student with disabilities  </li>
                                       
                                       <li>You're traveling to a country outside of Western Europe, Australia or New Zealand
                                          
                                       </li>
                                       
                                       <li>You intend to study a critical need language during your study abroad experience
                                          
                                          <ul>
                                             
                                             <li>Arabic (all dialects)</li>
                                             
                                             <li>Chinese (all dialects)</li>
                                             
                                             <li>Bahasa Indonesia</li>
                                             
                                             <li>Japanese</li>
                                             
                                             <li>Turkic (Azerbaijani, Kazakh, Kyrgz, Turkish, Turkmen, Uzbek)</li>
                                             
                                             <li>Persian (Farsi, Dari, Kurdish, Pashto, Tajiki)</li>
                                             
                                             <li>Indic (Hindi, Urdu, Nepali, Sinhala, Bengali, Punjabi, Marathi, Gujarati, Sindhi)</li>
                                             
                                             <li>Korean</li>
                                             
                                             <li>Russian</li>
                                             
                                             <li>Swahili</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.borenawards.org" target="_blank">Boren Scholarships</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Boren Scholarships offer&nbsp;unique opportunities for U.S. undergraduates to study abroad
                                    for a semester or a year in&nbsp;world regions critical to U.S. interests (including Africa,
                                    Asia, Eastern Europe, Eurasia, Latin American &amp; the Caribbean, and the Middle East).
                                    In exchange for scholarship funding, all Boren Scholars must agree to work for the
                                    Federal Government in a position with national security responsibilities. Refer to
                                    "Boren Scholarship Basics" for complete details. Funding varies by length of program
                                    up to $20,000. Read the <a href="http://www.borenawards.org/boren_scholarship/faq">Boren FAQs</a> for more information. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">AIFS Study Abroad</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">Each year, more than <strong>$600,000 in scholarships</strong>, grants and financial support is awarded to deserving students &amp; institutions. Funds
                                    are available for both summer and semester programs. Please check the website for
                                    more details and how to apply. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p><a href="http://www.myfloridaprepaid.com/" target="_blank">Florida Prepaid College</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">If you have a Florida Prepaid College program, you can use those funds to pay for
                                    the regular tuition rate for a study abroad course. These funds cannot be used for
                                    any additional program costs or lab fees.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.fundforeducationabroad.com/applicants/" target="_blank">Fund for Education Abroad</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> The FEA is committed to increasing the opportunities for dedicated American students
                                    to participate in high-quality, rigorous education abroad programs by reducing financial
                                    restrictions through the provision of grants and scholarships. Participants must be
                                    studying at least 4 weeks in country. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://us.fulbrightonline.org" target="_blank">U. S. Fulbright Student Program</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Sponsored by the U.S. Department of State, Bureau of Educational and Cultural Affairs.
                                    Fulbright is the largest U.S. international exchange program offering opportunities
                                    for students, scholars and professionals to undertake international graduate study,
                                    advanced research, university teaching, and teaching in elementary and secondary schools
                                    worldwide.                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="http://www.ccisabroad.org/" target="_blank">College Consortium for International Studies</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">The College Consortium for International Studies offers scholarships each term to
                                    students at CCIS member institutions. Valenica maintains a membership to CCIS. If
                                    you are participating in a CCIS Program, contact the Study Abroad Office for CCIS
                                    Scholarship information. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.caorc.org/" target="_blank">Critical Language Scholarships for Intensive Summer Institutes</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Scholarships for U.S. citizen undergraduate, master's and Ph.D. students to participate
                                    in beginning, intermediate and advanced level intensive summer language programs at
                                    American Overseas Research Centers.                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.floridastudentfinancialaid.org/SSFAD/home/uamain.htm" target="_blank">Florida Student Scholarship and Grant Programs</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The Office of Student Financial Assistance (OSFA) State Programs, within the Florida
                                    Department of Education, administers a variety of postsecondary educational state-funded
                                    grants and scholarships, and provides information to students, parents, and high school
                                    and postsecondary professionals.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/programs/Freeman-Asia" target="_blank">Freeman-Asia: Awards for Study in Asia</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Open to undergraduate students to study in East and Southeast Asia. Fall, Spring and
                                    Summer cycles. Awards amounts vary from $3,000 for summer term $5,000 for semester
                                    and $7,000 for academic year programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadfunding.org/" target="_blank">Institute of International Education </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">With hundreds of scholarship and grant listings, IIE's online directory Study Abroad
                                    Funding, is among the most comprehensive directories for U.S. students to find funding
                                    for studying abroad. It contains thousands of study abroad listings offered by U.S.
                                    and foreign universities and study abroad providers, along with key information on
                                    funding opportunities for study abroad experiences. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadscholars.org/index.html" target="_blank">Hispanic Study Abroad Scholars (HSAS)</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>                </strong>Over $8 Million in scholarships for Hispanic study abroad. The HSAS Scholarship offers
                                    $1,000 per semester to students whose college or university is a member of the Hispanic
                                    Association of Colleges and Universities (HACU) and are attending Global Learning
                                    Semesters programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.rotary.org/en/StudentsAndYouth/Pages/ridefault.aspx">Rotary International</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The Rotary Club offers ambassadorial scholarships for students and grants for university
                                    professors. These programs can change the lives of those who participate. Through
                                    these programs, participants can travel on cultural exchanges, or help a community
                                    through a service project. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="../../../finaid/Scholarship_bulletin.html#ScholarshipSearch" target="_blank">Scholarship Bulletin Board</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">This Scholarship Bulletin Board offers students information on available scholarships.
                                    Scholarship advertisements include application requirements, locations of applications,
                                    and specific deadlines. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.sit.edu/studyabroad/scholarships" target="_blank">SIT Study Abroad Financial Aid and Scholarships</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The <strong>S</strong>chool for <strong>I</strong>nternational <strong>T</strong>raining and World Learning offer a variety of study abroad scholarships ranging from
                                    $500 to $5,000. Awards are based on need, merit, and the inclusion of students from
                                    a variety of institutions, from across the U.S. and from around the world. Most scholarship
                                    funds are available for semester or summer programs.                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p><a href="http://valenciacollege.edu/international/studyabroad/students/documents/ScholarshipstoJapan.doc">Scholarships to Japan</a> <br>
                                       (Word document)
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.aatj.org/studyabroad/scholarships.html">http://www.aatj.org/studyabroad/scholarships.html</a><br>
                                       <a href="http://www.aatj.org/studyabroad/morgan.html">http://www.aatj.org/studyabroad/morgan.html</a><br>
                                       <a href="http://www.hashi.org/scholarshiphp.html">http://www.hashi.org/scholarshiphp.html</a>                  <br>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadfunding.org/" target="_blank">IIE Funding Study Abroad </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> This directory features detailed descriptions of hundreds of study abroad scholarships,
                                    fellowships, grants, and paid internships for U.S. undergraduate, graduate and post-graduate
                                    students, and professionals. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p><a href="http://www.nafsa.org/students.sec/financial_aid_for_study/" target="_blank">NAFSA: Association of International Educators</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> Financial aid for undergraduate study abroad consists mainly of federal grants and
                                    federal and private loans. However, scholarship money is also available from organizations
                                    and sponsoring companies. You likely have many questions about the cost of studying
                                    abroad and about how you can fund your abroad experience. Be sure to speak with your
                                    campus financial aid officer and study abroad adviser to learn more about specific
                                    funding options available at—and required procedures for—your college or university.
                                    
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Short-Term Programs - How To Apply </h3>
                        
                        <p><strong>PROGRAM DEADLINE AND PAYMENT DATES FOR 2015: </strong></p>
                        
                        <p><strong>PLEASE NOTE THAT THE APPLICATION FEE IS NON-REFUNDABLE UNLESS (1) YOU ARE NOT ACCEPTED
                              INTO A PROGRAM OR (2) THE PROGRAM IS CANCELLED. DO NOT APPLY UNLESS YOU ARE SURE THAT
                              YOU ARE GOING TO ENROLL IN THE PROGRAM!</strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">TERM</div>
                                 
                                 <div data-old-tag="th">ONLINE APPLICATION OPENS </div>
                                 
                                 <div data-old-tag="th">INTERNSHIP &amp; SERVICE LEARNING APPLICATION DEADLINE DATE </div>
                                 
                                 <div data-old-tag="th"><strong>SAGE APPLICATION DEADLINE DATE </strong></div>
                                 
                                 <div data-old-tag="th">DEPOSIT &amp; PASSPORT DUE DATE </div>
                                 
                                 <div data-old-tag="th">FINAL PAYMENT DUE DATE </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Spring Break </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>6/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10/31/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>12/05/14</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer A </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>6/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>12/05/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>12/05/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>02/13/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>03/20/15</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer B </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>6/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>02/13/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>02/13/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>03/06/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>04/03/15</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Financial aid disbursements for 2014-2015:</strong> September 26, February 6, June 5 (NOTE: Flex Start classes may delay disbursement)
                        </p>
                        
                        <p><strong>FOLLOW THE STEPS BELOW TO APPLY. Use the <a href="http://valenciacollege.edu/international/studyabroad/students/documents/HowtoApplyChecklist.docx">How to Apply Checklist</a> for assistance. </strong></p>
                        
                        <p><strong>(1) <u>PROGRAM OPTIONS</u>: </strong>Review the short-term <a href="shorttermprograms.html" target="_blank">study abroad program</a> options on the website. READ the information in FULL before applying! Please note
                           that not all short-term study abroad programs offer an internship or service learning
                           option. Contact the program leader for details. 
                        </p>
                        
                        <ul>
                           
                           <li>For <a href="../../../students/service-learning/students.html"><strong>SERVICE LEARNING</strong></a> options, follow the instructions on the Service Learning website.
                           </li>
                           
                           <li>For <a href="../../../internship/index.html"><strong>INTERNSHIP</strong></a> options, follow the instructions on the Internship website. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>(2) <u>PROGRAM ELIGIBILITY</u>: </strong> In order to apply, you must...
                        </p>
                        
                        <ul>
                           
                           <li> be at least 18 years old at the time of travel</li>
                           
                           <li>have earned 12 college-level credits prior to the anticipanted travel date </li>
                           
                           <li>have a 2.5 GPA at the application deadline date listed above</li>
                           
                           <li> meet the course requirements as outlined on the website - be sure to check prerequisites</li>
                           
                        </ul>
                        
                        <p><strong>(3) <u>SCHOLARSHIP ELIGIBILITY</u>: </strong> Verify that you meet the scholarship eligibility requirements below. You must meet
                           the requirements <u><strong>at the application deadline date</strong></u> above. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Students  must apply by the application deadline date (above) in order to receive
                                 the FULL scholarship amount. If you apply after the deadline date, you will only be
                                 eligible for a PARTIAL scholarship.</strong> <strong>ELIGIBILITY IS FACTORED BY THE APPLICATION DEADLINE DATE, NOT THE TRAVEL DATE. <br>
                                 </strong>
                              
                           </li>
                           
                           <li> Be a degree-seeking student at Valencia. You do not qualify for a scholarship if
                              you are a transient, audit, or dual enrollment student. For students who are currently
                              enrolled at Valencia and graduating in the term (semester) prior to travel, you can
                              get a scholarship, but you will not have access to financial aid. <br>
                              
                           </li>
                           
                           <li>Be enrolled in the minimum number of credits in the term of your study abroad program:
                              3 for spring (not including the study abroad program), 3 for fall (not includingthe
                              study abroad program), or 2 for summer (can include the study abroad program). Please
                              note that financial aid may have different requirements.<br>
                              
                           </li>
                           
                           <li>Have completed at least one semester with Valencia  within the last year. <br>
                              
                           </li>
                           
                           <li>Receive only one SAGE scholarship for study abroad within the academic year (fall-spring-summer).<br>
                              
                           </li>
                           
                           <li>Meet all course requirements (attend all pre- and post-trip meetings, participate
                              in all in-country activities, turn in all academic assignments, obtain a grade of
                              a C or better).<br>
                              
                           </li>
                           
                           <li>Submit a thank you card to the donor addressed "To My Esteemed Donor."<br>
                              
                           </li>
                           
                           <li>Complete the online course evaluation within two weeks upon your return.<br>
                              
                           </li>
                           
                           <li>Upon your return, you must contact the  SAGE Office within 30 days to coordinate your
                              scholarship project. The project will involve  promoting study abroad to future students
                              by assisting at an event, facilitating an information session, or conducting a classroom
                              presentation. Instructions will be provided and documentation of the project completion
                              will be necessary in order to retain your scholarship. <br>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>(4) <u>STUDY ABROAD / FINANCIAL AID ADVISING</u>: </strong>Meet with a study abroad advisor on your campus (click on the WELCOME tab above for
                           contact names) to ensure that the course fits into your academic plan and there are
                           no financial aid issues. The program must count towards your degree in order to use
                           your financial aid. 
                        </p>
                        
                        <p><strong>(5) <u>APPLICATION FEE</u>: </strong>Pay the application fee to the Business Office. DO THIS BEFORE YOU COMPLETE THE ONLINE
                           APPLICATION! <br>
                           <br>
                           <strong>(6) <u>ONLINE APPLICATION</u>: </strong>Complete the program online application process and do not skip any pages. Each application
                           packet contains the application form, student agreement, risk and release waiver,
                           and medical form. <strong>You can find the link on the program webpage. </strong>Please upload the following documents with your application: (1) UNOFFICIAL TRANSCRIPT;
                           (2) COPY OF A VALID PASSPORT UP TO 6 MONTHS AFTER THE PROGRAM END DATE.
                        </p>
                        
                        <p><strong>If you do not have a passport, you will need to apply as SOON as you are accepted
                              into the program (within 5 days). You will need to show proof of application (receipt)
                              or a copy of your passport when the program deposit is due or you will not be enrolled
                              in the course and your spot will be given to a waitlisted student. </strong></p>
                        
                        <p><strong>*********************************************************************************************************************************************************</strong></p>
                        
                        <p>  Here are some tools to help you plan:</p>
                        
                        <ul>
                           
                           <li> <a href="http://valenciacollege.edu/international/studyabroad/students/documents/Short-TermStudyAbroadPlanner-2014v2.pdf">Short-Term Study Abroad Planner</a>
                              
                           </li>
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/04BudgetWorksheetforStudyAbroad.pdf">Budget Worksheet for Study Abroad</a></li>
                           
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/05RecommendedPackingList.pdf">Recommended Packing List</a></li>
                           
                           <li>
                              <a href="http://valenciacollege.edu/international/studyabroad/students/documents/PropertyDocumentForm.pdf">Property Document Form</a> (for insurance claims)
                           </li>
                           
                           <li><a href="../resources/travelandsafety.html">Travel and Safety Resources </a></li>
                           
                        </ul>
                        
                        <p><strong>FIRST-SEMESTER COLLEGE STUDENTS:</strong><br>
                           If this is your first semester in college, you will need to upload a copy of your
                           reading and writing PERT scores with your application. 
                        </p>
                        
                        <p><strong>NON-U.S. PASSPORT HOLDERS (CHINA PROGRAM EXCLUDED!!):<br>
                              </strong>1. Before you apply, verify if you need a visa (it is usually a tourist visa that
                           you will need) and what the requirements are at <a href="http://www.visahq.com" target="_blank">www.visahq.com</a>.<br>
                           2. Contact the program country embassy
                           to confirm the requirements.<br>
                           3. You can only get conditional acceptance into a program. A place may be held for
                           you up to the program payment deadline, but you must secure the visa before we can
                           enroll you.<br>
                           4. Please note that not all visas are approved.  You will have to do this at your
                           own expense and risk. <br>
                           5.  If you are at Valencia on an F or J visa, you must meet with an international
                           advisor to see if there are any travel restrictions on your visa. 
                        </p>
                        
                        <p>Neither the SAGE office nor the International Student Services office is trained to
                           handle tourist visa questions. You must contact the resources listed above. 
                        </p>
                        
                        <p><strong>DUAL ENROLLMENT STUDENTS:<br>
                              </strong>1. You must check in with the Dual Enrollment office prior to applying: <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> or call 407-582-1600.<br>
                           2.  The course must be 3 credits and your high school must accept it for credit. This
                           should be done BEFORE you apply. <br>
                           3. You must be 18 years old at the time of travel.<br>
                           4. You do not qualify for a scholarship.<br>
                           5. You must pay the program fee in full.
                           
                        </p>
                        
                        <p><strong>IF YOU ARE WAITLISTED:</strong></p>
                        
                        <ul>
                           
                           <li>Be prepared to attend all predeparture meetings.</li>
                           
                           <li>Have a valid passport ready to go.</li>
                           
                           <li>Have your program payment ready to submit to the College.</li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/shorttermfundingstudyabroad_new.pcf">©</a>
      </div>
   </body>
</html>