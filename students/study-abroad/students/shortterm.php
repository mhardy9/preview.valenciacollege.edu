<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/shortterm.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Short-Term Programs - How To Apply </h2>
                        
                        <p><strong>PROGRAM DEADLINE AND PAYMENT DATES FOR 2015: </strong></p>
                        
                        <p><strong>PLEASE NOTE THAT THE APPLICATION FEE IS NON-REFUNDABLE UNLESS (1) YOU ARE NOT ACCEPTED
                              INTO A PROGRAM OR (2) THE PROGRAM IS CANCELLED. DO NOT APPLY UNLESS YOU ARE SURE THAT
                              YOU ARE GOING TO ENROLL IN THE PROGRAM!</strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">TERM</div>
                                 
                                 <div data-old-tag="th">ONLINE APPLICATION OPENS </div>
                                 
                                 <div data-old-tag="th">INTERNSHIP &amp; SERVICE LEARNING APPLICATION DEADLINE DATE </div>
                                 
                                 <div data-old-tag="th"><strong>SAGE APPLICATION DEADLINE DATE </strong></div>
                                 
                                 <div data-old-tag="th">DEPOSIT &amp; PASSPORT DUE DATE </div>
                                 
                                 <div data-old-tag="th">FINAL PAYMENT DUE DATE </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Spring Break </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>6/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>10/31/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>12/05/14</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer A </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>6/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>12/05/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>12/05/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>02/13/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>03/20/15</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer B </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>6/03/14</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>02/13/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>02/13/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>03/06/15</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>04/03/15</div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><strong>Financial aid disbursements for 2014-2015:</strong> September 26, February 6, June 5 (NOTE: Flex Start classes may delay disbursement)
                        </p>
                        
                        <p><strong>FOLLOW THE STEPS BELOW TO APPLY. Use the <a href="http://valenciacollege.edu/international/studyabroad/students/documents/HowtoApplyChecklist.docx">How to Apply Checklist</a> for assistance. </strong></p>
                        
                        <p><strong>(1) <u>PROGRAM OPTIONS</u>: </strong>Review the short-term <a href="shorttermprograms.html" target="_blank">study abroad program</a> options on the website. READ the information in FULL before applying! Please note
                           that not all short-term study abroad programs offer an internship or service learning
                           option. Contact the program leader for details. 
                        </p>
                        
                        <ul>
                           
                           <li>For <a href="../../../students/service-learning/students.html"><strong>SERVICE LEARNING</strong></a> options, follow the instructions on the Service Learning website.
                           </li>
                           
                           <li>For <a href="../../../internship/index.html"><strong>INTERNSHIP</strong></a> options, follow the instructions on the Internship website. 
                           </li>
                           
                        </ul>
                        
                        <p><strong>(2) <u>PROGRAM ELIGIBILITY</u>: </strong> In order to apply, you must...
                        </p>
                        
                        <ul>
                           
                           <li> be at least 18 years old at the time of travel</li>
                           
                           <li>have earned 12 college-level credits prior to the anticipanted travel date </li>
                           
                           <li>have a 2.5 GPA at the application deadline date listed above</li>
                           
                           <li> meet the course requirements as outlined on the website - be sure to check prerequisites</li>
                           
                        </ul>
                        
                        <p><strong>(3) <u>SCHOLARSHIP ELIGIBILITY</u>: </strong> Verify that you meet the scholarship eligibility requirements below. You must meet
                           the requirements <u><strong>at the application deadline date</strong></u> above. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <strong>Students  must apply by the application deadline date (above) in order to receive
                                 the FULL scholarship amount. If you apply after the deadline date, you will only be
                                 eligible for a PARTIAL scholarship.</strong> <strong>ELIGIBILITY IS FACTORED BY THE APPLICATION DEADLINE DATE, NOT THE TRAVEL DATE. <br>
                                 </strong>
                              
                           </li>
                           
                           <li> Be a degree-seeking student at Valencia. You do not qualify for a scholarship if
                              you are a transient, audit, or dual enrollment student. For students who are currently
                              enrolled at Valencia and graduating in the term (semester) prior to travel, you can
                              get a scholarship, but you will not have access to financial aid. <br>
                              
                           </li>
                           
                           <li>Be enrolled in the minimum number of credits in the term of your study abroad program:
                              3 for spring (not including the study abroad program), 3 for fall (not includingthe
                              study abroad program), or 2 for summer (can include the study abroad program). Please
                              note that financial aid may have different requirements.<br>
                              
                           </li>
                           
                           <li>Have completed at least one semester with Valencia  within the last year. <br>
                              
                           </li>
                           
                           <li>Receive only one SAGE scholarship for study abroad within the academic year (fall-spring-summer).<br>
                              
                           </li>
                           
                           <li>Meet all course requirements (attend all pre- and post-trip meetings, participate
                              in all in-country activities, turn in all academic assignments, obtain a grade of
                              a C or better).<br>
                              
                           </li>
                           
                           <li>Submit a thank you card to the donor addressed "To My Esteemed Donor."<br>
                              
                           </li>
                           
                           <li>Complete the online course evaluation within two weeks upon your return.<br>
                              
                           </li>
                           
                           <li>Upon your return, you must contact the  SAGE Office within 30 days to coordinate your
                              scholarship project. The project will involve  promoting study abroad to future students
                              by assisting at an event, facilitating an information session, or conducting a classroom
                              presentation. Instructions will be provided and documentation of the project completion
                              will be necessary in order to retain your scholarship. <br>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>(4) <u>STUDY ABROAD / FINANCIAL AID ADVISING</u>: </strong>Meet with a study abroad advisor on your campus (click on the WELCOME tab above for
                           contact names) to ensure that the course fits into your academic plan and there are
                           no financial aid issues. The program must count towards your degree in order to use
                           your financial aid. 
                        </p>
                        
                        <p><strong>(5) <u>APPLICATION FEE</u>: </strong>Pay the application fee to the Business Office. DO THIS BEFORE YOU COMPLETE THE ONLINE
                           APPLICATION! <br>
                           <br>
                           <strong>(6) <u>ONLINE APPLICATION</u>: </strong>Complete the program online application process and do not skip any pages. Each application
                           packet contains the application form, student agreement, risk and release waiver,
                           and medical form. <strong>You can find the link on the program webpage. </strong>Please upload the following documents with your application: (1) UNOFFICIAL TRANSCRIPT;
                           (2) COPY OF A VALID PASSPORT UP TO 6 MONTHS AFTER THE PROGRAM END DATE.
                        </p>
                        
                        <p><strong>If you do not have a passport, you will need to apply as SOON as you are accepted
                              into the program (within 5 days). You will need to show proof of application (receipt)
                              or a copy of your passport when the program deposit is due or you will not be enrolled
                              in the course and your spot will be given to a waitlisted student. </strong></p>
                        
                        <p><strong>*********************************************************************************************************************************************************</strong></p>
                        
                        <p>  Here are some tools to help you plan:</p>
                        
                        <ul>
                           
                           <li> <a href="http://valenciacollege.edu/international/studyabroad/students/documents/Short-TermStudyAbroadPlanner-2014v2.pdf">Short-Term Study Abroad Planner</a>
                              
                           </li>
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/04BudgetWorksheetforStudyAbroad.pdf">Budget Worksheet for Study Abroad</a></li>
                           
                           
                           <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/05RecommendedPackingList.pdf">Recommended Packing List</a></li>
                           
                           <li>
                              <a href="http://valenciacollege.edu/international/studyabroad/students/documents/PropertyDocumentForm.pdf">Property Document Form</a> (for insurance claims)
                           </li>
                           
                           <li><a href="../resources/travelandsafety.html">Travel and Safety Resources </a></li>
                           
                        </ul>
                        
                        <p><strong>FIRST-SEMESTER COLLEGE STUDENTS:</strong><br>
                           If this is your first semester in college, you will need to upload a copy of your
                           reading and writing PERT scores with your application. 
                        </p>
                        
                        <p><strong>NON-U.S. PASSPORT HOLDERS (CHINA PROGRAM EXCLUDED!!):<br>
                              </strong>1. Before you apply, verify if you need a visa (it is usually a tourist visa that
                           you will need) and what the requirements are at <a href="http://www.visahq.com" target="_blank">www.visahq.com</a>.<br>
                           2. Contact the program country embassy
                           to confirm the requirements.<br>
                           3. You can only get conditional acceptance into a program. A place may be held for
                           you up to the program payment deadline, but you must secure the visa before we can
                           enroll you.<br>
                           4. Please note that not all visas are approved.  You will have to do this at your
                           own expense and risk. <br>
                           5.  If you are at Valencia on an F or J visa, you must meet with an international
                           advisor to see if there are any travel restrictions on your visa. 
                        </p>
                        
                        <p>Neither the SAGE office nor the International Student Services office is trained to
                           handle tourist visa questions. You must contact the resources listed above. 
                        </p>
                        
                        <p><strong>DUAL ENROLLMENT STUDENTS:<br>
                              </strong>1. You must check in with the Dual Enrollment office prior to applying: <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> or call 407-582-1600.<br>
                           2.  The course must be 3 credits and your high school must accept it for credit. This
                           should be done BEFORE you apply. <br>
                           3. You must be 18 years old at the time of travel.<br>
                           4. You do not qualify for a scholarship.<br>
                           5. You must pay the program fee in full.
                           
                        </p>
                        
                        <p><strong>IF YOU ARE WAITLISTED:</strong></p>
                        
                        <ul>
                           
                           <li>Be prepared to attend all predeparture meetings.</li>
                           
                           <li>Have a valid passport ready to go.</li>
                           
                           <li>Have your program payment ready to submit to the College.</li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/shortterm.pcf">©</a>
      </div>
   </body>
</html>