<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/semesterfundingstudyabroad_new.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2>Funding Study Abroad<a name="top" id="top"></a>
                           
                        </h2>
                        
                        
                        <p>Finding the money to support a short-term or semester study abroad program can be
                           challenging for some. The first step in the process is to create a budget. Use the
                           <em>Budget Worksheet for Study Abroad</em> form on the <strong>How to Apply </strong> pages to help you. If you are applying for any scholarship money or plan on using
                           your financial aid, this is a required form.
                           The following is a list of resources to help fund your study abroad program: 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Generation Study Abroad </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The <a href="http://www.iie.org/Programs/Generation-Study-Abroad" target="_blank">Institute of International Education</a> has just announced the launch of <strong>Generation Study Abroad</strong>, a five-years campaign to double the number of American students who study abroad
                                       by the end of the decade.
                                    </p>
                                    
                                    <p>AIFS is the first study abroad organization to become a committed partner of Generation
                                       Study Abroad. We are now offering <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">Generation Study Abroad Scholarship for semester and summer programs</a></p>
                                    
                                    <p>Every student should have the opportunity to study abroad. Our goal is to have 600,000
                                       U.S. students studying abroad in credit and non-credit programs. Over the next 5 years,
                                       <strong>Generation Study Abroad</strong> will reach out to educators at all levels and stakeholders in the public and private
                                       sectors to encourage purposeful, innovative action to get more people interested to
                                       undertake an international experience. 
                                    </p>
                                    
                                    <p><strong>Why? Because international experience is one of the most important components of a
                                          21st. century education. Read more about the Generation Study Abroad challenge </strong></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p>SAGE Scholarships</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The SAGE office currently has funding from the Valencia Foundation and from Student
                                       Development to help offset the costs of study abroad. The amount for short-term study
                                       abroad is listed on the country website page. You must submit a separate application
                                       for semester study abroad. Awards range from $1000 to $4000 depending upon the number
                                       of applicants and the contents of your application. <strong>BE SURE TO READ THE SCHOLARSHIP REQUIREMENTS BEFORE APPLYING. </strong><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="../../../finaid/gettingstarted/getting_started.html" target="_blank">Financial Aid</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p>Full-time, Valencia students who are eligible for financial aid can use these funds
                                       for  study abroad programs that award academic credit for study.&nbsp; Read more about
                                       <a href="http://www.nafsa.org/Explore_International_Education/For_Students/Financial_Aid_For_Study_Abroad/Financial_Aid_for_Study_Abroad__An_Undergraduate_Student_s_Resource/" target="_blank">Using Financial Aid for Study Abroad</a>.                    <br>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/gilman" target="_blank">Benjamin A. Gilman International Scholarship Program</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>The Gilman Scholarship offers awards up to $5,000 to study abroad to U.S. citizen
                                       undergraduates receiving federal <strong>Pell Grant</strong> funding at 2-year or 4-year colleges or universities. Students studying a Critical
                                       Need Language may receive a $3,000 supplement for a total possible award of $8,000.
                                    </p>
                                    
                                    <p><strong>NOTE: For your application and essay, please be sure to include the following details
                                          IF they apply to you.</strong></p>
                                    
                                    <ul>
                                       
                                       <li>You're a current/past student at community college</li>
                                       
                                       <li>You're studying in a field underrepresented in study abroad (sciences and engineering)</li>
                                       
                                       <li> You're a first generation college student</li>
                                       
                                       <li>You're a student of diverse ethnic backgrounds</li>
                                       
                                       <li>You're a student with disabilities  </li>
                                       
                                       <li>You're traveling to a country outside of Western Europe, Australia or New Zealand
                                          
                                       </li>
                                       
                                       <li>You intend to study a critical need language during your study abroad experience
                                          
                                          <ul>
                                             
                                             <li>Arabic (all dialects)</li>
                                             
                                             <li>Chinese (all dialects)</li>
                                             
                                             <li>Bahasa Indonesia</li>
                                             
                                             <li>Japanese</li>
                                             
                                             <li>Turkic (Azerbaijani, Kazakh, Kyrgz, Turkish, Turkmen, Uzbek)</li>
                                             
                                             <li>Persian (Farsi, Dari, Kurdish, Pashto, Tajiki)</li>
                                             
                                             <li>Indic (Hindi, Urdu, Nepali, Sinhala, Bengali, Punjabi, Marathi, Gujarati, Sindhi)</li>
                                             
                                             <li>Korean</li>
                                             
                                             <li>Russian</li>
                                             
                                             <li>Swahili</li>
                                             
                                          </ul>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.borenawards.org" target="_blank">Boren Scholarships</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Boren Scholarships offer&nbsp;unique opportunities for U.S. undergraduates to study abroad
                                    for a semester or a year in&nbsp;world regions critical to U.S. interests (including Africa,
                                    Asia, Eastern Europe, Eurasia, Latin American &amp; the Caribbean, and the Middle East).
                                    In exchange for scholarship funding, all Boren Scholars must agree to work for the
                                    Federal Government in a position with national security responsibilities. Refer to
                                    "Boren Scholarship Basics" for complete details. Funding varies by length of program
                                    up to $20,000. Read the <a href="http://www.borenawards.org/boren_scholarship/faq">Boren FAQs</a> for more information. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="http://www.aifsabroad.com/scholarships.asp" target="_blank">AIFS Study Abroad</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">Each year, more than <strong>$600,000 in scholarships</strong>, grants and financial support is awarded to deserving students &amp; institutions. Funds
                                    are available for both summer and semester programs. Please check the website for
                                    more details and how to apply. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p><a href="http://www.myfloridaprepaid.com/" target="_blank">Florida Prepaid College</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">If you have a Florida Prepaid College program, you can use those funds to pay for
                                    the regular tuition rate for a study abroad course. These funds cannot be used for
                                    any additional program costs or lab fees.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.fundforeducationabroad.com/applicants/" target="_blank">Fund for Education Abroad</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> The FEA is committed to increasing the opportunities for dedicated American students
                                    to participate in high-quality, rigorous education abroad programs by reducing financial
                                    restrictions through the provision of grants and scholarships. Participants must be
                                    studying at least 4 weeks in country. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://us.fulbrightonline.org" target="_blank">U. S. Fulbright Student Program</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Sponsored by the U.S. Department of State, Bureau of Educational and Cultural Affairs.
                                    Fulbright is the largest U.S. international exchange program offering opportunities
                                    for students, scholars and professionals to undertake international graduate study,
                                    advanced research, university teaching, and teaching in elementary and secondary schools
                                    worldwide.                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>
                                       <a href="http://www.ccisabroad.org/" target="_blank">College Consortium for International Studies</a> 
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">The College Consortium for International Studies offers scholarships each term to
                                    students at CCIS member institutions. Valenica maintains a membership to CCIS. If
                                    you are participating in a CCIS Program, contact the Study Abroad Office for CCIS
                                    Scholarship information. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.caorc.org/" target="_blank">Critical Language Scholarships for Intensive Summer Institutes</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Scholarships for U.S. citizen undergraduate, master's and Ph.D. students to participate
                                    in beginning, intermediate and advanced level intensive summer language programs at
                                    American Overseas Research Centers.                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.floridastudentfinancialaid.org/SSFAD/home/uamain.htm" target="_blank">Florida Student Scholarship and Grant Programs</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The Office of Student Financial Assistance (OSFA) State Programs, within the Florida
                                    Department of Education, administers a variety of postsecondary educational state-funded
                                    grants and scholarships, and provides information to students, parents, and high school
                                    and postsecondary professionals.
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.iie.org/programs/Freeman-Asia" target="_blank">Freeman-Asia: Awards for Study in Asia</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">Open to undergraduate students to study in East and Southeast Asia. Fall, Spring and
                                    Summer cycles. Awards amounts vary from $3,000 for summer term $5,000 for semester
                                    and $7,000 for academic year programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadfunding.org/" target="_blank">Institute of International Education </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">With hundreds of scholarship and grant listings, IIE's online directory Study Abroad
                                    Funding, is among the most comprehensive directories for U.S. students to find funding
                                    for studying abroad. It contains thousands of study abroad listings offered by U.S.
                                    and foreign universities and study abroad providers, along with key information on
                                    funding opportunities for study abroad experiences. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadscholars.org/index.html" target="_blank">Hispanic Study Abroad Scholars (HSAS)</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <strong>                </strong>Over $8 Million in scholarships for Hispanic study abroad. The HSAS Scholarship offers
                                    $1,000 per semester to students whose college or university is a member of the Hispanic
                                    Association of Colleges and Universities (HACU) and are attending Global Learning
                                    Semesters programs. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.rotary.org/en/StudentsAndYouth/Pages/ridefault.aspx">Rotary International</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The Rotary Club offers ambassadorial scholarships for students and grants for university
                                    professors. These programs can change the lives of those who participate. Through
                                    these programs, participants can travel on cultural exchanges, or help a community
                                    through a service project. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="../../../finaid/Scholarship_bulletin.html#ScholarshipSearch" target="_blank">Scholarship Bulletin Board</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">This Scholarship Bulletin Board offers students information on available scholarships.
                                    Scholarship advertisements include application requirements, locations of applications,
                                    and specific deadlines. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.sit.edu/studyabroad/scholarships" target="_blank">SIT Study Abroad Financial Aid and Scholarships</a></div>
                                 </div>
                                 
                                 <div data-old-tag="td">The <strong>S</strong>chool for <strong>I</strong>nternational <strong>T</strong>raining and World Learning offer a variety of study abroad scholarships ranging from
                                    $500 to $5,000. Awards are based on need, merit, and the inclusion of students from
                                    a variety of institutions, from across the U.S. and from around the world. Most scholarship
                                    funds are available for semester or summer programs.                  
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p><a href="http://valenciacollege.edu/international/studyabroad/students/documents/ScholarshipstoJapan.doc">Scholarships to Japan</a> <br>
                                       (Word document)
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="http://www.aatj.org/studyabroad/scholarships.html">http://www.aatj.org/studyabroad/scholarships.html</a><br>
                                       <a href="http://www.aatj.org/studyabroad/morgan.html">http://www.aatj.org/studyabroad/morgan.html</a><br>
                                       <a href="http://www.hashi.org/scholarshiphp.html">http://www.hashi.org/scholarshiphp.html</a>                  <br>
                                       
                                    </p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div><a href="http://www.studyabroadfunding.org/" target="_blank">IIE Funding Study Abroad </a></div>
                                 </div>
                                 
                                 <div data-old-tag="td"> This directory features detailed descriptions of hundreds of study abroad scholarships,
                                    fellowships, grants, and paid internships for U.S. undergraduate, graduate and post-graduate
                                    students, and professionals. 
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    
                                    <p><a href="http://www.nafsa.org/students.sec/financial_aid_for_study/" target="_blank">NAFSA: Association of International Educators</a></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"> Financial aid for undergraduate study abroad consists mainly of federal grants and
                                    federal and private loans. However, scholarship money is also available from organizations
                                    and sponsoring companies. You likely have many questions about the cost of studying
                                    abroad and about how you can fund your abroad experience. Be sure to speak with your
                                    campus financial aid officer and study abroad adviser to learn more about specific
                                    funding options available at—and required procedures for—your college or university.
                                    
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>How To Apply</h3>
                        
                        <p> The process to research, plan, and prepare for a semester study abroad program is
                           a long one, so we recommend that you start at least one year prior to your program
                           start date and no less than 6 months prior. 
                        </p>
                        
                        <p><strong>How to Apply:</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>            </strong>Contact the SAGE office at 407-582-3188 or <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>. Let us know what your plans are and be sure to check in along the way. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              <strong>REQUIRED! </strong>Print out and follow the instructions on the <a href="http://valenciacollege.edu/international/studyabroad/students/documents/SemesterStudyAbroadPlannerv5.pdf" target="_blank">Semester Study Abroad Planner</a> and use the checklist to help you navigate this process.<br>
                              <br>
                              
                           </li>
                           
                           <li>Search for a <a href="semesterprograms.html">program provider</a>.
                           </li>
                           
                        </ul>            
                        
                        <p><span>IT IS VERY IMPORTANT THAT YOU SELECT A PROGRAM THAT WILL PROVIDE YOU WITH A TRANSCRIPT
                              ISSUED BY A U.S. INSTITUTION TO OBTAIN TRANSFER CREDIT. IF YOU DIRECTLY ENROLL IN
                              AN OVERSEAS INSTITUTION THAT HAS NO U.S. AFFILIATION, YOU CANNOT USE YOUR FINANCIAL
                              AID, YOU CANNOT GET A SAGE SCHOLARSHIP, YOU WILL NEED TO PAY FOR A NACES TRANSCRIPT
                              EVALUATION UPON PROGRAM COMPLETION, AND THERE IS NO GUARANTEE THAT THE CREDITS WILL
                              TRANSFER BACK TO VALENCIA. </span><br>
                           
                        </p>
                        
                        <ul>
                           
                           <li>Print out the following forms which have all been referenced in the <em>Semester Study Abroad Planner</em> above.<br>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/international/studyabroad/students/documents/04BudgetWorksheetforStudyAbroad.pdf">Budget Worksheet for Study Abroad</a> 
                                 </li>
                                 
                                 <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/ConsortiumAgreementFormforStudyAbroadv2.pdf"> Consortium Agreement Form for Study Abroad</a></li>
                                 
                                 <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/SAGEScholarshipInstructionsandApplicationForm.doc">SAGE Scholarship Instructions and Application Form</a></li>
                                 
                                 <li><a href="http://valenciacollege.edu/international/studyabroad/students/documents/05RecommendedPackingList.pdf">Recommended Packing List</a></li>
                                 
                                 <li>
                                    <a href="http://valenciacollege.edu/international/studyabroad/students/documents/05PropertyDocumentForm.pdf">Property Document Form</a><br>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>SAGE Scholarships: </strong>The SAGE office has  semester study abroad scholarships for up to $4000 (depending
                           upon the contents of your application) for which  you can apply for the 2013-2014
                           academic year. You do not have to be accepted into a program at the time of applying,
                           but you must provide proof of acceptance prior to receiving the scholarship. Scholarships
                           will be issued to your student account. In order to apply, you must:
                        </p>
                        
                        <ul>
                           
                           <li>Be a degree-seeking student at Valencia:
                              
                              <ul>
                                 
                                 <li>You cannot go on short-term study abroad with financial aid or a scholarship if you
                                    are already graduated.
                                 </li>
                                 
                                 <li>You do not qualify for a scholarship if you are a transient or audit student. </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Have a minimum 2.5 GPA at the time of applying to the program.</li>
                           
                           <li>Submit a <em>SAGE Scholarship Application Form</em> and supporting documents (above).  
                           </li>
                           
                           <li>Submit proof of program acceptance. </li>
                           
                           <li>Have completed at least one semester with Valencia prior to travel within the last
                              year.
                           </li>
                           
                           <li>Receive only one scholarship for study abroad within the academic year.</li>
                           
                           <li>Meet all course requirements (attend all pre- and post-trip meetings, participate
                              in all in-country activities, turn in all academic assignments, obtain a grade of
                              a C or better).
                           </li>
                           
                           <li>
                              <strong><a href="http://net4.valenciacollege.edu/forms/international/studyabroad/register_request.cfm" target="_blank">Register Your Trip</a></strong> with the SAGE Office. <span>IMPORTANT!</span> 
                           </li>
                           
                           <li>Submit a thank you card to the donor addressed "To My Esteemed Donor."</li>
                           
                           <li>Complete the online course evaluation within two weeks upon your return.</li>
                           
                           <li>Be willing to participate in a SAGE event to help promote study abroad to future students.
                              
                           </li>
                           
                        </ul>
                        
                        <p><strong>Important Websites: </strong>The following are some important website links for you. They are all referenced in
                           the <em>Semester Study Abroad Planner</em> above.
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://valenciacollege.edu/international/studyabroad/students/fundingstudyabroad.cfm" target="_blank">Study Abroad Scholarships</a>                
                           </li>
                           
                           <li>
                              <a href="http://www.usps.com/passport/" target="_blank">Passport</a> (your passport should be valid for up to 90 days after your program end date) 
                           </li>
                           
                           <li>
                              <a href="http://www.visahq.com/" target="_blank">Visa</a> (verify if you need a visa and apply 45-120 days prior) 
                           </li>
                           
                           <li><a href="http://www.cmiinsurance.com" target="_blank">Medical/trip insurance</a></li>
                           
                           <li><a href="http://www.isic.org/" target="_blank">International Student ID Card</a></li>
                           
                        </ul>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/semesterfundingstudyabroad_new.pcf">©</a>
      </div>
   </body>
</html>