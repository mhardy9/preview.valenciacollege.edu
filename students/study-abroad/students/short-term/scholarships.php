<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/short-term/scholarships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/short-term/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/short-term/">Short Term</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Short-Term Programs - Scholarships &amp; Financial Aid</h2>
                        
                        <p>The SAGE office has scholarship funding from the Valencia Foundation and Student Development
                           to help offset the costs of short-term study abroad. Please note the following:
                        </p>
                        
                        <ol>
                           
                           <li>The scholarship amount for short-term study abroad changes each year and is listed
                              on each country website page. 
                           </li>
                           
                           <li>The scholarship amount cannot exceed more than 50% of the total program cost so scholarship
                              amounts may differ across programs.
                           </li>
                           
                           <li>Some faculty get approved for additional scholarship funding which may increase the
                              amount of a scholarship for one particular program. 
                           </li>
                           
                        </ol>                  
                        
                        <p><strong>SCHOLARSHIP ELIGIBILITY REQUIREMENTS                  </strong></p>
                        
                        <ul>
                           
                           <li>Have a minimum 2.5 GPA at the time of applying to the program.</li>
                           
                           <li>Be a degree-seeking students at Valencia College:
                              
                              <ul>
                                 
                                 <li> you can get a scholarship after graduation as long as the program runs by the following
                                    semester so, for example, students who graduate in the fall term can get a scholarship
                                    for the spring term but not the summer term
                                 </li>
                                 
                                 <li>you do not qualify for a scholarship if you are a transient or audit student </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Have not received a study abroad scholarship within the current academic year</li>
                           
                           <li>Completed at least one semester with Valencia within the last year</li>
                           
                           <li>Submit a thank you card to your donor addressed to "My Esteemed Donor"</li>
                           
                           <li>Complete the online course evaluation at the end of the program </li>
                           
                           <li>Complete the SAGE Scholarship Project by the assigned deadline dates. </li>
                           
                        </ul>
                        
                        <p><strong>FINANCIAL AID </strong></p>
                        
                        <p>Recent changes in federal law have made it possible for students to use some or all
                           of their federal financial aid for semester study abroad. Full-time, Valencia students
                           who are eligible for <a href="../../../../finaid/gettingstarted/index.html">Financial Aid</a> can use these funds for study abroad programs that award academic credit and grades
                           counted towards your cumulative GPA for study.&nbsp; Eligibility is determined by the type
                           of study abroad program you attend, as well as the type of financial aid you receive.
                           Only students earning a degree at Valencia are eligible.
                        </p>
                        
                        <p>  Financial aid can be used to support summer study, as long as you are enrolled for
                           academic credit and you meet all other requirements. It is important to note that
                           many students max out their financial aid prior to summer, so be sure to plan accordingly!
                           
                        </p>
                        
                        <p> <strong>NOTE: </strong>You cannot use financial aid for study abroad after you graduate from Valencia! It
                           is important to meet with a Financial Aid Services advisor prior to accepting any
                           scholarship money to determine if it will affect your overall financial aid package.
                           Take your budget and the course approval forms to your Financial Aid Services advisor
                           for him or her to review to determine your eligibility. Eligibility is determined
                           by the type of program you attend, as well as the type of financial aid you receive.&nbsp;
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/short-term/scholarships.pcf">©</a>
      </div>
   </body>
</html>