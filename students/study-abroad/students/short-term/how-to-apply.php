<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/short-term/how-to-apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/short-term/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/short-term/">Short Term</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <h2>Short-Term Programs - How to Apply<a name="top" id="top"></a>
                           
                        </h2>
                        
                        <ul>
                           
                           <li><a href="#nextyear">Program Deadline, Orientation, and Payment Dates</a></li>
                           
                           <li><a href="#options">Program Options</a></li>
                           
                           <li><a href="#programeligibility">Program Eligibility</a></li>
                           
                           <li><a href="#scholareligibility">Scholarship Eligibility</a></li>
                           
                           <li>
                              <a href="#projects">Scholarship Project Opportunities</a> 
                           </li>
                           
                           <li><a href="#steps">Steps to Apply</a></li>
                           
                           <li>
                              <a href="#nonUS">Non-US Passport Holders</a> 
                           </li>
                           
                           <li><a href="#dual">Dual-Enrollment Students </a></li>
                           
                        </ul>
                        
                        <div>
                           
                           <p><strong>PLEASE NOTE THAT THE APPLICATION FEE IS NON-REFUNDABLE UNLESS (1) YOU ARE NOT ACCEPTED
                                 INTO A PROGRAM OR (2) THE PROGRAM IS CANCELLED. DO NOT APPLY UNLESS YOU ARE SURE THAT
                                 YOU ARE GOING TO ENROLL IN THE PROGRAM!</strong></p>
                           
                           
                        </div>
                        
                        <p>Use the <a href="documents/How-to-Apply-Checklist.pdf">How to Apply Checklist</a> for assistance. 
                        </p>
                        
                        <p><strong>PROGRAM DEADLINE, ORIENTATION, AND PAYMENT DATES FOR 2018: <a name="nextyear" id="nextyear"></a></strong></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">TERM</div>
                                 
                                 <div data-old-tag="th"><strong>Application Deadline Date with $250 Application Fee </strong></div>
                                 
                                 <div data-old-tag="th">Mandatory SAGE Orientation &amp; Passport Copy Due</div>
                                 
                                 <div data-old-tag="th">Deposit Payment Due at the Business Office </div>
                                 
                                 <div data-old-tag="th">Final Payment Due in Atlas </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Spring Break </strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>October 6, 2017</p>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <strong>November 2, 2017</strong> West Campus, room 11-106, 5-7 pm<br>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="td">November 10, 2017</div>
                                 
                                 <div data-old-tag="td">No Final Payment</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer A </strong></div>
                                 
                                 <div data-old-tag="td">December 15, 2017</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <strong>January 19, 2018</strong> Osceola Campus, room 4-105, 2-5 pm
                                       </li>
                                       
                                       <li>
                                          <strong>January 26, 2018</strong> East Campus, room 1-347, 2-5 pm
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">February 2 , 2018</div>
                                 
                                 <div data-old-tag="td">March 2, 2018</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer B </strong></div>
                                 
                                 <div data-old-tag="td">February 2, 2018</div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>
                                          <strong>February 16, 2018 </strong>                Osceola Campus, room 4-105, 2-5 pm
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                                 <div data-old-tag="td">March 2, 2018</div>
                                 
                                 <div data-old-tag="td">April 6, 2018</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Financial aid disbursements for 2017-2018: September 19/17, January 30/17, June 05/18
                           (Pell Grants July 10/18). 
                        </p>
                        
                        <p>Note: Flex Start classes may delay disbursement.</p>
                        
                        <p><strong>PROGRAM OPTIONS<a name="options" id="options"></a><br>
                              </strong>Click on the PROGRAMS link in the left navigation of this page to view all the short-term
                           study abroad program options for the current academic year. In addition to the credit
                           course/s listed, program leaders may make available other program options such as:
                           <a href="../internships/index.html">global learning internship</a>, <a href="../servicelearning/index.html">international service learning project</a>, independent study, or audit. It is up to the program leader what he or she would
                           like to offer, so please contact him/her directly to discuss your options. 
                        </p>
                        
                        <p><strong>PROGRAM ELIGIBILITY<a name="programeligibility" id="programeligibility"></a> <br>
                              </strong>In order to apply, you must...
                        </p>
                        
                        <ul>
                           
                           <li>be at least 18 years old at the time of departure</li>
                           
                           <li>have earned 12 college-level credits prior to the anticipated travel date</li>
                           
                           <li>have a 2.5 GPA at the time of application</li>
                           
                           <li>meet the course requirements as outlined on the program webpage (be sure to check
                              prerequisites) 
                           </li>
                           
                        </ul>
                        
                        <p><strong>SCHOLARSHIP ELIGIBILITY<a name="scholareligibility" id="scholareligibility"></a> <br>
                              </strong>In order to receive a scholarship, you must...
                        </p>
                        
                        <ul>
                           
                           <li>apply by the application deadline date above to receive the full scholarship amount</li>
                           
                           <li>be a degree-seeking student at Valencia College (you can get a scholarship after graduation
                              as long as the program runs by the following semester so, for example, students who
                              graduate in the fall term can get a scholarship for the spring term but not the summer
                              term) 
                           </li>
                           
                           <li>have not received a study abroad scholarship within the current academic year</li>
                           
                           <li>be enrolled in at least 3 additional credits other than the study abroad program in
                              the term of travel for spring (this does not apply for summer programs)
                           </li>
                           
                           <li>have completed at least one semester with Valencia within the last year</li>
                           
                           <li>Maintain a minimum GPA of 2.5 at application deadline</li>
                           
                           <li>attend the mandatory SAGE orientation session and all meetings with your program leader</li>
                           
                           <li>obtain a grade of C or better in the study abroad course</li>
                           
                           <li>submit a thank you card to your donor addressed to "My Esteemed Donor"</li>
                           
                           <li>complete the online course evaluation at the end of the program        </li>
                           
                        </ul>
                        
                        <p><strong>STEPS TO APPLY<a name="steps" id="steps"></a> </strong></p>
                        
                        <ol>
                           
                           <li>Review the program options and select a program</li>
                           
                           <li>Contact the program leader or SAGE Office if you have any questions </li>
                           
                           <li>Meet with a study abroad advisor if you have any concerns or questions about the course
                              or financial aid 
                           </li>
                           
                           <li>Complete the online application on your program webpage.&nbsp; Please use Google Chrome
                              as your Web Browser  when filling out the application.&nbsp; 
                           </li>
                           
                           <li>Receive an eligibility verification letter via email from the SAGE Office</li>
                           
                           <li>Take  the eligibility verification letter to any campus Business Office and pay the
                              $250 deposit (be sure to get a receipt)
                           </li>
                           
                           <li>Receive the program acceptance letter</li>
                           
                           <li>Add  the SAGE orientation date and all your program meeting dates to your calendar
                              and make arrangements to attend all meetings (you can find the meeting dates in  the
                              Program Booklet located on the program webpage)          
                           </li>
                           
                        </ol>
                        
                        <p><strong>NON-U.S. PASSPORT HOLDERS<a name="nonUS" id="nonUS"></a></strong></p>
                        
                        <ol>
                           
                           <li> Before you apply, verify if you need a visa (it is usually a tourist visa that you
                              will need) and what the requirements are at <a href="http://www.visahq.com" target="_blank">www.visahq.com</a>
                              
                           </li>
                           
                           <li>Contact the program country embassy to confirm the requirements</li>
                           
                           <li>You can only get conditional acceptance into a program. A place may be held for you
                              up to the program payment deadline, but you must secure the visa before we can enroll
                              you.
                           </li>
                           
                           <li>Please note that not all visas are approved and you will have to do this at your own
                              expense and risk
                           </li>
                           
                           <li>If you are at Valencia on an F or J visa, you must meet with an international advisor
                              to see if there are any travel restrictions on your visa
                           </li>
                           
                        </ol>
                        
                        <p>Neither the SAGE office nor the International Student Services office is trained to
                           handle tourist visa questions. You must contact the resources listed above. 
                        </p>
                        
                        <p><strong>DUAL ENROLLMENT STUDENTS<a name="dual" id="dual"></a></strong></p>
                        
                        <ol>
                           
                           <li> You must check in with the Dual Enrollment office prior to applying: <a href="mailto:dualenrollment@valenciacollege.edu">dualenrollment@valenciacollege.edu</a> or call 407-582-1600
                           </li>
                           
                           <li> The course must be 3 credits and your high school must accept it for credit and this
                              should be done BEFORE you apply
                           </li>
                           
                           <li> You must be 18 years old at the time of departure</li>
                           
                           <li>You do not qualify for a scholarship unless you have graduated, declared degree seeking
                              at Valencia after graduation, and have at least 12 college credits
                           </li>
                           
                        </ol>
                        
                        <p><a href="#top">Go to the top of this page </a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/short-term/how-to-apply.pcf">©</a>
      </div>
   </body>
</html>