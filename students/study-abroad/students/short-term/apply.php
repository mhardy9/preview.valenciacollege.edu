<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/short-term/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/short-term/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/short-term/">Short Term</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Short-Term Programs - 2018
                           
                           
                        </h2>
                        
                        
                        <p><strong>APPLICATIONS ARE NOW OPEN! </strong></p>
                        
                        <p>The programs listed below are part of a Valencia academic course. These short-term
                           programs are led by Valencia faculty members, are one to two weeks long, and held
                           over spring, summer, or winter break. For most programs, participants must be a Valencia
                           student although some allow transient and noncredit students to enroll as audit students.
                           Please note that some programs may have prerequisites and all participants must be
                           18 years of age or older. See the <a href="how-to-apply.html">How to Apply to Short-Term Programs</a> page for more information.
                        </p>
                        
                        <p>If you want to be considered for a program scholarship, you must be a degree-seeking
                           student at Valencia, have a minimum GPA of 2.5, have no disciplinary or academic holds,
                           and be registered in at least 3 credits for the term.
                        </p>
                        
                        <p> <strong>For more specific details about each program, click on a country flag below.</strong></p>
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">2018<a name="next" id="next"></a>
                                    
                                 </div>
                                 
                                 <div data-old-tag="th">Program Title</div>
                                 
                                 <div data-old-tag="th">Course Options</div>
                                 
                                 <div data-old-tag="th">Program Dates</div>
                                 
                                 <div data-old-tag="th">Program Provider</div>
                                 
                                 <div data-old-tag="th">Status</div>
                                 
                                 <div data-old-tag="th">Primary Program Leader Contacts</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="spain-2018-honors.html">Spain<br><img alt="Spain Flag" border="0" height="70" src="SpainFlag_003.jpg" width="106"></a><br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Spain Honors</strong></div>
                                 
                                 <div data-old-tag="td">IDH 2955<br>
                                    *Course enhanced with five hours of a Service Learning component
                                 </div>
                                 
                                 <div data-old-tag="td">March 9-16</div>
                                 
                                 <div data-old-tag="td">Study Abroad Association</div>
                                 
                                 <div data-old-tag="td"><strong>CLOSED</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="../../../../contact/Search_Detail2.cfm-ID=jmaguire1.html">Professor Jane Maguire</a><br>
                                       407-582-2228<br>
                                       East Campus, 3-103
                                    </p>
                                    
                                    <p> <a href="../../../../contact/Search_Detail2.cfm-ID=mrfoster.html">Campus Dean Michelle Foster</a><br>
                                       407-582-2008<br>
                                       East Campus 3-108F
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="costa-rica-2018.html">Costa Rica<br>
                                       <img alt="Costa Rica Flag" height="64" src="costa-rica-flag.jpg" width="106"></a></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Education in Costa Rica</strong></p>
                                    
                                    <p><strong> Sustainability in Costa Rica</strong> - <strong>WAITLIST</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>SLS 2940: Education</p>
                                    
                                    <p> SLS 2940: Sustainability<br>
                                       *Student choose one course/project
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">March 10-17</div>
                                 
                                 <div data-old-tag="td">Universidad Tcnica Nacional</div>
                                 
                                 <div data-old-tag="td"><strong>CANCELLED</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="../../../../contact/Search_Detail2.cfm-ID=jmenig.html">Professor Joe Menig</a><br>
                                       407-582-1048<br>
                                       West Campus 3-142
                                    </p>
                                    
                                    <p> <a href="../../../../contact/Search_Detail2.cfm-ID=jbenjaminfieullet.html">Professor Joy Benjamin-Fieulleteau </a><br>
                                       407-582-1495<br>
                                       West Campus, SSB 173C
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="belize-2018.html">Belize<br>                             
                                       <img alt="Dominican Republic Flag" height="70" src="Flag-of-Belize.png" width="105"></a></div>
                                 
                                 <div data-old-tag="td"><strong>Service Learning in the Belize</strong></div>
                                 
                                 <div data-old-tag="td">SLS 2940: Environmental Conservation and Dental Hygiene</div>
                                 
                                 <div data-old-tag="td">March 10-18</div>
                                 
                                 <div data-old-tag="td">Study Abroad Association</div>
                                 
                                 <div data-old-tag="td"><strong>CLOSED</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="../../../../contact/Search_Detail2.cfm-ID=bbrantley3.html">Professor Betsy Brantley</a><br>
                                       407-582-5750<br>
                                       West Campus, HSB-138
                                    </p>
                                    
                                    <p><a href="../../../../contact/Search_Detail2.cfm-ID=psandy.html">Professor Pam Sandy</a><br>
                                       407-582-1544
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="italy-greece-2018.html">Italy<br>
                                          <img alt="Italy Flag" height="70" src="Flag_of_Italy.svg.jpg" width="104"></a></p>
                                    
                                    
                                    <p><a href="italy-greece-2018.html">Greece<br>
                                          <img alt="Greece Flag" height="71" src="greece-flag.jpg" width="106"></a></p>
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Health in Italy &amp; Greece</strong></p>
                                    
                                    <p><strong>Social Entrepreneurship in Italy &amp; Greece</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>SLS 2940: Health<br>
                                       SLS 2940: Social Entrepreneurship
                                    </p>
                                    
                                    <p>*Student choose one course/project</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">April 27-May 6</div>
                                 
                                 <div data-old-tag="td">Study Abroad Association</div>
                                 
                                 <div data-old-tag="td">Open</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="../../../../contact/Search_Detail2.cfm-ID=mschreiber.html">Professor Melissa Schreiber</a><br>
                                       407-582-2246<br>
                                       East Campus, 1-128
                                    </p>
                                    
                                    <p> <a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=mbutler36">Professor Marsha Butler</a><br>
                                       407-582-1167<br>
                                       West Campus, 1-248
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="http://valenciacollege.edu/international/studyabroad/students/short-term/spain-2018.cfm">Spain<br><img alt="Spain Flag" border="0" height="70" src="SpainFlag_003.jpg" width="106"></a><br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>Humanities in Spain</strong></div>
                                 
                                 <div data-old-tag="td">HUM 2232: Renaissance &amp; Baroque Humanities</div>
                                 
                                 <div data-old-tag="td">May 1-13</div>
                                 
                                 <div data-old-tag="td">Study Abroad Association</div>
                                 
                                 <div data-old-tag="td">Open</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=jbassetti">Professor Jeremy Bassetti</a><br>
                                       407-582-2364<br>
                                       East Campus, 6-119
                                    </p>
                                    
                                    <p><a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=lsenecal">Professor Lisa Cole (Co-Leader)</a><br>
                                       407-582-1468<br>
                                       West Campus, 3-146
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://valenciacollege.edu/international/studyabroad/students/short-term/peru-2018.cfm">Peru<br><img alt="Peru Flag" border="0" height="70" src="Peru_Large_Flag_000.jpg" width="106"></a></div>
                                 
                                 <div data-old-tag="td"><strong>Environmental Science in Peru</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>EVR 1001: Environmental Science in a Developing Country</p>
                                    
                                    <p> *Course enhanced with five hours of a Service Learning component</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">April 28- May 9</div>
                                 
                                 <div data-old-tag="td">Amazon Explorama and Study Abroad Association</div>
                                 
                                 <div data-old-tag="td">Open</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=mtrone">Professor Marie Trone</a><br>
                                    407-582-4914<br>
                                    Osceola Campus, 4-424
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://valenciacollege.edu/international/studyabroad/students/short-term/uk-2018.cfm">United Kingdom<br><img alt="British Flag" border="0" height="70" src="Britishflag_000.jpg" width="108"></a></div>
                                 
                                 <div data-old-tag="td"><strong>College Mathematics in the UK</strong></div>
                                 
                                 <div data-old-tag="td">MGF 1106: College Mathematics</div>
                                 
                                 <div data-old-tag="td">May 16-31</div>
                                 
                                 <div data-old-tag="td">Horizons Du Monde</div>
                                 
                                 <div data-old-tag="td">Open</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=cmortimer2">Professor Charlotte Mortimer</a><br>
                                    407-582-1013<br>
                                    West Campus, 3-238
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="http://valenciacollege.edu/international/studyabroad/students/short-term/italy-sicily-2018.cfm">Italy<br><img alt="Italy Flag" border="0" height="70" src="Flag_of_Italy.svg.jpg" width="104"></a></div>
                                 
                                 <div data-old-tag="td"><strong>Humanities in Italy &amp; Sicily</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>HUM 2220: Greek and Rome Humanities</p>
                                    
                                    <p>*Course enhanced with one hour of a Service Learning component</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">June 1-14</div>
                                 
                                 <div data-old-tag="td">Study Abroad Association</div>
                                 
                                 <div data-old-tag="td">Open</div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=tgitto">Professor Tammy Gitto</a><br>
                                       321-682-4925<br>
                                       Osceola Campus, 2-223
                                    </p>
                                    
                                    <p> <a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=eearle">Professor Liz Earle (Co-Leader)</a><br>
                                       321-682-4121<br>
                                       Osceola Campus, 3-212
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><a href="http://valenciacollege.edu/international/studyabroad/students/short-term/france-switzerland-2018.cfm">France<br>
                                          <img alt="France flag" height="70" src="Franceflag_000.jpg" width="104"></a><br>
                                       
                                    </p>
                                    
                                    <p><a href="http://valenciacollege.edu/international/studyabroad/students/short-term/france-switzerland-2018.cfm">Switzerland<br>
                                          <img alt="Swiss flag" height="70" src="switzerlandflag_001.gif" width="104"></a><br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td"><strong>International Politics in France &amp; Switzerland</strong></div>
                                 
                                 <div data-old-tag="td">INR 2002: International Politics</div>
                                 
                                 <div data-old-tag="td">June 17-26, 2018</div>
                                 
                                 <div data-old-tag="td">KEI</div>
                                 
                                 <div data-old-tag="td">Open</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=screamer">Professor Scott Creamer</a><br>
                                    321-682-4971<br>
                                    Osceola Campus, 3-323
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/short-term/apply.pcf">©</a>
      </div>
   </body>
</html>