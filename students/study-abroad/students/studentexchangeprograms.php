<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>ENTER TITLE HERE | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/studentexchangeprograms.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>ENTER TITLE HERE</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        <h2><font color="#330033">Student Exchange Programs </font></h2>
                        
                        <p>                Valencia is currently working with the following institution on a
                           Student Exchange Programs. In this arrangement, there is a one-for-one exchange where
                           each student pays the tuition of his or her home institution, but studies for one
                           semester at the host institution. 
                        </p>
                        
                        <p>Zealand Institute of Business and Technology, DENMARK</p>
                        
                        <p><strong><a href="http://zibat.dk/about-zibat/" target="_blank">ABOUT ZEALAND INSTITUTE OF BUSINESS AND TECHNOLOGY</a> <img alt="Roskilde" height="168" src="campus_roskilde.jpg" width="274"></strong></p>
                        
                        <p><strong><a href="http://zibat.dk/oversigt-over-campusser/" target="_blank">CAMPUS LOCATIONS IN DENMARK</a> </strong></p>
                        
                        <p><strong><a href="http://zibat.dk/programmes-2/" target="_blank">PROGRAMS OF STUDY</a>: </strong>Marketing Management, Service Hospitality and Tourism Management, Multimedia Design,
                           Computer Science, Logistics Management
                        </p>
                        
                        <p><strong>SCHOLARSHIP FUNDING:</strong> The SAGE office has up to <span><strong>$4000</strong></span> to support this program. You must meet eligibility requirements.
                        </p>
                        
                        <p><strong>APPLICATION DEADLINES:</strong></p>
                        
                        <ul>
                           
                           <li>
                              <strong>Spring 2014</strong> (February 1 - end of June) - The application deadline is December 1, 2013
                           </li>
                           
                           <li>
                              <strong>Summer 2014</strong> - Not available
                           </li>
                           
                           <li>
                              <strong>Fall 2014</strong> (beginning of August - end of January) - The application deadline is May 31, 2014
                              
                           </li>
                           
                           <li>
                              <strong>Spring 2015</strong> (February 1 - end of June) - The application deadline is December 1, 2014 
                           </li>
                           
                        </ul>              
                        
                        <p><strong>APPLICATION REQUIREMENTS: </strong></p>
                        
                        <ul>
                           
                           <li>Have at least one year of college studies in the relevant program area. </li>
                           
                           <li>Have a valid passport up to 6 months past the program end date.</li>
                           
                           <li>Pay tuition fees to Valencia College.&nbsp;&nbsp; There is no ZIBAT application fee to pay.</li>
                           
                           <li>Be able to show financial support for the program duration for accommodations, air
                              and ground transportation, books, meals, activities, insurance, and incidentals.&nbsp;
                              Estimated costs per semester are $7000. 
                           </li>
                           
                           <li>Report one week before the term begins but do not arrive any earlier. </li>
                           
                           <li>The TOEFL test is not required.</li>
                           
                        </ul>              
                        
                        <p><strong>HOW TO APPLY: </strong></p>
                        
                        <ul>
                           
                           <li>Contact the SAGE office at 407-582-3188 or by email at <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a>.      
                           </li>
                           
                           <li>When you have been cleared to apply by the SAGE office, go to the following website
                              and complete the program application form:&nbsp; <a href="http://zibat.dk/how-to-apply-and-deadlines/">http://zibat.dk/how-to-apply-and-deadlines/</a> &nbsp;&nbsp;Follow the instructions on how and where to send your application. Be sure to copy
                              the SAGE office on your application.&nbsp;&nbsp; When you submit your application you must indicate
                              on the top of the first page of the application form that you are applying as:&nbsp; <em>Valencia College Exchange Program</em>. 
                           </li>
                           
                           <li> You must complete <em>the Course Pre-Approval / Consortium Agreement Form</em> for credit transfer and get the appropriate signatures.
                           </li>
                           
                        </ul>              
                        
                        <p><strong>COURSE INFORMATION: </strong></p>
                        
                        <ul>
                           
                           <li>15 U.S. credits equals 30 ECTS credits. This equates to approximately 40 hours of
                              work per week including class time, homework, projects, etc.
                           </li>
                           
                           <li>You will need to plan out the courses that you want to take in advance. Be sure to
                              talk to an academic adviser and a financial aid adviser to ensure that the courses
                              you select fit into your degree program, otherwise, you will not get sign off. 
                           </li>
                           
                           <li>You must indicate the semester and program you will be attending on the application
                              form.&nbsp;
                           </li>
                           
                        </ul>              
                        
                        <p><strong>ACCOMMODATIONS: </strong></p>
                        
                        <ul>
                           
                           <li>ZIBAT does not offer student housing, but in most cases you will get assistance with
                              finding accommodation. In some of the cities where you find a ZIBAT Campus, finding
                              housing is relatively easy, but in others, not as easy (in general – the closer to
                              Copenhagen the more difficult and more expensive)
                           </li>
                           
                           <li>Normally you have to pay the first month’s rent plus three month’s rent as a deposit.&nbsp;
                              The monthly rent for a single room with a small toilet and bath ranges from $430 to
                              $650 per month.&nbsp; 
                           </li>
                           
                           <li>For housing assistance, contact the campus contact person indicated on <a href="http://www.zibat.dk">www.zibat.dk</a> or in your information package received from your ZIBAT Campus. Be sure to let them
                              know that you are a Valencia College exchange student.
                           </li>
                           
                        </ul>              
                        
                        <p><strong>TRANSPORTATION:</strong> Students will arrive to Coppenhagen Airport:&nbsp; <a href="http://www.cph.dk/CPH/UK/ABOUT+CPH/">http://www.cph.dk/CPH/UK/ABOUT+CPH/</a> and will be picked up by a ZIBAT representative from your campus.&nbsp; ZIBAT has campuses
                           in 5 cities in the Copenhagen capital region (island of Zealand). All cities are not
                           a big city in a US context but mid sized cities in a Danish context..&nbsp; If you are
                           traveling from Copenhagen to one of the ZIBAT campus cities, you should take the train.&nbsp;
                           The ride is about 20 minutes to 1 hour long (depending on ZIBAT Campus/City location
                           your chose/study in).  Most Danes ride bikes, and in 2008 Copenhagen was declared
                           the world’s first bike city.&nbsp; They have free city bikes, bicycle taxis, guided bicycle
                           tours, and bike rentals.&nbsp; You can also rent a bike yourself to get around.
                        </p>
                        
                        <p><strong>OTHER IMPORTANT INFORMATION:</strong></p>
                        
                        <ul>
                           
                           <li>
                              
                              <p>Upon arrival students will be paired up with a Danish “buddy” to help acquaint them
                                 with the college, the city, and getting used to life in Denmark.&nbsp; Students will receive
                                 an orientation as well as a guide with basic “need to know” information.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Students are allowed to work 15 hours per week.&nbsp; The salary is approximately DKK (Danish
                                 Krone) 80-100 per hour ($1.00 = 5.20 KDD).&nbsp; Students often can find work in the service
                                 industry.&nbsp; You must pay income tax on all income earned which is approximately 40
                                 percent.
                              </p>
                              
                           </li>
                           
                           <li>
                              
                              <p>Approximately 5.4 million inhabitants of Denmark speak Danish, a Germanic language
                                 related to both English and German.&nbsp; Danish uses the same alphabet as English but
                                 with three additional letters.&nbsp; Free&nbsp; Danish language courses are offered by the College
                                 as a supplement to help in managing day-to-day life in Denmark.&nbsp; About 80 percent
                                 of the Danes speak English. 
                              </p>
                              
                           </li>
                           
                           <li> Students must be sure to get their original transcript sent to the SAGE office.&nbsp;
                              A copy will be kept on file and the original will be sent to the Admissions Department.
                              
                           </li>
                           
                        </ul>              
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/studentexchangeprograms.pcf">©</a>
      </div>
   </body>
</html>