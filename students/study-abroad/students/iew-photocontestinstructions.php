<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>$(document).ready(function(){
         	function onAfter(curr, next, opts) { 
         		$(this).children("div#transHide").show("slow");
         	}
         	function onBefore() { 
         		$("div#transHide").hide();
         	}
         	
         	$('#slideshow').hover(
         		function() { $('#slideNav').show(); },
         function() { $('#slideNav').hide(); }
         );
         	
         	$('#mainImage')
         	.cycle({
         	fx:    'fade',
         	timeout: 5000,
         before:  onBefore, 
         after:   onAfter,
         	next:   '#slidenext',
         prev:   '#slideprev'
         	});
         }); | Valencia College
      </title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/iew-photocontestinstructions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/study-abroad/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>$(document).ready(function(){
               function onAfter(curr, next, opts) { 
               $(this).children("div#transHide").show("slow");
               }
               function onBefore() { 
               $("div#transHide").hide();
               }
               
               $('#slideshow').hover(
               function() { $('#slideNav').show(); },
               function() { $('#slideNav').hide(); }
               );
               
               $('#mainImage')
               .cycle({
               fx:    'fade',
               timeout: 5000,
               before:  onBefore, 
               after:   onAfter,
               next:   '#slidenext',
               prev:   '#slideprev'
               });
               });
            </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li>$(document).ready(function(){
                  	function onAfter(curr, next, opts) { 
                  		$(this).children("div#transHide").show("slow");
                  	}
                  	function onBefore() { 
                  		$("div#transHide").hide();
                  	}
                  	
                  	$('#slideshow').hover(
                  		function() { $('#slideNav').show(); },
                  function() { $('#slideNav').hide(); }
                  );
                  	
                  	$('#mainImage')
                  	.cycle({
                  	fx:    'fade',
                  	timeout: 5000,
                  before:  onBefore, 
                  after:   onAfter,
                  	next:   '#slidenext',
                  prev:   '#slideprev'
                  	});
                  });
               </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <h2>International Education Week Photo Contest at Valencia  College</h2>
                                    
                                    <p> International Education Week is celebrated in more than 100  countries worldwide
                                       and gives international and domestic students an  opportunity to celebrate the benefits
                                       of international education and exchange.
                                    </p>
                                    
                                    <p> To celebrate International Education Week in 2013 and 2014,  Valencia College hosted
                                       a student and faculty photo contest.&nbsp; All submitted photos were required to be  original
                                       work representing a global/cross-cultural experience.&nbsp; Please see the winners below.
                                    </p>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <h2>IEW Photo Contest Winners!</h2>
                        
                        <p>Congratulations to the winners of our <strong>2nd Annual</strong> International Education Week Photo Contest at Valencia!! 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>1st Place </p>
                                    
                                    
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Zaid Anwar</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       
                                       <p><strong>"Masjid for the Common" </strong></p>
                                       
                                       <p><strong>Dubai</strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>2nd Place </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Jessica Insua </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       
                                       <p><strong>"Soul Singer" </strong></p>
                                       
                                       <p><strong>Mexico</strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>3rd Place </p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>David Matthews</p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       
                                       <p><strong>"Great Wall" </strong></p>
                                       
                                       <p><strong>China</strong></p>
                                       
                                    </div>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        <h2><strong>1st Annual International Education Week Photo Contest at Valencia!! </strong></h2>
                        
                        <h3><strong>STUDENT CATEGORY </strong></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">1st Place</div>
                                 
                                 <div data-old-tag="td">Giang Pham</div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>"Pulling Fishing Net"</strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">2nd Place</div>
                                 
                                 <div data-old-tag="td">Melissa Magee </div>
                                 
                                 <div data-old-tag="td"><strong>"The Lost City"</strong></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3rd Place </div>
                                 
                                 <div data-old-tag="td">Yesenia Cano </div>
                                 
                                 <div data-old-tag="td"><strong>"El Centro" </strong></div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">4th Place</div>
                                 
                                 <div data-old-tag="td">David Freund </div>
                                 
                                 <div data-old-tag="td"><strong>"City of Canals"</strong></div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <h3><strong>FACULTY/STAFF CATEGORY</strong></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">1st Place </div>
                                 
                                 <div data-old-tag="td">Kevin Mulholland </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>"Men of Chefchaouen" </strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">2nd Place </div>
                                 
                                 <div data-old-tag="td">Kevin Mulholland </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>"A Drink Vendor, Old Havana"</strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">3rd Place </div>
                                 
                                 <div data-old-tag="td">Karen Fowler </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>"Windows to the Past"</strong></div>
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/iew-photocontestinstructions.pcf">©</a>
      </div>
   </body>
</html>