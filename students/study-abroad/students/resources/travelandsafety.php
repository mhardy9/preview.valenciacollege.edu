<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Blank Template  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/study-abroad/students/resources/travelandsafety.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/international/studyabroad/students/resources/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internal Page Title</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/study-abroad/">Studyabroad</a></li>
               <li><a href="/students/study-abroad/students/">Students</a></li>
               <li><a href="/students/study-abroad/students/resources/">Resources</a></li>
               <li>Blank Template </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div>
                                 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 <img alt="" height="8" src="arrow.gif" width="8"> 
                                 
                                 
                                 
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          <div>
                                             
                                             
                                             
                                             
                                             
                                             
                                             <a name="navigate" id="navigate"></a>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div>Navigate</div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>  
                                                
                                             </div>
                                             
                                             
                                             
                                             
                                             <div>
                                                <span>Related Links</span>
                                                
                                                
                                                <ul>
                                                   
                                                   
                                                   <li><a href="../../../exchange/index.html">JEV</a></li>
                                                   
                                                   
                                                   <li><a href="http://international.valenciacollege.edu">ISS/CIE</a></li>
                                                   
                                                   
                                                   <li><a href="../../../../faculty/development/programs/curriculumDevelopment/index.html">Faculty Development</a></li>
                                                   
                                                   
                                                </ul>
                                                
                                             </div>
                                             
                                             
                                             
                                          </div>
                                       </div>
                                       
                                       <div>
                                          
                                          <h2>Travel and Safety </h2>
                                          
                                          <p>The following Internet resources will help you plan and prepare for your international
                                             experience abroad:
                                          </p>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   
                                                   <div>
                                                      
                                                      
                                                      <link href="../../../../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                                      
                                                      
                                                      
                                                      
                                                      <div>
                                                         
                                                         
                                                         
                                                         
                                                         <div> 
                                                            
                                                            <div>
                                                               <img alt="Featured 1" border="0" height="260" src="featured-1.png" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 2" border="0" height="260" src="featured-2.png" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                            <div>
                                                               <img alt="Featured 3" border="0" height="260" src="featured-3.png" width="770">
                                                               
                                                            </div>
                                                            
                                                            
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                      
                                                      
                                                      
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   
                                                   
                                                   
                                                   
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div>
                                             
                                             <div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Passports  &amp; Per Diems for International Travel </strong></div>
                                                   
                                                   <div>
                                                      
                                                      <p>Please note that effective September 2010, you must have a passport number when airline
                                                         tickets are purchased. Therefore, you must apply for a passport immediately once you
                                                         are accepted into a program if you do not have one. <br>
                                                         <br>
                                                         <u><a href="http://travel.state.gov/travel/cis_pa_tw/cis_pa_tw_2223.html" target="_blank">      New Requirements for Traveling Abroad</a></u><a href="http://www.state.gov/s/cpr/rls/fco/"><br>
                                                            </a><u><a href="http://travel.state.gov/passport/passport_1738.html" target="_blank">Department of State - Passport Information<br>
                                                               </a><a href="http://www.usps.com/passport/" target="_blank">US Postal Service Passport Application<br>
                                                               </a><a href="http://www.us-passport-service-guide.com/miami-passport-agency.html" target="_blank">Expedited Passports via Miami</a></u><u><a href="http://www.nafsa.org/regulatoryinformation/default.aspx?id=20314" target="_blank"><br>
                                                               </a></u><a href="http://aoprals.state.gov/content.asp?content_id=114&amp;menu_id=92" target="_blank">Per Diem by Country<br>
                                                            Meals Amount Breakdown </a><br>
                                                         <br>
                                                         
                                                      </p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Visas</strong></div>
                                                   
                                                   <div>
                                                      
                                                      <p><u><a href="http://www.usembassy.gov/" target="_blank">US Embassies and Consulates<br>
                                                               </a></u><u><a href="http://www.state.gov/s/cpr/rls/fco/" target="_blank">Foreign Consular Offices in the U.S.</a><br>
                                                            <a href="http://www.nafsa.org/regulatoryinformation/default.aspx?id=20314" target="_blank">Visas for Education Abroad<br>
                                                               </a></u><u><a href="http://www.visahq.com/" target="_blank">Visa Headquarters</a><br>
                                                            </u><u><br>
                                                            </u></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Destination Resources </strong></div>
                                                   
                                                   <div>
                                                      
                                                      <p><a href="http://www.jdpower.com/travel/ratings/airline-ratings/" target="_blank">JD Powers US Airline Ratings                      </a><a href="http://www.travel.state.gov/travel/cis_pa_tw/cis/cis_4965.html" target="_blank"><br>
                                                            </a><a href="http://www.airlinequality.com/" target="_blank">Airline and Airport Review Site</a><a href="http://www.travel.state.gov/travel/cis_pa_tw/cis/cis_4965.html" target="_blank"><br>                      
                                                            Department of State Country Information<br>
                                                            </a><a href="http://travel.state.gov/travel/living/living_1243.html" target="_blank">Department of State Tips for Living Abroad<br>
                                                            </a><a href="http://www.studentsabroad.state.gov/" target="_blank">Department of State Tips for Students Abroad<br>
                                                            </a><u><a href="http://www.lonelyplanet.com/" target="_blank">Lonelyplanet.com<br>
                                                               </a></u><u><a href="http://www.virtualtourist.com/vt/" target="_blank">Virtualtourist.com</a><br>
                                                            <a href="http://cafeabroadinprint.com/" target="_blank">Cafe Abroad</a><br>
                                                            <a href="http://www.us-passport-service-guide.com/travel-forum.html" target="_blank">International Travel Forum</a> <br>
                                                            <a href="http://www.crossculturalsolutions.org/" target="_blank">Cross-Cultural Solutions</a>                      <br>
                                                            <a href="http://www.iorworld.com/" target="_blank">IOR Global Services</a><br>
                                                            <a href="http://www.studentsabroad.com/contents.html" target="_blank">Study Abroad Student Handbook - The Center for Global Education</a></u><u><a href="http://www.eurocheapo.com" target="_blank"><br>
                                                               eurocheapo.com</a></u><u><br>
                                                            <a href="http://www.jetpunk.com/travel-quizzes.php" target="_blank">Jet Punk: Geography Quizzes</a><br>
                                                            <a href="http://www.customink.com/blog/student-guide-for-studying-abroad/" target="_blank">Travel Guide for Studying Abroad</a> <br>
                                                            <a href="http://www.keytravel.com/usa/" target="_blank">Key Travel</a> <br>
                                                            <a href="http://www.appina.travel/main.php?site=start&amp;lang=en" target="_blank">Appina Travel</a> <br>
                                                            </u></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Safety</strong></div>
                                                   
                                                   <div>
                                                      
                                                      <p><a href="http://www.travelinsurance.org/" target="_blank">World Airline Flag Carriers and Travel Safety Tips </a><br>
                                                         <a href="http://globaled.us/SAFETI/index.asp" target="_blank">Study Abroad First (SAFETI)<br>
                                                            </a><a href="http://www.greatgapyears.co.uk/SafetyWhenTravellingCategory.html" target="_blank">Safety When Traveling
                                                            </a><a href="http://travel.state.gov/travel/travel_1744.html" target="_blank"><br>
                                                            </a><a href="http://www.saraswish.org/Know_Before_You_Go_Video.htm" target="_blank">Know Before You Go<br>
                                                            </a><a href="http://www.saraswish.org/travel_safety_guidelines.htm" target="_blank">Travel Safety Guidelines - Sara's Wish Organization<br>
                                                            </a><a href="http://www.asirt.org/" target="_blank">Association for Safe International Road Travel</a> <br>
                                                         <a href="../../resources/documents/WhattheDOSCan-CantDoinCrisis.pdf">What the Dept. of State Can &amp; Can't Do in a Crisis</a> (pdf file) <br>
                                                         <a href="http://travel.state.gov/travel/travel_1744.html" target="_blank"> Department of State American Citizens Services &amp; Crisis Management </a><a href="http://travel.state.gov/travel/tips/emergencies/emergencies_1212.html" target="_blank"><br>
                                                            Department of State Emergencies &amp; Crises<br>
                                                            </a><a href="http://travel.state.gov/travel/tips/safety/safety_1180.html" target="_blank">Department of State Safety Tips<br>
                                                            </a><a href="http://travel.state.gov/travel/cis_pa_tw/pa/pa_1766.html" target="_blank">Department of State Travel Alerts<br>
                                                            </a><a href="http://travel.state.gov/travel/cis_pa_tw/tw/tw_1764.html" target="_blank">Department of State Travel Warnings<br>
                                                            </a><a href="https://travelregistration.state.gov/ibrs/ui/" target="_blank">Department of State Travel Registration<br>
                                                            </a><a href="http://travel.state.gov/travel/tips/tips_1232.html" target="_blank">Department of State Tips for Traveling Abroad<br>
                                                            </a><a href="http://www.osac.gov/" target="_blank">Overseas Security Advisory Council</a><a href="../../resources/documents/10TipsforStudentHealthandSafetyOverseas.doc.html"><br>
                                                            </a><a href="http://www.ricksteves.com/plan/tips/298scam.htm" target="_blank">Tourist Scams in Europe</a><br>
                                                         <a href="http://www.cheapflights.com" target="_blank">Cheapfligts Media </a><a href="http://www.ricksteves.com/plan/tips/298scam.htm" target="_blank">                        <br>
                                                            </a><a href="http://www.studentdebtrelief.us/knowledge-base/study-abroad-guide-for-students/" target="_blank">Study Abroad Guide for Students</a> 
                                                      </p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Your Healthcare Abroad </strong></div>
                                                   
                                                   <div><u><a href="http://www.cdc.gov/" target="_blank">Center for Disease Control </a><br>
                                                         <a href="http://travel.state.gov/travel/tips/brochures/brochures_1215.html" target="_blank">Medical Information for Americans Abroad</a></u></div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Cell Phone Access, Country Calling Codes &amp; Time Zones </strong></div>
                                                   
                                                   <div>
                                                      <p>If you want to be able to communicate overseas, you can purchase or rent a cell phone
                                                         or SIM card. Check with your current service provider for rates or you can use the
                                                         following company:<br>
                                                         <a href="http://www.telestial.com/" target="_blank">Telestial.com</a><br>
                                                         <a href="http://countrycallingcodes.com/" target="_blank">Country Calling Codes</a><br>
                                                         <a href="http://www.worldtimezone.com/" target="_blank">Time-Zone Search</a><a href="http://www.timeanddate.com/worldclock/"><br>
                                                            </a> <a href="http://www.timeanddate.com/worldclock/" target="_blank">Times Around the World </a><br>
                                                         <br>
                                                         
                                                      </p>
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Electrical Currency Standards &amp; Metric Conversions</strong></div>
                                                   
                                                   <div>
                                                      
                                                      <p><u><a href="http://kropla.com/electric2.htm" target="_blank">World Electric Guide<br>
                                                               </a></u><u><a href="http://www.sciencemadesimple.net/conversions.html" target="_blank">Science Made Simple Conversions</a></u></p>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Student ID Cards, Student Loans &amp; Health Insurance </strong></div>
                                                   
                                                   <div>
                                                      <a href="http://www.isic.org/student-card/the-isic-student-card.html" target="_blank">International Student Identification Card<br>
                                                         </a> <a href="http://internationalstudentloan.com/" target="_blank">International Student Loans</a><br>
                                                      <a href="http://www.cmi-insurance.com/" target="_blank">CMI Insurance Worldwide</a> 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Transportation &amp; Weather </strong></div>
                                                   
                                                   <div>
                                                      <a href="http://www.raileurope.com/us/rail/passes/europass_index.htm" target="_blank">Rail Europe</a><br>
                                                      <a href="http://www.cnn.com/WEATHER/index.html" target="_blank">CNN Weather by Continent</a> 
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Language Learning </strong></div>
                                                   
                                                   <div>
                                                      <p><a href="http://www.ethnologue.com/country_index.asp" target="_blank">Languages of the World <br>
                                                            </a><a href="http://www.centerforgloballanguages.com/" target="_blank">Center for Global Languages at Valencia</a> <br>
                                                         <a href="http://www.language-school-teachers.com/">LanguageSchoolTeachers.com</a> 
                                                      </p>
                                                   </div>
                                                   
                                                </div>
                                                
                                                <div>
                                                   
                                                   <div><strong>Jobs and News </strong></div>
                                                   
                                                   <div>
                                                      
                                                      <p><a href="http://www.delicious.com/valenciasage" target="_blank">Valencia SAGE's Delicious Links</a><br>
                                                         <a href="http://www.escapeartist.com/jobs/overseas1.htm" target="_blank">EscapeArtist.com<br>
                                                            </a><a href="http://www.intljobs.org/" target="_blank">International Job Finder<br>
                                                            </a><a href="http://www.jetprogramme.org/" target="_blank">Japan Exchange and Teaching Program (JET)</a><a href="http://www.intljobs.org/"><br>
                                                            </a><a href="http://www.jobsabroad.com/search.cfm" target="_blank">Jobs Abroad</a><a href="http://www.intljobs.org/"><br>
                                                            </a><a href="http://www.careerone.com.au/" target="_blank">Jobs in Australia&nbsp;</a><a href="http://www.monster.com" target="_blank"><br>
                                                            Monster.com<br>
                                                            </a><a href="http://jobregistry.nafsa.org/search/browse/" target="_blank">NAFSA – Job Registry</a><a href="http://www.overseasjobs.com/" target="_blank"><br>
                                                            Overseas Jobs</a><a href="http://www.peacecorps.gov/indexf.cfm" target="_blank"><br>
                                                            Peace Corps<br>
                                                            </a><a href="http://internationalcenter.umich.edu/swt/work/resources/workabroadmain.html" target="_blank">The University of Michigan Directory of Work Abroad </a><br>
                                                         <a href="http://www.abyznewslinks.com/" target="_blank">ABYZ News Links<br>
                                                            </a><a href="http://www.inkpot.com/news/" target="_blank">Flying Inkpots' World Newspaper Links<br>
                                                            </a><a href="http://onlinenewspapers.com/" target="_blank">OnlineNewspapers.com<br>
                                                            </a><a href="http://www.cec.org.uk/press/we/" target="_blank">This Week in Europe </a></p>
                                                      
                                                      
                                                      
                                                      
                                                   </div>  
                                                </div>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          
                                          
                                          <p><a href="travelandsafety.html#top">TOP</a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="15"></div>
                                       
                                       <div><img alt="" height="1" src="spacer.gif" width="770"></div>  
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <div><img alt="" height="1" src="spacer.gif" width="162"></div>
                              <img alt="" height="1" src="spacer.gif" width="798">
                              
                              
                              <br>  
                              
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                        </div>  
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/study-abroad/students/resources/travelandsafety.pcf">©</a>
      </div>
   </body>
</html>