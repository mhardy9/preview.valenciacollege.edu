<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Activities  | Valencia College</title>
      <meta name="Description" content="R.E.A.C.H.">
      <meta name="Keywords" content="college, school, educational, reach, activities">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/reach/activities.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/reach/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>R.E.A.C.H.</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/reach/">Reach</a></li>
               <li>Activities </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           
                           <h3><strong>Ropes Course</strong></h3>
                           
                           <p>This is a fun-filled day full of outdoor activities and teambuilding opportunities!
                              The REACH program has partnered with the Honors program and Vehicle for Change, Inc.
                              to provide students the opportunity to participate in various fun and challenging
                              activities. These include climbing a rock wall, soaring through the air with a zip
                              line, and navigating a high ropes course. 
                           </p>
                           
                           <p><a href="http://youtu.be/d1Z3DdjUl9g">Click here to view photos of the 2013-2014 Ropes Course!</a></p>
                           
                           <p><a href="http://www.youtube.com/watch?v=HgsDdR7mic4&amp;feature=youtu.be">Click here to view photos of the 2013-2014 Ropes Course!</a></p>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Teambuilding Day</strong></h3>
                           
                           <p>On this day, we bring in a facilitator to reinforce support and teamwork through fun
                              activities. Students work together to get through various challenges and brainstorm
                              how to solve problems given. 
                           </p>
                           
                           <p><a href="http://www.youtube.com/watch?v=tYa6eVk4_vU&amp;feature=youtu.be">Click here to view photos of the 2013-2014 Teambuilding Day!</a></p>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Meals of Hope Event</strong></h3>
                           
                           <p>R.E.A.C.H. students, with the help of several Valencia College clubs and organizations,
                              took the initiative to raise the $5,000.00 needed for a food packing event with various
                              fundraising events throughout the year. It turned out to be a huge success!
                           </p>
                           
                           <p>On the day of the event, R.E.A.C.H. students, student leaders, and many other volunteers
                              came together to pack 25,000 meals, which were then donated to local food pantries
                              in Osceola County. A big thank you goes out to Meals of Hope and everyone involved
                              in making this possible!
                           </p>
                           
                           
                           <p><a href="http://youtu.be/FYSmsssFHk8">Click here to view photos of the 2013-2014 Meals of Hope Event!</a></p>
                           
                           <p>To find out more about the Meals of Hope initiative, please visit their website at
                              <a href="http://meals-of-hope.org/">meals-of-hope.org</a>.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/reach/activities.pcf">©</a>
      </div>
   </body>
</html>