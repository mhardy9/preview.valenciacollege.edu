<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>R.E.A.C.H.  | Valencia College</title>
      <meta name="Description" content="R.E.A.C.H.">
      <meta name="Keywords" content="college, school, educational, reach">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/reach/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/reach/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>R.E.A.C.H.</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Reach</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              
                              
                              <p>R.E.A.C.H. (Reaching Every Academic Challenge Head-On) consists of a small group of
                                 students who take their first year of college together, with the 
                                 same courses and instructors. This sort of learning community helps students connect
                                 with their classmates and professors, <br>
                                 providing more individualized instruction, increased student success, and higher graduation
                                 rates.
                              </p>
                              	
                              			
                              <p><img src="/images/students/reach/Teamwork.JPG" alt="Reach Teamwork during Ropes Course" width="350" height="232" hspace="0" vspace="0" align="default"> 
                                 				<img src="/images/students/reach/RandyandCarlos.JPG" width="349" height="232" alt="Reach Teamwork during Ropes Course">
                                 
                              </p>	
                              
                              
                              <hr class="styled_2">
                              
                              
                              <h3><strong>How to Apply</strong></h3>
                              
                              <p>To submit our<u> online REACH application</u>, please click on the link below:
                              </p>
                              
                              <h2><a href="http://valenciacc.ut1.qualtrics.com/jfe/form/SV_0V2UtzIQJJkC8Id"><strong>LINK TO ONLINE REACH APPLICATION </strong></a></h2>
                              
                              <p>You may also contact Leslie Leadbeater at <a href="mailto:lleadbeater@valenciacollege.edu">lleadbeater@valenciacollege.edu.</a></p>
                              
                              
                              <hr class="styled_2">              
                              
                              
                              <h3><strong>Why R.E.A.C.H.?</strong></h3>
                              
                              <p><span><a href="Quotes-from-REACH-Students.php">See what our current R.E.A.C.H. students have to say.</a></span></p>
                              
                              <p>Share the college experience with a small group of classmates</p>
                              
                              <p>Get through developmental education courses faster (finish all in one term)</p>
                              
                              <p>Get through college faster (complete 24 college-level courses by the end of the first
                                 year)
                              </p>
                              
                              <p>No registration worries – all classes for the first three terms are guaranteed</p>
                              
                              <p>The program meets full financial aid requirements</p>
                              
                              
                              <hr class="styled_2">              
                              
                              
                              <h3><strong>Special Updates </strong></h3>
                              
                              <p>* On March, 20th, 2014, R.E.A.C.H. student Alexandrea Castro had the unique opportunity
                                 of meeting President Barrack Obama on his visit to Valencia College. Follow the link
                                 below to read her exciting story. This is truly a great example of how hard work and
                                 dedication can really pay off.  Congratulations Alex! 
                              </p>
                              
                              <p><a href="http://wp.valenciacollege.edu/thegrove/reflections-on-president-obamas-visit-to-valencia/">R.E.A.C.H. Student Alexandrea Castro meets President Obama</a>. 
                              </p>
                              
                              
                              <hr class="styled_2">              
                              
                              
                              
                              <h3><strong>Related Links</strong></h3>
                              
                              <ul>
                                 
                                 <li><a href="../../admissions-records/index.php">Admissions</a></li>
                                 
                                 <li><a href="../../assessments/index.php">Assessments</a></li>
                                 
                              </ul>              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/reach/index.pcf">©</a>
      </div>
   </body>
</html>