<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Quotes  | Valencia College</title>
      <meta name="Description" content="R.E.A.C.H.">
      <meta name="Keywords" content="college, school, educational, reach, student, quotes">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/reach/quotes-from-reach-students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/reach/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>R.E.A.C.H.</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/reach/">Reach</a></li>
               <li>Student Quotes </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>
                           
                           <div><img src="/students/reach/arrow.gif" alt="" width="8" height="8"><span>We've asked our current R.E.A.C.H. students to describe what it's been like to be
                                 a part of the program so far, and here's just some of what they had to say!</span></div>
                           
                           <blockquote>
                              
                              <p><em> "Being in the R.E.A.C.H. program is a very good experience. I love the fact that
                                    we all got the opportunity to meet new people and actually form good&nbsp;relationships.
                                    I&nbsp;also enjoy the way we can all relate to one another."</em></p>
                              
                           </blockquote>
                           
                           <blockquote>
                              
                              <p><em>"I like that we can all come together as a class for our courses. I also believe that
                                    being in one group has made us like a big family. I have been getting good grades
                                    and this experience has helped me have better success in college." </em></p>
                              
                              <p><em>"R.E.A.C.H. is a team-building group. We all work together to help each other succeed.
                                    When one of us fails, we all jump in to and try to help. That's my favorite thing
                                    about R.E.A.C.H." </em></p>
                              
                              <p><em>"I am a current R.E.A.C.H. student, and this was one of the best opportunities I have
                                    ever taken. Their professors really care about us. R.E.A.C.H. is like a family. The
                                    professors dedicate all of their time and effort just for us to succeed. I love R.E.A.C.H.
                                    and everything it has to offer. I cannot thank them enough because I would not be
                                    where I am with my grades today without them." </em></p>
                              
                              <p><em>"What I like about the program is that we are a small group and get along very well...I
                                    have learned a lot in this program. The activities we do are fun and educational!"
                                    </em></p>
                              
                           </blockquote>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <div>&nbsp;</div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/reach/quotes-from-reach-students.pcf">©</a>
      </div>
   </body>
</html>