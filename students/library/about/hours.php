<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/about/hours.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/about/">About</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        
                        <h2>Locations and Hours</h2>
                        
                        
                        
                        <h3>Library Schedule</h3>
                        
                        
                        <div>
                           
                           
                           <h3>Closed &amp; Limited Service Dates</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Libraries are closed on dates that the College is closed and also on Faculty &amp; Staff
                                    Learning Day (held annually in February). In addition, library hours and services
                                    may be limited on faculty work days and other dates that the college is open between
                                    semesters. For further information, see the current <a href="../../calendar/index.html">Academic Calendar</a>. 
                                 </p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <p><strong>Events</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="th">
                                             <p><strong>Dates </strong></p>
                                          </div>
                                          
                                          <div data-old-tag="th">
                                             <p><strong>Days</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="th">
                                             <p><strong>Library Status </strong></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Labor Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>September 4, 2017</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>College Night I </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>October 10, 2017</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Open with regular services at    East, West and Winter Park / Open with limited services
                                                at Osceola, Poinciana and Lake    Nona 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>College Night II </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>October 12, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Open with regular services at    Osceola, Poinciana and Lake Nona / Open with limited
                                                services at East, West and Winter    Park
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thanksgiving Break</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>November 22 â€“ 26, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday - Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Between Semesters </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>December 18 - 20, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday - Tuesday </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Limited hours &amp; services</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Winter Break</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>December 21, 2017 - January 1,    2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday - Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Between Semesters</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>January 2-7, 2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday-Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Limited hours &amp; services</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Martin Luther King, Jr. Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>January 15, 2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty &amp; Staff Learning Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>February 9, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Friday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Spring Break</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>March 12-18, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday - Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Between Semesters </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>April 30- May 6, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday - Sunday </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Limited hours &amp; services</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Memorial Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>May 28, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Independence Day </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>July 4, 2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Library Full Calendar</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>For further information, see the current <a href="../../calendar/index.html">Academic Calendar</a>. 
                                 </p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">
                                             <p><strong>Day / Date </strong></p>
                                          </div>
                                          
                                          <div data-old-tag="th">
                                             <p><strong>Information</strong></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, August 2, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of summer hours for 2017</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday, August 3, 2017</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between    semesters hours begin and extend through Sunday, August
                                                27, 2017 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, August 28, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>First day of fall term; fall hours    begin </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, September 4, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Labor Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday, October 10, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>College Night @ Osceola, Poinciana and Lake    Nona. Osceola, Poinciana and Lake Nona
                                                libraries have limited services. 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday, October 12, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>College Night @ East, West and    Winter Park. East, West and Winter Park libraries
                                                have limited services. 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, November 22 - Sunday,    November 26, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Thanksgiving Break </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, December 4, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Week prior to final exams; check    your campus library for information on extended
                                                hours for this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, December 11, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Final exams begin; check your    campus library for information on extended hours
                                                for this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Sunday, December 17, 2017 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of fall hours for 2017</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, December 18, 2017</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between    semesters hours begin and extend through Wednesday, December
                                                20, 2017 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday, December 21, 2017 -    Monday, January 1, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Winter Break </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday, January 2, 2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between    semesters hours begin and extend through Sunday, January
                                                7, 2018 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, January 8, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>First day of spring term; spring    hours begin </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, January 15, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Martin Luther King, Jr.    Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Friday, February 9, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Faculty &amp; Staff    Learning Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, March 12, 2018 - Sunday,    March 18, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Spring Break </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, April 16, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Week prior to final exams; check    your campus library for information on extended
                                                hours for this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, April 23, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Final exams begin; check your    campus library for information on extended hours
                                                for this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Sunday, April 29, 2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of spring hours for 2018</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, April 30, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between    semesters hours begin and extend through Sunday, May
                                                6, 2018 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, May 7, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>First day of summer term; summer    hours begin </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, May 28, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Memorial Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, July 4, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Independence Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, August 1, 2018</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of summer hours for 2018 </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday, August 2, 2018 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between    semesters hours begin and extend through Sunday, August
                                                26, 2018 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        <h3>Library Hours by Term</h3>
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>Fall and Spring</h3>
                              
                              <h4>East</h4>
                              
                              <p>Bldg 4, 2nd Floor<br>
                                 407-582-2459
                              </p>
                              
                              <p> Monday - Thursday: 7am to 10pm<br>
                                 Friday: 7am to 8pm<br>
                                 Saturday: 8am to 4pm<br>
                                 Sunday: 2pm to 8pm              
                              </p>
                              
                              <h4>Lake Nona</h4>
                              
                              <p>Bldg 1, Rm 330<br>
                                 407-582-7107
                              </p>
                              
                              <p>Monday - Thursday: 8am to 9pm<br>
                                 Friday: 8 am to 5pm
                              </p>
                              
                              <h4>Osceola</h4>
                              
                              <p>Bldg 4, Rm 202<br>
                                 407-582-4155
                              </p>
                              
                              <p>                Monday - Thursday: 7am to 10pm<br>
                                 Friday: 7am to 5pm<br>
                                 Saturday: 8am to 12pm
                              </p>
                              
                              <h4>Poinciana</h4>
                              
                              <p>Rm 331<br>
                                 407-582-6027
                                 
                              </p>
                              
                              <p>Monday - Thursday:  8am-8pm<br>
                                 Friday:   8am-5pm<br>
                                 Saturday: 8am-12pm
                              </p>
                              
                              <h4>West</h4>
                              
                              <p>Bldg 6<br>
                                 407-582-1574
                              </p>
                              
                              <p>Monday - Thursday: 7:30am to 10pm<br>
                                 Friday: 7:30am to 5pm<br>
                                 Saturday: 9am to 1pm<br>
                                 Sunday: 2pm to 6pm
                              </p>
                              
                              <h4>Winter Park</h4>
                              
                              <p>Bldg 1, Rm 140<br>
                                 407-582-6814                <br>
                                 
                              </p>
                              
                              <p>Monday - Thursday: 8am to 7pm<br>
                                 Friday: 8am to 5pm<br>
                                 
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Summer</h3>
                              
                              <p>These hours will be in effect from Monday, May 7, 2018 through Wednesday, August 1,
                                 2018.
                              </p>
                              
                              <h4>East</h4>
                              
                              <p>Bldg 4, 2nd Floor              <br>
                                 407-582-2459
                              </p>
                              
                              <p>Monday - Thursday: 7am to 10pm<br>
                                 Friday: 7am to 12pm; note exceptions below:
                              </p>
                              
                              <p> *Open 7:30 am to <strong>8 pm</strong> Friday, July 21, 2017 and Friday, July 28, 2017
                              </p>
                              
                              <p><br>
                                 Saturday: 8am to 2pm
                              </p>
                              
                              <h4>Lake Nona</h4>
                              
                              <p>Bldg 1, Rm 330<br>
                                 407-582-7107                
                              </p>
                              
                              <p>Monday - Thursday: 8am to 9pm<br>
                                 Friday: 8 am to 12pm; note exceptions below:
                              </p>
                              
                              <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 21, 2017 and Friday, July 28, 2017              
                              </p>
                              
                              <h4>Osceola</h4>
                              
                              <p>Bldg 4, Rm 202<br>
                                 407-582-4155
                              </p>
                              
                              <p>Monday - Thursday: 7am to 10pm<br>
                                 Friday: 7am to 12pm; note exceptions below:
                              </p>
                              
                              <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 21, 2017 and Friday, July 28, 2017
                              </p>
                              
                              <p> <br>
                                 Saturday: 8am to 12pm
                              </p>
                              
                              <h4>Poinciana</h4>
                              
                              <p>Rm 331<br>
                                 407-582-6027
                              </p>
                              
                              <p>Monday - Thursday: 7am to 10pm<br>
                                 Friday: 7am to 12pm; note exceptions below:
                              </p>
                              
                              <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 21, 2017 and Friday, July 28, 2017
                              </p>
                              
                              <p> <br>
                                 Saturday: 8am to 12pm              
                              </p>
                              
                              <h4>West</h4>
                              
                              <p>Bldg 6<br>
                                 407-582-1574
                              </p>
                              
                              <p>Monday - Thursday: 7:30am to 10pm<br>
                                 Friday: 7:30am to 12pm; note exceptions below:
                              </p>
                              
                              <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 21, 2017 and Friday, July 28, 2017
                              </p>
                              
                              <h4>Winter Park</h4>
                              
                              <p>Bldg 1, Rm 140<br>
                                 407-582-6814
                              </p>
                              
                              <p>Monday - Thursday: 8am to 7pm<br>
                                 Friday: 8am to 12pm; note exceptions below:
                              </p>
                              
                              <p>*Open 8:00 am to <strong>5 pm</strong> Friday, July 21, 2017 and Friday, July 28, 2017
                              </p>
                              
                              <h4>
                                 <br>
                                 
                              </h4>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Between Semesters</h3>
                              
                              <p>These hours will be in effect August 3-27, 2017; December 18-20, 2017; January 2-7,
                                 2018; and April 29-May 6, 2018.
                              </p>
                              
                              <h4>East</h4>
                              
                              <p>Bldg 4, 2nd Floor<br>
                                 407-582-2459
                              </p>
                              
                              <p>Monday - Friday: 8am to 5pm<br>
                                 Saturday-Sunday: Closed 
                              </p>
                              
                              <h4>Lake Nona</h4>
                              
                              <p>Bldg 1, Rm 330<br>
                                 407-582-7107
                              </p>
                              
                              <p>Monday - Thursday: 8am to 5pm<br>
                                 Friday: 8 am to 12pm<br>
                                 Saturday-Sunday: Closed 
                              </p>
                              
                              <h4>Osceola</h4>
                              
                              <p>Bldg 4, Rm 202<br>
                                 407-582-4155
                              </p>
                              
                              <p>Monday - Friday: 8am to 5pm<br>
                                 Saturday-Sunday: Closed 
                              </p>
                              
                              <h4>Poinciana</h4>
                              
                              <p>Rm 331<br>
                                 407-582-6027
                              </p>
                              
                              <p>Monday - Friday: 8am to 5pm<br>
                                 Saturday-Sunday: Closed 
                              </p>
                              
                              <h4>West</h4>
                              
                              <p>Bldg 6<br>
                                 407-582-1574
                              </p>
                              
                              <p>Monday - Friday: 8am to 5pm<br>
                                 Saturday-Sunday: Closed 
                              </p>
                              
                              <h4>Winter Park</h4>
                              
                              <p>Bldg 1, Rm 140<br>
                                 407-582-6814
                              </p>
                              
                              <p>Monday - Friday: 8am to 5pm<br>
                                 Closed: 12pm to 1pm<br>
                                 Saturday-Sunday: Closed 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/about/hours.pcf">©</a>
      </div>
   </body>
</html>