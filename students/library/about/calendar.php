<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/about/calendar.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/about/">About</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        
                        <h2>Calendar</h2>
                        
                        
                        <h3>Library Schedule</h3>
                        
                        
                        <div>
                           
                           
                           <h3>Closed &amp; Limited Service Dates</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Libraries are closed on dates that the College is closed and also on Faculty &amp; Staff
                                    Learning Day (held annually in February). In addition, library hours and services
                                    may be limited on faculty work days and other dates that the college is open between
                                    semesters. For further information, see the <a href="../../calendar/documents/15-16Approved.pdf.html">2015-2016 Academic Calendar. </a></p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Events</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Dates </strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Days</strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Library Status </strong></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Labor Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>September 7, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Closed</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>College Night I </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>October 6, 2015</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Open with regular services at East, West and Winter Park / Open with limited services
                                                at Osceola and Lake Nona 
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>College Night II </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>October 8, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Open with regular services at Osceola and Lake Nona / Open with limited services at
                                                East, West and Winter Park
                                             </p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thanksgiving Break</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>November 25 – 29, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday - Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Closed</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Between Semesters </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>December 21 - 22, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday - Tuesday </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Limited hours &amp; services</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Winter Break</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>December 23, 2015 - January 3, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday - Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Closed</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Between Semesters</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>January 4-10, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday-Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Limited hours &amp; services</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Martin Luther King, Jr. Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>January 18, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Closed</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty &amp; Staff Learning Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>February 12, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Friday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Closed</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Spring Break</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>March 7-13, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday - Sunday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Closed</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Between Semesters </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>May 2-8, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday - Sunday </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             
                                             <p>Limited hours &amp; services</p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Memorial Day</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>May 30, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Independence Day </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>July 4, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Monday </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed</p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <h3>Library Full Calendar</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>For further information, see the <a href="../../calendar/documents/15-16Approved.pdf.html">2015-2016 Academic Calendar. </a></p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Day / Date </strong></p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p><strong>Information</strong></p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday, August 4, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of summer hours for 2015</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, August 5, 2015</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between semesters hours begin and extend through Sunday, August
                                                30, 2015 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, August 31, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>First day of fall term; fall hours begin </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, September 7, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Labor Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday, October 6, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>College Night @ Osceola and Lake Nona. Osceola and Lake Nona libraries have limited
                                                services. 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Thursday, October 8, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>College Night @ East, West and Winter Park. East, West and Winter Park libraries have
                                                limited services. 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, November 25, 2015 - Sunday, November 29, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Thanksgiving Break </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, December 7, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Week prior to final exams; check your campus library for information on extended hours
                                                for this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, December 14, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Final exams begin; check your campus library for information on extended hours for
                                                this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Sunday, December 20, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of fall hours for 2015</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, December 21, 2015</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between semesters hours begin and extend through Tuesday, December
                                                22, 2015 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, December 23, 2015 - Sunday, January 3, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Winter Break </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, January 4, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between semesters hours begin and extend through Sunday, January
                                                10, 2016 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, January 11, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>First day of spring term; spring hours begin </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, January 18, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Martin Luther King, Jr. Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Friday, February 12, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Faculty &amp; Staff Learning Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, March 7, 2016 - Sunday, March 13, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Spring Break </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, April 18, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Week prior to final exams; check your campus library for information on extended hours
                                                for this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, April 25, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Final exams begin; check your campus library for information on extended hours for
                                                this week 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Sunday, May 1, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of spring hours for 2016</p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, May 2, 2015 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between semesters hours begin and extend through Sunday, May 8,
                                                2016 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, May 9, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>First day of summer term; summer hours begin </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, May 30, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Memorial Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Monday, July 4, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Closed for Independence Day </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Tuesday, August 2, 2016</p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Last day of summer hours for 2016 </p>
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="td">
                                             <p>Wednesday, August 3, 2016 </p>
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <p>Faculty work day; between semesters hours begin and extend through Sunday, August
                                                28, 2016 
                                             </p>
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                        </div>
                        
                        
                        <h3>Library Hours by Term</h3>
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>Fall and Spring</h3>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>East</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-2459</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>
                                          Friday: 7am to 8pm<br>
                                          Saturday: 8am to 4pm<br>
                                          Sunday: 2pm to 8pm
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Lake Nona</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 1, Rm 330</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-7107</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 8am to 9pm<br>
                                          Friday: 8 am to 5pm
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Osceola</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 4, Rm 202</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-4155</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>
                                          Friday: 7am to 5pm<br>
                                          Saturday: 8am to 12pm
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>West</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 6</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1574</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7:30am to 10pm<br>
                                          Friday: 7:30am to 5pm<br>
                                          Saturday: 9am to 1pm<br>
                                          Sunday: 2pm to 6pm
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Winter Park</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 1, Rm 140</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-6814</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>
                                          Friday: 8am to 5pm
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Summer</h3>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">These hours are in effect through August 4, 2015. For Summer 2016: May 9 - August
                                          2, 2016.*
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>East</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-2459</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Monday - Thursday: 7am to 10pm<br>
                                             Friday: 7am to 12pm; note exceptions below:
                                          </p>
                                          
                                          <p> *Open  7:30 am to <strong>8 pm</strong> Friday, July 24, 2015 and Friday, July 31, 2015
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>8 pm</strong> Friday, July 22, 2016 and Friday, July 29, 2016 
                                          </p>
                                          
                                          <p><br>
                                             Saturday: 8am to 2pm
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Lake Nona</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 1, Rm 330</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-7107</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Monday - Thursday: 8am to 9pm<br>
                                             Friday: 8 am to 12pm; note exceptions below:
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 24, 2015 and Friday, July 31, 2015
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 22, 2016 and Friday, July 29, 2016
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Osceola</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 4, Rm 202</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-4155</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Monday - Thursday: 7am to 10pm<br>
                                             Friday: 7am to 12pm; note exceptions below:
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 24, 2015 and Friday, July 31, 2015
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 22, 2016 and Friday, July 29, 2016 
                                          </p>
                                          
                                          <p> <br>
                                             Saturday: 8am to 12pm
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>West</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 6</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1574</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Monday - Thursday (1st Floor): 7:30am to 10pm<br>
                                             Monday - Thursday (2nd Floor): 7:30am to 9pm<br>
                                             Friday: 7:30am to 12pm; note exceptions below:
                                          </p>
                                          
                                          <p>*Open  7:30 am to <strong>5 pm</strong> Friday, July 24, 2015 and Friday, July 31, 2015
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 22, 2016 and Friday, July 29, 2016 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Winter Park</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 1, Rm 140</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-6814</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">
                                          
                                          <p>Monday - Thursday: 8am to 7pm<br>
                                             Friday: 8am to 12pm; note exceptions below:
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 24, 2015 and Friday, July 31, 2015
                                          </p>
                                          
                                          <p>*Open 7:30 am to <strong>5 pm</strong> Friday, July 22, 2016 and Friday, July 29, 2016 
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Between Semesters</h3>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">These hours  are in effect August 5-30, 2015, December 21 - 22, 2015, January 4-10,
                                          2016, May 2-8, 2016, and August 3-28, 2016. Services may be limited on these dates;
                                          contact your campus library for details.
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>East</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-2459</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Friday: 8am to 5pm</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Saturday-Sunday: Closed </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Lake Nona</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 1, Rm 330</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-7107</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 8am to 5pm<br>
                                          Friday: 8 am to 12pm
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Saturday-Sunday: Closed </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Osceola</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 4, Rm 202</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-4155</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Friday: 8am to 5pm</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Saturday-Sunday: Closed </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>West</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 6</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1574</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Friday: 8am to 5pm</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Saturday-Sunday: Closed </div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Winter Park</strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Bldg 1, Rm 140</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-6814</div>
                                       
                                    </div>
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Friday: 8am to 5pm<br>
                                          Closed: 12pm to 1pm
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Saturday-Sunday: Closed </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/about/calendar.pcf">©</a>
      </div>
   </body>
</html>