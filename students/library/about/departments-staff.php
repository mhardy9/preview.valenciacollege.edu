<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/about/departments-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>
               			Library
               		
            </h1>
            <p> </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/about/">About</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30" role="main">
                  			
                  <div class="row">
                     				
                     <h2>Departments &amp; Staff</h2>
                     				
                     <div class="box_style_4">
                        <p>Jump to:</p>
                        					
                        <ul>
                           					
                           <li><a href="#east">East Campus</a></li>
                           						
                           <li><a href="#lakenona">Lake Nona Campus</a></li>
                           						
                           <li><a href="#osceola">Osceola Campus</a></li>
                           						
                           <li><a href="#poinciana">Poinciana Campus</a></li>
                           						
                           <li><a href="#west">West Campus</a></li>
                           						
                           <li><a href="#winterpark">Winter Park Campus</a></li>
                           					
                        </ul>
                        					
                     </div>
                     				<a id="east"></a>
                     				
                     <h3>East Campus Departments &amp; Staff</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4">
                           						
                           <h3>Administration</h3>
                           						
                           <ul class="list_staff">
                              							
                              <li>
                                 								
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-male-thumb.png" alt="No Picture Available"></figure>
                                 								
                                 <h4><a href="mailto:lbass11@valenciacollege.edu">Leonard Bass</a></h4>
                                 								
                                 <p>Dean of Learning Support<br>407-582-2745
                                 </p>
                                 									
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:jdiaz68@valenciacollege.edu">Jen Diaz</a></h4>
                                 									
                                 <p>Library Office Systems Manager<br>407-582-2467
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:dmckay5@valenciacolle.edu">Deanna McKay </a></h4>
                                 									
                                 <p>Staff Assistant II <br>407-582-2021
                                 </p>
                                 								
                              </li>  
                              								
                           </ul>
                           
                           							
                        </div>
                        						
                        <div class="col-md-4">
                           							
                           <h3>Circulation</h3>
                           							
                           <ul class="list_staff">
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4>East Library Circulation Desk</h4>
                                 									
                                 <p>407-582-2459</p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:mmoreno@valenciacollege.edu">Maria Moreno</a></h4>
                                 									
                                 <p>Library Services Supervisor<br>407-582-2463
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:aalberty@valenciacollege.edu">Francisco Quintero</a></h4>
                                 									
                                 <p> Emerging Technology Library Specialist <br>407-582-2014
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4> <a href="mailto:aalberty@valenciacollege.edu">Ana Alberty </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2461
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:aalberty@valenciacollege.edu">Jaylene Alarcon </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:eatanelov@valenciacollege.edu">Eka Atanelov</a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:mayala13@valenciacollege.edu">Michael Ayala</a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:adavis103@valenciacollege.edu">Hawa Bawaney </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:cblanco11@valenciacollege.edu">Catalina Blanco </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4> <a href="mailto:aalberty@valenciacollege.edu">Kelsie Cox </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:rponsonby@valenciacollege.edu">Adriana Inamagua </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              								
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 									
                                 <h4><a href="mailto:aomeben@valenciacollege.edu">Straunje Nelson </a></h4>
                                 									
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 								
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:aomeben@valenciacollege.edu">Austine Omeben </a></h4>
                                 							
                                 <p>Library Assistant<br>407-582-2459
                                 </p>
                                 						
                              </li>
                              						
                           </ul>
                           				
                        </div>
                        				
                        <div class="col-md-4">
                           					
                           <h3>Reference</h3>
                           					
                           <ul class="list_staff">
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4>East Library Reference Desk</h4>
                                 							
                                 <p>407-582-2456</p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:Ddalrymple1@valenciacollege.edu">Diane Dalrymple</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2887
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:Eheintzelman@valenciacollege.edu">Erich Heintzelman</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2886
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:Cmoore71@valenciacollege.edu">Courtney Moore</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2883
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:cwettstein@valenciacollege.edu">Chris Wettstein</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2888
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:vbasco@valenciacollege.edu">Buenaventura Basco</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:hbodiford@valenciacollege.edu">Heather Bodiford</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:bcalhoun3@valenciacollege.edu">Brian Calhoun</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:rdavis61@valenciacollege.edu">Reginald Davis</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:elindsay@valenciacollege.edu">Josette Kubicki </a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:elindsay@valenciacollege.edu">Elazer    Lindsay</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:speterson11@valenciacollege.edu">Sonja    Mollison</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:bray@valenciacollege.edu">Barbara    Ray</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:mrickelman@valenciacollege.edu">Mary    Rickelman</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:jrodeheffer@valenciacollege.edu">Jane    Rodeheffer</a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              						
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 							
                                 <h4><a href="mailto:bray@valenciacollege.edu">Michael Taylor </a></h4>
                                 							
                                 <p>Librarian<br>407-582-2456
                                 </p>
                                 						
                              </li>
                              					
                           </ul>
                           				
                        </div>
                        			
                     </div>
                     				
                     <hr>
                     				<a id="lakenona"></a>
                     				
                     <h3>Lake Nona Campus</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4">
                           
                           <h4>Administration</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:mblackburn4@valenciacollege.edu">Michael Blackburn </a></h4>
                                 
                                 <p>Manager of Learning Support Services <br>407-582-7222
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:cdavella@valenciacollege.edu">Carol    D'Avella</a></h4>
                                 
                                 <p>Administrative Assistant<br>407-582-7111
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-4">
                           
                           <h4>Circulation and Reserves</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:lbrowning2@valenciacollege.edu">Laura Browning</a></h4>
                                 
                                 <p>Library Services Supervisor<br>407-582-7114
                                 </p>
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dbell@valenciacollege.edu">Denise Bell </a></h4>
                                 
                                 <p>Administrative Assistant<br>407-582-7107
                                 </p>
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dholloway1@valenciacollege.edu">Dewanna Holloway </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-7107
                                 </p>
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:alerch@valenciacollege.edu">Amber Lerch </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-7107
                                 </p>
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:anavarro17@valenciacollege.edu">Anna Navarro </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-7107
                                 </p>
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:asilva56@valenciacollege.edu">Andrea Silva </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-7107
                                 </p>
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:thinton@valenciacollege.edu">Travis Hinton </a></h4>
                                 
                                 <p>Library Assistant <br>407-582-7107
                                 </p>
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-4">
                           
                           <h4>Reference</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:kbest7@valenciacollege.edu">Karene    Best</a></h4>
                                 
                                 <p>Librarian <br>407-582-4315
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:jsettles@valenciacollege.edu">Jenna    Settles </a></h4>
                                 
                                 <p>Librarian <br>407-582-4853
                                 </p>
                                 
                                 
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <hr>
                     				<a id="osceola"></a>
                     				
                     <h3>Osceola Campus</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4">
                           
                           <h4>Administration</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:lshephard@valenciacollege.edu">Landon    Shephard</a></h4>
                                 
                                 <p>Dean of Learning Support<br>407-582-4877
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:lleadbeater@valenciacollege.edu">Leslie    Leadbeater</a></h4>
                                 
                                 <p>Administrative Assistant<br>407-582-4915
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-4">
                           
                           <h4>Circulation    and Reserves</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4>
                                    <a href="mailto:mrhoads@valenciacollege.edu">Marcie Rhoads </a></h4>
                                 
                                 <p>Library    Specialist<br>407-582-4153
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:jdiaz72@valenciacollege.edu">Jose Diaz </a></h4>
                                 
                                 <p>Library    Assistant<br>407-582-4155
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4>Akilah Reeder</h4>
                                 
                                 <p>Library Assistant<br>407-582-4155
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4>Chantal Washington</h4>
                                 
                                 <p>Library Assistant<br>407-582-4155
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-4">
                           
                           <h4>Reference</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:sdockray@valenciacollege.edu">Sarah    Dockray </a></h4>
                                 
                                 <p>Librarian <br>407-582-4156
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:jsettles@valenciacollege.edu">Jenna    Settles </a></h4>
                                 
                                 <p>Librarian <br>407-582-4853
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:calivingston@valenciacollege.edu">Cheryl Ann    Livingston</a></h4>
                                 
                                 <p>Librarian<br>407-582-4154
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4>Brendan Kalish</h4>
                                 
                                 <p>Librarian <br>407-582-4154
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:bkaniecki@valenciacollege.edu">Betsy Kaniecki </a></h4>
                                 
                                 <p>Librarian <br>407-582-4154
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dlogan8@valenciacollege.edu">Daniel Logan </a></h4>
                                 
                                 <p>Librarian <br>407-582-4154
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:Judith.Kuhns@ucf.edu">Judy Kuhns</a></h4>
                                 
                                 <p>UCF Librarian<br>407-582-4344
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <hr>
                     				<a id="poinciana"></a>
                     				
                     <h3>Poinciana Campus</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4">
                           
                           <h4>Administration</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:jaustin27@valenciacollege.edu">Joshua Austin</a></h4>
                                 
                                 <p>Manager, Learning Support<br>407-582-6155 
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        					
                        <div class="col-md-4">
                           <h4>Circulation</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dspence4@valenciacollege.edu">Debra Spence</a></h4>
                                 
                                 <p>Library Specialist<br>407-582-6026
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dholloway1@valenciacollege.edu">Dewanna Holloway</a></h4>
                                 
                                 <p>Library Assistant<br>407-582-6027
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4>
                                    <p dir="auto"><a href="mailto:jperez219@valenciacollege.edu">Jeannette Perez</a></p>
                                 </h4>
                                 
                                 <p>Library Assistant<br>407-582-6027
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-4"> 
                           
                           <h4>Reference</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:kbest7@valenciacollege.edu">Karene Best</a></h4>
                                 
                                 <p>Librarian<br>407-582-6025
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     				
                     <hr>
                     				<a id="west"></a>
                     				
                     <h3>West Campus</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4">
                           
                           <h4>Administration</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:kreilly5@valenciacollege.edu">Karen    Reilly</a></h4>
                                 
                                 <p>Dean    of Learning Support<br>407-582-1810
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:rsmith257@valenciacollege.edu">Ruth Smith </a></h4>
                                 
                                 <p>Library Director <br>407-582-1601
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:jrodriguez237@valenciacollege.edu">Jeannie    Rodriguez</a></h4>
                                 
                                 <p>Library    Office Systems Manager<br>407-582-1210
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           						
                           <hr>
                           						 
                           <h4>Acquisitions &amp; Technical Services</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:jcodding@valenciacollege.edu">Jennifer Codding </a></h4>
                                 
                                 <p>Manager of Library Cataloging &amp; Acquisitions <br>407-582-1270
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:sling@valenciacollege.edu">Susan    Ling</a></h4>
                                 
                                 <p>Accounting Clerk <br>407-582-1213
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:smarchant@valenciacollege.edu">Stacy Marchant </a></h4>
                                 
                                 <p>Cataloging Librarian <br>407-582-1221
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        					
                        <div class="col-md-4">
                           
                           <h4>Circulation</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:mcarlson@valenciacollege.edu">Marcia Carlson</a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1451
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:kgresham@valenciacollege.edu">Kaleema Gresham </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-5407
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:aspruce@valenciacollege.edu">Alicia Spruce</a></h4>
                                 
                                 <p>Library Services Supervisor<br>407-582-1653
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:wriosalmestica@valenciacollege.edu">Wanda Rios Almestica </a></h4>
                                 
                                 <p>Library Assistant <br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:aborzacchinicastr@valenciacollege.edu">Ana Borzacchini</a></h4>
                                 
                                 <p> Library Assistant<br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:kcoleman22@valenciacollege.edu">Kendyll Coleman </a></h4>
                                 
                                 <p>Library Assistant <br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:ddixonii@valenciacollege.edu">Davey Dixon </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dparillo@mail.valenciacollege.edu">Daryl Parillo </a></h4>
                                 
                                 <p>Library    Assistant<br>407-582-1574
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:eperez147@valenciacollege.edu">Emilia M. Perez</a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:rmanzanopina@valenciacollege.edu">Roberto Manzano Pina</a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:mtaylor108@mail.valenciacollege.edu">Miranda Taylor </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1574
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:atomlinson6@valenciacollege.edu">Angela Tomlinson </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:svazquez17@valenciacollege.edu">Stephanie Vazquez</a></h4>
                                 
                                 <p> Library Assistant<br>407-582-1707
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:vdyer1@valenciacollege.edu">Vicki Witz </a></h4>
                                 
                                 <p>Library Assistant<br>407-582-1574
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        
                        <div class="col-md-4">
                           
                           <h4>Reference</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:ncumberbatch@valenciacollege.edu">Nardia Cumberbatch </a></h4>
                                 
                                 <p>Librarian<br>407-582-1528
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:lking@valenciacollege.edu">Beth King</a></h4>
                                 
                                 <p>Librarian<br>407-582-1854
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:dramsingh1@valenciacollege.edu">Devika Ramsingh</a></h4>
                                 
                                 <p>Librarian<br>407-582-1536
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:rseguin@valenciacollege.edu">Regina Seguin</a></h4>
                                 
                                 <p>Librarian<br>407-582-1361
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:cbota@valenciacollege.edu">Cathleen Bota </a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:rdavis61@valenciacollege.edu">Reggie Davis </a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:sgomez26@valenciacollege.edu">Sara    Gomez</a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:chensley1@valenciacollege.edu">Ciara Hensley</a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:akapucu@valenciacollege.edu">Aysegul (Rose) Kapucu</a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:pmccall3@valenciacollege.edu">Patti McCall </a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:sjohnston17@valenciacollege.edu">Sharon Johnston</a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:rruizvazquez@valenciacollege.edu">Ricardo Ruiz Vazquez</a></h4>
                                 
                                 <p>Librarian<br>407-582-1432
                                 </p>
                                 
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:lily@ucf.edu">Lily Flick</a></h4>
                                 
                                 <p>UCF Librarian<br>407-582-1585
                                 </p>
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     
                     				
                     <hr>
                     				<a id="winterpark"></a>
                     				
                     <h3>Winter Park Campus</h3>
                     				
                     <div class="row">
                        					
                        <div class="col-md-4">
                           
                           <h4>Circulation</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4>Winter Park Library Circulation Desk</h4>
                                 
                                 <p>407-582-6814</p>
                                 
                              </li>
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:gbarjamoski@valenciacollege.edu">Grenka Fletcher </a></h4>
                                 
                                 <p>Library Specialist <br>407-582-6816
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           
                           					
                        </div>
                        					
                        <div class="col-md-4">
                           <h4>Reference</h4>
                           
                           <ul class="list_staff">
                              
                              <li>
                                 <figure><img class="img-circle" src="/_resources/img/no-photo-female-thumb.png" alt="No Picture Available"></figure>
                                 
                                 <h4><a href="mailto:bmittag@valenciacollege.edu">Ben    Mittag</a></h4>
                                 
                                 <p>Librarian<br>407-582-6832
                                 </p>
                                 
                                 
                              </li>
                              
                           </ul>
                           					
                        </div>
                        					
                        				
                     </div>
                     				
                     				
                     		
                  </div>
                  		
               </div>		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/about/departments-staff.pcf">©</a>
      </div>
   </body>
</html>