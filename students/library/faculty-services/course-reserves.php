<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/faculty-services/course-reserves.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/faculty-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/faculty-services/">Faculty Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Course Reserves</h2>
                        
                        <p>Course Reserves is available to  professors who would like to increase the availability
                           of supplemental  resources to their students. See the libraries' <a href="../documents/LibraryCollectionDevelopmentProcedure2010.pdf" title="Collection Development Procedure">Collection  Development Procedure</a> for a description of the collection.
                        </p>
                        
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>East</h3>
                              
                              <p>The East Campus  Library provides access to a collection of course-related materials
                                 for  non-commercial, educational student use.  Course Reserves are  located at the
                                 East Campus Library Circulation Desk on the second floor of  Building 4.  When requesting
                                 an item, students should be prepared  to provide the name of a textbook, the course
                                 number, and/or the professor's  name.  Students must present a valid Valencia College
                                 student ID to check  out any item.  No other form of identification will be accepted.
                                 Generally, items are loaned for a 4 hour  period and the time due will be printed
                                 on the checkout receipt.  Students are responsible for returning  checked-out items
                                 on time.  Overdue  course reserve items will accrue a $1.20 an hour overdue fee.
                              </p>
                              Professors can add items to  this collection by either filling out a <a href="../documents/reservematerial.pdf">Reserve  Material form</a> or  by stopping by the Circulation Department.  Faculty must ensure that all  materials
                              that they request to be added to the Course Reserves Collection  comply with copyright
                              law.   Faculty requesting the addition of an “on reserve”  textbook can supply a personal
                              or desk copy.  The Library can help  coordinate textbook acquisitions through academic
                              departments and other  sources, but acquisition by the library is not guaranteed,
                              as per <a href="../documents/LibraryCollectionDevelopmentProcedure2010.pdf">Valencia  College Collection Development Procedure</a>.  Items can be  added anytime during the semester but please allow 48 business hours
                              for  processing. 
                           </div>
                           
                           <div>
                              
                              <h3>Lake Nona</h3>
                              
                              <p>At Lake Nona, course reserves are held at the library  circulation desk. Reserve materials
                                 typically have a 2-hour checkout period,  but can vary at your request. To place materials
                                 on reserve, please bring the  materials to a library staff member at the circulation
                                 desk. 
                              </p>
                              You  can also temporarily place circulating library materials on reserve for a  class.
                              Please contact the library to do so. 
                           </div>
                           
                           <div>
                              
                              <h3>Osceola</h3>
                              
                              <p>At  Osceola, course reserves are held behind the library circulation desk. Reserve
                                 materials typically have a 2-hour checkout period, but can vary at your  request.
                                 To place materials on reserve, please bring the materials to Marcie  Rhoads (x4155)
                                 at the circulation desk. 
                              </p>
                              You  can also temporarily place circulating library materials on reserve for a  class.
                              Please contact your division library liaison to do so. 
                           </div>
                           
                           <div>
                              
                              <h3>Poinciana</h3>
                              
                              <p>At Poinciana, course reserves are held at the library circulation desk. Reserve materials
                                 typically have a 2-hour checkout period, but can vary at your request. To place materials
                                 on reserve, please bring the materials to a library staff member at the circulation
                                 desk.
                              </p>
                              
                              <p>You can also temporarily place circulating library materials on reserve for a class.
                                 Please contact the library to do so.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>West</h3>
                              
                              <p> At West Campus, Course Reserves are  held at the Library Check-Out Desk (1st floor).
                                 Varying check-out periods are  available at your request. For example, items may check
                                 out for two hours, one  day or several days. 
                              </p>
                              
                              <p> To place materials on Reserve, faculty  members must fill out a <a href="documents/RESERVEMATERIALFORM-READONLYPDF.pdf">Course Reserves Material Form</a>. Faculty are  responsible for obtaining copyright permissions when necessary to place
                                 materials on Reserve.
                              </p>
                              
                              <p><a href="documents/CopyofMASTER-ReserveMaterialForm-Usethisone.pdf">Course Reserves Material Form</a></p>
                              
                              <p> Please <strong>print out the completed form and bring it, along with your materials,</strong> to the Library  Check-Out Desk (1st floor). 
                              </p>
                              
                              <p>If you would like to place circulating library  materials temporarily on Reserve for
                                 a class, please contact your <a href="liason-librarians.html">division librarian liaison</a>. For more  information, please contact the Library Check-Out Desk (x1574).
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Winter Park</h3>
                              
                              <p>At Winter Park Campus, Course Reserves  are held at the Library Desk. Varying  check-out
                                 periods are available at your request. For example, items may check  out for two hours,
                                 one day or several days.
                              </p>
                              
                              <p>To place materials on Reserve, please  bring materials to the Library Desk. For  questions,
                                 contact Grenka Fletcher, <a href="mailto:gbajramoski@valenciacollege.edu">gbajramoski@valenciacollege.edu</a>. Faculty  are responsible for obtaining copyright permissions when necessary to place
                                 materials on Reserve.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/faculty-services/course-reserves.pcf">©</a>
      </div>
   </body>
</html>