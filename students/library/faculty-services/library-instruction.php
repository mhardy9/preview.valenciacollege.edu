<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/faculty-services/library-instruction.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/faculty-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/faculty-services/">Faculty Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Library Instruction</h2>
                        
                        <h3>What is Library Instruction?</h3>
                        
                        <p>The overall goal of all library instruction classes is to develop information literate
                           students. At Valencia, an "information literate" student is able to "locate, evaluate,
                           and effectively use information from diverse sources." See more <a href="information-literacy.html">About Information Literacy</a>.
                        </p>
                        
                        
                        <div>
                           
                           <h3>Request Library Instruction</h3>
                           
                           <p>Select a campus below to request Library instruction. </p>
                           
                           <ul>
                              
                              <li>For East,  stop by the library's Reference Desk or call (407)582-2456.</li>
                              
                              <li> For Lake Nona,  stop by the library's Reference Desk or call (407)582-7107.</li>
                              
                           </ul>
                           
                           <p>
                              <a href="http://net4.valenciacollege.edu/forms/library/faculty-services/request-library-instruction-osceola.cfm" target="_blank">Osceola</a> &nbsp;
                              <a href="http://net4.valenciacollege.edu/forms/library/faculty-services/request-library-instruction-poinciana.cfm" target="_blank">Poinciana</a> &nbsp;
                              <a href="request-library-instruction-west.html">West</a> &nbsp;
                              <a href="http://net4.valenciacollege.edu/forms/library/faculty-services/request-library-instruction-winter-park.cfm" target="_blank">Winter Park</a> 
                              
                           </p>
                           
                           
                        </div>
                        
                        <h3>Scope &amp; Format</h3>
                        
                        <p>In each active, hands-on library Instruction session, librarians engage students in
                           learning information literacy concepts, from identifying and locating sources of various
                           types, to evaluating and citing those sources. 
                        </p>
                        
                        <p>Options include tailored research instruction based on class assignments and curriculum,
                           and special topic workshops aimed at teaching specific skills, such as website evaluation.
                           
                        </p>
                        
                        <p>Sessions are typically the length of one class period. Multi-class visits requiring
                           closer faculty-librarian collaboration are welcome as well. 
                        </p>
                        
                        <p>Per campus, variations on the traditional sessions may be available. Valencia librarians
                           offer virtual, and embedded instruction.
                        </p>
                        
                        <p>Please plan to attend the session with your class or arrange for a substitute.</p>
                        
                        <h3>What to Expect @ Your Campus</h3>
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>East</h3>
                              
                              <p>Library instruction sessions are customized to meet the information literacy needs
                                 of students and faculty based on their assignments and courses. These sessions are
                                 interactive and often involve experiential activities based on the needs of the students.
                                 The typical library session may incorporate some or all of the following tenets of
                                 information literacy: choosing or narrowing a research topic, accessing the library
                                 catalog and online library databases, revising search phrases and keywords, evaluating
                                 and identifying websites for credibility, and ethically incorporating information
                                 by avoiding plagiarism and citing sources. 
                              </p>
                              
                              <p>Library instruction sessions can be scheduled at the library reference desk, by email
                                 through your Division Library Liaison, and over the phone by calling 407-582-2456.
                                 A minimum of 48 hours advance notice is required when scheduling a library instruction
                                 session, but two weeks notice is preferred. 
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Lake Nona</h3>
                              
                              <p>Librarians work  with each faculty member to design an instruction session to meet
                                 the  information literacy needs of the class.  Sessions are interactive and  designed
                                 with your specific assignment in mind.  Library instruction  generally takes place
                                 in the computer lab so students can practice accessing  the online catalog and databases
                                 and get a jump start on their research for  their papers, projects, speeches, or presentations.
                              </p>
                              
                              <p>Library  instruction can be scheduled at the reference desk or by calling 407-582-7107.
                                 Once scheduled, a librarian will contact you to develop an outline for the  session.
                                 If possible, please try to schedule sessions at least a week in  advance.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Osceola</h3>
                              
                              <p>Library instruction is normally  a two phased session comprised of a library tour
                                 and database instruction. The  library tour consists of a visual tour of the library
                                 where we meet with the  class in the library and point out all the key locations of
                                 the library (i.e.  the circulation desk, the quiet and group study areas and the reference
                                 desk).  If the instructor has any special reference material or handouts he/she would
                                 like us to explain, we go over the material at this time. We also discuss  library
                                 policies relating to loaning materials. Phase two includes moving the  class to the
                                 computer lab and demonstrating the appropriate and relevant  databases. We first demonstrate
                                 how to find a book in our online catalog, and  then we move to the databases chosen
                                 by the instructor. A demonstration is  given of how to use the database then the students
                                 try it themselves. 
                              </p>
                              
                              <p>While this is an example of the  “standard” library instruction, reference librarians
                                 are more than willing to  create a custom session based on the needs of your students.
                                 In the past, we  have held library instruction sessions teaching MLA formatting, plagiarism,
                                 and  evaluating web resources.
                              </p>
                              To  get the most out of your instruction session, please provide the librarian with
                              as much information is possible. Let us know if students will have research  topics
                              picked out in advance, and furnish the instructing librarian with a copy  of the assignment
                              prior to the instruction session. Library instruction works  best when it immediately
                              precedes a research assignment, as the information  will be fresh in the student's
                              mind as they begin to conduct research. 
                           </div>
                           
                           <div>
                              
                              <h3>Poinciana</h3>
                              
                              <p dir="auto">Library instruction sessions can be a general overview of resources available to students,
                                 or a targeted instruction based on an assignment. Librarians work with each faculty
                                 member to design an instruction session to meet the information literacy needs of
                                 the class. Sessions are interactive and designed with your specific assignment in
                                 mind. In most cases, library instructions are held in the classroom using laptops.
                                 Instructors are required to attend library instructions with the class.
                              </p>
                              
                              <p dir="auto">Please submit your request at least one week in advance and with at least two date
                                 choices. This will ensure the availability of a librarian, access to a laptop cart,
                                 and adequate preparation of materials. A customized presentation will require a copy
                                 of the assignment at the time of your request. If you would like a specific database
                                 covered, please include this as well as any other specific topics you would like covered.
                                 A librarian may contact you to discuss your assignment in more detail.
                              </p>
                              
                              <p dir="auto">To schedule an instruction, please visit the library in person, or contact <a href="mailto:kbest7@valenciacollege.edu" rel="noreferrer">Karene Best</a>.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>West</h3>
                              
                              <p>Library instruction sessions with librarians focus on information literacy concepts.
                                 The specific activities for the session are dependent on the needs of the students,
                                 the assignment and the course. Once scheduled, a librarian will contact you to discuss
                                 an outline for the session. 
                              </p>
                              
                              <p>Virtual library instruction is available as an option for any course.</p>
                              
                              <p>Face to face classes are typically scheduled in Classrooms 6-220 (27 workstations)
                                 and 6-221 (23 workstations). Classroom 6-118 may also be used (24 workstations). 
                              </p>
                              
                              <p>Instruction sessions on Power Point as well as other Microsoft Office applications,
                                 courseware and software are available from the Computer Access Lab. Please contact
                                 Francine Crum Scott, x1631.
                              </p>
                              
                              <p>For scheduling questions, please contact Regina Seguin, x1361. For all other questions
                                 regarding Library Instruction, please contact your Division Library Liaison. 
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Winter Park</h3>
                              
                              <p>Library instructions can be a general overview of resources available to students,
                                 or a targeted instruction based on an assignment. In most cases, library instructions
                                 are held in a computer lab. Instructors may also request dedicated research time in
                                 the library itself. Instructors are required to attend library instructions with the
                                 class. 
                              </p>
                              
                              <p>In either case, please submit your request at least two weeks in advance and with
                                 at least two date choices. This will ensure the availability of a librarian, access
                                 to a computer lab, and adequate preparation of materials.  A customized presentation
                                 will require a copy of the assignment at the time of your request. If you would like
                                 a specific database covered, please include this as well as any other specific topics
                                 you would like covered.  A librarian may contact you to discuss your assignment in
                                 more detail. 
                              </p>
                              
                              <p>To schedule an instruction, please visit the library in person, or contact <a href="mailto:bmittag@valenciacollege.edu">Ben Mittag</a>.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <h3>What to Provide</h3>
                        
                        <p>Please provide a copy of the assignment prior to the instruction session; it is essential
                           for the librarian to prepare a session that meets your students' needs. In addition,
                           the following information will also help the librarian prepare for and deliver a great
                           session:
                        </p>
                        
                        <ul type="disc">
                           
                           <li> Whether students will have research topics picked out in advance</li>
                           
                           <li> Whether your students will be working in groups, and whether the groups are pre-selected</li>
                           
                           <li> Whether you will need any of the class period for other class business</li>
                           
                           <li> Whether any of your students will require accommodations under ADA.      </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/faculty-services/library-instruction.pcf">©</a>
      </div>
   </body>
</html>