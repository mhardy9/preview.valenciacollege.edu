<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/faculty-services/information-literacy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/faculty-services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/faculty-services/">Faculty Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Information Literacy</h2>
                        
                        <p>Information Literacy is  one of Valencia's <a href="../../academic-affairs/institutional-effectiveness-planning/institutional-assessment/loa/outcomes_GenEd.html">general education learning outcomes</a>. At Valencia, an "information  literate" student is able to "locate, evaluate, and
                           effectively use  information from diverse sources." An expanded definition from  the
                           Association of College and Research Libraries informs our teaching  practice: Information
                           literacy is the  set of integrated abilities encompassing the reflective discovery
                           of  information, the understanding of how information is produced and valued, and
                           the use of information in creating new knowledge and participating ethically in  communities
                           of learning.
                        </p>
                        
                        <p>As faculty, Valencia  librarians teach information literacy to students.</p>
                        
                        <h3>Information Literacy <strong>and Related Literacies</strong>
                           
                        </h3>
                        
                        <p>"Digital Literacy" refers to "the ability to find, evaluate, utilize, share, and create
                           content using information technologies and the Internet," according to a  definition
                           from <a href="http://digitalliteracy.cornell.edu/">Cornell  University's Digital Literacy Resource</a>.
                        </p>
                        
                        <p><strong>Media Literacy,</strong> according to a  definition by the <a href="http://www.medialit.org/">Center  for Media Literacy</a>, is "a framework to access, analyze, evaluate, create  and participate with messages
                           in a variety of forms - from print to video to the  Internet. Media literacy builds
                           an understanding of the role of media in  society as well as essential skills of inquiry
                           and self-expression necessary  for citizens of a democracy."
                        </p>
                        
                        <p>              <strong>Metaliteracy</strong> "is a unified construct that  supports the acquisition, production, and sharing of
                           knowledge in collaborative  online communities. Metaliteracy challenges traditional
                           skills-based approaches  to information literacy by recognizing related&nbsp;literacy types
                           and  incorporating emerging technologies," according to <a href="https://metaliteracy.org/">Metaliteracy.org</a>. 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        
                        
                        
                        <div>
                           
                           
                           <h3>The Student Perspective</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4>Why is information literacy important?</h4>
                                 
                                 <p> Information literacy is a skill that doesn't  just help with an upcoming research
                                    paper, it helps with all aspects of life.   Knowing how to find information on the
                                    internet is easy.  Knowing  how to find the right information?  Sometimes it can be
                                    surprisingly hard. 
                                 </p>
                                 
                                 <h4> What does information literacy teach me?</h4>
                                 
                                 <p> It helps you develop the skills to answer the  following questions:</p>
                                 
                                 <ul>
                                    
                                    <li>Is this information correct?</li>
                                    
                                    <li>Is this information current?</li>
                                    
                                    <li>Is this information relevant?</li>
                                    
                                    <li>Is this information trustworthy?</li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>How does information literacy help me?</h4>
                                 
                                 <p> Every day we have questions that need answers.   Where do we go?  Whom can we trust?
                                    How can we find  information to help ourselves?  How can we help our family and friends?
                                    How can we learn about the world and be a better citizen?  How can we make  our voice
                                    heard?
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>The Faculty Perspective</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4>Integrating Information Literacy Instruction  into Your Course</h4>
                                 
                                 <p>There are numerous ways to partner with Valencia  librarians to integrate information
                                    literacy instruction into your course.  Download the handout: 
                                 </p>
                                 
                                 <p><a href="documents/SevenwaystoIntegrateInfoLit.pdf" target="_blank"><br>
                                       Seven Ways to Integrate Information  Literacy Instruction</a></p>
                                 
                                 <h4>Assessing Information Literacy</h4>
                                 
                                 <p>Information literacy is assessed at  Valencia by librarians as well as within the
                                    general education disciplines. 
                                 </p>
                                 
                                 <p>Authentic, summative assessment of  information literacy concepts and skills is usually
                                    achieved through the performance  of an information search-related task such as a
                                    research paper. Other summative assignments might include annotated  bibliographies,
                                    class debates or oral presentations.
                                 </p>
                                 
                                 <p>Formative information literacy  assessments may include short-answer response type
                                    items, multiple choice  quizzes, and various classroom assessment techniques. 
                                 </p>
                                 
                                 <p>The Valencia libraries' Program  Learning Outcomes Assessment Team includes librarians
                                    from all five Valencia  campuses. Our goal is to develop valid and reliable information
                                    literacy  assessment tools that can be adapted to the needs of any discipline. We
                                    believe  that information literacy assessment, like information literacy instruction,
                                    works best when it is integrated within a discipline and directly relates to  course
                                    assignments. Through partnerships with many disciplines we hope to  gather enough
                                    data to form an overall picture of information literacy competencies  at Valencia.
                                 </p>
                                 
                                 <p>We also want to be an information  source for disciplines interested in developing
                                    their own information literacy  assessment tools. Below are resources that faculty
                                    may find useful.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Resources</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4>ACRL (Association of College &amp; Research  Libraries) Links:</h4>
                                 
                                 <p><a href="http://www.ala.org/acrl/standards/ilframework" target="_blank">Framework for Information Literacy for Higher Education</a></p>
                                 
                                 <p><a href="http://www.ala.org/acrl/standards/guidelinesinstruction" target="_blank">Guidelines for Instruction Programs in  Academic Libraries</a></p>
                                 
                                 <p><a href="http://www.ala.org/ala/mgrps/divs/acrl/issues/infolit/overview/faculty/faculty.cfm" target="_blank">Information Literacy in a Nutshell for Faculty  and Administrators</a><br>
                                    This guide contains a definition of Information  Literacy, important points to know,
                                    and will also contain model programs and  additional information.
                                 </p>
                                 
                                 <h4>Additional Links</h4>
                                 
                                 <p><a href="http://projectinfolit.org/" target="_blank">Project  Information Literacy</a><br>
                                    A national, longitudinal research study based in  the University of Washington's iSchool,
                                    compiling data on how college students  seek and use information.
                                 </p>
                                 
                                 <p><a href="http://www.learningoutcomeassessment.org/documents/LibraryLO_000.pdf" target="_blank">An Essential Partner: The Librarian's Role in  Student Learning Assessment</a> (NILOA Occasional Paper No. 14.) This paper by Debra Gilchrist and Megan  Oakleaf
                                    (2012, April), reports on ways that librarians and teaching faculty can  partner to
                                    both facilitate and assess student learning. 
                                 </p>
                                 
                                 <p><a href="http://www.oclc.org/reports/2010perceptions.htm" target="_blank">Perceptions of Libraries, 2010: Context and Community</a><br>
                                    This report from OCLC (Online Computer Library  Center) explores students' information-seeking
                                    behaviors and perceptions. A  valuable resource for any faculty member interested
                                    in getting inside students'  heads when it comes to research.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/faculty-services/information-literacy.pcf">©</a>
      </div>
   </body>
</html>