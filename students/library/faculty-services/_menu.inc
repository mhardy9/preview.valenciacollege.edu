<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo">
<a href="/index.php"> <img alt="Valencia College Logo" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> </a> 
</div>
</div>
<nav aria-label="Subsite Navigation" class="col-md-9 col-sm-9 col-xs-9" role="navigation">
<a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a> 
<div class="site-menu">
<div id="site_menu">
<img alt="Valencia College" data-retina="true" height="40" src="/_resources/img/logo-vc.png" width="265" /> 
</div>
<a class="open_close" href="#" id="close_in"> <i class="far fa-close"> </i> </a> 
<ul class="mobile-only">
<li> <a href="https://preview.valenciacollege.edu/search"> Search </a> </li>
<li class="submenu"> <a class="show-submenu" href="javascript:void(0);"> Login <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="//atlas.valenciacollege.edu/"> Atlas </a> </li>
<li> <a href="//atlas.valenciacollege.edu/"> Alumni Connect </a> </li>
<li> <a href="//login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a> </li>
<li> <a href="//learn.valenciacollege.edu"> Valencia &amp; Online </a> </li>
<li> <a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a> </li>
</ul>
</li>
<li> <a class="show-submenu" href="javascript:void(0);"> Top Menu <i aria-hidden="true" class="far fa-angle-down"> </i> </a> 
<ul>
<li> <a href="/academics/current-students/index.php"> Current Students </a> </li>
<li> <a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a> </li>
<li> <a href="https://international.valenciacollege.edu"> International Students </a> </li>
<li> <a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a> </li>
<li> <a href="/academics/continuing-education/index.php"> Continuing Education </a> </li>
<li> <a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a> </li>
<li> <a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a> </li>
</ul>
</li>
</ul>
<ul>
<li><a href="../index.php">Library</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3">
<div class="col-md-4"><ul>
<a href="../services/index.php"><h3>Library Services</h3></a><li><a href="../services/accessibility.php"> Accessibility</a></li>
<li><a href="../services/how-to-borrow.php">  How to Borrow</a></li>
<li><a href="../services/internet-use.php">  Internet Use</a></li>
<li><a href="../services/print-scan-copy.php">  Print, Scan, &amp; Copy</a></li>
<li><a href="../services/research-help-information.php">  Research Help &amp; Info</a></li>
<li><a href="../services/interlibrary-loan.php">  Interlibrary Loan</a></li>
<li><a href="../services/study-areas.php">  Study Areas</a></li>
<li><a href="../services/visitors-guests.php">  Visitors &amp; Guests</a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="index.php"><h3>Faculty Services</h3></a><li><a href="library-instruction.php"> Library Instruction</a></li>
<li><a href="course-reserves.php">  Course Reserves</a></li>
<li><a href="liason-librarians.php">  Liaison Librarians</a></li>
<li><a href="information-literacy.php">  Information Literacy</a></li>
<li><a href="../mla-apa-chicago-guides/index.php">MLA, APA &amp; Chicago</a></li>
<a href="../tutorials/index.php"><h3>Tutorials</h3></a><li><a href="../tutorials/evaluating-websites.php">  Evaluating Web Sites</a></li>
<li><a href="../tutorials/information-literacy-modules.php">  Information Literacy Modules</a></li>
<li><a href="../tutorials/plagiarism.php">  Plagiarism </a></li>
</ul></div>
<div class="col-md-4"><ul>
<a href="../about/index.php"><h3>About Us</h3></a><li><a href="../about/hours.php">  Locations &amp; Hours</a></li>
<li><a href="../about/departments-staff.php">  Departments &amp; Staff</a></li>
<a href="../locations/east-library.php"><h3>Campus Libraries</h3></a><li><a href="../locations/east-library.php"> East Campus</a></li>
<li><a href="../locations/lake-nona-library.php">  Lake Nona Campus</a></li>
<li><a href="../locations/osceola-library.php">  Osceola Campus</a></li>
<li><a href="../locations/poinciana-library.php">  Poinciana Campus</a></li>
<li><a href="../locations/west-library.php">  West Campus</a></li>
<li><a href="../locations/winter-park-library.php">  Winter Park Campus</a></li>
<li><a href="../faq.php">Frequently Asked Questions</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/library/contact.cfm">Contact Us</a></li>
</ul></div>
</div>
</li>
</ul>
</div>
 
</nav>
</div>
</div>
 
</div>
