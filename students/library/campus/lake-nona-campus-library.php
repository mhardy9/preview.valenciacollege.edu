<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/campus/lake-nona-campus-library.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/campus/">Campus</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h2>Lake Nona Campus Library</h2>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>What's Happening
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <p>Welcome to Lake Nona! Please be sure to check our Facebook and Instagram accounts
                                             to stay updated with our latest events and happenings.
                                          </p>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="https://www.facebook.com/ValenciaLNCLibrary/" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '25609336'});" target="_blank">Facebook Page</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="https://www.instagram.com/valencialnclibrary/" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '25609341'});" target="_blank">Instagram Account</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>New Items in Lake Nona Campus Library
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://fcsopac.flvc.org/F/MD2QYCE91VXSVEVKSQXVJ2NNKT5C2I6VG8HD85UTMA8D3QX8UH-00436?func=find-b&amp;find_code=SYS&amp;request=002544701&amp;adjacent=N&amp;local_base=ALL&amp;pds_handle=GUEST" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '18700348'});" target="_blank">
                                                         
                                                         <span>Life and Death Decisions
                                                            </span><span> by </span><span>Sheldon Ekland-Olson
                                                            </span></a><div>Call Number: BD431 .E427 2015</div>
                                                      
                                                      <div>ISBN: 1138808873</div>
                                                      
                                                      <div>Publication Date: 2014-11-21</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://fcsopac.flvc.org/F/K8DC9EY5FBJ2RGAYK5AFBNN3M6AMTVB3ACX7E22RKCVL51LS36-00484?func=find-b&amp;find_code=SYS&amp;request=002529758&amp;adjacent=N&amp;local_base=ALL&amp;pds_handle=GUEST" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '18700350'});" target="_blank">
                                                         
                                                         <span>A Dictionary of Psychology
                                                            </span><span> by </span><span>Andrew M. Colman
                                                            </span></a><div>Call Number: BF31 .C65 2015</div>
                                                      
                                                      <div>ISBN: 0199657688</div>
                                                      
                                                      <div>Publication Date: 2015-02-02</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://fcsopac.flvc.org/F/3TMU68MDH7CVRBUCXQNF2A6XP96XL45PQYJH77E6GQKSHUEK1P-00526?func=find-b&amp;find_code=SYS&amp;request=002441500&amp;adjacent=N&amp;local_base=ALL&amp;pds_handle=GUEST" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '18700352'});" target="_blank">
                                                         
                                                         <span>One Nation under God
                                                            </span><span> by </span><span>Kevin M. Kruse
                                                            </span></a><div>Call Number: BR517 .K78 2015</div>
                                                      
                                                      <div>ISBN: 0465049494</div>
                                                      
                                                      <div>Publication Date: 2015-04-14</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://fcsopac.flvc.org/F/E8YNMPNEIEUKSY9VGVI3EDNLJERUJV8ELC2CKJM3D33HNJHUCU-00604?func=find-b&amp;find_code=SYS&amp;request=002465482&amp;adjacent=N&amp;local_base=ALL&amp;pds_handle=GUEST" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '18700354'});" target="_blank">
                                                         
                                                         <span>Rise of the Robots
                                                            </span><span> by </span><span>Martin Ford
                                                            </span></a><div>Call Number: HD6331 .F58 2015</div>
                                                      
                                                      <div>ISBN: 0465059996</div>
                                                      
                                                      <div>Publication Date: 2015-05-05</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://fcsopac.flvc.org/F/Y9Y926KQK2V2Q6LQLVRDBXJJTK84K31ML9JE1R2R11LMQ7FX22-00663?func=find-b&amp;find_code=SYS&amp;request=002459469&amp;adjacent=N&amp;local_base=ALL&amp;pds_handle=GUEST" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '18700356'});" target="_blank">
                                                         
                                                         <span>Common Core Dilemma - Who Owns Our Schools?
                                                            </span><span> by </span><span>Mercedes K. Schneider; Carol Corbett Burris (Foreword by)
                                                            </span></a><div>Call Number: LB3060.83 .S357 2015</div>
                                                      
                                                      <div>ISBN: 0807756490</div>
                                                      
                                                      <div>Publication Date: 2015-06-12</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://fcsopac.flvc.org/F/3EH6X3CLN75S6BVS3XGTY3E2IUS455H9DJLXKJN4GQXP36BYDF-00710?func=find-b&amp;find_code=SYS&amp;request=002528463&amp;adjacent=N&amp;local_base=ALL&amp;pds_handle=GUEST" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '18700357'});" target="_blank">
                                                         
                                                         <span>Film Criticism in the Digital Age
                                                            </span><span> by </span><span>Mattias Frey (Contribution by, Editor); Paolo Noto (Contribution by); Maria San Filippo
                                                            (Contribution by); Greg Taylor (Contribution by); Noah Tsika (Contribution by); Armond
                                                            White (Contribution by); Daniel McNeil (Contribution by); Cecilia Sayad (Contribution
                                                            by, Editor); Thomas Elsaesser (Contribution by); Outi Hakola (Contribution by); Anne
                                                            Hurault-Paupe (Contribution by); Nick James (Contribution by); Jasmina Kallay (Contribution
                                                            by); Giacomo Manzoli (Contribution by)
                                                            </span></a><div>Call Number: PN1995 .F4573 2015</div>
                                                      
                                                      <div>ISBN: 0813570735</div>
                                                      
                                                      <div>Publication Date: 2015-04-20</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>eBooks
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <p>Don’t forget about our eBooks!&nbsp; Lake Nona has a mostly digital library so students
                                             and faculty have access to thousands of eBooks from anyplace where they have an internet
                                             connection.&nbsp; Books can be downloaded to be read offline on a variety of eReaders including
                                             iPads, Kindles, Nooks, etc. or you can read books directly on your computer. Stop
                                             by if you need help!
                                          </p>
                                          
                                          
                                          <p>Are print books more your style? Let us know what you are looking for, and we can
                                             order titles in from other campuses.
                                          </p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Lake Nona Campus Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus Library 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/campus/lake-nona-campus-library.pcf">©</a>
      </div>
   </body>
</html>