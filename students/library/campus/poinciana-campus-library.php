<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/campus/poinciana-campus-library.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/campus/">Campus</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h2>Poinciana Campus Library</h2>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>What's Happening?
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <p><span><span>Welcome to the new Poinciana Campus Library!!! </span></span></p>
                                          
                                          
                                          <p><span><span>Have you had a chance to visit the library yet? Stop by Room 331.</span></span></p>
                                          
                                          
                                          <p><span><span>We have comfortable seating with a beautiful view. There is also a quiet study room
                                                   for you to concentrate and get your work done. You can use the computers for completing
                                                   homework. We also have a printer for you to print out your work. If you need a calculator
                                                   or a tablet, we have those too!&nbsp; Or you can just stop in and say hi. We look forward
                                                   to seeing you soon!! </span></span></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>Library Hours
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <p><span><strong><u>Fall Hours</u><u>:</u></strong></span></p>
                                          
                                          
                                          <p><span>Monday - Thursday: 8:00am - 8:00pm</span></p>
                                          
                                          
                                          <p><span>Friday: 8:00am - 5:00pm</span></p>
                                          
                                          
                                          <p><span>Saturday: 8:00am - 12:00pm</span></p>
                                          
                                          
                                          <p><span>Closed Sunday</span></p>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <p><span>Need help after hours? Try <strong><a href="http://askalibrarian.org/valencia">Ask a Librarian</a></strong></span></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Poinciana Campus Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Poinciana Campus Library </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div>Social</div>
                           
                           <div> <a href="https://twitter.com/VCPoincianaCrew" target="_blank" title="Twitter"><span>Twitter</span></a>
                              
                           </div>
                           
                           <div><strong> <a href="https://www.facebook.com/ValenciaCollegePoincianaCampus" target="_blank" title="Facebook"><span>Facebook</span></a> </strong></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/campus/poinciana-campus-library.pcf">©</a>
      </div>
   </body>
</html>