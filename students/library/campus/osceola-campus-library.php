<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/campus/osceola-campus-library.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/campus/">Campus</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h2>Osceola Campus Library</h2>
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>What's Happening
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <p><strong><span>Welcome back! Our fall semester hours are:</span></strong></p>
                                          
                                          
                                          <p><span>Monday-Thursday: 7:</span>00am<span> - 10:</span>00pm<br>
                                             
                                          </p>
                                          
                                          
                                          <p><span>Friday: 7:00am - 5:00pm</span></p>
                                          
                                          
                                          <p><span>Saturday: 8:00am - 1:00pm</span></p>
                                          
                                          
                                          <p><span>Closed Sunday</span></p>
                                          
                                          
                                          
                                          
                                          
                                          
                                          <p><span>Need help after hours? Try <strong><a href="http://askalibrarian.org/valencia">Ask a Librarian</a></strong></span></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>New Reads in the Browsing Collection
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://www.linccweb.org/catalog?screen=direct&amp;query=002689106" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '27284715'});" target="_blank">
                                                         
                                                         <span>They Can't Kill Us All: Ferguson, Baltimore, and a New Era in America’s Racial Justice
                                                            Movement
                                                            </span><span> by </span><span>Wesley Lowery
                                                            </span></a><div>Call Number: E184 .A1 L69 2016</div>
                                                      
                                                      <div>ISBN: 9780316312479</div>
                                                      
                                                      <div>Publication Date: 2016</div>
                                                      
                                                      <div>
                                                         
                                                         <div>In an effort to grasp the magnitude of the repose to Michael Brown's death and understand
                                                            the scale of the problem police violence represents, Lowery speaks to Brown's family
                                                            and the families of other victims other victims' families as well as local activists.
                                                            By posing the question, "What does the loss of any one life mean to the rest of the
                                                            nation?" Lowery examines the cumulative effect of decades of racially biased policing
                                                            in segregated neighborhoods with failing schools, crumbling infrastructure and too
                                                            few jobs.
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://www.linccweb.org/catalog?screen=direct&amp;query=002688408" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '27284720'});" target="_blank">
                                                         
                                                         <span>Born a Crime: Stories from a South African Childhood
                                                            </span><span> by </span><span>Trevor Noah
                                                            </span></a><div>Call Number: PN2287 .N557 A3 2016</div>
                                                      
                                                      <div>ISBN: 9780399588174</div>
                                                      
                                                      <div>Publication Date: 2016</div>
                                                      
                                                      <div>
                                                         
                                                         <div>Trevor Noah's unlikely path from apartheid South Africa to the desk of The Daily Show
                                                            began with a criminal act: his birth. Trevor was born to a white Swiss father and
                                                            a black Xhosa mother at a time when such a union was punishable by five years in prison.
                                                            Living proof of his parents' indiscretion, Trevor was kept mostly indoors for the
                                                            earliest years of his life, bound by the extreme and often absurd measures his mother
                                                            took to hide him from a government that could, at any moment, steal him away. Finally
                                                            liberated by the end of South Africa's tyrannical white rule, Trevor and his mother
                                                            set forth on a grand adventure, living openly and freely and embracing the opportunities
                                                            won by a centuries-long struggle.
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://www.linccweb.org/catalog?screen=direct&amp;query=002646237" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '27284675'});" target="_blank">
                                                         
                                                         <span>In the Country We Love: My Family Divided
                                                            </span><span> by </span><span>Diane Guerrero
                                                            </span></a><div>Call Number: PN2287.G7455 A3 2016</div>
                                                      
                                                      <div>ISBN: 1627795278</div>
                                                      
                                                      <div>Publication Date: 2016</div>
                                                      
                                                      <div>
                                                         
                                                         <div>"Diane Guerrero, the television actress from the megahit Orange is the New Black and
                                                            Jane the Virgin, was just fourteen years old on the day her parents and brother were
                                                            arrested and deported to Colombia while she was at school. Born in the U.S., Guerrero
                                                            was able to remain in the country and continue her education, depending on the kindness
                                                            of family friends who took her in and helped her build a life and a successful acting
                                                            career for herself, without the support system of her family. In the Country We Love
                                                            is a moving, heartbreaking story of one woman’s extraordinary resilience in the face
                                                            of the nightmarish struggles of undocumented residents in this country.
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://www.linccweb.org/catalog?screen=direct&amp;query=002680578" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '27284712'});" target="_blank">
                                                         
                                                         <span>Small Great Things
                                                            </span><span> by </span><span>Jodi Picoult
                                                            </span></a><div>Call Number: PS3566 .I372 S63 2016</div>
                                                      
                                                      <div>ISBN: 9780345544957</div>
                                                      
                                                      <div>Publication Date: 2016</div>
                                                      
                                                      <div>
                                                         
                                                         <div>Ruth Jefferson is a labor and delivery nurse at a Connecticut hospital with more than
                                                            twenty years experience. During her shift, Ruth begins a routine checkup on a newborn,
                                                            only to be told a few minutes later that she s been reassigned to another patient.
                                                            The parents are white supremacists and don t want Ruth, who is African American, to
                                                            touch their child. The hospital complies with their request, but the next day, the
                                                            baby goes into cardiac distress while Ruth is alone in the nursery. Does she obey
                                                            orders or does she intervene?
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://www.linccweb.org/catalog?screen=direct&amp;query=002680579" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '27284728'});" target="_blank">
                                                         
                                                         <span>Hidden Figures: The American Dream and the Untold Story of the Black Women Mathematicians
                                                            Who Helped Win the Space Race
                                                            </span><span> by </span><span>Margot Lee Shetterly
                                                            </span></a><div>Call Number: QA27.5 .L44 2016</div>
                                                      
                                                      <div>ISBN: 9780062363596</div>
                                                      
                                                      <div>Publication Date: 2016</div>
                                                      
                                                      <div>
                                                         
                                                         <div>Before John Glenn orbited the earth, or Neil Armstrong walked on the moon, a group
                                                            of dedicated female mathematicians known as “human computers” used pencils, slide
                                                            rules and adding machines to calculate the numbers that would launch rockets, and
                                                            astronauts, into space. Among these problem-solvers were a group of exceptionally
                                                            talented African American women, some of the brightest minds of their generation.
                                                            Originally relegated to teaching math in the South’s segregated public schools, they
                                                            were called into service during the labor shortages of World War II, when America’s
                                                            aeronautics industry was in dire need of anyone who had the right stuff. Suddenly,
                                                            these overlooked math whizzes had a shot at jobs worthy of their skills, and they
                                                            answered Uncle Sam’s call, moving to Hampton, Virginia and the fascinating, high-energy
                                                            world of the Langley Memorial Aeronautical Laboratory.
                                                         </div>
                                                         
                                                      </div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="http://libguides.valenciacollege.edu/newitemsosc" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '18937298'});" target="_blank">Click here to see more</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>Local Library Links
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="http://library.ucf.edu/" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '18937456'});" target="_blank">University of Central Florida Libraries</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="http://www.osceolalibrary.org/" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '18937473'});" target="_blank">Osceola County Public Library</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="http://www.ocls.lib.fl.us/" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '18937486'});" target="_blank">Orange County Public Library</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   <span><a href="http://www.linccweb.org/search/catalog.aspx?screen=keyword&amp;lib_code=FLCC2900" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '2',_st_content_id: '19250999'});" target="_blank">Valencia College Classic Catalog</a> 
                                                      </span>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Osceola Campus Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus Library 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div>Social</div>
                           
                           
                           <div>
                              <a href="https://www.facebook.com/OsceolaCampusLibrary" target="_blank" title="Facebook"><span>Facebook</span></a>
                              
                           </div>
                           
                           
                        </div>                 
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/campus/osceola-campus-library.pcf">©</a>
      </div>
   </body>
</html>