<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/campus/east-campus-library.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/campus/">Campus</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h2>East Campus Library</h2>
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>News and Events
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <p>News:</p>
                                          
                                          
                                          <ul>
                                             
                                             <li>Fall hours are listed below:</li>
                                             
                                             <li>
                                                
                                                <p>Monday - Thursday: 7am to 10pm<br>
                                                   Friday: 7am to 8pm; Saturday 8am to 4pm &amp; Sunday 2pm-8pm
                                                </p>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <p>The library recently received two amazing donations to the collection.&nbsp; <a href="https://translate.googleusercontent.com/translate_c?depth=1&amp;hl=en&amp;prev=search&amp;rurl=translate.google.com&amp;sl=zh-TW&amp;sp=nmt4&amp;u=http://arts.fgs.org.tw/fgs_arts/tw/introduce.php&amp;usg=ALkJrhja21GYiZIpXJu9c6vpjBfc-D48AQ">The Encyclopedia of Buddhist Arts</a>, a gorgeous 20 volume set from the Guang Ming Temple and a <a href="http://discover.linccweb.org/primo_library/libweb/action/search.do?vl(freeText0)=%20Valencia%20+%20College%20+%20Holocaust%20+%20Collection%20&amp;vl(351306042UI0)=sub&amp;vl(478594454UI1)=all_items&amp;vl(1UIStartWith0)=exact&amp;fn=search&amp;tab=default_tab&amp;mode=Basic&amp;vid=FLCC2900&amp;scp.scps=scope%3a(FLCC2900_sfx)%2cscope%3a(FLCC2902)%2cscope%3a(FLCC2920)%2cscope%3a(FLCC0100)%2cscope%3a(oai_fcla_fof)%2cprimo_central_multiple_fe">Holocaust Resource Collection </a>from The Holocaust Memorial Research and Education Center.
                                                </p>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                <p>Interested in social entrepreneurship?&nbsp; Tune into East Librarian Courtney Moore's
                                                   Podcast with partner Jerrid P. Kalakay, <a href="https://teachingchangepodcast.com/">Teaching Change</a></p>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                          
                                          
                                          
                                          <p><strong>Event Calendar</strong></p>
                                          
                                          
                                          <p>Faculty Brown Bag Discussion on&nbsp;Fake News: Tuesday, October 3, 1-2 pm in 3-113</p>
                                          
                                          
                                          <p>Fake News&nbsp;Student Workshops: Oct. 19, 26, 31 &amp; Nov. 15, 1-2 pm in 4-203</p>
                                          
                                          
                                          <p><span>Need help after hours? Try <strong><a href="http://askalibrarian.org/valencia">Ask a Librarian</a></strong></span></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>East Campus Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus Library 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <div>
                           
                           <div>Social</div>
                           
                           <div> <a href="https://twitter.com/VCEastLibrary" target="_blank" title="Twitter"><span>Twitter</span></a>
                              
                           </div>
                           
                           <div><strong> <a href="https://www.facebook.com/ValenciaCollegeEastCampusLibrary" target="_blank" title="Facebook"><span>Facebook</span></a> </strong></div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/campus/east-campus-library.pcf">©</a>
      </div>
   </body>
</html>