<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/campus/west-campus-library.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/campus/">Campus</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h2>West Campus Library</h2>
                        
                        
                        
                        
                        <link href="http://lgapi.libapps.com/web/jquery/css/1.8.24_themes_base_jquery-ui.css" rel="stylesheet">
                        
                        <link href="http://lgapi.libapps.com/web/css1.9.5/springshare.public.min.css" rel="stylesheet">
                        
                        <link href="http://lgapi.libapps.com/web/css1.9.5/springshare.common.min.css" rel="stylesheet">
                        
                        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>What's Happening
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                <p><strong>​<span>Fake News: Check B4U </span><span>Share</span></strong><br>
                                                   This workshop for students will be held on Oct. 17 &amp; 18, 1-2pm in room 6-220. Students
                                                   will learn how to differentiate legitimate news sources from fake news in their social
                                                   media feeds.&nbsp;
                                                </p>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h2>New Items in West Campus Library
                                    
                                 </h2>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <ul>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://discover.linccweb.org/FLCC2900:ccla_aleph002731851" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '33839431'});" target="_blank">
                                                         
                                                         <span>The Aisles Have Eyes
                                                            </span><span> by </span><span>Joseph Turow
                                                            </span></a><div>Call Number: HF5415.32 T876 2017</div>
                                                      
                                                      <div>ISBN: 9780300212198</div>
                                                      
                                                      <div>Publication Date: 2017-01-17</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://discover.linccweb.org/FLCC2900:ccla_aleph002547661" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '33839373'});" target="_blank">
                                                         
                                                         <span>Dress, Fashion and Technology
                                                            </span><span> by </span><span>Phyllis G. Tortora; Joanne B. Eicher (Contribution by)
                                                            </span></a><div>Call Number: TT504.T67 2015</div>
                                                      
                                                      <div>ISBN: 9780857851918</div>
                                                      
                                                      <div>Publication Date: 2015-04-23</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://discover.linccweb.org/FLCC2900:ccla_aleph002672412" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '33839383'});" target="_blank">
                                                         
                                                         <span>Financial Literacy for Millennials
                                                            </span><span> by </span><span>Andrew O. Smith
                                                            </span></a><div>Call Number: HG179.S5384 2016</div>
                                                      
                                                      <div>ISBN: 9781440834028</div>
                                                      
                                                      <div>Publication Date: 2016-08-22</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://discover.linccweb.org/FLCC2900:ccla_aleph002734036" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '33839367'});" target="_blank">
                                                         
                                                         <span>Human Rights Innovators
                                                            </span><span> by </span><span>Salem Press (Editor)
                                                            </span></a><div>Call Number: REF JC571.H86 2016</div>
                                                      
                                                      <div>ISBN: 9781682171578</div>
                                                      
                                                      <div>Publication Date: 2016-09-01</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             <li>
                                                
                                                
                                                <div>
                                                   
                                                   <div>
                                                      <a href="http://discover.linccweb.org/FLCC2900:ccla_aleph002737173" onclick="return springSpace.springTrack.trackLink({link: this,_st_type_id: '3',_st_content_id: '33839425'});" target="_blank">
                                                         
                                                         <span>The Right to Die
                                                            </span><span> by </span><span>Howard Ball
                                                            </span></a><div>Call Number: SPEECH R726.B255 2017</div>
                                                      
                                                      <div>ISBN: 9781440843112</div>
                                                      
                                                      <div>Publication Date: 2017-01-31</div>
                                                      
                                                   </div>
                                                   
                                                </div>
                                                
                                             </li>
                                             
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>West Campus Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus Library 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        <hr>
                        
                        
                        <div>
                           
                           <div>Social</div>
                           
                           
                           <div>
                              <a href="https://twitter.com/valenciawestlib" target="_blank" title="Twitter"><span>Twitter</span></a>
                              
                           </div>
                           
                           
                           <div>
                              <a href="http://pinterest.com/valenciawestlib/" target="_blank" title="Pinterest"><span>LinkedIn</span></a>
                              
                           </div>
                           
                           
                           <div>
                              <a href="https://www.facebook.com/valenciawestlibrary" target="_blank" title="Facebook"><span>Facebook</span></a>
                              
                           </div>
                           
                           
                        </div> 
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/campus/west-campus-library.pcf">©</a>
      </div>
   </body>
</html>