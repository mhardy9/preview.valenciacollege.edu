<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        
                        <h3>Library  Services</h3>
                        
                        
                        
                        
                        <div>
                           
                           <h3>Where is the library?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>East Campus library is located on the  second floor of Building 4.Â&nbsp; Lake Nona  library
                                    can be found in Building 1, Room 330.Â&nbsp;  Osceola Campus library is in Building 4,
                                    Room 202. West Campus library  is in Building 6.Â&nbsp; Winter Park library is  in Room
                                    140.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>What are the library's hours?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Each campus library has different hours  of operation.Â&nbsp; Click <a href="about/calendar.html">here</a> for a listing of times.Â&nbsp; Please  call ahead to verify the current library schedule.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Do you have any study rooms?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Each campus has some type of quiet study  area from group study rooms to quiet study
                                    zones.Â&nbsp; Ask each campus what options are available.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>What if I lost something at the Library?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Check with the library’s Circulation Desk  to see if the item was turned over to library
                                    staff first.Â&nbsp; Lost items are turned over to the Security  Department on each campus.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Is  there a place where I can watch or listen to media materials in the Library?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>All  libraries have headphones for checkout at the Circulation Desk so you can  listen
                                    to DVD’s, streaming videos, etc. at a library computer station.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Does the library have scanners?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia libraries have scanners except  for the Lake Nona Campus. Ask a library employee
                                    to point out their location.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Does the library have fax machines?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia libraries do not have fax  machines.Â&nbsp; Each campus may have another  department
                                    that offers faxing.Â&nbsp; Just ask!
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>Borrowing</h3>
                        
                        
                        <div>
                           
                           <h3>How do I check out a book?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Present your Valencia ID card with the  item you want to check out at the Circulation
                                    Desk.Â&nbsp; All items must be checked out with a Valencia  ID. No exceptions! 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I get a Library card?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Your Valencia ID card is your library  card.Â&nbsp; You can get your first card free  at
                                    each campus Security Department.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How many books can I check out?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You may have a total of 25 items checked  out on your account at any one time.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I pay a library fine?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Library fines can be paid at the Business  Office on each campus except Lake Nona
                                    and Winter Park. Library fines are paid in the Lake Nona Campus Administrative Office,
                                    Room 302. Library fines are paid in the Winter Park Answer Center
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I renew my materials online?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Before an item is overdue, log into your  library account to renew.Â&nbsp; Log into  Atlas,
                                    click on the Courses tab, go to the Libraries box, click on Search the  Library.Â&nbsp;
                                    In the upper right hand corner  of the catalog page, click on My Account to see what
                                    books you have checked  out, when they are due, and if you have any fines.Â&nbsp; If there
                                    are no holds on the item, you can  renew it once. If the item is overdue, you cannot
                                    renew a book online and must  return the item to any Valencia library.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Can I return materials to any Valencia  Library?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>All Valencia library materials may be  returned to any campus library.Â&nbsp; If you  have
                                    interlibrary loan materials borrowed through Valencia but from another  school, you
                                    must bring that item back to the library where you originally  checked it out.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I request a book from another  Valencia College library? How long will it take?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>You can request library materials from  other Valencia campuses and have them delivered
                                    to a specific campus. Through  the Valencia College Libraries <a href="http://discover.linccweb.org/primo_library/libweb/action/search.do?vid=FLCC2900">catalog</a>, click on the Request Item link in the catalog entry for the item you  want.Â&nbsp; Choose
                                    the campus location you  want the item delivered to.Â&nbsp; Deliveries  between campuses
                                    usually take between 2 - 3 days.Â&nbsp; You will receive an email to your Atlas  account
                                    once the item is delivered to the location of your choosing.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I request a book or article from  outside Valencia College? How long will it
                              take?
                           </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia  College Libraries is part of a few different interlibrary loan systems.Â&nbsp;
                                    Books and articles can be requested from  other Florida colleges and universities.Â&nbsp;
                                    In addition, we’re part of a system that can request materials from  other member
                                    libraries worldwide. It usually takes approximately 2 weeks to  receive materials
                                    from other institutions.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        <h3>Library  Account</h3>
                        
                        
                        <div>
                           
                           <h3>How  can I tell if my book is overdue?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Log into Atlas, click on the Courses tab,  go to the Libraries box, click on Search
                                    the Library.Â&nbsp; In the upper right hand corner of the catalog  page, click on My Account
                                    to see what books you have checked out, when they are  due, and if you have any fines.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        <h3>Research  Help</h3>
                        
                        
                        <div>
                           
                           <h3>Can a librarian help me when the Library  is closed?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia College Libraries is part of a  statewide system of research help called
                                    Ask-A-Librarian.Â&nbsp; Click <a href="http://askalibrarian.org/valencia">here</a> to be redirected to the Ask-A-Librarian site or click on the  Ask-A-Librarian link
                                    on the Valencia College Libraries’ homepage.Â&nbsp; Live chat with a Florida librarian
                                    is  available 10 am - midnight Sunday -Thursday and 10 am to 5 pm Friday and  Saturday.
                                    Email and texting are also available and will be answered by a  Valencia librarian
                                    during Valencia business hours.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>Photocopying</h3>
                        
                        
                        <div>
                           
                           <h3>How and where can I make copies in the  Library?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Black and white copy machines are located  throughout each Valencia campus library.Â&nbsp;
                                    Valencia has a pay-for-print system.Â&nbsp;  The charge is 10 cents a copy.Â&nbsp;  You must
                                    obtain a copy card from one of the many copy card vending  machines located throughout
                                    the libraries.Â&nbsp;  With your card loaded with a cash balance, place it in the card
                                    reader  for each copy machine.Â&nbsp; The copy cards  are also used for the printers.Â&nbsp;
                                    Copies  are 10 cents per page for a black and white copy.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Can I print in color in the Library?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Each campus has different equipment and  not all campuses offer color printing.Â&nbsp;
                                    Please check at each campus library for availability.Â&nbsp; Printing is 10 cents for the
                                    black and white copiers  and printers and 25 cents for color printers.Â&nbsp;  Lake Nona’s
                                    color printer is located in the Learning Center.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        <h3>Finding  Materials</h3>
                        
                        
                        <div>
                           
                           <h3>Does  the library loan textbooks?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Each  campus library has a Reserved Materials section that contains textbooks and
                                    other materials for classroom use.Â&nbsp; Each  campus collection is different.Â&nbsp; You can
                                    look for reserved materials on the Valencia College Libraries <a href="http://discover.linccweb.org/primo_library/libweb/action/search.do?vid=FLCC2900">catalog</a> page and click Course  Reserves in the I Need To Find section.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I find a specific item?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The <a href="http://discover.linccweb.org/primo_library/libweb/action/search.do?vid=FLCC2900">catalog</a> has search tools to help refine your research.Â&nbsp; If you are looking for a book, you
                                    can enter  the subject, author’s name, or title into the catalog’s search terms box.Â&nbsp;
                                    You can also limit the search to a specific  campus and/or the type of materials (books,
                                    articles, ebooks, etc.)Â&nbsp;
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Where can I look up books?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Search for materials by using the  Valencia College Libraries online catalog.Â&nbsp;  It
                                    can be accessed from anywhere you have internet access.Â&nbsp; Log into your Atlas account,
                                    go to the Courses  tab, find the Libraries box, and click on Search the Library.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>How do I find electronic  resources/databases?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> On the Valencia College Libraries Catalog  page, the toolbar at the top of the page
                                    has a number of links to access the  databases.Â&nbsp; Click on Databases A-Z or  Databases
                                    by Subject to find many different databases.Â&nbsp;
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3> Citation  Help</h3>
                        
                        
                        <div>
                           
                           <h3>How  do I cite the information I use in my paper?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The  Valencia College Libraries has a website with information about the MLA, APA
                                    and Chicago Style guides.Â&nbsp; The site  offers examples for citations for all three
                                    styles along with sample papers for  you to view.Â&nbsp; Click <a href="mla-apa-chicago-guides/index.html">here</a> to go to the style guides  page.Â&nbsp; If you have specific citation  questions, visit
                                    the Communications Center available on each campus.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>Technical  Issues</h3>
                        
                        
                        <div>
                           
                           <h3>I can’t find the answer to my question in  these FAQ â€“ where can I get more help?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>After  hours, go to Ask-A-Librarian for research-related questions.Â&nbsp; You can also
                                    find the link on the Valencia  College Libraries homepage or click <a href="http://askalibrarian.org/valencia" target="_blank">here</a>.  Library-specific or Valencia-specific questions can be answered during  operating
                                    hours by the Reference Desk at each campus. For East Campus, dial  (407) 582-2456.Â&nbsp;
                                    For Lake Nona Campus, dial  (407) 582-4315 or (407) 582-4853.Â&nbsp; For  Osceola Campus,
                                    dial (407) 582-4154.Â&nbsp; For  West Campus, dial (407) 582-1432.Â&nbsp; For  Winter Park Campus,
                                    dial (407) 582-6832.Â&nbsp;
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/faq.pcf">©</a>
      </div>
   </body>
</html>