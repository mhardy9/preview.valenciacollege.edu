<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 8 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall8.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 8</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Databases vs. Search Engines</strong>
                  
               </p>
               
               <p>The following table explains some of the differences between Databases and Search
                  Engines. Glancing at the table, it is easy to see how a database can actually save
                  Val time conducting college level research but it also helps Val identify which tool
                  to access for different information needs. In some cases, a search engine is the best
                  option.
               </p>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td valign="top" width="423px">
                        
                        <p><strong><em>Library Databases</em></strong></p>
                        
                        <p>(ex. Academic Search Complete, General One File)</p>
                        
                     </td>
                     
                     <td valign="top" width="3px">
                        
                        <p><strong><em>Search Engines</em></strong></p>
                        
                        <p>(ex. Google, Yahoo &amp; Bing)</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="3px" colspan="2">
                        
                        <p><strong>Information Sources</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Scholarly or peer reviewed journal articles</li>
                           
                           <li>Popular and general magazine articles</li>
                           
                           <li>Newspaper articles</li>
                           
                           <li>Reference book articles or chapters</li>
                           
                           <li>Books</li>
                           
                           <li>Streaming media</li>
                           
                           <li>Minimal or no advertising</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Limited number of scholarly articles, most cost money</li>
                           
                           <li>Globally popular web sites (Wikipedia, Youtube, Facebook)</li>
                           
                           <li>Consumer &amp; Shopping web sites (Amazon, eBay)</li>
                           
                           <li>Goverment sites, (.gov) educational sites (.edu), groups and organizations (.org)
                              (ex. USDA, Valencia College, PETA)
                           </li>
                           
                           <li>Current news (Orlando Sentinel, USA Today)</li>
                           
                           <li>Blogs</li>
                           
                           <li>Advertisements</li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Organizational Structure</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Highly methodical, each individual record is searchable</li>
                           
                           <li>Resources are based on specific criteria and disciplines</li>
                           
                           <li>Advanced search options, including subject headings, publisher, author,date etc.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Reliability &amp; other criteria not evaluated when pages are indexed</li>
                           
                           <li>Ranked results, keyword searches often default to the sites that have been visited
                              the most often
                           </li>
                           
                           <li>Search engines cannot be searched by subject, as most web pages are not cataloged
                              with subject headings
                           </li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Reliability</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Journal articles have been evaluated by experts in the field</li>
                           
                           <li>Most material in a database has been published and has gone through an evaluative
                              and editorial process
                           </li>
                           
                           <li>Databases are updated frequently</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Minimal to no quality control governs the Web</li>
                           
                           <li>Author credentials are often not verifiable; anonymous content</li>
                           
                           <li>Some web sites contain outdated information, no regular updates</li>
                           
                           <li>All sources on the web need to be evaluated and verified by the user. Sites with rerferences
                              listed make this easier
                           </li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Cost&amp; Access</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Databases are funded through student tuition, college funding, and the Florida state
                              funding
                           </li>
                           
                           <li>Databases are accessible 24/7 through a student's Atlas account</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Overwhelming majority of information is free</li>
                           
                           <li>Library databases and most journals are inaccessible on the web</li>
                           
                           <li>Most evaluated &amp; credible information requires membership to a group or organization,
                              and usually requires fees
                           </li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Usability</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Highly organized; availability of advanced search options and increased relevancy
                              of results
                           </li>
                           
                           <li>All retrieved articles in a full text database are accessible</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Limited options for precise results. Requires more effort to narrow</li>
                           
                           <li>Only the first 1000 results are actually accessible (Google)</li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>When To Use</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>College level research</li>
                           
                           <li>Quick access to verified, credible information</li>
                           
                           <li>Current and updated information</li>
                           
                           <li>Information with references to other sources</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Personal information</li>
                           
                           <li>Information on groups, companies, Government, and edcucational institutions</li>
                           
                           <li>News, weather, travel, &amp; shopping</li>
                           
                           <li>Overviews, statistics, and brief articles</li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <u>The bottom line: Databases and search engines both serve useful purposes when conducting
                        research. Understanding the strengths and limitations of each can save Val time.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall8.html#">return to top</a> | <a href="Module_Two_Fall7.html">previous page</a> | <a href="Module_Two_Fall9.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall8.pcf">©</a>
      </div>
   </body>
</html>