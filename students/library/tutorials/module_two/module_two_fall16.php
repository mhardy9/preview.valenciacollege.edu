<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 16 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall16.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 16</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/Tiger.jpg" width="180" alt="Tiger.jpg" class="resizable" height="280" border="0" hspace="30"> <strong>Advanced Search Techniques</strong></p>
               
               <p>There are a variety of advanced search techniques beyond the scope of this module.
                  The following <a target="_blank" href="http://library.duke.edu/services/instruction/libraryguide/advsearch.html">link</a> provides more information for those that are interested. Below are two examples of
                  more commonly used advanced search methods.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>Phrase Searching</u>
                  
               </p>
               
               <p>Enclosing search terms within quotations enables a search for the words exactly as
                  they are entered within the quotes. For example, the following search for "tattoos
                  in Christianity" will find articles that contain this exact phrase somewhere within
                  the article. Limiting to phrase searches is an excellent way to narrow a search, but
                  keep in mind that entering long sentences such as "How do perceptions of tattoos differ
                  in Eastern religions, Judaism, and Christianity" will rarely retrieve meaningful results
                  and most likely will return no results.
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p>
                  <u>Truncation</u>
                  
               </p>
               
               <p>Using the * symbol at the end of a root word will find variations of that words ending.&nbsp;
                  For example entering the word <strong>tattoo*</strong> in a search will find tattoos, tattooing, tattooed and tattooer.&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a target="_blank" href="http://www.tattoospit.com/2010/08/tiger-tattoos.html">Tattoospit.com</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc-nd/2.0/">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall16.html#">return to top</a> | <a href="Module_Two_Fall15.html">previous page</a> | <a href="Module_Two_Fall17.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall16.pcf">©</a>
      </div>
   </body>
</html>