<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 5 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall5.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 5</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Accessing Tools</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p><img align="left" width="193" src="http://valenciacollege.edu/library/tutorials/module_two/Tools.jpg" alt="Tools.jpg" class="resizable" height="173" border="0" metadata="obj22"> Using the example from Module One, Val has identified several research questions
                  for five different topics.&nbsp; Take for example the question <strong>how do perceptions of tattoos differ in Eastern religions, Judaism and Christianity?</strong> &nbsp;Professor Sage has indicated that the research essay needs to have three types of
                  sources: books, articles from databases, and credible websites.&nbsp;Val has already found
                  some background information, but needs more specific sources to answer her research
                  question. Val can effectively and efficiently find the sources required for the assignment,
                  by being able to diffrentiate and choose the appropriate method and tool for accessing
                  each source.
               </p>
               
               <p><strong>Library Catalog -</strong> Most libraries have an electronic catalog that is essentially an online index of
                  all the items within a library's collection. While many resources are accessible through
                  a library catalog, it is primarily used for finding books on the library's shelves.
               </p>
               
               <p><strong>Research Databases -</strong> These are digital warehouse of information organzied to allow users easy access to
                  information. Databases are used primarily to find articles from journals, magazines,
                  and newspapers.
               </p>
               
               <p><strong>Search Engines</strong> - Search engines collect and organize information from the internet. They are used
                  primarily to locate web sites and documents on the internet. They have advanced options
                  that can help users find sites that are more credible than what the average random
                  search often retrieves.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall5.html#">return to top</a> | <a href="Module_Two_Fall4.html">previous page</a> | <a href="Module_Two_Fall6.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall5.pcf">©</a>
      </div>
   </body>
</html>