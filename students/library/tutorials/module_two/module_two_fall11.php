<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 11 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 11</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Keywords &amp; Synonyms</strong>
                  <img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/Mom.jpg" width="175" alt="Mom.jpg" class="resizable" height="256" border="0" hspace="30">
                  
               </p>
               
               <p>Most forms of retrieving information electronically are done through <a class="textpopper" href="Module_Two_Fall11.html#" onmouseover="return overlib('Words or phrases used to retrieve and narrow results in a library catalog, database or search engine.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Words or phrases used to retrieve and narrow results in a library catalog, database or search engine.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">keywords</a><img src="http://valenciacollege.edu/library/tutorials/module_two/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation2.html" border="0">.&nbsp; Val has often searched Google, mixing and matching keywords to find results.&nbsp;This
                  same concept applies to finding books and articles in the library's electronic databases
                  or catalog.&nbsp; In Module One, Val discovered that trying different keywords altered
                  results, and worked better than using the entire research question as a search.
               </p>
               
               <p>A good approach is to brainstorm a group of keywords and synonyms mentally, or to
                  write them down. Val should choose terms with similar meanings in case the first keyword
                  or two does not return the results she would like. A diverse group of keywords and
                  synonyms allows Val the option to construct alternative searches to find the information
                  she needs &nbsp; Using the research question of <strong>how do perceptions of tattoos differ in Eastern religions, Judaism and Christianity</strong>, a list of possible but not exhaustive keywords are listed below.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <table class="table bannerstyletable">
                  
                  <tr>
                     
                     <th scope="col" width="87px">
                        
                        <p><strong><u>Keyword</u></strong></p>
                        
                     </th>
                     
                     <th scope="col" width="334px">
                        
                        <p><strong><u>Also Try</u></strong></p>
                        
                     </th>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Perception</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="334px">
                        
                        <p>view, idea, opinion, judgement</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Tattoos</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="334px">
                        
                        <p>body art, body marking, body adornment, iconography (symbolic meaning of art)</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Christianity</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="334px">
                        
                        <p>religion, faith, belief, spirtuality (applies to Judaism &amp; Eastern Religions as well)</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Eastern Religions</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td>
                        
                        <p>Islam, Buddhism, Hinduism, Confucianism etc.</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a target="_blank" href="http://thebesttattoogalery.blogspot.com/2010/05/art-heart-tattoo-designs-for-men-and.html">Best Tattoo Gallery</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc-nd/2.0/">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall11.html#">return to top</a> | <a href="Module_Two_Fall10.html">previous page</a> | <a href="Module_Two_Fall12.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall11.pcf">©</a>
      </div>
   </body>
</html>