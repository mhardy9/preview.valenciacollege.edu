<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 4 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 4</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Which Sources?</strong>
                  
               </p>
               
               <p>The following elements will influence what sources Val decides to access:</p>
               
               <p><strong>A.&nbsp;&nbsp;Topic</strong> - The topic will dictate how and what you access. For example, if Val decides she
                  wants to research strictly history, culture, and fashion of tattoos, she would be
                  unlikely to find this information in a medical database.
               </p>
               
               <p><strong>B.&nbsp;&nbsp;Assignment or Need</strong> – Your professor may require specific sources for the assignment that will take some
                  of the guesswork out of what you need to access.&nbsp; Likewise, if your research is for
                  personal reasons you will want to consider informal methods of research (interviews,
                  talking with friends) as well as library resources.
               </p>
               
               <p><strong>C</strong>. <strong>Procrastination</strong> - Waiting until the last minute frequently has a negative impact on the sources used.
                  Additionally, <a href="http://www.amsciepub.com/doi/abs/10.2466/pr0.1995.77.2.691" target="_blank">studies</a> have identified procrastination with higher rates of plagiarism and <a href="http://www.sciencedirect.com/science/article/pii/S0748575110000448" target="_blank">lower academic performance</a>. <strong>Module 5</strong> will have more information about using information ethically to avoid plagiarism.&nbsp;
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall4.html#">return to top</a> | <a href="Module_Two_Fall3.html">previous page</a> | <a href="Module_Two_Fall5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall4.pcf">©</a>
      </div>
   </body>
</html>