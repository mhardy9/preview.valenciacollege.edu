<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 24 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall24.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 24</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>Databases provide electronic access mainly to <a class="textpopper" href="Module_Two_Fall24.html#" onmouseover="return overlib('Magazines, journals, or newspapers are considered periodicals.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Magazines, journals, or newspapers are considered periodicals.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">periodicals</a><img src="http://valenciacollege.edu/library/tutorials/module_two/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation4.html" border="0">. Below are a list of most of the sources found in a database:
               </p>
               
               <ul>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Magazines</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Journals</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Newspapers</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Book chapters</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Streaming Media</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Primary Sources</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>Journal articles in databases are often referred to as <a class="textpopper" href="Module_Two_Fall24.html#" onmouseover="return overlib('Usually journal articles published specfically for scholars and researchers in a field. They are often reviewed for accuracy by peers and experts in the field before publication.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Usually journal articles published specfically for scholars and researchers in a field. They are often reviewed for accuracy by peers and experts in the field before publication.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">scholarly, peer reviewed or credible sources</a><img src="http://valenciacollege.edu/library/tutorials/module_two/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation5.html" border="0">.&nbsp;Databases are also the best place to find the most current research on a given topic,
                  as books are often slightly behind the most current research. Val has searched the
                  general database titled <strong>Academic Search Complete</strong> for <strong>tattoos and Christianity</strong>.&nbsp; The articles she retrieved can be viewed in their entirety by selecting the Full
                  Text link, or she can quickly determine an article's usefulness by reading the abstract.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall24.html#">return to top</a> | <a href="Module_Two_Fall23.html">previous page</a> | <a href="Module_Two_Fall25.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall24.pcf">©</a>
      </div>
   </body>
</html>