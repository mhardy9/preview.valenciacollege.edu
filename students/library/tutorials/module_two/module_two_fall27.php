<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 27 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall27.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 27</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Check Your Answers</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <table class="table bannerstyletable">
                  
                  <tr>
                     
                     <th scope="col" width="174px">
                        
                        <p>Research Databse</p>
                        
                     </th>
                     
                     <th scope="col" width="174px">
                        
                        <p>Search Engine</p>
                        
                     </th>
                     
                     <th scope="col" width="137px">
                        
                        <p>Library Catalog</p>
                        
                     </th>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="174px">
                        
                        <p>A magazine article about tattoo culture</p>
                        
                     </td>
                     
                     <td width="174px">
                        
                        <p>Information from the American Medical Association on tattoos</p>
                        
                     </td>
                     
                     <td width="137px">
                        
                        <p>A book about the history of tattooing</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="174px">
                        
                        <p>A newspaper article about recent tattoo needle infections</p>
                        
                     </td>
                     
                     <td width="174px">
                        
                        <p>Government information on licensing for tattoo artists</p>
                        
                     </td>
                     
                     <td width="137px">
                        
                        <p>A reference book on body art in ancient history</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall27.html#">return to top</a> | <a href="Module_Two_Fall26.html">previous page</a> | <a href="Module_Two_Fall28.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall27.pcf">©</a>
      </div>
   </body>
</html>