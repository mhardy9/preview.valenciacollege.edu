<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information Literacy 2 - Accessing Sources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Information Literacy 2 - Accessing Sources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Information Literacy 2 - Accessing Sources</strong><br>Valencia College Libraries
               </p>
               
               
               <p>&nbsp;</p>
               
               <h2>
                  <strong>
                     <font size="6">
                        <u>Module 2</u>
                        </font>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong>
                  
               </p>
               
               <p>
                  <strong>
                     <font size="5">Scene 2.1</font>
                     </strong>
                  
               </p>
               
               <p>It is Thursday morning and the second day of Professor Sage's Comp I class. &nbsp;Professor
                  Sage has brought her class to the library where the librarian, Mr. Bookman, will demonstrate
                  how to access general information sources for the class's upcoming research essay.
                  Professor Sage has told students they will be constructing an annotated bibliography.
                  Once everybody is seated, the librarian begins the session.
               </p>
               
               <p><strong>BOOKMAN:</strong> &nbsp;Good morning everyone! (There is a chorus of replied good mornings from the class.)
                  C<em>ontinues:</em>&nbsp;My name is Reed Bookman and I will be demonstrating how to access resources the library
                  provides for your research needs.&nbsp; Most of you probably have some familiarity with
                  Google and Internet resources, so I am going to give you a handout on website evaluation
                  to help guide you in selecting credible information using web resources (shuffling
                  of handouts being passed around the room).&nbsp; Today's session, however, will focus exclusively
                  on information available through the library's resources. &nbsp;
               </p>
               
               <p><strong>PROFESSOR SAGE:</strong> &nbsp;They all have ideas of topics that they would like to do and they are in the process
                  of narrowing it down to a research question.&nbsp; They will need books, articles, and
                  websites as sources.
               </p>
               
               <p><strong>BOOKMAN</strong>: Understood.&nbsp; The first thing to mention is that all the electronic information in
                  the library is accessible through your Atlas account, and your student ID is your
                  library card that you will use to check out books and other items from the library.
               </p>
               
               <p><strong>MATT:</strong> (Raising his hand):&nbsp; I had a library instruction a few years ago before I joined
                  the military, and we had to have an ID and pin to access resources.&nbsp; Are you saying
                  now that we only need to log in to Atlas to access everything?
               </p>
               
               <p><strong>BOOKMAN:</strong> That is correct. There is no longer any need to remember several usernames and passwords
                  to access the library's materials. &nbsp;(several students nod appreciatively).&nbsp; Keep in
                  mind that all of the librarians are here to help, and sometimes the most effective
                  search strategy you can employ to start with is to ask a librarian.&nbsp; Librarians can
                  always be found at the Reference desk if you need immediate assistance and we are
                  also available through chat, email, and telephone.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Steps To Accessing Sources</font>
                     </strong>
                  
               </p>
               
               <p>Once a research topic has been chosen, there are three important steps to take in
                  order to access and acquire the information you need:&nbsp;
               </p>
               
               <p><strong>1.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> Choosing and Accessing Appropriate Sources
               </p>
               
               <p><strong>2.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> Refining the Search Strategy
               </p>
               
               <p><strong>3.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</strong> Managing Information
               </p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Step 1. Choosing and Accessing Appropriate Sources</font>
                     </strong>
                  
               </p>
               
               <p>The first step is to choose the appropriate source that contains information about
                  a chosen topic.&nbsp; Standard Two of the American College and Research Libraries' Information
                  Literacy Competency Standards for Higher Education states, "The information literate
                  student accesses needed information effectively and efficiently." Essentially, this
                  means you will want to identify the best means of accessing sources that will provide
                  the information you need, or that your professor has required you to use, rather than
                  wasting time on sources that will not help you with your topic.&nbsp;Val's professor has
                  already indentified appropriate sources in the form of books, research databases,
                  and websites as tools that will help Val find useful information. While the library
                  resources will be helpful to Val's personal interest in a tattoo, she might also want
                  to interview people or conduct surveys of students and friends that have tattoos to
                  find additional information regarding her personal consideration of a tattoo. The
                  next page discusses several factors that influence what sources Val decides to access.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Which Sources?</strong>
                  
               </p>
               
               <p>The following elements will influence what sources Val decides to access:</p>
               
               <p><strong>A.&nbsp;&nbsp;Topic</strong> - The topic will dictate how and what you access. For example, if Val decides she
                  wants to research strictly history, culture, and fashion of tattoos, she would be
                  unlikely to find this information in a medical database.
               </p>
               
               <p><strong>B.&nbsp;&nbsp;Assignment or Need</strong> – Your professor may require specific sources for the assignment that will take some
                  of the guesswork out of what you need to access.&nbsp; Likewise, if your research is for
                  personal reasons you will want to consider informal methods of research (interviews,
                  talking with friends) as well as library resources.
               </p>
               
               <p><strong>C</strong>. <strong>Procrastination</strong> - Waiting until the last minute frequently has a negative impact on the sources used.
                  Additionally, studies have identified procrastination with higher rates of plagiarism
                  and lower academic performance. <strong>Module 5</strong> will have more information about using information ethically to avoid plagiarism.&nbsp;
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Accessing Tools</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p><img align="left" width="193" src="http://valenciacollege.edu/library/tutorials/module_two/Tools.jpg" alt="Tools.jpg" class="resizable" height="173" border="0" metadata="obj22"> Using the example from Module One, Val has identified several research questions
                  for five different topics.&nbsp; Take for example the question <strong>how do perceptions of tattoos differ in Eastern religions, Judaism and Christianity?</strong> &nbsp;Professor Sage has indicated that the research essay needs to have three types of
                  sources: books, articles from databases, and credible websites.&nbsp;Val has already found
                  some background information, but needs more specific sources to answer her research
                  question. Val can effectively and efficiently find the sources required for the assignment,
                  by being able to diffrentiate and choose the appropriate method and tool for accessing
                  each source.
               </p>
               
               <p><strong>Library Catalog -</strong> Most libraries have an electronic catalog that is essentially an online index of
                  all the items within a library's collection. While many resources are accessible through
                  a library catalog, it is primarily used for finding books on the library's shelves.
               </p>
               
               <p><strong>Research Databases -</strong> These are digital warehouse of information organzied to allow users easy access to
                  information. Databases are used primarily to find articles from journals, magazines,
                  and newspapers.
               </p>
               
               <p><strong>Search Engines</strong> - Search engines collect and organize information from the internet. They are used
                  primarily to locate web sites and documents on the internet. They have advanced options
                  that can help users find sites that are more credible than what the average random
                  search often retrieves.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>The Library Catalog &amp; Databases</strong>
                  
               </p>
               
               <p>
                  <strong><font size="5">Scene 2.2</font>&nbsp;</strong>
                  
               </p>
               
               <p>Val's class logs in to the library computers and Mr. Bookman, the librarian, begins
                  the interactive lesson. Val follows along on her computer as the librarian's steps
                  are shown on the projector at the front of the room.
               </p>
               
               <p><strong>LIBRARIAN:</strong> The first thing you will need to do is to login to Atlas in order to access books,
                  e-books, and articles via research databases. Even if you are on campus, you will
                  still need to login to access the research databases. Click the Search Libraries link
                  shown in the image below.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"><img width="380" src="http://valenciacollege.edu/library/tutorials/module_two/Atlas.jpg" alt="Atlas.jpg" class="resizable" height="207" border="0" metadata="obj25"> &nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><strong>LIBRARIAN:</strong> Once you log into Atlas, you can access both books and databases from the Valencia
                  Libraries Page.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img longdesc="Valencia Libraries Page" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/PicTwo.jpg" width="991" alt="PicTwo.jpg" class="resizable" height="420" border="0" hspace="30">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p><strong><u>The bottom line: &nbsp;For complete access to Valencia Libraries resources, Val needs to
                        login to Atlas and click Search The Libraries before she can proceed researching the
                        library databases.</u></strong>&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Databases vs. Search Engines</strong>
                  
               </p>
               
               <p>The following table explains some of the differences between Databases and Search
                  Engines. Glancing at the table, it is easy to see how a database can actually save
                  Val time conducting college level research but it also helps Val identify which tool
                  to access for different information needs. In some cases, a search engine is the best
                  option.
               </p>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td valign="top" width="423px">
                        
                        <p><strong><em>Library Databases</em></strong></p>
                        
                        <p>(ex. Academic Search Complete, General One File)</p>
                        
                     </td>
                     
                     <td valign="top" width="3px">
                        
                        <p><strong><em>Search Engines</em></strong></p>
                        
                        <p>(ex. Google, Yahoo &amp; Bing)</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="3px" colspan="2">
                        
                        <p><strong>Information Sources</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Scholarly or peer reviewed journal articles</li>
                           
                           <li>Popular and general magazine articles</li>
                           
                           <li>Newspaper articles</li>
                           
                           <li>Reference book articles or chapters</li>
                           
                           <li>Books</li>
                           
                           <li>Streaming media</li>
                           
                           <li>Minimal or no advertising</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Limited number of scholarly articles, most cost money</li>
                           
                           <li>Globally popular web sites (Wikipedia, Youtube, Facebook)</li>
                           
                           <li>Consumer &amp; Shopping web sites (Amazon, eBay)</li>
                           
                           <li>Goverment sites, (.gov) educational sites (.edu), groups and organizations (.org)
                              (ex. USDA, Valencia College, PETA)
                           </li>
                           
                           <li>Current news (Orlando Sentinel, USA Today)</li>
                           
                           <li>Blogs</li>
                           
                           <li>Advertisements</li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Organizational Structure</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Highly methodical, each individual record is searchable</li>
                           
                           <li>Resources are based on specific criteria and disciplines</li>
                           
                           <li>Advanced search options, including subject headings, publisher, author,date etc.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Reliability &amp; other criteria not evaluated when pages are indexed</li>
                           
                           <li>Ranked results, keyword searches often default to the sites that have been visited
                              the most often
                           </li>
                           
                           <li>Search engines cannot be searched by subject, as most web pages are not cataloged
                              with subject headings
                           </li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Reliability</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Journal articles have been evaluated by experts in the field</li>
                           
                           <li>Most material in a database has been published and has gone through an evaluative
                              and editorial process
                           </li>
                           
                           <li>Databases are updated frequently</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Minimal to no quality control governs the Web</li>
                           
                           <li>Author credentials are often not verifiable; anonymous content</li>
                           
                           <li>Some web sites contain outdated information, no regular updates</li>
                           
                           <li>All sources on the web need to be evaluated and verified by the user. Sites with rerferences
                              listed make this easier
                           </li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Cost&amp; Access</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Databases are funded through student tuition, college funding, and the Florida state
                              funding
                           </li>
                           
                           <li>Databases are accessible 24/7 through a student's Atlas account</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Overwhelming majority of information is free</li>
                           
                           <li>Library databases and most journals are inaccessible on the web</li>
                           
                           <li>Most evaluated &amp; credible information requires membership to a group or organization,
                              and usually requires fees
                           </li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>Usability</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>Highly organized; availability of advanced search options and increased relevancy
                              of results
                           </li>
                           
                           <li>All retrieved articles in a full text database are accessible</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Limited options for precise results. Requires more effort to narrow</li>
                           
                           <li>Only the first 1000 results are actually accessible (Google)</li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="810px" colspan="2">
                        
                        <p><strong>When To Use</strong></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td valign="top" width="414px">
                        
                        <ul>
                           
                           <li>College level research</li>
                           
                           <li>Quick access to verified, credible information</li>
                           
                           <li>Current and updated information</li>
                           
                           <li>Information with references to other sources</li>
                           
                        </ul>
                        
                     </td>
                     
                     <td valign="top" width="398px">
                        
                        <ul>
                           
                           <li>Personal information</li>
                           
                           <li>Information on groups, companies, Government, and edcucational institutions</li>
                           
                           <li>News, weather, travel, &amp; shopping</li>
                           
                           <li>Overviews, statistics, and brief articles</li>
                           
                        </ul>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <u>The bottom line: Databases and search engines both serve useful purposes when conducting
                        research. Understanding the strengths and limitations of each can save Val time.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Search Strategies</strong>
                  
               </p>
               
               <p align="left"><strong>Subject Headings (Controlled Vocabulary) -</strong> Library catalogs and databases use controlled vocabulary also known as subject headings,
                  as a means of organizing information. Valencia libraries, and most academic libraries
                  use the Library Of Congress Subject Headings as their controlled vocabulary. Often,
                  it can be more effective than using simple keyword searches.
               </p>
               
               <p>For example, if Val does a simple keyword search on "Hindu" in the Valencia library
                  catalog she will retrieve books on art, philosophy, music, civilization, and culture
                  for a total of 254 books. Val decides to change her search to "Hindu Religion". This
                  brings up 98 titles, some books on Hindu religion are retrieved but also encyclopedias
                  of all religions are returned and Hindu is just one of them. A subject search on the
                  term "Hinduism" would retrieve 48 books only on the Hindu religion and not peripheral
                  or related religions.
               </p>
               
               <p>Similarly, Val has searched the Christian Bible for information on Christanity's teachings
                  regarding tattoos. She tries a similar search to locate a Hindu text using the terms
                  "Hindu Bible" but has no results. In this case, using the subject heading <strong>Hinduism, Sacred Texts</strong> will return the results Val is looking for.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>&nbsp;Using Subject Headings</strong>
                  
               </p>
               
               <p align="left"><img align="left" width="106" src="http://valenciacollege.edu/library/tutorials/module_two/question1.JPG" alt="question1.JPG" class="resizable" height="106" border="0" metadata="obj20"> How does a person find the proper subject headings for a search? It is easier than
                  you might think.! Let's take Val's search from the previous slide "Hindu religion".
                  Below is the first book in her list of results. Simply click on the Detalils link,
                  and beside <strong>Subjects</strong> are the headings to describe that book.
               </p>
               
               <p>In the example below. <em>Hinduism; Gods</em>, <em>Hindu; India- - Religion</em> are the three subject headings for this record. Clicking on any of these headings
                  will bring up books classified under that particular subject. Note, that Hinduism
                  is the first heading in the list, which means it is usually the primary heading.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img width="969" src="http://valenciacollege.edu/library/tutorials/module_two/Hindu%20Screenshot.jpg" alt="Hindu Screenshot.jpg" class="resizable" height="341" border="0" metadata="obj21">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Keywords &amp; Synonyms</strong>
                  <img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/Mom.jpg" width="175" alt="Mom.jpg" class="resizable" height="256" border="0" hspace="30">
                  
               </p>
               
               <p>Most forms of retrieving information electronically are done through keywords.&nbsp; Val
                  has often searched Google, mixing and matching keywords to find results.&nbsp;This same
                  concept applies to finding books and articles in the library's electronic databases
                  or catalog.&nbsp; In Module One, Val discovered that trying different keywords altered
                  results, and worked better than using the entire research question as a search.
               </p>
               
               <p>A good approach is to brainstorm a group of keywords and synonyms mentally, or to
                  write them down. Val should choose terms with similar meanings in case the first keyword
                  or two does not return the results she would like. A diverse group of keywords and
                  synonyms allows Val the option to construct alternative searches to find the information
                  she needs &nbsp; Using the research question of <strong>how do perceptions of tattoos differ in Eastern religions, Judaism and Christianity</strong>, a list of possible but not exhaustive keywords are listed below.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <table class="table bannerstyletable">
                  
                  <tr>
                     
                     <th scope="col" width="87px">
                        
                        <p><strong><u>Keyword</u></strong></p>
                        
                     </th>
                     
                     <th scope="col" width="334px">
                        
                        <p><strong><u>Also Try</u></strong></p>
                        
                     </th>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Perception</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="334px">
                        
                        <p>view, idea, opinion, judgement</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Tattoos</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="334px">
                        
                        <p>body art, body marking, body adornment, iconography (symbolic meaning of art)</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Christianity</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="334px">
                        
                        <p>religion, faith, belief, spirtuality (applies to Judaism &amp; Eastern Religions as well)</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="87px">
                        
                        <p>Eastern Religions</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td>
                        
                        <p>Islam, Buddhism, Hinduism, Confucianism etc.</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Best Tattoo Gallery. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Boolean Operators</strong>
                  
               </p>
               
               <p>Boolean searching is the means of organizing keywords into concept sets.&nbsp; Electronic
                  resources are constructed to allow the use of Boolean operators to retrieve information.&nbsp;
                  This includes Google, library book catalogs, and electronic databases.&nbsp; The images
                  below illustrate how Boolean operators relate.&nbsp;&nbsp; For most research assignments at
                  Valencia College, the AND operator is usually sufficient to find relevant articles
                  in a database. However more advanced research topics may require the use of some or
                  all Boolean operators in a search. This flash video gives a quick and easy demonstration
                  of how easy Boolean logic can be.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Boolean Operators: Using the AND Operator</strong>
                  
               </p>
               
               <p>The search Religion AND tattoos is represented. Using this example, articles that
                  included both the word <strong>religion</strong> and the word <strong>tattoo</strong> would be returned. &nbsp; Articles with only one of those words somewhere in the text
                  would be ignored. &nbsp; As more keywords are added to a search, the number of articles
                  will decrease. &nbsp; When too many keywords are used, the database will often return no
                  results. &nbsp; For this reason, starting with a couple keywords first is a good way to
                  gauge how many articles are available before getting more specific.
               </p>
               
               <p>The diagram below is a visual representation of the search used above.The lighter
                  colored sections represent all the articles available in the Academic Search Complete
                  database that contain either the word religion or the word tattoo.&nbsp; The darkened intersection
                  is articles that contain <strong>both</strong> of these keywords within the article.&nbsp; The AND operator is most useful for finding
                  information on topics with <strong>two or more</strong> components.&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="Venn diagram with religion and tattoos" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/PicThree.png" width="264" alt="PicThree.png" class="resizable" height="187" border="0" hspace="30"> <strong>&nbsp;</strong></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Boolean Operators: Using the OR Operator</strong>
                  
               </p>
               
               <p>In this search, Liberal OR Democrat retrieves all articles containing either word,
                  regardless of whether they appear together in the same article or not. This operator
                  is used to get an overview of what is available before focusing more specifically
                  on one or both topics.&nbsp; It is also useful for searching for words with common synonyms
                  or alternate spellings, such as ax OR axe. Using the OR operator will return the largest
                  amount of articles of all the Boolean operators.
               </p>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="Liberal Democrat Venn Diagram" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/LDVennDiagram.png" width="335" alt="LDVennDiagram.png" class="resizable" height="205" border="0" hspace="30"> &nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Boolean Operators: Using the NOT Operator</strong>
                  
               </p>
               
               <p>In this search, School NOT Middle, all articles containing the word middle will be
                  eliminated from the list of results. The NOT operator is useful for excluding unwanted
                  results from articles that appear in a general search, for example articles about
                  New York&nbsp;but not the Yankees baseball team would look like this: New&nbsp;York NOT Yankees.
                  However, the NOT operator can also exclude relevant articles so use it carefully.&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="School Middle Venn Diagram" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/SchoolVenn%20Diagram.png" width="364" alt="SchoolVenn Diagram.png" class="resizable" height="211" border="0" hspace="30"> &nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/Tiger.jpg" width="180" alt="Tiger.jpg" class="resizable" height="280" border="0" hspace="30"> <strong>Advanced Search Techniques</strong></p>
               
               <p>There are a variety of advanced search techniques beyond the scope of this module.
                  The following link provides more information for those that are interested. Below
                  are two examples of more commonly used advanced search methods.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>Phrase Searching</u>
                  
               </p>
               
               <p>Enclosing search terms within quotations enables a search for the words exactly as
                  they are entered within the quotes. For example, the following search for "tattoos
                  in Christianity" will find articles that contain this exact phrase somewhere within
                  the article. Limiting to phrase searches is an excellent way to narrow a search, but
                  keep in mind that entering long sentences such as "How do perceptions of tattoos differ
                  in Eastern religions, Judaism, and Christianity" will rarely retrieve meaningful results
                  and most likely will return no results.
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p>
                  <u>Truncation</u>
                  
               </p>
               
               <p>Using the * symbol at the end of a root word will find variations of that words ending.&nbsp;
                  For example entering the word <strong>tattoo*</strong> in a search will find tattoos, tattooing, tattooed and tattooer.&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Tattoospit.com. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Activity: Connecting Keywords</strong>
                  
               </p>
               
               <p align="left">In the following activity, practice using Boolean Operators and phrase searching techniques
                  to combine keywords.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Using Boolean In Google</strong>
                  
               </p>
               
               <p align="left">The Google advanced search allows for the use of Boolean operators. Below is the advanced
                  search screen in Google and the arrows indicate which search bars represent the different
                  Boolean operators. You could also write this search out in the regular Google search
                  bar. It would look like this: <strong>religion tattoos OR "body art" -piercing.</strong> In Google, the minus sign represents the NOT operator, while the AND operator is
                  automatically inserted between every keyword unless otherwise substituted by OR, or
                  the minus sign. In other words, there is an invisible AND between <strong>religion tattoos</strong> in the bolded search above. Quotation marks in Google are a phrase search and work
                  the same as in the library research databases.
               </p>
               
               <p>&nbsp;Credible websites are only determined by evaluating them carefully. Module 3 will
                  explain in depth how to evaluate websites.
               </p>
               
               <p align="left">
                  <img width="834" src="http://valenciacollege.edu/library/tutorials/module_two/Boolean%20Google.jpg" alt="Boolean Google.jpg" class="resizable" height="375" border="0" metadata="obj14">
                  
               </p>
               
               <p><strong><u>The bottom line: Using keywords, Boolean Operators, and other techniques effectively
                        enables Val to focus her searches and find information more efficiently.</u></strong>&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Library Catalog Search</strong>
                  
               </p>
               
               <p>The library catalog is used primarily to locate books, DVDs, and eBooks within the
                  library. Books will often provide the best background and most in depth information
                  on many topics at the cost of being slightly behind the most recent updates. Magazine,
                  Journal, newspaper articles, and various streaming audio or video files are found
                  in Valencia's databases.&nbsp; Below is a list of records that Val retrieved from the library
                  catalog using the search <strong>tattoos and culture.&nbsp;</strong></p>
               
               <p>Val can click Location to find what libraries own the book, and if she wishes to place
                  a hold or have the book sent to her she can click the Request Item link. &nbsp;The <strong>call number</strong> gives the shelf location for the book in the library and begins with one or two letters
                  and is followed by numbers including the year it was published. Only books that are
                  listed as <strong>Circulation</strong> can be checked out. <strong>Reference</strong> books can be used in the library only. In most libraries, a reference book will have
                  an R or REF before the call number.&nbsp;
               </p>
               
               <p>
                  <strong>Ex. Body piercing and tattoos fashion - Call number : REF GT2345.B63 2003</strong>
                  
               </p>
               
               <p>Val can also click Details to find table of contents information as well as the subject
                  headings we discussed earlier.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p align="center"></p>
               
               <p>Val has the option to narrow her results to certain filters on the left frame under
                  Refine My Results. Related subjects and publication dates are just a few of the many
                  narrowing options listed.
               </p>
               
               <p>
                  <img longdesc="Valencia catalog results" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/PicFour.jpg" width="1067" alt="PicFour.jpg" class="resizable" height="716" border="0" hspace="30">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Activity: How Call Numbers Work</strong>
                  
               </p>
               
               <p>Books are arranged alphabetically according to the first letter in the call number.
                  Val reviews the following handout to learn more about the Library of Congress call
                  numbers and then practices this virtual shelving exercise.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp; </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Check Your Answers</strong>
                  
               </p>
               
               <p>The correct order for the previous exercise is :</p>
               
               <ol>
                  
                  <li>GT2343.G77 2002</li>
                  
                  <li>GT2345.M55 1997</li>
                  
                  <li>GT2345.R56 2004</li>
                  
                  <li>HV6439.U5 V354 2000</li>
                  
                  <li>RD119.5.B82 W54 1998</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Database Search</strong>
                  
               </p>
               
               <p>Valencia has over 180 databases, which can make it difficult to choose the appropriate
                  database for Val's needs. Using the subject listing for databases makes the choice
                  easier if Val is unsure of which database will be useful.&nbsp; As a rule of thumb, databases
                  grouped under the General subject contain information on most topics, but for certain
                  assignments they may not be specific enough to be useful.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="Choosing databases by subject" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/PicFive.jpg" width="779" alt="PicFive.jpg" class="resizable" height="691" border="0" hspace="30"> &nbsp;
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p align="center"></p>
               
               <p>Databases provide electronic access mainly to periodicals. Below are a list of most
                  of the sources found in a database:
               </p>
               
               <ul>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Magazines</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Journals</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Newspapers</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Book chapters</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Streaming Media</li>
                  
                  <li>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Primary Sources</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>Journal articles in databases are often referred to as scholarly, peer reviewed or
                  credible sources.&nbsp;Databases are also the best place to find the most current research
                  on a given topic, as books are often slightly behind the most current research. Val
                  has searched the general database titled <strong>Academic Search Complete</strong> for <strong>tattoos and Christianity</strong>.&nbsp; The articles she retrieved can be viewed in their entirety by selecting the Full
                  Text link, or she can quickly determine an article's usefulness by reading the abstract.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>Notice that the subject headings for each record are listed at the end of the abstract
                  for that record. For example, <strong>Tattoing - - Religious Aspects</strong> is a heading in the first record and would be a good term to use for other related
                  articles. Val can click Add to folder for each article as well as email, or save all
                  items in her folder at once which is useful when in a rush and bypasses print costs.&nbsp;
                  The left pane allows Val to filter her results to more current articles, or specific
                  sources such as Academic journals.
               </p>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="Abstract example in the catalog" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/PicSix.jpg" width="1040" alt="PicSix.jpg" class="resizable" height="826" border="0" hspace="30"> &nbsp;&nbsp;
               </p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Activity: Choosing the Best Access Tool</strong>
                  
               </p>
               
               <p align="left">In the following activity, practice matching each type of source with the most appropriate
                  tool for accessing it.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<strong>&nbsp;</strong></p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Check Your Answers</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <table class="table bannerstyletable">
                  
                  <tr>
                     
                     <th scope="col" width="174px">
                        
                        <p>Research Databse</p>
                        
                     </th>
                     
                     <th scope="col" width="174px">
                        
                        <p>Search Engine</p>
                        
                     </th>
                     
                     <th scope="col" width="137px">
                        
                        <p>Library Catalog</p>
                        
                     </th>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="174px">
                        
                        <p>A magazine article about tattoo culture</p>
                        
                     </td>
                     
                     <td width="174px">
                        
                        <p>Information from the American Medical Association on tattoos</p>
                        
                     </td>
                     
                     <td width="137px">
                        
                        <p>A book about the history of tattooing</p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="174px">
                        
                        <p>A newspaper article about recent tattoo needle infections</p>
                        
                     </td>
                     
                     <td width="174px">
                        
                        <p>Government information on licensing for tattoo artists</p>
                        
                     </td>
                     
                     <td width="137px">
                        
                        <p>A reference book on body art in ancient history</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <font size="5">Step 2. Refining the Search Strategy</font>
                     </strong>
                  
               </p>
               
               <p>Refining a search involves reflecting upon the sources of information that have been
                  retrieved in a search and determining if they are of a quality and quantity that is
                  sufficient to complete the assignment or need. Refining a search may be necessary
                  if hundreds or perhaps thousands of articles are returned using a keyword search.
                  Refinment is also necessary if a topic requires the most current research articles,
                  such as medical, technology and science topics but only older sources have been found.
                  Val should also consider refining a search if many of the sources that have been returned
                  are biased, for example a research study sponsored by Folgers Coffee that claims numerous
                  health benefits associated with drinking coffee.
               </p>
               
               <p>When refining a search, it might require trying different databases or a new string
                  of keywords to search in a current database.&nbsp; This is where having the keyword chart
                  previously mentioned becomes useful. &nbsp;Refining a search can also involve switching
                  to another access strategy altogether, such as visiting another library, contacting
                  a government agency or group, and borrowing items from another library which is known
                  as interlibrary loan.&nbsp; Seeking the assistance of a librarian would also be an important
                  step in refining a search strategy and in obtaining an interlibrary loan.
               </p>
               
               <p>&nbsp;</p>
               
               <p><img vspace="10" width="204" src="http://valenciacollege.edu/library/tutorials/module_two/Koi.jpg" alt="Koi.jpg" class="resizable" height="248" border="0" hspace="30"> &nbsp;
               </p>
               
               <p align="center">(Photo credit: Artux.org. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>How many results?</strong>
                  
               </p>
               
               <p>While there are no standard rules for how many sources are considered to be adequate
                  to complete a research topic, many indexes, such as Google limit Val's results to
                  roughly 1000 sources, even though many thousands more results are listed. Most databases
                  through Valencia do not impose a limit on your results. A search for <strong>tattoos</strong> in the Academic Search Complete database will return about 2400 articles<strong>.</strong> Do the same search in Google and over 46 million results are returned! However, Val
                  will actually have access to fewer than a 1000 of those results<strong>.</strong> For some people, a thousand results might be a perfect pool to begin with, while
                  for others they might prefer to narrow to a hundred articles or less.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: In many instances, the research databases give you not only more
                        credible resources, but a larger pool that is accessible.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Uncovering More Sources</strong>
                  
               </p>
               
               <p>&nbsp;A useful tip, and an easy way to refine a search, is to check the <strong>bibliographies or footnotes</strong> from an article or book you have found. Val could use the sources from References
                  to the article titled "Christian Student Perceptions Of Body Tattoos" to find other
                  sources through Valencia or another library.
               </p>
               
               <p>&nbsp;</p>
               
               <hr>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img width="770" src="http://valenciacollege.edu/library/tutorials/module_two/References.jpg" alt="References.jpg" class="resizable" height="649" border="0" metadata="obj11">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>
                  <strong>
                     <font size="5">Step 3. Managing Information</font>
                     </strong>
                  
               </p>
               
               <p>Val now needs a method for organizing and managing the information she has found,
                  so that she can easily refer to it and access it when necessary. Most books can be
                  temporarily checked out from the library, and photocopies of a limited number of pages
                  from reference books can be made. Electronic database articles can be emailed, printed,
                  bookmarked, or saved to a storage device as needed. The bookmarking option is the
                  only one that does not give you immediate access to the article. In order to access
                  the bookmarked article you will need to be logged into Atlas first. Use the Permalink
                  or Bookmark option near the bottom of the Tools menu if you plan to save the links
                  to your articles, or share them with others when working on a group project via Facebook,
                  Twitter or other service. Keep in mind that those you share the link with will have
                  to login to Atlas to be able to access it.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Printing, Saving and E-mailing Articles from Databases</strong>
                  
               </p>
               
               <p>Below, Val has selected article #3 from the previous search, titled "Christian Student
                  Perceptions of Body Tattoos: A Qualitative Analysis" by clicking on the title link.&nbsp;
                  Val has several options for managing the article, including printing, saving it, or
                  emailing it to herself.&nbsp; The citation information for the article includes the title
                  and beneath that, the authors, source, and date of publication information are all
                  provided.&nbsp; The citation information for all sources used in a research paper is usually
                  included at the end of a research paper.&nbsp; This list of cited sources is referred to
                  as a Works Cited Page, Bibliography, or References, depending upon the discipline
                  or course. <strong>Module 5</strong> will discuss in greater detail the different methods and styles of citing sources.
               </p>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="Print, email and save options" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_two/PicSeven.jpg" width="1042" alt="PicSeven.jpg" class="resizable" height="684" border="0" hspace="30"> &nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Activity: Review the Vocabulary</strong>
                  
               </p>
               
               <p>Complete the crossword puzzle to recall terms used in this module.&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <font size="5">Conclusion</font>
                  <img align="left" width="106" src="http://valenciacollege.edu/library/tutorials/module_two/info1.JPG" alt="info1.JPG" class="resizable" height="106" border="0" metadata="obj17">
                  
               </p>
               
               <p align="left">
                  <strong>This concludes Module #2: Accessing Sources. In this module, you learned:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>The steps involved in Accessing Sources: 1. Choosing Appropriate Sources 2. Refining
                     The Search Strategy 3. Manging Information
                  </li>
                  
                  <li>The reasons and methods for each of these steps</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>You also acquired the following skills:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Knowledge of how to access Valencia's print and electronic resources through Atlas</li>
                  
                  <li>Brainstorm keywords</li>
                  
                  <li>Employ Boolean search methods</li>
                  
                  <li>Locate and request items from from your library and other libraries</li>
                  
                  <li>Identify various sources</li>
                  
                  <li>Assess the quality of sources</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall_print.pcf">©</a>
      </div>
   </body>
</html>