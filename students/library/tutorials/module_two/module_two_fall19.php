<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 19 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall19.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 19</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Library Catalog Search</strong>
                  
               </p>
               
               <p>The library catalog is used primarily to locate books, DVDs, and eBooks within the
                  library. Books will often provide the best background and most in depth information
                  on many topics at the cost of being slightly behind the most recent updates. Magazine,
                  Journal, newspaper articles, and various streaming audio or video files are found
                  in Valencia's databases.&nbsp; Below is a list of records that Val retrieved from the library
                  catalog using the search <strong>tattoos and culture.&nbsp;</strong></p>
               
               <p>Val can click Location to find what libraries own the book, and if she wishes to place
                  a hold or have the book sent to her she can click the Request Item link. &nbsp;The <strong>call number</strong> gives the shelf location for the book in the library and begins with one or two letters
                  and is followed by numbers including the year it was published. Only books that are
                  listed as <strong>Circulation</strong> can be checked out. <strong>Reference</strong> books can be used in the library only. In most libraries, a reference book will have
                  an R or REF before the call number.&nbsp;
               </p>
               
               <p>
                  <strong>Ex. Body piercing and tattoos fashion - Call number : REF GT2345.B63 2003</strong>
                  
               </p>
               
               <p>Val can also click Details to find table of contents information as well as the subject
                  headings we discussed earlier.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall19.html#">return to top</a> | <a href="Module_Two_Fall18.html">previous page</a> | <a href="Module_Two_Fall20.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall19.pcf">©</a>
      </div>
   </body>
</html>