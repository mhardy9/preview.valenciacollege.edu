<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 34 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall34.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Page 34</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <font size="5" style="line-height: 1">Conclusion</font>
                  <img align="left" width="106" src="http://valenciacollege.edu/library/tutorials/module_two/info1.JPG" alt="info1.JPG" class="resizable" height="106" border="0" metadata="obj17">
                  
               </p>
               
               <p align="left">
                  <strong>This concludes Module #2: Accessing Sources. In this module, you learned:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>The steps involved in Accessing Sources: 1. Choosing Appropriate Sources 2. Refining
                     The Search Strategy 3. Manging Information
                  </li>
                  
                  <li>The reasons and methods for each of these steps</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>You also acquired the following skills:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Knowledge of how to access Valencia's print and electronic resources through Atlas</li>
                  
                  <li>Brainstorm keywords</li>
                  
                  <li>Employ Boolean search methods</li>
                  
                  <li>Locate and request items from from your library and other libraries</li>
                  
                  <li>Identify various sources</li>
                  
                  <li>Assess the quality of sources</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <div id="report" align="center">
                  
                  <p>After finishing this lesson, complete the form below:</p>
                  
                  <table class="table ">
                     
                     <tr>
                        <td>
                           
                           <form name="send_form" method="" action="" onsubmit="" id="send_form">
                              
                              <h2>Type your name or identifier:</h2>
                              
                              <p><input type="text" name="user_name" size="40"></p>
                              
                              <hr>
                              <input type="button" onclick="parent.lessonReport('summary')" value="Print Score Summary" name="PS">
                              
                           </form>
                           
                        </td>
                     </tr>
                     
                  </table>
                  
               </div>
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall34.html#">return to top</a> | <a href="Module_Two_Fall33.html">previous page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall34.pcf">©</a>
      </div>
   </body>
</html>