<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Module 2 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_two/module_two_fall.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_two/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_two/">Module Two</a></li>
               <li>Module 2</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>&nbsp;</p>
               <a name="module2" id="module2"></a><h2>
                  <strong>
                     <font size="6" style="line-height: 1">
                        <u>Module 2</u>
                        </font>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong>
                  
               </p>
               
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Scene 2.1</font>
                     </strong>
                  
               </p>
               
               <p>It is Thursday morning and the second day of Professor Sage's Comp I class. &nbsp;Professor
                  Sage has brought her class to the library where the librarian, Mr. Bookman, will demonstrate
                  how to access general information sources for the class's upcoming research essay.
                  Professor Sage has told students they will be constructing an annotated bibliography.
                  Once everybody is seated, the librarian begins the session.
               </p>
               
               <p><strong>BOOKMAN:</strong> &nbsp;Good morning everyone! (There is a chorus of replied good mornings from the class.)
                  C<em>ontinues:</em>&nbsp;My name is Reed Bookman and I will be demonstrating how to access resources the library
                  provides for your research needs.&nbsp; Most of you probably have some familiarity with
                  Google and Internet resources, so I am going to give you a handout on website evaluation
                  to help guide you in selecting credible information using web resources (shuffling
                  of handouts being passed around the room).&nbsp; Today's session, however, will focus exclusively
                  on information available through the library's resources. &nbsp;
               </p>
               
               <p><strong>PROFESSOR SAGE:</strong> &nbsp;They all have ideas of topics that they would like to do and they are in the process
                  of narrowing it down to a research question.&nbsp; They will need books, articles, and
                  websites as sources.
               </p>
               
               <p><strong>BOOKMAN</strong>: Understood.&nbsp; The first thing to mention is that all the electronic information in
                  the library is accessible through your Atlas account, and your student ID is your
                  library card that you will use to check out books and other items from the library.
               </p>
               
               <p><strong>MATT:</strong> (Raising his hand):&nbsp; I had a library instruction a few years ago before I joined
                  the military, and we had to have an ID and pin to access resources.&nbsp; Are you saying
                  now that we only need to log in to Atlas to access everything?
               </p>
               
               <p><strong>BOOKMAN:</strong> That is correct. There is no longer any need to remember several usernames and passwords
                  to access the library's materials. &nbsp;(several students nod appreciatively).&nbsp; Keep in
                  mind that all of the librarians are here to help, and sometimes the most effective
                  search strategy you can employ to start with is to ask a librarian.&nbsp; Librarians can
                  always be found at the Reference desk if you need immediate assistance and we are
                  also available through chat, email, and telephone.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Two_Fall.html#">return to top</a> | <a href="Module_Two_Fall2.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_two/module_two_fall.pcf">©</a>
      </div>
   </body>
</html>