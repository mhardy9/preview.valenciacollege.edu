<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Magazine Examples | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Magazine Examples</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="magazineexamples" id="magazineexamples"></a><h2>Magazine Examples</h2>
               
               <p>
                  <font color="#8000FF">*Make sure you complete the activity at the bottom of the page.*</font>
                  
               </p>
               
               <p><img align="left" hspace="30" width="300" longdesc="ada_files/ada_image2.html" class="resizable" height="400" border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/magazines2.jpg" vspace="10" alt="magazines2.jpg"> Magazine articles are written by journalists for a general audience. The articles
                  tend to be easy to read and relatively short. Sometimes magazine articles are found
                  in library subscription databases. Other times they may be found on free web sites,
                  as in the following examples.
               </p>
               
               <p>The elements needed to cite a magazine article are:</p>
               
               <ul>
                  
                  <li>author.</li>
                  
                  <li>date of publication.</li>
                  
                  <li>title of article.</li>
                  
                  <li>title of magazine.</li>
                  
                  <li>volume (if available, APA only).</li>
                  
                  <li>issue (if available, APA only).</li>
                  
                  <li>page numbers (if available).</li>
                  
                  <li>date of access (MLA only).</li>
                  
                  <li>database name (if a database is used for access; MLA only).</li>
                  
                  <li>URL (APA only).</li>
                  
               </ul>
               
               <p>APA:</p>
               
               <p>Fetini, A. (18 June 2009). Human trafficking rises in recession. <em>Time</em>. Retrieve
               </p>
               
               <p style="margin-left: 20.0px">from http://www.time.com/.</p>
               
               <p><a href="../../mla-apa-chicago-guides/apa-documentation-electronic.html" target="_blank">APA examples for library subscription databases.</a> Select the relevant database from the list.
               </p>
               
               <p>&nbsp;</p>
               
               <p>MLA:</p>
               
               <p>Fetini, Alyssa. "Human Trafficking Rises in Recession." <em>Time</em>. Time, 18 June 2009. Web. 7 July
               </p>
               
               <p style="margin-left: 20.0px">2010.</p>
               
               <p><a href="../../mla-apa-chicago-guides/mla-documentation-electronic.html" target="_blank">MLA examples for library subscription databases.</a> Select the relevant database from the list.
               </p>
               
               <p>
                  <strong>Click on Labeling Activity to complete the activity.</strong>
                  
               </p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/fullcitations/ada-activity.gif" title="Labeling Activity indicator" longdesc="ada_files/ada_activity5.html" border="0"><a id="y5_" href="javascript:parent.newWin('activityApplet5.html','win5','resizable=1,scrollbars=1,width=660,height=660')"><img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/labeling_custom.png" alt="Hyperlink to Labeling Activity"></a>&nbsp;
               </p>
               
               <p>
                  <a href="MagazineLabeling.pdf" target="_blank">Text version of labeling activity-PDF</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations4.html#">return to top</a> | <a href="fullcitations3.html">previous page</a> | <a href="fullcitations5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations4.pcf">©</a>
      </div>
   </body>
</html>