<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web Site Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Web Site Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="websiteactivity" id="websiteactivity"></a><h2>Web Site Activity</h2>
               
               <p>&nbsp;</p>
               
               <p>Connect to the web site listed below and see if you can find the author, title of
                  the page, title of the web site, publisher or sponsor and the date of publication.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <a href="http://auto.howstuffworks.com/car-driving-safety/accidents-hazardous-conditions/traffic.htm" target="_blank">http://auto.howstuffworks.com/car-driving-safety/accidents-hazardous-conditions/traffic.htm</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>Now put it all together and create a citation for the web page listed above in either
                  MLA or APA style.
               </p>
               
               <p>Italics will not work in the box; use an i at the beginning and end of a word or words
                  to indicate where italics should go.
               </p>
               
               <p>The tab key also will not work; use + at the beginning of a line to indicate that
                  it should be indented.
               </p>
               
               <p>Remember that you can go back to the <a href="fullcitations6.html#web">previous page</a> to see an example.
               </p>
               
               <p>Click on <a href="../../mla-apa-chicago-guides/apa-documentation-electronic.html">More APA web examples</a> or <a href="../../mla-apa-chicago-guides/mla-documentation-electronic.html">More MLA web examples</a> if the example on the previous page does not match the information you have.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/fullcitations/spacer.gif" name="check17" alt="" id="check17"><a id="a17" title="Question 1" href="javascript:parent.toggletable('quizpopper17')"><img src="http://valenciacollege.edu/library/tutorials/fullcitations/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 1" border="0"></a> 
               </p>
               
               <div id="quizpopper17" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #eeeeee; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f17" id="f17">Keep a copy of this citation; you will need it for an upcoming exercise.
                        <div style="margin: 1em 15px;"><textarea name="q17" rows="16" wrap="virtual" onfocus="parent.clearEssayFeedback(17)" style="width: 390px;"></textarea></div>
                        
                        <div align="center">
                           <input onclick="parent.check_q(17, 0, 8, true)" type="button" name="Check" value="Finish">&nbsp;&nbsp;&nbsp;<input onclick="parent.print_essay(17)" type="button" name="Print" value="Print">
                           
                        </div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done17" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed17" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations7.html#">return to top</a> | <a href="fullcitations6.html">previous page</a> | <a href="fullcitations8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations7.pcf">©</a>
      </div>
   </body>
</html>