<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Web Page Examples | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations6.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Web Page Examples</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="webpageexamples" id="webpageexamples"></a><h2>
                  <a id="bookmark" name="web"></a> Web Page Examples
               </h2>
               
               <p>Web sites are sometimes the most confusing sources to cite. Magazine, journal, and
                  newspaper articles and even books are all found on the web; in those cases, citations
                  will be similar to print versions of magazine, journals, newspapers and books. Sometimes,
                  however, the content on a web page does not fit into one of those categories. In cases
                  like these, you need:
               </p>
               
               <ul>
                  
                  <li>author (if available).</li>
                  
                  <li>title of the page.</li>
                  
                  <li>title of the web site.</li>
                  
                  <li>publisher or sponsor of the web site (if available).</li>
                  
                  <li>date of publication (if available).</li>
                  
                  <li>date of access.</li>
                  
               </ul>
               
               <p>APA:</p>
               
               <p>U.S. Department of State. (2010). <em>Trafficking in persons report 2010</em>. Retrieved from
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; http://www.state.gov/g/tip/rls/tiprpt/2010/index.htm</p>
               
               <p>
                  <a href="../../mla-apa-chicago-guides/apa-documentation-electronic.html" target="_blank">More APA web examples.</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>MLA:</p>
               
               <p>U.S. Department of State. <em>Trafficking in Persons Report 2010</em>. U.S. Department of State. June
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2010. Web. 7 July 2010.</p>
               
               <p>
                  <a href="../../mla-apa-chicago-guides/mla-documentation-electronic.html" target="_blank">More MLA web examples.</a>
                  
               </p>
               
               <p>Keep in mind that sources that provide more of this information are likely to be more
                  credible than those that do not, so if you are having trouble finding some of this
                  information you should carefully evaluate whether to use the source in your speech.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Places to look for this information include:</p>
               
               <ul>
                  
                  <li>the top of the page.</li>
                  
                  <li>the bottom of the page.</li>
                  
                  <li>the home page.</li>
                  
                  <li>links that say something like "About Us."</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>In particular, author is usually at the top or bottom of the page.</p>
               
               <p>Title of the web page can be found either on the top of the page or on the top toolbar.</p>
               
               <p>The title of the web site and the publisher or sponsor of the site can usually be
                  found by looking at the home page of the site, e.g. for http://www.state.gov/g/tip/rls/tiprpt/2010/index.htm,
                  cut off the rest of the URL and look at http://www.state.gov, or in "About Us" link.
               </p>
               
               <p>The date of publication, if there is one, is usually at the top or bottom of the page.</p>
               
               <p>The date of access is just the date you looked at the site.</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations6.html#">return to top</a> | <a href="fullcitations5.html">previous page</a> | <a href="fullcitations7.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations6.pcf">©</a>
      </div>
   </body>
</html>