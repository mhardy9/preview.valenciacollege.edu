<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Creating a Works Cited or References page | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations10.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Creating a Works Cited or References page</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="%C2%A0creatingaworkscitedorreferencespage" id="%C2%A0creatingaworkscitedorreferencespage"></a><h2>&nbsp;Creating a Works Cited or References page</h2>
               
               <p>Putting several full citation entries together to make a bibliography is called a
                  Works Cited page in MLA or a References page in APA.
               </p>
               
               <ul>
                  
                  <li>The page should have 1 inch margins at the top, bottom, left, and right.</li>
                  
                  <li>All text on the page should be double-spaced.</li>
                  
                  <li>Center Works Cited or References at the top of the page.</li>
                  
                  <li>The entries will appear in alphabetical order by the author's last name, or, if there
                     is no author, by the title of the work.
                  </li>
                  
                  <li>If the full citation requires more than one line of text, the second line (and any
                     following lines) will be indented. This is called a hanging indent. The paragraph
                     settings in Microsoft Word can be used to create a hanging indent or use the tab key.
                  </li>
                  
               </ul>
               
               <p>
                  <a href="References.pdf" target="_blank">APA sample References page - PDF</a>
                  
               </p>
               
               <p>
                  <a href="Works%20Cited.pdf" target="_blank">MLA sample Works Cited page - PDF</a>
                  
               </p>
               
               <p>Create a sample citation for the following (hint: <a href="fullcitations5.html#news">go back to page 5</a> to see an example):
               </p>
               
               <p>Minstries aim to close Jerusalem's Old City to traffic.</p>
               
               <p>Jerusalem Post, The (Israel) - Tuesday, April 29, 2008.</p>
               
               <p>Author: SHELLY PAZ.</p>
               
               <p>The Tourism and Transportation ministries are working toward closing Jerusalem's Old
                  City to transportation, both private and public.
               </p>
               
               <p>(The rest of the article would appear next, but instead we will skip to the bottom.)</p>
               
               <p>Edition: Daily.</p>
               
               <p>Section: News.</p>
               
               <p>Page: 05.</p>
               
               <p>Record Number: 120677CF1956B990.</p>
               
               <p>Copyright, 2008, The Jerusalem Post, All Rights Reserved.</p>
               
               <p>
                  <a href="http://auto.howstuffworks.com/car-driving-safety/accidents-hazardous-conditions/traffic.htm" target="_blank"></a>
                  
               </p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/fullcitations/spacer.gif" name="check18" alt="" id="check18"><a id="a18" title="Question 3" href="javascript:parent.toggletable('quizpopper18')"><img src="http://valenciacollege.edu/library/tutorials/fullcitations/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 3" border="0"></a> 
               </p>
               
               <div id="quizpopper18" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #eeeeee; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f18" id="f18">Create a sample Works Cited or References page with two entries, the web page citation
                        you previously created and the citation you just created for the sample article above.
                        Use i for italics, + for indenting, and C for centering.
                        <div style="margin: 1em 15px;"><textarea name="q18" rows="16" wrap="virtual" onfocus="parent.clearEssayFeedback(18)" style="width: 390px;"></textarea></div>
                        
                        <div align="center">
                           <input onclick="parent.check_q(18, 0, 8, true)" type="button" name="Check" value="Finish">&nbsp;&nbsp;&nbsp;<input onclick="parent.print_essay(18)" type="button" name="Print" value="Print">
                           
                        </div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done18" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed18" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations10.html#">return to top</a> | <a href="fullcitations9.html">previous page</a> | <a href="fullcitations11.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations10.pcf">©</a>
      </div>
   </body>
</html>