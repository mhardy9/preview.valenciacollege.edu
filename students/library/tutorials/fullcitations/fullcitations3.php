<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Book Examples | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Book Examples</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="bookexamples" id="bookexamples"></a><h2>Book Examples</h2>
               
               <p>
                  <font color="#8000FF">*Make sure you complete the activity at the bottom of the page.*&nbsp;<img align="left" hspace="30" width="300" height="400" class="resizable" longdesc="ada_files/ada_image1.html" border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/books1.jpg" vspace="10" alt="books1.jpg"></font>
                  
               </p>
               
               <p>APA:</p>
               
               <p>Bales, K. &amp; Soodalter, R. (2009). <em>The slave next door: Human trafficking and slavery in</em></p>
               
               <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; America today</em>. Berkeley: University of California Press
               </p>
               
               <p>MLA:</p>
               
               <p>Bales, Kevin and Rod Soodalter. <em>The Slave Next Door: Human Trafficking and Slavery in</em></p>
               
               <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; America Today</em>. Berkeley: U of California P, 2009. Print.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Both examples include:</p>
               
               <ul>
                  
                  <li>the authors' names.</li>
                  
                  <li>the title of the book.</li>
                  
                  <li>the place of publication.</li>
                  
                  <li>the publisher.</li>
                  
                  <li>the date.</li>
                  
               </ul>
               
               <p>This information is just in a different order. Both also use double-spacing and a
                  hanging indentation (the second line and all the following lines are indented), but
                  other details of the formatting are a little different.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <a href="http://www.valenciacollege.edu/library/doc_apa_print.cfm" target="_blank"></a>
                  
               </p>
               
               <p>
                  <a href="http://www.valenciacollege.edu/library/doc_apa_print.cfm" target="_blank">APA (For additional information see http://www.valenciacollege.edu/library/doc_apa_print.cfm).</a>
                  
               </p>
               
               <ul>
                  
                  <li>uses initials for author's first names.</li>
                  
                  <li>places the date second and in parenthesis.</li>
                  
                  <li>capitalizes the first letter of the first word of the title and subtitle (e.g. The;
                     Human).
                  </li>
                  
               </ul>
               
               <p>
                  <a href="http://www.valenciacollege.edu/library/doc_mla_print.cfm" target="_blank">MLA (For additional information see http://www.valenciacollege.edu/library/doc_mla_print.cfm).</a>
                  
               </p>
               
               <ul>
                  
                  <li>uses the author's full names.</li>
                  
                  <li>capitalizes the first letter of all the important words.</li>
                  
                  <li>uses abbreviations for university (U) and press (P).</li>
                  
                  <li>places the date near the end.</li>
                  
                  <li>adds the medium of publication, e.g. print or web.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Click on Labeling Activity to complete the activity.</strong>
                  
               </p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/fullcitations/ada-activity.gif" title="Labeling Activity indicator" longdesc="ada_files/ada_activity10.html" border="0"><a id="y10_" href="javascript:parent.newWin('activityApplet10.html','win10','resizable=1,scrollbars=1,width=660,height=660')"><img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/labeling_custom.png" alt="Hyperlink to Labeling Activity"></a>&nbsp;
               </p>
               
               <p>&nbsp;<a href="BookLabelingText.pdf" target="_blank">Text version of labeling activity-PDF</a></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations3.html#">return to top</a> | <a href="fullcitations2.html">previous page</a> | <a href="fullcitations4.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations3.pcf">©</a>
      </div>
   </body>
</html>