<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Which is Which? | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations9.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Which is Which?</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="whichiswhich?" id="whichiswhich?"></a><h2>Which is Which?</h2>
               
               <p>Look at the following list of sources in the APA style (with numbers added), observe
                  the different elements used among the citations,
               </p>
               
               <p>and then answer the questions below.</p>
               
               <p>&nbsp;</p>
               
               <p align="center">References</p>
               
               <p>1. Bell, K. (2010, March). 5 surprising signs you're sleep deprived. <em>Prevention, 62</em> (3), 59-62.
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Retrieved from http://www.prevention.com/health</p>
               
               <p>2. Brody, J. (1999, December 28). Personal health: Paying the price for cheating on
                  sleep. <em>New</em></p>
               
               <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; York Times</em>, p. 7. Retrieved from http://www.nytimes.com
               </p>
               
               <p>3. Epstein, L. J. (2010, June 28). The surprising toll of sleep deprivation. <em>Newsweek</em> &nbsp;<em>155/156,</em></p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (26/1), 75. Retrieved from http://www.newsweek.com</p>
               
               <p>4. National Heart, Lung and Blood Institute. (2006, April). <em>In brief: Your guide to healthy sleep</em>.
               </p>
               
               <p style="margin-left: 40.0px">Retrieved July 12, 2010, from</p>
               
               <p style="margin-left: 40.0px">http://www.nhlbi.nih.gov/health/public/sleep/healthysleepfs.pdf</p>
               
               <p>5. National Sleep Foundation. (2009). <em>How much sleep do we really need?</em> Retrieved July 12, 2010,
               </p>
               
               <p style="margin-left: 40.0px">from http://www.sleepfoundation.org/article/how-sleep-works/how-much-sleep-do-we-</p>
               
               <p style="margin-left: 40.0px">really-need</p>
               
               <p>6. Schenck, C. (2007). <em>Sleep: The mysteries, the problems and the solutions</em>. New York: Avery.
               </p>
               
               <p>7. Swanson, J. (1999). <em>Sleep disorders sourcebook</em>. Detroit: Omnigraphics.
               </p>
               
               <p>8. Turner, E., Jr. (2010, March 10). Sleep deprivation a serious issue for students.
                  <em>Hilltop</em>. Retrieved
               </p>
               
               <p style="margin-left: 40.0px">July 12, 2010 from http://www.thehilltoponline.com</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<a title="Which is Which" href="javascript:parent.toggleQuizGoup(1,%20false,%20false,%20false)"><img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/quizgroup_custom.png" id="quizgroup1" title="Which is Which" alt="Toggle open/close quiz group"></a>
                  
                  
               </p>
               
               <div id="quizgroupwrapper1" style="display:none;padding-left:20px;border:1px solid #000000;">
                  
                  <div id="quizgroupspace1"></div>
                  
                  <div id="quizgroupfeedback1"></div>
                  
                  <p>
                     <input type="button" value="&amp;lt;  Prev" onclick="parent.writePrevGroupQP(1, false)" style="display:none;" id="qgbackbottom1">&nbsp;
                     <input type="button" value="Next  &amp;gt;" onclick="parent.writeNextGroupQP(1, false, false)" style="display:inline;" id="qgforwardbottom1">
                     <br><br>
                     <input type="button" value="Check Answers" onclick="parent.checkQuizGroup(1, false, false, false)" style="display:none;" id="quizgroupresults1">
                     
                  </p>
                  
               </div>
               
               
               
               
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations9.html#">return to top</a> | <a href="fullcitations8.html">previous page</a> | <a href="fullcitations10.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations9.pcf">©</a>
      </div>
   </body>
</html>