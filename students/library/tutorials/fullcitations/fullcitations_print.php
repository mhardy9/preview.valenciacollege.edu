<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Full Citations: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/fullcitations/fullcitations_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/fullcitations/">Fullcitations</a></li>
               <li>Full Citations:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Full Citations:</strong><br>To Cite or Not to Cite? Part 3 of 5
               </p>
               
               
               <h2>Full Citations</h2>
               
               <p><strong>Outcome:</strong> You will understand the parts of a full citation and be able to create one.
               </p>
               
               <p><strong>Approximate Completion Time</strong>: 40 minutes.
               </p>
               
               <p>
                  <strong>
                     <font color="#8000FF">**To receive credit for completing this portion of the tutorial you must fill out
                        the form on page 11.**</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Citation Formats</h2>
               
               <p>&nbsp;At Valencia there are two commonly used formats for creating citations: APA and MLA.
                  APA stands for American Psychological Association. This format emphasizes the currency
                  of the information, so the date is more prominently displayed. APA style is commonly
                  used in psychology classes and may also be used in nursing, business, economics, and
                  other science and social science disciplines. MLA is the Modern Language Association
                  style, most often required in English and Humanities classes.
               </p>
               
               <p>Both styles use in-text citations and full citations. The full citations shown in
                  this section would be incorporated in a complete list of the sources used in a speech
                  (or paper).
               </p>
               
               <p>The two styles use most of the same basic components in a citation; they just arrange
                  and display them differently.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Book Examples</h2>
               
               <p>
                  <font color="#8000FF">*Make sure you complete the activity at the bottom of the page.*&nbsp;<img align="left" hspace="30" width="300" height="400" class="resizable" longdesc="Photograph of books on libraray shelves." border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/books1.jpg" vspace="10" alt="books1.jpg"></font>
                  
               </p>
               
               <p>APA:</p>
               
               <p>Bales, K. &amp; Soodalter, R. (2009). <em>The slave next door: Human trafficking and slavery in</em></p>
               
               <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; America today</em>. Berkeley: University of California Press
               </p>
               
               <p>MLA:</p>
               
               <p>Bales, Kevin and Rod Soodalter. <em>The Slave Next Door: Human Trafficking and Slavery in</em></p>
               
               <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; America Today</em>. Berkeley: U of California P, 2009. Print.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Both examples include:</p>
               
               <ul>
                  
                  <li>the authors' names.</li>
                  
                  <li>the title of the book.</li>
                  
                  <li>the place of publication.</li>
                  
                  <li>the publisher.</li>
                  
                  <li>the date.</li>
                  
               </ul>
               
               <p>This information is just in a different order. Both also use double-spacing and a
                  hanging indentation (the second line and all the following lines are indented), but
                  other details of the formatting are a little different.
               </p>
               
               <p>&nbsp;</p>
               
               <p></p>
               
               <p>APA (For additional information see http://www.valenciacollege.edu/library/doc_apa_print.cfm).</p>
               
               <ul>
                  
                  <li>uses initials for author's first names.</li>
                  
                  <li>places the date second and in parenthesis.</li>
                  
                  <li>capitalizes the first letter of the first word of the title and subtitle (e.g. The;
                     Human).
                  </li>
                  
               </ul>
               
               <p>MLA (For additional information see http://www.valenciacollege.edu/library/doc_mla_print.cfm).</p>
               
               <ul>
                  
                  <li>uses the author's full names.</li>
                  
                  <li>capitalizes the first letter of all the important words.</li>
                  
                  <li>uses abbreviations for university (U) and press (P).</li>
                  
                  <li>places the date near the end.</li>
                  
                  <li>adds the medium of publication, e.g. print or web.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Click on Labeling Activity to complete the activity.</strong>
                  
               </p>
               
               <p>&nbsp;<img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/labeling_custom.png" alt="Hyperlink to Labeling Activity">&nbsp;
               </p>
               
               <p>&nbsp;Text version of labeling activity-PDF</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Magazine Examples</h2>
               
               <p>
                  <font color="#8000FF">*Make sure you complete the activity at the bottom of the page.*</font>
                  
               </p>
               
               <p><img align="left" hspace="30" width="300" longdesc="Photograph of magazines on library shelves" class="resizable" height="400" border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/magazines2.jpg" vspace="10" alt="magazines2.jpg"> Magazine articles are written by journalists for a general audience. The articles
                  tend to be easy to read and relatively short. Sometimes magazine articles are found
                  in library subscription databases. Other times they may be found on free web sites,
                  as in the following examples.
               </p>
               
               <p>The elements needed to cite a magazine article are:</p>
               
               <ul>
                  
                  <li>author.</li>
                  
                  <li>date of publication.</li>
                  
                  <li>title of article.</li>
                  
                  <li>title of magazine.</li>
                  
                  <li>volume (if available, APA only).</li>
                  
                  <li>issue (if available, APA only).</li>
                  
                  <li>page numbers (if available).</li>
                  
                  <li>date of access (MLA only).</li>
                  
                  <li>database name (if a database is used for access; MLA only).</li>
                  
                  <li>URL (APA only).</li>
                  
               </ul>
               
               <p>APA:</p>
               
               <p>Fetini, A. (18 June 2009). Human trafficking rises in recession. <em>Time</em>. Retrieve
               </p>
               
               <p style="margin-left: 20.0">from http://www.time.com/.</p>
               
               <p>APA examples for library subscription databases. Select the relevant database from
                  the list.
               </p>
               
               <p>&nbsp;</p>
               
               <p>MLA:</p>
               
               <p>Fetini, Alyssa. "Human Trafficking Rises in Recession." <em>Time</em>. Time, 18 June 2009. Web. 7 July
               </p>
               
               <p style="margin-left: 20.0">2010.</p>
               
               <p>MLA examples for library subscription databases. Select the relevant database from
                  the list.
               </p>
               
               <p>
                  <strong>Click on Labeling Activity to complete the activity.</strong>
                  
               </p>
               
               <p>&nbsp;<img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/labeling_custom.png" alt="Hyperlink to Labeling Activity">&nbsp;
               </p>
               
               <p>Text version of labeling activity-PDF</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> Newspaper Examples</h2>
               
               <p>
                  <font color="#8000FF">*Make sure you complete the activity at the bottom of the page.*</font>
                  
               </p>
               
               <p><img align="left" hspace="30" width="300" longdesc="Photograph of newspapers on library shelves" class="resizable" height="400" border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/newspapers.jpg" vspace="10" alt="newspapers.jpg"> To cite a newspaper article you need:
               </p>
               
               <ul>
                  
                  <li>author (if available).</li>
                  
                  <li>title of article.</li>
                  
                  <li>name of newspaper.</li>
                  
                  <li>date of publication.</li>
                  
                  <li>access date.</li>
                  
                  <li>page number.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;APA :</p>
               
               <p>Mizell, V. (8 October 2009). Working to shed light on very dark practices – Activists
                  seek to end
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; human trafficking in D.C. <em>Washington Post</em>. Retrieved from http://www.washingtonpost.com
               </p>
               
               <p>More APA examples.</p>
               
               <p>MLA:</p>
               
               <p>Mizell, Vanessa. "Working to Shed Light on Very Dark Practices – Activists Seek to
                  End
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Human Trafficking in D.C." <em>Washington Post</em> 8 Oct. 2009: T3. <em>Newsbank</em>. Web. 7 July
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2010.</p>
               
               <p>More MLA examples.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Click on Labeling Activity to complete the activity.</strong>
                  
               </p>
               
               <p>&nbsp;<img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/labeling_custom.png" alt="Hyperlink to Labeling Activity">&nbsp;
               </p>
               
               <p>&nbsp;Text version of labeling activity-PDF</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> Web Page Examples</h2>
               
               <p>Web sites are sometimes the most confusing sources to cite. Magazine, journal, and
                  newspaper articles and even books are all found on the web; in those cases, citations
                  will be similar to print versions of magazine, journals, newspapers and books. Sometimes,
                  however, the content on a web page does not fit into one of those categories. In cases
                  like these, you need:
               </p>
               
               <ul>
                  
                  <li>author (if available).</li>
                  
                  <li>title of the page.</li>
                  
                  <li>title of the web site.</li>
                  
                  <li>publisher or sponsor of the web site (if available).</li>
                  
                  <li>date of publication (if available).</li>
                  
                  <li>date of access.</li>
                  
               </ul>
               
               <p>APA:</p>
               
               <p>U.S. Department of State. (2010). <em>Trafficking in persons report 2010</em>. Retrieved from
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; http://www.state.gov/g/tip/rls/tiprpt/2010/index.htm</p>
               
               <p>More APA web examples.</p>
               
               <p>&nbsp;</p>
               
               <p>MLA:</p>
               
               <p>U.S. Department of State. <em>Trafficking in Persons Report 2010</em>. U.S. Department of State. June
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 2010. Web. 7 July 2010.</p>
               
               <p>More MLA web examples.</p>
               
               <p>Keep in mind that sources that provide more of this information are likely to be more
                  credible than those that do not, so if you are having trouble finding some of this
                  information you should carefully evaluate whether to use the source in your speech.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Places to look for this information include:</p>
               
               <ul>
                  
                  <li>the top of the page.</li>
                  
                  <li>the bottom of the page.</li>
                  
                  <li>the home page.</li>
                  
                  <li>links that say something like "About Us."</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>In particular, author is usually at the top or bottom of the page.</p>
               
               <p>Title of the web page can be found either on the top of the page or on the top toolbar.</p>
               
               <p>The title of the web site and the publisher or sponsor of the site can usually be
                  found by looking at the home page of the site, e.g. for http://www.state.gov/g/tip/rls/tiprpt/2010/index.htm,
                  cut off the rest of the URL and look at http://www.state.gov, or in "About Us" link.
               </p>
               
               <p>The date of publication, if there is one, is usually at the top or bottom of the page.</p>
               
               <p>The date of access is just the date you looked at the site.</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Web Site Activity</h2>
               
               <p>&nbsp;</p>
               
               <p>Connect to the web site listed below and see if you can find the author, title of
                  the page, title of the web site, publisher or sponsor and the date of publication.
               </p>
               
               <p>&nbsp;</p>
               
               <p>http://auto.howstuffworks.com/car-driving-safety/accidents-hazardous-conditions/traffic.htm</p>
               
               <p>&nbsp;</p>
               
               <p>Now put it all together and create a citation for the web page listed above in either
                  MLA or APA style.
               </p>
               
               <p>Italics will not work in the box; use an i at the beginning and end of a word or words
                  to indicate where italics should go.
               </p>
               
               <p>The tab key also will not work; use + at the beginning of a line to indicate that
                  it should be indented.
               </p>
               
               <p>Remember that you can go back to the previous page to see an example.</p>
               
               <p>Click on More APA web examples or More MLA web examples if the example on the previous
                  page does not match the information you have.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/quizme_custom.png" title="Question 1" alt="Toggle open/close quiz question"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Differences Among Types of Sources</h2>
               
               <p>When you look at a citation you should be able to tell what kind of source it represents.</p>
               
               <p>Books will always have a year of publication instead of a specific month, day, or
                  season and will also include a place of publication.
               </p>
               
               <p>Magazines, journals and newspapers all have more specific dates and will have an article
                  and a publication title.
               </p>
               
               <p>Journal articles tend to have more scholarly titles, both of the articles themselves
                  and of the publication, often including the word "journal" in name of the publication.
                  In MLA style, volume and issue are included with journals, but not with magazines
                  or newspapers.
               </p>
               
               <p>Magazines and newspapers tend to have more catchy titles. Newspapers are usually published
                  daily and often include the name of the city in the newspaper title. Magazines are
                  usually published weekly or monthly.
               </p>
               
               <p>Web pages may also be magazine, newspaper, or journal articles or even books, or sometimes
                  they are original content not included as part of one of the other types. In APA style
                  the URL for the web site is included. In MLA style, web is always mentioned as the
                  medium.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Which is Which?</h2>
               
               <p>Look at the following list of sources in the APA style (with numbers added), observe
                  the different elements used among the citations,
               </p>
               
               <p>and then answer the questions below.</p>
               
               <p>&nbsp;</p>
               
               <p align="center">References</p>
               
               <p>1. Bell, K. (2010, March). 5 surprising signs you're sleep deprived. <em>Prevention, 62</em> (3), 59-62.
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Retrieved from http://www.prevention.com/health</p>
               
               <p>2. Brody, J. (1999, December 28). Personal health: Paying the price for cheating on
                  sleep. <em>New</em></p>
               
               <p><em>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; York Times</em>, p. 7. Retrieved from http://www.nytimes.com
               </p>
               
               <p>3. Epstein, L. J. (2010, June 28). The surprising toll of sleep deprivation. <em>Newsweek</em> &nbsp;<em>155/156,</em></p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (26/1), 75. Retrieved from http://www.newsweek.com</p>
               
               <p>4. National Heart, Lung and Blood Institute. (2006, April). <em>In brief: Your guide to healthy sleep</em>.
               </p>
               
               <p style="margin-left: 40.0">Retrieved July 12, 2010, from</p>
               
               <p style="margin-left: 40.0">http://www.nhlbi.nih.gov/health/public/sleep/healthysleepfs.pdf</p>
               
               <p>5. National Sleep Foundation. (2009). <em>How much sleep do we really need?</em> Retrieved July 12, 2010,
               </p>
               
               <p style="margin-left: 40.0">from http://www.sleepfoundation.org/article/how-sleep-works/how-much-sleep-do-we-</p>
               
               <p style="margin-left: 40.0">really-need</p>
               
               <p>6. Schenck, C. (2007). <em>Sleep: The mysteries, the problems and the solutions</em>. New York: Avery.
               </p>
               
               <p>7. Swanson, J. (1999). <em>Sleep disorders sourcebook</em>. Detroit: Omnigraphics.
               </p>
               
               <p>8. Turner, E., Jr. (2010, March 10). Sleep deprivation a serious issue for students.
                  <em>Hilltop</em>. Retrieved
               </p>
               
               <p style="margin-left: 40.0">July 12, 2010 from http://www.thehilltoponline.com</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/quizgroup_custom.png" id="quizgroup1" title="Which is Which" alt="Toggle open/close quiz group"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>&nbsp;Creating a Works Cited or References page</h2>
               
               <p>Putting several full citation entries together to make a bibliography is called a
                  Works Cited page in MLA or a References page in APA.
               </p>
               
               <ul>
                  
                  <li>The page should have 1 inch margins at the top, bottom, left, and right.</li>
                  
                  <li>All text on the page should be double-spaced.</li>
                  
                  <li>Center Works Cited or References at the top of the page.</li>
                  
                  <li>The entries will appear in alphabetical order by the author's last name, or, if there
                     is no author, by the title of the work.
                  </li>
                  
                  <li>If the full citation requires more than one line of text, the second line (and any
                     following lines) will be indented. This is called a hanging indent. The paragraph
                     settings in Microsoft Word can be used to create a hanging indent or use the tab key.
                  </li>
                  
               </ul>
               
               <p>APA sample References page - PDF</p>
               
               <p>MLA sample Works Cited page - PDF</p>
               
               <p>Create a sample citation for the following (hint: go back to page 5 to see an example):</p>
               
               <p>Minstries aim to close Jerusalem's Old City to traffic.</p>
               
               <p>Jerusalem Post, The (Israel) - Tuesday, April 29, 2008.</p>
               
               <p>Author: SHELLY PAZ.</p>
               
               <p>The Tourism and Transportation ministries are working toward closing Jerusalem's Old
                  City to transportation, both private and public.
               </p>
               
               <p>(The rest of the article would appear next, but instead we will skip to the bottom.)</p>
               
               <p>Edition: Daily.</p>
               
               <p>Section: News.</p>
               
               <p>Page: 05.</p>
               
               <p>Record Number: 120677CF1956B990.</p>
               
               <p>Copyright, 2008, The Jerusalem Post, All Rights Reserved.</p>
               
               <p></p>
               
               <p>&nbsp;<img border="0" src="http://valenciacollege.edu/library/tutorials/fullcitations/quizme_custom.png" title="Question 3" alt="Toggle open/close quiz question"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Print Your Score</h2>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/fullcitations/fullcitations_print.pcf">©</a>
      </div>
   </body>
</html>