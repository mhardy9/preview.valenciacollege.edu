<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Print Book Citations | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Print Book Citations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="printbookcitations" id="printbookcitations"></a><h2>Print Book Citations</h2>
               
               <p>
                  <font color="#8000FF">&nbsp;</font>
                  <img width="300" alt="Library books" class="resizable" hspace="30" metadata="obj11" border="0" height="400" longdesc="ada_files/ada_image1.html" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/books1.jpg" vspace="10" align="right">
                  
               </p>
               
               <p>The elements needed to cite a print book are:</p>
               
               <ol>
                  
                  <li><span style="background-color: #00ffff">author(s)</span></li>
                  
                  <li><span style="background-color: #ffff66">date of publication</span></li>
                  
                  <li><span style="background-color: #ff00ff">title of the book</span></li>
                  
                  <li><span style="background-color: #ff8000">place of publication</span></li>
                  
                  <li><span style="background-color: #0aa0f7">publisher</span></li>
                  
               </ol>
               
               <p>The following is an APA full citation for a print book:</p>
               
               <p><span style="background-color: #00ffff">Huang, H. K.</span><span style="background-color: #ffff66">(2010).</span><em><span style="background-color: #ff00ff">PACS and imaging informatics: Basic principles and applications</span></em>. <span style="background-color: #ff8000">Hoboken:</span><span style="background-color: #0aa0f7">Wiley-Blackwell.</span></p>
               
               <p>This example includes:</p>
               
               <ol>
                  
                  <li>the author's name --› Huang, H. K.</li>
                  
                  <li>the date --› (2010).</li>
                  
                  <li>the title of the book --› <em>PACS and imaging informatics : Basic principles and applications</em>.
                  </li>
                  
                  <li>the place of publication --› Hoboken:</li>
                  
                  <li>the publisher --› Wiley-Blackwell.</li>
                  
               </ol>
               
               <p>APA citations should be double-spaced and have a hanging indent (the second line and
                  all the following lines are indented).
               </p>
               
               <p>The APA format</p>
               
               <ul>
                  
                  <li>uses initials for author's first and middle names (e.g. Huang, <span style="background-color: #ffff66">H. K.</span>)
                  </li>
                  
                  <li>places the date second and in parenthesis</li>
                  
                  <li>capitalizes the first letter of the first word of the title and subtitle (e.g. <em><span style="background-color: #ffff66">P</span>ACS and imaging informatics: <span style="background-color: #ffff66">B</span>asic principles and applications</em>.)
                  </li>
                  
               </ul>
               
               <p>&nbsp;Valencia Library has created color coded APA cheat sheets and made them available
                  online. Review our <a target="_blank" href="../../mla-apa-chicago-guides/apa-documentation-print.html">APA examples for books</a>.&nbsp;
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations3.html#">return to top</a> | <a href="fullcitations2.html">previous page</a> | <a href="fullcitations4.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations3.pcf">©</a>
      </div>
   </body>
</html>