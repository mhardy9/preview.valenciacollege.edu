<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Creating a Reference List | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Creating a Reference List</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="creatingareferencelist" id="creatingareferencelist"></a><h2>Creating a Reference List</h2>
               
               <p>Putting several full citation entries together to make a bibliography is called a
                  Reference List in APA.
               </p>
               
               <ul>
                  
                  <li>The page should have 1 inch margins at the top, bottom, left, and right.</li>
                  
                  <li>All text on the page should be double-spaced.</li>
                  
                  <li>The word References should be centered at the top of the page.</li>
                  
                  <li>The entries should appear in alphabetical order by the author's last name, or, if
                     there is no author, by the title of the work.
                  </li>
                  
                  <li>If the full citation requires more than one line of text, the second line (and any
                     following lines) should be indented. This is called a hanging indent.
                  </li>
                  
               </ul>
               
               <p>
                  <img width="800" alt="references.jpg" class="resizable" hspace="30" metadata="obj18" height="455" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/references.jpg" vspace="10">
                  
               </p>
               
               <p><em>Figure 1.</em> Image of APA References page. Reprinted from Purdue Online Writing Lab. (2012). <em>APA sample paper</em>. Retrieved from http://owl.english.purdue.edu/owl/resource/560/18/
               </p>
               
               <p>&nbsp;</p>
               
               <p>You can also visit The Owl at Purdue's website to view a properly formatted researcher
                  paper and references page: <a target="_blank" href="http://owl.english.purdue.edu/owl/resource/560/18/">APA sample paper and references page - PDF</a>&nbsp;
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations11.html#">return to top</a> | <a href="fullcitations10.html">previous page</a> | <a href="fullcitations12.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations11.pcf">©</a>
      </div>
   </body>
</html>