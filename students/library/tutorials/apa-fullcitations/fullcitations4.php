<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 4 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Page 4</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Complete the activity below. Put the APA book citation in the correct order. You can
                     review our <a target="_blank" href="../../mla-apa-chicago-guides/apa-documentation-print.html">APA examples for books</a> to help you.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/spacer.gif" name="check22" alt="" id="check22"><a id="a22" href="javascript:toggletable('quizpopper22')" title="Question 1"><img src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" alt="Toggle open/close quiz question" title="Question 1" border="0"></a> 
               </p>
               
               <div id="quizpopper22" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #DBA917; background: #fbf1d2; line-height: 1.5em; padding: 10px 15px;">
                     
                     <form name="f22" id="f22">Put the items in the correct order.
                        <div class="hide_stuff">Below is a sequence of events. Place them in the order they should occur, number 1
                           being the first item. Select the step number from the drop down next to each item.
                        </div>
                        
                        <div style="margin: 1em 15px;">
                           
                           <div class="hide_stuff">Items to order:<br>1.&nbsp;<em>Why we drive the way we do (and what it says about us).</em><br>2.&nbsp;Vanderbilt, T.<br>3.&nbsp;(2008).<br>4.&nbsp;Knopf.<br>5.&nbsp;New York:<br>
                              
                           </div>
                           
                           <table class="table ">
                              
                              <tr valign="top">
                                 
                                 <td><label for="q22_1"><em>Why we drive the way we do (and what it says about us).</em></label></td>
                                 
                                 <td width="1%"><select name="q22_1" id="q22_1">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option>
                                       
                                       <option>5&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q22_2">Vanderbilt, T.</label></td>
                                 
                                 <td width="1%"><select name="q22_2" id="q22_2">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option>
                                       
                                       <option>5&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q22_3">(2008).</label></td>
                                 
                                 <td width="1%"><select name="q22_3" id="q22_3">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option>
                                       
                                       <option>5&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q22_4">Knopf.</label></td>
                                 
                                 <td width="1%"><select name="q22_4" id="q22_4">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option>
                                       
                                       <option>5&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q22_5">New York:</label></td>
                                 
                                 <td width="1%"><select name="q22_5" id="q22_5">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option>
                                       
                                       <option>5&nbsp;</option></select></td>
                                 
                              </tr>
                              
                           </table>
                           
                        </div>
                        
                        <div align="center"><input onclick="check_q(22, 5, 6, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done22" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed22" style="font-family: Comic Sans MS; border-top: 1px solid #DBA917; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               &nbsp;&nbsp;
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations4.html#">return to top</a> | <a href="fullcitations3.html">previous page</a> | <a href="fullcitations5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations4.pcf">©</a>
      </div>
   </body>
</html>