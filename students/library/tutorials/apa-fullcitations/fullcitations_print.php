<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Full Citations: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Full Citations:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Full Citations:</strong><br>To Cite or Not to Cite? Part 3 of 5
               </p>
               
               
               <h2>Full Citations</h2>
               
               <p>Learning Outcome: By the end of the lesson, the student will be able to</p>
               
               <ul>
                  
                  <li>identify the parts of standard APA citations</li>
                  
                  <li>develop citations for books, journals, magazines, newspapers and websites</li>
                  
                  <li>create a Reference List</li>
                  
               </ul>
               
               <p><strong>Approximate Completion Time</strong>: 30 minutes.
               </p>
               
               <p>
                  <font color="#ff0000">
                     <strong>**To receive credit for completing this tutorial you must complete the online assessment
                        provided by your instructor.**</strong>
                     </font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p></p>
               
               <p align="left">This tutorial works best in Mozilla Firefox.</p>
               
               <p>It cannot save your place, so please take note of the approximate completion time
                  above and either work on it when you have time to finish or keep track of where you
                  were.
               </p>
               
               <p>&nbsp;</p>
               
               <hr>
               
               <p>
                  <strong>If you encounter technical problems, please copy the error message and send it with
                     a description of the problem to lking@valenciacollege.edu</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Citation Formats</h2>
               
               <p>At Valencia there are two commonly used formats for creating citations: APA and MLA.
                  APA stands for American Psychological Association. This format emphasizes the currency
                  of the information, so the date is more prominently displayed. APA style is commonly
                  used in nursing, allied health science and psychology courses. MLA is the Modern Language
                  Association style, most often required in English and Humanities classes. Only the
                  APA style will be covered in this tutorial.
               </p>
               
               <p>Both styles use in-text citations and full citations. The full citations shown in
                  this tutorial would be incorporated into a complete list of the sources used in a
                  paper (called References).
               </p>
               
               <p>The two styles use most of the same basic components in a citation; they just arrange
                  and display them differently.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Print Book Citations</h2>
               
               <p>
                  <font color="#8000FF">&nbsp;</font>
                  <img width="300" alt="Library books" class="resizable" hspace="30" metadata="obj11" border="0" height="400" longdesc="Photograph of books on library shelves." src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/books1.jpg" vspace="10" align="right">
                  
               </p>
               
               <p>The elements needed to cite a print book are:</p>
               
               <ol>
                  
                  <li><span style="background-color: #00ffff">author(s)</span></li>
                  
                  <li><span style="background-color: #ffff66">date of publication</span></li>
                  
                  <li><span style="background-color: #ff00ff">title of the book</span></li>
                  
                  <li><span style="background-color: #ff8000">place of publication</span></li>
                  
                  <li><span style="background-color: #0aa0f7">publisher</span></li>
                  
               </ol>
               
               <p>The following is an APA full citation for a print book:</p>
               
               <p><span style="background-color: #00ffff">Huang, H. K.</span><span style="background-color: #ffff66">(2010).</span><em><span style="background-color: #ff00ff">PACS and imaging informatics: Basic principles and applications</span></em>. <span style="background-color: #ff8000">Hoboken:</span><span style="background-color: #0aa0f7">Wiley-Blackwell.</span></p>
               
               <p>This example includes:</p>
               
               <ol>
                  
                  <li>the author's name --› Huang, H. K.</li>
                  
                  <li>the date --› (2010).</li>
                  
                  <li>the title of the book --› <em>PACS and imaging informatics : Basic principles and applications</em>.
                  </li>
                  
                  <li>the place of publication --› Hoboken:</li>
                  
                  <li>the publisher --› Wiley-Blackwell.</li>
                  
               </ol>
               
               <p>APA citations should be double-spaced and have a hanging indent (the second line and
                  all the following lines are indented).
               </p>
               
               <p>The APA format</p>
               
               <ul>
                  
                  <li>uses initials for author's first and middle names (e.g. Huang, <span style="background-color: #ffff66">H. K.</span>)
                  </li>
                  
                  <li>places the date second and in parenthesis</li>
                  
                  <li>capitalizes the first letter of the first word of the title and subtitle (e.g. <em><span style="background-color: #ffff66">P</span>ACS and imaging informatics: <span style="background-color: #ffff66">B</span>asic principles and applications</em>.)
                  </li>
                  
               </ul>
               
               <p>&nbsp;Valencia Library has created color coded APA cheat sheets and made them available
                  online. Review our APA examples for books.&nbsp;
               </p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Complete the activity below. Put the APA book citation in the correct order. You can
                     review our APA examples for books to help you.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" title="Question 1">&nbsp;&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Journal and Magazine Citations</h2>
               
               <p><img width="300" alt="Image of Magazines" class="resizable" hspace="30" metadata="obj12" border="0" height="400" longdesc="Photograph of magazines on library shelves" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/magazines2.jpg" vspace="10" align="right"> The elements needed to cite a journal or magazine article are:
               </p>
               
               <ol>
                  
                  <li><span style="background-color: #ffff66">author</span></li>
                  
                  <li><span style="background-color: #00ff00">date of publication</span></li>
                  
                  <li><span style="background-color: #00ffff">title of article</span></li>
                  
                  <li><span style="background-color: #ffcc00">title of journal/magazine</span></li>
                  
                  <li><span style="background-color: #ff00ff">volume</span></li>
                  
                  <li>issue (include only if the source starts on page 1 rather than continuously paginated)</li>
                  
                  <li><span style="background-color: #009900">page numbers</span></li>
                  
                  <li>
                     <span style="background-color: #ffff00">DOI or URL of the publication</span> (if the source is online)
                  </li>
                  
               </ol>
               
               <p>The following is an APA full citation for a journal article (with a DOI):</p>
               
               <p><span style="background-color: #ffff66">Mendelson, C.</span><span style="background-color: #00ff00">(2009)</span>. <span style="background-color: #00ffff">Diagnosis: A liminal state for women living with Lupus</span>. <em><span style="background-color: #ffcc00">Health Care for Women International,</span><span style="background-color: #ff00ff">30</span></em><span style="background-color: #ff00ff">,</span><span style="background-color: #009900">390-407.</span><span style="background-color: #ffff00">doi:10.1080/07399330902785158</span></p>
               
               <p>This example includes:</p>
               
               <ol>
                  
                  <li>the author's name --› Mendelson, C.</li>
                  
                  <li>the date --› (2009).</li>
                  
                  <li>the title of the article --› Diagnosis: A liminal state for women living with Lupus.</li>
                  
                  <li>the title of the journal --› <em>Health Care for Women International,</em>
                     
                  </li>
                  
                  <li>volume --› <em>30,</em>
                     
                  </li>
                  
                  <li>page numbers --› 390-407.</li>
                  
                  <li>DOI of the publication --› doi:10.1080/07399330902785158</li>
                  
               </ol>
               
               <p align="left">
                  <em>
                     <strong>What are DOIs?</strong>
                     </em>
                  
               </p>
               
               <p align="left">DOIs provide the permanent location of an article in the form of a URL. The DOI system
                  guarantees that links will never change, so authors and readers can rely on them to
                  locate articles indefinitely. Many, but not all, publishers are including DOIs in
                  journal articles. If a DOI for an article is <strong>not</strong> available, APA directs writers to simply <strong>include the URL of the journal's website in the citation.</strong>&nbsp;
               </p>
               
               <p>Valencia Library has created APA cheat sheets and made them available online. Review
                  our APA examples for library subscription databases.
               </p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Complete the activity below. Put the APA journal article citation in the correct order.
                     You can review our APA examples for databases to help you.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" title="Question 2"></p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Newspaper Examples</h2>
               
               <p>
                  <img width="300" alt="Image of Newspapers" class="resizable" hspace="30" metadata="obj15" border="0" height="400" longdesc="Photograph of newspapers on library shelves" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/newspapers.jpg" vspace="10" align="right">
                  
               </p>
               
               <p>The elements needed to cite a newspaper are:</p>
               
               <ol>
                  
                  <li><span style="background-color: #ffff66">author</span></li>
                  
                  <li><span style="background-color: #00ff00">date of publication</span></li>
                  
                  <li><span style="background-color: #00ffff">title of article</span></li>
                  
                  <li><span style="background-color: #ffcc00">title of newspaper</span></li>
                  
                  <li>
                     <span style="background-color: #ff00ff">URL of the publication</span> (if the newspaper is online)
                  </li>
                  
               </ol>
               
               <p>&nbsp;APA full citation a newspaper:</p>
               
               <p><span style="background-color: #ffff66">Gee, A</span>. <span style="background-color: #00ff00">(2012, June 13)</span>. <span style="background-color: #00ffff">Radiation concerns rise with patients' exposure.</span><em><span style="background-color: #ffcc00">The New York Times.</span></em><span style="background-color: #ff00ff">Retrieved from</span></p>
               
               <p style="margin-left: 40.0">
                  <span style="background-color: #ff00ff">http://www.nytimes.com/</span>
                  
               </p>
               
               <p>This example includes:</p>
               
               <ol>
                  
                  <li>the author's name --› Gee, A.</li>
                  
                  <li>the date --› (2012, June 13).</li>
                  
                  <li>the title of the article --› Radiation concerns rise with patients' exposure</li>
                  
                  <li>the title of the newspaper --› <em>The New York Times.</em>
                     
                  </li>
                  
                  <li>URL of the publication --› http://www.nytimes.com/</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <p>Valencia Library has created APA cheat sheets and made them available online. Review
                  our APA examples for newspapers.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;<strong>Complete the activity below. Put the APA newspaper citation in the correct order.
                     You can review our APA examples for newspapers to help you.</strong></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" title="Question 3">&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Webpage Examples</h2>
               
               <p>Webpages and websites are sometimes the most confusing sources to cite. Magazine,
                  journal, and newspaper articles are all found on the web; in those cases, citations
                  will be similar to the print versions of these items. Sometimes, however, the content
                  on a webpage does not fit into one of those categories - the content may simply be
                  information on a page. In cases like these, you need to think about the four W's:
               </p>
               
               <ul>
                  
                  <li>Who --› who wrote it?</li>
                  
                  <li>When --› what was the date of publication?</li>
                  
                  <li>What --› what was the name of the publication?</li>
                  
                  <li>Where --› where can it be found?</li>
                  
               </ul>
               
               <p>Just remember that regardless of format, APA instructs that you gather those four
                  pieces of information.
               </p>
               
               <p>APA full citation:</p>
               
               <p>U.S. National Library of Medicine, MedlinePlus. (2012). Imaging and radiology. Retrieved
                  from http://www.nlm.nih.gov/medlineplus/ency/article/007451.htm
               </p>
               
               <p>In this example:</p>
               
               <ul>
                  
                  <li>Who --› U.S. National Library of Medicine, MedlinePlus.</li>
                  
                  <li>When --› (2012).</li>
                  
                  <li>What --› Imaging and radiology.</li>
                  
                  <li>Where --› http://www.nlm.nih.gov/medlineplus/ency/article/007451.htm</li>
                  
               </ul>
               
               <p>Valencia Library has created APA cheat sheets and made them available online. Review
                  our APA examples for websites.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Website Activity</h2>
               
               <p>&nbsp;</p>
               
               <p>Connect to the website listed below and see if you can find the corporate author,
                  date of publication, title of the webpage and retrieval information.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Non-Small Cell Lung Cancer Treatment</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Complete the activity below. Put the APA website citation in the correct order. Review
                     our APA examples for websites to help you.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" title="Question 4"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Creating a Reference List</h2>
               
               <p>Putting several full citation entries together to make a bibliography is called a
                  Reference List in APA.
               </p>
               
               <ul>
                  
                  <li>The page should have 1 inch margins at the top, bottom, left, and right.</li>
                  
                  <li>All text on the page should be double-spaced.</li>
                  
                  <li>The word References should be centered at the top of the page.</li>
                  
                  <li>The entries should appear in alphabetical order by the author's last name, or, if
                     there is no author, by the title of the work.
                  </li>
                  
                  <li>If the full citation requires more than one line of text, the second line (and any
                     following lines) should be indented. This is called a hanging indent.
                  </li>
                  
               </ul>
               
               <p>
                  <img width="800" alt="references.jpg" class="resizable" hspace="30" metadata="obj18" height="455" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/references.jpg" vspace="10">
                  
               </p>
               
               <p><em>Figure 1.</em> Image of APA References page. Reprinted from Purdue Online Writing Lab. (2012). <em>APA sample paper</em>. Retrieved from http://owl.english.purdue.edu/owl/resource/560/18/
               </p>
               
               <p>&nbsp;</p>
               
               <p>You can also visit The Owl at Purdue's website to view a properly formatted researcher
                  paper and references page: APA sample paper and references page - PDF&nbsp;
               </p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Complete the activity below. Put the APA References page in the correct order.</strong>
                  
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" title="Question 5"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>For Additional Assistance</h2>
               
               <p>We highly recommend visiting the official APA Style Blog written and run by the folks
                  at APA (APA style experts). The contributing writers of the blog (who use the APA
                  style daily in their work) publish weekly posts on every aspect of APA style one can
                  think of. Questions posed by students, faculty and writers who need clarification
                  on the finer points of the APA style are answered by the bloggers. They even have
                  a Facebook page where student questions are answered.&nbsp;
               </p>
               
               <p>
                  <img width="800" alt="blog2.jpg" class="resizable" hspace="30" metadata="obj24" height="279" border="0" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/blog2.jpg" vspace="10">
                  
               </p>
               
               <p><em>Figure 2.</em> Image of APA Style Blog. Reprinted from American Psychological Association - APA
                  Style Blog. (2009). <em>APA style experts</em>. Retrieved from blog.apastyle.org/
               </p>
               
               <p>&nbsp;</p>
               
               <p>APA Style Blog</p>
               
               <p>APA Style on Facebook</p>
               
               <p align="center"></p>
               
               <h2>Assessment</h2>
               
               <p>
                  <font color="#ff0000">
                     <strong>Your instructor will provide the link to the online assessment. You must take the
                        assessment to receive credit for completing this tutorial.</strong>
                     </font>
                  
               </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations_print.pcf">©</a>
      </div>
   </body>
</html>