<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 12 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations12.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Page 12</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Complete the activity below. Put the APA References page in the correct order.</strong>
                  
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/spacer.gif" name="check29" alt="" id="check29"><a id="a29" href="javascript:toggletable('quizpopper29')" title="Question 5"><img src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/selfcheck_custom.png" alt="Toggle open/close quiz question" title="Question 5" border="0"></a> 
               </p>
               
               <div id="quizpopper29" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #DBA917; background: #fbf1d2; line-height: 1.5em; padding: 10px 15px;">
                     
                     <form name="f29" id="f29">Put the items in the correct order.
                        <div class="hide_stuff">Below is a sequence of events. Place them in the order they should occur, number 1
                           being the first item. Select the step number from the drop down next to each item.
                        </div>
                        
                        <div style="margin: 1em 15px;">
                           
                           <div class="hide_stuff">Items to order:<br>1.&nbsp;Butler, K., Rayens, M., Zhang, M., &amp; Hahn, E. (2011). Motivation to quit smoking
                              among relatives of lung cancer patients. <em>Public Health Nursing</em>, <em>28</em>(1), 43-50. doi:10.1111/j.1525-1446.2010.00916.x<br>2.&nbsp;Tse, L., Yu, I., Qiu, H., Kai, &amp; Wang, X. (2011). A Case-Referent Study of Lung
                              Cancer and Incense Smoke, Smoking, and Residential Radon in Chinese Men. <em>Environmental Health Perspectives</em>, <em>119</em>(11), 1641-1646.<br>3.&nbsp;References<br>4.&nbsp;Moreland, S. (2010). Nutrition screening and counseling in adults with lung cancer.
                              <em>Clinical Journal Of Oncology Nursing</em>, <em>14</em>(5), 609-614. doi:10.1188/10.CJON.609-614<br>
                              
                           </div>
                           
                           <table class="table ">
                              
                              <tr valign="top">
                                 
                                 <td><label for="q29_1">Butler, K., Rayens, M., Zhang, M., &amp; Hahn, E. (2011). Motivation to quit smoking among
                                       relatives of lung cancer patients. <em>Public Health Nursing</em>, <em>28</em>(1), 43-50. doi:10.1111/j.1525-1446.2010.00916.x</label></td>
                                 
                                 <td width="1%"><select name="q29_1" id="q29_1">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q29_2">Tse, L., Yu, I., Qiu, H., Kai, &amp; Wang, X. (2011). A Case-Referent Study of Lung Cancer
                                       and Incense Smoke, Smoking, and Residential Radon in Chinese Men. <em>Environmental Health Perspectives</em>, <em>119</em>(11), 1641-1646.</label></td>
                                 
                                 <td width="1%"><select name="q29_2" id="q29_2">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q29_3">References</label></td>
                                 
                                 <td width="1%"><select name="q29_3" id="q29_3">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option></select></td>
                                 
                              </tr>
                              
                              <tr valign="top">
                                 
                                 <td><label for="q29_4">Moreland, S. (2010). Nutrition screening and counseling in adults with lung cancer.
                                       <em>Clinical Journal Of Oncology Nursing</em>, <em>14</em>(5), 609-614. doi:10.1188/10.CJON.609-614</label></td>
                                 
                                 <td width="1%"><select name="q29_4" id="q29_4">
                                       <option>1&nbsp;</option>
                                       
                                       <option>2&nbsp;</option>
                                       
                                       <option>3&nbsp;</option>
                                       
                                       <option>4&nbsp;</option></select></td>
                                 
                              </tr>
                              
                           </table>
                           
                        </div>
                        
                        <div align="center"><input onclick="check_q(29, 4, 6, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done29" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed29" style="font-family: Comic Sans MS; border-top: 1px solid #DBA917; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations12.html#">return to top</a> | <a href="fullcitations11.html">previous page</a> | <a href="fullcitations13.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations12.pcf">©</a>
      </div>
   </body>
</html>