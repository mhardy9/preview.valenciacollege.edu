<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Journal and Magazine Citations | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations5.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Journal and Magazine Citations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="journalandmagazinecitations" id="journalandmagazinecitations"></a><h2>Journal and Magazine Citations</h2>
               
               <p><img width="300" alt="Image of Magazines" class="resizable" hspace="30" metadata="obj12" border="0" height="400" longdesc="ada_files/ada_image2.html" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/magazines2.jpg" vspace="10" align="right"> The elements needed to cite a journal or magazine article are:
               </p>
               
               <ol>
                  
                  <li><span style="background-color: #ffff66">author</span></li>
                  
                  <li><span style="background-color: #00ff00">date of publication</span></li>
                  
                  <li><span style="background-color: #00ffff">title of article</span></li>
                  
                  <li><span style="background-color: #ffcc00">title of journal/magazine</span></li>
                  
                  <li><span style="background-color: #ff00ff">volume</span></li>
                  
                  <li>issue (include only if the source starts on page 1 rather than continuously paginated)</li>
                  
                  <li><span style="background-color: #009900">page numbers</span></li>
                  
                  <li>
                     <span style="background-color: #ffff00">DOI or URL of the publication</span> (if the source is online)
                  </li>
                  
               </ol>
               
               <p>The following is an APA full citation for a journal article (with a DOI):</p>
               
               <p><span style="background-color: #ffff66">Mendelson, C.</span><span style="background-color: #00ff00">(2009)</span>. <span style="background-color: #00ffff">Diagnosis: A liminal state for women living with Lupus</span>. <em><span style="background-color: #ffcc00">Health Care for Women International,</span><span style="background-color: #ff00ff">30</span></em><span style="background-color: #ff00ff">,</span><span style="background-color: #009900">390-407.</span><span style="background-color: #ffff00">doi:10.1080/07399330902785158</span></p>
               
               <p>This example includes:</p>
               
               <ol>
                  
                  <li>the author's name --› Mendelson, C.</li>
                  
                  <li>the date --› (2009).</li>
                  
                  <li>the title of the article --› Diagnosis: A liminal state for women living with Lupus.</li>
                  
                  <li>the title of the journal --› <em>Health Care for Women International,</em>
                     
                  </li>
                  
                  <li>volume --› <em>30,</em>
                     
                  </li>
                  
                  <li>page numbers --› 390-407.</li>
                  
                  <li>DOI of the publication --› doi:10.1080/07399330902785158</li>
                  
               </ol>
               
               <p align="left">
                  <em>
                     <strong>What are DOIs?</strong>
                     </em>
                  
               </p>
               
               <p align="left">DOIs provide the permanent location of an article in the form of a URL. The DOI system
                  guarantees that links will never change, so authors and readers can rely on them to
                  locate articles indefinitely. Many, but not all, publishers are including DOIs in
                  journal articles. If a DOI for an article is <strong>not</strong> available, APA directs writers to simply <strong>include the URL of the journal's website in the citation.</strong>&nbsp;
               </p>
               
               <p>Valencia Library has created APA cheat sheets and made them available online. Review
                  our <a target="_blank" href="http://valenciacc.edu/library/doc_apa_electronic.cfm">APA examples for library subscription databases.</a></p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations5.html#">return to top</a> | <a href="fullcitations4.html">previous page</a> | <a href="fullcitations6.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations5.pcf">©</a>
      </div>
   </body>
</html>