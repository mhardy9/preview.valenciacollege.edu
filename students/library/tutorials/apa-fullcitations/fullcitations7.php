<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Newspaper Examples | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-fullcitations/fullcitations7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-fullcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-fullcitations/">Apa Fullcitations</a></li>
               <li>Newspaper Examples</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="newspaperexamples" id="newspaperexamples"></a><h2>Newspaper Examples</h2>
               
               <p>
                  <img width="300" alt="Image of Newspapers" class="resizable" hspace="30" metadata="obj15" border="0" height="400" longdesc="ada_files/ada_image3.html" src="http://valenciacollege.edu/library/tutorials/apa-fullcitations/newspapers.jpg" vspace="10" align="right">
                  
               </p>
               
               <p>The elements needed to cite a newspaper are:</p>
               
               <ol>
                  
                  <li><span style="background-color: #ffff66">author</span></li>
                  
                  <li><span style="background-color: #00ff00">date of publication</span></li>
                  
                  <li><span style="background-color: #00ffff">title of article</span></li>
                  
                  <li><span style="background-color: #ffcc00">title of newspaper</span></li>
                  
                  <li>
                     <span style="background-color: #ff00ff">URL of the publication</span> (if the newspaper is online)
                  </li>
                  
               </ol>
               
               <p>&nbsp;APA full citation a newspaper:</p>
               
               <p><span style="background-color: #ffff66">Gee, A</span>. <span style="background-color: #00ff00">(2012, June 13)</span>. <span style="background-color: #00ffff">Radiation concerns rise with patients' exposure.</span><em><span style="background-color: #ffcc00">The New York Times.</span></em><span style="background-color: #ff00ff">Retrieved from</span></p>
               
               <p style="margin-left: 40.0px">
                  <span style="background-color: #ff00ff">http://www.nytimes.com/</span>
                  
               </p>
               
               <p>This example includes:</p>
               
               <ol>
                  
                  <li>the author's name --› Gee, A.</li>
                  
                  <li>the date --› (2012, June 13).</li>
                  
                  <li>the title of the article --› Radiation concerns rise with patients' exposure</li>
                  
                  <li>the title of the newspaper --› <em>The New York Times.</em>
                     
                  </li>
                  
                  <li>URL of the publication --› http://www.nytimes.com/</li>
                  
               </ol>
               
               <p>&nbsp;</p>
               
               <p>Valencia Library has created APA cheat sheets and made them available online. Review
                  our <a target="_blank" href="http://valenciacc.edu/library/documents/NewsbankAmericasNewspapersAPA6.pdf">APA examples for newspapers.</a></p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="fullcitations7.html#">return to top</a> | <a href="fullcitations6.html">previous page</a> | <a href="fullcitations8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-fullcitations/fullcitations7.pcf">©</a>
      </div>
   </body>
</html>