<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 12 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities12.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Page 12</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<br>
               
               
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Content</font>
                     </strong>
                  
               </p>
               
               <p><strong><font size="5" style="line-height: 1"><br strong="true">
                        </font></strong> <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/C%20Tattoo.jpg" hspace="30" width="102" align="left" height="106" vspace="10" metadata="obj5" alt="C Tattoo.jpg" border="0"> <a class="textpopper" href="Module_Three_Sans_Activities12.html#" onmouseover="return overlib('The things that are held or included in something', CAPTION, 'content', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('The things that are held or included in something', CAPTION, 'content', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">Content</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation6.html" border="0"> has Val focus in on the <strong>HOW</strong>. How is a site organized? Is it easy to navigate or do you have to search through
                  page after page for the information you need? Does the site look professional? Spelling
                  and/or grammatical errors are indicators of the work put into the site and clues to
                  the validity of the information posted. If someone can't run the spell check option,
                  can they be trusted to check the facts to verify their validity? Mistakes on the page
                  should have you questioning how thorough the author/publisher was with all aspects
                  of their work.<br>
                  
               </p>
               
               <p>Content also covers the information provided by the website and information about
                  the publisher. Val should be able to find out who is responsible for the information
                  on the website and how to contact them. Can Val find the author/organization's mission
                  statement? Is the information provided meant to inform, entertain, coerce or sell?
                  The <a href="http://www.si.edu/" target="_blank">Smithsonian Institution</a> has a website linking to its associated 19 museums, the national Zoo and nine affiliated
                  research centers. While it does provide information on membership benefits, the content
                  of the site is overwhelmingly geared to its mission statement of the "diffusion of
                  knowledge." Smithsonianmag.com is a magazine associated with the Smithsonian that
                  provides popular articles on the various subject areas covered in the many museums.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Smithsonian.png" hspace="30" longdesc="ada_files/ada_image7.html" width="967" height="763" vspace="10" alt="Smithsonian.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a href="http://www.designtattooideas.com/live-love-laugh-tattoo/" target="_blank">Design.Tattoo.Ideas</a>. Used under a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/" target="_blank">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities12.html#">return to top</a> | <a href="Module_Three_Sans_Activities11.html">previous page</a> | <a href="Module_Three_Sans_Activities13.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities12.pcf">©</a>
      </div>
   </body>
</html>