<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Module 3 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Module 3</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="module3" id="module3"></a><h2>
                  <strong>
                     <font size="6" style="line-height: 1">
                        <u>Module 3</u>
                        </font>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Evaluating Websites</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;<strong>[Read the following scene. Then click "next page" to begin the learning module.]</strong></p>
               
               <p>
                  <font size="5" style="line-height: 1">Scene 3</font>
                  
               </p>
               
               <p><img longdesc="ada_files/ada_image1.html" align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_three/Book%20Tattoos.jpg" width="220" alt="Book Tattoos.jpg" class="resizable" height="229" border="0" hspace="30"> Val has looked for books from the library and articles from the databases on her
                  topic of tattoos. But, it's so hard for her to ignore the Internet. After all, she
                  uses it all the time to help get information for personal decisions. Val wonders if
                  there is any difference between information needed for personal use and information
                  for scholarly research. Certainly, she thought, there is something on the Internet
                  that can be used for her classroom assignment. She decides to go talk to the librarian,
                  Mr. Bookman, who came and visited her class.
               </p>
               
               <p><strong>VAL</strong>: Excuse me, Mr. Bookman.&nbsp; I've found some great information from both books and databases
                  for my paper.&nbsp; But really, don't most people just go to the Internet for answers these
                  days?
               </p>
               
               <p><strong>BOOKMAN:&nbsp;</strong> Yes, Val.&nbsp; People do go to the Internet for all kinds of information.&nbsp; But, what
                  we have to remember about the Internet is that anyone, anywhere, at any time can post
                  information.&nbsp; As the user of information, we don't know who these people really are,
                  when and where they got their information, and if the information is factual. While
                  people think it might be quicker to just "Google it," you have to analyze information
                  from the Internet.&nbsp;
               </p>
               
               <p><strong>VAL:</strong>&nbsp; I'd like to know the difference between what I find in books and articles and what
                  is on the Internet.
               </p>
               
               <p>&nbsp;</p>
               
               <p><strong>BOOKMAN:</strong> Val, there are many fabulous web sites on the Internet with reliable, credible information
                  that even the most experienced researchers often turn to. However, the catch is that
                  there is also a lot of junk! It's up to you, the researcher, to determine which is
                  which.
               </p>
               
               <p><strong>VAL:</strong> So finding a good web site on the Internet is like trying to find a needle in a haystack?
               </p>
               
               <p><strong>BOOKMAN:</strong> (Laughing) How about more like panning for gold? You need to sift out the junk and
                  know how to spot the best sites. Which reminds me, I have some tips for you on how
                  to do just that (and gives Val a handout with web site evaluation tips).&nbsp;&nbsp;
               </p>
               
               <p align="center">(Photo credit: <a target="_blank" href="http://www.cherrybombed.com/2009/06/i-love-books-a-study-in-literary-tattoos/">CherryBombed.com</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc-nd/2.0/">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities.html#">return to top</a> | <a href="Module_Three_Sans_Activities2.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities.pcf">©</a>
      </div>
   </body>
</html>