<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information Literacy 3 - Website Evaluation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Information Literacy 3 - Website Evaluation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Information Literacy 3 - Website Evaluation</strong><br>Valencia College Libraries
               </p>
               
               
               <h2>
                  <strong>
                     <font size="6">
                        <u>Module 3</u>
                        </font>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>
                     <font size="5">Evaluating Websites</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;<strong>[Read the following scene. Then click "next page" to begin the learning module.]</strong></p>
               
               <p>
                  <font size="5">Scene 3</font>
                  
               </p>
               
               <p><img longdesc="Tatto of books" align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_three/Book%20Tattoos.jpg" width="220" alt="Book Tattoos.jpg" class="resizable" height="229" border="0" hspace="30"> Val has looked for books from the library and articles from the databases on her
                  topic of tattoos. But, it's so hard for her to ignore the Internet. After all, she
                  uses it all the time to help get information for personal decisions. Val wonders if
                  there is any difference between information needed for personal use and information
                  for scholarly research. Certainly, she thought, there is something on the Internet
                  that can be used for her classroom assignment. She decides to go talk to the librarian,
                  Mr. Bookman, who came and visited her class.
               </p>
               
               <p><strong>VAL</strong>: Excuse me, Mr. Bookman.&nbsp; I've found some great information from both books and databases
                  for my paper.&nbsp; But really, don't most people just go to the Internet for answers these
                  days?
               </p>
               
               <p><strong>BOOKMAN:&nbsp;</strong> Yes, Val.&nbsp; People do go to the Internet for all kinds of information.&nbsp; But, what
                  we have to remember about the Internet is that anyone, anywhere, at any time can post
                  information.&nbsp; As the user of information, we don't know who these people really are,
                  when and where they got their information, and if the information is factual. While
                  people think it might be quicker to just "Google it," you have to analyze information
                  from the Internet.&nbsp;
               </p>
               
               <p><strong>VAL:</strong>&nbsp; I'd like to know the difference between what I find in books and articles and what
                  is on the Internet.
               </p>
               
               <p>&nbsp;</p>
               
               <p><strong>BOOKMAN:</strong> Val, there are many fabulous web sites on the Internet with reliable, credible information
                  that even the most experienced researchers often turn to. However, the catch is that
                  there is also a lot of junk! It's up to you, the researcher, to determine which is
                  which.
               </p>
               
               <p><strong>VAL:</strong> So finding a good web site on the Internet is like trying to find a needle in a haystack?
               </p>
               
               <p><strong>BOOKMAN:</strong> (Laughing) How about more like panning for gold? You need to sift out the junk and
                  know how to spot the best sites. Which reminds me, I have some tips for you on how
                  to do just that (and gives Val a handout with web site evaluation tips).&nbsp;&nbsp;
               </p>
               
               <p align="center">(Photo credit: CherryBombed.com. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><br>
                  <img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_three/Tattoo.jpg" width="275" alt="Tattoo.jpg" class="resizable" height="183" border="0" hspace="30"> With so much information available, how can Val tell what is "true" and what is not
                  on the Internet? Some websites look like valid resources. Yet, after closer inspection,
                  the information provided is either suspect or proven to be incorrect when compared
                  to known legitimate sources. How can she determine if the information from the Internet
                  is appropriate for use in her academic assignment? Val looks at the concept map on
                  the handout given to her by the librarian. It diagramed the steps she should take
                  to determine if an Internet source is valid for research – the ABC³!
               </p>
               
               <p>&nbsp;</p>
               
               <p><br>
                  
               </p>
               
               <p align="center">
                  <img src="http://valenciacollege.edu/library/tutorials/module_three/paste_image1.png" width="624" class="resizable" height="433">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Tatto Design Ideas. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license Image credit: Microsoft Clip Gallery.)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img longdesc="info sign" align="left" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" width="92" alt="info1.jpg" class="resizable" height="92" border="0" metadata="obj1">
                  
               </p>
               
               <p><strong><u>After evaluating the ABC³ questions for any source, Val should be able to confidently
                        determine the validity of sources. She will be able to focus in on the important elements
                        of a website to determine if the site is a legitimate, authoritative information source
                        and appropriate for use in her academic research project. Complete all acivities in
                        this module so you, too, can recognize the legitimacy of any informational source.</u></strong><br>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Authority</font>
                     </strong>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/letter%20a.png" hspace="30" longdesc="Letter A Tattoo" width="225" align="right" vspace="10" height="178" metadata="obj9" alt="letter a.png" border="0">
                  
               </p>
               
               <p>Authority is the <strong>WHO</strong>. Who is the author of the website? Val needs to check through the site thoroughly.
                  She should be able to determine who created the site. Is it a government site with
                  a web address (URL) ending in .gov or an educational institution site with an URL
                  ending in .edu? The Centers for Disease Control and Prevention is part of the U.S.
                  Department of Health and Human Services. Information from this website is considered
                  accurate because it is backed by an authoritative<img src="https://lh5.googleusercontent.com/SQegB9uBq0NqkL_wXA9aWMpN3GUvSeCFMC72Jo4ZV1kZ5ntz5cM0jqircEXaPUktGhHS6r08aR8AckT6jNjPCuL56O-b47-u64_CQavSFywREikjbxQ" width="1" height="1"> source. Val found information on safety issuses related to tattooing from the CDC.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/CDC%20Tattoo%20Site.png" hspace="30" longdesc="CDC Tattoo Site" width="1019" height="770" vspace="10" alt="CDC Tattoo Site.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Tattoos Valley. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Authority</font>
                     </strong>
                  
               </p>
               
               <p>Val searched the Internet using a search engine like Google. One search result on
                  the history of tattooing had an .edu ending. Val looked closer at the site to try
                  and determine the author of the information.<br>
                  
               </p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/EDU.png" hspace="30" width="832" height="495" vspace="10" metadata="obj25" alt="EDU.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Authority</font>
                     </strong>
                  
               </p>
               
               <p>After clicking on the Contact link at the bottom of the page, Val noticed that the
                  author of the website was a student who developed the site as a class project. Val
                  was surprised to find student work on the Internet that at first glance looked like
                  information provided by the educational institution. After she thought about it, she
                  realized that higher education organizations can publish all kinds of information
                  – not just what is considered academic research.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Edu%20Tattoo%20Shorter.png" hspace="30" width="841" height="368" vspace="10" metadata="obj26" alt="Edu Tattoo Shorter.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Authority</font>
                     </strong>
                  
               </p>
               
               <p>Consider more than <strong>government</strong> and <strong>academic</strong> sites. <strong>Nonprofit institutions'</strong> URLs generally end in .org and can offer rich sources of information. The American
                  Cancer Society provides up-to-date information on research into many aspects associated
                  with different types of cancer. The URL endings are a guideline to a site's authority.
                  Be warned. Some sites such as Wikipedia end in .org. While it is a nonprofit organization,
                  information Wikipedia posts is NOT considered citable. Anyone can enter the site and
                  create their own posts or add misleading or incorrect information to existing posts.<br>
                  
               </p>
               
               <p>Other popular URL endings or extensions are .net for internet service providers and
                  .mil for U. S. military websites. Can an individual not considered an expert in a
                  subject develop a website because it's their hobby? Of course. Val was surprised that
                  the History of Tattooing site she had found earlier was not only a class project;
                  it was merely an interest of the site's author. An individual's hobby does not make
                  that person an expert in the subject. The information may sound legitimate. But, if
                  you cannot determine that the author has some type of expertise in the subject, try
                  looking for another source. Commercial websites usually end with .com or .co. Individuals
                  and businesses can buy a website name and post information that they want to disseminate.
                  Remember, there are plenty of websites posted on the World Wide Web. Look for sites
                  that are backed by a known authority.<br>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left"><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" hspace="30" width="92" align="left" height="92" vspace="10" metadata="obj13" alt="info1.jpg" border="0"> <strong>Val needs to ask these questions</strong>:
               </p>
               
               <p>Can she identify the author/site sponsor of the website? (She might have to look hard.)</p>
               
               <p>What makes this person/entity an expert in the field?</p>
               
               <p>Is the author affiliated with any reputable organizations?</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <u>The bottom line: Information needs to come from sources that are knowledgeable about
                        that subject.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Bias</font>
                     </strong>
                  
               </p>
               
               <p><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Fancy%20b%20tattoo.jpg" hspace="30" longdesc="Letter B tattoo" width="189" align="right" vspace="10" height="267" metadata="obj3" alt="Fancy b tattoo.jpg" border="0"> Bias has Val look behind the information and ask the question <strong>WHY</strong>. Why did this person/organization develop this website? Is it to inform or is it
                  to persuade? Val needs to look to see if a website is presenting information with
                  a biased <strong>point-of-view</strong> to influence the way she thinks about a subject. Val can look at websites that advertise
                  they have the best tattoo designs and they know the real history behind tattoos. But,
                  are they really just trying to sell Val a tattoo? These sites are meant to influence
                  our perception of the product. Remember that everyone has an agenda or reason for
                  providing a website. Val's job is to find out if that reason is to share information
                  or to influence a reader.<br>
                  
               </p>
               
               <p>Val should also check to see if a site hosts advertisements that might show some degree
                  of partiality towards a particular point. Advertisements are placed on websites that
                  reflect the advertisers' point of view. Val must then ask if hosting ads is limiting
                  a balanced point of view. Sponsorships can create a conflict of interest and indicate
                  a tendency for a website to provide selective information and not all of the facts
                  that you would find on an impartial site. However, a site established to provide only
                  one point of view does not make it worthless to you as a researcher. In fact, it might
                  contain valuable up-to-date information. Be vigilant about the validity of the information
                  provided and make sure you cross check information. All sites have some level of bias.
                  It is Val's job to determine if bias affects the validity of the information.<br>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Sofie's Art Studio. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p align="center">&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Bias</font>
                     </strong>
                  
               </p>
               
               <p>Val found a site about tattoo history that is really a site about tattoo removals.
                  The site does not list what makes them an expert in the history of tattoos. They also
                  state that they are the leading authority in tattoo removal but once again they do
                  not say what has given them this authority. Therefore, the information they provide
                  is suspect. Val feels that this site is only trying to sell her a service so she moves
                  on to other websites. Anyone can publish their own personal viewpoints, or opinions,
                  on the Internet. Facts provided by a source must be verified for authenticity<img src="https://lh5.googleusercontent.com/SQegB9uBq0NqkL_wXA9aWMpN3GUvSeCFMC72Jo4ZV1kZ5ntz5cM0jqircEXaPUktGhHS6r08aR8AckT6jNjPCuL56O-b47-u64_CQavSFywREikjbxQ" width="1" height="1">.<br>
                  
               </p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Tattoo%20Removal%20Website.png" hspace="30" longdesc="Tattoo Removal Website" width="1088" height="760" vspace="10" alt="Tattoo Removal Website.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" hspace="30" width="92" align="left" height="92" vspace="10" metadata="obj15" alt="info1.jpg" border="0"> <strong>Val needs to ask these questions</strong>:
               </p>
               
               <p>Can you tell the level of the information's accuracy?</p>
               
               <p>Is the "tone" of the page impartial?</p>
               
               <p>Is there any supporting evidence cited for verification?</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <u>The bottom line: If information sources are biased, the facts might be presented in
                        a way where we need to question the validity of that information. Biased sources don't
                        always provide misinformation, but it might be possible. Check other sources to verify
                        the information as accurate.</u>
                     </strong>
                  
               </p>
               <br>
               
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               <br>
               
               
               <p>
                  <strong>
                     <font size="5">Content</font>
                     </strong>
                  
               </p>
               
               <p><strong><font size="5"><br strong="true">
                        </font></strong> <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/C%20Tattoo.jpg" hspace="30" width="102" align="left" height="106" vspace="10" metadata="obj5" alt="C Tattoo.jpg" border="0"> Content has Val focus in on the <strong>HOW</strong>. How is a site organized? Is it easy to navigate or do you have to search through
                  page after page for the information you need? Does the site look professional? Spelling
                  and/or grammatical errors are indicators of the work put into the site and clues to
                  the validity of the information posted. If someone can't run the spell check option,
                  can they be trusted to check the facts to verify their validity? Mistakes on the page
                  should have you questioning how thorough the author/publisher was with all aspects
                  of their work.<br>
                  
               </p>
               
               <p>Content also covers the information provided by the website and information about
                  the publisher. Val should be able to find out who is responsible for the information
                  on the website and how to contact them. Can Val find the author/organization's mission
                  statement? Is the information provided meant to inform, entertain, coerce or sell?
                  The Smithsonian Institution has a website linking to its associated 19 museums, the
                  national Zoo and nine affiliated research centers. While it does provide information
                  on membership benefits, the content of the site is overwhelmingly geared to its mission
                  statement of the "diffusion of knowledge." Smithsonianmag.com is a magazine associated
                  with the Smithsonian that provides popular articles on the various subject areas covered
                  in the many museums.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Smithsonian.png" hspace="30" longdesc="Smithsonian Magazine article on tattoos" width="967" height="763" vspace="10" alt="Smithsonian.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Design.Tattoo.Ideas. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Content</font>
                     </strong>
                  
               </p>
               
               <p>Beware of parody sites. These websites are meant to entertain - not inform. Val can
                  spot a satire website if it makes outrageous claims like the parody site Lasik@Home.
               </p>
               
               <p>Here's another parody site Val found at Cat For Gold!:</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/CatsForGold.jpg" hspace="30" width="815" height="696" vspace="10" metadata="obj6" alt="CatsForGold.jpg" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" hspace="30" width="92" align="left" height="92" vspace="10" metadata="obj16" alt="info1.jpg" border="0"> <strong>Val needs to ask these questions</strong>:
               </p>
               
               <p>Is the site easy to navigate?</p>
               
               <p>Is the information presented in a clear, informative manner?</p>
               
               <p>Does the presence of any illustrations add to your understanding of the subject?</p>
               <br>
               <br>
               <strong>
                  <u>The bottom line: Information needs to be provided in a clear manner from a trusted
                     source.</u>
                  </strong> 
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <font size="5">Currency</font>
                     </strong>
                  
               </p>
               
               <p><br>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/C%20Tattoo.jpg" hspace="30" width="102" align="left" height="106" vspace="10" alt="C Tattoo.jpg" border="0"> Currency refers to the timeliness of the information or <strong>WHEN</strong>. Every website should state when it was originally developed or copyrighted. It should
                  also include when it was last modified or the date the information was published.
                  The Homeland Security site on immigration shows the last date the web page was "reviewed/modified"
                  at the bottom of the screen. A site on stem cell research posted in 2000 might provide
                  background information into the field. It will not, however, contain up-to-date information
                  from a field that has seen many changes over the last few years. If "fresh" data is
                  necessary for a project, Val should make sure the website consulted lists a recent
                  publication date. It might be difficult to find dates detailing the posting of information
                  on a website but look closely. If Val can't find a date associated with the posting
                  of information, she would be wise to find another source. Val realizes that her subject
                  isn't dependent on time for the validity of the information. She still wants to know
                  if the sites might include up-to-date facts so she will find out how long ago her
                  websites were published and updated.<br>
                  
               </p>
               
               <p align="center">(Photo credit: Design.Tattoo.Ideas. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" hspace="30" width="92" align="left" height="92" vspace="10" metadata="obj17" alt="info1.jpg" border="0"> <strong>Val needs to ask these questions</strong>:
               </p>
               
               <p>Can you find the date that the site was first published/copyrighted?</p>
               
               <p>Can you find the date the website was last updated?</p>
               
               <p>Older information may be suited for historical purposes. Do you need timely data?</p>
               <br>
               <br>
               <strong>
                  <u>The bottom line: Sometimes the age of the information makes a difference - sometimes
                     it doesn't. Depending on your research question, determine if you need the most recent
                     data.</u>
                  </strong> 
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Consistency</font>
                     </strong>
                  
               </p>
               
               <p><br>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/C%20Tattoo.jpg" hspace="30" width="102" align="left" height="106" vspace="10" alt="C Tattoo.jpg" border="0"> Consistency is the last test for evaluating a website. It compares information from
                  different sources to determine if the information has some uniformity. Consistency
                  answers the <strong>WHAT</strong> question. What information is given? Does the site offer both pros and cons regarding
                  a particular subject? Does the information provided generally coincide with other
                  authoritative sources? If a site does not provide information on both sides of an
                  argument, it should at least be a reputable source that provides verifiable information.
                  Mothers Against Drunk Driving provides statistics to support the law that drinking
                  should be illegal for individuals under the age of 21. While the site can be seen
                  as having some bias, the information provided can be found and verified on other legitimate
                  sites. Check the related links provided by the website. The links should go to other
                  authoritative sites. Val needs websites that have a clear authority and where the
                  information provided has similar information to information she already found in the
                  books and database articles.
               </p>
               
               <p>The two websites below were sites that Val found interesting. The first site, tattoos.com,
                  not only looks biased, Val is not sure that any of the information comes from a reasonable
                  source. Val also questions if the information is any way consistent with the information
                  from her prior research.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Commercial%20Site.png" hspace="30" longdesc="Commercial tattoo site" width="838" height="774" vspace="10" alt="Commercial Site.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: Design.Tattoo.Ideas. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;<strong><font size="5">Consistency</font></strong></p>
               
               <p>&nbsp;</p>
               
               <p>On the other hand, Val found the information from the pbs.org site to not only be
                  consistent with her research, she knows they are a name she can trust.
               </p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/PBS%20site.png" hspace="30" longdesc="PBS website on polynesian tattoos" width="711" height="790" vspace="10" alt="PBS site.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left"><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" hspace="30" width="92" align="left" height="92" vspace="10" metadata="obj18" alt="info1.jpg" border="0"> <strong>Val needs to ask these questions</strong>:
               </p>
               
               <p>What is the original source of the information?</p>
               
               <p>Are there additional web links to reputable additional/related sites?</p>
               
               <p>What other sites link to the site under review?</p>
               <br>
               <strong>
                  <u>The bottom line: Not all information contains the exact same facts and figures. But,
                     information should include a similar thread of thought. If you find information that
                     is totally different from everything else you've found, you need to question the source.</u>
                  </strong> 
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <p align="left"><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Tattoo2.jpg" hspace="30" width="275" align="right" height="183" vspace="10" alt="Tattoo2.jpg" border="0"> Val decided that there's a lot of information on the Internet – maybe too much! If
                  she could use popular sources for her academic research paper, she would go back and
                  look at the information available at the pbs.org and Smithsonian.com sites. However,
                  Val is comfortable with the information she found in the books and articles from the
                  databases. She feels she can now judge the quality of information found on the web.
                  So the next time she needs any information from the Internet, Val is better prepared
                  to decided what is quality and what is just quantity.<br>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p><br>
                  
               </p>
               
               <p align="center">(Photo credit: Tattoos-and-art.com. Used under a Creative Commons Attribution-Non-Commercial-NoDerivs
                  2.0 Generic license)
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Conclusion</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left"><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/info1.jpg" hspace="30" width="92" align="left" height="92" vspace="10" metadata="obj18" alt="info1.jpg" border="0"> In this module you learned about:
               </p>
               
               <ul>
                  
                  <li>Examining information to evaluate authority, bias, content, currency and consistency.</li>
                  
                  <li>Recognizing pariody.</li>
                  
                  <li>Comparing new and prior knowledge for discrepancies.</li>
                  
                  <li>Identifing the source of the information.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities_print.pcf">©</a>
      </div>
   </body>
</html>