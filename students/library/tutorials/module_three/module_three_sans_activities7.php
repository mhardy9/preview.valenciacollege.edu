<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 7 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Page 7</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Authority</font>
                     </strong>
                  
               </p>
               
               <p>Consider more than <strong>government</strong> and <strong>academic</strong> sites. <strong>Nonprofit institutions'</strong> URLs generally end in .org and can offer rich sources of information. The <a href="http://www.cancer.org/" target="_blank">American Cancer Society</a> provides up-to-date information on research into many aspects associated with different
                  types of cancer. The URL endings are a guideline to a site's authority. Be warned.
                  Some sites such as <a href="http://www.wikipedia.org/">Wikipedia</a> end in .org. While it is a nonprofit organization, information Wikipedia posts is
                  NOT considered citable. Anyone can enter the site and create their own posts or add
                  misleading or incorrect information to existing posts.<br>
                  
               </p>
               
               <p>Other popular URL endings or extensions are .net for internet service providers and
                  .mil for U. S. military websites. Can an individual not considered an expert in a
                  subject develop a website because it's their hobby? Of course. Val was surprised that
                  the <a href="https://www.msu.edu/~krcmari1/individual/history.html" target="_blank">History of Tattooing</a> site she had found earlier was not only a class project; it was merely an interest
                  of the site's author. An individual's hobby does not make that person an expert in
                  the subject. The information may sound legitimate. But, if you cannot determine that
                  the author has some type of expertise in the subject, try looking for another source.
                  Commercial websites usually end with .com or .co. Individuals and businesses can buy
                  a website name and post information that they want to disseminate. Remember, there
                  are plenty of websites posted on the World Wide Web. Look for sites that are backed
                  by a known authority.<br>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities7.html#">return to top</a> | <a href="Module_Three_Sans_Activities6.html">previous page</a> | <a href="Module_Three_Sans_Activities8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities7.pcf">©</a>
      </div>
   </body>
</html>