<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 2 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Page 2</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><br>
                  <img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_three/Tattoo.jpg" width="275" alt="Tattoo.jpg" class="resizable" height="183" border="0" hspace="30"> With so much information available, how can Val tell what is "true" and what is not
                  on the Internet? Some websites look like valid resources. Yet, after closer inspection,
                  the information provided is either suspect or proven to be incorrect when compared
                  to known <a class="textpopper" href="Module_Three_Sans_Activities2.html#" onmouseover="return overlib('in accordance with established rules, principles, or standards', CAPTION, 'legitimate', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('in accordance with established rules, principles, or standards', CAPTION, 'legitimate', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">legitimate</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation1.html" border="0"> sources. How can she determine if the information from the Internet is appropriate
                  for use in her academic assignment? Val looks at the concept map on the handout given
                  to her by the librarian. It diagramed the steps she should take to determine if an
                  Internet source is valid for research – the ABC³!
               </p>
               
               <p>&nbsp;</p>
               
               <p><br>
                  
               </p>
               
               <p align="center">
                  <img src="http://valenciacollege.edu/library/tutorials/module_three/paste_image1.png" width="624" class="resizable" height="433">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a target="_blank" href="http://www.tattoosdesignideas.com/tag/celebrity-tattoos-women/">Tatto Design Ideas</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc-nd/2.0/">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a> Image credit: Microsoft Clip Gallery.)
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities2.html#">return to top</a> | <a href="Module_Three_Sans_Activities.html">previous page</a> | <a href="Module_Three_Sans_Activities3.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities2.pcf">©</a>
      </div>
   </body>
</html>