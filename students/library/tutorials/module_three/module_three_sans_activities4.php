<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 4 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Page 4</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Authority</font>
                     </strong>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/letter%20a.png" hspace="30" longdesc="ada_files/ada_image3.html" width="225" align="right" vspace="10" height="178" metadata="obj9" alt="letter a.png" border="0">
                  
               </p>
               
               <p><a class="textpopper" href="Module_Three_Sans_Activities4.html#" onmouseover="return overlib('The right to act in a specified way', CAPTION, 'authority', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('The right to act in a specified way', CAPTION, 'authority', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">Authority</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation2.html" border="0"> is the <strong>WHO</strong>. Who is the author of the website? Val needs to check through the site thoroughly.
                  She should be able to determine who created the site. Is it a government site with
                  a web address (URL) ending in .gov or an educational institution site with an URL
                  ending in .edu? The <a href="http://www.cdc.gov/" target="_blank">Centers for Disease Control and Prevention</a> is part of the U.S. Department of Health and Human Services. Information from this
                  website is considered accurate because it is backed by an <a class="textpopper" href="Module_Three_Sans_Activities4.html#" onmouseover="return overlib('Able to be trusted as being accurate or true', CAPTION, 'authoritative', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('Able to be trusted as being accurate or true', CAPTION, 'authoritative', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">authoritative</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation3.html" border="0"><img src="https://lh5.googleusercontent.com/SQegB9uBq0NqkL_wXA9aWMpN3GUvSeCFMC72Jo4ZV1kZ5ntz5cM0jqircEXaPUktGhHS6r08aR8AckT6jNjPCuL56O-b47-u64_CQavSFywREikjbxQ" width="1" height="1"> source. Val found information on safety issuses related to tattooing from the <a href="http://www.cdc.gov/Features/BodyArt/" target="_blank">CDC</a>.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/CDC%20Tattoo%20Site.png" hspace="30" longdesc="ada_files/ada_image4.html" width="1019" height="770" vspace="10" alt="CDC Tattoo Site.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a href="http://www.tattoosvalley.com/word-tattoos/tattoo-of-one-letter/" target="_blank">Tattoos Valley</a>. Used under a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/" target="_blank">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities4.html#">return to top</a> | <a href="Module_Three_Sans_Activities3.html">previous page</a> | <a href="Module_Three_Sans_Activities5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities4.pcf">©</a>
      </div>
   </body>
</html>