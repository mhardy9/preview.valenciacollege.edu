<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 9 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities9.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Page 9</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Bias</font>
                     </strong>
                  
               </p>
               
               <p><img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Fancy%20b%20tattoo.jpg" hspace="30" longdesc="ada_files/ada_image5.html" width="189" align="right" vspace="10" height="267" metadata="obj3" alt="Fancy b tattoo.jpg" border="0"> <a class="textpopper" href="Module_Three_Sans_Activities9.html#" onmouseover="return overlib('to cause partiality or favoritism, especially unfairly', CAPTION, 'bias', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('to cause partiality or favoritism, especially unfairly', CAPTION, 'bias', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">Bias</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation4.html" border="0"> has Val look behind the information and ask the question <strong>WHY</strong>. Why did this person/organization develop this website? Is it to inform or is it
                  to persuade? Val needs to look to see if a website is presenting information with
                  a biased <strong>point-of-view</strong> to influence the way she thinks about a subject. Val can look at websites that advertise
                  they have the best tattoo designs and they know the real history behind tattoos. But,
                  are they really just trying to sell Val a tattoo? These sites are meant to influence
                  our perception of the product. Remember that everyone has an agenda or reason for
                  providing a website. Val's job is to find out if that reason is to share information
                  or to influence a reader.<br>
                  
               </p>
               
               <p>Val should also check to see if a site hosts advertisements that might show some degree
                  of partiality towards a particular point. Advertisements are placed on websites that
                  reflect the advertisers' point of view. Val must then ask if hosting ads is limiting
                  a balanced point of view. Sponsorships can create a conflict of interest and indicate
                  a tendency for a website to provide selective information and not all of the facts
                  that you would find on an impartial site. However, a site established to provide only
                  one point of view does not make it worthless to you as a researcher. In fact, it might
                  contain valuable up-to-date information. Be vigilant about the validity of the information
                  provided and make sure you cross check information. All sites have some level of bias.
                  It is Val's job to determine if bias affects the validity of the information.<br>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a href="http://sofiesartstudio.net/sasink/tekstletters.html" target="_blank">Sofie's Art Studio</a>. Used under a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/" target="_blank">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p align="center">&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities9.html#">return to top</a> | <a href="Module_Three_Sans_Activities8.html">previous page</a> | <a href="Module_Three_Sans_Activities10.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities9.pcf">©</a>
      </div>
   </body>
</html>