<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 17 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_three/module_three_sans_activities17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_three/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_three/">Module Three</a></li>
               <li>Page 17</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Consistency</font>
                     </strong>
                  
               </p>
               
               <p><br>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/C%20Tattoo.jpg" hspace="30" width="102" align="left" height="106" vspace="10" alt="C Tattoo.jpg" border="0"> <a class="textpopper" href="Module_Three_Sans_Activities17.html#" onmouseover="return overlib('Conformity in the application of something, typically that which is necessary for the sake of logic, accuracy, or fairness', CAPTION, 'consistency', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('Conformity in the application of something, typically that which is necessary for the sake of logic, accuracy, or fairness', CAPTION, 'consistency', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">Consistency</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation9.html" border="0"> is the last test for evaluating a website. It compares information from different
                  sources to determine if the information has some <a class="textpopper" href="Module_Three_Sans_Activities17.html#" onmouseover="return overlib('overall sameness, homogeneity, or regularity', CAPTION, 'uniformity', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('overall sameness, homogeneity, or regularity', CAPTION, 'uniformity', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">uniformity</a><img src="http://valenciacollege.edu/library/tutorials/module_three/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation10.html" border="0">. Consistency answers the <strong>WHAT</strong> question. What information is given? Does the site offer both pros and cons regarding
                  a particular subject? Does the information provided generally coincide with other
                  authoritative sources? If a site does not provide information on both sides of an
                  argument, it should at least be a reputable source that provides verifiable information.
                  <a href="http://www.madd.org/" target="_blank">Mothers Against Drunk Driving</a> provides statistics to support the law that drinking should be illegal for individuals
                  under the age of 21. While the site can be seen as having some bias, the information
                  provided can be found and verified on other legitimate sites. Check the related links
                  provided by the website. The links should go to other authoritative sites. Val needs
                  websites that have a clear authority and where the information provided has similar
                  information to information she already found in the books and database articles.
               </p>
               
               <p>The two websites below were sites that Val found interesting. The first site, <a href="http://tattoos.com/" target="_blank">tattoos.com</a>, not only looks biased, Val is not sure that any of the information comes from a
                  reasonable source. Val also questions if the information is any way consistent with
                  the information from her prior research.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img class="resizable" src="http://valenciacollege.edu/library/tutorials/module_three/Commercial%20Site.png" hspace="30" longdesc="ada_files/ada_image8.html" width="838" height="774" vspace="10" alt="Commercial Site.png" border="0">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">(Photo credit: <a href="http://www.designtattooideas.com/live-love-laugh-tattoo/" target="_blank">Design.Tattoo.Ideas</a>. Used under a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/" target="_blank">Creative Commons Attribution-Non-Commercial-NoDerivs 2.0 Generic license</a>)
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Three_Sans_Activities17.html#">return to top</a> | <a href="Module_Three_Sans_Activities16.html">previous page</a> | <a href="Module_Three_Sans_Activities18.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_three/module_three_sans_activities17.pcf">©</a>
      </div>
   </body>
</html>