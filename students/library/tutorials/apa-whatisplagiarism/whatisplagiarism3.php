<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Intellectual Property or Why is Plagiarism a Big Deal? | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-whatisplagiarism/whatisplagiarism3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-whatisplagiarism/">Apa Whatisplagiarism</a></li>
               <li>Intellectual Property or Why is Plagiarism a Big Deal?</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="intellectualpropertyorwhyisplagiarismabigdeal?" id="intellectualpropertyorwhyisplagiarismabigdeal?"></a><h2>Intellectual Property or <em>Why is Plagiarism a Big Deal?</em>
                  
               </h2>
               
               <p>The <em>Oxford English Dictionary</em> defines plagiarism as follows: "The action or practice of taking someone else's work,
                  idea, etc., and passing it off as one's own; literary theft."
               </p>
               
               <p>The idea of plagiarism as theft is an important one in American academic culture;
                  it stems from the concept of intellectual property.
               </p>
               
               <p>Western cultures, such as those of the United States, Canada, the United Kingdom,
                  and Australia, commonly value intellectual property. Intellectual property is the
                  idea that a person or organization can own a idea, a phrase, an essay, a song, a particular
                  photograph, etc. Ideas can be owned even if the owner decides to make the idea freely
                  available on the Internet. For instance a band might decide to release a video of
                  a particular song on YouTube. Despite making that video available the band still owns
                  the content, and people finding that video on YouTube cannot claim ownership or use
                  that video any way they please. If you took a video from YouTube, put it on your own
                  web site and tried to sell it, you would find yourself in court defending yourself
                  against a law suit.
               </p>
               
               <p>Watch this 4 minute video on why plagiarism is such a big deal in college and the
                  workplace.
               </p>
               
               <p>
                  <font color="#C00000">Note: This video may take a moment to load.</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <span id="inlinemedia40">
                     <iframe src="http://digital.films.com/play/NWDWD4" width="600" height="400"></iframe>
                     </span>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whatisplagiarism3.html#">return to top</a> | <a href="whatisplagiarism2.html">previous page</a> | <a href="whatisplagiarism4.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-whatisplagiarism/whatisplagiarism3.pcf">©</a>
      </div>
   </body>
</html>