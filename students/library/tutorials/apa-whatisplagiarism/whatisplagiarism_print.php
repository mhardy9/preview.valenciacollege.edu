<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>What is Plagiarism?: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-whatisplagiarism/whatisplagiarism_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-whatisplagiarism/">Apa Whatisplagiarism</a></li>
               <li>What is Plagiarism?:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>What is Plagiarism?:</strong><br>To Cite or Not to Cite? Part 1 of 5
               </p>
               
               
               <h2>What is Plagiarism?</h2>
               
               <p>Learning Outcome: By the end of the lesson, the student will be able to</p>
               
               <ul>
                  
                  <li>Define plagiarism</li>
                  
                  <li>Recognize the consquences of plagiarism</li>
                  
                  <li>Identify instances of plagiarism</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p><strong>Approximate Completion Time</strong>: 15 minutes
               </p>
               
               <p>
                  <font color="#ff0000">
                     <strong>**To receive credit for completing this tutorial you must complete the online assessment
                        provided by your instructor.**</strong>
                     </font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p></p>
               
               <p>&nbsp;</p>
               
               <p>This tutorial works best in Mozilla Firefox.</p>
               
               <p>It cannot save your place, so please take note of the approximate completion time
                  above and either work on it when you have time to finish or keep track of where you
                  were.
               </p>
               
               <p>&nbsp;</p>
               
               <hr>
               
               <p>
                  <strong>If you encounter technical problems, please copy the error message and send it with
                     a description of the problem to lking@valenciacollege.edu</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Defining Plagiarism</h2>
               
               <p>Plagiarism is using someone else's words, ideas, pictures or other original content
                  without acknowledgment.
               </p>
               
               <p>Students often think that it is not a big deal to</p>
               
               <ul>
                  
                  <li>cut and paste a few sentences from a web site,</li>
                  
                  <li>use a picture found freely available on a web site in a paper,</li>
                  
                  <li>or paraphrase someone's else idea</li>
                  
               </ul>
               
               <p>without acknowledging the source of the words, picture or idea.</p>
               
               <p>But all of these uses are plagiarism, and plagiarism can have severe consequences.</p>
               
               <p>At Valencia (and most colleges in the United States) plagiarism is considered academic
                  dishonesty.
               </p>
               
               <p><font color="#C00000"><strong>You will notice throughout this tutorial we cite all images and words that are not
                        our own.</strong></font> The figure and its citation below are examples of following the rules for citation.
               </p>
               
               <p>
                  <img vspace="10" alt="academic.jpg" height="253" src="http://valenciacollege.edu/library/tutorials/apa-whatisplagiarism/academic.jpg" class="resizable" hspace="30" width="800" border="0" metadata="obj23">
                  
               </p>
               
               <p><em>Figure 1.</em> Image of Academic Dishonesty Policy Statement. Adapted from Valencia College. Office
                  of Policy and General Counsel. (2007, December 11). <em>Policy: <span><font color="black">6Hx28:8-11:</font></span></em></p>
               
               <p>
                  <em>Academic dishonesty policy statement. Retrieved from http://valenciacollege.edu/generalcounsel/policy/documents/8-11-Academic-Dishonesty.pdf</em>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>The full text of Valencia's Policy for Academic Dishonesty can be found here.</p>
               
               <p>&nbsp;</p>
               
               <p>The possible penalties for academic dishonesty at Valencia include:</p>
               
               <ul>
                  
                  <li>"loss of credit for an assignment,"</li>
                  
                  <li>"withdrawal from course,"</li>
                  
                  <li>"reduction in the course grade,"</li>
                  
                  <li>"a grade of 'F' in the course,"</li>
                  
                  <li>"probation, suspension and/or expulsion from the College" (Valencia College, 2007).</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Intellectual Property or <em>Why is Plagiarism a Big Deal?</em>
                  
               </h2>
               
               <p>The <em>Oxford English Dictionary</em> defines plagiarism as follows: "The action or practice of taking someone else's work,
                  idea, etc., and passing it off as one's own; literary theft."
               </p>
               
               <p>The idea of plagiarism as theft is an important one in American academic culture;
                  it stems from the concept of intellectual property.
               </p>
               
               <p>Western cultures, such as those of the United States, Canada, the United Kingdom,
                  and Australia, commonly value intellectual property. Intellectual property is the
                  idea that a person or organization can own a idea, a phrase, an essay, a song, a particular
                  photograph, etc. Ideas can be owned even if the owner decides to make the idea freely
                  available on the Internet. For instance a band might decide to release a video of
                  a particular song on YouTube. Despite making that video available the band still owns
                  the content, and people finding that video on YouTube cannot claim ownership or use
                  that video any way they please. If you took a video from YouTube, put it on your own
                  web site and tried to sell it, you would find yourself in court defending yourself
                  against a law suit.
               </p>
               
               <p>Watch this 4 minute video on why plagiarism is such a big deal in college and the
                  workplace.
               </p>
               
               <p>
                  <font color="#C00000">Note: This video may take a moment to load.</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Famous Plagiarism Cases</h2>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td width="427px">
                        
                        <h3 align="center">Famous Plagiarism Cases</h3>
                        
                     </td>
                     
                     <td width="316px">
                        
                        <h3 align="center">Photograph of Katy Perry</h3>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="427px">
                        
                        <p>Accusations of plagiarism come up frequently in politics, journalism, music, movies
                           and other areas as well as education.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Some famous people who have been accused of plagiarism are:</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Jayson Blair, former New York Times columnist.</p>
                        
                        <p>Doris Kearns Goodwin, historian.</p>
                        
                        <p>James Cameron, film director.</p>
                        
                        <p>Kaavya Viswanathan, Harvard student writer.</p>
                        
                        <p>Australian band Men at Work.</p>
                        
                        <p>Katy Perry, singer.</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>A Google news search on "plagiarism" is sure to turn up hundreds of articles.&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="316px">
                        
                        <p align="center"><img vspace="10" alt="A photograph of singer Katy Perry performing a song." height="500" src="http://valenciacollege.edu/library/tutorials/apa-whatisplagiarism/katyperry.jpg" class="resizable" hspace="30" width="400" border="0" metadata="obj1" longdesc="A photograph of singer Katy Perry performing a song."></p>
                        
                        <p><em>Figure 2.</em> Photo of Katy Perry. Reprinted from Defoe, D. (2008, July 18). <em>Katy Perry</em> [Photo]. Retrieved from http://www.flickr.com/photos/thetoad01/2680949005/ Used under
                           a Creative Commons Attribution-NonCommerical 2.0 Generic license.
                        </p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Examples of Plagiarism</h2>
               
               <p>Copying phrases without citing.</p>
               
               <p>Copying sentences without citing.</p>
               
               <p>Paraphrasing without citing. (Putting an idea into your own words.)</p>
               
               <p>Summarizing without citing. (Giving an overview of another person's ideas.)</p>
               
               <p>Copying charts, graphs or videos to use in your presentation without citing.</p>
               
               <p>Reusing an assignment you created for a previous class in your current class without
                  permission from both instructors (Self-plagiarism).
               </p>
               
               <p>Copying an entire paper.</p>
               
               <p>Purchasing an entire paper from a web site.</p>
               
               <p>Sometimes asking a friend/relative/classmate for help on your paper can also result
                  in plagiarism, depending on how much and what kind of help that person gives you.
               </p>
               
               <p>Bottom line: If you use someone else's words, ideas, or creative work without citing
                  it, you have plagiarized.
               </p>
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <h2>Why is Plagiarism a Big Deal? Part II</h2>
               
               <p>Coming back to the classroom environment at Valencia, if you understand that your
                  professors think of plagiarism as literary theft, you can understand why plagiarism
                  is considered academic dishonesty and has such severe consequences. College assignments
                  are meant to prepare you for the types of challenges you will meet in your professional
                  lives. Students need to be able to think critically about information and make sense
                  of it independently to be successful in college and the workplace.
               </p>
               
               <p>Watch this 4 minute video clip that will further introduce the concepts of plagiarism
                  and intellectual property.
               </p>
               
               <p>
                  <font color="#C00000">Note: This video may take a moment to load.</font>
                  
               </p>
               
               <p></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Sources Used in this Module - Cited in APA Format</h2>
               
               <p align="center">References</p>
               
               <p>&nbsp;</p>
               
               <p>Films Media Group.&nbsp;(2011).&nbsp;<em>Plagiarism 2.0: Information ethics in the digital age</em>&nbsp;[H.264]. Retrieved from http://digital.films.com&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>Plagiarism. Def. 1. (2009). In <em>Oxford English dictionary online</em>. Retrieved from http://www.oup.com&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>Valencia College. Office of Policy and General Counsel. (2007, December 11). <em>Policy: <span><font color="black">6Hx28:8-11:</font></span> Academic dishonesty policy statement.</em></p>
               
               <p>&nbsp;</p>
               
               <p style="margin-left: 40.0">Retrieved from http://valenciacollege.edu/generalcounsel/policy/documents/8-11-Academic-Dishonesty.pdf</p>
               
               <p style="margin-left: 40.0"></p>
               
               <p align="center"></p>
               
               <h2>Assessment</h2>
               
               <p>
                  <strong>
                     <font color="#ff0000">Your instructor will provide the link to the online assessment. You must take the
                        assessment to receive credit for completing this tutorial.</font>
                     </strong>
                  
               </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-whatisplagiarism/whatisplagiarism_print.pcf">©</a>
      </div>
   </body>
</html>