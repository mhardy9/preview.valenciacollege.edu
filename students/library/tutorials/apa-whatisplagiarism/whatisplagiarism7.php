<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sources Used in this Module - Cited in APA Format | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-whatisplagiarism/whatisplagiarism7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-whatisplagiarism/">Apa Whatisplagiarism</a></li>
               <li>Sources Used in this Module - Cited in APA Format</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="sourcesusedinthismodule-citedinapaformat" id="sourcesusedinthismodule-citedinapaformat"></a><h2>Sources Used in this Module - Cited in APA Format</h2>
               
               <p align="center">References</p>
               
               <p>&nbsp;</p>
               
               <p>Films Media Group.&nbsp;(2011).&nbsp;<em>Plagiarism 2.0: Information ethics in the digital age</em>&nbsp;[H.264]. Retrieved from http://digital.films.com&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>Plagiarism. Def. 1. (2009). In <em>Oxford English dictionary online</em>. Retrieved from http://www.oup.com&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>Valencia College. Office of Policy and General Counsel. (2007, December 11). <em>Policy: <span><font color="black">6Hx28:8-11:</font></span> Academic dishonesty policy statement.</em></p>
               
               <p>&nbsp;</p>
               
               <p style="margin-left: 40.0px">Retrieved from http://valenciacollege.edu/generalcounsel/policy/documents/8-11-Academic-Dishonesty.pdf</p>
               
               <p style="margin-left: 40.0px"></p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whatisplagiarism7.html#">return to top</a> | <a href="whatisplagiarism6.html">previous page</a> | <a href="whatisplagiarism8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-whatisplagiarism/whatisplagiarism7.pcf">©</a>
      </div>
   </body>
</html>