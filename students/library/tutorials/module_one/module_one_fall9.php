<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Step Three: Brainstorming Research Questions | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall9.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Step Three: Brainstorming Research Questions</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="stepthree:brainstormingresearchquestions" id="stepthree:brainstormingresearchquestions"></a><h2>Step Three: Brainstorming Research Questions</h2>
               
               <p>
                  <img align="right" src="http://valenciacollege.edu/library/tutorials/module_one/notebook2.JPG" width="325" alt="notebook2.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj10">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <em>On Thursday morning, Professor Sage approves Val's focused topic. To Val's relief,
                     the lesson is about forming a research question - the part she didn't understand before.
                     As she takes out her notebook, she begins to relax just a little.</em>
                  
               </p>
               
               <p>Professor Sage explains: "When you do research in your personal life, it's usually
                  because there's a question you need to answer. For example, Which car is the best
                  buy? Should I see a doctor?
               </p>
               
               <p>"Researching for a college paper is the same way. Ask a question that you would like
                  to know about the topic. Your research will help you answer the question."
               </p>
               
               <p>A good research question is <strong><u><a class="textpopper" href="Module_One_Fall9.html#" onmouseover="return overlib('A type of question that includes factors such as which people, problems, places or times the question refers to.', CAPTION, 'Well-defined Research Question', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('A type of question that includes factors such as which people, problems, places or times the question refers to.', CAPTION, 'Well-defined Research Question', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">well-defined</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation6.html" border="0"></u></strong> and <strong><u><a class="textpopper" href="Module_One_Fall9.html#" onmouseover="return overlib('A type of question that does not have an obvious answer and cannot be answered by a single fact or figure. It may sometimes take the form of a comparison, cause /effect, or problem/ solution.', CAPTION, 'Open-ended Research Question', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('A type of question that does not have an obvious answer and cannot be answered by a single fact or figure. It may sometimes take the form of a comparison, cause /effect, or problem/ solution.', CAPTION, 'Open-ended Research Question', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">open-ended</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation7.html" border="0"></u></strong>. The following pages will describe these concepts in more detail.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <font size="2" style="line-height: 1">&nbsp;Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall9.html#">return to top</a> | <a href="Module_One_Fall8.html">previous page</a> | <a href="Module_One_Fall10.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall9.pcf">©</a>
      </div>
   </body>
</html>