<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Conclusion | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall27.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Conclusion</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="conclusion" id="conclusion"></a><h2>Conclusion</h2>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/info1.JPG" width="100" alt="info1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj16"> <strong>This concludes Tutorial #1: Defining an Information Need.</strong> <strong>In this module, you learned:</strong></p>
               
               <ul>
                  
                  <li>The meaning of the term "information need"</li>
                  
                  <li>The steps taken to clarify and describe an information need: 1. Gather background
                     information, 2. Focus the topic, 3. Choose a research question, 4. Identify key terms,
                     5. Identify a variety of appropriate sources.
                  </li>
                  
                  <li>The reasons for each of these steps</li>
                  
               </ul>
               
               <p>
                  <strong>You also acquired the following skills:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Identify characteristics and examples of general information sources.</li>
                  
                  <li>Arrange topics from broad to narrow.</li>
                  
                  <li>Identify characteristics and examples of open-ended research questions.</li>
                  
                  <li>Identify characteristics and examples of well-defined research questions.</li>
                  
                  <li>Identify characteristics and examples of current vs. historical, primary vs. secondary,
                     and popular vs.scholarly sources.
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               
               
               
               <div id="report" align="center">
                  
                  <p>After finishing this lesson, complete the form below:</p>
                  
                  <table class="table ">
                     
                     <tr>
                        <td>
                           
                           <form name="send_form" method="" action="" onsubmit="" id="send_form">
                              
                              <h2>Type your name or identifier:</h2>
                              
                              <p><input type="text" name="user_name" size="40"></p>
                              
                              <hr>
                              <input type="button" onclick="parent.lessonReport('summary')" value="Print Score Summary" name="PS">
                              
                           </form>
                           
                        </td>
                     </tr>
                     
                  </table>
                  
               </div>
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall27.html#">return to top</a> | <a href="Module_One_Fall26.html">previous page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall27.pcf">©</a>
      </div>
   </body>
</html>