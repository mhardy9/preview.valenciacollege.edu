<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information Literacy 1 - Defining an Information Need | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Information Literacy 1 - Defining an Information Need</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Information Literacy 1 - Defining an Information Need</strong><br>Valencia College Libraries
               </p>
               
               
               <h2>
                  <strong>
                     <u>Module 1</u>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong>
                  
               </p>
               
               <p>
                  <img align="right" src="http://valenciacollege.edu/library/tutorials/module_one/val_altb1.JPG" width="325" alt="val_altb1.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj17">
                  
               </p>
               
               <p>
                  <strong>
                     <font size="5">Scene 1</font>
                     </strong>
                  
               </p>
               
               <p>It's the first day of a new semester, and Val, a nineteen-year-old with shoulder length
                  hair, nervously takes a seat in Professor Sage's Comp I class. Professor Sage enters
                  the classroom and the other students settle into their seats. After a brief discussion
                  of the syllabus, Professor Sage begins the work of the class.
               </p>
               
               <p><strong>PROFESSOR SAGE:</strong> Now, I like to know about my students' interests. Are there any writers in the class?
               </p>
               
               <p>(A couple of hands go up, shyly)</p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>Great! You'll all be writers by the end of this course. We're going to work toward
                  writing a basic 5-paragraph essay, including a thesis statement, introduction, supporting
                  paragraphs, conclusion, and MLA list of Works Cited.
               </p>
               
               <p>(Groans are heard from the back of the room)</p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>(Waving a hand) Alright, alright, don't worry; we're going to take this process in
                  steps. Starting today, we'll be working on your first assignment, which is to write
                  an annotated bibliography. That's a list of articles, books and web sites about your
                  topic, with a summary of each one. (She begins handing out an assignment sheet to
                  the class)
               </p>
               
               <p>Next to Val, Matt, a thirty-five-year-old student wearing an Army t-shirt, raises
                  his hand.
               </p>
               
               <p><strong>MATT:</strong> What does the topic have to be?
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>I'm glad you asked that. I'd encourage you to pick a topic you're personally interested
                  in, since you'll be using this topic all semester long to develop your work and eventually
                  write a full length essay. In fact, your homework for Thursday will be to select a
                  topic with which to work.
               </p>
               
               <p>For the first time, Val feels a little bit excited about the class. She knows immediately
                  what she wants to write about – tattoos. For a while now, she's been considering getting
                  one ... her thoughts wander off. Professor Sage continues:
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>Once I've approved your topic, you'll narrow it down to an appropriate research question.
                  Then next week, we'll meet in the library, and the librarian will show us how to research
                  the answers to our questions. (Glancing up at the clock) That's about it for today.
                  Have a great day!
               </p>
               
               <p>The shuffling of papers snaps Val out of her daydream. What did Professor Sage say
                  about a research question? Her brain reeling with information about the class, the
                  homework assignment, the task of writing a full-length essay and her busy new college
                  schedule, Val tucks her assignment sheet into her bag and exits.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">&nbsp;Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Introduction: A Need for Information</h2>
               
               <p>&nbsp;</p>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/info1.JPG" width="100" alt="info1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj18"> The first step in research is to <strong>define</strong>, or clarify, and <strong>describe</strong> the information needed. &nbsp; &nbsp; Val has already chosen a topic for her paper - tattoos.
                  She has some personal knowledge about this topic, but she will soon realize that there
                  is a lot she does not know.
               </p>
               
               <p>The information Val does not yet know, and needs to find out, about tattoos is called
                  her <strong><u>information need</u>.</strong></p>
               
               <p>
                  <strong>
                     <u>This module explains the steps Val took to clarify and describe her information need.
                        Be sure to complete all the activities so that you can practice these steps, too.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Step One: Background Information</h2>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/typing1.JPG" width="325" alt="typing1.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj4"> Val starts her research by searching the word "tattoos" in Google.
               </p>
               
               <p>She looks for some background information about the topic. Basically, she hopes to
                  find some history and an overview of some of the issues regarding tattoos.
               </p>
               
               <p>She finds what she is looking for in an online <strong>encyclopedia</strong>.
               </p>
               
               <p>An encyclopedia is one example of a <strong>general information source.</strong></p>
               
               <p>General information sources provide broad coverage of a variety of topics. They include
                  encyclopedias and dictionaries, and are a good place to begin finding background on
                  a topic.
               </p>
               
               <p>As Val reads more about tattoos, she realizes that there is more to this topic than
                  she thought! She begins to feel overwhelmed...
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: Finding background information has helped Val clarify her information
                        need by helping her realize that her current knowledge is limited compared to all
                        there is to know about tattoos.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Step Two: Focusing the Topic</h2>
               
               <p>Val is suddenly unsure if she can really do a paper on tattoos. There are so many
                  angles to this topic! Since she's a visual learner, Val draws the following Concept
                  Map to illustrate the possible directions her topic could take:
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <img src="https://lh3.googleusercontent.com/Lg291wH_DOolSITSzvtV25xwbOf4FavbjC2IPzbIoSwop9xbVKH2ZbLSCJkiUqkE_QyNGS5NQ7mDqbuCdwIMMFCtm1Kz4ckH8i0iAZl6rMrhP3bOqU4" width="641px;" height="341px;">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>She realizes that she needs to pick just one of the possible topics from her concept
                  map; otherwise, her research project will be too large for the assignment. She decides
                  that she will stick to the cultural significance of tattoos.
               </p>
               
               <p>What Val has just done is known as <strong><u>narrowing, or focusing, the topic</u></strong>.
               </p>
               
               <p>In the activity on the following page, practice ranking the topic ideas from broad
                  to narrow.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Activity: Broad to Narrow Topics</h2>
               
               <p>&nbsp;</p>
               
               <p align="left">&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Check your answers:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>"Tattoos" <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Tattooing in Indian culture" <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Henna tattooing in Indian culture" <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Dyes and pigments used in Indian Henna tattooing."</li>
                  
               </ul>
               
               <p>Note how Val added detail to the topic to make it narrower each time. Researching
                  the dyes and pigments used in Henna tattooing would be a narrower focus than researching
                  everything there is to know about Henna tattooing.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>A topic can also be narrowed by focusing on <strong>specific examples</strong>. Try this more challenging activity. When it is completed correctly, each topic idea
                  should be an example of the one above it. &nbsp;
               </p>
               
               <p>
                  <strong>Activity: Broad to Narrow Topics II</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center"></p>
               
               <p>Check your answers:</p>
               
               <ul>
                  
                  <li>"Cultural customs" <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Rituals" (a type of custom) <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Initiation Ceremonies" (an example of a ritual) <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Secret Society Inductions" (a specific kind of initiation ceremony)</li>
                  
               </ul>
               
               <p>If Val researched tattooing in secret society inductions, she would have a narrower
                  focus than if she researched tattooing in all types of initiation ceremonies.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: Focusing the topic helps Val clarify her information need. She now
                        knows that she will only need to find information about one aspect of her topic -
                        the cultural significance of tattoos.</u>
                     </strong>
                  
               </p>
               
               <p>Answer the following quiz question to check your understanding of this concept.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_one/quizme_custom.png" alt="Toggle open/close quiz question" border="0" title="Question 1"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Step Three: Brainstorming Research Questions</h2>
               
               <p>
                  <img align="right" src="http://valenciacollege.edu/library/tutorials/module_one/notebook2.JPG" width="325" alt="notebook2.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj10">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <em>On Thursday morning, Professor Sage approves Val's focused topic. To Val's relief,
                     the lesson is about forming a research question - the part she didn't understand before.
                     As she takes out her notebook, she begins to relax just a little.</em>
                  
               </p>
               
               <p>Professor Sage explains: "When you do research in your personal life, it's usually
                  because there's a question you need to answer. For example, Which car is the best
                  buy? Should I see a doctor?
               </p>
               
               <p>"Researching for a college paper is the same way. Ask a question that you would like
                  to know about the topic. Your research will help you answer the question."
               </p>
               
               <p>A good research question is <strong><u>well-defined</u></strong> and <strong><u>open-ended</u></strong>. The following pages will describe these concepts in more detail.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <font size="2">&nbsp;Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/question1.JPG" width="100" alt="question1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj13"> A <strong>well-defined research question</strong> is a type of question that includes factors such as which people, problems, places
                  or times the question refers to.
               </p>
               
               <p>For example, "What are the risks of tattoos?" is NOT a very well-defined research
                  question. It doesn't provide the researcher any frame of reference.
               </p>
               
               <p>A better question might be, "What are the medical risks of tattoos among young people
                  today?" This question specifies factors such as what kinds of risks (medical), which
                  people (young people) and what time period (today) is being researched. This gives
                  the researcher a clear direction.
               </p>
               
               <p>In the activity on the following page, help Val determine which research questions
                  are well-defined.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Activity: Well-Defined Research Questions</strong>
                  
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><strong>Activity: Well-Defined Research Questions:</strong> Check your answers:
               </p>
               
               <ul>
                  
                  <li>What percent of Americans under age 24 have tattoos? <strong>Well-defined. This question specifies "who" (Americans under age 24).</strong>
                     
                  </li>
                  
                  <li>What are people's different perceptions of tattoos? <strong>Not Well-defined. This question does not specify which "people" are being researched.</strong>
                     
                  </li>
                  
                  <li>How do perceptions of tattoos compare in Judaism and Christianity? <strong>Well-defined. This question specifies which groups are being researched (Judaism and
                        Christianity).</strong>
                     
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/question1.JPG" width="100" alt="question1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj14"> An <strong>open-ended research question</strong> is a type of question that does not have an obvious answer and cannot be answered
                  by a single fact or figure. It may sometimes take the form of a comparison, cause
                  /effect, or problem/ solution.
               </p>
               
               <p>For example, the question, "Does Starbucks ban employees from wearing tattoos?" is
                  NOT open-ended. The answer is either yes, or it is no. There is no room for further
                  study of the question.
               </p>
               
               <p>On the other hand, the question, "How do attitudes about tattoos compare between Starbucks
                  and other area employers?" is open-ended. This particular example takes the form of
                  a comparison. The answer is not obvious, so research must be done to find examples
                  of tattoo policies among area employers. There is much information to be found on
                  this topic.
               </p>
               
               <p>In the activity on the following page, help Val determine which questions are open-ended.</p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Activity: Open-Ended Research Questions</h2>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><strong>Activity: Open-Ended Research Questions:</strong> Check your answers:
               </p>
               
               <ul>
                  
                  <li>What percent of Americans under age 24 have tattoos? <strong>Not open-ended. This question can be answered by a single fact.</strong>
                     
                  </li>
                  
                  <li>What are people's different perceptions of tattoos? <strong>Open-ended. This question cannot be answered by a single fact.</strong>
                     
                  </li>
                  
                  <li>How do perceptions of tattoos compare in Judaism and Christianity? <strong>Open-ended. This question cannot be answered by a single fact. It takes the form of
                        a comparison.</strong>
                     
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>As you may have noticed from the activities, it is possible for a research question
                  to be well-defined but not open-ended, or open-ended but not well-defined. The best
                  research questions are both open-ended AND well-defined.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: Choosing a research question helps Val clarify her information need
                        because she now knows that she needs to find information that answers her question.</u>
                     </strong>
                  
               </p>
               
               <p>Answer the following quiz question to check your understanding of this concept.</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_one/quizme_custom.png" alt="Toggle open/close quiz question" border="0" title="Question 2"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Step Four: Key Terms</h2>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/search2.JPG" width="100" alt="search2.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj6"> Val has decided to research the question, <strong>"How do perceptions of tattoos compare in Eastern religions, Judaism and Christianity?"</strong></p>
               
               <p>She types this question into Google, but gets only a few results. So, she changes
                  her search a little:
               </p>
               
               <ul>
                  
                  <li><em>perceptions tattoos Judaism Christianity Eastern religions</em></li>
                  
               </ul>
               
               <p>More results appear, but at first glance, they don't seem to help much. Frustrated,
                  Val makes one more change:
               </p>
               
               <ul>
                  
                  <li>
                     <strong><em>beliefs</em></strong> <em>tattoos Judaism Christianity Eastern religions</em>
                     
                  </li>
                  
               </ul>
               
               <p>Better! Val realizes that changing her search words helps her find more <strong>relevant</strong> search results. Quickly, she begins listing words she can use. The web sites she
                  has found so far give her some ideas, too.
               </p>
               
               <ul>
                  
                  <li><em>tattoos, beliefs, perceptions, ideologies, stigma, religions, Judaism, Christianity,
                        Eastern religions, Buddhism, spirituality</em></li>
                  
               </ul>
               
               <p>
                  <strong>
                     <u>The bottom line: Making a list of key words helps Val describe her information need
                        because she can now use many terms and phrases to research her question.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Step Five: The Right Tools</h2>
               
               <p>
                  <img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/puzzle1.JPG" width="100" alt="puzzle1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj15">
                  
               </p>
               
               <p>After searching a few web sites, Val wonders if the information she wants is even
                  out there. Suddenly, Val remembers something else Professor Sage said on the first
                  day of class - the annotated bibliography would include books, articles <em>and</em> web sites. She digs out her assignment sheet. The assignment says:
               </p>
               
               <p>
                  <font face="Georgia">"Include in your annotated bibliography a variety of source types, including <strong>current</strong> and <strong>historical</strong>, <strong>primary</strong> and <strong>secondary</strong>. &nbsp; Include <strong>newspapers</strong>, <strong>popular</strong> <strong>magazines</strong> and <strong>scholarly</strong> <strong>journals</strong> as well as books and web sites. You are also encouraged to use a variety of media,
                     such as <strong>Internet</strong>, <strong>audiovisual</strong> and <strong>print</strong> sources."</font>
                  
               </p>
               
               <p>Some of the source types mentioned in the assignment are familiar to Val and some
                  are not. In the three activities on the following pages, help Val brush up her knowledge.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2 align="left">Activity: Current vs. Historical</h2>
               
               <p align="left">Before completing this activity, review the definitions of <strong>current</strong> and <strong>historical.</strong></p>
               
               <ul>
                  
                  <li>Current sources provide information about events happening recently, and are up-to-date.</li>
                  
                  <li>Historical sources are either artifacts from past events or give information about
                     past events.
                  </li>
                  
               </ul>
               
               <p align="left">&nbsp;&nbsp;</p>
               
               <h2 align="left">&nbsp;</h2>
               
               <p align="center"></p>
               
               <p align="left"><strong>Activity: Current vs. Historical:</strong> Check your answers:
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <ul>
                  
                  <li>Breaking television news report on tattoo safety: <strong>Current (this is breaking news)</strong>
                     
                  </li>
                  
                  <li>Statistics on the number of Americans with tattoos, gathered from 1960: <strong>Historical (statistics were gathered long ago)</strong>
                     
                  </li>
                  
                  <li>Today's blog entry from Val's favorite tattoo artist: <strong>Current (the information is from today)</strong>
                     
                  </li>
                  
                  <li>Diary entry of a Holocaust victim, written in 1940: <strong>Historical (the diary entry was written many years ago)</strong>
                     
                  </li>
                  
                  <li>Recent scientific study of tattoo inks: <strong>Current (the study is recent)</strong>
                     
                  </li>
                  
                  <li>3,000-year-old tattooed mummies at a local museum: <strong>Historical (this is a historical artifact)</strong>
                     
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Activity: Primary vs. Secondary</strong>
                  
               </p>
               
               <p align="left">Before completing this activity, review the definitions of primary and secondary.</p>
               
               <ul>
                  
                  <li>Primary sources provide original, unfiltered information. Diaries, artifacts, personal
                     correspondences and raw data are some examples.
                  </li>
                  
                  <li>Secondary sources provide information that has been filtered through a secondhand
                     point of view. Encyclopedia articles, summaries and reports are some examples.
                  </li>
                  
               </ul>
               
               <p align="left">&nbsp;&nbsp;</p>
               
               <h2 align="left">&nbsp;</h2>
               
               <p align="center"></p>
               
               <p align="left"><strong>Activity: Primary vs. Secondary:</strong> Check your answers:
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <ul>
                  
                  <li>Personal interview with a friend about what it's like to have a tattoo: <strong>Primary (a personal interview has not been filtered through another person's perspective)</strong>
                     
                  </li>
                  
                  <li>Summary of a book about the cultural significance of tattoos: <strong>Secondary (a summary is a retelling of the information by a second person)</strong>
                     
                  </li>
                  
                  <li>Encyclopedia article on ancient uses of tattoos: <strong>Secondary (an encyclopedia article does not include original artifacts but gives an
                        overview of a subject, collected from many sources)</strong>
                     
                  </li>
                  
                  <li>3,000-year-old tattooed mummies at a local museum: <strong>Primary (these are actual artifacts)</strong>
                     
                  </li>
                  
                  <li>Magazine article reporting on the popularity of tattoos: <strong>Secondary (magazines rarely conduct their own research studies but instead report
                        on research done by others)</strong>
                     
                  </li>
                  
                  <li>Raw data from a survey of students' opinions about tattoos: <strong>Primary (raw data are original numbers that have not been skewed or represented in
                        any particular way)</strong>
                     
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Activity: Popular vs. Scholarly</strong>
                  
               </p>
               
               <p align="left">Before completing this activity, review the definitions of popular and scholarly.</p>
               
               <ul>
                  
                  <li>Popular sources provide information that is meant to inform and entertain general
                     readers. Blogs, Internet forums, and magazines are some examples.
                  </li>
                  
                  <li>Scholarly sources provide information that is written by experts, and is intended
                     to educate scholars and students about a topic.
                  </li>
                  
               </ul>
               
               <p align="left">&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left"><strong>Activity: Popular vs. Scholarly:</strong> Check your answers:
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <ul>
                  
                  <li>Today's blog entry from Val's favorite tattoo artist: <strong>Popular (a blog is intended for information or entertainment rather than education
                        of scholars and students)</strong>
                     
                  </li>
                  
                  <li>Recent scientific study of tattoo inks: <strong>Scholarly ("scientific" indicates the study was done in a scholarly manner)</strong>
                     
                  </li>
                  
                  <li>Magazine article reporting on the popularity of tattoos: <strong>Popular (magazines are intended to inform or entertain rather than scholarly education;
                        magazines are written by journalists rather than scholars of the subject)</strong>
                     
                  </li>
                  
                  <li>Journal article about medical risks of tattooing, by a dermatologist: <strong>Scholarly (journals are intended to educate students and scholars; a dermatologist
                        would be an expert on this topic)</strong>
                     
                  </li>
                  
                  <li>Personal interview with a friend about what it's like to have a tattoo: <strong>Popular (a discussion with a friend would not be intended to educate scholars and
                        students about the topic)</strong>
                     
                  </li>
                  
                  <li>Book about the cultural significance of tattoos by an anthropologist: <strong>Scholarly (an anthropologist would be an expert on this topic)</strong>
                     
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>As she reviews the different types of sources, Val notes that the same sources can
                  be categorized in several ways. She also notices that all of these types of sources
                  can be found in print (hard copy), electronic, or audio-visual media.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: Knowing the tools available helps Val describe her information need
                        because she now knows which kinds of information sources to seek out.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;Please continue to the next slide to <strong>test your knowledge</strong>.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <font size="5">Test Your Knowledge!</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_one/testyourself_custom.png" alt="Toggle open/close quiz question" border="0" title="Question 3"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Conclusion</h2>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/info1.JPG" width="100" alt="info1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj16"> <strong>This concludes Tutorial #1: Defining an Information Need.</strong> <strong>In this module, you learned:</strong></p>
               
               <ul>
                  
                  <li>The meaning of the term "information need"</li>
                  
                  <li>The steps taken to clarify and describe an information need: 1. Gather background
                     information, 2. Focus the topic, 3. Choose a research question, 4. Identify key terms,
                     5. Identify a variety of appropriate sources.
                  </li>
                  
                  <li>The reasons for each of these steps</li>
                  
               </ul>
               
               <p>
                  <strong>You also acquired the following skills:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Identify characteristics and examples of general information sources.</li>
                  
                  <li>Arrange topics from broad to narrow.</li>
                  
                  <li>Identify characteristics and examples of open-ended research questions.</li>
                  
                  <li>Identify characteristics and examples of well-defined research questions.</li>
                  
                  <li>Identify characteristics and examples of current vs. historical, primary vs. secondary,
                     and popular vs.scholarly sources.
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall_print.pcf">©</a>
      </div>
   </body>
</html>