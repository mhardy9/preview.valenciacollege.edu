<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 16 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall16.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Page 16</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>As you may have noticed from the activities, it is possible for a research question
                  to be well-defined but not open-ended, or open-ended but not well-defined. The best
                  research questions are both open-ended AND well-defined.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: Choosing a research question helps Val clarify her information need
                        because she now knows that she needs to find information that answers her question.</u>
                     </strong>
                  
               </p>
               
               <p>Answer the following quiz question to check your understanding of this concept.</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_one/spacer.gif" name="check14" alt="" id="check14"><a id="a14" title="Question 2" href="javascript:parent.toggletable('quizpopper14')"><img src="http://valenciacollege.edu/library/tutorials/module_one/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 2" border="0"></a> 
               </p>
               
               <div id="quizpopper14" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 1</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px;">
                     
                     <form name="f14" id="f14">Which THREE questions would be the most appropriate research questions? Choose the
                        three best answers.
                        <div style="margin: 1em 15px;">
                           <input type="checkbox" name="q14" value="a" id="q14a">&nbsp;<label for="q14a"><strong>a.</strong>&nbsp;What is the meaning of tattoos?</label><br><input type="checkbox" name="q14" value="b" id="q14b">&nbsp;<label for="q14b"><strong>b.</strong>&nbsp;In the state of Florida, what is the legal age to get a tattoo without parental permission?</label><br><input type="checkbox" name="q14" value="c" id="q14c">&nbsp;<label for="q14c"><strong>c.</strong>&nbsp;What were the effects of forced tattooing on survivors of Holocaust concentration
                              camps?</label><br><input type="checkbox" name="q14" value="d" id="q14d">&nbsp;<label for="q14d"><strong>d.</strong>&nbsp;How has the significance of tattoos changed in Egyptian culture from ancient times
                              to today?</label><br><input type="checkbox" name="q14" value="e" id="q14e">&nbsp;<label for="q14e"><strong>e.</strong>&nbsp;What precautions should young people take to reduce the medical risks of tattooing?</label><p></p>
                           <span style="font-size: 90%;">[mark all correct answers]</span>
                           
                        </div>
                        
                        <div align="center"><input onclick="parent.check_q(14, 5, 3, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done14" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed14" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall16.html#">return to top</a> | <a href="Module_One_Fall15.html">previous page</a> | <a href="Module_One_Fall17.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall16.pcf">©</a>
      </div>
   </body>
</html>