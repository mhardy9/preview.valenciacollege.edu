<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Step Two: Focusing the Topic | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Step Two: Focusing the Topic</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="steptwo:focusingthetopic" id="steptwo:focusingthetopic"></a><h2>Step Two: Focusing the Topic</h2>
               
               <p>Val is suddenly unsure if she can really do a paper on tattoos. There are so many
                  angles to this topic! Since she's a visual learner, Val draws the following <a class="textpopper" href="Module_One_Fall4.html#" onmouseover="return overlib('A visual diagram that shows how smaller concepts are related to larger ones. The main topic goes in the center, and more specific aspects of the topic are written in the spokes.', CAPTION, 'Concept Map', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('A visual diagram that shows how smaller concepts are related to larger ones. The main topic goes in the center, and more specific aspects of the topic are written in the spokes.', CAPTION, 'Concept Map', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">Concept Map</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation4.html" border="0"> to illustrate the possible directions her topic could take:
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <img src="https://lh3.googleusercontent.com/Lg291wH_DOolSITSzvtV25xwbOf4FavbjC2IPzbIoSwop9xbVKH2ZbLSCJkiUqkE_QyNGS5NQ7mDqbuCdwIMMFCtm1Kz4ckH8i0iAZl6rMrhP3bOqU4" width="641px;" height="341px;">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>She realizes that she needs to pick just one of the possible topics from her concept
                  map; otherwise, her research project will be too large for the assignment. She decides
                  that she will stick to the cultural significance of tattoos.
               </p>
               
               <p>What Val has just done is known as <strong><u><a class="textpopper" href="Module_One_Fall4.html#" onmouseover="return overlib('Narrowing or Focusing the Topic = Choosing a more specific aspect of a larger topic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Narrowing or Focusing the Topic = Choosing a more specific aspect of a larger topic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">narrowing, or focusing, the topic</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation5.html" border="0"></u></strong>.
               </p>
               
               <p>In the activity on the following page, practice ranking the topic ideas from broad
                  to narrow.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall4.html#">return to top</a> | <a href="Module_One_Fall3.html">previous page</a> | <a href="Module_One_Fall5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall4.pcf">©</a>
      </div>
   </body>
</html>