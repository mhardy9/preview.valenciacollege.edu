<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 23 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall23.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Page 23</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p align="left">
                  <strong>Activity: Popular vs. Scholarly</strong>
                  
               </p>
               
               <p align="left">Before completing this activity, review the definitions of <a class="textpopper" href="Module_One_Fall23.html#" onmouseover="return overlib('Popular = Information meant to inform and entertain general readers. Blogs, Internet forums, and magazines are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Popular = Information meant to inform and entertain general readers. Blogs, Internet forums, and magazines are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">popular</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation23.html" border="0"> and <a class="textpopper" href="Module_One_Fall23.html#" onmouseover="return overlib('Scholarly = Referring to work written by an expert that is intended to educate scholars and students about a topic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Scholarly = Referring to work written by an expert that is intended to educate scholars and students about a topic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">scholarly</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation24.html" border="0">.
               </p>
               
               <ul>
                  
                  <li>Popular sources provide information that is meant to inform and entertain general
                     readers. Blogs, Internet forums, and magazines are some examples.
                  </li>
                  
                  <li>Scholarly sources provide information that is written by experts, and is intended
                     to educate scholars and students about a topic.
                  </li>
                  
               </ul>
               
               <p align="left">&nbsp;</p>
               
               <p>&nbsp; 
                  <a href="Module_One_Fall23.html#endofactivity18"><img title="go to end of activity" src="http://valenciacollege.edu/library/tutorials/module_one/spacer.gif" border="0" width="0" height="0"></a>
                  
               </p>
               
               <table class="table ">
                  
                  <tr valign="middle">
                     
                     <td width="10%"><a href="ada_files/ada_activity18.html" target="_new"><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-access.gif" hspace="3" width="20" height="20" align="middle" title="alternative accessible activity content" longdesc="ada_files/ada_activity18.html" border="0" alt="learning activity"></a></td>
                     
                     <td>In this activity, help Val determine if the information sources are popular or scholarly.
                        Click the card deck to view a card. Drag the card from the bottom to the correct category:
                        either "popular" or "scholarly."
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td colspan="2" align="center">
                        
                        <div id="activity-18" style="width: 550px; height: 446px; border: 1px solid #000000; background-color: #eeeeee;">
                           
                           <div style="width:100%;height:100%;text-align:center;">
                              <table class="table ">
                                 <tr style="width:100%;height:100%;border-width:0px;">
                                    <td style="width:100%;height:100%;border-width:0px;vertical-align:middle;">
                                       <span style="font-weight:bold;">This content requires Flash Player 10 or higher.</span><br><br><a href="http://www.adobe.com/go/getflashplayer" target="_blank"><img src="http://valenciacollege.edu/library/tutorials/module_one/get_flash_player.gif" alt="Get Adobe Flash Player" style="border:0px;"></a>
                                       
                                    </td>
                                 </tr>
                              </table>
                           </div>
                           
                        </div>
                        
                        
                     </td>
                     
                     <td></td>
                     
                  </tr>
                  
               </table>
               
               <div id="endofactivity18"></div>
               &nbsp;
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall23.html#">return to top</a> | <a href="Module_One_Fall22.html">previous page</a> | <a href="Module_One_Fall24.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall23.pcf">©</a>
      </div>
   </body>
</html>