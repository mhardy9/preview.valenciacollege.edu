<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Module 1 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Module 1</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="module1" id="module1"></a><h2>
                  <strong>
                     <u>Module 1</u>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong>
                  
               </p>
               
               <p>
                  <img align="right" src="http://valenciacollege.edu/library/tutorials/module_one/val_altb1.JPG" width="325" alt="val_altb1.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj17">
                  
               </p>
               
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Scene 1</font>
                     </strong>
                  
               </p>
               
               <p>It's the first day of a new semester, and Val, a nineteen-year-old with shoulder length
                  hair, nervously takes a seat in Professor Sage's Comp I class. Professor Sage enters
                  the classroom and the other students settle into their seats. After a brief discussion
                  of the syllabus, Professor Sage begins the work of the class.
               </p>
               
               <p><strong>PROFESSOR SAGE:</strong> Now, I like to know about my students' interests. Are there any writers in the class?
               </p>
               
               <p>(A couple of hands go up, shyly)</p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>Great! You'll all be writers by the end of this course. We're going to work toward
                  writing a basic 5-paragraph essay, including a thesis statement, introduction, supporting
                  paragraphs, conclusion, and MLA list of Works Cited.
               </p>
               
               <p>(Groans are heard from the back of the room)</p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>(Waving a hand) Alright, alright, don't worry; we're going to take this process in
                  steps. Starting today, we'll be working on your first assignment, which is to write
                  an annotated bibliography. That's a list of articles, books and web sites about your
                  topic, with a summary of each one. (She begins handing out an assignment sheet to
                  the class)
               </p>
               
               <p>Next to Val, Matt, a thirty-five-year-old student wearing an Army t-shirt, raises
                  his hand.
               </p>
               
               <p><strong>MATT:</strong> What does the topic have to be?
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>I'm glad you asked that. I'd encourage you to pick a topic you're personally interested
                  in, since you'll be using this topic all semester long to develop your work and eventually
                  write a full length essay. In fact, your homework for Thursday will be to select a
                  topic with which to work.
               </p>
               
               <p>For the first time, Val feels a little bit excited about the class. She knows immediately
                  what she wants to write about – tattoos. For a while now, she's been considering getting
                  one ... her thoughts wander off. Professor Sage continues:
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>Once I've approved your topic, you'll narrow it down to an appropriate research question.
                  Then next week, we'll meet in the library, and the librarian will show us how to research
                  the answers to our questions. (Glancing up at the clock) That's about it for today.
                  Have a great day!
               </p>
               
               <p>The shuffling of papers snaps Val out of her daydream. What did Professor Sage say
                  about a research question? Her brain reeling with information about the class, the
                  homework assignment, the task of writing a full-length essay and her busy new college
                  schedule, Val tucks her assignment sheet into her bag and exits.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">&nbsp;Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall.html#">return to top</a> | <a href="Module_One_Fall2.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall.pcf">©</a>
      </div>
   </body>
</html>