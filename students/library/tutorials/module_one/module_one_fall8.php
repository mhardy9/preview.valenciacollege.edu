<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 8 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall8.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Page 8</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>Check your answers:</p>
               
               <ul>
                  
                  <li>"Cultural customs" <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Rituals" (a type of custom) <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Initiation Ceremonies" (an example of a ritual) <strong>is broader than</strong>
                     
                  </li>
                  
                  <li>"Secret Society Inductions" (a specific kind of initiation ceremony)</li>
                  
               </ul>
               
               <p>If Val researched tattooing in secret society inductions, she would have a narrower
                  focus than if she researched tattooing in all types of initiation ceremonies.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: Focusing the topic helps Val clarify her information need. She now
                        knows that she will only need to find information about one aspect of her topic -
                        the cultural significance of tattoos.</u>
                     </strong>
                  
               </p>
               
               <p>Answer the following quiz question to check your understanding of this concept.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_one/spacer.gif" name="check13" alt="" id="check13"><a id="a13" title="Question 1" href="javascript:parent.toggletable('quizpopper13')"><img src="http://valenciacollege.edu/library/tutorials/module_one/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 1" border="0"></a> 
               </p>
               
               <div id="quizpopper13" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 1</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px;">
                     
                     <form name="f13" id="f13">Which three choices would be appropriate ways to narrow the topic, "medical risks
                        of tattooing"? Choose the best THREE answers.
                        <div style="margin: 1em 15px;">
                           <input type="checkbox" name="q13" value="a" id="q13a">&nbsp;<label for="q13a"><strong>a.</strong>&nbsp;Risks of tattooing</label><br><input type="checkbox" name="q13" value="b" id="q13b">&nbsp;<label for="q13b"><strong>b.</strong>&nbsp;Disease transmission from tattooing instruments</label><br><input type="checkbox" name="q13" value="c" id="q13c">&nbsp;<label for="q13c"><strong>c.</strong>&nbsp;Medical risks of tattooing and piercing</label><br><input type="checkbox" name="q13" value="d" id="q13d">&nbsp;<label for="q13d"><strong>d.</strong>&nbsp;Allergic reactions to tattoo inks</label><br><input type="checkbox" name="q13" value="e" id="q13e">&nbsp;<label for="q13e"><strong>e.</strong>&nbsp;Medical risks of tattooing in third-world countries</label><p></p>
                           <span style="font-size: 90%;">[mark all correct answers]</span>
                           
                        </div>
                        
                        <div align="center"><input onclick="parent.check_q(13, 5, 3, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done13" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed13" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall8.html#">return to top</a> | <a href="Module_One_Fall7.html">previous page</a> | <a href="Module_One_Fall9.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall8.pcf">©</a>
      </div>
   </body>
</html>