<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Step Five: The Right Tools | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall18.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Step Five: The Right Tools</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="stepfive:therighttools" id="stepfive:therighttools"></a><h2>Step Five: The Right Tools</h2>
               
               <p>
                  <img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/puzzle1.JPG" width="100" alt="puzzle1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj15">
                  
               </p>
               
               <p>After searching a few web sites, Val wonders if the information she wants is even
                  out there. Suddenly, Val remembers something else Professor Sage said on the first
                  day of class - the annotated bibliography would include books, articles <em>and</em> web sites. She digs out her assignment sheet. The assignment says:
               </p>
               
               <p>
                  <font face="Georgia">"Include in your annotated bibliography a variety of source types, including <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Current = Referring to events happening recently, up-to-date.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Current = Referring to events happening recently, up-to-date.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">current</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation8.html" border="0"></strong> and <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Referring to artifacts from past events or information about past events.', CAPTION, 'Historical', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onfocus="return overlib('Referring to artifacts from past events or information about past events.', CAPTION, 'Historical', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000', CAPCOLOR, '#ffffff');" onmouseout="nd(0);" onblur="nd(0);">historical</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation9.html" border="0"></strong>, <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Primary = Original, unfiltered information. Diaries, artifacts, personal correspondences and raw data are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Primary = Original, unfiltered information. Diaries, artifacts, personal correspondences and raw data are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">primary</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation10.html" border="0"></strong> and <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Secondary = Information that has been filtered through a secondhand point of view. Encyclopedia articles, summaries and reports are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Secondary = Information that has been filtered through a secondhand point of view. Encyclopedia articles, summaries and reports are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">secondary</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation11.html" border="0"></strong>. &nbsp; Include <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Newspaper = Publication that reports on current events, usually published daily or weekly, can be in print or electronic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Newspaper = Publication that reports on current events, usually published daily or weekly, can be in print or electronic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">newspapers</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation12.html" border="0"></strong>, <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Popular = Information meant to inform and entertain general readers. Blogs, Internet forums, and magazines are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Popular = Information meant to inform and entertain general readers. Blogs, Internet forums, and magazines are some examples.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">popular</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation13.html" border="0"></strong> <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Magazine = Publication that provides news and stories for the general reader. Lots of images and advertisements. Usually published weekly or monthly, can be print or electronic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Magazine = Publication that provides news and stories for the general reader. Lots of images and advertisements. Usually published weekly or monthly, can be print or electronic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">magazines</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation14.html" border="0"></strong> and <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Scholarly = Referring to work written by an expert that is intended to educate scholars and students about a topic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Scholarly = Referring to work written by an expert that is intended to educate scholars and students about a topic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">scholarly</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation15.html" border="0"></strong> <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Journal = Publication that provides expert research on specific subjects. Usually published monthly or quarterly, can be print or electronic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Journal = Publication that provides expert research on specific subjects. Usually published monthly or quarterly, can be print or electronic.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">journals</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation16.html" border="0"></strong> as well as books and web sites. You are also encouraged to use a variety of media,
                     such as <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Internet = Vast electronic network where anyone can self-publish.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Internet = Vast electronic network where anyone can self-publish.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">Internet</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation17.html" border="0"></strong>, <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('AudioVisual = Refers to any media with both sound and images, can be analog, digital, or streaming.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('AudioVisual = Refers to any media with both sound and images, can be analog, digital, or streaming.', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">audiovisual</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation18.html" border="0"></strong> and <strong><a class="textpopper" href="Module_One_Fall18.html#" onmouseover="return overlib('Print = A paper \'\'hard copy.\'\'', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onfocus="return overlib('Print = A paper \'\'hard copy.\'\'', CAPTIONBACKUP, '', CAPTIONSIZE, 2, CGCOLOR, '#000000', PADX, 5, 5, PADY, 5, 5, CLOSECLICK, CLOSETEXT, '&amp;lt;img src=\'tpclose_custom.png\' border=0&amp;gt;', BELOW, RIGHT, BORDER, 1, BGCOLOR, '#000000', FGCOLOR, '#ffffff', WIDTH, 200, TEXTSIZE, 2, TEXTCOLOR, '#000000');" onmouseout="nd(0);" onblur="nd(0);">print</a><img src="http://valenciacollege.edu/library/tutorials/module_one/ada-annotation.gif" title="text annotation indicator" longdesc="ada_files/ada_annotation19.html" border="0"></strong> sources."</font>
                  
               </p>
               
               <p>Some of the source types mentioned in the assignment are familiar to Val and some
                  are not. In the three activities on the following pages, help Val brush up her knowledge.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall18.html#">return to top</a> | <a href="Module_One_Fall17.html">previous page</a> | <a href="Module_One_Fall19.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall18.pcf">©</a>
      </div>
   </body>
</html>