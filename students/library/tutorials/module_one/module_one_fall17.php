<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Step Four: Key Terms | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_one/module_one_fall17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_one/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_one/">Module One</a></li>
               <li>Step Four: Key Terms</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="stepfour:keyterms" id="stepfour:keyterms"></a><h2>Step Four: Key Terms</h2>
               
               <p><img align="left" src="http://valenciacollege.edu/library/tutorials/module_one/search2.JPG" width="100" alt="search2.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj6"> Val has decided to research the question, <strong>"How do perceptions of tattoos compare in Eastern religions, Judaism and Christianity?"</strong></p>
               
               <p>She types this question into Google, but gets only a few results. So, she changes
                  her search a little:
               </p>
               
               <ul>
                  
                  <li><em>perceptions tattoos Judaism Christianity Eastern religions</em></li>
                  
               </ul>
               
               <p>More results appear, but at first glance, they don't seem to help much. Frustrated,
                  Val makes one more change:
               </p>
               
               <ul>
                  
                  <li>
                     <strong><em>beliefs</em></strong> <em>tattoos Judaism Christianity Eastern religions</em>
                     
                  </li>
                  
               </ul>
               
               <p>Better! Val realizes that changing her search words helps her find more <strong>relevant</strong> search results. Quickly, she begins listing words she can use. The web sites she
                  has found so far give her some ideas, too.
               </p>
               
               <ul>
                  
                  <li><em>tattoos, beliefs, perceptions, ideologies, stigma, religions, Judaism, Christianity,
                        Eastern religions, Buddhism, spirituality</em></li>
                  
               </ul>
               
               <p>
                  <strong>
                     <u>The bottom line: Making a list of key words helps Val describe her information need
                        because she can now use many terms and phrases to research her question.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_One_Fall17.html#">return to top</a> | <a href="Module_One_Fall16.html">previous page</a> | <a href="Module_One_Fall18.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_one/module_one_fall17.pcf">©</a>
      </div>
   </body>
</html>