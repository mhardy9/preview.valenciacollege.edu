<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>What is Plagiarism?: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/whatisplagiarism/whatisplagiarism_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/whatisplagiarism/">Whatisplagiarism</a></li>
               <li>What is Plagiarism?:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>What is Plagiarism?:</strong><br>To Cite or Not to Cite? Part 1 of 5
               </p>
               
               
               <h2>What is Plagiarism?</h2>
               
               <p><strong>Outcome:</strong> You will be able to define plagiarism.
               </p>
               
               <p><strong>Approximate Completion Time</strong>: 10 minutes
               </p>
               
               <p>
                  <strong>
                     <font color="#8000FF">**To receive credit for completing this portion of the tutorial you must fill out
                        the form on page 9.**</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Defining Plagiarism</h2>
               
               <p>Plagiarism is using someone else's words, ideas, pictures or other original content
                  without acknowledgment.
               </p>
               
               <p>Students often think that it is not a big deal to</p>
               
               <ul>
                  
                  <li>cut and paste a few sentences from a web site,</li>
                  
                  <li>use a picture found freely available on a web site in a paper or speech,</li>
                  
                  <li>or paraphrase someone's else idea.</li>
                  
               </ul>
               
               <p>without acknowledging the source of the words, picture or idea.</p>
               
               <p>But all of these uses are plagiarism, and plagiarism can have severe consequences.</p>
               
               <p>At Valencia (and most colleges in the United States) plagiarism is considered academic
                  dishonesty. Read the college's definition of academic dishonesty at http://www.valenciacollege.edu/generalcounsel/policydetail.cfm?RecordID=193.
               </p>
               
               <p>The possible penalties for academic dishonesty include:</p>
               
               <ul>
                  
                  <li>"loss of credit for an assignment,"</li>
                  
                  <li>"withdrawal from course,"</li>
                  
                  <li>"reduction in the course grade,"</li>
                  
                  <li>"a grade of 'F' in the course,"</li>
                  
                  <li>"probation, suspension and/or expulsion from the College" (Valencia College).</li>
                  
               </ul>
               
               <p>Read Valencia's procedures for Academic Dishonesty from the Policy Manual at http://www.valenciacollege.edu/generalcounsel/proceduredetail.cfm?RecordID=193.</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Intellectual Property or Why is Plagiarism a Big Deal?</h2>
               
               <p>The <em>Oxford English Dictionary</em> defines plagiarism as follows: "The action or practice of taking someone else's work,
                  idea, etc., and passing it off as one's own; literary theft."
               </p>
               
               <p>The idea of plagiarism as theft is an important one in American academic culture;
                  it stems from the concept of intellectual property.
               </p>
               
               <p>Western cultures, such as those of the United States, Canada, the United Kingdom,
                  and Australia, commonly value intellectual property. Intellectual property is the
                  idea that a person or organization can own a idea, a phrase, an essay, a song, a particular
                  photograph, etc. Ideas can be owned even if the owner decides to make the idea freely
                  available on the Internet. For instance a band might decide to release a video of
                  a particular song on YouTube. Despite making that video available the band still owns
                  the content, and people finding that video on YouTube cannot claim ownership or use
                  that video any way they please. If you took a video from YouTube, put it on your own
                  web site and tried to sell it, you would find yourself in court defending yourself
                  against a law suit.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Famous Plagiarism Cases</h2>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td width="450px">
                        
                        <h2 align="center">Famous Plagiarism Cases</h2>
                        
                     </td>
                     
                     <td width="399px">
                        
                        <h2 align="center">Photograph of Katy Perry</h2>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="450px">
                        
                        <p>Accusations of plagiarism come up frequently in politics, journalism, music, movies
                           and other areas as well as education.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Some famous people who have been accused of plagiarism are:</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Jayson Blair, former New York Times columnist.</p>
                        
                        <p>Doris Kearns Goodwin, historian.</p>
                        
                        <p>James Cameron, film director.</p>
                        
                        <p>Kaavya Viswanathan, Harvard student writer.</p>
                        
                        <p>Australian band Men at Work.</p>
                        
                        <p>Katy Perry, singer.</p>
                        
                        <p>&nbsp;</p>
                        
                        <p>A Google news search on "plagiarism" is sure to turn up hundreds of articles.</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="399px">
                        
                        <p align="center"><img hspace="30" vspace="10" class="resizable" height="500" longdesc="A photograph of singer Katy Perry performing a song." alt="katyperry.jpg" src="http://valenciacollege.edu/library/tutorials/whatisplagiarism/katyperry.jpg" width="400" border="0"></p>
                        
                        <p>(Photo credit: The Toad. Katy Perry. Used under a Creative Commons Attribution-NonCommerical
                           2.0 Generic license.)
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Fig. 1: Defoe, David. "Katy Perry." 18 July 2008. Photo. <em>Flickr</em>. Yahoo!, n.d. 21 Aug. 2010.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Examples of Plagiarism</h2>
               
               <p>Copying phrases without citing.</p>
               
               <p>Copying sentences without citing.</p>
               
               <p>Paraphrasing without citing. (Putting an idea into your own words.)</p>
               
               <p>Summarizing without citing. (Giving an overview of another person's ideas.)</p>
               
               <p>Copying charts, graphs or videos to use in your presentation without citing.</p>
               
               <p>Reusing an assignment you created for a previous class in your current class without
                  permission from both instructors.
               </p>
               
               <p>Copying an entire paper or speech.</p>
               
               <p>Purchasing an entire speech or paper from a web site.</p>
               
               <p>Sometimes asking a friend/relative/classmate for help on your speech can also result
                  in plagiarism, depending on how much and what kind of help that person gives you.
               </p>
               
               <p>Bottom line: If you use someone else's words, ideas, or creative work without citing
                  it, you have plagiarized
               </p>
               
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <h2>Why is Plagiarism a Big Deal? Part II</h2>
               
               <p>Coming back to the classroom environment at Valencia, if you understand that your
                  professors think of plagiarism as literary theft, you can understand why plagiarism
                  is considered academic dishonesty and has such severe consequences. You are cheating
                  yourself of the opportunity to learn. College assignments are meant to prepare you
                  for the types of challenges you will meet in your personal and work lives. Everyone
                  needs to be able to prepare and present a speech, whether you end up using the skill
                  in sales calls, presentations to your colleagues, or toasting a friend at a wedding.
               </p>
               
               <p>Watch a 3 minute video clip introducing plagiarism and intellectual property. (Note:
                  for closed captioned version, select the Windows Media version of the clip from the
                  drop-down menu underneath the video. Then click the icon on the right-hand side on
                  the tool bar beneath the video to turn the captioning on.)
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" src="http://valenciacollege.edu/library/tutorials/whatisplagiarism/quizme_custom.png" title="Question 1" border="0"></p>
               
               <p>
                  <font color="#8000FF">Click Finish and then Print to keep a copy of your answer.</font>
                  
               </p>
               
               <p align="center"></p>
               
               <h2>Works Cited</h2>
               
               <p align="center">Works Cited</p>
               
               <p>&nbsp;</p>
               
               <p>"Plagiarism." Def. 1. <em>Oxford English Dictionary</em>. Oxford UP, 2009. Web. 13 Aug. 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p><em>Plagiarism: What Do You Value?</em> Dir. Michael Henry. Learning Essentials, 2005. <em>Films on</em></p>
               
               <p style="margin-left: 20.0"><em>Demand</em>. Web. 13 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Valencia College. "Academic Dishonesty Policy." <em>Policies and Procedures</em><span><font color="black">6Hx28:</font></span></p>
               
               <p style="margin-left: 20.0">
                  <span>
                     <font color="black">8-11.ValenciaCommunity CollegeGeneral Counsel, 11. Dec. 2007. Web.13 Aug. 2010.</font>
                     </span>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>Valencia College. "Academic Dishonesty Procedure." <em>Policies and Procedures</em></p>
               
               <p style="margin-left: 20.0">
                  <span>
                     <font color="black">6Hx28: 8-11.Valencia College General Counsel, 11. Dec. 2007. Web. 13</font>
                     </span>
                  
               </p>
               
               <p style="margin-left: 20.0">
                  <span>
                     <font color="black">Aug. 2010.</font>
                     </span>
                  
               </p>
               
               <p align="center"></p>
               
               <h2>
                  <span>
                     <font color="black">Print Your Score</font>
                     </span>
                  
               </h2>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/whatisplagiarism/whatisplagiarism_print.pcf">©</a>
      </div>
   </body>
</html>