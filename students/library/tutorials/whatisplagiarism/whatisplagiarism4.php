<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Famous Plagiarism Cases | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/whatisplagiarism/whatisplagiarism4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/whatisplagiarism/">Whatisplagiarism</a></li>
               <li>Famous Plagiarism Cases</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="famousplagiarismcases" id="famousplagiarismcases"></a><h2>Famous Plagiarism Cases</h2>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td width="450px">
                        <a name="famousplagiarismcases" id="famousplagiarismcases"></a><h2 align="center">Famous Plagiarism Cases</h2>
                        
                     </td>
                     
                     <td width="399px">
                        <a name="photographofkatyperry" id="photographofkatyperry"></a><h2 align="center">Photograph of Katy Perry</h2>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="450px">
                        
                        <p>Accusations of plagiarism come up frequently in politics, journalism, music, movies
                           and other areas as well as education.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Some famous people who have been accused of plagiarism are:</p>
                        
                        <p>&nbsp;</p>
                        
                        <p><a href="http://www.pbs.org/newshour/media/media_ethics/casestudy_blair.php" target="_blank">Jayson Blair</a>, former New York Times columnist.
                        </p>
                        
                        <p><a href="http://www.forbes.com/2002/02/27/0227goodwin.html" target="_blank">Doris Kearns Goodwin</a>, historian.
                        </p>
                        
                        <p><a href="http://www.linccweb.org/webscripts/purl.asp?purl=DqqjGYmGqQLqqqqK&amp;lib_code=FLCC2900" target="_blank">James Cameron</a>, film director.
                        </p>
                        
                        <p><a href="http://nymag.com/arts/books/features/16932/" target="_blank">Kaavya Viswanathan</a>, Harvard student writer.
                        </p>
                        
                        <p>Australian band <a href="http://news.bbc.co.uk/2/hi/8497433.stm" target="_blank">Men at Work.</a></p>
                        
                        <p><a href="http://www.bbc.co.uk/news/entertainment-arts-10891016" target="_blank">Katy Perry</a>, singer.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <p>A Google news search on "plagiarism" is sure to turn up hundreds of articles.</p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="399px">
                        
                        <p align="center"><img hspace="30" vspace="10" class="resizable" height="500" longdesc="ada_files/ada_image1.html" alt="katyperry.jpg" src="http://valenciacollege.edu/library/tutorials/whatisplagiarism/katyperry.jpg" width="400" border="0"></p>
                        
                        <p>(Photo credit: <a href="http://www.flickr.com/photos/thetoad01/" target="_blank">The Toad</a>. <a href="http://www.flickr.com/photos/thetoad01/2680949005/" target="_blank">Katy Perry</a>. Used under a <a href="http://creativecommons.org/licenses/by-nc/2.0/deed.en" target="_blank">Creative Commons Attribution-NonCommerical 2.0 Generic license</a>.)
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <p>Fig. 1: Defoe, David. "Katy Perry." 18 July 2008. Photo. <em>Flickr</em>. Yahoo!, n.d. 21 Aug. 2010.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whatisplagiarism4.html#">return to top</a> | <a href="whatisplagiarism3.html">previous page</a> | <a href="whatisplagiarism5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/whatisplagiarism/whatisplagiarism4.pcf">©</a>
      </div>
   </body>
</html>