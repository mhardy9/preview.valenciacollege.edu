<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 7 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/whatisplagiarism/whatisplagiarism7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/whatisplagiarism/">Whatisplagiarism</a></li>
               <li>Page 7</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/whatisplagiarism/spacer.gif" name="check1" alt="" id="check1"><a href="javascript:parent.toggletable('quizpopper1')" id="a1" title="Question 1"><img src="http://valenciacollege.edu/library/tutorials/whatisplagiarism/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 1" border="0"></a> 
               </p>
               
               <div id="quizpopper1" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #eeeeee; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f1" id="f1">What is plagiarism? Give a definition and at least 3 examples.
                        <div style="margin: 1em 15px;"><textarea name="q1" rows="16" wrap="virtual" onfocus="parent.clearEssayFeedback(1)" style="width: 390px;"></textarea></div>
                        
                        <div align="center">
                           <input onclick="parent.check_q(1, 0, 8, true)" type="button" name="Check" value="Finish">&nbsp;&nbsp;&nbsp;<input onclick="parent.print_essay(1)" type="button" name="Print" value="Print">
                           
                        </div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done1" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed1" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>
                  <font color="#8000FF">Click Finish and then Print to keep a copy of your answer.</font>
                  
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whatisplagiarism7.html#">return to top</a> | <a href="whatisplagiarism6.html">previous page</a> | <a href="whatisplagiarism8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/whatisplagiarism/whatisplagiarism7.pcf">©</a>
      </div>
   </body>
</html>