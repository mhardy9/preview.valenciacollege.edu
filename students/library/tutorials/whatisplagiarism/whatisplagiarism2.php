<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Defining Plagiarism | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/whatisplagiarism/whatisplagiarism2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/whatisplagiarism/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/whatisplagiarism/">Whatisplagiarism</a></li>
               <li>Defining Plagiarism</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="definingplagiarism" id="definingplagiarism"></a><h2>Defining Plagiarism</h2>
               
               <p>Plagiarism is using someone else's words, ideas, pictures or other original content
                  without acknowledgment.
               </p>
               
               <p>Students often think that it is not a big deal to</p>
               
               <ul>
                  
                  <li>cut and paste a few sentences from a web site,</li>
                  
                  <li>use a picture found freely available on a web site in a paper or speech,</li>
                  
                  <li>or paraphrase someone's else idea.</li>
                  
               </ul>
               
               <p>without acknowledging the source of the words, picture or idea.</p>
               
               <p>But all of these uses are plagiarism, and plagiarism can have severe consequences.</p>
               
               <p>At Valencia (and most colleges in the United States) plagiarism is considered academic
                  dishonesty. <a target="_blank" href="http://www.valenciacollege.edu/generalcounsel/policydetail.cfm?RecordID=193">Read the college's definition of academic dishonesty at http://www.valenciacollege.edu/generalcounsel/policydetail.cfm?RecordID=193.</a></p>
               
               <p>The possible penalties for academic dishonesty include:</p>
               
               <ul>
                  
                  <li>"loss of credit for an assignment,"</li>
                  
                  <li>"withdrawal from course,"</li>
                  
                  <li>"reduction in the course grade,"</li>
                  
                  <li>"a grade of 'F' in the course,"</li>
                  
                  <li>"probation, suspension and/or expulsion from the College" (Valencia College).</li>
                  
               </ul>
               
               <p>
                  <a target="_blank" href="http://www.valenciacollege.edu/generalcounsel/proceduredetail.cfm?RecordID=193">Read Valencia's procedures for Academic Dishonesty from the Policy Manual at http://www.valenciacollege.edu/generalcounsel/proceduredetail.cfm?RecordID=193.</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whatisplagiarism2.html#">return to top</a> | <a href="whatisplagiarism.html">previous page</a> | <a href="whatisplagiarism3.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/whatisplagiarism/whatisplagiarism2.pcf">©</a>
      </div>
   </body>
</html>