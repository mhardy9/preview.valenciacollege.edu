<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Why Cite in a Particular Format? | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-whycite/whycite4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-whycite/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-whycite/">Apa Whycite</a></li>
               <li>Why Cite in a Particular Format?</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="whyciteinaparticularformat?" id="whyciteinaparticularformat?"></a><h2>Why Cite in a Particular Format?</h2>
               
               <p>Following a set of rules ensures that all the information needed to find a particular
                  source will be included in the citation. As the ways we look for information change
                  citation styles also change. Certain elements may no longer be needed, and other new
                  elements are included. &nbsp;
               </p>
               
               <p>The rules are also designed to ensure consistency. If you become accustomed to a certain
                  style, citations written in that style become much easier to read. The elements of
                  the citation will always be in the same order, and you will recognize that order.
               </p>
               
               <p>
                  <strong>Which list below is easier to read?</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Example 1)&nbsp;</strong>
                  
               </p>
               
               <p align="center">References</p>
               
               <p>2009.&nbsp; Kevin Bales. U of California P. <em>THE SLAVE NEXT DOOR: human trafficking and slavery in america today</em>. Rod Soodalter. Berkeley.Print.
               </p>
               
               <p><em>Time</em>. 7 July 2010. "Human Trafficking Rises in Recession." <strong>Fetini, Alyssa</strong>. 18 June 2009. Web.
               </p>
               
               <p>Santana, M. C. Who is worth saving: human traffic news in the Caribbean and the United</p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; States. (2009). Retrieved from <em>Academic Search Complete</em> database.<em>International Journal of Academic Research</em> <em>1</em>(2), 206-11.
               </p>
               
               <p>V. MIzELl. T3.Web.<em>Washington Post</em> "Working to Shed Light on Very Dark Practices – Activists Seek to End
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Human Trafficking in D.C." 8 Oct. 2009. <em>Newsbank</em>. 7 July 2010."Trafficking in Persons Report 2010." U.S. Department of State. June
                  2010. Web. 7 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>Example 2)</strong>
                  
               </p>
               
               <p align="center">References&nbsp;</p>
               
               <p>Bales, K. &amp; Soodalter, R. (2009). The slave next door: Human trafficking and slavery
                  in America today. Berkeley: U of California P.&nbsp;
               </p>
               
               <p>Fetini, A. (2010). <em>Human trafficking rises in recession</em>. Retrieved from http://www.time.com/time/arts/article/0,8599,1905330,00.html
               </p>
               
               <p>Mizell, V. (2009, October 8). Working to shed light on very dark practices: Activists
                  seek to end human trafficking in D.C. <em>Washington Post.</em></p>
               
               <p style="margin-left: 40.0px">Retrieved from http://www.washingtonpost.com/</p>
               
               <p>Santana, M. C. (2009). Who is worth saving: Human traffic news in the Caribbean and
                  the United States. <em>International Journal of Academic Research, 1</em>(2).&nbsp;
               </p>
               
               <p style="margin-left: 40.0px">Retrieved from http://www.ijar.lit.az/</p>
               
               <p>U. S. Department of State. (2010). <em>Trafficking in Persons Report 2010</em>. Retrieved from www.state.gov/j/tip/rls/tiprpt/2010/&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whycite4.html#">return to top</a> | <a href="whycite3.html">previous page</a> | <a href="whycite5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-whycite/whycite4.pcf">©</a>
      </div>
   </body>
</html>