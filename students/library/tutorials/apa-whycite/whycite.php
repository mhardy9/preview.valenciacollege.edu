<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Why Cite? | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-whycite/whycite.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-whycite/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-whycite/">Apa Whycite</a></li>
               <li>Why Cite?</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="whycite?" id="whycite?"></a><h2>Why Cite?</h2>
               
               <p>Learning Outcome: By the end of the lesson, the student will be able to</p>
               
               <ul>
                  
                  <li>Understand the purpose of citation</li>
                  
                  <li>Give relevant uses for citations</li>
                  
                  <li>Explain why one citation format should be used consistently</li>
                  
               </ul>
               
               <p><strong>Approximate Completion Time</strong>: 10 minutes
               </p>
               
               <p>
                  <font color="#ff0000">
                     <strong>**To receive credit for completing this tutorial you must complete the online assessment
                        provided by your instructor.**</strong>
                     </font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p><span id="inlinemedia17">
                     <a href="ada_files/ada_media17.html" target="_new"><img src="http://valenciacollege.edu/library/tutorials/apa-whycite/ada-access.gif" hspace="3" width="20" height="20" align="middle" title="alternative accessible widget content" longdesc="ada_files/ada_media17.html" border="0" alt=""></a>
                     <a href="whycite.html#endofinline17"><img title="go to end of inline object" src="http://valenciacollege.edu/library/tutorials/apa-whycite/spacer.gif" border="0" width="0" height="0"></a>
                     
                     <embed height="315" width="560" type="application/x-shockwave-flash" allowscriptaccess="always" src="http://www.youtube.com/v/vsvQ7oshv-4?hl=en_US&amp;version=3" allowfullscreen="true">
                     
                     <div id="endofinline17"></div>
                     </span>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>This tutorial works best in Mozilla Firefox.</p>
               
               <p>It cannot save your place, so please take note of the approximate completion time
                  above and either work on it when you have time to finish or keep track of where you
                  were.
               </p>
               
               <p>&nbsp;</p>
               
               <hr>
               
               <p>
                  <strong>If you encounter technical problems, please copy the error message and send it with
                     a description of the problem to lking@valenciacollege.edu</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whycite.html#">return to top</a> | <a href="whycite2.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-whycite/whycite.pcf">©</a>
      </div>
   </body>
</html>