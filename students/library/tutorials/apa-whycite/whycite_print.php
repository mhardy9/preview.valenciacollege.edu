<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Why Cite?: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/apa-whycite/whycite_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/apa-whycite/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/apa-whycite/">Apa Whycite</a></li>
               <li>Why Cite?:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Why Cite?:</strong><br>To Cite or Not to Cite? Part 2 of 5
               </p>
               
               
               <h2>Why Cite?</h2>
               
               <p>Learning Outcome: By the end of the lesson, the student will be able to</p>
               
               <ul>
                  
                  <li>Understand the purpose of citation</li>
                  
                  <li>Give relevant uses for citations</li>
                  
                  <li>Explain why one citation format should be used consistently</li>
                  
               </ul>
               
               <p><strong>Approximate Completion Time</strong>: 10 minutes
               </p>
               
               <p>
                  <font color="#ff0000">
                     <strong>**To receive credit for completing this tutorial you must complete the online assessment
                        provided by your instructor.**</strong>
                     </font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p></p>
               
               <p>&nbsp;</p>
               
               <p>This tutorial works best in Mozilla Firefox.</p>
               
               <p>It cannot save your place, so please take note of the approximate completion time
                  above and either work on it when you have time to finish or keep track of where you
                  were.
               </p>
               
               <p>&nbsp;</p>
               
               <hr>
               
               <p>
                  <strong>If you encounter technical problems, please copy the error message and send it with
                     a description of the problem to lking@valenciacollege.edu</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>The Purpose of Citation</h2>
               
               <p>In writing, if you do not acknowledge your sources of information, your audience is
                  led to believe that everything presented is your own work. Citing helps your audience
                  differentiate between your own work and that of your sources.
               </p>
               
               <p>Using the work of experts can increase your credibility but for that to work the audience
                  has to know that you are using the expert's work and be able to assess the credibility
                  of the expert.
               </p>
               
               <p>For instance if I tell you that in 2016 Social Security is going to start spending
                  more than it takes in, you must assume that I am making that statement from my own
                  knowledge, and what do I know about Social Security spending? However if I tell you
                  that in a July 2010 report the Congressional Budget Office (a non-partisan Congressional
                  agency charged with evaluating Congressional proposals for their financial impact.)
                  projects that in 2016 Social Security is going to start spending more than it takes
                  in, you can evaluate whether you trust the Congressional Budget Office as a source
                  of information. You can go and find that report and read the statement for yourself
                  and decide whether I have interpreted it correctly.&nbsp;
               </p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td>
                        
                        <p><img width="800" class="resizable" alt="Social Security Benefits -2009-2083" border="0" vspace="10" hspace="30" src="http://valenciacollege.edu/library/tutorials/apa-whycite/800px-Social_Security_Benefits_-_2009-2083.png" height="520" longdesc="This is a chart showing that Social Security will begin to use reserve funds in 2016. Over time those funds will be exhausted." metadata="obj5"></p>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td>
                        
                        <p align="left"><em>Figure 1.</em> Chart of Social Security Benefits - 2009-2083. Reprinted from Rcragun. (7 October
                           2009). <em>Social Security Benefits - 2009-2083.png</em> [Chart]. Retrieved from http://commons.wikimedia.org/wiki/File:Social_Security_Benefits_-_2009-2083.png
                           Used under a Creative Commons Attribution 3.0 Unported license.
                        </p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Additional Uses for Citations</h2>
               
               <p>Providing citations also gives your audience the means to find your sources.</p>
               
               <p>Have you ever read something and thought that a particular web site, article or book
                  referenced sounded really interesting and wanted to find it for yourself? Conversely
                  have you ever heard about a particular web site, wanted to read it, and found that
                  the person who told you about it did not give you enough information to actually find
                  it? Providing citations allows your readers to discover any sources used to fill their
                  own intellectual curiosity.
               </p>
               
               <p>Watch this 4 minute video to learn more about why it is important to cite your work
                  and avoid plagiarism.
               </p>
               
               <p>
                  <font color="#C00000">Note: This video may take a moment to load.</font>
                  
               </p>
               
               <p></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Why Cite in a Particular Format?</h2>
               
               <p>Following a set of rules ensures that all the information needed to find a particular
                  source will be included in the citation. As the ways we look for information change
                  citation styles also change. Certain elements may no longer be needed, and other new
                  elements are included. &nbsp;
               </p>
               
               <p>The rules are also designed to ensure consistency. If you become accustomed to a certain
                  style, citations written in that style become much easier to read. The elements of
                  the citation will always be in the same order, and you will recognize that order.
               </p>
               
               <p>
                  <strong>Which list below is easier to read?</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Example 1)&nbsp;</strong>
                  
               </p>
               
               <p align="center">References</p>
               
               <p>2009.&nbsp; Kevin Bales. U of California P. <em>THE SLAVE NEXT DOOR: human trafficking and slavery in america today</em>. Rod Soodalter. Berkeley.Print.
               </p>
               
               <p><em>Time</em>. 7 July 2010. "Human Trafficking Rises in Recession." <strong>Fetini, Alyssa</strong>. 18 June 2009. Web.
               </p>
               
               <p>Santana, M. C. Who is worth saving: human traffic news in the Caribbean and the United</p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; States. (2009). Retrieved from <em>Academic Search Complete</em> database.<em>International Journal of Academic Research</em> <em>1</em>(2), 206-11.
               </p>
               
               <p>V. MIzELl. T3.Web.<em>Washington Post</em> "Working to Shed Light on Very Dark Practices – Activists Seek to End
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Human Trafficking in D.C." 8 Oct. 2009. <em>Newsbank</em>. 7 July 2010."Trafficking in Persons Report 2010." U.S. Department of State. June
                  2010. Web. 7 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>Example 2)</strong>
                  
               </p>
               
               <p align="center">References&nbsp;</p>
               
               <p>Bales, K. &amp; Soodalter, R. (2009). The slave next door: Human trafficking and slavery
                  in America today. Berkeley: U of California P.&nbsp;
               </p>
               
               <p>Fetini, A. (2010). <em>Human trafficking rises in recession</em>. Retrieved from http://www.time.com/time/arts/article/0,8599,1905330,00.html
               </p>
               
               <p>Mizell, V. (2009, October 8). Working to shed light on very dark practices: Activists
                  seek to end human trafficking in D.C. <em>Washington Post.</em></p>
               
               <p style="margin-left: 40.0">Retrieved from http://www.washingtonpost.com/</p>
               
               <p>Santana, M. C. (2009). Who is worth saving: Human traffic news in the Caribbean and
                  the United States. <em>International Journal of Academic Research, 1</em>(2).&nbsp;
               </p>
               
               <p style="margin-left: 40.0">Retrieved from http://www.ijar.lit.az/</p>
               
               <p>U. S. Department of State. (2010). <em>Trafficking in Persons Report 2010</em>. Retrieved from www.state.gov/j/tip/rls/tiprpt/2010/&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Sources Used in this Module - Cited in APA Format</h2>
               
               <p align="center">References</p>
               
               <p>&nbsp;</p>
               
               <p>Congressional Budget Office. (2010). <em>Social Security Policy Options</em>. Retrieved from http://www.cbo.gov/publication/21547
               </p>
               
               <p>Films Media Group.&nbsp;(2011).&nbsp;<em>Plagiarism 2.0: Information ethics in the digital age</em>&nbsp;[H.264]. Retrieved from http://digital.films.com
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Assessment</h2>
               
               <p>
                  <font color="#ff0000">
                     <strong>Your instructor will provide the link to the online assessment. You must take the
                        assessment to receive credit for completing this tutorial.</strong>
                     </font>
                  
               </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/apa-whycite/whycite_print.pcf">©</a>
      </div>
   </body>
</html>