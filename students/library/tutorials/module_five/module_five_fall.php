<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Module 5 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Module 5</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="module5" id="module5"></a><h2>
                  <strong>
                     <u>Module 5</u>
                     </strong>
                  
               </h2>
               
               <p><img longdesc="ada_files/ada_image1.html" align="right" src="http://valenciacollege.edu/library/tutorials/module_five/computer.jpg" width="193" alt="computer.jpg" class="resizable" height="190" border="0" hspace="19" metadata="obj22"> <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Scene 5</font>
                     </strong>
                  
               </p>
               
               <p>English class, Tuesday morning.</p>
               
               <p><strong>PROFESSOR SAGE</strong>: Your paper including a complete Works Cited page is due on Thursday. You have already
                  practiced doing citations for your Annotated Bibliography assignment, so this should
                  be review. Remember that you will include only the sources that you actually used
                  in your paper on your Works Cited page.
               </p>
               
               <p><strong>VAL</strong> (raising hand): What if we have questions about citing some of our sources? I have
                  a web site that I'm having a little trouble with.
               </p>
               
               <p><strong>PROFESSOR SAGE</strong>: If you would like to stay for a minute after class I can take a look at it, but
                  if you get stuck later at home, there are lots of other resources you can use. Look
                  at the example sheet that I've given you, and if you still have questions, you can
                  consult your handbook or the <em>MLA Handbook</em> available at the library. The library has lots of citation examples on their web
                  site, so use those, too. If you're still stuck, you can also get help from the librarians
                  or from the Writing Center.
               </p>
               
               <p><strong>MATT</strong> (raising hand): I have an article from a database that I'm using, and I remember
                  that there was a list of citation examples for databases somewhere, but I forget where.
               </p>
               
               <p><strong>PROFESSOR SAGE</strong>: That's on the library web site, but you can get to it from Atlas. Login and click
                  on MLA and APA Style Guides from the MyAtlas tab. All of the databases are listed
                  under MLA Documentation of Electronic Sources.
               </p>
               
               <p><strong>MATT</strong>: Okay. Thanks.
               </p>
               
               <p><strong>PROFESSOR SAGE</strong>: Please remember to use in-text citations every time you quote, paraphrase or summarize.
                  I do not want any of you to plagiarize, even unintentionally...
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall.html#">return to top</a> | <a href="Module_Five_Fall2.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall.pcf">©</a>
      </div>
   </body>
</html>