<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 18 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall18.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Page 18</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>
                     <font size="5" style="line-height: 1">Test Your Knowledge!</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p><strong><font size="5" style="line-height: 1">Carefully read the original article on the</font></strong> <strong><a href="http://www.pbs.org/skinstories/history/hawaii.html">history of tattoo in Hawaii</a></strong></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Now read the following student passage and identify all the problems you see in the
                     passage. In the box below explain what is wrong with the passage and what you would
                     do to fix it.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>Captain Cook played a role in the breakdown of Hawaiian traditions including tattooing.
                  Hawaiian tattooing was called "kakau," and Hawaiians considered it important for aesthetic,
                  health and religious reasons (Pacific Islanders in Communication). Traditional Hawaiian
                  tattoos took their patterns from nature. The designs were applied by specially trained
                  kahuna, experts in one or more critical task, who applied pigment to the skin with
                  a needle made from bone, tied to a stick and struck by a mallet. The process was a
                  big secret, and the tools were broken afterward. In the nineteenth century this practice
                  began to disappear. "Kakau was discouraged and suppressed."
               </p>
               
               <p>&nbsp;</p>
               
               <p>Pacific Islanders in Communication. "History of Tattoo." <em>Skin Stories: the Art and Culture of Polynesian</em></p>
               
               <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<em>Tattoo</em>. PBS, 2003. Web. 3 Aug. 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_five/spacer.gif" name="check31" alt="" id="check31"><a id="a31" title="Question 5" href="javascript:parent.toggletable('quizpopper31')"><img src="http://valenciacollege.edu/library/tutorials/module_five/testyourself_custom.png" alt="Toggle open/close quiz question" title="Question 5" border="0"></a> 
               </p>
               
               <div id="quizpopper31" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f31" id="f31">Explain what is wrong with the passage and what you would do to fix it.
                        <div style="margin: 1em 15px;"><textarea name="q31" rows="16" wrap="virtual" onfocus="parent.clearEssayFeedback(31)" style="width: 390px;"></textarea></div>
                        
                        <div align="center">
                           <input onclick="parent.check_q(31, 0, 8, true)" type="button" name="Check" value="Finish">&nbsp;&nbsp;&nbsp;<input onclick="parent.print_essay(31)" type="button" name="Print" value="Print">
                           
                        </div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done31" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed31" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall18.html#">return to top</a> | <a href="Module_Five_Fall17.html">previous page</a> | <a href="Module_Five_Fall19.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall18.pcf">©</a>
      </div>
   </body>
</html>