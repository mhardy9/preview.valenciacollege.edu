<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Read the following passage and then decide whether the quiz examples are plagiarized. | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Read the following passage and then decide whether the quiz examples are plagiarized.</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="readthefollowingpassageandthendecidewhetherthequizexamplesareplagiarized." id="readthefollowingpassageandthendecidewhetherthequizexamplesareplagiarized."></a><h2>
                  <font size="5" style="line-height: 1">Read the following passage and then decide whether the quiz examples are plagiarized.</font>
                  
               </h2>
               
               <p><strong><em>Assume that the citation given below is on a Works Cited page at the end of the paper</em></strong>:
               </p>
               
               <p>&nbsp;</p>
               
               <p>"Obesity poses a major public health challenge. Each year, obesity contributes to
                  an estimated 112,000 preventable deaths. Obese adults are at increased risk for many
                  serious health conditions, including high blood pressure, high cholesterol, type 2
                  diabetes and its complications, coronary heart disease, stroke, gallbladder disease,
                  osteoarthritis, sleep apnea, and respiratory problems, as well as endometrial, breast,
                  prostate, and colon cancers" (Office of the Surgeon General 2)
               </p>
               
               <p>MLA Works Cited Entry:</p>
               
               <p>Office of the Surgeon General. <em>The Surgeon General's Vision for a Healthy and Fit Nation</em>. <em>U.S.</em></p>
               
               <p style="margin-left: 40.0px"><em>Department of Health and Human Services, Office of the Surgeon General</em>. U.S. Department of
               </p>
               
               <p style="margin-left: 40.0px">Health and Human Services, January 2010. Web. 1 Nov. 2010.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<a title="QPS" href="javascript:parent.toggleQuizGoup(2,%20false,%20false,%20false)"><img src="http://valenciacollege.edu/library/tutorials/module_five/quizgroup_custom.png" alt="Toggle open/close quiz group" id="quizgroup2" border="0" title="QPS"></a>
                  
                  
               </p>
               
               <div id="quizgroupwrapper2" style="display:none;padding-left:10px;border:1px solid #000000;">
                  
                  <div id="quizgroupspace2"></div>
                  
                  <div id="quizgroupfeedback2"></div>
                  
                  <p>
                     <input type="button" value="&amp;lt;  Prev" onclick="parent.writePrevGroupQP(2, false)" style="display:none;" id="qgbackbottom2">&nbsp;
                     <input type="button" value="Next  &amp;gt;" onclick="parent.writeNextGroupQP(2, false, false)" style="display:inline;" id="qgforwardbottom2">
                     <br><br>
                     <input type="button" value="Check Answers" onclick="parent.checkQuizGroup(2, false, false, false)" style="display:none;" id="quizgroupresults2">
                     
                  </p>
                  
               </div>
               
               
               
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall17.html#">return to top</a> | <a href="Module_Five_Fall16.html">previous page</a> | <a href="Module_Five_Fall18.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall17.pcf">©</a>
      </div>
   </body>
</html>