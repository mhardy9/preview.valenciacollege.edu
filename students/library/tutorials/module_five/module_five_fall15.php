<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Paraphrasing Example | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall15.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Paraphrasing Example</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="paraphrasingexample" id="paraphrasingexample"></a><h2>
                  <font size="5" style="line-height: 1">Paraphrasing Example</font>
                  
               </h2>
               
               <p><img longdesc="ada_files/ada_image13.html" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/FDA.jpg" width="280" alt="FDA.jpg" class="resizable" height="362" border="0" hspace="19" metadata="obj27"> In a paraphrase, Val takes information from a sentence or a group of sentences and
                  puts it in her own words. This means that Val's paraphrase should not use the same
                  structure as the original sentence nor the same words (except for words that are essential
                  to the concept. In this case it would be difficult to rewrite the sentence without
                  using either "tattoo" or "ink.")
               </p>
               
               <p>&nbsp;</p>
               
               <p>Original Passage:</p>
               
               <p>"While state and local authorities oversee the practice of tattooing, ink and ink
                  colorings (pigments) used in tattoos are subject to FDA regulation as cosmetics and
                  color additives."
               </p>
               
               <p>&nbsp;</p>
               
               <p>Paraphrase:</p>
               
               <p>&nbsp;</p>
               
               <p>The US Food and Drug Administration makes rules about tattoo ink, and cities and states
                  are in charge of tattoo parlors (US Food and Drug Admin.)
               </p>
               
               <p>&nbsp;</p>
               
               <p>The complete citation information goes on the Works Cited page at the end of the paper.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Works Cited</p>
               
               <p>&nbsp;</p>
               
               <p align="left">US Food and Drug Admin. Think Before You Ink: Are Tattoos Safe? <em>US Food and Drug Admin</em>. US Food and Drug Admin., Oct. 2009.Web. 19 June 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: US Food and Drug Administration</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall15.html#">return to top</a> | <a href="Module_Five_Fall14.html">previous page</a> | <a href="Module_Five_Fall16.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall15.pcf">©</a>
      </div>
   </body>
</html>