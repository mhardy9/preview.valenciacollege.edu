<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Help Val choose the common knowledge sentences that don't need citations. | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall12.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Help Val choose the common knowledge sentences that don't need citations.</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <h2>&nbsp;</h2>
               <a name="helpvalchoosethecommonknowledgesentencesthatdon'tneedcitations." id="helpvalchoosethecommonknowledgesentencesthatdon'tneedcitations."></a><h2>
                  <font size="5" style="line-height: 1">Help Val choose the common knowledge sentences that don't need citations.</font>
                  
               </h2>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<a title="Common Knowledge Quiz" href="javascript:parent.toggleQuizGoup(3,%20false,%20false,%20false)"><img src="http://valenciacollege.edu/library/tutorials/module_five/quizgroup_custom.png" alt="Toggle open/close quiz group" id="quizgroup3" border="0" title="Common Knowledge Quiz"></a>
                  
                  
               </p>
               
               <div id="quizgroupwrapper3" style="display:none;padding-left:10px;border:1px solid #000000;">
                  
                  <div id="quizgroupspace3"></div>
                  
                  <div id="quizgroupfeedback3"></div>
                  
                  <p>
                     <input type="button" value="&amp;lt;  Prev" onclick="parent.writePrevGroupQP(3, false)" style="display:none;" id="qgbackbottom3">&nbsp;
                     <input type="button" value="Next  &amp;gt;" onclick="parent.writeNextGroupQP(3, false, false)" style="display:inline;" id="qgforwardbottom3">
                     <br><br>
                     <input type="button" value="Check Answers" onclick="parent.checkQuizGroup(3, false, false, false)" style="display:none;" id="quizgroupresults3">
                     
                  </p>
                  
               </div>
               
               
               
               
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall12.html#">return to top</a> | <a href="Module_Five_Fall11.html">previous page</a> | <a href="Module_Five_Fall13.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall12.pcf">©</a>
      </div>
   </body>
</html>