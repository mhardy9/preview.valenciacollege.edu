<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Penalties for Plagiarism | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall6.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Penalties for Plagiarism</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               <a name="penaltiesforplagiarism" id="penaltiesforplagiarism"></a><h2 align="left">Penalties for Plagiarism</h2>
               
               <p><img longdesc="ada_files/ada_image5.html" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/F%20grade.png" width="200" alt="F grade.png" class="resizable" height="238" border="0" hspace="30" metadata="obj4"> Plagiarism at Valencia can be considered either an academic offense or a violation
                  of the Student Code of Conduct.
               </p>
               
               <p>Penalties for an academic offense include:</p>
               
               <ul>
                  
                  <li>"loss of credit for an assignment, examination or project"</li>
                  
                  <li>"withdrawal from a course"</li>
                  
                  <li>"a reduction in the course grade"</li>
                  
                  <li>"a grade of 'F' in the course"</li>
                  
               </ul>
               
               <p>Penalties for a violation of the Student Code of Conduct include:</p>
               
               <ul>
                  
                  <li>"warning"</li>
                  
                  <li>"probation"</li>
                  
                  <li>"suspension" from the College</li>
                  
                  <li>"expulsion from the College"</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>VALENCIA COLLEGE DISTRICT BOARD OF TRUSTEES</p>
               
               <p>POLICY AND PROCEDURE Number: 6Hx28:10-16</p>
               
               <p>Academic Dishonesty</p>
               
               <p>
                  <a target="_blank" href="../../../generalcounsel/policy/ValenciaCollegePolicy.cfm-policyID=193.html">http://valenciacollege.edu/generalcounsel/policy/ValenciaCollegePolicy.cfm?policyID=193</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: Plagiarism can have very serious consequences at Valencia.&nbsp;</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall6.html#">return to top</a> | <a href="Module_Five_Fall5.html">previous page</a> | <a href="Module_Five_Fall7.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall6.pcf">©</a>
      </div>
   </body>
</html>