<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Types of Plagiarism | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall5.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Types of Plagiarism</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>&nbsp;</p>
               <a name="typesofplagiarism" id="typesofplagiarism"></a><h2>
                  <a id="bookmark" name="typesofplagiarism"></a> Types of Plagiarism
               </h2>
               
               <p><img longdesc="ada_files/ada_image4.html" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/thief%20mouse.png" width="350" alt="thief mouse.png" class="resizable" height="266" border="0" hspace="20" metadata="obj3"> Plagiarism can be either intentional or unintentional.&nbsp; What does this mean?&nbsp;
               </p>
               
               <p><strong><em>Intentional plagiarism</em>:</strong> If Val purposely copied from another person and did not give him or her credit to
                  hide the fact that material used was not her own work, she plagiarized. This category
                  includes
               </p>
               
               <ul>
                  
                  <li>buying all or parts of an assignment</li>
                  
                  <li>asking someone else to write all or part of an assignment</li>
                  
                  <li>copying all or part of an assignment from another work, e.g. a book or a web site</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p><strong><em>Unintentional plagiarism</em>:</strong> If Val is sloppy with her citations or documentation, she is still plagiarizing even
                  if she didn't mean to steal. This category includes:
               </p>
               
               <ul>
                  
                  <li>failing to cite a source</li>
                  
                  <li>failing to use quotation marks when quoting an author's words</li>
                  
                  <li>failing to put a paraphrase into her own words</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<strong><em>Self-plagiarism</em></strong>, using an assignment created for another class without permission, can be either
                  intentional or unintentional. Self-plagiarism is considered plagiarism because each
                  assignment given by a professor is intended to add to Val's knowledge and skills.
                  If Val simply reuses an old assignment, she will not learn anything new or add to
                  her skills.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: Plagiarism is intellectual theft, even when it is unintentional.</u>
                  
               </p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall5.html#">return to top</a> | <a href="Module_Five_Fall4.html">previous page</a> | <a href="Module_Five_Fall6.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall5.pcf">©</a>
      </div>
   </body>
</html>