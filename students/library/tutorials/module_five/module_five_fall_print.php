<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information Literacy 5 - Using Information Ethically | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Information Literacy 5 - Using Information Ethically</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Information Literacy 5 - Using Information Ethically</strong><br>Valencia College Libraries
               </p>
               
               
               <h2>
                  <strong>
                     <u>Module 5</u>
                     </strong>
                  
               </h2>
               
               <p><img longdesc="Drawing of a computer" align="right" src="http://valenciacollege.edu/library/tutorials/module_five/computer.jpg" width="193" alt="computer.jpg" class="resizable" height="190" border="0" hspace="19" metadata="obj22"> <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <font size="5">Scene 5</font>
                     </strong>
                  
               </p>
               
               <p>English class, Tuesday morning.</p>
               
               <p><strong>PROFESSOR SAGE</strong>: Your paper including a complete Works Cited page is due on Thursday. You have already
                  practiced doing citations for your Annotated Bibliography assignment, so this should
                  be review. Remember that you will include only the sources that you actually used
                  in your paper on your Works Cited page.
               </p>
               
               <p><strong>VAL</strong> (raising hand): What if we have questions about citing some of our sources? I have
                  a web site that I'm having a little trouble with.
               </p>
               
               <p><strong>PROFESSOR SAGE</strong>: If you would like to stay for a minute after class I can take a look at it, but
                  if you get stuck later at home, there are lots of other resources you can use. Look
                  at the example sheet that I've given you, and if you still have questions, you can
                  consult your handbook or the <em>MLA Handbook</em> available at the library. The library has lots of citation examples on their web
                  site, so use those, too. If you're still stuck, you can also get help from the librarians
                  or from the Writing Center.
               </p>
               
               <p><strong>MATT</strong> (raising hand): I have an article from a database that I'm using, and I remember
                  that there was a list of citation examples for databases somewhere, but I forget where.
               </p>
               
               <p><strong>PROFESSOR SAGE</strong>: That's on the library web site, but you can get to it from Atlas. Login and click
                  on MLA and APA Style Guides from the MyAtlas tab. All of the databases are listed
                  under MLA Documentation of Electronic Sources.
               </p>
               
               <p><strong>MATT</strong>: Okay. Thanks.
               </p>
               
               <p><strong>PROFESSOR SAGE</strong>: Please remember to use in-text citations every time you quote, paraphrase or summarize.
                  I do not want any of you to plagiarize, even unintentionally...
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <h2>Introduction: Using Information Ethically</h2>
               
               <p>
                  <img longdesc="The scales of justice" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/scales.png" width="180" alt="scales.png" class="resizable" height="183" border="0" metadata="obj1">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>The final step in research is crediting your sources correctly. Val must use the MLA
                  citation style consistently and correctly. This includes compiling a complete list
                  of her works cited to insert at the end of her paper, and incorporating in-text citations
                  and quotation marks where appropriate. Val needs to know when to use in-text citations
                  and when they are not necessary, so that she can avoid plagiarism.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>This module explains what plagiarism is and how Val can avoid it. Complete all activities
                     to learn how to avoid plagiarism.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <h2>What is plagiarism?</h2>
               
               <p>
                  <img longdesc="A photograph of a student copying work." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/cheating.jpg" width="400" alt="cheating.jpg" class="resizable" height="267" border="0" hspace="20" metadata="obj2">
                  
               </p>
               
               <p>"The unauthorized use or close imitation of the language and thoughts of another author
                  and the representation of them as one's own original work."
               </p>
               
               <p>From The Random House Dictionary of the English Language, 2d ed.</p>
               
               <p>&nbsp;</p>
               
               <p>If Val copies someone else's words into her paper and she does not give the original
                  author appropriate credit for his or her work, she is plagiarizing. Val is stealing
                  the hard work of another person and passing it off as if it were her own effort.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: Using an author's words, thoughts, or ideas without giving credit
                     is plagiarism.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: istockphoto</p>
               
               <p align="center"></p>
               
               <h2>
                  <font size="5">Which of the following situations are plagiarism?</font>
                  
               </h2>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_five/quizgroup_custom.png" alt="Toggle open/close quiz group" id="quizgroup1" border="0" title="Is It Plagiarism?"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <h2> Types of Plagiarism</h2>
               
               <p><img longdesc="An illustration of a mouse wearing a mask and carrying a bag of stolen goods." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/thief%20mouse.png" width="350" alt="thief mouse.png" class="resizable" height="266" border="0" hspace="20" metadata="obj3"> Plagiarism can be either intentional or unintentional.&nbsp; What does this mean?&nbsp;
               </p>
               
               <p><strong><em>Intentional plagiarism</em>:</strong> If Val purposely copied from another person and did not give him or her credit to
                  hide the fact that material used was not her own work, she plagiarized. This category
                  includes
               </p>
               
               <ul>
                  
                  <li>buying all or parts of an assignment</li>
                  
                  <li>asking someone else to write all or part of an assignment</li>
                  
                  <li>copying all or part of an assignment from another work, e.g. a book or a web site</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p><strong><em>Unintentional plagiarism</em>:</strong> If Val is sloppy with her citations or documentation, she is still plagiarizing even
                  if she didn't mean to steal. This category includes:
               </p>
               
               <ul>
                  
                  <li>failing to cite a source</li>
                  
                  <li>failing to use quotation marks when quoting an author's words</li>
                  
                  <li>failing to put a paraphrase into her own words</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<strong><em>Self-plagiarism</em></strong>, using an assignment created for another class without permission, can be either
                  intentional or unintentional. Self-plagiarism is considered plagiarism because each
                  assignment given by a professor is intended to add to Val's knowledge and skills.
                  If Val simply reuses an old assignment, she will not learn anything new or add to
                  her skills.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: Plagiarism is intellectual theft, even when it is unintentional.</u>
                  
               </p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <h2 align="left">Penalties for Plagiarism</h2>
               
               <p><img longdesc="An F grade on a paper." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/F%20grade.png" width="200" alt="F grade.png" class="resizable" height="238" border="0" hspace="30" metadata="obj4"> Plagiarism at Valencia can be considered either an academic offense or a violation
                  of the Student Code of Conduct.
               </p>
               
               <p>Penalties for an academic offense include:</p>
               
               <ul>
                  
                  <li>"loss of credit for an assignment, examination or project"</li>
                  
                  <li>"withdrawal from a course"</li>
                  
                  <li>"a reduction in the course grade"</li>
                  
                  <li>"a grade of 'F' in the course"</li>
                  
               </ul>
               
               <p>Penalties for a violation of the Student Code of Conduct include:</p>
               
               <ul>
                  
                  <li>"warning"</li>
                  
                  <li>"probation"</li>
                  
                  <li>"suspension" from the College</li>
                  
                  <li>"expulsion from the College"</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>VALENCIA COLLEGE DISTRICT BOARD OF TRUSTEES</p>
               
               <p>POLICY AND PROCEDURE Number: 6Hx28:10-16</p>
               
               <p>Academic Dishonesty</p>
               
               <p>http://valenciacollege.edu/generalcounsel/policy/documents/8-11-Academic-Dishonesty.pdf</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: Plagiarism can have very serious consequences at Valencia.&nbsp;</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <h2>Review Questions</h2>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_five/quizgroup_custom.png" alt="Toggle open/close quiz group" id="quizgroup8" border="0" title="review"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> <font size="5">Citation Styles</font>
                  
               </h2>
               
               <p>Val has already created an annotated bibliography in the MLA style, but she has heard
                  that other classes use the APA style. She wonders when she would use one instead of
                  the other.
               </p>
               
               <p><img longdesc="Picture of the cover of the Chicago Manual of Style" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/chicago2.jpg" width="106" alt="chicago2.jpg" class="resizable" height="160" border="0" hspace="19" metadata="obj25"> There are actually lots of different styles used for creating citations. These include
               </p>
               
               <ul>
                  
                  <li>AMA style</li>
                  
                  <li>APA style</li>
                  
                  <li>BibTeX style</li>
                  
                  <li>Chicago style</li>
                  
                  <li>MLA style</li>
                  
               </ul>
               
               <p>At Valencia the two commonly used formats for creating citations are APA and MLA,
                  but some professors may require other styles. If you are unsure, always ask your professor
                  which style to use.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: There are many citation styles. Ask your professor which one to use.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Syndetic Solutions</p>
               
               <p align="center"></p>
               
               <h2>
                  <font size="5">APA and MLA</font>
                  
               </h2>
               
               <p>
                  <img longdesc="The cover of the APA Manual." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/apa2.png" width="106" alt="apa2.png" class="resizable" height="158" border="0" hspace="19" metadata="obj15">
                  
               </p>
               
               <p>APA stands for American Psychological Association. It is often used in</p>
               
               <ul>
                  
                  <li>psychology</li>
                  
                  <li>nursing</li>
                  
                  <li>business</li>
                  
                  <li>economics</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img longdesc="Cover of the MLA Handbook" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/mlahb.jpg" width="100" alt="mlahb.jpg" class="resizable" height="150" border="0" hspace="20" metadata="obj8">
                  
               </p>
               
               <p>MLA is the Modern Language Association style. It is most often required in</p>
               
               <ul>
                  
                  <li>English</li>
                  
                  <li>humanities</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>APA and MLA use most of the same basic components in a citation; they just arrange
                  and display them differently.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: The two most commonly used citation styles at Valencia are MLA and
                     APA.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credits: Syndetic Solutions</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2 align="left">To Cite or Not to Cite?</h2>
               
               <p align="left">
                  <img longdesc="A picture of a question mark" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/question%20mark.png" width="147" alt="question mark.png" class="resizable" height="194" border="0" metadata="obj9">
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Val knows that for her final paper she must use in-text citations as well as including
                  all her sources on a works cited page at the end of the paper, but she is uncertain
                  about when she needs to include the in-text citations.
               </p>
               
               <p align="left">
                  <u>In the next section, we will look at when to cite and when not to.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> When Not To Cite</h2>
               
               <p>The following do not require citation:</p>
               
               <ul>
                  
                  <li>Val's own thoughts</li>
                  
                  <li>common knowledge</li>
                  
               </ul>
               
               <p>
                  <img longdesc="The periodic table of the elements: an example of common knowledge" align="right" src="http://valenciacollege.edu/library/tutorials/module_five/periodictable.jpg" width="500" alt="periodictable.jpg" class="resizable" height="346" border="0" hspace="19" metadata="obj19">
                  
               </p>
               
               <p>
                  <strong>Common Knowledge</strong>
                  
               </p>
               
               <p>If information is "common knowledge," or can be gathered from many sources, it does
                  not need a specific cited source directly linked to it.&nbsp; The same information appearing
                  in three or more reputable sources is considered common knowledge. Examples of this
                  often include:
               </p>
               
               <ul>
                  
                  <li>historical events (Terrorists crashed two airplanes into the World Trade Center on
                     September 11, 2001.),
                  </li>
                  
                  <li>places (Disney World is located south of Orlando, Florida),</li>
                  
                  <li>famous people (Barack Obama is the president of the United States.),</li>
                  
                  <li>some scientific facts (C is the chemical symbol for carbon),</li>
                  
                  <li>information covered in the class for which Val is writing a paper.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>However, if Val has any doubt, it's best for her to cite a source!</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: An author's personal thoughts and common knowledge do not require
                     an in-text citation.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: pt. periodictable. Used under a Creative Commons Attribution-NonCommercial-NoDerivs
                     2.0 Generic license</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               
               <h2>
                  <font size="5">Help Val choose the common knowledge sentences that don't need citations.</font>
                  
               </h2>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_five/quizgroup_custom.png" alt="Toggle open/close quiz group" id="quizgroup3" border="0" title="Common Knowledge Quiz"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> When to Cite</h2>
               
               <p><img longdesc="Drawing of a lightbulb." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/lightbulb.JPG" width="225" alt="lightbulb.JPG" class="resizable" height="225" border="0" hspace="19" metadata="obj29"> <strong>Direct Quotations</strong></p>
               
               <p>If Val uses an author's exact words she must put them in quotation marks and cite
                  the author, both in the body of her paper immediately following the quote and at the
                  end of her paper.
               </p>
               
               <p>
                  <strong>Paraphrasing</strong>
                  
               </p>
               
               <p>Val can put an author's thoughts into her own words, but she must cite the source,
                  again both at the end of the sentence and at the end of her paper.
               </p>
               
               <p>And…changing only a few words does NOT constitute paraphrasing.&nbsp;Val really does have
                  to reword the quote and paraphrase in HER own words!
               </p>
               
               <p>
                  <strong>Summaries</strong>
                  
               </p>
               
               <p>Val can sum up an author's ideas in her own words, but because the ideas are not her
                  own, she must cite in and at the end of the paper.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: Direct quotes, paraphrases and summaries all require an in-text citation.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               
               <p align="center"></p>
               
               <h2>
                  <font size="5">Direct Quotations Examples</font>
                  
               </h2>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="A tattoo after two laser treatments" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/tattooremoval.jpg" width="200" alt="tattooremoval.jpg" class="resizable" height="300" border="0" hspace="19" metadata="obj26"> Here are two examples of direct quotations in the MLA style:
               </p>
               
               <ul>
                  
                  <li>"Unwanted scar tissue may form when getting or removing a tattoo" (US Food and Drug
                     Admin.)
                  </li>
                  
                  <li>According to the U.S. Food and Drug Administration, one way to remove a tattoo is
                     "by laser treatment, which delivers short flashes of light at very high intensities
                     to the skin to break down the tattoo ink."
                  </li>
                  
               </ul>
               
               <p>In the second example, the author of the quotation is used to introduce the quotation
                  and does not need to be included at the end of the sentence. If there were a page
                  number for the source, this would be still be included in parentheses at the end in
                  the MLA style.
               </p>
               
               <p>Here's a link to the original article: http://www.fda.gov/downloads/ForConsumers/ConsumerUpdates/UCM143401.pdf</p>
               
               <p>The full citation for these quotations would be included on the Works Cited page at
                  the end of the paper:
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Works Cited</p>
               
               <p align="left">US Food and Drug Admin. Think Before You Ink: Are Tattoos Safe? <em>US Food and Drug Admin</em>. US Food and Drug Admin., Oct. 2009.Web. 19 June 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: ragz1138. Phoenix coverup during. Used under a Creative Commons Attribution-NonCommercial
                     2.0 Generic license</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>
                  <font size="5">Paraphrasing Example</font>
                  
               </h2>
               
               <p><img longdesc="US FDA flyer requesting reports on bad reactions to cosmetics." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/FDA.jpg" width="280" alt="FDA.jpg" class="resizable" height="362" border="0" hspace="19" metadata="obj27"> In a paraphrase, Val takes information from a sentence or a group of sentences and
                  puts it in her own words. This means that Val's paraphrase should not use the same
                  structure as the original sentence nor the same words (except for words that are essential
                  to the concept. In this case it would be difficult to rewrite the sentence without
                  using either "tattoo" or "ink.")
               </p>
               
               <p>&nbsp;</p>
               
               <p>Original Passage:</p>
               
               <p>"While state and local authorities oversee the practice of tattooing, ink and ink
                  colorings (pigments) used in tattoos are subject to FDA regulation as cosmetics and
                  color additives."
               </p>
               
               <p>&nbsp;</p>
               
               <p>Paraphrase:</p>
               
               <p>&nbsp;</p>
               
               <p>The US Food and Drug Administration makes rules about tattoo ink, and cities and states
                  are in charge of tattoo parlors (US Food and Drug Admin.)
               </p>
               
               <p>&nbsp;</p>
               
               <p>The complete citation information goes on the Works Cited page at the end of the paper.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Works Cited</p>
               
               <p>&nbsp;</p>
               
               <p align="left">US Food and Drug Admin. Think Before You Ink: Are Tattoos Safe? <em>US Food and Drug Admin</em>. US Food and Drug Admin., Oct. 2009.Web. 19 June 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: US Food and Drug Administration</p>
               
               <p align="center"></p>
               
               <h2>
                  <font size="5">Summarizing Example</font>
                  
               </h2>
               
               <p><img longdesc="FDA scientists" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/fda%20scientists.jpg" width="250" alt="fda scientists.jpg" class="resizable" height="166" border="0" hspace="19" metadata="obj28"> &nbsp;When Val summarizes, she puts information in her own words, but the information
                  is not taken from a sentence or two. It is taken from a larger section of the source,
                  or even the entire source.
               </p>
               
               <p>&nbsp;</p>
               
               <p>The FDA has not done enough research to decide whether tattoos are safe; they are
                  looking into chemical reactions between ink and skin and the interaction among light,
                  skin and ink (US Food and Drug Admin.)
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Works Cited</p>
               
               <p>&nbsp;</p>
               
               <p align="left">US Food and Drug Admin. Think Before You Ink: Are Tattoos Safe? <em>US Food and Drug Admin</em>. US Food and Drug Admin., Oct. 2009.Web. 19 June 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: US Food and Drug Administration</p>
               
               <p align="center"></p>
               
               <h2>
                  <font size="5">Read the following passage and then decide whether the quiz examples are plagiarized.</font>
                  
               </h2>
               
               <p><strong><em>Assume that the citation given below is on a Works Cited page at the end of the paper</em></strong>:
               </p>
               
               <p>&nbsp;</p>
               
               <p>"Obesity poses a major public health challenge. Each year, obesity contributes to
                  an estimated 112,000 preventable deaths. Obese adults are at increased risk for many
                  serious health conditions, including high blood pressure, high cholesterol, type 2
                  diabetes and its complications, coronary heart disease, stroke, gallbladder disease,
                  osteoarthritis, sleep apnea, and respiratory problems, as well as endometrial, breast,
                  prostate, and colon cancers" (Office of the Surgeon General 2)
               </p>
               
               <p>MLA Works Cited Entry:</p>
               
               <p>Office of the Surgeon General. <em>The Surgeon General's Vision for a Healthy and Fit Nation</em>. <em>U.S.</em></p>
               
               <p style="margin-left: 40.0"><em>Department of Health and Human Services, Office of the Surgeon General</em>. U.S. Department of
               </p>
               
               <p style="margin-left: 40.0">Health and Human Services, January 2010. Web. 1 Nov. 2010.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_five/quizgroup_custom.png" alt="Toggle open/close quiz group" id="quizgroup2" border="0" title="QPS"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>
                     <font size="5">Test Your Knowledge!</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p><strong><font size="5">Carefully read the original article on the</font></strong> <strong>history of tattoo in Hawaii</strong></p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Now read the following student passage and identify all the problems you see in the
                     passage. In the box below explain what is wrong with the passage and what you would
                     do to fix it.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>Captain Cook played a role in the breakdown of Hawaiian traditions including tattooing.
                  Hawaiian tattooing was called "kakau," and Hawaiians considered it important for aesthetic,
                  health and religious reasons (Pacific Islanders in Communication). Traditional Hawaiian
                  tattoos took their patterns from nature. The designs were applied by specially trained
                  kahuna, experts in one or more critical task, who applied pigment to the skin with
                  a needle made from bone, tied to a stick and struck by a mallet. The process was a
                  big secret, and the tools were broken afterward. In the nineteenth century this practice
                  began to disappear. "Kakau was discouraged and suppressed."
               </p>
               
               <p>&nbsp;</p>
               
               <p>Pacific Islanders in Communication. "History of Tattoo." <em>Skin Stories: the Art and Culture of Polynesian</em></p>
               
               <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<em>Tattoo</em>. PBS, 2003. Web. 3 Aug. 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_five/testyourself_custom.png" alt="Toggle open/close quiz question" border="0" title="Question 5"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2 align="left">&nbsp;</h2>
               
               <p>
                  <img longdesc="A photo of a paper with A grade on the top." align="left" src="http://valenciacollege.edu/library/tutorials/module_five/A%20grade.jpg" width="400" alt="A grade.jpg" class="resizable" height="266" border="0" hspace="20" metadata="obj10">
                  
               </p>
               
               <p>...English Class, the following Thursday.</p>
               
               <p><strong>PROFESSOR SAGE</strong>: I was pleased with your papers. There were a lot of interesting topics, and most
                  of you did very well with your citations. I am going to return them now. Please read
                  my comments before you ask any questions about your grade.
               </p>
               
               <p>[Sounds of papers being distributed.]</p>
               
               <p><strong>MATT</strong>: [to VAL] How did you do?
               </p>
               
               <p><strong>VAL</strong>: An A!
               </p>
               
               <p><strong>MATT</strong>: Me, too! You know, writing that paper was not as hard as I thought it would be.
                  Breaking the process up into steps really helps. I think I'll be able to do this again
                  for other classes.
               </p>
               
               <p><strong>VAL</strong>: I think so, too. Comp II here we come!
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft</p>
               
               <p align="center"></p>
               
               <h2>&nbsp;Conclusion</h2>
               
               <p>
                  <img longdesc="An illustration of a flower" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/flower1.JPG" width="300" alt="flower1.JPG" class="resizable" height="300" border="0" metadata="obj11">
                  
               </p>
               
               <p>This concludes Tutorial #5: Using Information Ethically. In this module you learned:</p>
               
               <ul>
                  
                  <li>What plagiarism is</li>
                  
                  <li>The consequences of plagiarism</li>
                  
                  <li>When to use different citation styles</li>
                  
                  <li>When to cite</li>
                  
                  <li>When citation is not necessary</li>
                  
               </ul>
               
               <p>You also practiced identifying:</p>
               
               <ul>
                  
                  <li>situations that constitute plagiarism</li>
                  
                  <li>common knowledge</li>
                  
                  <li>plagiarized passages&nbsp;</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Microsoft Clip Gallery</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall_print.pcf">©</a>
      </div>
   </body>
</html>