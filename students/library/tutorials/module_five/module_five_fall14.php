<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Direct Quotations Examples | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall14.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Direct Quotations Examples</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="directquotationsexamples" id="directquotationsexamples"></a><h2>
                  <font size="5" style="line-height: 1">Direct Quotations Examples</font>
                  
               </h2>
               
               <p>&nbsp;</p>
               
               <p><img longdesc="ada_files/ada_image12.html" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/tattooremoval.jpg" width="200" alt="tattooremoval.jpg" class="resizable" height="300" border="0" hspace="19" metadata="obj26"> Here are two examples of direct quotations in the MLA style:
               </p>
               
               <ul>
                  
                  <li>"Unwanted scar tissue may form when getting or removing a tattoo" (US Food and Drug
                     Admin.)
                  </li>
                  
                  <li>According to the U.S. Food and Drug Administration, one way to remove a tattoo is
                     "by laser treatment, which delivers short flashes of light at very high intensities
                     to the skin to break down the tattoo ink."
                  </li>
                  
               </ul>
               
               <p>In the second example, the author of the quotation is used to introduce the quotation
                  and does not need to be included at the end of the sentence. If there were a page
                  number for the source, this would be still be included in parentheses at the end in
                  the MLA style.
               </p>
               
               <p>Here's a link to the original article: <a target="_blank" href="http://www.fda.gov/downloads/ForConsumers/ConsumerUpdates/UCM143401.pdf">http://www.fda.gov/downloads/ForConsumers/ConsumerUpdates/UCM143401.pdf</a></p>
               
               <p>The full citation for these quotations would be included on the Works Cited page at
                  the end of the paper:
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Works Cited</p>
               
               <p align="left">US Food and Drug Admin. Think Before You Ink: Are Tattoos Safe? <em>US Food and Drug Admin</em>. US Food and Drug Admin., Oct. 2009.Web. 19 June 2012.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: <a target="_blank" href="http://www.flickr.com/photos/ragzrejected/">ragz1138</a>. <a target="_blank" href="http://www.flickr.com/photos/ragzrejected/4901947345/">Phoenix coverup during</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc/2.0/deed.en">Creative Commons Attribution-NonCommercial 2.0 Generic license</a></font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall14.html#">return to top</a> | <a href="Module_Five_Fall13.html">previous page</a> | <a href="Module_Five_Fall15.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall14.pcf">©</a>
      </div>
   </body>
</html>