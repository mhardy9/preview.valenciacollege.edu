<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>When Not To Cite | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>When Not To Cite</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="whennottocite" id="whennottocite"></a><h2>
                  <a id="bookmark" name="whennottocite"></a> When Not To Cite
               </h2>
               
               <p>The following do not require citation:</p>
               
               <ul>
                  
                  <li>Val's own thoughts</li>
                  
                  <li>common knowledge</li>
                  
               </ul>
               
               <p>
                  <img longdesc="ada_files/ada_image10.html" align="right" src="http://valenciacollege.edu/library/tutorials/module_five/periodictable.jpg" width="500" alt="periodictable.jpg" class="resizable" height="346" border="0" hspace="19" metadata="obj19">
                  
               </p>
               
               <p>
                  <strong>Common Knowledge</strong>
                  
               </p>
               
               <p>If information is "common knowledge," or can be gathered from many sources, it does
                  not need a specific cited source directly linked to it.&nbsp; The same information appearing
                  in three or more reputable sources is considered common knowledge. Examples of this
                  often include:
               </p>
               
               <ul>
                  
                  <li>historical events (Terrorists crashed two airplanes into the World Trade Center on
                     September 11, 2001.),
                  </li>
                  
                  <li>places (Disney World is located south of Orlando, Florida),</li>
                  
                  <li>famous people (Barack Obama is the president of the United States.),</li>
                  
                  <li>some scientific facts (C is the chemical symbol for carbon),</li>
                  
                  <li>information covered in the class for which Val is writing a paper.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>However, if Val has any doubt, it's best for her to cite a source!</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: An author's personal thoughts and common knowledge do not require
                     an in-text citation.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: <a target="_blank" href="http://www.flickr.com/photos/pmtorrone/">pt</a>. <a target="_blank" href="http://www.flickr.com/photos/pmtorrone/150008091/">periodictable</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc-nd/2.0/deed.en">Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Generic license</a></font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall11.html#">return to top</a> | <a href="Module_Five_Fall10.html">previous page</a> | <a href="Module_Five_Fall12.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall11.pcf">©</a>
      </div>
   </body>
</html>