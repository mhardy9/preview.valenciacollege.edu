<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Citation Styles | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_five/module_five_fall8.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_five/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_five/">Module Five</a></li>
               <li>Citation Styles</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="citationstyles" id="citationstyles"></a><h2>
                  <a id="bookmark" name="citationformats"></a> <font size="5" style="line-height: 1">Citation Styles</font>
                  
               </h2>
               
               <p>Val has already created an annotated bibliography in the MLA style, but she has heard
                  that other classes use the APA style. She wonders when she would use one instead of
                  the other.
               </p>
               
               <p><img longdesc="ada_files/ada_image6.html" align="left" src="http://valenciacollege.edu/library/tutorials/module_five/chicago2.jpg" width="106" alt="chicago2.jpg" class="resizable" height="160" border="0" hspace="19" metadata="obj25"> There are actually lots of different styles used for creating citations. These include
               </p>
               
               <ul>
                  
                  <li>AMA style</li>
                  
                  <li><a target="_blank" href="../../mla-apa-chicago-guides/default.html">APA style</a></li>
                  
                  <li>BibTeX style</li>
                  
                  <li><a target="_blank" href="../../mla-apa-chicago-guides/index.html">Chicago style</a></li>
                  
                  <li><a target="_blank" href="../../mla-apa-chicago-guides/default.html">MLA style</a></li>
                  
               </ul>
               
               <p>At Valencia the two commonly used formats for creating citations are APA and MLA,
                  but some professors may require other styles. If you are unsure, always ask your professor
                  which style to use.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <u>The bottom line: There are many citation styles. Ask your professor which one to use.</u>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">Image credit: Syndetic Solutions</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Five_Fall8.html#">return to top</a> | <a href="Module_Five_Fall7.html">previous page</a> | <a href="Module_Five_Fall9.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_five/module_five_fall8.pcf">©</a>
      </div>
   </body>
</html>