<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Illustrations | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Illustrations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="illustrations%C2%A0" id="illustrations%C2%A0"></a><h2>Illustrations&nbsp;</h2>
               
               <p>Sometimes you may use an image purely as an illustration, e.g.</p>
               
               <ul>
                  
                  <li>a photograph of the person you are discussing.</li>
                  
                  <li>clip art of medicine bottles in a presentation on prescription drugs.</li>
                  
               </ul>
               
               <p>Such illustrations do not include information important to the point you are making
                  and need not be included as full citations on your Works Cited or References page.
               </p>
               
               <p>When considering whether to include brief citations on your visual aid, remember the
                  general principle of giving credit to others for their work but also consult your
                  professor for his or her preferences.
               </p>
               
               <p>Keep in mind that if you were giving other types of speeches, e.g.</p>
               
               <ul>
                  
                  <li>a sales presentation.</li>
                  
                  <li>a presentation to your boss and colleagues.</li>
                  
                  <li>a conference presentation.</li>
                  
                  <li>an interview presentation, etc.,</li>
                  
               </ul>
               
               <p>the rules may be different than for a college speech class. In those cases, consider
                  professional standards in your field.
               </p>
               
               <p>If you do include citations on your visual aid, follow the guidelines previously given
                  for graphs, charts, audio and video files.
               </p>
               
               <p>Example:</p>
               
               <p align="center">
                  <img hspace="30" vspace="10" class="resizable" height="300" longdesc="ada_files/ada_image5.html" alt="freefloating.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/freefloating.jpg" width="400" border="0">
                  
               </p>
               
               <p align="center">Source: NASA. "Free Floating (1984)."</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations17.html#">return to top</a> | <a href="inspeechcitations16.html">previous page</a> | <a href="inspeechcitations18.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations17.pcf">©</a>
      </div>
   </body>
</html>