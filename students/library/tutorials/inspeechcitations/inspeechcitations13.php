<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Citing Charts, Tables, Images, Videos, etc. | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations13.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Citing Charts, Tables, Images, Videos, etc.</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="citingcharts,tables,images,videos,etc." id="citingcharts,tables,images,videos,etc."></a><h2>
                  <a id="bookmark" name="charts"></a> Citing Charts, Tables, Images, Videos, etc.
               </h2>
               
               <p>The MLA and APA publication manuals are designed for published written work and do
                  not give explicit guidelines for how to cite audiovisual materials used as a visual
                  aid while speaking, so here are some suggested guidelines:
               </p>
               
               <p>If your chart, table, audio file, or video file</p>
               
               <ul>
                  
                  <li>provides data that you are using to support your point.</li>
                  
                  <li>was not created by you from your own data.</li>
                  
               </ul>
               
               <p>cite it within your speech and provide a full citation on your Works Cited or References
                  page.
               </p>
               
               <p>&nbsp;</p>
               
               <p>To cite within your speech, insert a line underneath the chart, table, audio or video
                  file including at least
               </p>
               
               <ul>
                  
                  <li>Source:</li>
                  
                  <li>the author or creator (This might be an organization such as the U.S. Census Bureau
                     in the example given on the next page.)
                  </li>
                  
               </ul>
               
               <p>Including at least this much information will lead your audience to the correct entry
                  on your works cited page.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Also consider including additional information if it will help your audience understand
                  the context, such as:
               </p>
               
               <ul>
                  
                  <li>the title (For example, if you were including a video you found on YouTube, it might
                     be helpful for your audience to know the name of it.)
                  </li>
                  
                  <li>the date (If this information is not given elsewhere, consider including it, especially
                     for data tables and graphs, cases in which the date is important.)
                  </li>
                  
                  <li>the organization or web site (This would be helpful in such cases as the YouTube video;
                     your audience might like to know that you found the video on YouTube.)
                  </li>
                  
               </ul>
               
               <h2>&nbsp;</h2>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations13.html#">return to top</a> | <a href="inspeechcitations12.html">previous page</a> | <a href="inspeechcitations14.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations13.pcf">©</a>
      </div>
   </body>
</html>