<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Works Cited | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations19.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Works Cited</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="workscited" id="workscited"></a><h2>Works Cited</h2>
               
               <p align="center">Works Cited</p>
               
               <p>&nbsp;</p>
               
               <p>"Health Disparities and the Affordable Care Act." <em>HealthCare.gov</em>. U.S. Department of Health and Human
               </p>
               
               <p style="margin-left: 40.0px">&nbsp;Services. 2010. Web. 13 July 2010.</p>
               
               <p><em>Information Literacy: the Perils of Online Research</em>. Dir. Amy S. Weber and Ryan Demetrak.
               </p>
               
               <p style="margin-left: 40.0px">Cambridge Educational, 2006. <em>Films on Demand</em>. Web. 13 July 2010.
               </p>
               
               <p>Jones, Jeffrey M. "Greater Optimism about U.S. Health System Coverage, Costs." &nbsp;<em>Gallup</em>. 19 Nov. 2009.&nbsp;
               </p>
               
               <p style="margin-left: 40.0px">Web. 13 July 2010.</p>
               
               <p>Kelland, Kate. "Analysis: Health and Austerity: When Budget Cuts Cost Lives." <em>Reuters</em>. 4 July 2010. Web.
               </p>
               
               <p style="margin-left: 40.0px">13 July 2010.</p>
               
               <p>
                  <span>
                     <font color="black">Kirszner, Laurie G. and Stephen R. Mandell. <em>TheWadsworthHandbook</em>. 9<sup>th</sup> ed.Boston: Wadsworth, 2011.</font>
                     </span>
                  
               </p>
               
               <p>Obama, Barack. "Remarks by the President to a Joint Session of Congress on Health
                  Care." U.S. Capitol,
               </p>
               
               <p style="margin-left: 40.0px">Washington, DC.&nbsp; 9 Sept. 2009. Web. 13 July 2010.</p>
               
               <p>Samuelson, Robert. "A Savings Mirage on Health Care." <em>Washington Post</em> 14 Dec. 2009: A21. <em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>
                  <span>
                     <font color="black">Stolley, Karl and Allen Brizee. "Is it Plagiarism Yet?" <em>Purdue Online Writing Lab</em>. Purdue U., 21 April 2010.</font>
                     </span>
                  
               </p>
               
               <p style="margin-left: 40.0px">
                  <span>
                     <font color="black">Web.13 Aug. 2010.</font>
                     </span>
                  
               </p>
               
               <p>Thomas, Mike. "Health Care: Can It Stand Up to the Challenges?" <em>Orlando Sentinel</em> 24 Mar. 2010: B1.
               </p>
               
               <p style="margin-left: 40.0px">&nbsp;<em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations19.html#">return to top</a> | <a href="inspeechcitations18.html">previous page</a> | <a href="inspeechcitations20.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations19.pcf">©</a>
      </div>
   </body>
</html>