<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Common Knowledge | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Common Knowledge</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="commonknowledge" id="commonknowledge"></a><h2>
                  <a id="bookmark" name="common"></a> Common Knowledge
               </h2>
               
               <p>Common knowledge includes</p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Facts that are well-known and not widely disputed, often historical.</p>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td>
                        <a name="examplesofwell-knownfacts" id="examplesofwell-knownfacts"></a><h2 align="center">Examples of well-known facts</h2>
                        
                     </td>
                     
                     <td>
                        <a name="periodictableoftheelements" id="periodictableoftheelements"></a><h2 align="center">Periodic Table of the Elements</h2>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td>
                        
                        <ul>
                           
                           <li>The Declaration of Independence was signed in 1776.</li>
                           
                           <li>Barack Obama is the 44<sup>th</sup> President of the United States.
                           </li>
                           
                           <li>Disney World is located south of Orlando, Florida.</li>
                           
                           <li>Thanksgiving is celebrated on the 4<sup>th</sup> Thursday in November in the United States.
                           </li>
                           
                           <li>The chemical symbol for carbon is C.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td>
                        
                        <p align="center"><img hspace="30" vspace="10" class="resizable" height="346" longdesc="ada_files/ada_image1.html" alt="Periodic Table of the Elements" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/periodictable.jpg" width="500" border="0"></p>
                        
                        <p>(Image credit: <a href="http://www.flickr.com/photos/pmtorrone/" target="_blank">pt</a>. <a href="http://www.flickr.com/photos/pmtorrone/150008091/" target="_blank">periodictable</a>. Used under a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/deed.en" target="_blank">Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Generic license</a>)&nbsp;
                        </p>
                        
                        <p>Fig. 1: Torrone, Phillip. <em>Periodictable</em>. 19 Aug. 2005. <em>Flickr</em>. Web. 21 Aug. 2010.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>Experts do not always agree on what else is included in common knowledge.</p>
               
               <p>Karl Stolley and Allen Brizee of the Purdue Online Writing Lab, a widely used and
                  highly regarded source on citation, suggest, "Generally speaking, you can regard something
                  as common knowledge if you find the same information undocumented in at least five
                  credible sources."
               </p>
               
               <p>Many sources such as Laurie Kirszner and Stephen Mandell's Wadsworth Handbook include
                  "familiar sayings and well-known quotations" (223)
               </p>
               
               <p>Kirszner &amp; Mandell also include "information most readers probably know" (223).</p>
               
               <p>Information your readers (or listeners) already know may be context-dependent, i.e.
                  it may vary based on which class you are in, who the other students are, which discipline
                  (English, Math, Biology, etc.) is involved, etc.
               </p>
               
               <p>&nbsp;</p>
               
               <p>When in doubt, cite. It is better to have some unnecessary citations than to neglect
                  needed ones.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations4.html#">return to top</a> | <a href="inspeechcitations3.html">previous page</a> | <a href="inspeechcitations5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations4.pcf">©</a>
      </div>
   </body>
</html>