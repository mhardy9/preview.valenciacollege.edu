<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>In-Text Citation Quiz | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations12.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>In-Text Citation Quiz</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="in-textcitationquiz" id="in-textcitationquiz"></a><h2 align="left">In-Text Citation Quiz</h2>
               
               <p>Connect to the following story from the Orlando Sentinel and decide how you would
                  do an MLA in-text citation for it:
               </p>
               
               <p>
                  <a href="http://www.orlandosentinel.com/health/os-indoor-temperatures-making-us-fatt20110125,0,6339927.story">Orlando Sentinel story</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/inspeechcitations/spacer.gif" name="check23" alt="" id="check23"><a href="javascript:parent.toggletable('quizpopper23')" id="a23" title="Question 3"><img src="http://valenciacollege.edu/library/tutorials/inspeechcitations/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 3" border="0"></a> 
               </p>
               
               <div id="quizpopper23" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 2</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #eeeeee; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f23" id="f23">What would be a correct MLA in-text citation for this quotation taken from the story
                        above: "A new study by British researchers suggests that staying indoors longer during
                        the winter — and turning up the thermostat — could be making us fatter."?<br><div style="margin: 1em 15px;">
                           <input type="radio" name="q23" value="a" id="q23a">&nbsp;<label for="q23a"><strong>a.</strong>&nbsp;"A new study by British researchers suggests that staying indoors longer during the
                              winter — and turning up the thermostat — could be making us fatter" (Orlando Sentinel).</label><br><input type="radio" name="q23" value="b" id="q23b">&nbsp;<label for="q23b"><strong>b.</strong>&nbsp;"A new study by British researchers suggests that staying indoors longer during the
                              winter — and turning up the thermostat — could be making us fatter" ("Staying Indoors").</label><br><input type="radio" name="q23" value="c" id="q23c">&nbsp;<label for="q23c"><strong>c.</strong>&nbsp;"A new study by British researchers suggests that staying indoors longer during the
                              winter — and turning up the thermostat — could be making us fatter" (Shrieves).</label><br>
                           
                        </div>
                        
                        <div align="center"><input onclick="parent.check_q(23, 3, 1, false)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done23" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed23" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations12.html#">return to top</a> | <a href="inspeechcitations11.html">previous page</a> | <a href="inspeechcitations13.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations12.pcf">©</a>
      </div>
   </body>
</html>