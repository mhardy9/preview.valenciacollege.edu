<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>A Word on Copyright | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations18.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>A Word on Copyright</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="awordoncopyright" id="awordoncopyright"></a><h2>A Word on Copyright</h2>
               
               <p>Note that plagiarism and copyright are two separate issues. Citing a photo, video
                  or other media file does not mean that you have used the material legally.
               </p>
               
               <p>Generally using these types of materials:</p>
               
               <ul>
                  
                  <li>once.</li>
                  
                  <li>in class.</li>
                  
                  <li>for a student presentation.</li>
                  
               </ul>
               
               <p>is allowed under fair use guidelines. See <a href="http://www.copyright.gov/fls/fl102.html">U.S. Copyright Office - Fair Use</a> and <a href="http://fairuse.stanford.edu/Copyright_and_Fair_Use_Overview/chapter9/9-b.html">Stanford Copyright and Fair Use</a> for more information.
               </p>
               
               <p>However, if you intend to</p>
               
               <ul>
                  
                  <li>make your presentation available over the Internet,</li>
                  
                  <li>to present it to an audience more than once,</li>
                  
                  <li>to give a speech for which you will be paid,</li>
                  
                  <li>or to give a speech to the general public,</li>
                  
               </ul>
               
               <p>then consider the copyright laws for including photographs, videos, etc. Seek permission
                  and provide appropriate legal notices in the body of your presentation. In some cases,
                  a royalty must be paid in order to use others' creations. In other cases, royalty-free
                  work is available for use under certain conditions.
               </p>
               
               <p>The photo credit given below is an appropriate one for a <a href="http://creativecommons.org/" target="_blank">Creative Commons</a> image being reproduced on a web site for educational purposes.
               </p>
               
               <p align="center">
                  <img hspace="30" vspace="10" class="resizable" height="427" longdesc="ada_files/ada_image6.html" alt="machupicchu.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/machupicchu.jpg" width="640" border="0">
                  
               </p>
               
               <p align="center">(Photo credit: <a href="http://www.flickr.com/photos/lukeredmond/" target="_blank">Luke Redmond</a>.<a href="http://www.flickr.com/photos/lukeredmond/886434683/" target="_blank">Machu Picchu (The Lost City of the Incas)</a>.Used under a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/deed.en" target="_blank">Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Generic license</a>.)
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations18.html#">return to top</a> | <a href="inspeechcitations17.html">previous page</a> | <a href="inspeechcitations19.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations18.pcf">©</a>
      </div>
   </body>
</html>