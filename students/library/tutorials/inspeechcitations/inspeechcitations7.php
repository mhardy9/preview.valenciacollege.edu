<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Oral Footnotes Continued | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Oral Footnotes Continued</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="oralfootnotescontinued" id="oralfootnotescontinued"></a><h2>Oral Footnotes Continued</h2>
               
               <p>It is common for newspaper articles, books, web sites and other sources to quote other
                  sources in the course of an article. Be sure to quote the original source&nbsp; (person
                  or organization) of the information in your oral footnote and then the article in
                  which you found that information on your works cited page.
               </p>
               
               <p>For example, in a March 24, 2010 article in the <em>Orlando Sentinel</em>, columnist Mike Thomas quoted Bill McCollum: "The U.S. Constitution grants no authority
                  to Congress to compel citizens to purchase health insurance when those citizens choose
                  not to enter the health-care market" (B1).
               </p>
               
               <p>If you used this quotation from Bill McCollum in a speech you would say something
                  like this:&nbsp; According to Bill McCollum, Attorney General for the State of Florida,
                  "The U.S. Constitution grants no authority to Congress to compel citizens to purchase
                  health insurance when those citizens choose not to enter the health-care market."
               </p>
               
               <p>Your Works Cited page would include the full citation for the newspaper article that
                  included this quote:
               </p>
               
               <p>Thomas, Mike. "Health Care: Can It Stand Up to the Challenges?" <em>Orlando Sentinel</em> 24 Mar. 2010: B1.
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>Notice that the full citation does not mention Bill McCollum; it is not necessary
                  to go back and find the original source for Mike Thomas' quotation and include it
                  on your Works Cited Page. But it is necessary to mention Bill McCollum as the sources
                  of the words in your oral footnote.
               </p>
               
               <p>Incorrect Examples:</p>
               
               <p>According to the <em>Orlando Sentinel</em>, "The U.S. Constitution grants no authority to Congress to compel citizens to purchase
                  health insurance when those citizens choose not to enter the health-care market."
               </p>
               
               <p>According to Mike Thomas, "The U.S. Constitution grants no authority to Congress to
                  compel citizens to purchase health insurance when those citizens choose not to enter
                  the health-care market."
               </p>
               
               <p>These are not the words of Mike Thomas nor the official position of the <em>Orlando Sentinel</em>.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations7.html#">return to top</a> | <a href="inspeechcitations6.html">previous page</a> | <a href="inspeechcitations8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations7.pcf">©</a>
      </div>
   </body>
</html>