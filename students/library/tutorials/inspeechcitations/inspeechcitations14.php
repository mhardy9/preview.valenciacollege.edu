<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Example | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations14.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Example</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="example" id="example"></a><h2>Example</h2>
               
               <p>Here is an example of a graph created from data on a table in the U.S. Census Bureau's
                  <em>Statistical Abstract.</em></p>
               
               <p align="center">Registered and Actual Voters in the 2008 Presidential Election</p>
               
               <p align="center">
                  <img hspace="30" vspace="10" class="resizable" height="452" longdesc="ada_files/ada_image4.html" alt="voters3.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/voters3.jpg" width="752" border="0">
                  
               </p>
               
               <p align="center">&nbsp;Source: U.S. Census Bureau.</p>
               
               <p>If including more than one graph from the same source, e.g. U.S. Census Bureau, give
                  your audience enough information to distinguish the two graphs on your Works Cited
                  or References page. In this example, putting the name of the publication and the table
                  number, as below, should be enough to distinguish this graph from other U.S. Census
                  Bureau graphs:
               </p>
               
               <p align="center">Source: U.S. Census Bureau. <em>2011 Statistical Abstract</em>. Table 416. "Voting-Age Population--Reported Registration and Voting by Selected
                  Characteristics: 1996 to 2008."
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">To create a full citation for this table to put on your Works Cited or References
                  page, cite it as you would any other web page.
               </p>
               
               <p align="left">APA:</p>
               
               <p align="left">U.S. Census Bureau. (2011). <em>Table 416. Voting-age population--Reported registration and voting</em></p>
               
               <p align="left"><em>by selected characteristics: 1996 to 2008</em>. Retrieved from
               </p>
               
               <p align="left">http://www.census.gov/compendia/statab/2011/tables/11s0416.pdf</p>
               
               <p align="left">MLA:</p>
               
               <p>U.S. Census Bureau. "Table 416. Voting-Age Population--Reported Registration and Voting
                  by
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Selected Characteristics: 1996 to 2008." <em>2011 Statistical Abstract</em>. U.S. Census Bureau,
               </p>
               
               <p>2011. Web. 24 Jan. 2011.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations14.html#">return to top</a> | <a href="inspeechcitations13.html">previous page</a> | <a href="inspeechcitations15.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations14.pcf">©</a>
      </div>
   </body>
</html>