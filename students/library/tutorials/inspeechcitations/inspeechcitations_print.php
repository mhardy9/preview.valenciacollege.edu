<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>In Speech Citations: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>In Speech Citations:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>In Speech Citations:</strong><br>To Cite or Not to Cite? Part 4 of 5
               </p>
               
               
               <h2>In Speech Citations</h2>
               
               <p><strong>Outcome:</strong> You will understand and be able to use oral footnotes and in-text citations.
               </p>
               
               <p><strong>Approximate Completion Time</strong>: 45 minutes.
               </p>
               
               <p>
                  <strong>
                     <font color="#8000FF">**To receive credit for completing this portion of the tutorial you must fill out
                        the form on page 20.**</font>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>An Introduction to In-Speech Citations</h2>
               
               <p>Including a full list of citations on a Works Cited or Reference page is a good start
                  to your speech, but it is not enough. In this section you will learn how to include
                  citations within your speech and your presentation aids:
               </p>
               
               <p>&nbsp;</p>
               
               <p>When to Cite.</p>
               
               <p>Common Knowledge.</p>
               
               <p>Oral Footnotes.</p>
               
               <p>In-Text Citations.</p>
               
               <p>Citing Charts, Tables, Images, Videos, etc.</p>
               
               <p align="center"></p>
               
               <h2> When to Cite</h2>
               
               <p>There are only two types of information that do not require a citation:</p>
               
               <ul>
                  
                  <li>Your own original thoughts and creations (pictures, music, surveys, videos).</li>
                  
                  <li>Common knowledge.</li>
                  
               </ul>
               
               <p>Everything else requires a citation. When in doubt, cite.</p>
               
               <p>&nbsp;</p>
               
               <p>Watch a brief video clip on using images and paraphrasing. (Note: for closed captioned
                  version, select the Windows Media version of the clip from the drop-down menu underneath
                  the video. Then click the icon on the right-hand side on the tool bar beneath the
                  video to turn the captioning on.)
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> Common Knowledge</h2>
               
               <p>Common knowledge includes</p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Facts that are well-known and not widely disputed, often historical.</p>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td>
                        
                        <h2 align="center">Examples of well-known facts</h2>
                        
                     </td>
                     
                     <td>
                        
                        <h2 align="center">Periodic Table of the Elements</h2>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td>
                        
                        <ul>
                           
                           <li>The Declaration of Independence was signed in 1776.</li>
                           
                           <li>Barack Obama is the 44<sup>th</sup> President of the United States.
                           </li>
                           
                           <li>Disney World is located south of Orlando, Florida.</li>
                           
                           <li>Thanksgiving is celebrated on the 4<sup>th</sup> Thursday in November in the United States.
                           </li>
                           
                           <li>The chemical symbol for carbon is C.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td>
                        
                        <p align="center"><img hspace="30" vspace="10" class="resizable" height="346" longdesc="This is an image of the Periodic Table of the Elements, used in chemistry. This is an example of common knowledge." alt="Periodic Table of the Elements" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/periodictable.jpg" width="500" border="0"></p>
                        
                        <p>(Image credit: pt. periodictable. Used under a Creative Commons Attribution-NonCommercial-NoDerivs
                           2.0 Generic license)&nbsp;
                        </p>
                        
                        <p>Fig. 1: Torrone, Phillip. <em>Periodictable</em>. 19 Aug. 2005. <em>Flickr</em>. Web. 21 Aug. 2010.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>Experts do not always agree on what else is included in common knowledge.</p>
               
               <p>Karl Stolley and Allen Brizee of the Purdue Online Writing Lab, a widely used and
                  highly regarded source on citation, suggest, "Generally speaking, you can regard something
                  as common knowledge if you find the same information undocumented in at least five
                  credible sources."
               </p>
               
               <p>Many sources such as Laurie Kirszner and Stephen Mandell's Wadsworth Handbook include
                  "familiar sayings and well-known quotations" (223)
               </p>
               
               <p>Kirszner &amp; Mandell also include "information most readers probably know" (223).</p>
               
               <p>Information your readers (or listeners) already know may be context-dependent, i.e.
                  it may vary based on which class you are in, who the other students are, which discipline
                  (English, Math, Biology, etc.) is involved, etc.
               </p>
               
               <p>&nbsp;</p>
               
               <p>When in doubt, cite. It is better to have some unnecessary citations than to neglect
                  needed ones.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>When to Cite Quiz</h2>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img id="quizgroup1" alt="Toggle open/close quiz group" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/quizgroup_custom.png" title="When to Cite" border="0"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> Oral Footnotes</h2>
               
               <p>Speakers use oral footnotes in a speech to establish the credibility of the information
                  presented. The audience should be assured that the information comes from a reliable
                  source which can be looked up later by an interested listener.
               </p>
               
               <p>Examples:</p>
               
               <ul>
                  
                  <li>In a September 2009 speech to Congress, President Obama stated, "It has now been nearly
                     a century since Theodore Roosevelt first called for health care reform. And ever since,
                     nearly every President and Congress, whether Democrat or Republican, has attempted
                     to meet this challenge in some way."
                  </li>
                  
                  <li>According to a November 2009 <em>Gallup Poll</em>, 38% of Americans rate healthcare coverage as excellent or good.
                  </li>
                  
               </ul>
               
               <p>In the examples above, "In a September 2009 speech to Congress, President Obama stated"
                  and "According to a November 2009 <em>Gallup Poll</em>" are the oral footnotes. They give the listener a brief idea of the source of the
                  information and introduce the quote and statistic given above. When composing an oral
                  footnote include at least:
               </p>
               
               <ul>
                  
                  <li>the person or organization who produced the information.</li>
                  
               </ul>
               
               <p>You may also want to include (and your professor may require):</p>
               
               <ul>
                  
                  <li>the date of the information, e.g. 2009 in the examples above.</li>
                  
                  <li>the credentials of the person being quoted, e.g. President of the United States.</li>
                  
                  <li>the name of the publication or program in which the information appeared, e.g. The
                     <em>Wall Street Journal</em> or The <em>CBS Evening News</em>.
                  </li>
                  
               </ul>
               
               <p>Make sure you are clear on your professor's requirements before you give your speech.</p>
               
               <p>Oral footnotes refer the listener to the full citation on the References or Works
                  Cited page. So for the two examples given above, the speaker would include a full
                  citation to President Obama's speech and to the <em>Gallup Poll</em> on the Works Cited page. MLA Works Cited Page for a speech using those two examples:
               </p>
               
               <p align="center">Works Cited</p>
               
               <p>Jones, Jeffrey M. "Greater Optimism about U.S. Health System Coverage, Costs." &nbsp;<em>Gallup</em>. 19 Nov. 2009.&nbsp;
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Web. 13 July 2010.</p>
               
               <p>Obama, Barack. "Remarks by the President to a Joint Session of Congress on Health
                  Care." U.S. Capitol,
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Washington, DC.&nbsp; 9 Sept. 2009. Web. 13 July 2010.</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Oral Footnotes Continued</h2>
               
               <p>It is common for newspaper articles, books, web sites and other sources to quote other
                  sources in the course of an article. Be sure to quote the original source&nbsp; (person
                  or organization) of the information in your oral footnote and then the article in
                  which you found that information on your works cited page.
               </p>
               
               <p>For example, in a March 24, 2010 article in the <em>Orlando Sentinel</em>, columnist Mike Thomas quoted Bill McCollum: "The U.S. Constitution grants no authority
                  to Congress to compel citizens to purchase health insurance when those citizens choose
                  not to enter the health-care market" (B1).
               </p>
               
               <p>If you used this quotation from Bill McCollum in a speech you would say something
                  like this:&nbsp; According to Bill McCollum, Attorney General for the State of Florida,
                  "The U.S. Constitution grants no authority to Congress to compel citizens to purchase
                  health insurance when those citizens choose not to enter the health-care market."
               </p>
               
               <p>Your Works Cited page would include the full citation for the newspaper article that
                  included this quote:
               </p>
               
               <p>Thomas, Mike. "Health Care: Can It Stand Up to the Challenges?" <em>Orlando Sentinel</em> 24 Mar. 2010: B1.
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>Notice that the full citation does not mention Bill McCollum; it is not necessary
                  to go back and find the original source for Mike Thomas' quotation and include it
                  on your Works Cited Page. But it is necessary to mention Bill McCollum as the sources
                  of the words in your oral footnote.
               </p>
               
               <p>Incorrect Examples:</p>
               
               <p>According to the <em>Orlando Sentinel</em>, "The U.S. Constitution grants no authority to Congress to compel citizens to purchase
                  health insurance when those citizens choose not to enter the health-care market."
               </p>
               
               <p>According to Mike Thomas, "The U.S. Constitution grants no authority to Congress to
                  compel citizens to purchase health insurance when those citizens choose not to enter
                  the health-care market."
               </p>
               
               <p>These are not the words of Mike Thomas nor the official position of the <em>Orlando Sentinel</em>.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Oral Footnotes -- Test Your Knowledge</h2>
               
               <p>Read the article "A Savings Mirage on Health Care." You will need your borrower ID
                  number from the back of your Valencia ID (either your VID or a longer number beginning
                  with 259). Your PIN should be the last four digits of your VID.
               </p>
               
               <p>Click on Quiz Group below to quiz yourself on your knowledge of oral footnotes.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img id="quizgroup2" alt="Toggle open/close quiz group" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/quizgroup_custom.png" title="Oral Footnotes Quiz" border="0"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> In-Text Citations</h2>
               
               <p>In-text citations are the written equivalent of oral footnotes, giving the reader
                  a brief idea of the source of information. In-text citations are sometimes called
                  parenthetical citations because they appear in parenthesis in the body of the text
                  in both the APA and MLA styles. Although more commonly used in papers, in-text citations
                  are sometimes used in speech classes and incorporated into
               </p>
               
               <ul>
                  
                  <li>outlines,</li>
                  
                  <li>speaker notes,</li>
                  
                  <li>PowerPoint presentations.</li>
                  
               </ul>
               
               <p>Check with your instructor to determine his or her preferences on including in-text
                  citations in any of the above.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>In-Text Citation Examples</h2>
               
               <p>In the APA format, an in-text citation includes</p>
               
               <ul>
                  
                  <li>the author's last name</li>
                  
                  <li>date (use n.d. if no date is available),</li>
                  
                  <li>page number, if available.</li>
                  
               </ul>
               
               <p>Example: "If government insures 30 million or more Americans, health spending will
                  rise" (Samuelson, 2009, p. A21).
               </p>
               
               <p>&nbsp;</p>
               
               <p>For MLA style, include</p>
               
               <ul>
                  
                  <li>the author's last name.</li>
                  
                  <li>the page number, if given.</li>
                  
               </ul>
               
               <p>Example: "If government insures 30 million or more Americans, health spending will
                  rise" (Samuelson A21).
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img hspace="30" vspace="10" class="resizable" height="332" longdesc="This photograph shows conservative activists protesting the Affordable Health Care for All Americans Act." alt="Conservative Activists" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/protesters.jpg" width="500" border="0">
                  
               </p>
               
               <p>(Photo credit: TalkMediaNews. Conservative Activists Hold a Second 'House Call.' Used
                  under a Creative Commons Attribution-NonCommercial-ShareAlike 2.0 Generic license.)
               </p>
               
               <p>Fig. 2: Talk Radio News Service. <em>Conservative Activists Hold a Second 'House Call.'</em> 7 Nov. 2009. <em>Flickr</em>. Web. 21 Aug. 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>In-Text Citations Examples for Web Sources and No Author Given</h2>
               
               <p>For electronic resources that have no page numbers omit the page number. For APA,
                  substitute a paragraph number.
               </p>
               
               <p>APA: "If a government's first priority is to protect the lives of its people, then
                  ringfencing health spending while cutting other budgets and trying to drive down the
                  cost of medicines—policies being pursued in Europe—seem sensible options" (Kelland,
                  2010, para. 2).
               </p>
               
               <p>&nbsp;</p>
               
               <p>MLA: "If a government's first priority is to protect the lives of its people, then
                  ringfencing health spending while cutting other budgets and trying to drive down the
                  cost of medicines—policies being pursued in Europe—seem sensible options" (Kelland).
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img hspace="30" vspace="10" class="resizable" height="337" longdesc="This photograph from Speaker of the House Nancy Pelosi shows Molly Secours, an American citizen, speaking about the Affordable Health Care Act" alt="mollysecours.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/mollysecours.jpg" width="500" border="0">
                  
               </p>
               
               <p>(Photo credit: Speaker Pelosi. Molly Secours. Used under a Creative Commons Attribution
                  2.0 Generic license.)&nbsp;
               </p>
               
               <p>Fig. 3: Pelosi, Nancy. <em>Molly Secours</em>. 22 July 2009. <em>Flickr</em>. Web. 21 Aug. 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>If a particular work has no author use a shortened version of the title instead of
                  the author's last name.
               </p>
               
               <p>APA: "The Affordable Care Act expands initiatives to increase racial and ethnic diversity
                  in the health care professions." ("Health Disparities," 2010).
               </p>
               
               <p>MLA: "The Affordable Care Act expands initiatives to increase racial and ethnic diversity
                  in the health care professions." ("Health Disparities").
               </p>
               
               <p>&nbsp;</p>
               
               <p>Full MLA citations for the in-text citation examples given:</p>
               
               <p>"Health Disparities and the Affordable Care Act." <em>HealthCare.gov</em>. U.S. Department of Health and Human
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Services. 2010. Web. 13 July 2010.</p>
               
               <p>Kelland, Kate. "Analysis: Health and Austerity: When Budget Cuts Cost Lives." <em>Reuters</em>. 4 July 2010. Web.
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 13 July 2010.</p>
               
               <p>Samuelson, Robert. "A Savings Mirage on Health Care." <em>Washington Post</em> 14 Dec. 2009: A21. <em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2 align="left">In-Text Citation Quiz</h2>
               
               <p>Connect to the following story from the Orlando Sentinel and decide how you would
                  do an MLA in-text citation for it:
               </p>
               
               <p>Orlando Sentinel story</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/quizme_custom.png" title="Question 3" border="0"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2> Citing Charts, Tables, Images, Videos, etc.</h2>
               
               <p>The MLA and APA publication manuals are designed for published written work and do
                  not give explicit guidelines for how to cite audiovisual materials used as a visual
                  aid while speaking, so here are some suggested guidelines:
               </p>
               
               <p>If your chart, table, audio file, or video file</p>
               
               <ul>
                  
                  <li>provides data that you are using to support your point.</li>
                  
                  <li>was not created by you from your own data.</li>
                  
               </ul>
               
               <p>cite it within your speech and provide a full citation on your Works Cited or References
                  page.
               </p>
               
               <p>&nbsp;</p>
               
               <p>To cite within your speech, insert a line underneath the chart, table, audio or video
                  file including at least
               </p>
               
               <ul>
                  
                  <li>Source:</li>
                  
                  <li>the author or creator (This might be an organization such as the U.S. Census Bureau
                     in the example given on the next page.)
                  </li>
                  
               </ul>
               
               <p>Including at least this much information will lead your audience to the correct entry
                  on your works cited page.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Also consider including additional information if it will help your audience understand
                  the context, such as:
               </p>
               
               <ul>
                  
                  <li>the title (For example, if you were including a video you found on YouTube, it might
                     be helpful for your audience to know the name of it.)
                  </li>
                  
                  <li>the date (If this information is not given elsewhere, consider including it, especially
                     for data tables and graphs, cases in which the date is important.)
                  </li>
                  
                  <li>the organization or web site (This would be helpful in such cases as the YouTube video;
                     your audience might like to know that you found the video on YouTube.)
                  </li>
                  
               </ul>
               
               
               <p align="center"></p>
               
               <h2>Example</h2>
               
               <p>Here is an example of a graph created from data on a table in the U.S. Census Bureau's
                  <em>Statistical Abstract.</em></p>
               
               <p align="center">Registered and Actual Voters in the 2008 Presidential Election</p>
               
               <p align="center">
                  <img hspace="30" vspace="10" class="resizable" height="452" longdesc="This graph shows registered and actual voters in the 2008 election by age group. The actual numbers are not important. The graph is used as an illustration of how you would give credit for a graph used as a visual aid in a speech." alt="voters3.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/voters3.jpg" width="752" border="0">
                  
               </p>
               
               <p align="center">&nbsp;Source: U.S. Census Bureau.</p>
               
               <p>If including more than one graph from the same source, e.g. U.S. Census Bureau, give
                  your audience enough information to distinguish the two graphs on your Works Cited
                  or References page. In this example, putting the name of the publication and the table
                  number, as below, should be enough to distinguish this graph from other U.S. Census
                  Bureau graphs:
               </p>
               
               <p align="center">Source: U.S. Census Bureau. <em>2011 Statistical Abstract</em>. Table 416. "Voting-Age Population--Reported Registration and Voting by Selected
                  Characteristics: 1996 to 2008."
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">To create a full citation for this table to put on your Works Cited or References
                  page, cite it as you would any other web page.
               </p>
               
               <p align="left">APA:</p>
               
               <p align="left">U.S. Census Bureau. (2011). <em>Table 416. Voting-age population--Reported registration and voting</em></p>
               
               <p align="left"><em>by selected characteristics: 1996 to 2008</em>. Retrieved from
               </p>
               
               <p align="left">http://www.census.gov/compendia/statab/2011/tables/11s0416.pdf</p>
               
               <p align="left">MLA:</p>
               
               <p>U.S. Census Bureau. "Table 416. Voting-Age Population--Reported Registration and Voting
                  by
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Selected Characteristics: 1996 to 2008." <em>2011 Statistical Abstract</em>. U.S. Census Bureau,
               </p>
               
               <p>2011. Web. 24 Jan. 2011.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Citing Charts, Graphs, Audio and Video Quiz</h2>
               
               <p>Connect to the following article from the Gallup Poll and look at the graph labeled,
                  "American Adults, by Weight Category."
               </p>
               
               <p>&nbsp;</p>
               
               <p>Gallup Poll article</p>
               
               <p></p>
               
               <p>&nbsp;<img alt="Toggle open/close quiz question" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/quizme_custom.png" title="Question 4" border="0"></p>
               
               <p align="center"></p>
               
               <h2>Why URLs are Not Enough</h2>
               
               <p>Including a URL in a citation, whether a full citation on a Works Cited or References
                  page or on your visual aid, can be helpful to your audience in locating a web resource
                  (and it is required for an APA full citation), but by itself it is not enough for
                  three reasons:
               </p>
               
               <ul>
                  
                  <li>URLs are easy to mistype. One character off, and your audience may never find it if
                     you have given no other information.
                  </li>
                  
                  <li>Web sites often reorganize their files. The same page may still be there but in a
                     different location.
                  </li>
                  
                  <li>It does not give credit to the individual who created the information.</li>
                  
               </ul>
               
               <p>If you have included a full citation for web resources used, your audience has a good
                  chance of finding them even if the URLs have changed. Simply typing the author and
                  title into a search engine will probably be enough to find it.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Illustrations&nbsp;</h2>
               
               <p>Sometimes you may use an image purely as an illustration, e.g.</p>
               
               <ul>
                  
                  <li>a photograph of the person you are discussing.</li>
                  
                  <li>clip art of medicine bottles in a presentation on prescription drugs.</li>
                  
               </ul>
               
               <p>Such illustrations do not include information important to the point you are making
                  and need not be included as full citations on your Works Cited or References page.
               </p>
               
               <p>When considering whether to include brief citations on your visual aid, remember the
                  general principle of giving credit to others for their work but also consult your
                  professor for his or her preferences.
               </p>
               
               <p>Keep in mind that if you were giving other types of speeches, e.g.</p>
               
               <ul>
                  
                  <li>a sales presentation.</li>
                  
                  <li>a presentation to your boss and colleagues.</li>
                  
                  <li>a conference presentation.</li>
                  
                  <li>an interview presentation, etc.,</li>
                  
               </ul>
               
               <p>the rules may be different than for a college speech class. In those cases, consider
                  professional standards in your field.
               </p>
               
               <p>If you do include citations on your visual aid, follow the guidelines previously given
                  for graphs, charts, audio and video files.
               </p>
               
               <p>Example:</p>
               
               <p align="center">
                  <img hspace="30" vspace="10" class="resizable" height="300" longdesc="An example of an image used purely for illustration. The photo shows an astronaut on a spacewalk with the Earth in the background." alt="freefloating.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/freefloating.jpg" width="400" border="0">
                  
               </p>
               
               <p align="center">Source: NASA. "Free Floating (1984)."</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>A Word on Copyright</h2>
               
               <p>Note that plagiarism and copyright are two separate issues. Citing a photo, video
                  or other media file does not mean that you have used the material legally.
               </p>
               
               <p>Generally using these types of materials:</p>
               
               <ul>
                  
                  <li>once.</li>
                  
                  <li>in class.</li>
                  
                  <li>for a student presentation.</li>
                  
               </ul>
               
               <p>is allowed under fair use guidelines. See U.S. Copyright Office - Fair Use and Stanford
                  Copyright and Fair Use for more information.
               </p>
               
               <p>However, if you intend to</p>
               
               <ul>
                  
                  <li>make your presentation available over the Internet,</li>
                  
                  <li>to present it to an audience more than once,</li>
                  
                  <li>to give a speech for which you will be paid,</li>
                  
                  <li>or to give a speech to the general public,</li>
                  
               </ul>
               
               <p>then consider the copyright laws for including photographs, videos, etc. Seek permission
                  and provide appropriate legal notices in the body of your presentation. In some cases,
                  a royalty must be paid in order to use others' creations. In other cases, royalty-free
                  work is available for use under certain conditions.
               </p>
               
               <p>The photo credit given below is an appropriate one for a Creative Commons image being
                  reproduced on a web site for educational purposes.
               </p>
               
               <p align="center">
                  <img hspace="30" vspace="10" class="resizable" height="427" longdesc="A photograph of Machu Picchu, Peru, used to demonstrate how to give credit for an image taken from the Internet." alt="machupicchu.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/machupicchu.jpg" width="640" border="0">
                  
               </p>
               
               <p align="center">(Photo credit: Luke Redmond.Machu Picchu (The Lost City of the Incas).Used under a
                  Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Generic license.)
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Works Cited</h2>
               
               <p align="center">Works Cited</p>
               
               <p>&nbsp;</p>
               
               <p>"Health Disparities and the Affordable Care Act." <em>HealthCare.gov</em>. U.S. Department of Health and Human
               </p>
               
               <p style="margin-left: 40.0">&nbsp;Services. 2010. Web. 13 July 2010.</p>
               
               <p><em>Information Literacy: the Perils of Online Research</em>. Dir. Amy S. Weber and Ryan Demetrak.
               </p>
               
               <p style="margin-left: 40.0">Cambridge Educational, 2006. <em>Films on Demand</em>. Web. 13 July 2010.
               </p>
               
               <p>Jones, Jeffrey M. "Greater Optimism about U.S. Health System Coverage, Costs." &nbsp;<em>Gallup</em>. 19 Nov. 2009.&nbsp;
               </p>
               
               <p style="margin-left: 40.0">Web. 13 July 2010.</p>
               
               <p>Kelland, Kate. "Analysis: Health and Austerity: When Budget Cuts Cost Lives." <em>Reuters</em>. 4 July 2010. Web.
               </p>
               
               <p style="margin-left: 40.0">13 July 2010.</p>
               
               <p>
                  <span>
                     <font color="black">Kirszner, Laurie G. and Stephen R. Mandell. <em>TheWadsworthHandbook</em>. 9<sup>th</sup> ed.Boston: Wadsworth, 2011.</font>
                     </span>
                  
               </p>
               
               <p>Obama, Barack. "Remarks by the President to a Joint Session of Congress on Health
                  Care." U.S. Capitol,
               </p>
               
               <p style="margin-left: 40.0">Washington, DC.&nbsp; 9 Sept. 2009. Web. 13 July 2010.</p>
               
               <p>Samuelson, Robert. "A Savings Mirage on Health Care." <em>Washington Post</em> 14 Dec. 2009: A21. <em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>
                  <span>
                     <font color="black">Stolley, Karl and Allen Brizee. "Is it Plagiarism Yet?" <em>Purdue Online Writing Lab</em>. Purdue U., 21 April 2010.</font>
                     </span>
                  
               </p>
               
               <p style="margin-left: 40.0">
                  <span>
                     <font color="black">Web.13 Aug. 2010.</font>
                     </span>
                  
               </p>
               
               <p>Thomas, Mike. "Health Care: Can It Stand Up to the Challenges?" <em>Orlando Sentinel</em> 24 Mar. 2010: B1.
               </p>
               
               <p style="margin-left: 40.0">&nbsp;<em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Print Your Score</h2>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations_print.pcf">©</a>
      </div>
   </body>
</html>