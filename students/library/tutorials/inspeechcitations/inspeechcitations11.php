<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>In-Text Citations Examples for Web Sources and No Author Given | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>In-Text Citations Examples for Web Sources and No Author Given</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="in-textcitationsexamplesforwebsourcesandnoauthorgiven" id="in-textcitationsexamplesforwebsourcesandnoauthorgiven"></a><h2>In-Text Citations Examples for Web Sources and No Author Given</h2>
               
               <p>For electronic resources that have no page numbers omit the page number. For APA,
                  substitute a paragraph number.
               </p>
               
               <p>APA: "If a government's first priority is to protect the lives of its people, then
                  ringfencing health spending while cutting other budgets and trying to drive down the
                  cost of medicines—policies being pursued in Europe—seem sensible options" (Kelland,
                  2010, para. 2).
               </p>
               
               <p>&nbsp;</p>
               
               <p>MLA: "If a government's first priority is to protect the lives of its people, then
                  ringfencing health spending while cutting other budgets and trying to drive down the
                  cost of medicines—policies being pursued in Europe—seem sensible options" (Kelland).
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img hspace="30" vspace="10" class="resizable" height="337" longdesc="ada_files/ada_image3.html" alt="mollysecours.jpg" src="http://valenciacollege.edu/library/tutorials/inspeechcitations/mollysecours.jpg" width="500" border="0">
                  
               </p>
               
               <p>(Photo credit: <a href="http://www.flickr.com/photos/speakerpelosi/" target="_blank">Speaker Pelosi</a>. <a href="http://www.flickr.com/photos/speakerpelosi/3748460236/" target="_blank">Molly Secours</a>. Used under a <a href="http://creativecommons.org/licenses/by/2.0/deed.en" target="_blank">Creative Commons Attribution 2.0 Generic license</a>.)&nbsp;
               </p>
               
               <p>Fig. 3: Pelosi, Nancy. <em>Molly Secours</em>. 22 July 2009. <em>Flickr</em>. Web. 21 Aug. 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>If a particular work has no author use a shortened version of the title instead of
                  the author's last name.
               </p>
               
               <p>APA: "The Affordable Care Act expands initiatives to increase racial and ethnic diversity
                  in the health care professions." ("Health Disparities," 2010).
               </p>
               
               <p>MLA: "The Affordable Care Act expands initiatives to increase racial and ethnic diversity
                  in the health care professions." ("Health Disparities").
               </p>
               
               <p>&nbsp;</p>
               
               <p>Full MLA citations for the in-text citation examples given:</p>
               
               <p>"Health Disparities and the Affordable Care Act." <em>HealthCare.gov</em>. U.S. Department of Health and Human
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Services. 2010. Web. 13 July 2010.</p>
               
               <p>Kelland, Kate. "Analysis: Health and Austerity: When Budget Cuts Cost Lives." <em>Reuters</em>. 4 July 2010. Web.
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 13 July 2010.</p>
               
               <p>Samuelson, Robert. "A Savings Mirage on Health Care." <em>Washington Post</em> 14 Dec. 2009: A21. <em>Newsbank</em>. Web. 13 July 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations11.html#">return to top</a> | <a href="inspeechcitations10.html">previous page</a> | <a href="inspeechcitations12.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations11.pcf">©</a>
      </div>
   </body>
</html>