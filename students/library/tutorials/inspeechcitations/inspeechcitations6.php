<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Oral Footnotes | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/inspeechcitations/inspeechcitations6.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/inspeechcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/inspeechcitations/">Inspeechcitations</a></li>
               <li>Oral Footnotes</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="oralfootnotes" id="oralfootnotes"></a><h2>
                  <a id="bookmark" name="oral"></a> Oral Footnotes
               </h2>
               
               <p>Speakers use oral footnotes in a speech to establish the credibility of the information
                  presented. The audience should be assured that the information comes from a reliable
                  source which can be looked up later by an interested listener.
               </p>
               
               <p>Examples:</p>
               
               <ul>
                  
                  <li>In a September 2009 speech to Congress, President Obama stated, "It has now been nearly
                     a century since Theodore Roosevelt first called for health care reform. And ever since,
                     nearly every President and Congress, whether Democrat or Republican, has attempted
                     to meet this challenge in some way."
                  </li>
                  
                  <li>According to a November 2009 <em>Gallup Poll</em>, 38% of Americans rate healthcare coverage as excellent or good.
                  </li>
                  
               </ul>
               
               <p>In the examples above, "In a September 2009 speech to Congress, President Obama stated"
                  and "According to a November 2009 <em>Gallup Poll</em>" are the oral footnotes. They give the listener a brief idea of the source of the
                  information and introduce the quote and statistic given above. When composing an oral
                  footnote include at least:
               </p>
               
               <ul>
                  
                  <li>the person or organization who produced the information.</li>
                  
               </ul>
               
               <p>You may also want to include (and your professor may require):</p>
               
               <ul>
                  
                  <li>the date of the information, e.g. 2009 in the examples above.</li>
                  
                  <li>the credentials of the person being quoted, e.g. President of the United States.</li>
                  
                  <li>the name of the publication or program in which the information appeared, e.g. The
                     <em>Wall Street Journal</em> or The <em>CBS Evening News</em>.
                  </li>
                  
               </ul>
               
               <p>Make sure you are clear on your professor's requirements before you give your speech.</p>
               
               <p>Oral footnotes refer the listener to the full citation on the References or Works
                  Cited page. So for the two examples given above, the speaker would include a full
                  citation to President Obama's speech and to the <em>Gallup Poll</em> on the Works Cited page. MLA Works Cited Page for a speech using those two examples:
               </p>
               
               <p align="center">Works Cited</p>
               
               <p>Jones, Jeffrey M. "Greater Optimism about U.S. Health System Coverage, Costs." &nbsp;<em>Gallup</em>. 19 Nov. 2009.&nbsp;
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Web. 13 July 2010.</p>
               
               <p>Obama, Barack. "Remarks by the President to a Joint Session of Congress on Health
                  Care." U.S. Capitol,
               </p>
               
               <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Washington, DC.&nbsp; 9 Sept. 2009. Web. 13 July 2010.</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="inspeechcitations6.html#">return to top</a> | <a href="inspeechcitations5.html">previous page</a> | <a href="inspeechcitations7.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/inspeechcitations/inspeechcitations6.pcf">©</a>
      </div>
   </body>
</html>