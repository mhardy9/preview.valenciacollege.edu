<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Some Tips for Avoiding Plagiarism | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/aptips/aptips2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/aptips/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/aptips/">Aptips</a></li>
               <li>Some Tips for Avoiding Plagiarism</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="sometipsforavoidingplagiarism" id="sometipsforavoidingplagiarism"></a><h2>Some Tips for Avoiding Plagiarism</h2>
               
               <p>Collect citation information as you collect your sources.</p>
               
               <p>When taking notes, do not mix sources. Use one source per card or page.&nbsp;</p>
               
               <p>Take good notes that differentiate between quotes and your own thoughts.</p>
               
               <ul>
                  
                  <li>Use color coding, e.g. your own words in black and <font color="#006600">quotes</font> in green.
                  </li>
                  
                  <li>"Indicate in your notes which ideas are taken from sources with a big S, and which
                     are your own insights (ME)." (Stolley and Brizee).
                  </li>
                  
                  <li>Lipson suggests "Q-quotes" (34). Use a capital Q and a page number to mark exact words
                     taken from a source, e.g. Q34 Some honest writers find themselves in hot water, accused
                     of plagiarism, because their notes are so bad they cannot tell what they copied and
                     what they wrote themselves. (This quotation is also from Lipson.)
                  </li>
                  
               </ul>
               
               <p>Manage your time.<img hspace="30" vspace="10" align="right" class="resizable" height="150" longdesc="ada_files/ada_image1.html" alt="clock.jpg" src="http://valenciacollege.edu/library/tutorials/aptips/clock.jpg" width="200" border="0"></p>
               
               <ul>
                  
                  <li>Give yourself a realistic timeline for completing a project. Some students try to
                     research and compile an entire speech hours before it is due; these students tend
                     to end up very stressed.
                  </li>
                  
                  <li>Break the project down into smaller tasks and complete them over a longer period of
                     time. For example, you might first find two articles on the topic of your speech.
                     The next free block of time you have, you might read one of the two articles you found
                     and take notes on it. Later read the second article, and so on. Once you have decided
                     to use a source in your speech, go ahead and complete the citation; it's usually easier
                     to do one citation at a time than several at once. As you are collecting your sources,
                     make sure that you collect the information needed to write a citation. It can very
                     difficult to find a book, article or web site again at the last minute if you have
                     not taken down that information in the first place. In addition to breaking the research
                     into pieces, break the speech itself into pieces. If you outline your speech, you
                     can work on just a portion of the outline at one time.
                  </li>
                  
               </ul>
               
               <p>Ask for help.</p>
               
               <ul>
                  
                  <li>Librarians are available to answer your citation questions. Stop by the library and
                     talk to us. <a href="http://www.valenciacollege.edu/library/hours.cfm" target="_blank">Call</a>. Or use the <a href="http://www.askalibrarian.org/local.php?LibraryName=Valencia%20Community%20College&amp;DepartmentNumber=22902&amp;Secondary=22498&amp;LibraryType=Academic" target="_blank">Ask a Librarian service</a>.
                  </li>
                  
                  <li>Visit the Writing Center on any campus: <a href="http://www.valenciacollege.edu/east/academicsuccess/writing/default.cfm" target="_blank">East Communications Center</a>, <a href="http://www.valenciacollege.edu/osceola/learningcenter/writing.cfm" target="_blank">Osceola Writing Center</a>, <a href="http://www.valenciacollege.edu/west/lss/communications/writing.cfm" target="_blank">West Writing Center</a>, <a href="http://www.valenciacollege.edu/wp/cssc/services02.asp" target="_blank">Winter Park Communications Student Support Center</a>.
                  </li>
                  
                  <li>Talk to your instructor!</li>
                  
               </ul>
               
               <p>When in doubt, cite.</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span></p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="aptips2.html#">return to top</a> | <a href="aptips.html">previous page</a> | <a href="aptips3.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/aptips/aptips2.pcf">©</a>
      </div>
   </body>
</html>