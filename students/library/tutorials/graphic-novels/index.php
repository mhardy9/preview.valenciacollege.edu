<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Graphic Novels 2017 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/graphic-novels/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/graphic-novels/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li>Graphic Novels</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="interstitial white"></div>
               <span id="loadingSpinner" class="loading"></span>
               
               
               <div class="framewrap">
                  
                  <div class="landscape_menu no_logo">
                     
                     <div class="logo_background">
                        
                        <div class="logo_section"></div>
                        
                        <div class="presenter_video_section"></div>
                        
                        <div class="presenter_section"></div>
                        
                     </div>
                     
                     <ul id="menu_tabs" class="menu_nav">
                        
                     </ul>
                     
                     <div id="menu_container" class="menu_container scrollarea">
                        
                        <ul id="slide_list" class="menu_list">
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
                  
                  <div class="contentpane">
                     
                     <div id="slidebg"></div>
                     
                     <div id="topbar">
                        
                        <div id="storytitle">
                           
                           <div id="storytitle_section"></div>
                           
                           <div id="timer_section"></div>
                           
                        </div>
                        
                        <div id="toplinks_left"></div>
                        
                        <div id="toplinks_right"></div>
                        
                     </div>
                     
                     <div id="slideframe">
                        
                        <div id="slidecontainer">
                           
                        </div>
                        
                     </div>
                     
                     <div id="controls">
                        
                        <div id="control-finish" class="controlbar-button right" style="display:none;">
                           <a class="icon finish-slide"></a><div class="label finish">FINISH</div>
                           
                        </div>
                        
                        <div id="control-submit" class="controlbar-button right" style="display:none;">
                           <a class="icon submit-slide"></a><div class="label submit">SUBMIT</div>
                           
                        </div>
                        
                        <div id="control-next" class="controlbar-button right">
                           
                           <div class="label next">NEXT</div>
                           <a class="icon next-slide"></a>
                           
                        </div>
                        
                        <div id="control-previous" class="controlbar-button right">
                           <a class="icon previous-slide"></a><div class="label prev">PREV</div>
                           
                        </div>
                        
                        <div id="control-submitall" class="controlbar-button" style="display:none;">
                           <div class="label submitall">Submit All</div>
                        </div>
                        
                        <div id="control-volume" class="controlbar-button compact"><a class="icon volume half"></a></div>
                        
                        
                        <div id="control-progress" class="controlbar-button">
                           
                           <div id="control-pauseplay" class="ignore">
                              
                              <div class="playbtn progress-bar-button" style="display:none;">
                                 
                                 <div class="icon play"></div>
                                 
                              </div>
                              
                              <div class="pausebtn progress-bar-button" style="display:none;">
                                 
                                 <div class="icon pause"></div>
                                 
                              </div>
                              
                           </div>
                           
                           <div id="progressbar" class="progress-container">
                              
                              <div class="progress-bar"></div>
                              
                           </div>
                           
                           <div class="progress-bar-button restart">
                              
                              <div class="icon restart"></div>
                              
                           </div>
                           
                        </div>
                        <span id="indicator"></span>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/graphic-novels/index.pcf">©</a>
      </div>
   </body>
</html>