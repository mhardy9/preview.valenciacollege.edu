<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/archive.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Tutorial Archive</h2>
                        This page lists tutorials previously contained on our main tutorials page. These tutorials
                        are no longer being updated.
                        
                        <h3>
                           <a name="beginning" id="beginning"></a>BEGINNING YOUR RESEARCH
                        </h3>
                        
                        <ul>
                           
                           <li><a href="http://www.quia.com/rr/40308.html" target="_blank">Quiz: Determining Your Information Need</a></li>
                           
                           <li><a href="http://www.quia.com/jg/367490.html" target="_blank">Matching: Valencia Databases/Information Needed</a></li>
                           
                        </ul>
                        
                        <h3>
                           <a name="searching" id="searching"></a>SEARCHING FOR INFORMATION
                        </h3>
                        
                        <ul>
                           
                           <li>Searching Databases
                              
                              <ul>
                                 
                                 <li><a href="http://www.quia.com/cm/22460.html" target="_blank">Keyword and Subject Heading Searches (Matching)</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Searching for Websites
                              
                              <ul>
                                 
                                 <li><a href="http://www.quia.com/rr/40540.html" target="_blank">Quiz: Internet Search Tools</a></li>
                                 
                                 <li><a href="http://www.quia.com/cz/13140.html" target="_blank">Quiz: Google Syntaxes</a></li>
                                 
                                 <li><a href="http://www.quia.com/cb/33819.html" target="_blank">Search Engine Jeopardy</a></li>
                                 
                                 <li><a href="http://www.quia.com/hm/105543.html" target="_blank">Meta-Search Engine Identification</a></li>
                                 
                                 <li><a href="http://www.quia.com/jg/1388860.html">Web Site Domains (Matching)</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li><a href="http://www.quia.com/cz/12775.html" target="_blank">Quiz: General Search Features</a></li>
                           
                           <li><a href="http://www.quia.com/rr/40085.html" target="_blank">Quiz: The Best Search Statement</a></li>
                           
                           <li><a href="http://www.quia.com/jg/350083.html" target="_blank">Search Strategies (Matching)</a></li>
                           
                           <li><a href="http://www.quia.com/hm/105552.html" target="_blank">Logical Fallacies Hangman</a></li>
                           
                           <li><a href="http://www.quia.com/jg/361443.html" target="_blank">Logical Fallacies Matching</a></li>
                           
                        </ul>
                        
                        <h3>
                           <a name="comparing" id="comparing"></a>COMPARING AND EVALUATING RESOURCES
                        </h3>
                        
                        <ul>
                           
                           <li><a href="http://www.quia.com/cm/22545.html" target="_blank">Characteristics of Web Sites (Matching)</a></li>
                           
                           <li><a href="http://www.quia.com/cm/62038.html" target="_blank">Identifying Primary and Secondary Sources (Matching)</a></li>
                           
                           <li><a href="http://www.quia.com/jg/361674.html" target="_blank">Media File Types (Matching)</a></li>
                           
                           <li><a href="http://www.quia.com/cm/22311.html" target="_blank">Netiquette (Matching)</a></li>
                           
                           <li><a href="http://www.quia.com/cm/22295.html" target="_blank">Popular and Scholarly Web Sites (Matching)</a></li>
                           
                        </ul>
                        
                        <h3>
                           <a name="citing" id="citing"></a>CITING YOUR SOURCES
                        </h3>
                        
                        <ul>
                           
                           <li>APA Style
                              
                              <ul>
                                 
                                 <li><a href="http://www.quia.com/rd/34857.html" target="_blank">APA Documentation Citation Order: Periodical Article (Put it in Order)</a></li>
                                 
                                 <li><a href="http://www.quia.com/cm/60762.html" target="_blank">APA Documentation: In-Text Citations (Matching)</a></li>
                                 
                                 <li><a href="http://www.quia.com/tq/293172.html" target="_blank">Quiz: APA Documentation of Library-Based Electronic Resources</a></li>
                                 
                                 <li><a href="http://www.quia.com/tq/293025.html" target="_blank">Quiz: APA Documentation of Web Sites</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>MLA Style
                              
                              <ul>
                                 
                                 <li><a href="http://www.quia.com/rd/35267.html" target="_blank">MLA Documentation Citation Order: Periodical Article (Put it in Order)</a></li>
                                 
                                 <li><a href="http://www.quia.com/cm/61292.html" target="_blank">MLA Documentation: In-Text Citations (Matching)</a></li>
                                 
                                 <li><a href="http://www.quia.com/jg/500676.html" target="_blank">MLA Parts of a Citation (Matching)</a></li>
                                 
                                 <li><a href="http://www.quia.com/tq/293179.html" target="_blank">Quiz: MLA Documentation of Library-Based Electronic Resources</a></li>
                                 
                                 <li><a href="http://www.quia.com/tq/293174.html" target="_blank">Quiz: MLA Documentation of Web Sites</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>General
                              
                              <ul>
                                 
                                 <li><a href="http://www.quia.com/cm/22399.html" target="_blank">To Cite or Not? (Matching)</a></li>
                                 
                                 <li><a href="http://www.quia.com/tq/292821.html" target="_blank">Quiz: Plagiarism or Not?</a></li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/archive.pcf">©</a>
      </div>
   </body>
</html>