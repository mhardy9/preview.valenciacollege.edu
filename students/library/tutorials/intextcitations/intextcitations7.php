<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>In-Text Citation Example for Web Sources | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>In-Text Citation Example for Web Sources</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="in-textcitationexampleforwebsources" id="in-textcitationexampleforwebsources"></a><h2>In-Text Citation Example for Web Sources</h2>
               
               <p>For electronic resources that have no page numbers, substitute a paragraph number
                  IF the paragraphs are numbered. In the event the paragraphs are NOT numbered (most
                  are not) look for a section name within the document. From within the section, locate
                  and list the paragraph number from which the information came from.
               </p>
               
               <p><a href="http://www.reuters.com/article/2010/07/04/us-austerity-health-analysis-idUSTRE6630AF20100704" target="_blank">Review this news article</a> to see how this type of citation would work:
               </p>
               
               <p style="margin-left: 40.0px">According to Kelland (2010), "Scientists have found a strong correlation between higher
                  economic growth and lower mortality rates" (Shock Therapy section, para. 1).
               </p>
               
               <p>In the event there are no page numbers, no paragraph numbers and no sections listed,
                  simply enclose the author's last name and the publication year in parentheses. Your
                  readers will have to use the Find Text feature in their browser to locate the passage
                  you quoted.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img longdesc="ada_files/ada_image3.html" src="http://valenciacollege.edu/library/tutorials/intextcitations/mollysecours.jpg" border="0" hspace="30" width="500" vspace="10" height="337" class="resizable" alt="Image of Molly Secours" metadata="obj7">
                  
               </p>
               
               <p><em>Figure 3.</em> Photo of <a href="http://www.flickr.com/photos/speakerpelosi/3748460236/" target="_blank">Speaker Pelosi and Molly Secours</a>.
               </p>
               
               <p>Reprinted from <em>Speaker Pelosi and Molly Secours</em> [Photo]. (2009,July 22). Retrieved from: http://www.flickr.com/photos/speakerpelosi/3748460236/
               </p>
               
               <p>Used under a <a href="http://creativecommons.org/licenses/by/2.0/deed.en" target="_blank">Creative Commons Attribution 2.0 Generic license</a>.
               </p>
               <a name="in-textcitationexamplefornoauthorgiven" id="in-textcitationexamplefornoauthorgiven"></a><h2>In-Text Citation Example for No Author Given</h2>
               
               <p>If a particular work has no author, use a shortened version of the title instead of
                  the author's last name.
               </p>
               
               <p>Example: "The Affordable Care Act expands initiatives to increase racial and ethnic
                  diversity in the health care professions." ("Health Disparities," 2010).&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="intextcitations7.html#">return to top</a> | <a href="intextcitations6.html">previous page</a> | <a href="intextcitations8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations7.pcf">©</a>
      </div>
   </body>
</html>