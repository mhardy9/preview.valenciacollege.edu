<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Example | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations10.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>Example</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="example" id="example"></a><h2>Example</h2>
               
               <p>Here is a graph created from data taken from a study conducted by the University of
                  Michigan. The graph is located on the government web site National Institute on Drug
                  Abuse. Below the graph is a caption giving an explanation of the figure and the figure
                  title (the title is the first sentence of the caption). Captions should be brief,
                  descriptive and include citation information.
               </p>
               
               <p align="center">
                  <img longdesc="ada_files/ada_image4.html" src="http://valenciacollege.edu/library/tutorials/intextcitations/marijuana.jpg" border="0" hspace="30" width="589" vspace="10" height="405" class="resizable" alt="Marijuana or Cigarette Use" metadata="obj2">
                  
               </p>
               
               <p align="left"><em>Figure 4.</em> Graph of the percent of 12th grade students reporting marijuana or cigarette use
                  in the past month from the years 1975 to 2011 from a study conducted by the University
                  of Michigan. Adapted from "Topics in Brief: Marijuana," by National Institute on Drug
                  Abuse, National Institutes of Health. (2011). <em>Percent of 12th Grade Students Reporting Marijuana or Cigarette Use in the Past Month,
                     1975 to 2011</em> [Graph]. Retrieved from http://www.drugabuse.gov/publications/topics-in-brief/marijuana.
                  Copyright 2011by the University of Michigan.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Notice that the full citation for the source comes after the "Adapted from" information.
                  In this case, the full citation for this source would be:
               </p>
               
               <p align="left">National Institute on Drug Abuse, National Institutes of Health. (2011). <em>Percent of 12th Grade Students Reporting Marijuana</em></p>
               
               <p style="margin-left: 40.0px" align="left"><em>or Cigarette Use in the Past Month, 1975 to 2011</em> [Graph]. Retrieved from http://www.drugabuse.gov/publications/topics-in-brief/marijuana
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Any figures used in the body of the paper should be included in the References list
                  as well.
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="intextcitations10.html#">return to top</a> | <a href="intextcitations9.html">previous page</a> | <a href="intextcitations11.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations10.pcf">©</a>
      </div>
   </body>
</html>