<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>In-Text Citations: | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>In-Text Citations:</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>In-Text Citations:</strong><br>To Cite or Not to Cite? Part 4 of 5
               </p>
               
               
               <h2>In-Text Citations</h2>
               
               <p>Learning Outcome: By the end of the lesson, the student will be able to</p>
               
               <ul>
                  
                  <li>Understand when to include in-text citations</li>
                  
                  <li>Identify the parts on an in-text citation</li>
                  
                  <li>Develop in-text citations</li>
                  
               </ul>
               
               <p><strong>Approximate Completion Time</strong>: 25 minutes.
               </p>
               
               <p>
                  <font color="#ff0000">
                     <strong>**To receive credit for completing this tutorial you must complete the online assessment
                        provided by your instructor.**</strong>
                     </font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">&nbsp;</p>
               
               <p>This tutorial works best in Mozilla Firefox.</p>
               
               <p>It cannot save your place, so please take note of the approximate completion time
                  above and either work on it when you have time to finish or keep track of where you
                  were.
               </p>
               
               <p>&nbsp;</p>
               
               <hr>
               
               <p>
                  <strong>If you encounter technical problems, please copy the error message and send it with
                     a description of the problem to lking@valenciacollege.edu</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>In-Text Citations</h2>
               
               <p>Including a full list of citations on a Reference page is important when writing a
                  paper, but it is not enough. In this section you will learn how to include in-text
                  citations within your paper.&nbsp;
               </p>
               
               <h2>When to Cite</h2>
               
               <p>There are only two types of information that do not require a citation:</p>
               
               <ul>
                  
                  <li>Your own original thoughts and creations (ideas, pictures, music, surveys, videos).</li>
                  
                  <li>Common knowledge.</li>
                  
               </ul>
               
               <p>Everything else requires a citation. When in doubt, cite.&nbsp;</p>
               
               <p>Watch a brief video clip on the differences between paraphrasing and plagiarizing.&nbsp;</p>
               
               <p>
                  <font color="#C00000">Note: This video may take a moment to load.</font>
                  
               </p>
               
               <p></p>
               
               <p align="center"></p>
               
               <h2>Common Knowledge</h2>
               
               <p>Common knowledge includes facts that are well-known and not widely disputed. They
                  are often historical.
               </p>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td width="133px">
                        
                        <h3 align="center">Examples of well-known facts</h3>
                        
                     </td>
                     
                     <td width="384px">
                        
                        <h3 align="center">Periodic Table of the Elements</h3>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="133px">
                        
                        <ul>
                           
                           <li>The Declaration of Independence was signed in 1776.</li>
                           
                           <li>Barack Obama is the 44<sup>th</sup> President of the United States.
                           </li>
                           
                           <li>Disney World is located south of Orlando, Florida.</li>
                           
                           <li>Thanksgiving is celebrated on the 4<sup>th</sup> Thursday in November in the United States.
                           </li>
                           
                           <li>The chemical symbol for carbon is C.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="384px">
                        
                        <p align="center"><img longdesc="This is an image of the Periodic Table of the Elements, used in chemistry. This is an example of common knowledge." src="http://valenciacollege.edu/library/tutorials/intextcitations/periodictable.jpg" border="0" hspace="30" width="500" vspace="10" height="346" class="resizable" alt="Periodic Table of the Elements"></p>
                        
                        <p><em>Figure 1.</em> Image of the periodic table. Reprinted from Torrone, P. (2005, August 19). <em>Periodictable</em> [Image]. Retrieved from http://www.flickr.com/photos/pmtorrone/150008091/ Used under
                           a Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Generic license.
                        </p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>Experts do not always agree on what else is included in common knowledge.</p>
               
               <p>Stolley and Brizee (2010) of the Purdue Online Writing Lab, a widely used and highly
                  regarded source on citation, suggest, "Generally speaking, you can regard something
                  as common knowledge if you find the same information undocumented in at least five
                  credible sources ("Deciding If Something Is," para. 1)."
               </p>
               
               <p>Many sources such as Kirszner and Mandell's (2011) Wadsworth Handbook include "familiar
                  sayings and well-known quotations" and "information most readers probably know" (p.
                  223).
               </p>
               
               <p>Keep in mind though that information your readers already know may be context-dependent,
                  i.e. it may vary based on which class you are in, who the other students are, which
                  discipline (English, Math, Biology, etc.) is involved, etc.
               </p>
               
               <p>When in doubt, cite. It is better to have some unnecessary citations than to neglect
                  needed ones.&nbsp;&nbsp;
               </p>
               
               <p align="center"></p>
               
               <h2>When to Cite Activity</h2>
               
               <p><strong>Complete the following activity. Decide which of the following examples should be
                     cited.</strong>&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/intextcitations/testyourself_custom.png" border="0" title="Question 1" alt="Toggle open/close quiz question"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>In-Text Citations</h2>
               
               <p>In-text citations give the reader a brief idea of the source of information used.
                  In-text citations are sometimes called parenthetical citations because they appear
                  in parenthesis in the body of the text in both the APA and MLA styles. Although more
                  commonly used in papers, in-text citations are sometimes incorporated into
               </p>
               
               <ul>
                  
                  <li>outlines,</li>
                  
                  <li>speaker notes,</li>
                  
                  <li>PowerPoint presentations.</li>
                  
               </ul>
               
               <p>Check with your instructor to determine his or her preferences on including in-text
                  citations in any of the above.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>In-Text Citation Examples</h2>
               
               <p>In the APA format, an in-text citation includes</p>
               
               <ul>
                  
                  <li>the author's last name</li>
                  
                  <li>date (use n.d. if no date is available),</li>
                  
                  <li>page or paragraph number, if available.</li>
                  
               </ul>
               
               <p>Example: "If government insures 30 million or more Americans, health spending will
                  rise" (Samuelson, 2009, p. A21).
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img longdesc="This photograph shows conservative activists protesting the Affordable Health Care for All Americans Act." src="http://valenciacollege.edu/library/tutorials/intextcitations/protesters.jpg" border="0" hspace="30" width="500" vspace="10" height="332" class="resizable" alt="Conservative Activists">
                  
               </p>
               
               <p><em>Figure 2.</em> Photo of Conservative Activists Hold a Second 'House Call.' Reprinted from Talk Radio
                  News Service. (2009, November 7). <em>Conservative Activists Hold a Second 'House Call'</em> [Photo]. Retrieved from http://www.flickr.com/photos/talkradionews/4083500279
               </p>
               
               <p>Used under a Creative Commons Attribution-NonCommercial-ShareAlike 2.0 Generic license.&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>In-Text Citation Example for Web Sources</h2>
               
               <p>For electronic resources that have no page numbers, substitute a paragraph number
                  IF the paragraphs are numbered. In the event the paragraphs are NOT numbered (most
                  are not) look for a section name within the document. From within the section, locate
                  and list the paragraph number from which the information came from.
               </p>
               
               <p>Review this news article to see how this type of citation would work:</p>
               
               <p style="margin-left: 40.0">According to Kelland (2010), "Scientists have found a strong correlation between higher
                  economic growth and lower mortality rates" (Shock Therapy section, para. 1).
               </p>
               
               <p>In the event there are no page numbers, no paragraph numbers and no sections listed,
                  simply enclose the author's last name and the publication year in parentheses. Your
                  readers will have to use the Find Text feature in their browser to locate the passage
                  you quoted.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img longdesc="This photograph from Speaker of the House Nancy Pelosi shows Molly Secours, an American citizen, speaking about the Affordable Health Care Act" src="http://valenciacollege.edu/library/tutorials/intextcitations/mollysecours.jpg" border="0" hspace="30" width="500" vspace="10" height="337" class="resizable" alt="Image of Molly Secours" metadata="obj7">
                  
               </p>
               
               <p><em>Figure 3.</em> Photo of Speaker Pelosi and Molly Secours.
               </p>
               
               <p>Reprinted from <em>Speaker Pelosi and Molly Secours</em> [Photo]. (2009,July 22). Retrieved from: http://www.flickr.com/photos/speakerpelosi/3748460236/
               </p>
               
               <p>Used under a Creative Commons Attribution 2.0 Generic license.</p>
               
               <h2>In-Text Citation Example for No Author Given</h2>
               
               <p>If a particular work has no author, use a shortened version of the title instead of
                  the author's last name.
               </p>
               
               <p>Example: "The Affordable Care Act expands initiatives to increase racial and ethnic
                  diversity in the health care professions." ("Health Disparities," 2010).&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2 align="left">In-Text Citation Activity</h2>
               
               <p>Connect to the following story from the Orlando Sentinel and decide how you would
                  do an in-text citation for it:
               </p>
               
               <p>Orlando Sentinel story</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Now complete the following citation activity.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/intextcitations/selfcheck_custom.png" border="0" title="Question 2" alt="Toggle open/close quiz question">&nbsp;&nbsp;&nbsp;
               </p>
               
               <p align="center"></p>
               
               <h2>Citing Figures</h2>
               
               <p>According to the Sixth Edition of the Publication Manual of the American Psychological
                  Association (2010), "figures enable authors to present a large amount of information
                  efficiently" (p. 125). Figures should be used sparingly, and only when it is best
                  to convey information using an image.
               </p>
               
               <p>Figures include graphs, charts, maps, drawings and photographs.</p>
               
               <p>If the figure you use</p>
               
               <ul>
                  
                  <li>provides data that you are using to support your point</li>
                  
                  <li>was not created by you from your own data</li>
                  
               </ul>
               
               <p>cite it within your paper and provide a full citation on your References page.</p>
               
               <p>When including a figure, give credit to the original author in a caption located at
                  the bottom of the source. Sources should be numbered consecutively throughout the
                  paper, ie. <em>Figure 1</em>.
               </p>
               
               <p align="center"></p>
               
               <h2>Example</h2>
               
               <p>Here is a graph created from data taken from a study conducted by the University of
                  Michigan. The graph is located on the government web site National Institute on Drug
                  Abuse. Below the graph is a caption giving an explanation of the figure and the figure
                  title (the title is the first sentence of the caption). Captions should be brief,
                  descriptive and include citation information.
               </p>
               
               <p align="center">
                  <img longdesc="Percent of 12th Grade Students Reporting Marijuana or Cigarette Use in the Past Month, 1975 to 2011" src="http://valenciacollege.edu/library/tutorials/intextcitations/marijuana.jpg" border="0" hspace="30" width="589" vspace="10" height="405" class="resizable" alt="Marijuana or Cigarette Use" metadata="obj2">
                  
               </p>
               
               <p align="left"><em>Figure 4.</em> Graph of the percent of 12th grade students reporting marijuana or cigarette use
                  in the past month from the years 1975 to 2011 from a study conducted by the University
                  of Michigan. Adapted from "Topics in Brief: Marijuana," by National Institute on Drug
                  Abuse, National Institutes of Health. (2011). <em>Percent of 12th Grade Students Reporting Marijuana or Cigarette Use in the Past Month,
                     1975 to 2011</em> [Graph]. Retrieved from http://www.drugabuse.gov/publications/topics-in-brief/marijuana.
                  Copyright 2011by the University of Michigan.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Notice that the full citation for the source comes after the "Adapted from" information.
                  In this case, the full citation for this source would be:
               </p>
               
               <p align="left">National Institute on Drug Abuse, National Institutes of Health. (2011). <em>Percent of 12th Grade Students Reporting Marijuana</em></p>
               
               <p style="margin-left: 40.0" align="left"><em>or Cigarette Use in the Past Month, 1975 to 2011</em> [Graph]. Retrieved from http://www.drugabuse.gov/publications/topics-in-brief/marijuana
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Any figures used in the body of the paper should be included in the References list
                  as well.
               </p>
               
               <p align="center"></p>
               
               <h2>Citing Graphs Activity</h2>
               
               <p>Connect to the following article from the Gallup Poll and look at the graph labeled,
                  "American Adults, by Weight Category."
               </p>
               
               <p>&nbsp;</p>
               
               <p>Gallup Poll article</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Now complete the following activity.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/intextcitations/selfcheck_custom.png" border="0" title="Question 3" alt="Toggle open/close quiz question"></p>
               
               <p align="center"></p>
               
               <h2>Why URLs are Not Enough</h2>
               
               <p>Including a URL in a citation can be helpful to your audience in locating a web resource
                  (and it is required for an APA full citation), but by itself it is not enough for
                  three reasons:
               </p>
               
               <ul>
                  
                  <li>URLs are easy to mistype. One character off, and your audience may never find it if
                     you have given no other information.
                  </li>
                  
                  <li>Web sites often reorganize their files. The same page may still be there but in a
                     different location.
                  </li>
                  
                  <li>It does not give credit to the individual who created the information.</li>
                  
               </ul>
               
               <p>If you have included a full citation for web resources used, your audience has a good
                  chance of finding them even if the URLs have changed. Simply typing the author and
                  title into a search engine will probably be enough to find it.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Sources Used in this Module - Cited in APA Format</h2>
               
               <p align="center">References</p>
               
               <p>&nbsp;</p>
               
               <p>Gallup. (2010). <em>American adults, by weight category: Weight category as determined by BMI [Graph].</em> Retrieved from http://www.gallup.com/poll/145802/
               </p>
               
               <p style="margin-left: 40.0">Adult-Obesity-Stabilizes-2010.aspx</p>
               
               <p>Kelland, K. (2010). Analysis: Health and austerity: When budget cuts cost lives. Retrieved
                  from http://www.reuters.com/article/2010/07/04/
               </p>
               
               <p style="margin-left: 40.0">us-austerity-health-analysis-idUSTRE6630AF20100704</p>
               
               <p>
                  <font color="black">Kirszner, L. G. &amp; Mandell, S. R. (2011).<em>The wadsworth handbook</em> (9th ed.). Boston: Wadsworth.</font>
                  
               </p>
               
               <p>Samuelson, R. (2010, July 13). <em>A savings mirage on health care.</em> Retrieved from http://www.washingtonpost.com/wp-dyn/content/article/2009/12/13/
               </p>
               
               <p style="margin-left: 40.0">AR2009121302450.html?sid=ST2009121703771</p>
               
               <p><font color="black">Stolley, K. &amp; Brizee, A. (2010, April 21). <em>Is it plagiarism yet?</em> Retrieved from http://owl.english.purdue.edu/owl/resource/589/2/</font>&nbsp;
               </p>
               
               <p>U.S. Department of Health &amp; Human Services. (2012). <em>Health disparities and the affordable care act</em>. Retrieved from http://www.healthcare.gov/news/factsheets/2010/07/
               </p>
               
               <p style="margin-left: 40.0">health-disparities.html</p>
               
               <p>Weber, Amy S. &amp; Demetrak, R. (Directors). (2006). <em>Information literacy: The perils of online research</em>. Retrieved from http://digital.films.com/play/92TR2T
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Assessment</h2>
               
               <p>
                  <strong>
                     <font color="#ff0000">Your instructor will provide the link to the online assessment. You must take the
                        assessment to receive credit for completing this tutorial.</font>
                     </strong>
                  
               </p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations_print.pcf">©</a>
      </div>
   </body>
</html>