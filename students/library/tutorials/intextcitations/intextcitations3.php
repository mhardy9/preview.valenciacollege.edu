<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Common Knowledge | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>Common Knowledge</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="commonknowledge" id="commonknowledge"></a><h2>Common Knowledge</h2>
               
               <p>Common knowledge includes facts that are well-known and not widely disputed. They
                  are often historical.
               </p>
               
               <p>&nbsp;</p>
               
               <table class="table ">
                  
                  <tr>
                     
                     <td width="133px">
                        <a name="examplesofwell-knownfacts" id="examplesofwell-knownfacts"></a><h3 align="center">Examples of well-known facts</h3>
                        
                     </td>
                     
                     <td width="384px">
                        <a name="periodictableoftheelements" id="periodictableoftheelements"></a><h3 align="center">Periodic Table of the Elements</h3>
                        
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td width="133px">
                        
                        <ul>
                           
                           <li>The Declaration of Independence was signed in 1776.</li>
                           
                           <li>Barack Obama is the 44<sup>th</sup> President of the United States.
                           </li>
                           
                           <li>Disney World is located south of Orlando, Florida.</li>
                           
                           <li>Thanksgiving is celebrated on the 4<sup>th</sup> Thursday in November in the United States.
                           </li>
                           
                           <li>The chemical symbol for carbon is C.</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                     </td>
                     
                     <td width="384px">
                        
                        <p align="center"><img longdesc="ada_files/ada_image1.html" src="http://valenciacollege.edu/library/tutorials/intextcitations/periodictable.jpg" border="0" hspace="30" width="500" vspace="10" height="346" class="resizable" alt="Periodic Table of the Elements"></p>
                        
                        <p><em>Figure 1.</em> Image of <a href="http://www.flickr.com/photos/pmtorrone/150008091/" target="_blank">the periodic table</a>. Reprinted from Torrone, P. (2005, August 19). <em>Periodictable</em> [Image]. Retrieved from http://www.flickr.com/photos/pmtorrone/150008091/ Used under
                           a <a href="http://creativecommons.org/licenses/by-nc-nd/2.0/deed.en" target="_blank">Creative Commons Attribution-NonCommercial-NoDerivs 2.0 Generic license.</a></p>
                        
                     </td>
                     
                  </tr>
                  
               </table>
               
               <p>&nbsp;</p>
               
               <p>Experts do not always agree on what else is included in common knowledge.</p>
               
               <p>Stolley and Brizee (2010) of the Purdue Online Writing Lab, a widely used and highly
                  regarded source on citation, suggest, "Generally speaking, you can regard something
                  as common knowledge if you find the same information undocumented in at least five
                  credible sources ("Deciding If Something Is," para. 1)."
               </p>
               
               <p>Many sources such as Kirszner and Mandell's (2011) Wadsworth Handbook include "familiar
                  sayings and well-known quotations" and "information most readers probably know" (p.
                  223).
               </p>
               
               <p>Keep in mind though that information your readers already know may be context-dependent,
                  i.e. it may vary based on which class you are in, who the other students are, which
                  discipline (English, Math, Biology, etc.) is involved, etc.
               </p>
               
               <p>When in doubt, cite. It is better to have some unnecessary citations than to neglect
                  needed ones.&nbsp;&nbsp;
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="intextcitations3.html#">return to top</a> | <a href="intextcitations2.html">previous page</a> | <a href="intextcitations4.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations3.pcf">©</a>
      </div>
   </body>
</html>