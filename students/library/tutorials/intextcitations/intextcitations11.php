<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Citing Graphs Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>Citing Graphs Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="citinggraphsactivity" id="citinggraphsactivity"></a><h2>Citing Graphs Activity</h2>
               
               <p>Connect to the following article from the Gallup Poll and look at the graph labeled,
                  "American Adults, by Weight Category."
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <a href="http://www.gallup.com/poll/145802/Adult-Obesity-Stabilizes-2010.aspx">Gallup Poll article</a>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>Now complete the following activity.</strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/intextcitations/spacer.gif" name="check24" alt="" id="check24"><a href="javascript:toggletable('quizpopper24')" title="Question 3" id="a24"><img src="http://valenciacollege.edu/library/tutorials/intextcitations/selfcheck_custom.png" alt="Toggle open/close quiz question" title="Question 3" border="0"></a> 
               </p>
               
               <div id="quizpopper24" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #DBA917; background: #fbf1d2; line-height: 1.5em; padding: 10px 15px;">
                     
                     <form name="f24" id="f24">What would be the correct way to cite this graph from the Gallup Poll within the caption?
                        <div style="margin: 1em 15px;">
                           <input type="radio" name="q24" value="a" id="q24a">&nbsp;<label for="q24a"><strong>a.</strong>&nbsp;Gallup. (2010). American adults, by weight category: Weight category as determined
                              by BMI [Graph]. Retrieved from http://www.gallup.com/poll/145802/Adult-Obesity-Stabilizes-2010.aspx</label><br><input type="radio" name="q24" value="b" id="q24b">&nbsp;<label for="q24b"><strong>b.</strong>&nbsp;Fig. 1: Graph of American adults by weight category. Adapted from "In U.S., Adult
                              Obesity Stabilizes in 2010, at 26.6%."</label><br><input type="radio" name="q24" value="c" id="q24c">&nbsp;<label for="q24c"><strong>c.</strong>&nbsp;Fig. 1: Graph of American adults by weight category. Adapted from "In U.S., Adult
                              Obesity Stabilizes in 2010, at 26.6%," by Gallup. (2010). American adults, by weight
                              category: Weight category as determined by BMI [Graph]. Retrieved from http://www.gallup.com/poll/145802/Adult-Obesity-Stabilizes-2010.aspx</label><br><input type="radio" name="q24" value="d" id="q24d">&nbsp;<label for="q24d"><strong>d.</strong>&nbsp;Fig. 1: Graph of American adults by weight category. http://www.gallup.com/poll/145802/Adult-Obesity-Stabilizes-2010.aspx</label><br>
                           
                        </div>
                        
                        <div align="center"><input onclick="check_q(24, 4, 1, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done24" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed24" style="font-family: Comic Sans MS; border-top: 1px solid #DBA917; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="intextcitations11.html#">return to top</a> | <a href="intextcitations10.html">previous page</a> | <a href="intextcitations12.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations11.pcf">©</a>
      </div>
   </body>
</html>