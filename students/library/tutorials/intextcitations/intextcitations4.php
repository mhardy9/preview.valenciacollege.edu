<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>When to Cite Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>When to Cite Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="whentociteactivity" id="whentociteactivity"></a><h2>When to Cite Activity</h2>
               
               <p><strong>Complete the following activity. Decide which of the following examples should be
                     cited.</strong>&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/intextcitations/spacer.gif" name="check32" alt="" id="check32"><a href="javascript:toggletable('quizpopper32')" title="Question 1" id="a32"><img src="http://valenciacollege.edu/library/tutorials/intextcitations/testyourself_custom.png" alt="Toggle open/close quiz question" title="Question 1" border="0"></a> 
               </p>
               
               <div id="quizpopper32" class="expand">
                  
                  <div style="padding: 5px 10px;"></div>
                  
                  <div class="qpq" style="border: 1px solid #DBA917; background: #fbf1d2; line-height: 1.5em; padding: 10px 15px;">
                     
                     <form name="f32" id="f32">Decide which of the following should be cited.
                        <div style="margin: 1em 15px;">
                           <input type="checkbox" name="q32" value="a" id="q32a">&nbsp;<label for="q32a"><strong>a.</strong>&nbsp;Including the statement in your paper:
                              <p>66.3% of Valencia students enrolled in Fall 2008 were between the ages of 18 and 24.</p></label><br><input type="checkbox" name="q32" value="b" id="q32b">&nbsp;<label for="q32b"><strong>b.</strong>&nbsp;Including a graph that you found on the web.</label><br><input type="checkbox" name="q32" value="c" id="q32c">&nbsp;<label for="q32c"><strong>c.</strong>&nbsp;Giving your own opinion on whether citation is important.</label><br><input type="checkbox" name="q32" value="d" id="q32d">&nbsp;<label for="q32d"><strong>d.</strong>&nbsp;Including the statement that Havana is the capital of Cuba.</label><br><input type="checkbox" name="q32" value="e" id="q32e">&nbsp;<label for="q32e"><strong>e.</strong>&nbsp;Including information about a study that suggests students with tattoos are more
                              likely to use drugs.</label><br><input type="checkbox" name="q32" value="f" id="q32f">&nbsp;<label for="q32f"><strong>f.</strong>&nbsp;Including this direct quote within your paper:
                              <p>Even within the seemingly normative boundaries of American college culture, there
                                 is perhaps a threshold of body art activity which takes individuals outside the mainstream,
                                 creating and maintaining an identity reinforced by social deviance.
                              </p></label><br><br><span style="font-size: 90%;">[mark all correct answers]</span>
                           
                        </div>
                        
                        <div align="center"><input onclick="check_q(32, 6, 3, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done32" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed32" style="font-family: Comic Sans MS; border-top: 1px solid #DBA917; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="intextcitations4.html#">return to top</a> | <a href="intextcitations3.html">previous page</a> | <a href="intextcitations5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations4.pcf">©</a>
      </div>
   </body>
</html>