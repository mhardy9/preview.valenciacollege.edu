<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sources Used in this Module - Cited in APA Format | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/intextcitations/intextcitations13.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/intextcitations/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/intextcitations/">Intextcitations</a></li>
               <li>Sources Used in this Module - Cited in APA Format</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="sourcesusedinthismodule-citedinapaformat" id="sourcesusedinthismodule-citedinapaformat"></a><h2>Sources Used in this Module - Cited in APA Format</h2>
               
               <p align="center">References</p>
               
               <p>&nbsp;</p>
               
               <p>Gallup. (2010). <em>American adults, by weight category: Weight category as determined by BMI [Graph].</em> Retrieved from http://www.gallup.com/poll/145802/
               </p>
               
               <p style="margin-left: 40.0px">Adult-Obesity-Stabilizes-2010.aspx</p>
               
               <p>Kelland, K. (2010). Analysis: Health and austerity: When budget cuts cost lives. Retrieved
                  from http://www.reuters.com/article/2010/07/04/
               </p>
               
               <p style="margin-left: 40.0px">us-austerity-health-analysis-idUSTRE6630AF20100704</p>
               
               <p>
                  <font color="black">Kirszner, L. G. &amp; Mandell, S. R. (2011).<em>The wadsworth handbook</em> (9th ed.). Boston: Wadsworth.</font>
                  
               </p>
               
               <p>Samuelson, R. (2010, July 13). <em>A savings mirage on health care.</em> Retrieved from http://www.washingtonpost.com/wp-dyn/content/article/2009/12/13/
               </p>
               
               <p style="margin-left: 40.0px">AR2009121302450.html?sid=ST2009121703771</p>
               
               <p><font color="black">Stolley, K. &amp; Brizee, A. (2010, April 21). <em>Is it plagiarism yet?</em> Retrieved from http://owl.english.purdue.edu/owl/resource/589/2/</font>&nbsp;
               </p>
               
               <p>U.S. Department of Health &amp; Human Services. (2012). <em>Health disparities and the affordable care act</em>. Retrieved from http://www.healthcare.gov/news/factsheets/2010/07/
               </p>
               
               <p style="margin-left: 40.0px">health-disparities.html</p>
               
               <p>Weber, Amy S. &amp; Demetrak, R. (Directors). (2006). <em>Information literacy: The perils of online research</em>. Retrieved from http://digital.films.com/play/92TR2T
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="intextcitations13.html#">return to top</a> | <a href="intextcitations12.html">previous page</a> | <a href="intextcitations14.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/intextcitations/intextcitations13.pcf">©</a>
      </div>
   </body>
</html>