<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Format of the Presentation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall15.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Format of the Presentation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="formatofthepresentation" id="formatofthepresentation"></a><h2>Format of the Presentation</h2>
               
               <p>&nbsp;</p>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/barista.JPG" width="325" alt="barista.JPG" class="resizable" height="216" border="0" hspace="10" metadata="obj19"> At last, Val is nearly finished with her paper. But, she's still undecided about
                  a few things:
               </p>
               
               <ul>
                  
                  <li>Val is having trouble deciding when to quote her sources directly and when to paraphrase.
                     It seems like a random choice.
                  </li>
                  
               </ul>
               
               <ul>
                  
                  <li>She wants to include a great picture of a barista with a tattoo on her arm. It really
                     makes her point about how tattoos in the workplace are a form of self-expression!
                     Should she paste the picture into her paper, or just describe it?
                  </li>
                  
               </ul>
               
               <ul>
                  
                  <li>Val has some statistics, but she's not sure if she should just write them out, or
                     include a graph in her paper.
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>Val reviews what she has learned about the factors to consider. She considers the
                  <strong>assignment</strong> first, but there are no directions about paraphrasing, pictures, or graphs. Unsure
                  what to do, she visits Professor Sage during office hours.
               </p>
               
               <p>Professor Sage suggests that Val use her best judgment based on what she has learned
                  so far about integrating sources into her paper.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">(Photo credit: <a target="_blank" href="http://www.flickr.com/photos/wakelucid/">matthanns</a>. <a target="_blank" href="http://www.flickr.com/photos/wakelucid/4786330183/">DSC_3355</a>. Used under a <a target="_blank" href="http://creativecommons.org/licenses/by-nc-sa/2.0/deed.en">Creative Commons Attribution-NonCommercial-ShareAlike 2.0 Generic</a> license.)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall15.html#">return to top</a> | <a href="Module_Four_Fall14.html">previous page</a> | <a href="Module_Four_Fall16.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall15.pcf">©</a>
      </div>
   </body>
</html>