<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall11.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="activity" id="activity"></a><h2>Activity</h2>
               
               <p>Try the two multiple choice questions first. Then complete the ordering activity.</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_four/spacer.gif" name="check22" alt="" id="check22"><a id="a22" title="Question 2" href="javascript:parent.toggletable('quizpopper22')">Show/Hide Question #1</a> 
               </p>
               
               <div id="quizpopper22" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 1</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f22" id="f22">Val wants to make the statement that "Banning tattoos violates employees' rights to
                        Freedom of Speech." Which <strong>more basic piece of information</strong> does Val's audience need, in order to understand this statement?
                        <div style="margin: 1em 15px;">
                           <input type="radio" name="q22" value="a" id="q22a">&nbsp;<label for="q22a"><strong>a.</strong>&nbsp;Banning tattoos is unfair.</label><br><input type="radio" name="q22" value="b" id="q22b">&nbsp;<label for="q22b"><strong>b.</strong>&nbsp;Tattoos are a form of self-expression.</label><br><input type="radio" name="q22" value="c" id="q22c">&nbsp;<label for="q22c"><strong>c.</strong>&nbsp;Tattooing dates back to ancient civilizations.</label><br>
                           
                        </div>
                        
                        <div align="center"><input onclick="parent.check_q(22, 3, 1, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done22" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed22" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_four/spacer.gif" name="check23" alt="" id="check23"><a id="a23" title="Question 3" href="javascript:parent.toggletable('quizpopper23')">Show/Hide Question #2</a> 
               </p>
               
               <div id="quizpopper23" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 1</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f23" id="f23">Val does not want to bore her audience with information they already know. If Val's
                        audience already understands that "tattoos are a form of self expression," then which
                        more basic piece of information do they probably <strong>also</strong> already understand?
                        <div style="margin: 1em 15px;">
                           <input type="radio" name="q23" value="a" id="q23a">&nbsp;<label for="q23a"><strong>a.</strong>&nbsp;Tattoos are a type of body art.</label><br><input type="radio" name="q23" value="b" id="q23b">&nbsp;<label for="q23b"><strong>b.</strong>&nbsp;Tattooing dates back to ancient civilizations.</label><br><input type="radio" name="q23" value="c" id="q23c">&nbsp;<label for="q23c"><strong>c.</strong>&nbsp;Banning tattoos is unfair.</label><br>
                           
                        </div>
                        
                        <div align="center"><input onclick="parent.check_q(23, 3, 1, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done23" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed23" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp; 
                  <a href="Module_Four_Fall11.html#endofactivity7"><img title="go to end of activity" src="http://valenciacollege.edu/library/tutorials/module_four/spacer.gif" border="0" width="0" height="0"></a>
                  
               </p>
               
               <table class="table ">
                  
                  <tr valign="middle">
                     
                     <td width="10%"><a href="ada_files/ada_activity7.html" target="_new"><img src="http://valenciacollege.edu/library/tutorials/module_four/ada-access.gif" hspace="3" width="20" height="20" align="middle" title="alternative accessible activity content" longdesc="ada_files/ada_activity7.html" border="0" alt="learning activity"></a></td>
                     
                     <td>Drag each of Val’s topic ideas to the correct order in the right column, with the
                        MOST BASIC INFORMATION AT THE TOP and the most complex idea at the bottom.
                     </td>
                     
                  </tr>
                  
                  <tr>
                     
                     <td colspan="2" align="center">
                        
                        <div id="activity-7" style="width: 550px; height: 500px; border: 1px solid #000000; background-color: #eeeeee;">
                           
                           <div style="width:100%;height:100%;text-align:center;">
                              <table class="table ">
                                 <tr style="width:100%;height:100%;border-width:0px;">
                                    <td style="width:100%;height:100%;border-width:0px;vertical-align:middle;">
                                       <span style="font-weight:bold;">This content requires Flash Player 10 or higher.</span><br><br><a href="http://www.adobe.com/go/getflashplayer" target="_blank"><img src="http://valenciacollege.edu/library/tutorials/module_four/get_flash_player.gif" alt="Get Adobe Flash Player" style="border:0px;"></a>
                                       
                                    </td>
                                 </tr>
                              </table>
                           </div>
                           
                        </div>
                        
                        
                     </td>
                     
                     <td></td>
                     
                  </tr>
                  
               </table>
               
               <div id="endofactivity7"></div>
               &nbsp;
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall11.html#">return to top</a> | <a href="Module_Four_Fall10.html">previous page</a> | <a href="Module_Four_Fall12.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall11.pcf">©</a>
      </div>
   </body>
</html>