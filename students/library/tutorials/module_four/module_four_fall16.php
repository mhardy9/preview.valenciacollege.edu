<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 16 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall16.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Page 16</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/puzzle1.JPG" width="100" alt="puzzle1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj14"> First, Val considers the <strong>purposes of the presentation</strong>. Since her purpose is to persuade the audience, Val decides to use the picture, because
                  she thinks the image will be more powerful than simply describing it.
               </p>
               
               <p>Next, Val considers the <strong>context</strong> of the paper. It is an academic paper and needs to be professionally written. Val
                  decides that a graph will look neat and professional in her paper.
               </p>
               
               <p>Finally, Val considers the <strong>characteristics of the audience</strong>. The audience has little background knowledge of the topic. Based on this, Val decides
                  to paraphrase instead of directly quoting from her source, so that she can make the
                  language less technical and help her audience understand.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <u>The bottom line: Decisions on the format for presenting information should be based
                        on the requirements of the assignment first. However, beyond that, consider the context
                        of the presentation, the purposes of the presentation, and the characteristics of
                        the audience.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>Please continue to the next slide to <strong>test your knowledge</strong>.&nbsp;
               </p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall16.html#">return to top</a> | <a href="Module_Four_Fall15.html">previous page</a> | <a href="Module_Four_Fall17.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall16.pcf">©</a>
      </div>
   </body>
</html>