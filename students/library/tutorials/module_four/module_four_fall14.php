<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 14 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall14.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Page 14</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p align="left">
                  <strong>Val's Paper So Far...</strong>
                  
               </p>
               
               <p align="left">Pause here and take a look at how Val has planned out her paper at this point.</p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">I. Introduction</font>
                     </strong>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">A. Background information / Hook: Tattoos are a form of self-expression (Source: Book,
                     <em>Written on the body : the Tattoo in European and American History</em>, Princeton University Press -- educational publisher)</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">B. Background information: Description of the issue.</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">C. Thesis: "Employees should be allowed to wear and display their tattoos on the job."</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">II. Main Point #1: Banning tattoos violates the Freedom of Speech</font>
                     </strong>
                  
               </p>
               
               <p style="margin-left: 20.0px">
                  <font color="#990099">A. Discussion of Freedom of Speech (Source: Text of the First Amendment -- government
                     document)</font>
                  
               </p>
               
               <p style="margin-left: 20.0px">
                  <font color="#990099">B. Tattoos as a form of speech (Source: article from <em>Wake Forest Law Review</em> -- scholarly journal)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">III. Main Point #2: Tattoos have become more accepted today.</font>
                     </strong>
                  
               </p>
               
               <p style="margin-left: 20.0px">
                  <font color="#990099">A. More Americans now have tattoos (Source: Book, <em>Written on the body : the tattoo in European and American history</em>, Princeton University Press -- educational publisher)</font>
                  
               </p>
               
               <p style="margin-left: 20.0px">
                  <font color="#990099">B. Americans' attitudes about tattoos are improving (Source: Pew Research Center --
                     expert organization)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">IV. Opposing Argument: Employers should be allowed to regulate tattoos.</font>
                     </strong>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">A. Courts have upheld employers' legal rights to ban tattoos. (Source: article from
                     <em>Advanced Management Journal</em> -- scholarly journal)</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">B. Employers report increased sales when dress codes are enforced (Source: National
                     Public Radio -- reputable news organization)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">V. Conclusion</font>
                     </strong>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">A. Summary of the main points</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0px">
                  <font color="#990099">B. Re-statement of thesis: "Employees should be allowed to wear and display their
                     tattoos on the job."</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Note that as her assignment requires, Val has included two scholarly journals and
                  one book. She has also selected three additional sources appropriate for a scholarly
                  context (she was only required to use two additional sources, but found that she needed
                  three!). She has included data and evidence that should help support the purpose of
                  her paper. She has also included background information to bring her inexperienced
                  audience up to speed on the issue.
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall14.html#">return to top</a> | <a href="Module_Four_Fall13.html">previous page</a> | <a href="Module_Four_Fall15.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall14.pcf">©</a>
      </div>
   </body>
</html>