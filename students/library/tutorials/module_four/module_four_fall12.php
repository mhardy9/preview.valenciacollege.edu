<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 12 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall12.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Page 12</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <strong>Review the answers:</strong>
                  
               </p>
               
               <p>Question #1:</p>
               
               <p>Val wants to make the statement that "Banning tattoos violates employees' rights to
                  Freedom of Speech." Which <strong>more basic piece of information</strong> does the audience need, in order to understand this statement?
               </p>
               
               <ul>
                  
                  <li>
                     <strong>Banning tattoos is unfair.</strong><br>
                     Incorrect answer: This is a conclusion that someone might draw from the facts, but
                     it does not help the audience understand why banning tattoos has anything to do with
                     the Freedom of Speech.
                  </li>
                  
                  <li>
                     <strong>Tattoos are a form of self-expression.</strong><br>
                     Correct answer. This is a statement of basic fact that helps clarify why banning tattoos
                     has to do with the Freedom of Speech.
                  </li>
                  
                  <li>
                     <strong>Tattooing dates back to ancient civilizations.</strong><br>
                     Incorrect answer. This is background information on the topic of tattoos, but it doesn't
                     directly help the audience understand Val's statement.
                  </li>
                  
               </ul>
               
               <p>&nbsp;Question #2:</p>
               
               <p>Val does not want to bore her audience with information they already know. If Val's
                  audience already understands that "tattoos are a form of self expression," then which
                  <strong>more basic piece of information</strong> do they probably <strong>also already</strong> <strong>understand</strong>?
               </p>
               
               <ul>
                  
                  <li>
                     <strong>Tattoos are a type of body art.</strong><br>
                     Correct answer: This is a basic definition of tattoos. If one already knows that tattoos
                     are used as a form of self-expression, it is reasonable to guess that he or she already
                     knows what a tattoo is.
                  </li>
                  
                  <li>
                     <strong>Tattooing dates back to ancient civilizations.</strong><br>
                     Incorrect answer: Just because the audience knows that tattoos are a form of self-expression
                     does not necessarily mean that they have background information about the history
                     of tattoos.
                  </li>
                  
                  <li>
                     <strong>Banning tattoos is unfair.</strong><br>
                     Incorrect answer: Just because the audience knows that tattoos are a form of self-expression
                     does not necessarily mean that they have also drawn this conclusion.
                  </li>
                  
               </ul>
               
               <p>Ordering Activity:</p>
               
               <p>Correct order:</p>
               
               <ol>
                  
                  <li>
                     <strong>Tattoos are a type of body art.</strong><br>
                     Most basic definition of tattoos.
                  </li>
                  
                  <li>
                     <strong>Tattoos are a form of self-expression.</strong><br>
                     More complex idea. For the audience to understand this, they should also know that
                     tattoos are a type of body art.
                  </li>
                  
                  <li>
                     <strong>Banning tattoos violates the Freedom of Speech.</strong><br>
                     More complex idea. For the audience to understand this, they should also know what
                     tattoos are and that they are a form of self expression.
                  </li>
                  
               </ol>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall12.html#">return to top</a> | <a href="Module_Four_Fall11.html">previous page</a> | <a href="Module_Four_Fall13.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall12.pcf">©</a>
      </div>
   </body>
</html>