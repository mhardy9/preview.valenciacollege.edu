<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 17 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall17.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Page 17</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>
                  <font size="5" style="line-height: 1">Test Your Knowledge!&nbsp;</font>
                  
               </p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_four/spacer.gif" name="check24" alt="" id="check24"><a id="a24" title="Question 4" href="javascript:parent.toggletable('quizpopper24')"><img src="http://valenciacollege.edu/library/tutorials/module_four/testyourself_custom.png" alt="Toggle open/close quiz question" title="Question 4" border="0"></a> 
               </p>
               
               <div id="quizpopper24" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 10</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f24" id="f24">Imagine that you have just given a persuasive speech to your class about the medical
                        risks of tattooing. After class, a fellow student asks if you would present on this
                        topic at a Health Fair sponsored by Student Development. The goal of the Health Fair
                        is to inform students about making healthy choices. The atmosphere will be fun, like
                        a carnival.
                        <p>What is different about the Health Fair presentation compared with the speech that
                           you just gave? What, if anything, would you change about your presentation to make
                           it appropriate for the Health Fair?
                        </p>
                        
                        <div style="margin: 1em 15px;"><textarea name="q24" rows="16" wrap="virtual" onfocus="parent.clearEssayFeedback(24)" style="width: 390px;"></textarea></div>
                        
                        <div align="center">
                           <input onclick="parent.check_q(24, 0, 8, true)" type="button" name="Check" value="Finish">&nbsp;&nbsp;&nbsp;<input onclick="parent.print_essay(24)" type="button" name="Print" value="Print">
                           
                        </div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done24" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed24" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall17.html#">return to top</a> | <a href="Module_Four_Fall16.html">previous page</a> | <a href="Module_Four_Fall18.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall17.pcf">©</a>
      </div>
   </body>
</html>