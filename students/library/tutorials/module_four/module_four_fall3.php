<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Factors to Consider: The Assignment | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall3.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Factors to Consider: The Assignment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="factorstoconsider:theassignment" id="factorstoconsider:theassignment"></a><h2>Factors to Consider: The Assignment</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/backpack1.JPG" width="100" alt="backpack1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj7"> Val is glad she started her research with an annotated bibliography; she has a good
                  list of sources to choose from for her final paper. The problem now is deciding which
                  ones should she include.
               </p>
               
               <p>To help her decide, Val first looks at her research paper <strong>assignment</strong>. It says:
               </p>
               
               <p>
                  <font face="Georgia">&nbsp;"Include in your paper references to at least five sources. At least two of the five
                     sources should be scholarly journal articles from the library databases. At least
                     one should be a book (hard-copy print book or e-book is OK). The final two sources
                     can be your choice."</font>
                  
               </p>
               
               <p>&nbsp;Val recalls Professor Sage explaining that the source requirements were intended
                  to <strong>(1) give the students practice accessing and using different kinds of sources, and
                     (2) steer them toward the most appropriate sources for the paper.</strong>&nbsp;
               </p>
               
               <p>Overall, Val is glad to have this guidance from Professor Sage. It does make her decision
                  easier, but she still doesn't know how she'll choose the last two sources for her
                  paper.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: When deciding which sources are most appropriate, the first factor
                        to consider is the requirements of the assignment; if there is one.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall3.html#">return to top</a> | <a href="Module_Four_Fall2.html">previous page</a> | <a href="Module_Four_Fall4.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall3.pcf">©</a>
      </div>
   </body>
</html>