<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Accessible Activity | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/ada_files/ada_activity5.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li><a href="/students/library/tutorials/module_four/ada_files/">Ada Files</a></li>
               <li>Accessible Activity</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p>This is an alternate content page containing a Flash Card Activity.  It has opened
                  in a new window.<br>Here is a list of the terms and definitions on each card.
               </p>
               
               <p>
                  Card 1:<br>
                  Term: Government Agency<br>
                  Definition: Example: Centers for Disease Control (www.cdc.gov). Information from government
                  agencies is professionally researched and backed by the US Government. Appropriate
                  for a scholarly context.<br>
                  Card 2:<br>
                  Term: Reputable News Organization<br>
                  Definition: Example: The New York Times (www.nytimes.com). "Reputable" is the key
                  term here, but well-established news organizations with well-paid, professional journalists
                  are considered appropriate in most scholarly contexts.<br>
                  Card 3:<br>
                  Term: Educational Publisher<br>
                  Definition: Examples: McGraw-Hill, Oxford University Press, Meridian Films. Books,
                  films and web sites from these publishers are meant for educational uses and are appropriate
                  in a scholarly context.<br>
                  Card 4:<br>
                  Term: Scholarly Journal<br>
                  Definition: Examples: Journal of the American Medical Association, Wilson Quarterly.
                  Scholarly journals are meant to educate scholars and students. Appropriate for a scholarly
                  context.<br>
                  Card 5:<br>
                  Term: Professional Organization<br>
                  Definition: Examples: Society of Permanent Cosmetic Professionals (www.spcp.org),
                  American Medical Association (www.ama.org). These organizations are made up of professionals
                  in a certain field and contribute to scholarship in the field. Appropriate for a scholarly
                  context.<br>
                  Card 6:<br>
                  Term: Content Farm<br>
                  Definition: Examples: About.com, E-How.com. These web sites often claim to offer educational
                  information, but their content is primarily aimed at directing Internet traffic toward
                  their advertisements. Content on a wide variety of subjects is written by freelance
                  writers with little expertise. Appropriate for informal use.<br>
                  Card 7:<br>
                  Term: Online Forum<br>
                  Definition: Examples: The Tattoo Forum (www.thetattooforum.com), or any online user
                  reviews. Online forums are generally informal and not a scholarly exchange of ideas.
                  "Credentials" of other contributors are usually not verifiable. Appropriate in an
                  informal context.<br>
                  Card 8:<br>
                  Term: Social Network<br>
                  Definition: Examples: Twitter, Facebook. The online equivalent of a discussion with
                  friends, social media sites are informal and not a scholarly exchange of ideas. Appropriate
                  for an informal context.<br>
                  Card 9:<br>
                  Term: Blog<br>
                  Definition: Examples: CNN News Blog (news.blogs.cnn.com), Needles and Sins (needlesandsins.com),
                  Tattoos Day (tattoosday.blogspot.com). Blogs are typically unedited, personal, web
                  sites that usually express the writer's viewpoint and invite comments by others. Appropriate
                  for an informal context.<br>
                  Card 10:<br>
                  Term: Personal Web Site<br>
                  Definition: Examples: MySpace pages, Angelfire sites. An individual's unedited personal
                  opinion or information. Appropriate for an informal context.<br>
                  Card 11:<br>
                  Term: Known Expert Individual or Organization<br>
                  Definition: Example: Mayo Clinic (www.mayoclinic.com). A reputable organization or
                  expert individual can provide information appropriate for a scholarly context.<br>
                  Card 12:<br>
                  Term: Wiki<br>
                  Definition: Examples: WikiHow (wikihow.com), Wikipedia (wikipedia.org). A web site
                  that invites readers to contribute to and collaboratively build its content. Since
                  contributions are usually anonymous, wikis are generally appropriate for informal
                  use.<br>
                  
               </p>
               
               <p></p>
               
               <br><a href="javascript:window.close();">Close this window.</a>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/ada_files/ada_activity5.pcf">©</a>
      </div>
   </body>
</html>