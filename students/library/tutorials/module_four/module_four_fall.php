<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Module 4 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Module 4</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="module4" id="module4"></a><h2>
                  <strong>
                     <u>Module 4</u>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong>
                  
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/val_altb1.JPG" width="325" alt="val_altb1.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj6"> <strong><font size="5" style="line-height: 1">Scene 4</font></strong></p>
               
               <p>English class, Thursday morning. Val is talking to her professor. Matt is sitting
                  in the front row, nearby.
               </p>
               
               <p><strong>PROFESSOR SAGE:</strong> Val, I thought your annotated bibliography was very good. You selected some excellent
                  sources on tattoos.
               </p>
               
               <p><strong>VAL:</strong> Oh, thanks! I was really surprised at how much I found, especially after I talked
                  with the librarian. Who would have guessed so many people would write about tattoos?
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>Should it surprise you? <em>You</em> wanted to write about them!
               </p>
               
               <p><strong>VAL:</strong> [Smiles] True, but only because I wanted to get a tattoo, myself.
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>So you were doing your own personal research at the same time?</p>
               
               <p><strong>VAL:</strong> I guess that's true. I hadn't really thought about it like that, but I really was
                  doing my own research. I didn't want to have any regrets.
               </p>
               
               <p><strong>MATT:</strong> [Pointing to his upper arm] Smart! I'd love to have this one removed - it's my ex's
                  name!
               </p>
               
               <p><strong>VAL:</strong> [Grimaces] Ouch!
               </p>
               
               <p><strong>MATT:</strong> Did you end up getting your tattoo?
               </p>
               
               <p><strong>VAL:</strong> Yep! [She exposes a small flower tattooed just above her hip.] I wasn't sure at first
                  if I should, but I talked with my friends about it, and they kinda talked me into
                  it. So I found some online reviews that recommended a really good place. And I got
                  the picture of the flower I wanted from a tattoo blog I found.
               </p>
               
               <p><strong>PROF:</strong> Well, today I'll be introducing our new research paper assignment, so just remember,
                  Val, you'll use more scholarly sources for your paper than you did for your personal
                  research. [Addressing the class] Alright, everyone, let's get started. [She begins
                  distributing the new assignment sheet.]
               </p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall.html#">return to top</a> | <a href="Module_Four_Fall2.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall.pcf">©</a>
      </div>
   </body>
</html>