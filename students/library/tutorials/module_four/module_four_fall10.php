<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Factors to Consider: Characteristics of the Audience | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall10.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Factors to Consider: Characteristics of the Audience</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="factorstoconsider:characteristicsoftheaudience" id="factorstoconsider:characteristicsoftheaudience"></a><h2 align="left">Factors to Consider: Characteristics of the Audience</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/blocks.JPG" width="100" alt="blocks.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj16"> One final note in Val's assignment helps as well. It says to <font face="Georgia">"assume the audience has little background knowledge on the topic."</font></p>
               
               <p>Val had figured that Professor Sage was the audience for her paper, but she now realizes
                  that Professor Sage wants her to imagine an <strong>audience</strong> for her paper - <strong>people who know very little about the issue of tattoos in the workplace</strong>.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Val recalls what it was like to read a textbook when she knew very little about the
                  topic. It was helpful that the textbook gave a lot of background information, such
                  as definitions and history of the topic, to help her understand.
               </p>
               
               <p>On the other hand, Val did not want to be bored with facts she already knew. When
                  the information was too basic, it seemed like a waste of time. Val realizes that she
                  needs to <strong>build on the audience's prior knowledge of the topic</strong>.
               </p>
               
               <p>Val now understands that the <strong>characteristics of her audience</strong> - what they already know, and what they need to know - about the topic, should help
                  her decide what information to include from her sources.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: When deciding which information to include, consider the characteristics
                        of the audience, such as their current knowledge and what they need to know about
                        the topic.</u>
                     </strong>
                  
               </p>
               
               <p>In the activity on the following page, help Val choose the most useful background
                  information for her audience.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall10.html#">return to top</a> | <a href="Module_Four_Fall9.html">previous page</a> | <a href="Module_Four_Fall11.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall10.pcf">©</a>
      </div>
   </body>
</html>