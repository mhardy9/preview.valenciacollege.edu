<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Factors to Consider: Context of the Presentation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall4.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Factors to Consider: Context of the Presentation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="factorstoconsider:contextofthepresentation" id="factorstoconsider:contextofthepresentation"></a><h2>Factors to Consider: Context of the Presentation</h2>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/friends2.JPG" width="325" alt="friends2.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj8"> Val continues reading her assignment sheet:
               </p>
               
               <p>
                  <font face="Georgia">"Remember, this research paper is a scholarly study of a topic, not a personal manifesto.
                     Your final product should be professionally written and should include credible sources
                     appropriate to a scholarly context."</font>
                  
               </p>
               
               <p>Val now realizes why Professor Sage wanted her to use at least two scholarly journal
                  articles. The requirement helped Val choose sources appropriate for a scholarly <strong>context</strong>.
               </p>
               
               <p>This also explains Professor Sage's comment to Val that her paper would be based on
                  different kinds of sources from those used for her personal decision. She thinks about
                  the comments she has gotten from her friends, online reviews she has looked at, and
                  blogs she has read. They have all helped her decide on her new tattoo ... but Val
                  knows she won't include them in her paper.
               </p>
               
               <p><strong><u>The bottom line: When deciding which sources are most appropriate, consider the context
                        of the presentation, including whether it is scholarly/professional or informal/personal.</u></strong>&nbsp;
               </p>
               
               <p>In the activity on the following page, review the following types of sources and their
                  appropriate contexts -- scholarly/professional or informal/personal.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall4.html#">return to top</a> | <a href="Module_Four_Fall3.html">previous page</a> | <a href="Module_Four_Fall5.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall4.pcf">©</a>
      </div>
   </body>
</html>