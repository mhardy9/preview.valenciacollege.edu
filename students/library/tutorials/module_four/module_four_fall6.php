<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Page 6 | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall6.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Page 6</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p align="left">
                  <strong>Review the answers:</strong>
                  
               </p>
               
               <p align="left">
                  <strong>The following sources are usually appropriate in a scholarly context:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Government Agency</li>
                  
                  <li>Educational Publisher</li>
                  
                  <li>Reputable News Organization</li>
                  
                  <li>Scholarly Journal</li>
                  
                  <li>Professional Organization</li>
                  
                  <li>Known Expert Individual or Organization</li>
                  
               </ul>
               
               <p>
                  <strong>The following sources are usually appropriate only in an informal context:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Online Forum</li>
                  
                  <li>Social Network</li>
                  
                  <li>Blog</li>
                  
                  <li>Content Farm</li>
                  
                  <li>Personal Web Site</li>
                  
                  <li>Wiki</li>
                  
               </ul>
               
               <p>Answer the following quiz question to check your understanding of this concept:</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_four/spacer.gif" name="check21" alt="" id="check21"><a id="a21" title="Question 1" href="javascript:parent.toggletable('quizpopper21')"><img src="http://valenciacollege.edu/library/tutorials/module_four/quizme_custom.png" alt="Toggle open/close quiz question" title="Question 1" border="0"></a> 
               </p>
               
               <div id="quizpopper21" class="expand">
                  
                  <div style="padding: 5px 10px;">Value: 3</div>
                  
                  <div class="qpq" style="border: 1px solid #000000; background: #cccccc; line-height: 1.5em; padding: 10px 15px; width: 430px;">
                     
                     <form name="f21" id="f21">Val finds information on the medical risks of tattooing from several different web
                        sites. Which THREE sources would be the most appropriate options for her scholarly
                        paper? (Choose the best three answers).
                        <div style="margin: 1em 15px;">
                           <input type="checkbox" name="q21" value="a" id="q21a">&nbsp;<label for="q21a"><strong>a.</strong>&nbsp;National Institute of Health (health.nih.gov)</label><br><input type="checkbox" name="q21" value="b" id="q21b">&nbsp;<label for="q21b"><strong>b.</strong>&nbsp;World Health Organization (www.who.int)</label><br><input type="checkbox" name="q21" value="c" id="q21c">&nbsp;<label for="q21c"><strong>c.</strong>&nbsp;HowStuffWorks (www.howstuffworks.com)</label><br><input type="checkbox" name="q21" value="d" id="q21d">&nbsp;<label for="q21d"><strong>d.</strong>&nbsp;Tattoo Artist Magazine Blog (tattooartistmagazineblog.com)
                              <p>&nbsp;</p></label><br><input type="checkbox" name="q21" value="e" id="q21e">&nbsp;<label for="q21e"><strong>e.</strong>&nbsp;American Society for Dermatologic Surgery (www.asds.net)</label><br><input type="checkbox" name="q21" value="f" id="q21f">&nbsp;<label for="q21f"><strong>f.</strong>&nbsp;WikiHealth (wikihealth.com)</label><br><input type="checkbox" name="q21" value="g" id="q21g">&nbsp;<label for="q21g"><strong>g.</strong>&nbsp;New York Hardcore Tattoos on MySpace (www.myspace.com/hardcorenyc)
                              <p>&nbsp;</p>
                              
                              <p>&nbsp;</p></label><p></p>
                           <span style="font-size: 90%;">[mark all correct answers]</span>
                           
                        </div>
                        
                        <div align="center"><input onclick="parent.check_q(21, 7, 3, true)" type="button" name="Check" value="Check Answer"></div>
                        
                     </form>
                     
                     <div class="collapse" id="f_done21" style="margin: 1em;"></div>
                     
                     <div class="collapse" id="feed21" style="font-family: Comic Sans MS; border-top: 1px solid #000000; margin: 1em;"></div>
                     
                  </div>
                  
               </div>
               
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall6.html#">return to top</a> | <a href="Module_Four_Fall5.html">previous page</a> | <a href="Module_Four_Fall7.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall6.pcf">©</a>
      </div>
   </body>
</html>