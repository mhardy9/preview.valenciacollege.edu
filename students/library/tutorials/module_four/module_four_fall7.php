<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Factors to Consider: Purpose of the Presentation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall7.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Factors to Consider: Purpose of the Presentation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="factorstoconsider:purposeofthepresentation" id="factorstoconsider:purposeofthepresentation"></a><h2>Factors to Consider: Purpose of the Presentation</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/point1.JPG" width="100" alt="point1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj15"> Val now has a good idea of which sources are most appropriate to use in her paper.
                  But she's not exactly sure what information to include in her paper. Which facts and
                  ideas are important enough to put in her paper?
               </p>
               
               <p>Val recalls Professor Sage emphasizing that the research paper would be persuasive
                  - Val needs to take a stand on an issue related to her topic.
               </p>
               
               <p>Val decides to take the side that <em>'employees should be allowed to wear and display their tattoos on the job.'</em> &nbsp; She will now look for information and quotes that help accomplish the <strong>purpose of the paper</strong> - to persuade the "audience" of her point of view.
               </p>
               
               <p>Val sees that she has some articles with opinions in support of her side and some
                  opposing. Also, she has some statistics and primary sources that don't express a point
                  of view, but could be used to demonstrate her main ideas.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: When deciding which information to include, consider the purpose
                        of the presentation, and include information that helps accomplish that goal.</u>
                     </strong>
                  
               </p>
               
               <p>Complete the activity on the following page to help Val outline the points she will
                  make in her paper.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2" style="line-height: 1">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="Module_Four_Fall7.html#">return to top</a> | <a href="Module_Four_Fall6.html">previous page</a> | <a href="Module_Four_Fall8.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall7.pcf">©</a>
      </div>
   </body>
</html>