<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Information Literacy 4 - Applying Information | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/module_four/module_four_fall_print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/module_four/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/module_four/">Module Four</a></li>
               <li>Information Literacy 4 - Applying Information</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <p><strong>Information Literacy 4 - Applying Information</strong><br>Valencia College Libraries
               </p>
               
               
               <h2>
                  <strong>
                     <u>Module 4</u>
                     </strong>
                  
               </h2>
               
               <p>
                  <strong>[Read the following scene. Then, click "next page" to begin the learning module.]</strong>
                  
               </p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/val_altb1.JPG" width="325" alt="val_altb1.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj6"> <strong><font size="5">Scene 4</font></strong></p>
               
               <p>English class, Thursday morning. Val is talking to her professor. Matt is sitting
                  in the front row, nearby.
               </p>
               
               <p><strong>PROFESSOR SAGE:</strong> Val, I thought your annotated bibliography was very good. You selected some excellent
                  sources on tattoos.
               </p>
               
               <p><strong>VAL:</strong> Oh, thanks! I was really surprised at how much I found, especially after I talked
                  with the librarian. Who would have guessed so many people would write about tattoos?
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>Should it surprise you? <em>You</em> wanted to write about them!
               </p>
               
               <p><strong>VAL:</strong> [Smiles] True, but only because I wanted to get a tattoo, myself.
               </p>
               
               <p>
                  <strong>PROFESSOR SAGE:</strong>
                  
               </p>
               
               <p>So you were doing your own personal research at the same time?</p>
               
               <p><strong>VAL:</strong> I guess that's true. I hadn't really thought about it like that, but I really was
                  doing my own research. I didn't want to have any regrets.
               </p>
               
               <p><strong>MATT:</strong> [Pointing to his upper arm] Smart! I'd love to have this one removed - it's my ex's
                  name!
               </p>
               
               <p><strong>VAL:</strong> [Grimaces] Ouch!
               </p>
               
               <p><strong>MATT:</strong> Did you end up getting your tattoo?
               </p>
               
               <p><strong>VAL:</strong> Yep! [She exposes a small flower tattooed just above her hip.] I wasn't sure at first
                  if I should, but I talked with my friends about it, and they kinda talked me into
                  it. So I found some online reviews that recommended a really good place. And I got
                  the picture of the flower I wanted from a tattoo blog I found.
               </p>
               
               <p><strong>PROF:</strong> Well, today I'll be introducing our new research paper assignment, so just remember,
                  Val, you'll use more scholarly sources for your paper than you did for your personal
                  research. [Addressing the class] Alright, everyone, let's get started. [She begins
                  distributing the new assignment sheet.]
               </p>
               
               <p align="center">
                  <font size="2">Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p align="center"></p>
               
               <h2>Introduction: Effectively Applying Information</h2>
               
               <p><img align="right" src="http://valenciacollege.edu/library/tutorials/module_four/flower1.JPG" width="150" alt="flower1.JPG" class="resizable" height="150" border="0" hspace="5" metadata="obj4"> The next step in research is to know <u>how and when to use the information gathered</u>. Now that Val has collected several sources on her topic, she has to decide:
               </p>
               
               <ul>
                  
                  <li>Which sources are most appropriate for her research paper</li>
                  
                  <li>Which information from her sources is most important to include</li>
                  
                  <li>Whether to directly quote or paraphrase from her sources</li>
                  
                  <li>Whether to present the information graphically or in the text of her paper.&nbsp;</li>
                  
               </ul>
               
               <p><strong><u>This module explains how Val decided which sources to use in her paper and how to
                        apply them. Complete all activities to learn how to apply sources in different situations.</u></strong>&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Factors to Consider: The Assignment</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/backpack1.JPG" width="100" alt="backpack1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj7"> Val is glad she started her research with an annotated bibliography; she has a good
                  list of sources to choose from for her final paper. The problem now is deciding which
                  ones should she include.
               </p>
               
               <p>To help her decide, Val first looks at her research paper <strong>assignment</strong>. It says:
               </p>
               
               <p>
                  <font face="Georgia">&nbsp;"Include in your paper references to at least five sources. At least two of the five
                     sources should be scholarly journal articles from the library databases. At least
                     one should be a book (hard-copy print book or e-book is OK). The final two sources
                     can be your choice."</font>
                  
               </p>
               
               <p>&nbsp;Val recalls Professor Sage explaining that the source requirements were intended
                  to <strong>(1) give the students practice accessing and using different kinds of sources, and
                     (2) steer them toward the most appropriate sources for the paper.</strong>&nbsp;
               </p>
               
               <p>Overall, Val is glad to have this guidance from Professor Sage. It does make her decision
                  easier, but she still doesn't know how she'll choose the last two sources for her
                  paper.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: When deciding which sources are most appropriate, the first factor
                        to consider is the requirements of the assignment; if there is one.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Factors to Consider: Context of the Presentation</h2>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/friends2.JPG" width="325" alt="friends2.JPG" class="resizable" height="325" border="0" hspace="10" metadata="obj8"> Val continues reading her assignment sheet:
               </p>
               
               <p>
                  <font face="Georgia">"Remember, this research paper is a scholarly study of a topic, not a personal manifesto.
                     Your final product should be professionally written and should include credible sources
                     appropriate to a scholarly context."</font>
                  
               </p>
               
               <p>Val now realizes why Professor Sage wanted her to use at least two scholarly journal
                  articles. The requirement helped Val choose sources appropriate for a scholarly <strong>context</strong>.
               </p>
               
               <p>This also explains Professor Sage's comment to Val that her paper would be based on
                  different kinds of sources from those used for her personal decision. She thinks about
                  the comments she has gotten from her friends, online reviews she has looked at, and
                  blogs she has read. They have all helped her decide on her new tattoo ... but Val
                  knows she won't include them in her paper.
               </p>
               
               <p><strong><u>The bottom line: When deciding which sources are most appropriate, consider the context
                        of the presentation, including whether it is scholarly/professional or informal/personal.</u></strong>&nbsp;
               </p>
               
               <p>In the activity on the following page, review the following types of sources and their
                  appropriate contexts -- scholarly/professional or informal/personal.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Photo credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Activity</strong>
                  
               </p>
               
               <p>The following activity will introduce 12 different types of sources. Click the "Show"
                  button beneath each flash card (or check the "Show Definition" box once) to see examples
                  and definitions, and to discover in which context it belongs. Click the arrow to see
                  the next card.
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Review the answers:</strong>
                  
               </p>
               
               <p align="left">
                  <strong>The following sources are usually appropriate in a scholarly context:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Government Agency</li>
                  
                  <li>Educational Publisher</li>
                  
                  <li>Reputable News Organization</li>
                  
                  <li>Scholarly Journal</li>
                  
                  <li>Professional Organization</li>
                  
                  <li>Known Expert Individual or Organization</li>
                  
               </ul>
               
               <p>
                  <strong>The following sources are usually appropriate only in an informal context:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Online Forum</li>
                  
                  <li>Social Network</li>
                  
                  <li>Blog</li>
                  
                  <li>Content Farm</li>
                  
                  <li>Personal Web Site</li>
                  
                  <li>Wiki</li>
                  
               </ul>
               
               <p>Answer the following quiz question to check your understanding of this concept:</p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_four/quizme_custom.png" alt="Toggle open/close quiz question" border="0" title="Question 1"></p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Factors to Consider: Purpose of the Presentation</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/point1.JPG" width="100" alt="point1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj15"> Val now has a good idea of which sources are most appropriate to use in her paper.
                  But she's not exactly sure what information to include in her paper. Which facts and
                  ideas are important enough to put in her paper?
               </p>
               
               <p>Val recalls Professor Sage emphasizing that the research paper would be persuasive
                  - Val needs to take a stand on an issue related to her topic.
               </p>
               
               <p>Val decides to take the side that <em>'employees should be allowed to wear and display their tattoos on the job.'</em> &nbsp; She will now look for information and quotes that help accomplish the <strong>purpose of the paper</strong> - to persuade the "audience" of her point of view.
               </p>
               
               <p>Val sees that she has some articles with opinions in support of her side and some
                  opposing. Also, she has some statistics and primary sources that don't express a point
                  of view, but could be used to demonstrate her main ideas.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: When deciding which information to include, consider the purpose
                        of the presentation, and include information that helps accomplish that goal.</u>
                     </strong>
                  
               </p>
               
               <p>Complete the activity on the following page to help Val outline the points she will
                  make in her paper.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Activity</strong>
                  
               </p>
               
               <p>Val has decided on two main arguments to support her claim that "employees should
                  be allowed to wear and display their tattoos on the job":
               </p>
               
               <p>(1) Banning tattoos violates the Freedom of Speech.</p>
               
               <p>(2) Tattoos have become more accepted today.</p>
               
               <p>Also, her assignment requires that she include a third paragraph explaining the opposing
                  side, in favor of workplace tattoo bans.
               </p>
               
               <p>
                  <strong>In the following activity, sort the cards into appropriate piles based on where Val
                     should include each piece of information in her paper.</strong>
                  
               </p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Review the answers:</strong>
                  
               </p>
               
               <p align="left">
                  <strong>Banning tattoos violates the Freedom of Speech:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Text of the First Amendment</li>
                  
                  <li>Examples of tattoos as a form of speech</li>
                  
               </ul>
               
               <p>
                  <strong>Tattoos have become more accepted today:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Statistics showing increasing numbers of Americans with tattoos</li>
                  
                  <li>Poll data showing positive public opinions on tattoos</li>
                  
               </ul>
               
               <p>Note that in the above paragraphs, Val uses statistics, data and primary sources to
                  support her point. <strong>Even though the sources are neutral and do not actually express a point of view on
                     Val's issue, Val can use the information</strong> as evidence in her argument.
               </p>
               
               <p>
                  <strong>Opposing argument: For tattoo bans:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Legal case upholding employers' rights to ban tattoos</li>
                  
                  <li>Statistics showing increased sales at businesses that ban tattoos</li>
                  
               </ul>
               
               <p>The important point is that <strong>Val is able to use sources in her paper even if they support the opposing side</strong>. Explaining the other side gives the audience a more complete understanding of the
                  issue. Since Val's paper is persuasive, she will use her other two main points to
                  argue against this side.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2 align="left">Factors to Consider: Characteristics of the Audience</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/blocks.JPG" width="100" alt="blocks.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj16"> One final note in Val's assignment helps as well. It says to <font face="Georgia">"assume the audience has little background knowledge on the topic."</font></p>
               
               <p>Val had figured that Professor Sage was the audience for her paper, but she now realizes
                  that Professor Sage wants her to imagine an <strong>audience</strong> for her paper - <strong>people who know very little about the issue of tattoos in the workplace</strong>.
               </p>
               
               <p>&nbsp;</p>
               
               <p>Val recalls what it was like to read a textbook when she knew very little about the
                  topic. It was helpful that the textbook gave a lot of background information, such
                  as definitions and history of the topic, to help her understand.
               </p>
               
               <p>On the other hand, Val did not want to be bored with facts she already knew. When
                  the information was too basic, it seemed like a waste of time. Val realizes that she
                  needs to <strong>build on the audience's prior knowledge of the topic</strong>.
               </p>
               
               <p>Val now understands that the <strong>characteristics of her audience</strong> - what they already know, and what they need to know - about the topic, should help
                  her decide what information to include from her sources.
               </p>
               
               <p>
                  <strong>
                     <u>The bottom line: When deciding which information to include, consider the characteristics
                        of the audience, such as their current knowledge and what they need to know about
                        the topic.</u>
                     </strong>
                  
               </p>
               
               <p>In the activity on the following page, help Val choose the most useful background
                  information for her audience.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Activity</h2>
               
               <p>Try the two multiple choice questions first. Then complete the ordering activity.</p>
               
               <p>&nbsp;Show/Hide Question #1</p>
               
               <p>&nbsp;Show/Hide Question #2</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>&nbsp;</strong>
                  
               </p>
               
               <p align="center"></p>
               
               <p>
                  <strong>Review the answers:</strong>
                  
               </p>
               
               <p>Question #1:</p>
               
               <p>Val wants to make the statement that "Banning tattoos violates employees' rights to
                  Freedom of Speech." Which <strong>more basic piece of information</strong> does the audience need, in order to understand this statement?
               </p>
               
               <ul>
                  
                  <li>
                     <strong>Banning tattoos is unfair.</strong><br>
                     Incorrect answer: This is a conclusion that someone might draw from the facts, but
                     it does not help the audience understand why banning tattoos has anything to do with
                     the Freedom of Speech.
                  </li>
                  
                  <li>
                     <strong>Tattoos are a form of self-expression.</strong><br>
                     Correct answer. This is a statement of basic fact that helps clarify why banning tattoos
                     has to do with the Freedom of Speech.
                  </li>
                  
                  <li>
                     <strong>Tattooing dates back to ancient civilizations.</strong><br>
                     Incorrect answer. This is background information on the topic of tattoos, but it doesn't
                     directly help the audience understand Val's statement.
                  </li>
                  
               </ul>
               
               <p>&nbsp;Question #2:</p>
               
               <p>Val does not want to bore her audience with information they already know. If Val's
                  audience already understands that "tattoos are a form of self expression," then which
                  <strong>more basic piece of information</strong> do they probably <strong>also already</strong> <strong>understand</strong>?
               </p>
               
               <ul>
                  
                  <li>
                     <strong>Tattoos are a type of body art.</strong><br>
                     Correct answer: This is a basic definition of tattoos. If one already knows that tattoos
                     are used as a form of self-expression, it is reasonable to guess that he or she already
                     knows what a tattoo is.
                  </li>
                  
                  <li>
                     <strong>Tattooing dates back to ancient civilizations.</strong><br>
                     Incorrect answer: Just because the audience knows that tattoos are a form of self-expression
                     does not necessarily mean that they have background information about the history
                     of tattoos.
                  </li>
                  
                  <li>
                     <strong>Banning tattoos is unfair.</strong><br>
                     Incorrect answer: Just because the audience knows that tattoos are a form of self-expression
                     does not necessarily mean that they have also drawn this conclusion.
                  </li>
                  
               </ul>
               
               <p>Ordering Activity:</p>
               
               <p>Correct order:</p>
               
               <ol>
                  
                  <li>
                     <strong>Tattoos are a type of body art.</strong><br>
                     Most basic definition of tattoos.
                  </li>
                  
                  <li>
                     <strong>Tattoos are a form of self-expression.</strong><br>
                     More complex idea. For the audience to understand this, they should also know that
                     tattoos are a type of body art.
                  </li>
                  
                  <li>
                     <strong>Banning tattoos violates the Freedom of Speech.</strong><br>
                     More complex idea. For the audience to understand this, they should also know what
                     tattoos are and that they are a form of self expression.
                  </li>
                  
               </ol>
               
               <p align="center"></p>
               
               <h2>Factors to Consider: Prior Knowledge</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/degree1.JPG" width="100" alt="degree1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj12"> Still, one question bothers Val. Professor Sage specifically wanted students to choose
                  a topic of personal interest. <em>Does that mean she should include what she already knew about tattoos before doing
                     her research?</em></p>
               
               <p>Unsure, Val sends Professor Sage an e-mail. An hour later, she receives this response:</p>
               
               <p>
                  <font face="Georgia">"Good question, Val! I ask that you only use scholarly sources for this paper. Scholarly
                     sources are from experts on the topic. Therefore, it depends on whether or not you
                     are an expert on tattoos! If you are an expert, then <strong>please include your credentials</strong> in the paper so that I know. <strong>If you're not an expert, then it's best to quote someone who is</strong>!"</font>
                  
               </p>
               
               <p>Although Val is pretty confident in her knowledge of tattoos, she realizes that she
                  doesn't have a degree or any formal experience to back it up. She decides to quote
                  her sources instead.
               </p>
               
               <p><strong><u>The bottom line: Include your prior knowledge of the topic <em>only</em> if you can give credentials to back up your information.</u></strong>&nbsp;
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p align="left">
                  <strong>Val's Paper So Far...</strong>
                  
               </p>
               
               <p align="left">Pause here and take a look at how Val has planned out her paper at this point.</p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">I. Introduction</font>
                     </strong>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">A. Background information / Hook: Tattoos are a form of self-expression (Source: Book,
                     <em>Written on the body : the Tattoo in European and American History</em>, Princeton University Press -- educational publisher)</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">B. Background information: Description of the issue.</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">C. Thesis: "Employees should be allowed to wear and display their tattoos on the job."</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">II. Main Point #1: Banning tattoos violates the Freedom of Speech</font>
                     </strong>
                  
               </p>
               
               <p style="margin-left: 20.0">
                  <font color="#990099">A. Discussion of Freedom of Speech (Source: Text of the First Amendment -- government
                     document)</font>
                  
               </p>
               
               <p style="margin-left: 20.0">
                  <font color="#990099">B. Tattoos as a form of speech (Source: article from <em>Wake Forest Law Review</em> -- scholarly journal)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">III. Main Point #2: Tattoos have become more accepted today.</font>
                     </strong>
                  
               </p>
               
               <p style="margin-left: 20.0">
                  <font color="#990099">A. More Americans now have tattoos (Source: Book, <em>Written on the body : the tattoo in European and American history</em>, Princeton University Press -- educational publisher)</font>
                  
               </p>
               
               <p style="margin-left: 20.0">
                  <font color="#990099">B. Americans' attitudes about tattoos are improving (Source: Pew Research Center --
                     expert organization)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">IV. Opposing Argument: Employers should be allowed to regulate tattoos.</font>
                     </strong>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">A. Courts have upheld employers' legal rights to ban tattoos. (Source: article from
                     <em>Advanced Management Journal</em> -- scholarly journal)</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">B. Employers report increased sales when dress codes are enforced (Source: National
                     Public Radio -- reputable news organization)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">
                  <strong>
                     <font color="#990099">V. Conclusion</font>
                     </strong>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">A. Summary of the main points</font>
                  
               </p>
               
               <p align="left" style="margin-left: 20.0">
                  <font color="#990099">B. Re-statement of thesis: "Employees should be allowed to wear and display their
                     tattoos on the job."</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="left">Note that as her assignment requires, Val has included two scholarly journals and
                  one book. She has also selected three additional sources appropriate for a scholarly
                  context (she was only required to use two additional sources, but found that she needed
                  three!). She has included data and evidence that should help support the purpose of
                  her paper. She has also included background information to bring her inexperienced
                  audience up to speed on the issue.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Format of the Presentation</h2>
               
               <p>&nbsp;</p>
               
               <p><img align="right" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/barista.JPG" width="325" alt="barista.JPG" class="resizable" height="216" border="0" hspace="10" metadata="obj19"> At last, Val is nearly finished with her paper. But, she's still undecided about
                  a few things:
               </p>
               
               <ul>
                  
                  <li>Val is having trouble deciding when to quote her sources directly and when to paraphrase.
                     It seems like a random choice.
                  </li>
                  
               </ul>
               
               <ul>
                  
                  <li>She wants to include a great picture of a barista with a tattoo on her arm. It really
                     makes her point about how tattoos in the workplace are a form of self-expression!
                     Should she paste the picture into her paper, or just describe it?
                  </li>
                  
               </ul>
               
               <ul>
                  
                  <li>Val has some statistics, but she's not sure if she should just write them out, or
                     include a graph in her paper.
                  </li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p>Val reviews what she has learned about the factors to consider. She considers the
                  <strong>assignment</strong> first, but there are no directions about paraphrasing, pictures, or graphs. Unsure
                  what to do, she visits Professor Sage during office hours.
               </p>
               
               <p>Professor Sage suggests that Val use her best judgment based on what she has learned
                  so far about integrating sources into her paper.
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">(Photo credit: matthanns. DSC_3355. Used under a Creative Commons Attribution-NonCommercial-ShareAlike
                     2.0 Generic license.)</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/puzzle1.JPG" width="100" alt="puzzle1.JPG" class="resizable" height="100" border="0" hspace="10" metadata="obj14"> First, Val considers the <strong>purposes of the presentation</strong>. Since her purpose is to persuade the audience, Val decides to use the picture, because
                  she thinks the image will be more powerful than simply describing it.
               </p>
               
               <p>Next, Val considers the <strong>context</strong> of the paper. It is an academic paper and needs to be professionally written. Val
                  decides that a graph will look neat and professional in her paper.
               </p>
               
               <p>Finally, Val considers the <strong>characteristics of the audience</strong>. The audience has little background knowledge of the topic. Based on this, Val decides
                  to paraphrase instead of directly quoting from her source, so that she can make the
                  language less technical and help her audience understand.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <strong>
                     <u>The bottom line: Decisions on the format for presenting information should be based
                        on the requirements of the assignment first. However, beyond that, consider the context
                        of the presentation, the purposes of the presentation, and the characteristics of
                        the audience.</u>
                     </strong>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>Please continue to the next slide to <strong>test your knowledge</strong>.&nbsp;
               </p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <p>
                  <font size="5">Test Your Knowledge!&nbsp;</font>
                  
               </p>
               
               <p>&nbsp;<img src="http://valenciacollege.edu/library/tutorials/module_four/testyourself_custom.png" alt="Toggle open/close quiz question" border="0" title="Question 4"></p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p align="center"></p>
               
               <h2>Conclusion</h2>
               
               <p><img align="left" vspace="10" src="http://valenciacollege.edu/library/tutorials/module_four/flower1.JPG" width="150" alt="flower1.JPG" class="resizable" height="150" border="0" hspace="10" metadata="obj17"> <strong>This concludes Tutorial #4: Applying Information. In this module, you learned:</strong></p>
               
               <ul>
                  
                  <li>Factors to consider when choosing appropriate sources for an assignment: requirements
                     of the assignment, context of the presentation.
                  </li>
                  
                  <li>Factors to consider when choosing which information to include in a paper or presentation:
                     purpose of the presentation, characteristics of the audience.
                  </li>
                  
                  <li>When and how to use / avoid using your own personal knowledge in a paper or presentation.</li>
                  
               </ul>
               
               <p>
                  <strong>You also acquired the following skills:</strong>
                  
               </p>
               
               <ul>
                  
                  <li>Identify sources appropriate for a scholarly/professional or informal/personal context.</li>
                  
                  <li>Given an outline of main ideas, categorize supporting details into appropriate paragraphs
                     for a research paper.
                  </li>
                  
                  <li>Arrange ideas from more basic to more complex.</li>
                  
               </ul>
               
               <p>&nbsp;</p>
               
               <p align="center">
                  <font size="2">Image credit: Microsoft Clip Gallery</font>
                  
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/module_four/module_four_fall_print.pcf">©</a>
      </div>
   </body>
</html>