<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>The Purpose of Citation | Valencia College</title><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/whycite/whycite2.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/whycite/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li><a href="/students/library/tutorials/whycite/">Whycite</a></li>
               <li>The Purpose of Citation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		<a name="thepurposeofcitation" id="thepurposeofcitation"></a><h2>The Purpose of Citation</h2>
               
               <p>In either writing or speaking if you do not acknowledge your sources of information,
                  your audience is led to believe that everything presented is your own work. Citing
                  helps your audience differentiate between your own work and that of your sources.
               </p>
               
               <p>Using the work of experts can increase your credibility but for that to work the audience
                  has to know that you are using the expert's work and be able to assess the credibility
                  of the expert.
               </p>
               
               <p>For instance if I tell you that in 2016 Social Security is going to start spending
                  more than it takes in, you must assume that I am making that statement from my own
                  knowledge, and what do I know about Social Security spending? However if I tell you
                  that in a July 2010 report the Congressional Budget Office (a non-partisan Congressional
                  agency charged with evaluating Congressional proposals for their financial impact.)
                  projects that in 2016 Social Security is going to start spending more than it takes
                  in, you can evaluate whether you trust the Congressional Budget Office as a source
                  of information. You can go and find that report and read the statement for yourself
                  and decide whether I have interpreted it correctly.
               </p>
               
               <p>&nbsp;</p>
               
               <p>
                  <img hspace="30" vspace="10" class="resizable" height="520" longdesc="ada_files/ada_image1.html" alt="Social Security Benefits -2009-2083" src="http://valenciacollege.edu/library/tutorials/whycite/800px-Social_Security_Benefits_-_2009-2083.png" width="800" border="0">
                  
               </p>
               
               <p>&nbsp;(Image credit: Rcragun. <a href="http://commons.wikimedia.org/wiki/File:Social_Security_Benefits_-_2009-2083.png" target="_blank">Social Security Benefits - 2009-2083.png</a>. Used under a <a href="http://en.wikipedia.org/wiki/en:Creative_Commons" title="w:en:Creative Commons">Creative Commons</a> <a href="http://creativecommons.org/licenses/by/3.0/deed.en" rel="nofollow">Attribution 3.0 Unported</a> license.)
               </p>
               
               <p>&nbsp;</p>
               
               <p>Fig. 1: Rcragun. "Social Security Benefits - 2009-2083.png." Chart. <em>Wikimedia Commons,</em> 7 Oct. 2009. Web. 16 Aug. 2010.
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               <p>Citation also gives your audience the means to find your sources.</p>
               
               <p>Have you ever listened to a speech and thought that a particular web site, article
                  or book sounded really interesting and wanted to find it for yourself? Conversely
                  have you ever heard about a particular web site, wanted to read it, and found that
                  the person who told you about it did not give you enough information to actually find
                  it?
               </p>
               
               <p>&nbsp;</p>
               
               <p>&nbsp;</p>
               
               
               
               
               <br>
               
               <p class="nav2"><span id="navOpenCloseBottom"></span><a href="whycite2.html#">return to top</a> | <a href="whycite.html">previous page</a> | <a href="whycite3.html">next page</a></p>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/whycite/whycite2.pcf">©</a>
      </div>
   </body>
</html>