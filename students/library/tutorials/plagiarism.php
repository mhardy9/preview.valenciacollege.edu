<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/tutorials/plagiarism.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/tutorials/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/tutorials/">Tutorials</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Plagiarism</h2>
                        
                        
                        <h3>Plagiarism Tutorial: Brief  (Self-Paced)</h3>
                        
                        <ul>
                           
                           <li><a href="http://eastlrc.valenciacollege.edu/tutorials/Plagiarism_Tutorial/">Brief Plagiarism Tutorial</a></li>
                           
                        </ul>
                        
                        <h3>Plagiarism Tutorial:  Comprehensive (Self-Paced)</h3>
                        
                        <p>These tutorials can only be viewed in Internet Explorer or Firefox.</p>
                        
                        <p>On MLA Style</p>
                        
                        <ul>
                           
                           <li>
                              <a href="whatisplagiarism/index.html" target="_blank" title="What is Plagiarism?">What is Plagiarism?</a> (takes about 10 minutes)
                           </li>
                           
                           <li>
                              <a href="whycite/index.html" target="_blank" title="Why Cite?">Why Cite?</a> (takes about 5 minutes)
                           </li>
                           
                           <li>
                              <a href="fullcitations/index.html" target="_blank" title="Full Citations">Full Citations</a> (takes about 40 minutes)
                           </li>
                           
                           <li>
                              <a href="inspeechcitations/index.html" target="_blank" title="In-Speech Citations">In-Speech Citations</a> (takes about 45 minutes)
                           </li>
                           
                           <li>
                              <a href="aptips/index.html" target="_blank" title="Avoiding Plagiarism">Avoiding Plagiarism</a> (takes about 5 minutes)
                           </li>
                           
                        </ul>
                        
                        
                        
                        <p>On APA Style</p>
                        
                        <ul> 
                           
                           <li><a href="apa-whatisplagiarism/index.html" target="_blank" title="What is Plagiarism?">What is Plagiarism?</a></li>
                           
                           <li><a href="apa-whycite/index.html" target="_blank" title="Why Cite?">Why Cite?</a></li>
                           
                           <li><a href="apa-fullcitations/index.html" target="_blank" title="Full Citations">Full Citations</a></li>
                           
                           <li><a href="intextcitations/index.html" target="_blank" title="In-Speech Citations">In-Text Citations</a></li>
                           
                           <li><a href="apa-aptips/index.html" target="_blank" title="Avoiding Plagiarism">Avoiding Plagiarism</a></li>
                           
                        </ul>
                        
                        <div>
                           
                           
                           <h3>Troubleshooting for Self-Paced Tutorials</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>These  tutorials work with Internet Explorer and Mozilla Firefox. They cannot save
                                    your place, so please take note of the approximate completion time below and  either
                                    work on it when you have time to finish or keep track of where you were.
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Internet  Explorer may display an error message on the top of the screen: <br>
                                       "To  help protect your security, Internet Explorer has restricted this webpage from
                                       running scripts or ActiveX controls that could access your computer. Click here  for
                                       options." 
                                    </li>
                                    
                                    <li>If you  see this message, complete the following steps to make sure you can complete
                                       the quizzes and activities and successfully submit your scores: 
                                    </li>
                                    
                                    <li>Click  where the browser says "click here for options." </li>
                                    
                                    <li>A short  menu will appear. Click on Allow Blocked Content. </li>
                                    
                                    <li>A popup  will appear with the following message: "Allowing active content such as
                                       script and ActiveX controls can be useful. However, active content might also  harm
                                       your computer. Are you sure you want to let this file run active  content?" 
                                    </li>
                                    
                                    <li>Click  Yes. </li>
                                    
                                    <li>If you  encounter other problems, please contact a librarian at your campus. </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        <h3>MLA The Easy Way (Video) </h3>
                        
                        <p>This series of  five videos, with audio narration by Dr. Phillips, explain and show
                           students  how to use the library MLA &amp; APA Style Guides to cite various types of 
                           sources.
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://youtu.be/wBs-P5HKjAU">MLA Sample Paper</a> (5:48)
                           </li>
                           
                           <li>
                              <a href="http://youtu.be/3CWkD3B0mqA">Documenting Internet Sources</a> (6:10)
                           </li>
                           
                           <li>
                              <a href="http://youtu.be/hFNdGx86hJs">Documenting Sources from  Valencia Databases</a> (5:17)
                           </li>
                           
                           <li>
                              <a href="http://youtu.be/_yle8IybC8U">In-Text Citations</a> (5:27)
                           </li>
                           
                           <li>
                              <a href="http://youtu.be/2WhUzqOPKeg">Finalizing a Works Cited Page</a> (2:51)
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/tutorials/plagiarism.pcf">©</a>
      </div>
   </body>
</html>