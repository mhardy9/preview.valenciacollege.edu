<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <div>  
                           
                           <div>
                              
                              <div> <img alt=" " height="8" src="arrow.gif" width="8"> 
                                 <img alt=" " height="8" src="arrow.gif" width="8">
                                 
                                 
                              </div>
                              <br>
                              
                              
                              <div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <ul>
                                          
                                          <li><a href="campus/east-library.html" title="">
                                                <p>EAST CAMPUS</p></a></li>
                                          
                                          <li><a href="campus/osceola-library.html" title="">
                                                <p>OSCEOLA CAMPUS</p></a></li>
                                          
                                          <li><a href="campus/poinciana-library.html" title="">
                                                <p>POINCIANA CAMPUS</p></a></li>
                                          
                                          <li><a href="campus/west-library.html" title="">
                                                <p>WEST CAMPUS</p></a></li>
                                          
                                          <li><a href="campus/winter-park-library.html" title="">
                                                <p>WINTER PARK CAMPUS</p></a></li>
                                          
                                          <li><a href="campus/lake-nona-library.html" title="">
                                                <p>LAKE NONA CAMPUS</p></a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                    <link href="../includes/jquery/css/slider.css" rel="stylesheet" type="text/css">
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <a name="navigate" id="navigate"></a>
                                       
                                       
                                       <h2>Help with Research</h2>
                                       <a href="http://askalibrarian.org/valencia" target="_blank">Ask a Librarian</a>
                                       <a href="http://libguides.valenciacollege.edu/databasesandarticles" target="_blank">Databases &amp; Articles</a>
                                       <a href="http://libguides.valenciacollege.edu/" target="_blank">Research Guides</a>
                                       <a href="mla-apa-chicago-guides/index.html">MLA, APA &amp; Chicago Citations</a>
                                       <a href="http://net4.valenciacollege.edu/forms/library/contact.cfm" target="_blank">Contact the Library</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <a name="content" id="content"></a>
                                       
                                       
                                       <h2>Search the Library</h2>
                                       
                                       
                                       
                                       <form action="http://www.linccweb.org/Discover" method="get">
                                          <input name="lib_code" type="hidden" value="FLCC2900">
                                          <input aria-label="SearchLibrary" name="query" placeholder="Enter search term." type="text">
                                          <input type="submit" value="Search">
                                          
                                       </form>
                                       
                                       
                                       
                                       <div>For <strong>full access</strong>, log in: <br>
                                          
                                          <ul>
                                             
                                             <li>Through <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> on the  Courses tab, <strong>or</strong>
                                                
                                             </li>
                                             
                                             <li>With <a href="http://discover.linccweb.org/primo_library/libweb/action/login.do?loginFn=signin&amp;vid=FLCC2900&amp;targetURL=http%3a%2f%2fdiscover.linccweb.org%2fprimo_library%2flibweb%2faction%2fsearch.do%3fdscnt%3d0%26amp%3bdstmp%3d1450359938912%26amp%3bvid%3dFLCC2900%26amp%3binitializeIndex%3dtrue" target="_blank">Borrower  ID and PIN</a><br>
                                                <a href="ccla-login-instructions.html">What  are my Borrower ID and PIN?</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                          <p><a href="ccla-login-benefits.html">Why log  in? Benefits of full access</a></p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       
                                       <h2>At Your Library</h2>
                                       
                                       <a href="about/hours.html">Locations &amp; Hours</a>
                                       
                                       <a href="services/internet-use.html">Use Computers &amp; Technology</a>
                                       
                                       <a href="services/how-to-borrow.html">How to Borrow / Renew Items</a>
                                       
                                       <a href="services/index.html">More Library Services</a>
                                       
                                       <a href="faq.html">Library FAQ</a>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>          
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        <div>
                           
                           
                           
                           
                           
                        </div> 
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/index.pcf">©</a>
      </div>
   </body>
</html>