<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/west/cal/guidelines_student.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/west/cal/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/west/">West</a></li>
               <li><a href="/students/library/west/cal/">Cal</a></li>
               <li>West Campus Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h3>Student Services </h3>
                        
                        <p><span>The Computer Access Lab is equipped with close to 100 computers (Windows and Mac)
                              for students to use to complete assignments, projects, and take tests. The Computer
                              Access Lab is open seven days a week (during Spring and Fall), most days until 10:00pm.
                              <br>
                              Our staff is ready to assist you with your software or hardware questions.</span><br>
                           
                        </p>
                        
                        <h3><img alt="Students" height="130" src="Picture3_000.jpg.png" width="481"></h3>
                        
                        <h3>Services</h3>
                        
                        <ul>
                           
                           <li>Free, one-on-one workshops on Word, Excel, PowerPoint, Photoshop, MovieMaker, Blackboard,
                              and  more.
                           </li>
                           
                        </ul>
                        
                        
                        <ul>
                           
                           <li>              Knowledgeable staff available to assist you with your projects, movie/photo
                              editing and  other questions regarding our software.
                           </li>
                           
                           <li>              Access the Internet and Microsoft Office Suite on any of our computers.</li>
                           
                           <li><strong>Click <a href="http://libguides.valenciacollege.edu/downloadingebooks">here</a> to learn how to download eBooks to your computer, smartphone or tablet.</strong></li>
                           
                        </ul>
                        
                        <h3>Hardware</h3>
                        
                        <ul>
                           
                           <li>Windows and Macintosh computers</li>
                           
                           <li>              Laser Printers: Color and B&amp;W (single and double-sided)</li>
                           
                           <li>              Scanners</li>
                           
                           <li>Copy Machines</li>
                           
                           <li>Microphones</li>
                           
                           <li>Webcams</li>
                           
                           <li>              Camcorders</li>
                           
                           <li> Digital Cameras</li>
                           
                           <li>              Graphing Tablets</li>
                           
                           <li>BluRay players</li>
                           
                           <li>VHS players</li>
                           
                        </ul>
                        
                        <h3>SPACE - Student Production Lab</h3>
                        
                        <ul>
                           
                           <li>Collaboration and group study multimedia cubicles: produce and edit videos and sound
                              files.
                           </li>
                           
                        </ul>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <aside>
                           
                           
                           
                           <h2>Location, Contact, &amp; Hours</h2>
                           
                           
                           <div> 
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>
                                             West Campus Computer Access Lab 
                                             </strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Building 6 (Library), First Floor </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1646</div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7:30 am - 10:00 pm<br>Friday: 7:30 am - 12:00 pm<br>Saturday - Sunday: Closed<br>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                        </aside>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/west/cal/guidelines_student.pcf">©</a>
      </div>
   </body>
</html>