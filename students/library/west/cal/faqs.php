<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/west/cal/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/west/cal/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/west/">West</a></li>
               <li><a href="/students/library/west/cal/">Cal</a></li>
               <li>West Campus Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Frequently Asked Questions </h2>            
                        
                        
                        
                        <ol>
                           
                           <li> 
                              
                              <p><a href="#01">Can anyone use the lab?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#05">Can I access the Internet?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#06">Do you have wireless connectivity for my 
                                    device?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#07">Can I use the lab to do personal work?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#08">Can I access my Blackboard courses?</a></p>
                              
                           </li>
                           
                           <li>
                              
                              <p><a href="#16">How do I get help with using Software?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#09">Can I download and listen to music?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#10">Do you have headphones?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#11">Is color printing available?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#12">Where can I get a copy card?</a></p>
                              
                           </li>
                           
                           <li> 
                              
                              <p><a href="#15">Where can I save my work?</a></p>
                              
                           </li>
                           
                           <li>                      <a href="#dvdcd">Can I burn a CD or a DVD?</a> 
                           </li>
                           
                        </ol>
                        
                        
                        
                        
                        
                        
                        
                        
                        <h3> Can anyone use the lab? </h3>
                        
                        <blockquote> 
                           
                           <p> Any Valencia student with an active Atlas account or any currently enrolled UCF Valencia
                              West  or UCF Valencia Osceola Campus student (using NID and PID) may use the lab.
                              You may log in at any  computer with your Atlas user name and password. 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Can I access the Internet? </h3>
                        
                        <blockquote> 
                           
                           <p>Yes, all Computer Access Lab computers have internet access.  Internet Explorer is
                              our default browser for Windows and Safari for Macintosh computers. We 
                              also have Mozilla Firefox and Google Chrome on all computers. 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Do you have wireless connectivity for my device? 
                           
                        </h3>
                        
                        <blockquote> 
                           
                           <p>Valencia <em>does </em> have a wireless network that you can 
                              connect to using your Atlas credentials or if you are a currently enrolled UCF Valencia
                              West  or UCF Valencia Osceola Campus student you may contact a Library Staff member
                              for login information. Look for the "Wi-Fi" signs for stronger signal. 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Can I use the lab to do personal work? </h3>
                        
                        <blockquote> 
                           
                           <p>Yes, however, between the hours of 10am - 2pm Monday - Friday, only course-relates
                              work/study on lab computers. Personal work may be done anytime outside of these hours.
                              
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3> Can I access my Blackboard courses?</h3>
                        
                        <blockquote> 
                           
                           <p> Yes, you can access these courses by going to the <strong>Quick Links</strong> tab on the Valencia College home page and clicking on <strong>Online Courses</strong>. You may also access these courses by logging into your Atlas account and click the
                              link under the <strong>My Courses</strong> tab.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>How do I get help with using Software?            </h3>
                        
                        <blockquote>
                           
                           <p>For help with all supported software, talk to our knowledgeable staff for assistance.</p>
                           
                        </blockquote>
                        
                        <h3><a name="09" id="09"></a></h3>
                        
                        <h3> Can I download and listen to music?</h3>
                        
                        <blockquote> 
                           
                           <p>Most music is copyrighted and downloading it would be a violation 
                              of US copyright law. In order to protect students, we do not permit 
                              music downloading. You are free to listen to any music you bring 
                              with you provided  you use headphones. 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Do you have headphones?</h3>
                        
                        <blockquote> 
                           
                           <p> We have many items   available for check-out. These items include headphones, microphones,
                              cameras, camcorders, webcams, and graphics tablets. Just ask if 
                              you're looking for something in particular.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Is color printing available?</h3>
                        
                        <blockquote> 
                           
                           <p>We have a laser color printer available for student use. While 
                              the cost of black and white printing is only 10 cents per page/surface, color printing
                              
                              cost 25 cents per page/surface.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Where can I get a copy card?</h3>
                        
                        <blockquote> 
                           
                           <p>Copy card machines are located on the first and second floors 
                              of the library. You will need a $1 bill in order to purchase a new 
                              card. The  card will retain  $1 value until you use it. There is no need to purchase
                              multiple cards; you can 
                              add value to any card by simply inserting the card first and then 
                              inserting money. The copy card machine does NOT accept change. 
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Where can I save my work?</h3>
                        
                        <blockquote> 
                           
                           <p>We recommend that you 
                              purchase a USB drive  to save your work. Current Valencia students may also upload
                              files to the Live@edu SkyDrive. See lab staff for details.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Can I burn a CD or a DVD?? </h3>
                        
                        <blockquote>
                           
                           <p>All computers within the Computer Access Lab are equipped with DVD and CD burners.
                              Our computers are also equiped with software such as Roxio or Nero for burning media.
                              The only thing you need to provide is a  blank DVD or a blank CD. 
                           </p>
                           
                           
                        </blockquote>          
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <aside>
                           
                           
                           
                           <h2>Location, Contact, &amp; Hours</h2>
                           
                           
                           <div> 
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>
                                             West Campus Computer Access Lab 
                                             </strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Building 6 (Library), First Floor </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1646</div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7:30 am - 10:00 pm<br>Friday: 7:30 am - 12:00 pm<br>Saturday - Sunday: Closed<br>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                        </aside>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/west/cal/faqs.pcf">©</a>
      </div>
   </body>
</html>