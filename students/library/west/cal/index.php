<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/west/cal/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/west/cal/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/west/">West</a></li>
               <li>Cal</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>What is the Computer Access Lab?</h2>
                        
                        <p><a href="documents/CalBrochure2016updated11-14-2016.pdf" target="_blank">Download our brochure.</a> 
                        </p>
                        
                        <p><strong>What's New? Click the CAL Student Services link and look under Services.</strong></p>
                        
                        <p>The Computer Access Lab is available to any Valencia student with an active atlas
                           account. Currently enrolled UCF Valencia West and/or UCF Valencia Osceola Campus students
                           may also use the lab.
                        </p>
                        
                        <p><strong>Notice</strong>: 
                        </p>
                        
                        <ul>
                           
                           <li>All food and drinks are prohibited in the Computer Access Lab and in room 6-118. </li>
                           
                           <li>Taking phone calls and talking on the phone are not permitted. Phone calls may be
                              taken in the lobby of the building. 
                           </li>
                           
                           <li>Children are not allowed in the Computer Access Lab nor in room 6-118.</li>
                           
                        </ul>            
                        <p>Lab staff are available to assist even the most novice computer user.</p>            
                        
                        
                        <p>Please see here for policies on <a href="../../services/internet-use.html">Internet Use</a>.
                        </p>
                        
                        <p>The Computer Access  Lab offers a wide variety of software that can help you 
                           complete your assignments. A full listing of available software can 
                           be found in our <a href="documents/Software-Catalog-Student-Version-05-11-17.pdf" target="_blank">Software Catalog</a>. 
                        </p>
                        
                        <p>
                           We also have a variety of hardware available for student use, including 
                           two color scanners (for course related work only), DVD/CD burners, 
                           pen tablets,  digital cameras and  digital camcorders.
                        </p>
                        
                        <p>
                           The lab has three black and white laser printers (10¢ per page), 
                           as well as one color laser printer (25¢ per page). All printing requires 
                           a copy card, which can be purchased on the first and second floors of the Library.
                        </p>
                        
                        <p>
                           We offer both  Windows and Macintosh computers. Students are responsible 
                           for bringing their own storage media to save their 
                           files. It is 
                           suggested that students use a USB portable storage device or CD-R/CD-RW.
                        </p>
                        
                        <p>
                           The  Lab is  accessible to all students. 
                           We have  wheelchair accessible workstations, trackball mice, 
                           and software to assist low vision and hearing impaired students. (JAWS,  Kurzweil,
                           and MAGIc). 
                        </p>
                        
                        <p>
                           To use the lab students must sign in at the computer they wish to use by entering
                           their Atlas user name and password.  Eligible UCF students may sign in using their
                           PID and NID.
                        </p>
                        
                        <h2>What is the SPACE (<span>S</span>tudent <span>P</span>roduction <span>A</span>nd <span>C</span>ollaborative <span>E</span>ducation)?
                        </h2>
                        
                        <p>The SPACE is a section of the Computer Access Lab dedicated to developing group projects
                           &amp; presentations.
                        </p>
                        
                        <p>Students may use this area to work alone or in groups to practice speeches, produce
                           and edit video or create and edit sound files.
                        </p>
                        
                        <p>In addition to the  software and most of the equipment that is available in the Computer
                           Access Lab, SPACE has sound and video editing software (Sound Forge and Vegas Pro).
                           Second Life is also available on all of our computers for students who may need it
                           for their class.
                        </p>
                        
                        <h2>Need Atlas help? </h2>
                        
                        <p><a href="../../../about/support/howto/index.html">Access a range of tutorials that will help you navigate your Atlas account more effectively.</a><br>
                           
                        </p>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <aside>
                           
                           
                           
                           <h2>Location, Contact, &amp; Hours</h2>
                           
                           
                           <div> 
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>
                                             West Campus Computer Access Lab 
                                             </strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Building 6 (Library), First Floor </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1646</div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7:30 am - 10:00 pm<br>Friday: 7:30 am - 12:00 pm<br>Saturday - Sunday: Closed<br>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                        </aside>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/west/cal/index.pcf">©</a>
      </div>
   </body>
</html>