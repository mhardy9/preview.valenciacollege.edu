<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/west/cal/guidelines_faculty.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/west/cal/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>West Campus Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/west/">West</a></li>
               <li><a href="/students/library/west/cal/">Cal</a></li>
               <li>West Campus Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        <h2>Faculty Services </h2>
                        
                        <p>The Computer Access Lab staff invites all Valencia faculty to 
                           visit the lab. Come see what we have to offer you and your students. 
                           Please feel free to <em><a href="mailto:kknauth@valenciacollege.edu">contact us</a></em> if you 
                           have any comments or concerns regarding software workshops we offer, 
                           course specific software you would like us to purchase for your 
                           students to use, or even hardware you would like to have available 
                           for your students. We value your input and look forward to working 
                           with you. Please review our guidelines for faculty below.
                        </p>
                        
                        
                        <h3>SCHEDULING A CLASS</h3>
                        
                        <ul>
                           
                           <li> An instructor may schedule a class to use one of the lab classrooms, 
                              but may not schedule it for an entire semester. To schedule a 
                              class the instructor should e-mail at least a week in advance to make 
                              a reservation.
                           </li>
                           
                           <li> Instructor will inform the lab regarding software, hardware 
                              and any peripheral needs for the class.
                           </li>
                           
                           <li>The Instructor should accompany the class to the lab and remain 
                              in the lab during the class.
                           </li>
                           
                           <li>West Campus credit classes will be given first priority in scheduling 
                              the classroom.
                           </li>
                           
                        </ul>
                        
                        <h3>CLASS ASSIGNMENTS MADE TO STUDENTS
                           
                        </h3>
                        
                        <ul>
                           
                           <li>To better assist students, an Instructor should notify the lab 
                              when he/she makes an assignment that will require students to 
                              use the lab.
                           </li>
                           
                           <li>The following information will be most helpful to lab staff: 
                              the instructor's name, the course number, the software to be used, 
                              and the date the assignment is due.
                           </li>
                           
                           <li> It will also be very beneficial if the instructor notifies 
                              the lab manager of any problems that students might have in the 
                              lab, so that such problems may be remedied.
                           </li>
                           
                        </ul>
                        
                        <h3>ADHERENCE TO COPYRIGHT LAW</h3>
                        
                        <ul>
                           
                           <li>Faculty members using the Computer Access Lab will observe the terms 
                              of agreement for each software package used. Copies may be made for use by classes
                              
                              only as provided for in the terms of agreement for the software 
                              in question.
                           </li>
                           
                           <li>Any faculty member who wishes to use software not owned by the 
                              Computer Access Lab must provide the lab with written documentation stating 
                              that the software may be used and copied for teaching purposes.
                           </li>
                           
                        </ul>
                        
                        
                        <p>We look forward to working with you.
                           
                           
                        </p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <aside>
                           
                           
                           
                           <h2>Location, Contact, &amp; Hours</h2>
                           
                           
                           <div> 
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              
                              <div data-old-tag="table">
                                 
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>
                                             West Campus Computer Access Lab 
                                             </strong></div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Building 6 (Library), First Floor </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">407-582-1646</div>
                                       
                                    </div>
                                    
                                    
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td">Monday - Thursday: 7:30 am - 10:00 pm<br>Friday: 7:30 am - 12:00 pm<br>Saturday - Sunday: Closed<br>
                                          <br>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                        </aside>
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/west/cal/guidelines_faculty.pcf">©</a>
      </div>
   </body>
</html>