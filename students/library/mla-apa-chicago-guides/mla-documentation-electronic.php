<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/mla-apa-chicago-guides/mla-documentation-electronic.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/mla-apa-chicago-guides/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/mla-apa-chicago-guides/">Mla Apa Chicago Guides</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        
                        <h2> MLA Style for Electronic Sources</h2>
                        
                        
                        
                        
                        <p><a href="default.html">MLA, APA &amp; Chicago Style Guides</a> - <a href="mlacitation.html">MLA Citation</a> - Electronic Sources 
                        </p>
                        
                        <hr>
                        
                        <h3>Works Cited List </h3>
                        
                        <p>The color-coded guides below adhere to formatting rules from the <em>MLA Handbook for Writers of Research Papers,<strong> 8th ed.</strong></em></p>
                        
                        <p>For help with MLA 7th edition citation, see our <a href="http://libguides.valenciacollege.edu/mla7" target="_blank">MLA 7th Edition Citation Guide</a>. 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Library Databases</div>
                                 
                                 <div data-old-tag="th"> Web Sites and Other Electronic Sources</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <h2>A</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Academic-One-File-MLA.pdf">Academic One File</a></li>
                                       
                                       <li><a href="documents/Academic-Search-Complete-MLA.pdf">Academic Search Complete</a></li>
                                       
                                       <li><a href="documents/Access-Engineering-MLA.pdf">Access Engineering</a></li>
                                       
                                       <li><a href="documents/ACLS-Humanities-Ebook-MLA.pdf">ACLS Humanities Ebook</a></li>
                                       
                                       <li><a href="documents/African-American-Experience-MLA.pdf">African American Experience</a></li>
                                       
                                       <li><a href="documents/American-Government-MLA.pdf">American Government</a></li>
                                       
                                       <li><a href="documents/American-History-MLA.pdf">American History</a></li>
                                       
                                       <li><a href="documents/American-Indian-Experience-MLA.pdf">American Indian Experience</a></li>
                                       
                                       <li><a href="documents/Americas-News-MLA.pdf">America's News</a></li>
                                       
                                       <li><a href="documents/Anatomy-TV-MLA.pdf">Anatomy.TV</a></li>
                                       
                                       <li><a href="documents/Applied-Science-and-Technology-Source-MLA.pdf">Applied Science and Technology Source</a></li>
                                       
                                       <li><a href="documents/Art-Source-MLA.pdf">Art Source</a></li>
                                       
                                       <li><a href="documents/Artemis-Literary-Sources-MLA.pdf">Artemis Literary Sources</a></li>
                                       
                                       <li>
                                          <a href="documents/Article-on-a-Website-MLA.pdf"></a><a href="documents/ArtStor-MLA.pdf">ArtStor</a>
                                          
                                       </li>
                                       
                                       <li><a href="documents/Associates-Program-Source-Plus-MLA.pdf">Associates Program Source Plus</a></li>
                                       
                                    </ul>
                                    
                                    <h2>B</h2>
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="documents/Bible-Verse-MLA.pdf"></a><a href="documents/Biography-in-Context-MLA.pdf">Biography in Context</a>
                                          
                                       </li>
                                       
                                       <li><a href="documents/Biography-Reference-Bank.pdf">Biography Reference Bank</a></li>
                                       
                                       <li><a href="documents/Books-and-Authors-MLA.pdf">Books and Authors</a></li>
                                       
                                       <li><a href="documents/Brittanica-Academic-MLA.pdf">Brittanica Academic</a></li>
                                       
                                       <li><a href="documents/Business-Collection-MLA.pdf">Business Collection</a></li>
                                       
                                       <li><a href="documents/Business-Economics-and-Theory-Collection-MLA.pdf">Business Economics and Theory Collection</a></li>
                                       
                                       <li><a href="documents/Business-Insights-MLA.pdf">Business Insights</a></li>
                                       
                                       <li><a href="documents/Business-Source-Complete-MLA.pdf">Business Source Complete</a></li>
                                       
                                    </ul>
                                    
                                    <h2>C                  </h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Chronicle-of-Higher-Education-MLA.pdf">Chronicle of Higher Education</a></li>
                                       
                                       <li><a href="documents/CINAHL-MLA.pdf">CINAHL</a></li>
                                       
                                       <li><a href="documents/Cochrane-Library-MLA.pdf">Cochrane Library</a></li>
                                       
                                       <li><a href="documents/Computer-Database-MLA.pdf">Computer Database</a></li>
                                       
                                       <li><a href="documents/Consumer-Reports-MLA.pdf">Consumer Reports</a></li>
                                       
                                       <li><a href="documents/CQ-Researcher-MLA.pdf">CQ Researcher</a></li>
                                       
                                       <li><a href="documents/Criminal-Justice-Database-MLA.pdf">Criminal Justice Database</a></li>
                                       
                                       <li><a href="documents/Culinary-Arts-Collection-MLA.pdf">Culinary Arts Collection</a></li>
                                       
                                    </ul>
                                    
                                    <h2>D</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Daily-Life-Through-History-MLA.pdf">Daily Life Through History</a></li>
                                       
                                       <li><a href="documents/DemographicsNow-MLA.pdf">DemographicsNow</a></li>
                                       
                                       <li><a href="documents/Dictionary-of-Literary-Biography-MLA.pdf">Dictionary of Literary Biography</a></li>
                                       
                                       <li><a href="documents/DSM-V-MLA.pdf">DSM V</a></li>
                                       
                                    </ul>
                                    
                                    <h2>E</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Ebook-Central-MLA.pdf">ebook Central (ProQuest)</a></li>
                                       
                                       <li><a href="documents/eBook-ABC-Clio-MLA.pdf">eBook Collection (ABC CLIO)</a></li>
                                       
                                       <li><a href="documents/eBook-Collection-EBSCO-MLA.pdf">eBook Collection (EBSCO)</a></li>
                                       
                                       <li><a href="documents/eBook-Salem-MLA.pdf">eBook Collection (Salem Press)</a></li>
                                       
                                       <li><a href="documents/Education-Source-MLA.pdf">Education Source</a></li>
                                       
                                       <li><a href="documents/Educators-Reference-Complete-MLA.pdf">Educators Reference Complete</a></li>
                                       
                                       <li><a href="documents/Engineering-Case-Studies-MLA.pdf">Engineering Case Studies</a></li>
                                       
                                       <li><a href="documents/ERIC-EBSCO-MLA.pdf">ERIC via EBSCO</a></li>
                                       
                                       <li><a href="documents/Eric-via-FirstSearch-MLA.pdf">Eric via FirstSearch</a></li>
                                       
                                       <li><a href="documents/European-Views-of-the-Americas-MLA.pdf">European Views of the Americas</a></li>
                                       
                                       <li><a href="documents/Expanded-Academic-ASAP-MLA.pdf">Expanded Academic ASAP</a></li>
                                       
                                    </ul>
                                    
                                    <h2>F</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Fergusons-Career-Guidance-Center-MLA.pdf">Fergusons Career Guidance Center</a></li>
                                       
                                       <li><a href="documents/Film-and-Television-Literature-Index-MLA.pdf">Film and Television Literature Index</a></li>
                                       
                                       <li><a href="documents/Films-on-Demand-MLA.pdf">Films on Demand</a></li>
                                       
                                       <li><a href="documents/Florida-Newspapers-MLA.pdf">Florida Newspapers</a></li>
                                       
                                       <li><a href="documents/Funk-and-Wagnalls-MLA.pdf">Funk and Wagnalls</a></li>
                                       
                                    </ul>
                                    
                                    <h2>G</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/General-One-File-MLA.pdf">General One File</a></li>
                                       
                                       <li><a href="documents/General-Reference-Center-Gold-MLA.pdf">General Reference Center Gold</a></li>
                                       
                                       <li><a href="documents/Global-Issues-in-Context-MLA.pdf">Global Issues in Context</a></li>
                                       
                                       <li><a href="documents/Global-Road-Warrior-MLA.pdf">Global Road Warrior</a></li>
                                       
                                       <li><a href="documents/Grangers-World-of-Poetry-MLA.pdf">Grangers World of Poetry</a></li>
                                       
                                       <li><a href="documents/GreenFile-MLA.pdf">GreenFile</a></li>
                                       
                                    </ul>
                                    
                                    <h2>H</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Health-and-Wellness-Resource-Center-MLA.pdf">Health and Wellness Resource Center</a></li>
                                       
                                       <li><a href="documents/Health-Reference-Center-Academic-MLA.pdf">Health Reference Center Academic</a></li>
                                       
                                       <li><a href="documents/Health-Source-Consumer-Edition-MLA.pdf">Health Source Consumer Edition</a></li>
                                       
                                       <li><a href="documents/Health-Source-Nursing-Academic-Edition-MLA.pdf">Health Source Nursing Academic Edition</a></li>
                                       
                                       <li><a href="documents/History-Reference-Center-MLA.pdf">History Reference Center</a></li>
                                       
                                       <li><a href="documents/Hoovers-Premium-MLA.pdf">Hoovers Premium</a></li>
                                       
                                       <li><a href="documents/Hospitality-and-Tourism-Complete-MLA.pdf">Hospitality and Tourism Complete</a></li>
                                       
                                       <li><a href="documents/Hospitality-MLA.pdf">Hospitality, Tourism and Leisure</a></li>
                                       
                                       <li><a href="documents/Humanities-Source-MLA.pdf">Humanities Source</a></li>
                                       
                                    </ul>
                                    
                                    <h2>I</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Index-to-Legal-Periodicals-and-Books-MLA.pdf">Index to Legal Periodicals and Books</a></li>
                                       
                                       <li><a href="documents/Informe-MLA.pdf">Informe</a></li>
                                       
                                       <li><a href="documents/Infotrac-Communication-and-Mass-Media-Complete-MLA.pdf">Infotrac Communication and Mass Media Complete</a></li>
                                       
                                       <li><a href="documents/Infotrac-Criminal-Justice-MLA.pdf">Infotrac Criminal Justice</a></li>
                                       
                                       <li><a href="documents/Infotrac-Professional-MLA.pdf">Infotrac Professional</a></li>
                                       
                                       <li><a href="documents/InfoTrac-Psychology-Collection-MLA.pdf">InfoTrac Psychology Collection</a></li>
                                       
                                       <li><a href="documents/Infotrac-Research-in-Context-MLA.pdf">Infotrac Research in Context</a></li>
                                       
                                       <li><a href="documents/Infotrac-Student-MLA.pdf">Infotrac Student</a></li>
                                       
                                       <li><a href="documents/Issues-and-Controversies-MLA.pdf">Issues and Controversies</a></li>
                                       
                                       <li><a href="documents/Issues-Understanding-Controversy-and-Society-MLA.pdf">Issues Understanding Controversy and Society</a></li>
                                       
                                    </ul>
                                    
                                    <h2>J</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/JSTOR-MLA.pdf">JSTOR</a></li>
                                       
                                    </ul>
                                    
                                    <h2>K</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Kanopy-Streaming-Video.pdf">Kanopy Streaming Video</a></li>
                                       
                                    </ul>
                                    
                                    <h2>L</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Latino-American-Experience-MLA.pdf">Latino American Experience</a></li>
                                       
                                       <li><a href="documents/LexisNexis-MLA.pdf">LexisNexis</a></li>
                                       
                                       <li><a href="documents/Library-Information-Science-and-Technology-Abstract-with-Full-Text-MLA.pdf">Library Information Science &amp; Technology Abstracts with Full-Text</a></li>
                                       
                                       <li><a href="documents/Library-Information-Science-and-Technology-MLA.pdf">Library Information Science and Technology</a></li>
                                       
                                       <li><a href="documents/Library-Literature-and-Information-Science-Full-Text-MLA.pdf">Library Literature and Information Science Full Text</a></li>
                                       
                                       <li><a href="documents/Literary-Reference-Center-Plus-MLA.pdf">Literary Reference Center Plus</a></li>
                                       
                                       <li><a href="documents/Literature-Criticism-Online-MLA.pdf">Literature Criticism Online</a></li>
                                       
                                       <li><a href="documents/LitFinder-MLA.pdf">LitFinder</a></li>
                                       
                                    </ul>
                                    
                                    <h2>M</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/MAS-Ultra-School-Edition-MLA.pdf">MAS Ultra School Edition</a></li>
                                       
                                       <li><a href="documents/MasterFILE-Complete-MLA.pdf">MasterFILE Complete</a></li>
                                       
                                       <li><a href="documents/MEDLINE-With-FullText-MLA.pdf">MEDLINE With FullText</a></li>
                                       
                                       <li><a href="documents/Military-and-Intelligence-MLA.pdf">Military and Intelligence</a></li>
                                       
                                       <li>Military and Government Collection </li>
                                       
                                    </ul>
                                    
                                    <h2>N</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/New-York-Times-Gale-MLA.pdf">New York Times (Gale)</a></li>
                                       
                                       <li><a href="documents/NewsBank-MLA.pdf">NewsBank</a></li>
                                       
                                       <li><a href="documents/Newsstand-MLA.pdf">Newsstand</a></li>
                                       
                                       <li><a href="documents/Nursing-and-Allied-Health-Collection-MLA.pdf">Nursing and Allied Health Collection</a></li>
                                       
                                    </ul>
                                    
                                    <h2>O</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Opposing-Viewpoints-MLA.pdf">Opposing Viewpoints</a></li>
                                       
                                       <li><a href="documents/Orlando-Sentinel-MLA.pdf">Orlando Sentinel</a></li>
                                       
                                       <li><a href="documents/Ovid-eBooks.pdf">Ovid eBooks</a></li>
                                       
                                       <li><a href="documents/Oxford-Art-Online-MLA.pdf">Oxford Art Online</a></li>
                                       
                                       <li><a href="documents/Oxford-English-Dictionary-MLA.pdf">Oxford English Dictionary</a></li>
                                       
                                       <li><a href="documents/Oxford-Reference-MLA.pdf">Oxford Reference</a></li>
                                       
                                    </ul>
                                    
                                    <h2>P</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Points-of-View-Reference-Center-MLA.pdf">Points of View Reference Center</a></li>
                                       
                                       <li><a href="documents/Pop-Culture-Universe-MLA.pdf">Pop Culture Universe</a></li>
                                       
                                       <li><a href="documents/Popular-Magazines-MLA.pdf">Popular Magazines</a></li>
                                       
                                       <li><a href="documents/Primary-Search-MLA.pdf">Primary Search</a></li>
                                       
                                       <li><a href="documents/ProQuest-Historical-Newspapers-The-New-York-Times-MLA.pdf">ProQuest Historical Newspapers: The New York Times</a></li>
                                       
                                       <li><a href="documents/ProQuest-MLA.pdf">ProQuest Databases</a></li>
                                       
                                       <li><a href="documents/PsycArticles-MLA.pdf">PsycArticles</a></li>
                                       
                                    </ul>
                                    
                                    <h2>R</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Regional-Busines-News-MLA.pdf">Regional Busines News</a></li>
                                       
                                    </ul>
                                    
                                    <h2>S</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Sage-Journals-MLA.pdf">Sage Journals</a></li>
                                       
                                       <li><a href="documents/Science-Direct-MLA.pdf">Science Direct</a></li>
                                       
                                       <li><a href="documents/Science-Online-MLA.pdf">Science Online</a></li>
                                       
                                       <li><a href="documents/Slavery-in-America-and-the-World-MLA.pdf">Slavery in America and the World</a></li>
                                       
                                       <li><a href="documents/Small-Business-Resource-Center-MLA.pdf">Small Business Resource Center</a></li>
                                       
                                       <li><a href="documents/Sources-in-US-History-MLA.pdf">Sources in U.S. History</a></li>
                                       
                                       <li><a href="documents/Springer-EJournal-Collection.pdf">Springer eJournal Collection</a></li>
                                       
                                       <li><a href="documents/Swank-Motion-Pictures.pdf">Swank Motion Pictures</a></li>
                                       
                                    </ul>
                                    
                                    <h2>T</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Taylor-and-Francis-eBooks-MLA.pdf">Taylor and Francis eBooks</a></li>
                                       
                                       <li><a href="documents/Teacher-Reference-Center-MLA.pdf">Teacher Reference Center</a></li>
                                       
                                    </ul>
                                    
                                    <h2>V</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Virtual-Reference-Library-MLA.pdf">Virtual Reference Library</a></li>
                                       
                                    </ul>
                                    
                                    <h2>W</h2>
                                    
                                    <ul>
                                       
                                       <li><a href="documents/World-at-War-MLA.pdf">World at War</a></li>
                                       
                                       <li><a href="documents/World-Book-Online-MLA.pdf">World Book Online</a></li>
                                       
                                       <li><a href="documents/World-Folklore-and-Folklife-MLA.pdf">World Folklore and Folklife</a></li>
                                       
                                       <li><a href="documents/World-Geography-MLA.pdf">World Geography</a></li>
                                       
                                       <li><a href="documents/World-History-Ancient-MLA.pdf">World History: Ancient</a></li>
                                       
                                       <li><a href="documents/World-History-Modern-MLA.pdf">World History: Modern</a></li>
                                       
                                       <li><a href="documents/World-Religions-MLA.pdf">World Religions</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="documents/Article-on-a-Website-MLA.pdf">Article on a Website</a></li>
                                       
                                       <li><a href="documents/Website-Corporate-Group-Author-MLA.pdf">Article on a Website with a Corporate or Group Author</a></li>
                                       
                                       <li><a href="documents/Audio-Broadcast-MLA.pdf">Audio Broadcast</a></li>
                                       
                                       <li><a href="documents/Entire-Website-MLA.pdf">Entire Website </a></li>
                                       
                                       <li><a href="documents/Online-Magazine-Newspaper-Journal-MLA.pdf">Online Magazine, Newspaper or Journal Article </a></li>
                                       
                                       <li><a href="documents/Online-Book-Translations-MLA.pdf">Online Book Translations</a></li>
                                       
                                       <li><a href="documents/Bible-Verse-MLA.pdf">Bible Verse</a></li>
                                       
                                       <li><a href="documents/Blog-MLA.pdf">Blog</a></li>
                                       
                                       <li><a href="documents/Book-Download-Device-MLA.pdf">Book Downloaded to a Device</a></li>
                                       
                                       <li><a href="documents/Book-on-the-Web-MLA.pdf">Book on the Web</a></li>
                                       
                                       <li><a href="documents/Chapter-Book-Online-MLA.pdf">Chapter in a Book on the Web </a></li>
                                       
                                       <li><a href="documents/Email-MLA.pdf">Email</a></li>
                                       
                                       <li><a href="documents/Map-MLA.pdf">Map</a></li>
                                       
                                       <li><a href="documents/Professors-Syllabus-MLA.pdf">Professor's Syllabus</a></li>
                                       
                                       <li><a href="documents/R2-Digital-Library.pdf">R2 Digital Library</a></li>
                                       
                                       <li><a href="documents/SlideShare-Presentation-MLA.pdf">SlideShare Presentation</a></li>
                                       
                                       <li><a href="documents/Twitter-Facebook-MLA.pdf">Twitter or Facebook</a></li>
                                       
                                       <li><a href="documents/Video-Internet-MLA.pdf">Video on the Web </a></li>
                                       
                                       <li><a href="documents/YouTube-Video-MLA.pdf">YouTube Video</a></li>
                                       
                                    </ul>                  
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <hr>
                        
                        <h4>
                           <a name="intext" id="intext"></a>In-Text Citations 
                        </h4>
                        
                        <p><strong>Parenthetical Documentation or Citing Sources in the Text: </strong>Use the following examples as a guide for referencing sources in the body of your
                           paper. 
                        </p>
                        
                        <blockquote>
                           
                           <p><strong>Site with one author: </strong><br>
                              "LifeMap is a guide to help you figure out your career and educational goals" (Jones).
                              
                           </p>
                           
                           <p><strong>Site with two or three authors: </strong><br>
                              The LRC has many electronic resources (Smith, Adams, and Williams). 
                           </p>
                           
                           <p><strong>Site with more than three authors: </strong><br>
                              "Online courses provide a way for students to use their time wisely" (Kilby et al.).
                              
                           </p>
                           
                           <p><strong>Site with no author; use first two words of title: </strong><br>
                              Valencia has a vital workforce development program ("More Companies"). 
                           </p>
                           
                           <p><strong>Site with a corporate author: </strong><br>
                              "Valencia is a better place to start" (Valencia College). 
                           </p>
                           
                           <p><strong>Site which numbers paragraphs: </strong><br>
                              <em>Academic Search Premier</em> is an extremely versatile database (Byrnes, pars. 5-6). 
                           </p>
                           
                           <p><strong>Site for an article in pdf format which includes accurate page numbers:</strong><br>
                              "An understanding of international politics is essential in today's world" (Crawford
                              55). 
                           </p>
                           
                           
                        </blockquote>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/mla-apa-chicago-guides/mla-documentation-electronic.pcf">©</a>
      </div>
   </body>
</html>