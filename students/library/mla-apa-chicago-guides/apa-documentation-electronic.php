<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/mla-apa-chicago-guides/apa-documentation-electronic.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/mla-apa-chicago-guides/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/mla-apa-chicago-guides/">Mla Apa Chicago Guides</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        
                        <h2>APA Style for Electronic Sources</h2>
                        
                        
                        
                        
                        <p> <a href="default.html">MLA, APA &amp; Chicago Style Guides</a> - <a href="apacitation.html">APA Citation</a> - Electronic Sources 
                        </p>
                        
                        <hr>
                        
                        <h3>References List </h3>
                        
                        <p>The color-coded guides below adhere to the guidelines from the <em>Publication Manual of the American Psychological Association, 6th ed.</em></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>Library Databases </div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>Web Sites and Other Electronic Sources</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>A</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AcademicOneFIle-APA6.pdf">Academic One File</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AcademicSearchCompleteAPA6.pdf">Academic Search Complete</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AccessEngineeringAPA6.pdf">Access Engineering </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/AfricanAmericanExperienceAPA6.pdf">African American Experience</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AmericanGovernmentAPA6.pdf">American Government</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/AmericanHistoryAPA6.pdf">American History</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AmericanIndianExperienceAPA6.pdf">American Indian Experience</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AmericasNewspapersAPA6revised.pdf">Americas Newspapers</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/AppliedScienceandTechnologyFullTextAPA6.pdf">Applied Science and Technology Full Text</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ArtSourceAPA.pdf">Art Source</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ARTstorAPA6.pdf">ARTstor</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ArtemisLiterarySourcesAPA.pdf">Artemis Literary Sources</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>B</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BiographyinContext.pdf">Biography in Context</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BusinessEconomicsTheoryCollectionAPA6.pdf">Business Economics Theory Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BusinessInsightsAPA.pdf">Business Insights</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BusinessSourcesCompleteAPA6.pdf">Business Source Complete</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>C</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/CAMIOAPA6.pdf">CAMIO</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ChronicleofHigherEducationAPA6.pdf">Chronicle of Higher Education </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/CINAHLAPA6.docx.pdf">CINAHL Plus Full Text</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/CochraneLibraryAPA6.pdf">Cochrane Library</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ComputerDatabaseAPA6.pdf">Computer Database</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/CQResearcherPlusArchiveAPA6.pdf">CQ Researcher Plus Archive</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/CriminalJusticePortalsAPA6.pdf">Criminal Justice</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/CulinaryArtsCollectionAPA6.pdf">Culinary Arts Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>D</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/DailyLifeThroughHistoryAPA6.pdf">Daily Life Through History </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/DictionaryofLiteraryBiographyAPA6.pdf">Dictionary of Literary Biography</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>E</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/eBookCollectionABC-CLIOAPA.pdf">eBook Collection (ABC-CLIO)</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/eBookCollectionEBSCOAPA.pdf">eBook Collection (EBSCO)</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/eBookCollectionSalemAPA.pdf">eBook Collection (Salem)</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ebraryAPA6.pdf">ebrary</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/EducationFullTextAPA6.pdf">Education Full Text</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/EducatorsReferenceCompleteAPA6.pdf">Educator's Reference Complete</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ERICviaEBSCOAPA6.pdf">ERIC via EBSCO</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ExpandedAcademicASAPAPA6.pdf"> Expanded Academic ASAP</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>F</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/FergusonsCareerGuidanceCenterAPA6.pdf">Ferguson's Career Guidance Center</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/FilmsOnDemandAPA6.pdf">Films On Demand</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/FilmTelevisionLiteratureIndexwithFullTextAPA6.pdf">Film &amp; Television Literature Index with Full Text </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/FloridasNewspapersAPA6.pdf">Florida's Newspapers </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/FunkWagnallsAPA6.pdf">Funk &amp; Wagnall's Encyclopedia</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>G</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GeneralBusinessFileASAPAPA6.pdf">General Business File ASAP</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GeneralOneFileAPA6.pdf">General OneFILE</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GeneralReferenceCenterGoldAPA6.pdf">General Reference Center Gold</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GeneralScienceFullTextAPA6.pdf">General Science Full Text</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GlobalIssuesInContextAPA6.pdf">Global Issues In Context</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GlobalRoadWarriorAPA6.pdf">Global Road Warrior </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GreenfileCitationAPA6.pdf">GreenFILE</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>H</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/HealthNursingAPA6.pdf">Health &amp; Nursing</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/HealthWellnessResourceCenterAPA6.pdf.html">Health &amp; Wellness Resource Center</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/HealthReferenceCenterAcademicAPA6.pdf">Health Reference Center Academic</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/HealthSourceConsumerEditionAPA6.pdf">Health Source: Consumer Edition</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/HealthSourceNursingAcademicEditionAPA6.pdf">Health Source: Nursing Academic</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/HistoryReferenceCenterAPA6.pdf">History Reference Center</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/HooversAPA6.pdf">Hoover's Premium</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/HospitalityandTourismCompleteAPA6.pdf">Hospitality &amp; Tourism Complete </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>I</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InformeAPA6.pdf">Informe</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfoTracCommunicationsAPA6.pdf">Infotrac Communications</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfoTracCriminalJusticAPA6.pdf">InfoTrac Criminal Justice</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfotracTourismAPA6.pdf">Infotrac Hospitality, Tourism, &amp; Leisure Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfoTracKidsAPA6.pdf">Infotrac Kids</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfotracProfessionalCollectionAPA6.pdf">Infotrac Professional Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfoTracPsychologyAPA6.pdf">Infotrac Psychology</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfotracStudentEditionAPA6.pdf">Infotrac Student Edition</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/InfotracTourismAPA6.pdf">Infotrac Tourism</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/IntelecomAPA6.pdf">Intelecom</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/IssuesControversiesAPA6.pdf">Issues &amp; Controversies</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/IssuesUnderstandingControversyandSocietyAPA6.pdf">Issues: Understanding Controversy &amp; Society</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>J</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/JSTORAPA6.pdf">JSTOR</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>L</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/LatinoAmericanExperienceAPA6.pdf">Latino American Experience</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/LegalPeriodicalsFullTextAPA6.pdf">Legal Periodicals Full Text</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/Lexis-Nexis-Academic-APA6.pdf">LexisNexis Academic </a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/LibraryInformationScienceandTechnologyAbstractswithFulltextAPA6.pdf">Library Information Science &amp; Technology Abstracts with Full Text </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/LibraryLiteratureAPA6.pdf">Library Literature &amp; Information Science&nbsp; Full Text</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                
                                                <p><a href="documents/LiteraryReferenceCenterAPA6.pdf">Literary Reference Center</a></p>
                                                
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/LiteratureCriticismOnlineAPA6.pdf">Literature Criticism Online </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/LitFinderAPA6.pdf.html">LitFinder</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>M</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MASUltraSchoolEditionAPA6.pdf">MAS Ultra School Edition</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/MasterFILEPremierAPA6.pdf">MasterFILE Premier</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MedlinewithFullTextAPA6.pdf">MEDLINE with Full Text (EBSCO)</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MilitaryandGovernmentCollectionAPA6.pdf">Military &amp; Government Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>N</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/NewYorkTimesAPA6Revised.pdf">New York Times</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/NewYorkTimesHistoricalAPA6Revised.pdf">New York Times Historical</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/NewsbankAPA6revised.pdf">Newsbank</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/NewsstandAPA6.pdf">Newsstand</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/NursingandAlliedHealthCollectionAPA6.pdf">Nursing &amp; Allied Health Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>O</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/OpposingViewpointsinContextAPA6.pdf">Opposing Viewpoints in Context</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/OrlandoSentinelAPA6Revised.pdf">Orlando Sentinel</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/OvidEBooksAPA6.pdf">Ovid eBooks</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/OxfordArtOnlineAPA6.pdf">Oxford Art Online</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/OxfordEnglishDictionaryOnlineAPA6.pdf">Oxford English Dictionary</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/OxfordReferenceOnlinePremiumAPA6.pdf">Oxford Reference Online Premium</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>P</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/PointsofViewReferenceCenterAPA6.pdf">Points of View Reference Center</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/PopCultureUniverseAPA6.pdf">Pop Culture Universe</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/PopularMagazinesAPA6.pdf">Popular Magazines</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/PrimarySearchAPA6.pdf">Primary Search</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ProQuestAPA6.pdf">ProQuest</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/PsycArticlesAPA6.pdf">PsycArticles</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>R</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/RegionalBusinessNewsAPA6.pdf">Regional Business News</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>S</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SageJournalsAPA6.pdf">Sage Journals</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SmallBusinessResourceCenterAPA6.pdf">Small Business Resource Center</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/SocialStudiesAPA.pdf">Social Studies</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SourcesinU.S.History.pdf">Sources in U.S. History</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/SpringerEJournalCollectionAPA6.pdf">Springer E-Journal Collection</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>T</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/TeacherReferenceCenter-APA6.pdf">Teacher Reference Center</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>V</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/VirtualReferenceLibraryAPA6.pdf">Virtual Reference Library</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><strong>W</strong></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WorldatWarAPA6.pdf">World at War</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WorldFolkloreandFolklifeAPA6.pdf">World Folklore and Folklife</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WorldGeographyAPA6.pdf">World Geography</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WorldHistoryAncientMedievalErasAPA6.pdf">World History Ancient &amp; Medieval Eras</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WorldHistoryModernEraAPA6.pdf">World History Modern Era</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WorldReligionsAPA6.pdf">World Religions </a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="td">
                                    
                                    
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BlogAPA6.pdf">Blog</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/GoogleBookAPA6.pdf">Google Book</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/PersonalCommunicationAPA6.pdf">Interviews and Other Personal Communications</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ForumsMailingListsAPA6.docx">Online Forums &amp; Electronic Mailing Lists</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/OnlineNewspaperMagazineArticlesAPA6.pdf">Online Newspaper &amp; Magazine Articles</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/TwitterFacebookAPA.pdf">Twitter and Facebook </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/YouTubeVideoAPA6.pdf">YouTube Video</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/VideoAPA6.pdf">Video/Film (Other)</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/WebSitesAPA6.pdf">Web Sites &amp; Web Articles</a></div>
                                             
                                          </div>
                                          
                                          
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/mla-apa-chicago-guides/apa-documentation-electronic.pcf">©</a>
      </div>
   </body>
</html>