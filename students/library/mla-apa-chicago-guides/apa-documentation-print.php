<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/mla-apa-chicago-guides/apa-documentation-print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/mla-apa-chicago-guides/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/mla-apa-chicago-guides/">Mla Apa Chicago Guides</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h3>
                           
                        </h3>
                        
                        
                        
                        
                        
                        
                        
                        <h2>APA Style for Print and Other Sources</h2>
                        
                        
                        
                        
                        <p> <a href="default.html">MLA, APA &amp; Chicago Style Guides</a> - <a href="apacitation.html">APA Citation</a> - Print and Other Sources 
                        </p> 
                        
                        <hr>
                        
                        <h3>References List </h3>
                        
                        <p> The color-coded guides below adhere to the guidelines from the <em>Publication Manual of the American Psychological Association, 6th ed. </em></p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">Books</div>
                                 
                                 <div data-old-tag="th">Periodical Articles </div>
                                 
                                 <div data-old-tag="th">Other Non-Electronic Sources </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BookbyOneAuthorAPA6.pdf">Book by one author</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BookbyTwoAuthorsAPA6.pdf">Book by two author</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BookbyanEntityAPA6.pdf">Book by an Entity (Government, Association, Agency or Company</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/BookbyanEditorAPA6.pdf">Book by an Editor</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ArticlefromEditedCollectionAPA6.pdf">Article from an edited collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/ReferenceBookAPA6.pdf">Article from a reference book </a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SignedOneDailyNewspaperAPA6.pdf">Signed article, one author, from a daily newspaper </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SignedTwoAuthorDailyNewspaperAPA6.pdf">Signed article, two authors, from a daily newspaper</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SignedThreeAuthorMonthlyAPA6.pdf">Signed article, three to six author, from a monthly magazine</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SignedEightAuthorAPA6.pdf">Signed article, eight or more authors with continuous pagination</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/UnsignedDailyAPA6.pdf">Unsigned article from a daily newspaper </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/SignedOneQuarterlyAPA6.pdf">Signed article, one author, from a quarterly journal using continuous pagination</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/PamphletBrochureBulletinAPA6.pdf">Pamphlets, Brochures, &amp; Bulletins</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/PersonalCommunications.pdf">Personal Communication</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/VideoAPA6.pdf">Video / Film </a></div>
                                             
                                          </div>
                                          
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/mla-apa-chicago-guides/apa-documentation-print.pcf">©</a>
      </div>
   </body>
</html>