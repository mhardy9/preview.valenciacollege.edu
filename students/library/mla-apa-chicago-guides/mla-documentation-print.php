<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/mla-apa-chicago-guides/mla-documentation-print.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/mla-apa-chicago-guides/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/mla-apa-chicago-guides/">Mla Apa Chicago Guides</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        
                        <h2>MLA Style for Print and Other Sources </h2>
                        
                        
                        <p><a href="default.html">MLA, APA &amp; Chicago Style Guides</a> - <a href="mlacitation.html">MLA Citation</a> - Print and Other Sources
                        </p>
                        
                        <hr>
                        
                        <h3>Works Cited List </h3>
                        
                        <p>The color-coded guides below adhere to formatting rules from the <em>MLA Handbook for Writers of Research Papers, <strong>8th ed</strong></em>.
                        </p>
                        
                        <p>For help with MLA 7th edition citation, see our <a href="http://libguides.valenciacollege.edu/mla7" target="_blank">MLA 7th Edition Citation Guide</a>. 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <div>Books</div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>Print Periodicals </div>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <div>Other Sources </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-by-One-Author.pdf" target="_blank">Book by One Author</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-by-a-Corporate-Author.pdf" target="_blank">Book by a Corporate Author</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td">
                                                <a href="documents/MLA-Print-Book-by-Two-Authors.pdf" target="_blank">Book by Two Authors</a> 
                                             </div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-by-Three-Authors.pdf" target="_blank">Book by Three Authors</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-no-author-only-editor.pdf" target="_blank">Book with No Author, with Editor</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-with-an-author-and-editor.pdf" target="_blank">Book with an Author and Editor</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-Published-Before-1900.pdf" target="_blank">Book Published Before 1900</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-with-Multiple-Publishers.pdf" target="_blank">Book with Multiple Publishers</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-Graphic.pdf" target="_blank">Graphic Narrative or Illustrated Book </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-book-series.pdf" target="_blank">Book in a Series</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/Bookmultivolumework.pdf" target="_blank">Book in a Multivolume Work</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-book-foreign.pdf" target="_blank">Book in a Language Other Than English </a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Article-Reprinted-in-a-Book.pdf" target="_blank">Article Reprinted in a Book</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Selection-in-an-Anthology.pdf" target="_blank">One Work in an Anthology or Edited Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-More-than-One-Work-Anthology.pdf" target="_blank">More Than One Work in an Anthology or Edited Collection</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Reference-Book.pdf" target="_blank">Entry in a Reference Book</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Book-Preface.pdf" target="_blank">Preface to a Book</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Article-in-a-Newspaper.pdf" target="_blank">Article in a Newspaper</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Article-in-a-Scholarly-Periodical.pdf" target="_blank">Article in a Scholarly Journal</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Anonymous-Article.pdf" target="_blank">Anonymous Article</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Signed-Editorial-in-a-newspaper.pdf" target="_blank">Signed Editorial in a Newspaper</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div data-old-tag="table">
                                       
                                       <div data-old-tag="tbody">
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-DVD-Video.pdf" target="_blank">DVD / Video</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Government-Publication.pdf" target="_blank">Government Publication</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Interview.pdf" target="_blank">Interview</a></div>
                                             
                                          </div>
                                          
                                          <div data-old-tag="tr">
                                             
                                             <div data-old-tag="td"><a href="documents/MLA-Print-Lecture.pdf" target="_blank">Lecture</a></div>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/mla-apa-chicago-guides/mla-documentation-print.pcf">©</a>
      </div>
   </body>
</html>