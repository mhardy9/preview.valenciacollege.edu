<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/mla-apa-chicago-guides/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/mla-apa-chicago-guides/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/mla-apa-chicago-guides/">Mla Apa Chicago Guides</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        
                        <h2>MLA, APA, and Chicago Guides</h2>
                        
                        
                        <h3>Use the links in the table below to browse through guides to different citation formats:</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">
                                    <p><a href="apacitation.html">American Psychological Association</a></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p><a href="mlacitation.html">Modern Language Association</a></p>
                                 </div>
                                 
                                 <div data-old-tag="th">
                                    <p><a href="chicagocitation.html">Chicago Manual of Style</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="apa-documentation-electronic.html">APA Citation of Electronic Sources</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="mla-documentation-electronic.html">MLA 8 Citation of Electronic Sources</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="chicago-documentation-electronic.html">Chicago Citation of Electronic Sources</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="apa-documentation-print.html">APA Citation of Print Sources</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="mla-documentation-print.html">MLA 8 Citation of Print Sources</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="chicago-documentation-print.html">Chicago Citation of Print Sources</a></p>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><a href="https://owl.english.purdue.edu/owl/resource/560/18/" target="_blank">APA Sample Paper</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="https://style.mla.org/sample-papers/" target="_blank">MLA 8 Sample Papers</a></p>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <p><a href="https://owl.english.purdue.edu/media/pdf/1300991022_717.pdf" target="_blank">Chicago Sample Paper</a></p>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Why Cite Sources?</h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Avoid plagiarism.</strong> By citing, you let your reader know what words and ideas are yours and what was borrowed
                              from someone else.
                           </li>
                           
                           <li>
                              <strong>Provide a way for the reader to retrieve the sources you used. </strong>Your reader may be fascinated by the material and wish to read further, or your reader
                              may question your use of the material and wish to check up on you. Your accurate documentation
                              of the material will protect you and allow the reader to see if you have correctly
                              interpreted the original source.
                           </li>
                           
                           <li>
                              <strong> Establish your reputation as a competent and credible researcher and writer. </strong>Your reader will see that you have used information from experts and authorities on
                              your topic. 
                           </li>
                           
                           <li>
                              <strong>Show how your work relates to a scholarly conversation.</strong> Citing the previous work of others may show the reader how knowledge of the topic
                              has developed among experts and authorities over time, and can help provide context
                              for your contribution to that conversation. 
                           </li>
                           
                        </ul>
                        
                        <h3>What Should Be Cited? </h3>
                        
                        <ul>
                           
                           <li>
                              <strong>Any information you quote exactly. </strong>Use quotation marks around the text quoted. Keep direct quotations to a minimum. Only
                              quote text expressed in such a unique way that it would lose impact if you did not
                              do so.
                           </li>
                           
                           <li>
                              <strong>Any information you summarize or paraphrase</strong>. State the ideas entirely in your own words, using neither the words nor the sentence
                              structure of the original. You do not need to include quotation marks, but you must
                              still include a citation. Use paraphrasing or summarizing for the majority of your
                              cited information. 
                           </li>
                           
                        </ul>
                        
                        <h3>What Should Not Be Cited?</h3>
                        
                        <ul>
                           
                           <li>
                              <span>Your own opinions or unique ideas.</span> Any conclusions that you draw are your own. If others are impressed with your opinions
                              and ideas, they will need to cite you as a source!
                           </li>
                           
                           <li>
                              <strong>Information that is common knowledge</strong>. If information is commonly known to be true it does not need to be cited, even if
                              you found the information in a source. Examples include historical dates and facts
                              (such as birth and death dates, dates of historical events, the fact that George Washington
                              was the first President of the United States). If you are not certain whether something
                              is common knowledge, cite it.
                           </li>
                           
                        </ul>
                        
                        <h3>How Can I Learn More About Avoiding Plagiarism?</h3>
                        
                        <ul>
                           
                           <li>Check out our <a href="../tutorials/plagiarism.html">Plagiarism Tutorials</a>! 
                           </li>
                           
                           <li>
                              <a href="http://net4.valenciacollege.edu/forms/library/contact.cfm">Contact a librarian</a> for more help. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/mla-apa-chicago-guides/default.pcf">©</a>
      </div>
   </body>
</html>