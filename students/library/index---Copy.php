<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library - Valencia College | Valencia College</title>
      <meta name="Description" content="A multi-campus college dedicated to the premise that educational opportunities are necessary to bring together the diverse forces in society"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/index---Copy.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/library/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li>Library - Valencia College</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div id="containerHeaderAlert">
                  
                  
                  
                  
                  
                  
                  
                  
               </div>
               
               
               <div id="containerTop">
                  
                  
                  <header><a name="top" id="top"></a>
                     
                     <div id="accessibility">
                        <a href="index.html#navigate" class="trkHeader">Skip to Local Navigation</a> | <a href="index.html#content" class="trkHeader">Skip to Content</a>
                        
                     </div>
                     
                     <div id="outContainerTopBar">
                        
                        <div id="topBar">
                           
                           <div id="logoandlogin">
                              
                              <div id="logo">
                                 
                                 
                                 <a href="../index.html" class="trkHeader" aria-label="Valencia College"><img src="https://valenciacollege.edu/images/logos/valencia-college-logo.svg" alt="Valencia College" width="275" height="42" role="img"></a>
                                 
                                 
                              </div>
                              
                              <div class="AtlasLoginContainer"> 
                                 <a href="https://atlas.valenciacollege.edu/" title="Atlas Login" style="text-decoration:none;" class="trkHeader"><span class="AtlasIcon"></span> <span class="AtlasText"><span class="kern-2">A</span>T<span class="kern-1">LAS</span>&nbsp;<span class="kern-2">L</span><span class="kern-1">OGIN</span></span></a>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        <div id="navContainer" role="navigation" aria-label="Sitewide Navigation">
                           
                           <div id="nav">
                              
                              <ul class="navMenu">
                                 
                                 <li class="navLinks"><a href="http://preview.valenciacollege.edu/future-students/" id="nav-future" class="trkHeader">Future Students</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../students/index.html" id="nav-current" class="trkHeader">Current Students</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../faculty.html" id="nav-faculty" class="trkHeader">Faculty &amp; Staff</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="navLinks"><a href="../visitors.html" id="nav-visitors" class="trkHeader">Visitors &amp; Friends</a></li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="blank">
                                    
                                    <div class="util">
                                       
                                       <ul class="x_level1">
                                          
                                          <li class="x_quicklinks">
                                             <a href="../includes/quicklinks.html" style="text-decoration:none;" class="trkHeader">Quick Links</a>
                                             
                                             <ul class="x_level2">
                                                
                                                <li class="x_quicklinks2"><a href="../calendar/index.html" class="trkHeader">Academic Calendar</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../students/admissions-records/index.html" class="trkHeader">Admissions</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://net4.valenciacollege.edu/promos/internal/blackboard.cfm" class="trkHeader">Blackboard Learn</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://www.valenciabookstores.com/valencc/" class="trkHeader">Bookstore</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../business-office/index.html" class="trkHeader">Business Office</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../calendar/index.html" class="trkHeader">Calendars</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../map/index.html" class="trkHeader">Campuses</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../catalog/index.html" class="trkHeader">Catalog</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/schedule/" class="trkHeader">Class Schedule</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/continuing-education/" class="trkHeader">Continuing Education</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../programs/index.html" class="trkHeader">Degree &amp; Career Programs</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../departments/index.html" class="trkHeader">Departments</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/economicdevelopment/" class="trkHeader">Economic Development</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../human-resources/index.html" class="trkHeader">Employment</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../finaid/index.html" class="trkHeader">Financial Aid Services</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../international/index.html" class="trkHeader">International</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="index.html" class="trkHeader">Libraries</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../map/index.html" class="trkHeader">Locations</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../contact/directory.html" class="trkHeader">Phone Directory</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../students/admissions-records/index.html" class="trkHeader">Records/Transcripts</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../security/index.html" class="trkHeader">Security/Parking</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="../support/index.html" class="trkHeader">Support</a></li>
                                                
                                                <li class="x_quicklinks2"><a href="http://valenciacollege.edu/events/" class="trkHeader">Valencia Events</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </li>
                                 
                                 <li class="spacer">
                                    
                                 </li>
                                 <li class="blank">
                                    
                                    <div class="util">
                                       
                                       <div class="x_search" id="Search">
                                          
                                          <form label="search" class="relative" action="https://search.valenciacollege.edu/search" name="seek" id="seek" method="get">
                                             <label for="q"><span style="display:none;">Search</span></label>
                                             <input type="text" name="q" id="q" alt="Search" value="" aria-label="Search">
                                             <button style="position:absolute; top:0; right:12;" type="submit" name="submitMe" value="seek" onmouseover="clearTextSubmit(document.seek.q)" onfocus="clearTextSubmit(document.seek.q)" onmouseout="clearTextSubmitOut(document.seek.q)" onblur="clearTextSubmitOut(document.seek.q)">go</button>
                                             <input type="hidden" name="site" value="default_collection">
                                             <input type="hidden" name="client" value="default_frontend">
                                             <input type="hidden" name="proxystylesheet" value="default_frontend">
                                             <input type="hidden" name="output" value="xml_no_dtd">
                                             
                                          </form>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </li>
                                 
                              </ul>
                              
                              
                              
                           </div>
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </header>
                  
               </div>
               
               <div id="containerMain">
                  
                  
                  <div id="containerContent" role="main">
                     
                     <div id="wrapper">
                        
                        <div id="crumb">
                           <a href="../default.html">Home</a> <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=" "> 
                           <a href="../students/index.html">Students</a> <img src="http://valenciacollege.edu/images/arrow.gif" width="8" height="8" alt=" ">
                           <a href="index.html">Library</a>
                           
                        </div>
                        <br>
                        
                        
                        <div class="container_12 row" id="content">
                           
                           
                           
                           <div class="grid_3">
                              
                              <div id="tabs">
                                 
                                 <ul>
                                    
                                    <li><a href="campus/east-library.html" title="">
                                          <p>EAST CAMPUS</p></a></li>
                                    
                                    <li><a href="campus/osceola-library.html" title="">
                                          <p>OSCEOLA CAMPUS</p></a></li>
                                    
                                    <li><a href="campus/poinciana-library.html" title="">
                                          <p>POINCIANA CAMPUS</p></a></li>
                                    
                                    <li><a href="campus/west-library.html" title="">
                                          <p>WEST CAMPUS</p></a></li>
                                    
                                    <li><a href="campus/winter-park-library.html" title="">
                                          <p>WINTER PARK CAMPUS</p></a></li>
                                    
                                    <li><a href="campus/lake-nona-library.html" title="">
                                          <p>LAKE NONA CAMPUS</p></a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="grid_9">
                              
                              
                              <link rel="stylesheet" type="text/css" href="../includes/jquery/css/slider.css">
                              
                              
                              
                              
                              
                              
                              <div id="slideshow-library-home">
                                 
                                 
                                 
                                 <div id="mainImage-library-home"> 
                                    
                                    <div>
                                       <a href="http://valencia.digital.flvc.org/cfm" target="_blank"><img src="http://valenciacollege.edu/library/images/photo-slider-library-digital-730x246.jpg" width="730" height="245" alt="Digital Library" border="0"></a>
                                       
                                    </div>
                                    
                                    <div style="display:none;">
                                       <img src="http://valenciacollege.edu/library/images/photo-slider-east-library-night-730x246.jpg" width="730" height="245" alt="Library East Campus" border="0">
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;">
                                       <img src="http://valenciacollege.edu/library/images/photo-slider-osceola-night-730x246.jpg" width="730" height="245" alt="Library Osceola Campus" border="0">
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;">
                                       <img src="http://valenciacollege.edu/library/images/photo-slider-west-inside-730x246.jpg" width="730" height="245" alt="Library West Campus" border="0">
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;">
                                       <img src="http://valenciacollege.edu/library/images/photo-slider-winter-park-campus-730x246.jpg" width="730" height="245" alt="Library Winter Park Campus" border="0">
                                       
                                    </div>
                                    
                                    
                                    <div style="display:none;">
                                       <img src="http://valenciacollege.edu/library/images/photo-slider-lake-nona-aerial-730x246.jpg" width="730" height="245" alt="Library Lake Nona Campus" border="0">
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           
                           <div class="clear">&nbsp;</div> 
                           
                           <h1 style="margin: 0 0 0 15px;">Library</h1>
                           
                           
                           <div class="grid_12">&nbsp;</div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:265px;">
                                 
                                 <a name="navigate" id="navigate"></a>
                                 
                                 
                                 <h2 style="margin: 0;">Help with Research</h2>
                                 <a href="http://askalibrarian.org/valencia" class="button color2 narrow" target="_blank">Ask a Librarian</a>
                                 <a href="http://libguides.valenciacollege.edu/databasesandarticles" class="button color2 narrow" target="_blank">Databases &amp; Articles</a>
                                 <a href="http://libguides.valenciacollege.edu/" class="button color2 narrow" target="_blank">Research Guides</a>
                                 <a href="mla-apa-chicago-guides/index.html" class="button color2 narrow">MLA, APA &amp; Chicago Citations</a>
                                 <a href="http://net4.valenciacollege.edu/forms/library/contact.cfm" target="_blank" class="button color2 narrow">Contact the Library</a>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:265px;">
                                 
                                 <a name="content" id="content"></a>
                                 
                                 
                                 <h2 style="margin: 0;">Search the Library</h2>
                                 
                                 
                                 
                                 <form action="http://www.linccweb.org/Discover" method="get">
                                    <input type="hidden" name="lib_code" value="FLCC2900">
                                    <input type="text" name="query" placeholder="Enter search term." aria-label="SearchLibrary">
                                    <input type="submit" value="Search" id="go" class="button color1 inline">
                                    
                                 </form>
                                 
                                 
                                 
                                 <div style="margin-top:-15px;">For <strong>full access</strong>, log in: <br>
                                    
                                    <ul>
                                       
                                       <li>Through <a href="https://atlas.valenciacollege.edu/" target="_blank">Atlas</a> on the  Courses tab, <strong>or</strong>
                                          
                                       </li>
                                       
                                       <li>With <a href="http://discover.linccweb.org/primo_library/libweb/action/login.do?loginFn=signin&amp;vid=FLCC2900&amp;targetURL=http%3a%2f%2fdiscover.linccweb.org%2fprimo_library%2flibweb%2faction%2fsearch.do%3fdscnt%3d0%26amp%3bdstmp%3d1450359938912%26amp%3bvid%3dFLCC2900%26amp%3binitializeIndex%3dtrue" target="_blank">Borrower  ID and PIN</a><br>
                                          <a href="ccla-login-instructions.html" class="colorbox">What  are my Borrower ID and PIN?</a>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><a href="ccla-login-benefits.html" class="colorbox">Why log  in? Benefits of full access</a></p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <div class="grid_4">
                              
                              <div class="SPgreyBox" style="height:265px;">
                                 
                                 
                                 <h2 style="margin: 0;">At Your Library</h2>
                                 
                                 <a href="about/hours.html" class="button color2 narrow">Locations &amp; Hours</a>
                                 
                                 <a href="services/internet-use.html" class="button color2 narrow">Use Computers &amp; Technology</a>
                                 
                                 <a href="services/how-to-borrow.html" class="button color2 narrow">How to Borrow / Renew Items</a>
                                 
                                 <a href="services/index.html" class="button color2 narrow">More Library Services</a>
                                 
                                 <a href="faq.html" class="button color2 narrow">Library FAQ</a>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="grid_12">&nbsp;</div>
                           
                           <div class="clear">&nbsp;</div> 
                           
                           
                        </div>          
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <div id="containerBtm">
                     
                     
                     
                     
                     
                  </div> 
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/index---Copy.pcf">©</a>
      </div>
   </body>
</html>