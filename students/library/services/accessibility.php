<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/services/accessibility.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/services/">Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Accessibility</h2>
                        
                        <h3>Facility  Access </h3>
                        
                        <ul>
                           
                           <li> Automatic doors provide wheelchair  accessible entrances and exits</li>
                           
                           <li> Wheelchair accessible restrooms on all  floors </li>
                           
                           <li> Elevator</li>
                           
                           <li> TTY telephones available at the  Check-Out Desks</li>
                           
                        </ul> 
                        
                        <h3>Library Resources</h3>
                        
                        <p> Please ask for  assistance if any library resource is inaccessible. </p>
                        
                        <h3> Audio-Visual Materials</h3>
                        
                        <p> The library makes  every effort to acquire closed captioned materials where available.
                           
                        </p>
                        
                        <h3> Library Website</h3>
                        
                        <p> We are working hard  to make our website fully accessible. Please direct any comments
                           about the  accessibility of our website to <a href="mailto:rseguin@valenciacollege.edu">Regina Seguin</a>, librarian. 
                        </p>
                        
                        <h3> Library E-Resources</h3>
                        
                        <p> Valencia libraries  either subscribe to or purchase access to electronic resources,
                           including  online databases, electronic journals and e-books for curriculum support.
                           These  electronic resources are housed on the web sites of third-party vendors. 
                        </p>
                        
                        <p> Although the library  does not have direct control over changes in accessibility
                           of these web sites,  criteria for the selection of e-resources, as stated in the <a href="../documents/collectiondevelopment2012.pdf">Valencia College Library Collection Development Procedure</a>, include "Accessibility of electronic resources to students  with disabilities according
                           to the Section 508 standards of the US  Rehabilitation Act."
                        </p>
                        
                        <p><a href="../documents/Compliance_Statements.docx">Chart indicating which e-resource vendors have supplied a  Section 508 compliance
                              statement</a></p>
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>East</h3>
                              
                              <p> Located on the second floor of Building 4, the library currently  carries the following
                                 OSD-approved hardware:
                              </p>
                              
                              <ul>
                                 
                                 <li>4 Zoomtext keyboards</li>
                                 
                                 <li>7 computer stations with a single large 20”  monitor </li>
                                 
                                 <li>1 computer station with dual large 20” monitors</li>
                                 
                                 <li>2 trackballs </li>
                                 
                                 <li>SARA station (scans and reads)</li>
                                 
                                 <li>CCTV</li>
                                 
                              </ul>
                              
                              <p>The OSD software options provided for student use on all library  computers are:</p>
                              
                              <ul>
                                 
                                 <li>Jaws</li>
                                 
                                 <li>Magic</li>
                                 
                                 <li>Kurzweil 3000</li>
                                 
                                 <li>Dolphin Easy Reader</li>
                                 
                                 <li>Claroread Pro </li>
                                 
                              </ul>
                              
                              <p>If help is needed at any computer, copy or printing station,  please ask the Computer
                                 Desk Lab Assistant in the blue vest for  assistance.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Lake Nona</h3>
                              
                              <p>The Lake Nona Campus Library strives to ensure that  electronic resources and instructional
                                 materials are compliant with  accessibility standards. Students requesting accommodations
                                 are encouraged to  register with the <a href="../../students/office-for-students-with-disabilities/default.html">Office for Students with Disabilities</a>.  In addition, the Lake  Nona Campus Library provides assistive technology and software.
                                 All the  library computers have the following OSD software installed on their base
                                 image:
                              </p>
                              
                              <ul>
                                 
                                 <li>Jaws</li>
                                 
                                 <li>Magic</li>
                                 
                                 <li>Zoomtext</li>
                                 
                                 <li>Kurzweil</li>
                                 
                                 <li>Dolphin Easy Reader</li>
                                 
                                 <li>Adobe Reader</li>
                                 
                              </ul>
                              
                              <p>Assistive technology available in the Lake Nona Campus  library includes:</p>
                              
                              <ul>
                                 
                                 <li>SesnseView Duo portable magnifier</li>
                                 
                                 <li>Optelec   Clearview CCTV</li>
                                 
                                 <li>Onyx  Visuam Enhancement System</li>
                                 
                                 <li>Headphones            </li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Osceola</h3>
                              
                              <p>The Osceola Campus Library currently carries the following OSD-approved  hardware:</p>
                              
                              <ul>
                                 
                                 <li>SARA</li>
                                 
                                 <li>ClearView  Video Magnifier</li>
                                 
                              </ul>
                              
                              <p>All  computers in the Osceola Campus Library are equipped with the following OSD 
                                 software:
                              </p>
                              
                              <ul>
                                 
                                 <li>Jaws</li>
                                 
                                 <li>Magic</li>
                                 
                                 <li>Kurzweil  3000</li>
                                 
                                 <li>Dolphin  Easy Reader</li>
                                 
                                 <li>Dragon  Naturally Speaking</li>
                                 
                              </ul>
                              Please seek a library staff member for any  assistance using or locating OSD equipment
                              and software. 
                           </div>
                           
                           <div>
                              
                              <h3>West</h3>
                              
                              <h3> Computer Workstations &amp; Assistive  Technology</h3>
                              
                              <p> <a href="../documents/LibraryComputerLayouts.pdf">Map indicating the locations of all accessible workstations and  assistive technology
                                    within the library </a></p>
                              
                              <h3> E-mail Quick Check Stations </h3>
                              
                              <p> One wheelchair  accessible station, with adjustable height table and 21 inch monitor,
                                 on the  first floor (6-101) and one in the Library Loft located on the second floor.
                                 
                              </p>
                              
                              <h3> Print Stations</h3>
                              
                              <p> All print stations in  the library are wheelchair accessible. While we are arranging
                                 access to these  stations for students with visual impairments, please ask for assistance.
                                 
                              </p>
                              
                              <h3> OSD Resource Room - First Floor, Room 6-105</h3>
                              
                              <p> The OSD Resource  Center is a student study center as well as a good place for interpreters
                                 to  use. The room contains the following hardware and software:
                              </p>
                              
                              <ul>
                                 
                                 <li> Sorenson VP system </li>
                                 
                                 <li> Computer equipped with 
                                    
                                    <ul>
                                       
                                       <li> Text to speech programs: JAWS and Victor</li>
                                       
                                       <li> Speech to text program: Dragon Naturally Speaking</li>
                                       
                                       <li> DAISY Reader </li>
                                       
                                       <li> Headset</li>
                                       
                                       <li> Also runs XP SP2 operating system, Microsoft Office Suite, Media  Player 10 and Adobe
                                          Reader.
                                       </li>
                                       
                                       <li> New! Text to speech reader</li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <h3>Computer Access Lab - First Floor: Room  101A </h3>
                              
                              <p> All workstations in  the Computer Access Lab are equipped with the following assistive
                                 software: 
                              </p>
                              
                              <ul>
                                 
                                 <li> Kurzweil 3000 </li>
                                 
                                 <li> JAWS</li>
                                 
                                 <li> Magic</li>
                                 
                                 <li> Dolphin Easy Reader</li>
                                 
                                 <li> Dragon Naturally Speaking: SPACE station #3 only</li>
                                 
                              </ul>
                              
                              <p>The following  workstations in the Computer Access Lab are equipped with the assistive
                                 hardware: 
                              </p>
                              
                              <ul>
                                 
                                 <li>Workstation #2: 20-inch monitor </li>
                                 
                                 <li>Workstation #6: Keys-U-See Keyboard, 20-inch monitor</li>
                                 
                                 <li>Workstation #37: 20-inch monitor </li>
                                 
                                 <li>Workstation #51: Height adjustable table, 20-inch monitor, SARA,  Trackball, Braille
                                    keyboard, and PACMate
                                 </li>
                                 
                                 <li>Workstation #60: 20-inch monitor </li>
                                 
                                 <li>Workstation #64: Height adjustable  table, Keys-U-See Keyboard, 20-inch monitor </li>
                                 
                                 <li>Workstation #65: Height adjustable  table</li>
                                 
                                 <li>SPACE #3: 20-inch monitor</li>
                                 
                                 <li>PACMate may also be used on any Dell workstation in this lab</li>
                                 
                              </ul>
                              
                              <h3>Classroom - First Floor: 6-118 </h3>
                              
                              <p> Each classroom  workstation contains Kurzweil 3000, JAWS, Magic, and Dolphin Easy
                                 reader.  Workstation #19 has a 20 inch monitor.
                              </p>
                              
                              <h3> Research Computers - Second Floor: 6-201</h3>
                              
                              <p> Two workstations: LRC  39 and LRC 54 are equipped with the following assistive software
                                 and hardware: 
                              </p>
                              
                              <ul>
                                 
                                 <li>Kurzweil 3000 </li>
                                 
                                 <li>JAWS</li>
                                 
                                 <li>21-inch monitor</li>
                                 
                                 <li>Trackball on LRC 39</li>
                                 
                                 <li>Keys-U-See Keyboard on LRC 39</li>
                                 
                              </ul>
                              
                              <p>Two additional workstations:  LRC 13 and LRC 25 are equipped with Kurzweil 3000 and
                                 Easy Reader. A fifth  workstation: LRC 06 is equipped with Kurzweil 3000 only. 
                              </p>
                              
                              <h3> Classrooms - Second Floor: 6-220 and 6-221</h3>
                              
                              <p> Each classroom  contains one station equipped with Kurzweil 3000, Dragon Naturally
                                 Speaking,  JAWS, a trackball and a 21 inch monitor.
                              </p>
                              
                              <p> Classroom 6-220 also  contains a second workstation equipped with Kurzweil 3000 and
                                 JAWS.
                              </p>
                              
                              <h3> Text Magnification</h3>
                              
                              <p> CCTV is located on  the Second floor (6-201) to the left of the Honors Resource Center.
                                 
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Winter Park</h3>
                              
                              <h3>Computer  Workstations &amp; Assistive Technology</h3>
                              
                              <p>All workstations in Library are equipped with the following  assistive software:</p>
                              
                              
                              <ul>
                                 
                                 <li>Kurzweil 3000</li>
                                 
                                 <li>JAWS</li>
                                 
                                 <li>Magic</li>
                                 
                                 <li>Dolphin Easy Reader</li>
                                 
                                 <li>ClaroRead</li>
                                 
                                 <li>Math player</li>
                                 
                              </ul>
                              
                              <p>CCTV is located in the Quiet/Group Study room.</p>
                              
                              <p>One wheelchair accessible station, with adjustable height table, Keys-U-See  Keyboard,
                                 and dual monitors.
                              </p>
                              
                              <p>One Kurzweil Reader is located in the Quiet/Group Study room.</p>
                              
                           </div>
                           
                        </div> 
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/services/accessibility.pcf">©</a>
      </div>
   </body>
</html>