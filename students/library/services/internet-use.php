<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/services/internet-use.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/services/">Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Internet Use</h2>
                        
                        <h3>Computer Workstations</h3>
                        
                        <p> Computer workstations are  available for student, faculty and staff use.  All Valencia
                           users must log in to library  workstations using their Atlas Username and PIN; UCF
                           Valencia West and Osceola  campus students must use their NID and PID.
                        </p>
                        
                        <p> All users MUST log  in using their own credentials. Users MAY NOT:</p>
                        
                        <ul>
                           
                           <li>Log in with an Atlas Username / PIN or NID / PID  that is not their own.</li>
                           
                           <li>Knowingly utilize another person's log-in  credentials by using a workstation that
                              was not properly logged off by the  previous user.
                           </li>
                           
                           <li>Give their Atlas Username / PIN or NID / PID to  another individual for the purposes
                              of allowing access to Valencia computers.
                           </li>
                           
                           <li>Purposely allow others to utilize their log-in  credentials by failing to log off
                              a Valencia computer.
                           </li>
                           
                        </ul>
                        
                        <p>Please note: All Internet and  network traffic is subject to logging / monitoring.
                           Do not give your log in  credentials to anyone else to use. Always log out when you
                           have finished  working.
                        </p>
                        
                        <h3> Wireless Network</h3>
                        
                        <p> Valencia wi-fi is available  throughout the Library building. Students, faculty and
                           staff must log in to the  wireless network by pointing your browser to  and  entering
                           your Atlas Username and PIN.
                        </p>
                        
                        <h3> Visitors and Guests</h3>
                        
                        <p> Please see our <a href="visitors-guests.html">Visitors and Guests</a> page for information.
                        </p>
                        
                        <h3> Acceptable Use of Library Computers</h3>
                        
                        <p> Access to research computers in  the Library is available to currently enrolled Valencia
                           College students, staff  and faculty, currently enrolled UCF Valencia West and/or
                           UCF Valencia Osceola  Campus students.
                        </p>
                        
                        <p> Library Computers provide access  to the Internet, library electronic resources,
                           and course-specific software.
                        </p>
                        
                        <p> Research and course-related uses  have priority on all library workstations, at all
                           times. <br>
                           The privilege of using library  computers means that users comply with the guidelines
                           herein, the Acceptable  Use of Information Technology Resources policy and the Valencia
                           College Student  Code of Conduct.
                        </p>
                        
                        <h3> Unacceptable Uses of Library  Computers</h3>
                        
                        <ul>
                           
                           <li>Displaying images, sound or text which disrupts  or creates an atmosphere of distress
                              or harassment of other users.
                           </li>
                           
                           <li>Committing illegal or unethical acts, including  unauthorized entry ("hacking") into
                              other computers.
                           </li>
                           
                           <li>Willful damaging or altering computer hardware  or software. </li>
                           
                           <li>Internet hardware/software is to be used as  installed and cannot be modified, deleted
                              or enhanced.
                           </li>
                           
                           <li>Downloading or installing inappropriate files,  programs, etc., to any hard drive
                              or network drive.
                           </li>
                           
                           <li>Using any Valencia computers for commercial  purposes.</li>
                           
                           <li>Violating software license agreements, copyright  laws and fair use provisions through
                              inappropriate reproduction or  dissemination of licensed or copyrighted text, images,
                              etc. Consider everything  on the Internet to be copyrighted or licensed.
                           </li>
                           
                        </ul>
                        
                        <p>Failure to abide by this Acceptable  Use statement may result in the user's access
                           privileges being revoked.
                        </p>
                        
                        <p> All Internet and network traffic is  subject to logging / monitoring. Valencia's
                           online privacy policy can be  reviewed at the following: Privacy Statement
                        </p>
                        
                        <h3> Special Considerations by Campus</h3>
                        
                        <h3> West Campus</h3>
                        
                        <p> During high-use hours between 10  am and 2 pm, Monday through Friday, computer use
                           is limited to research and  course-related activities ONLY.
                        </p>
                        
                        <p> Designated quick-check e-mail  computers are located on the 1st floor of the West
                           Campus Library. These  computers have a timed logoff after 15 minutes of use.    
                           
                        </p>
                        
                        <h3>East Campus</h3>
                        
                        <p> The Library Computer Lab is the Library's academic research lab.  Access to the Internet
                           and online databases is provided for the academic  support and research of Valencia's
                           students, faculty and staff. 
                        </p>
                        
                        <ul>
                           
                           <li>Users must have a  current Valencia ID. </li>
                           
                           <li>Workstations are  available on a first-come, first-served basis. </li>
                           
                           <li>Reference and technical  assistance is available. </li>
                           
                           <li>Library Computer Lab  users are required to abide to the <a href="../documents/computer-guidelines.pdf">Computer Guidelines</a>.
                           </li>
                           
                        </ul>
                        
                        <p><strong>Phone</strong> (407) 582-2889
                        </p>
                        
                        <p><strong>Location</strong> Building  4, Room 201
                        </p>
                        
                        <p><strong>Questions/Comments?</strong> Call us at (407) 582-2889 or send us an <a href="mailto:jchapkin@valenciacollege.edu">email</a>. 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/services/internet-use.pcf">©</a>
      </div>
   </body>
</html>