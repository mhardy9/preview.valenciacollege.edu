<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/services/research-help-information.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/services/">Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Research Help &amp; Info</h2>
                        
                        <p>You can ask a librarian for assistance with:</p>
                        
                        <ul>
                           
                           <li>Choosing a topic for your research</li>
                           
                           <li>Identifying sources you will need for your  research</li>
                           
                           <li>Finding and accessing sources using the  library's databases, catalog, or the Internet</li>
                           
                           <li>Locating items on library shelves</li>
                           
                           <li>Determining whether a source is credible enough  for college-level research</li>
                           
                           <li>Citing sources in MLA or APA format</li>
                           
                           <li>Other information-related questions</li>
                           
                        </ul>
                        
                        <p>Connect with us online, by phone, or in person. </p>
                        
                        <h3> Online: Ask-a-Librarian </h3>
                        
                        <p> Offers live chat, e-mail and texting to connect you with  librarians throughout Florida.
                           Go to <a href="http://www.askalibrarian.org/valencia">www.askalibrarian.org/Valencia</a> to connect  to chat or e-mail. Send texts to 407-456-8155.
                        </p>
                        
                        <h3> In Person and Phone  Services</h3>
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>East</h3>
                              
                              <h3>In Person</h3>
                              
                              <p>Librarians staff the  Information Desk on the library 2nd floor (4-201) during the
                                 Fall and Spring  semesters from 7:30 am until 9:00 pm, Monday through Thursday. Friday
                                 hours are 7:30 am to 5:00 pm.  Librarians are also available on Saturdays from 10:00
                                 am until 2:00 pm. The  Information Desk is not staffed on Sunday. We will be happy
                                 to take appointments if requested,  but walk-ins are welcome and encouraged.
                              </p>
                              
                              <h3>Phone</h3>
                              
                              <p>  Call the Information Desk at  <strong>407-582-2456</strong> for help over the phone.
                              </p>
                              
                              
                              <h3>Other Local Libraries</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://library.ucf.edu/">University of Central Florida</a></li>
                                 
                                 <li><a href="http://www.osceolalibrary.org/">Osceola County Library System</a></li>
                                 
                                 <li><a href="http://www.ocls.lib.fl.us/">Orange County Public Library</a></li>
                                 
                                 <li><a href="http://www.seminolecountyfl.gov/Libraries/index.aspx">Seminole County Public Library </a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Lake Nona</h3>
                              
                              <h3>In Person</h3>
                              
                              <p>Librarians staff the circulation  desk in the library during the fall and spring semesters
                                 from 8:00 am until  4:00 pm Monday through Thursday. No Librarian is on duty Friday.
                                 We will be  happy to take appointments if requested, but walk-ins are welcome and
                                 encouraged.
                              </p>
                              
                              <h3>Phone</h3>
                              
                              <p>Call the  library at <strong>407-582-7107</strong> for help  over the phone.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Osceola</h3>
                              
                              <h3>General  Library Information</h3>
                              
                              <p>407-582-4155</p>
                              
                              <h3>Research  Help</h3>
                              
                              <p>407-582-4154</p>
                              
                              <h3>Helpful Links</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://www.askalibrarian.org/valencia">Ask a Librarian</a></li>
                                 
                                 <li><a href="http://libguides./duncan_industries">Library Research Guides</a></li>
                                 
                                 <li><a href="http://libguides./newitemsosceola">New Items in the Osceola Campus  Library</a></li>
                                 
                              </ul>
                              
                              <h3>Other Local  Libraries</h3>
                              
                              <ul>
                                 
                                 <li><a href="http://library.ucf.edu/">University of Central Florida</a></li>
                                 
                                 <li><a href="http://www.osceolalibrary.org/">Osceola County Library System</a></li>
                                 
                                 <li><a href="http://www.ocls.lib.fl.us/">Orange County Public Library</a></li>
                                 
                              </ul>
                              
                              <p>NOTE: When using  other libraries, limitations may apply.</p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>West</h3>
                              
                              <h3>In Person</h3>
                              
                              <p> Librarians staff the  Information Desk on the library 2nd floor (6-201) beginning
                                 at 8 am and  continuing until closing time, Monday through Friday, and whenever the
                                 library  is open on weekends. We will be happy to take appointments if requested,
                                 but  walk-ins are welcome and encouraged.
                              </p>
                              
                              <h3>Phone</h3>
                              
                              <p> Call the Information Desk at  <strong>407-582-1432</strong> for help over the phone.
                              </p>
                              
                              <p> The Computer Access Lab on the  first floor (6-101) offers computer-related help
                                 (Microsoft Office products,  courseware, and other software applications). However,
                                 if you have computer  questions while working on the second floor, a librarian will
                                 be happy to  assist you.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Winter Park</h3>
                              
                              <h3>In Person</h3>
                              
                              <p>Librarians are available  beginning at 8 am and continuing until closing time, Monday
                                 through Friday. We  will be happy to take appointments if requested, but walk-ins
                                 are welcome and  encouraged.
                              </p>
                              
                              <h3>Phone</h3>
                              
                              <p>Call the Library at  <strong>407-582-6814</strong> for help over the phone.
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/services/research-help-information.pcf">©</a>
      </div>
   </body>
</html>