<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/services/study-areas.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/services/">Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Study Areas</h2>
                        
                        <p>Each campus library provides a choice of study environments to serve diverse student
                           needs.
                        </p>
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           <div>
                              
                              <h3>East</h3>
                              
                              <p> The East Campus Library is a working library and therefore a  certain low level of
                                 noise is to be expected.   We do ask that cell phones, as well as other portable electronic
                                 devices, are turned off.  If you do need  to take a phone call, please leave the library
                                 to do so.
                              </p>
                              
                              <p> Disruptive foods (cooked or crunchy) are not allowed in the  library.  In addition,
                                 drinks need to  have tops on them.  Please be courteous  to your fellow students and
                                 make sure your station is left food-free when you  leave. 
                              </p>
                              
                              <h3> Group Study </h3>
                              
                              <p> East Campus Library provides 4 group study rooms on a first  come/first serve basis.
                                 These  student-use only rooms are checked out at the Circulation Desk with a current
                                 Valencia College student ID for a 2 hour maximum time limit.  The room use is also
                                 limited to 5 students.  Each room has a table, 5 chairs, White Board,  markers and
                                 erasers.  If students need  any additional equipment, devices can be checked out by
                                 the students from the  Audio/Visual Department or Circulation. 
                              </p>
                              
                              <h3> Quiet Study</h3>
                              
                              <p> A Quiet Study Zone is located in the east corner of the  library by the Circulation
                                 Collection.    Please respect your fellow students' needs for a distraction-free area
                                 and  refrain from activities that compromise the atmosphere of quiet study.  Please
                                 notify a library staff member if  someone is disrupting the learning environment.
                                 
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Lake Nona</h3>
                              
                              <h3>Group  Study</h3>
                              
                              <p>The  Lake Nona Campus Library has three group study rooms that are available for 
                                 students' use, two smaller rooms which seat four students and one larger room  which
                                 can comfortably fit up to ten.  The group study rooms are available  in two-hour increments
                                 and can be reserved up to one week in advance. Each room  has a 40 inch plasma television
                                 with VGA and HDMI outlets for laptop  connectivity. There are also two movable whiteboards
                                 that can be used in any of  the rooms.  See the library's circulation desk to reserve
                                 a room or to  checkout a laptop to use in the rooms - a student ID is required.
                              </p>
                              
                              <p>To see  if any rooms are available, visit our <a href="http://10.10.64.147/booking/day.php?area=2">online schedule</a>.
                              </p>
                              
                              <h3>Quiet  Study</h3>
                              Some  students need a quiet space in order to focus when writing a paper or cramming
                              for a test.  To accommodate those students' needs, the library has a  dedicated quiet
                              study room. No cell phone use, headphone use, or conversation  is permitted in this
                              area, and students who wish to collaborate with each other  are free to use an available
                              group study room or the main library space. 
                              
                           </div>
                           
                           <div>
                              
                              <h3>Osceola</h3>
                              
                              <h3>Group Study</h3>
                              
                              <p>The  Osceola Campus Library has seven group study rooms that can be reserved up to
                                 one week in advance. Each room is equipped with a dry erase board and  flatscreen
                                 television for laptop or iPad display, and accommodates a maximum of  six students.
                                 Laptops and iPads are available for checkout if needed. Rooms are  available for a
                                 two hour block, with an additional two hours possible if no  other reservations are
                                 made for your room. See the library's circulation desk  to reserve a room - a student
                                 ID is required.
                              </p>
                              
                              <p>To see  if there are any rooms available, visit our <a href="http://10.10.64.147/booking/day.php?area=2">online schedule</a>.
                              </p>
                              
                              <h3>Quiet Study</h3>
                              Students  who prefer a quiet environment can study in the library's quiet study room,
                              located in the center of the library. No cell phone use, headphone use, or  conversation
                              is permitted, though quiet computer use is permitted. Laptops can  be checked out
                              at the circulation desk if you need quiet while working on a  computer. You do not
                              need to sign in or make a reservation to use the quiet  study room. 
                              
                           </div>
                           
                           <div>
                              
                              <h3>West</h3>
                              
                              <h3> Group Study</h3>
                              
                              <p> Tables are available on the first floor for groups to meet  and study.</p>
                              
                              <p> There are no study rooms available at the West Campus  library. However, the Computer
                                 Access Lab provides a group "SPACE"  where student groups can be seated in cubicles
                                 around computer workstations.
                              </p>
                              
                              <h3> Quiet Study</h3>
                              
                              <p> The second floor of the library is reserved for quiet study.  Cell phones may not
                                 be used in this area and cell phone conversations must be  moved to the adjacent lobby.
                                 Headphones and quiet conversations are permitted.  Discreet snacks and beverages may
                                 be consumed but are not permitted at computer  workstations.
                              </p>
                              
                              <p> Students needing absolute quiet may use the Quiet Study  Room, located on the 2nd
                                 floor (6-201B). Enter the library 2nd floor through  the double doors (6-201) and
                                 turn to your left. Quiet Study room is located at  the end of the corridor, near the
                                 "A" call numbers. All students  entering Quiet Study agree to:
                              </p>
                              
                              <ul>
                                 
                                 <li>Turn off all portable electronic devices,  including cell phones and music devices.</li>
                                 
                                 <li>Use computers in other areas.</li>
                                 
                                 <li>Consume food and beverages in other areas.</li>
                                 
                                 <li>Use the 1st floor for group study.</li>
                                 
                                 <li>Respect the needs of other users to maintain an  absolutely quiet environment.</li>
                                 
                              </ul>
                              
                              <p>Please notify a library staff member if someone is  disrupting the learning environment.
                                 Headphones are available for check-out  from the Library Services desk.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Winter Park</h3>
                              
                              <h3>Quiet  Study</h3>
                              
                              <p>The Quiet Study room is reserved for students needing absolute quiet. Students are
                                 required to respect the needs of other users to maintain an absolutely quiet environment
                                 by turning off all electronic devices and using other places on campus to meet with
                                 other students. Absoutely no conversations are allowed in this area. 
                              </p>
                              
                              <p>Please be aware repeatedly going in and out of the Quiet Study is considered a disruption
                                 of the Quiet Study room.
                              </p>
                              
                              <h3>Library Space</h3>
                              
                              <p>Due to the size of the library, students should respect the learning environment and
                                 refrain from loud or disruptive behavior, including loud conversations, cell phone
                                 conversations or excessive volume when using earbuds. Headphones are available at
                                 computer stations. Discreet snacks and beverages may be consumed in the library.
                              </p>
                              
                              <p>Please notify a library staff member if someone is  disrupting the learning environment
                                 in either space. 
                              </p>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/services/study-areas.pcf">©</a>
      </div>
   </body>
</html>