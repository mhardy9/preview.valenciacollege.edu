<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Library | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/library/services/interlibrary-loan.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/library/services/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Library</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/library/">Library</a></li>
               <li><a href="/students/library/services/">Services</a></li>
               <li>Library</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <h2>Interlibrary Loan &amp; Hold Requests</h2>
                        
                        <p>InterLibrary Loan  Services provides access to library materials not owned by your
                           campus library.  All Valencia students, faculty and staff who are eligible to borrow
                           materials  from the Valencia library are eligible to use Interlibrary Loan services.
                           
                        </p>
                        
                        <p> Hold Requests may  be placed on currently checked-out materials that are owned by
                           your campus  library.
                        </p>
                        
                        
                        <div>
                           
                           
                           <h3>To  Submit a Request </h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4> Online</h4>
                                 
                                 <p> For items owned within the Florida  College System: In the Library Catalog (<a href="http://discover.linccweb.org/primo_library/libweb/action/search.do?dscnt=1&amp;campus_level=true&amp;scp.scps=scope%3A%28FLCC2900_sfx%29%2Cscope%3A%28FLCC2902%29%2Cscope%3A%28FLCC2920%29%2Cscope%3A%28FLCC0100%29%2Cscope%3A%28oai_fcla_fof%29%2Cprimo_central_multiple_fe&amp;dstmp=1369927959762&amp;vid=FLCC2900&amp;amp=&amp;fromLogin=true">LINCCWeb</a>),  click on the Request link found in the record for the item you wish to borrow;
                                    follow instructions. 
                                 </p>
                                 
                                 <p> For items not owned within the  Florida College System: Use the <a href="http://www.linccweb.org/eResources.asp?DB=FirstWeb&amp;CID=29">FirstSearch</a> databases to search for materials. Click on the ILL button found in the record  for
                                    the item you wish to borrow; follow instructions.
                                 </p>
                                 
                                 <h4> In  person</h4>
                                 
                                 <p> Inquire with a librarian at the  Reference Desk or Information Desk.</p>
                                 
                                 <h4> By  email</h4>
                                 
                                 <p> Students: Please go to <a href="http://www.askalibrarian.org/valencia">www.askalibrarian.org/valencia</a> to send an e-mail.
                                 </p>
                                 
                                 <p> Faculty: Please e-mail your <a href="../faculty-services/liason-librarians.html">division  library liaison</a>.
                                 </p>
                                 
                                 <h4> By  phone</h4>
                                 
                                 <p> Contact the Reference Desk at your  campus library:</p>
                                 
                                 <ul>
                                    
                                    <li> East: (407) 582-2456</li>
                                    
                                    <li> Lake Nona: (407) 582-7107</li>
                                    
                                    <li> Osceola: (407) 582-4154</li>
                                    
                                    <li> West: (407) 582-1432</li>
                                    
                                    <li> Winter Park: (407) 582-6814</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Limitations</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> Please do NOT  place hold or loan requests for items already available at your campus
                                    library.  Instead, see a librarian for assistance in locating available materials.
                                 </p>
                                 
                                 <p>Some materials may  not be available via Interlibrary Loan: </p>
                                 
                                 <ul>
                                    
                                    <li>Textbooks </li>
                                    
                                    <li>Reference materials</li>
                                    
                                    <li>Most audiovisual materials</li>
                                    
                                    <li>Rare books </li>
                                    
                                    <li>Other materials as designated by lending  institutions </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Time for Delivery</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <h4> Interlibrary Loan  Requests</h4>
                                 
                                 <p>In general, please allow for delivery of Interlibrary Loan materials: </p>
                                 
                                 <ul>
                                    
                                    <li>Two to four days for materials requested from  other Valencia libraries</li>
                                    
                                    <li>One week for materials requested from libraries  within the Florida College system</li>
                                    
                                    <li>One to two weeks for materials requested from  libraries outside the Florida College
                                       system.
                                    </li>
                                    
                                 </ul>
                                 
                                 <h4>Hold Requests</h4>
                                 
                                 <p> Time required for checked-out Valencia materials placed on hold varies  depending
                                    on the due date and the timely return of materials by the current  borrower. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Borrowing and Returning Materials</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> Library staff will contact the user by Valencia e-mail when your item arrives and
                                    becomes  available.
                                 </p>
                                 
                                 <p> Loan periods are determined by the lending library.</p>
                                 
                                 <p> Return items to the Check-Out Desk.</p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Overdue Materials</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p> Interlibrary loan  materials are subject to the same fines as materials owned by
                                    Valencia.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <h3>Library Hours</h3>
                        
                        <p>Hours are subject to change due to final exams, holidays, and other college closings.</p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        <div data-old-tag="table">
                           
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       East Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, 2nd Floor</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-2459</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7am to 10pm<br>Friday: 7am-8pm<br>Saturday - 8am to 4pm<br>Sunday: 2pm-8pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Lake Nona Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 330</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-7107</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 9pm<br>Friday: 8am - 5pm<br>Saturday: 8am - 3pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Osceola Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 4, Rm 202</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-4155</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 7:00am to 10:00pm<br>Friday: 7:00am to 5:00pm<br>Saturday: 8:00am to 1:00pm<br>Closed Sundays<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Poinciana Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 331</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6027</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr"> 
                                 
                                 <div data-old-tag="td">
                                    <a href="mailto:kbest7@valenciacollege.edu">kbest7@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am - 8pm
                                    Friday: 8am - 5pm
                                    Saturday: 8am - 12pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       West Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 6</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-1574</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <br>Monday - Thursday: 7:30am-10:00pm<br>Friday: 7:30am-5:00pm<br>Saturday: 9:00am-1:00pm<br>Sunday: 2:00pm-6:00pm<br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>
                                       Winter Park Campus 
                                       </strong></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Bldg 1, Rm 140</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">407-582-6814</div>
                                 
                              </div>
                              
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Monday - Thursday: 8am to 7pm<br>Fridays: 8am to 12pm<br>The Winter Park Campus is closed Saturday and Sunday.
                                    <br>
                                    <br>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/library/services/interlibrary-loan.pcf">©</a>
      </div>
   </body>
</html>