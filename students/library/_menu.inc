<ul>
	<li><a href="/library/">Library</a></li>
	<li class="submenu"><a class="show-submenu" href="/library/services/">Services <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li> <a href="/library/services/accessibility.php"> Accessibility</a></li>
			<li><a href="/library/services/how-to-borrow.php">  How to Borrow</a></li>
			<li><a href="/library/services/internet-use.php">  Internet Use</a></li>
			<li><a href="/library/services/print-scan-copy.php">  Print, Scan, &amp; Copy</a></li>
			<li> <a href="/library/services/research-help-information.php">  Research Help &amp; Info</a></li>
			<li> <a href="/library/services/interlibrary-loan.php">  Interlibrary Loan</a></li>
			<li><a href="/library/services/study-areas.php">  Study Areas</a></li>
			<li><a href="/library/services/visitors-guests.php">  Visitors &amp; Guests</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/library/faculty-services/">Faculty Services <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/library/faculty-services/library-instruction.php"> Library Instruction</a></li>
			<li><a href="/library/faculty-services/course-reserves.php">  Course Reserves</a></li>
			<li><a href="/library/faculty-services/liason-librarians.php">  Liaison Librarians</a></li>
			<li><a href="/library/faculty-services/information-literacy.php">  Information Literacy</a></li>
		</ul>
	</li>
	<li><a href="/library/mla-apa-chicago-guides/">MLA, APA &amp; Chicago</a></li>
	<li class="submenu"><a class="show-submenu" href="/library/tutorials/">Tutorials <span class="caret" aria-hidden="true"></span></a>

		<ul>
			<li><a href="/library/tutorials/evaluating-websites.php">  Evaluating Web Sites</a></li>
			<li><a href="/library/tutorials/information-literacy-modules.php">  Information Literacy Modules</a></li>
			<li> <a href="/library/tutorials/plagiarism.php">  Plagiarism </a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu"  href="/library/about/">About Us <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li> <a href="/library/about/hours.php">  Locations &amp; Hours</a></li>
			<li> <a href="/library/about/departments-staff.php">  Departments &amp; Staff</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu"  href="/library/locations/east-campus-library.php">Campus Libraries <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li> <a href="/library/locations/east-campus-library.php"> East Campus</a> </li>
			<li><a href="/library/locations/lake-nona-campus-library.php">  Lake Nona Campus</a></li>
			<li><a href="/library/locations/osceola-campus-library.php">  Osceola Campus</a></li>
			<li> <a href="/library/locations/poinciana-campus-library.php">  Poinciana Campus</a></li>
			<li><a href="/library/locations/west-campus-library.php">  West Campus</a></li>
			<li> <a href="/library/locations/winter-park-campus-library.php">  Winter Park Campus</a></li>
		</ul>
	</li>
	<li><a href="/library/faq.php">FAQ</a></li>
	<li><a href="http://net4.valenciacollege.edu/forms/library/contact.php">Contact Us</a></li>
</ul>