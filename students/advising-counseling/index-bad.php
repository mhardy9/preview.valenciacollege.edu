<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advising &amp; Counseling  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/advising-counseling/index-bad.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/advising-counseling/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/advising-counseling/">Advising Counseling</a></li>
               <li>Advising &amp; Counseling </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>About Advising &amp; Counseling</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="wrapper_indent">
                        
                        <p>Educational advising staff and counselors provide developmental advising which includes
                           life, career and
                           educational planning, interpretation of assessments, strategies to address academic
                           difficulties, programs
                           to develop student success skills, preparation for university transfer, and workforce
                           preparedness.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Office Hours &amp; Locations</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        
                        
                        <ul class="list-unstyled">
                           
                           <li>
                              <strong>Monday - Thursday:</strong> 8 AM to 6 PM
                           </li>
                           
                           <li>
                              <strong>Friday:</strong> 9 AM - 5 PM
                           </li>
                           
                           <li>
                              <strong>Summer Fridays:</strong> 9 AM - 12 PM
                           </li>
                           
                        </ul>
                        
                        
                        <hr>
                        
                        
                        <ul class="list-inline">
                           
                           <li>
                              <p><strong>West</strong><br> HSB, Rm. 105
                              </p>
                           </li>
                           
                           
                           <li>
                              <p><strong>East</strong><br> Bldg. 5, Rm. 211
                              </p>
                           </li>
                           
                           
                           <li>
                              <p><strong>Osceola</strong><br> Bldg. 2, Rm. 150
                              </p>
                           </li>
                           
                           
                           <li>
                              <p><strong>Winter Park</strong><br> Bldg. 1, Rm. 210
                              </p>
                           </li>
                           
                           &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                           &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                           
                           <li>
                              <p><strong>Lake Nona</strong><br> Bldg. 1, Rm. 149 
                              </p>
                           </li>
                           =======
                           =======
                           
                           <div id="preloader">
                              
                              <div class="pulse"></div>
                              
                           </div>
                           <a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
                           
                           &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; 1c7cf389237c1dd4755e37315e933d29961ba73f
                           
                           <div class="header-alert" id="valencia-alert"></div>
                           
                           
                           <div class="header-chrome desktop-only">
                              
                              <div class="container">
                                 
                                 <nav>
                                    
                                    <div class="row">
                                       
                                       <div class="chromeNav col-lg-9 col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Main Navigation">
                                          
                                          <ul>
                                             
                                             <li class="submenu">
                                                <a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                                
                                                <ul>
                                                   
                                                   <li><a href="/academics/current-students/index.html">Current Students</a></li>
                                                   
                                                   <li><a href="https://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                                   
                                                   <li><a href="https://international.valenciacollege.edu">International Students</a></li>
                                                   
                                                   <li><a href="/academics/military-veterans/index.html">Military &amp;
                                                         Veterans</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li><a href="/academics/continuing-education/index.html">Continuing Education</a></li>
                                             
                                             <li><a href="/EMPLOYEES/faculty-staff.html">Faculty &amp; Staff</a></li>
                                             
                                             <li><a href="/FOUNDATION/alumni/index.html">Alumni &amp; Foundation</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <div class="chromeNav col-md-3 col-sm-3 col-xs-3 pull-right">
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="#" data-toggle="modal" data-target="#login">Login <i class="far fa-angle-down" aria-hidden="true"></i></a>
                                                
                                                <ul>
                                                   
                                                   <li class="fa-2x"><a href="https://atlas.valenciacollege.edu/"><i class="far fa-user-circle-o" aria-hidden="true">&nbsp;</i> Atlas</a></li>
                                                   
                                                   <li>
                                                      <a href="https://valenciaalumniconnect.com/"><i class="far fa-users" aria-hidden="true">&nbsp;</i> Alumni Connect</a>
                                                      
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"><i class="valencia-font icon-o365" aria-hidden="true">&nbsp;</i> Office 365</a>
                                                      
                                                   </li>
                                                   
                                                   
                                                   <li><a href="http://learn.valenciacollege.edu"><i class="far fa-graduation-cap" aria-hidden="true">&nbsp;</i>Valencia Online</a></li>
                                                   
                                                   <li><a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"><i class="far fa-envelope" aria-hidden="true">&nbsp;</i> Webmail</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="https://search.valenciacollege.edu" id="search_bt"><i class="far fa-search" aria-hidden="true"></i><span>Search</span></a>
                                                
                                             </li>
                                             &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; fea88b4764015604ad30f5e4d5de1ce47c588a31
                                             
                                          </ul>
                                          
                                          
                                       </div>
                                       
                                       <hr class="styled_2">
                                       
                                       
                                       <div class="indent_title_in">
                                          
                                          <h3>Calculate Your GPA</h3>
                                          
                                       </div>
                                       
                                       <div class="wrapper_indent">
                                          
                                          <p>Your Grade Point Average (GPA) is a weighted average of all your course work. This
                                             means that a grade in
                                             a four-hour course will change your GPA more than the same grade in a three-hour course.
                                             It also means
                                             that your GPA gets harder to change when you have taken more courses.
                                          </p>
                                          
                                          
                                          <p>Semester GPA (Grade Point Average) is calculated by dividing the total grade points
                                             earned by the total
                                             number of semester credit hours. (Total Grade Points/Total Credit Hours = GPA.)
                                          </p>
                                          
                                          
                                          <p>To calculate your estimated GPA, you would do the following:</p>
                                          
                                          
                                          <p>For each course you multiply the course credit hours times the grade you receive to
                                             get the "grade
                                             points" for that course. Your grades have the following numerical equivalents; each
                                             A is 4, each B is 3,
                                             each C is 2, each D is 1, each F is 0 and each WF is 0. W and WP do not receive grade
                                             points and do not
                                             have an effect on the GPA. Add up the grade points for each course and divide the
                                             results by the total
                                             credit hours to get your semester's GPA <strong>(remember to exclude Ws and WPs from your total credit
                                                hours)</strong>.
                                          </p>
                                          
                                          
                                          <p>You can use the form below to help you calculate your GPA.</p>
                                          
                                          
                                          <form id="gpaData">
                                             
                                             
                                             <table class="table table table-striped add_bottom_30">
                                                
                                                <thead>
                                                   
                                                   <tr>
                                                      
                                                      <th id="courseLabel">Course
                                                         
                                                         
                                                      </th>
                                                      
                                                      <th id="creditHoursLabel">Credit Hours
                                                         
                                                         
                                                      </th>
                                                      
                                                      <th id="gradeLabel">Grade
                                                         
                                                         
                                                      </th>
                                                      
                                                   </tr>
                                                   
                                                </thead>
                                                
                                                <tbody>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course1" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours1" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade1">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course2" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours2" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade2">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course3" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours3" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade3">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course4" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours4" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade4">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course5" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours5" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade5">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course6" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours6" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade6">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                   
                                                   <tr>
                                                      
                                                      <td><input type="text" size="30" name="course7" aria-labelledby="courseLabel"></td>
                                                      
                                                      <td><input type="text" size="30" name="creditHours7" aria-labelledby="creditHoursLabel"></td>
                                                      
                                                      <td>
                                                         <select name="grade7">
                                                            
                                                            <option value="-1" selected>Select</option>
                                                            
                                                            <option value="4.0">A</option>
                                                            
                                                            <option value="3.0">B</option>
                                                            
                                                            <option value="2.0">C</option>
                                                            
                                                            <option value="1.0">D</option>
                                                            
                                                            <option value="0.0">F</option>
                                                            
                                                            <option value="-1">W</option>
                                                            
                                                            <option value="-1">WP</option>
                                                            
                                                            <option value="0.0">WF</option>
                                                            </select>
                                                         
                                                      </td>
                                                      
                                                   </tr>
                                                   
                                                </tbody>
                                                
                                             </table>
                                             
                                             <a href="javascript:gpaCalculator()" class="button">Calculte Your GPA</a>
                                             
                                             
                                             <p>Your GPA:&nbsp;<span id="gpaResults"></span></p>
                                             
                                             
                                          </form>
                                          
                                       </div>
                                       
                                       <hr class="styled_2">
                                       
                                       
                                       <div class="indent_title_in">
                                          
                                          <h3>Raise Your GPA</h3>
                                          
                                       </div>
                                       
                                       <div class="wrapper_indent">
                                          
                                          
                                       </div> 
                                       
                                    </div>
                                    
                                    
                                    <aside class="col-md-3">
                                       
                                       <div class="box_side">
                                          
                                          
                                          <h3 class="add_bottom_30">Helpful Links</h3>
                                          
                                          
                                          <a href="https://valenciacollege.edu/support/howto/atlas-advising-chat-setup.cfm" class="button btn-block text-center">Advising Chat</a> <a href="http://net4.valenciacollege.edu/forms/advising-counseling/contact.cfm" class="button btn-block text-center">Contact an Advisor</a>
                                          
                                       </div>
                                       
                                       
                                    </aside>
                                    
                                 </nav>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                        </ul>
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     <main role="main">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </main>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/advising-counseling/index-bad.pcf">©</a>
      </div>
   </body>
</html>