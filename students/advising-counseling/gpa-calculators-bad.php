<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advising &amp; Counseling | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/advising-counseling/gpa-calculators-bad.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/advising-counseling/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Advising &amp; Counseling</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/advising-counseling/">Advising Counseling</a></li>
               <li>Advising &amp; Counseling</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="main-title">
                     
                     <h2>GPA Calclulators</h2>
                     
                     <p></p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-9"> <a name="content" id="content"></a>
                        
                        <h3>Semester GPA Calculator</h3>
                        
                        <div class="box_style_3">
                           
                           <form name="gpadata" method="post" aria-label="Semester GPA Calculator" id="gpadata">
                              
                              <div class="row margin-30">
                                 
                                 <div class="col-md-10 col-sm-10">
                                    <label for="course1">Course:</label>
                                    <input type="text" size="30" name="course1" aria-label="Name of Course 1">
                                    <label for="credithours1">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours1" aria-label="Credit Hours for Course 1">
                                    <label for="grade1">Grade:</label>
                                    <select name="grade1" aria-label="Grade for Course 1">
                                       
                                       <option value="-1" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    <br>
                                    <label for="course2">Course:</label>
                                    <input type="text" size="30" name="course2" aria-label="Name of Course 2">
                                    <label for="credithours2">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours2" aria-label="Credit Hours for Course 2">
                                    <label for="grade2">Grade:</label>
                                    <select name="grade2" aria-label="Grade for Course 2">
                                       
                                       <option value="-2" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    <br>
                                    <label for="course3">Course:</label>
                                    <input type="text" size="30" name="course3" aria-label="Name of Course 3">
                                    <label for="credithours3">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours3" aria-label="Credit Hours for Course 3">
                                    <label for="grade3">Grade:</label>
                                    <select name="grade3" aria-label="Grade for Course 3">
                                       
                                       <option value="-2" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    <br>
                                    <label for="course4">Course:</label>
                                    <input type="text" size="30" name="course4" aria-label="Name of Course 4">
                                    <label for="credithours4">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours4" aria-label="Credit Hours for Course 4">
                                    <label for="grade4">Grade:</label>
                                    <select name="grade4" aria-label="Grade for Course 4">
                                       
                                       <option value="-2" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    <br>
                                    <label for="course5">Course:</label>
                                    <input type="text" size="30" name="course5" aria-label="Name of Course 5">
                                    <label for="credithours5">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours5" aria-label="Credit Hours for Course 5">
                                    <label for="grade5">Grade:</label>
                                    <select name="grade5" aria-label="Grade for Course 5">
                                       
                                       <option value="-2" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    <br>
                                    <label for="course6">Course:</label>
                                    <input type="text" size="30" name="course6" aria-label="Name of Course 6">
                                    <label for="credithours6">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours6" aria-label="Credit Hours for Course 6">
                                    <label for="grade6">Grade:</label>
                                    <select name="grade6" aria-label="Grade for Course 6">
                                       
                                       <option value="-2" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    <br>
                                    <label for="course7">Course:</label>
                                    <input type="text" size="30" name="course7" aria-label="Name of Course 7">
                                    <label for="credithours7">Credit Hours:</label>
                                    <input type="text" size="5" name="credithours7" aria-label="Credit Hours for Course 7">
                                    <label for="grade7">Grade:</label>
                                    <select name="grade7" aria-label="Grade for Course 7">
                                       
                                       <option value="-2" selected>Select</option>
                                       
                                       <option value="4.0">A</option>
                                       
                                       <option value="3.0">B</option>
                                       
                                       <option value="2.0">C</option>
                                       
                                       <option value="1.0">D</option>
                                       
                                       <option value="0.0">F</option>
                                       
                                       <option value="-1">W</option>
                                       
                                       <option value="-1">WP</option>
                                       
                                       <option value="0.0">WF</option>
                                       </select>
                                    
                                 </div>
                                 
                                 <div class="col-md-2 col-sm-2">Your Calculated GPA:<br>
                                    <input type="text" size="5" name="result" class="results" readonly aria-label="Calculated GPA">
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div class="row">
                                 
                                 <div class="col-md-12 col-sm-12 text-center">
                                    <input type="button" name="calculate" value="Calculate Your GPA" onclick="gpacalculator(this.form)" class="button-outline">
                                    <input type="reset" name="reset" value="Clear" class="button-outline" aria-label="Clear Form">
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              
                           </form>
                           
                        </div>
                        
                        <div class="row">
                           
                           <p>Semester GPA (Grade Point Average) is calculated by dividing the total grade points
                              earned by the total number of semester credit hours.
                           </p>
                           
                           <blockquote>
                              
                              <p>Total Grade Points/Total Credit Hours = GPA</p>
                              
                           </blockquote>
                           
                           <h4>EXAMPLE</h4>
                           
                           <p>Your Grade Point Average (GPA) is a weighted average of all your course work. This
                              means that a grade in a four-hour course will change your GPA more than the same grade
                              in a three-hour course. It also means that your GPA gets harder to change when you
                              have taken more courses. To calculate your estimated GPA, you would do the following
                           </p>
                           
                           <p>For each course you multiply the course credit hours times the grade you receive to
                              get the "grade points" for that course. Your grades have the following numerical equivalents;
                              each A is 4, each B is 3, each C is 2, each D is 1, each F is 0 and each WF is 0.
                              W and WP do not receive grade points and do not have an effect on the GPA.  Add up
                              the grade points for each course and divide the results by the total credit hours
                              to get your semester's GPA (remember to exclude Ws and WPs from your total credit
                              hours). For example, suppose you were taking the following courses:
                           </p>
                           
                           <table class="table table table-striped add_bottom_30">
                              
                              <tr>
                                 
                                 <th scope="col">Course</th>
                                 
                                 <th scope="col">Credit Hours</th>
                                 
                                 <th scope="col">Letter Grade</th>
                                 
                                 <th scope="col">Grade Points</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1. ENC 1102</td>
                                 
                                 <td>3</td>
                                 
                                 <td>A</td>
                                 
                                 <td>12</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2. MAC 2311</td>
                                 
                                 <td>5</td>
                                 
                                 <td>C</td>
                                 
                                 <td>10</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3. PEM 1171</td>
                                 
                                 <td>2</td>
                                 
                                 <td>B</td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>4. SPC 1600</td>
                                 
                                 <td>3</td>
                                 
                                 <td>D</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td align="right"><strong>Total</strong></td>
                                 
                                 <td><strong>13</strong></td>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <td><strong>31</strong></td>
                                 
                              </tr>
                              
                           </table>
                           
                           <p>Your grade point total for the term (31) is divided by your total credit hours (13)
                              which equals your GPA (2.38).
                           </p>
                           
                           <p>If you want to raise your GPA, an additional calculator below helps you determine
                              how many credit hours and what grade average you will need to raise your current GPA.
                           </p>
                           
                        </div>
                        
                        <div class="row">
                           
                           <h3>Raise Your GPA</h3>
                           
                           <form action="none" name="GPA" aria-label="Raise GPA Calculator" id="GPA">
                              
                              <div class="box_style_3">
                                 
                                 <div class="col-md-12 col-sm-12">
                                    
                                    <p>In order to answer these questions you need to supply the following information:</p>
                                    <label for="CPGA">Current Cumulative GPA:</label>
                                    <input type="text" name="CGPA" size="4" value="0" aria-label="Current Cumulative GPA"><br>
                                    <label for="TTD">Total Number of Attempted Credit Hours to Date:</label>
                                    <input type="text" name="TTD" size="3" value="0" aria-label="Total Number of Attempted Credit Hours to Date">
                                    
                                    <p><strong>DO NOT INCLUDE</strong> current term hours and Ws and WPs)
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <hr class="styled_2">
                              
                              <div class="box_style_3">
                                 
                                 <div class="row margin-30">
                                    
                                    <div class="col-md-10 col-sm-10">
                                       
                                       
                                       <h4>Grades Needed to Raise GPA</h4>
                                       
                                       <p>Enter the credit hours and expected grade for (up to six) courses that you are presently
                                          taking below and press CALCULATE to find your predicted new cumulative GPA at the
                                          end of this term.
                                       </p>
                                       <label>Course 1:</label>
                                       <label for="cach">Credit Hours:</label>
                                       <select name="cach" aria-label="Credit Hours for Course 1">
                                          
                                          <option value="0" selected>0</option>
                                          
                                          <option value="1">1</option>
                                          
                                          <option value="2">2</option>
                                          
                                          <option value="3">3</option>
                                          
                                          <option value="4">4</option>
                                          
                                          <option value="5">5</option>
                                          </select>
                                       <label for="cagr">Anticipated Grade:</label>
                                       <select name="cagr" aria-label="Anticipated Grade for Course 1">
                                          
                                          <option value="0">WF</option>
                                          
                                          <option value="0">F</option>
                                          
                                          <option value="1">D</option>
                                          
                                          <option value="2">C</option>
                                          
                                          <option value="3">B</option>
                                          
                                          <option value="4" selected>A</option>
                                          </select>
                                       <br>
                                       <label>Course 2:</label>
                                       <label for="cbch">Credit Hours:</label>
                                       <select name="cbch" aria-label="Credit Hours for Course 2">
                                          
                                          <option value="0" selected>0</option>
                                          
                                          <option value="1">1</option>
                                          
                                          <option value="2">2</option>
                                          
                                          <option value="3">3</option>
                                          
                                          <option value="4">4</option>
                                          
                                          <option value="5">5</option>
                                          </select>
                                       <label for="cbgr">Anticipated Grade:</label>
                                       <select name="cbgr" aria-label="Anticipated Grade for Course 2">
                                          
                                          <option value="0">WF</option>
                                          
                                          <option value="0">F</option>
                                          
                                          <option value="1">D</option>
                                          
                                          <option value="2">C</option>
                                          
                                          <option value="3">B</option>
                                          
                                          <option value="4" selected>A</option>
                                          </select>
                                       <br>
                                       <label>Course 3:</label>
                                       <label for="ccch">Credit Hours:</label>
                                       <select name="ccch" aria-label="Credit Hours for Course 3">
                                          
                                          <option value="0" selected>0</option>
                                          
                                          <option value="1">1</option>
                                          
                                          <option value="2">2</option>
                                          
                                          <option value="3">3</option>
                                          
                                          <option value="4">4</option>
                                          
                                          <option value="5">5</option>
                                          </select>
                                       <label for="ccgr">Anticipated Grade:</label>
                                       <select name="ccgr" aria-label="Anticipated Grade for Course 3">
                                          
                                          <option value="0">WF</option>
                                          
                                          <option value="0">F</option>
                                          
                                          <option value="1">D</option>
                                          
                                          <option value="2">C</option>
                                          
                                          <option value="3">B</option>
                                          
                                          <option value="4" selected>A</option>
                                          </select>
                                       <br>
                                       <label>Course 4:</label>
                                       <label for="cdch">Credit Hours:</label>
                                       <select name="cdch" aria-label="Credit Hours for Course 4">
                                          
                                          <option value="0" selected>0</option>
                                          
                                          <option value="1">1</option>
                                          
                                          <option value="2">2</option>
                                          
                                          <option value="3">3</option>
                                          
                                          <option value="4">4</option>
                                          
                                          <option value="5">5</option>
                                          </select>
                                       <label for="cdgr">Anticipated Grade:</label>
                                       <select name="cdgr" aria-label="Anticipated Grade for Course 4">
                                          
                                          <option value="0">WF</option>
                                          
                                          <option value="0">F</option>
                                          
                                          <option value="1">D</option>
                                          
                                          <option value="2">C</option>
                                          
                                          <option value="3">B</option>
                                          
                                          <option value="4" selected>A</option>
                                          </select>
                                       <br>
                                       <label>Course 5:</label>
                                       <label for="cech">Credit Hours:</label>
                                       <select name="cech" aria-label="Credit Hours for Course 5">
                                          
                                          <option value="0" selected>0</option>
                                          
                                          <option value="1">1</option>
                                          
                                          <option value="2">2</option>
                                          
                                          <option value="3">3</option>
                                          
                                          <option value="4">4</option>
                                          
                                          <option value="5">5</option>
                                          </select>
                                       <label for="cegr">Anticipated Grade:</label>
                                       <select name="cegr" aria-label="Anticipated Grade for Course 5">
                                          
                                          <option value="0">WF</option>
                                          
                                          <option value="0">F</option>
                                          
                                          <option value="1">D</option>
                                          
                                          <option value="2">C</option>
                                          
                                          <option value="3">B</option>
                                          
                                          <option value="4" selected>A</option>
                                          </select>
                                       <br>
                                       <label>Course 6:</label>
                                       <label for="cfch">Credit Hours:</label>
                                       <select name="cfch" aria-label="Credit Hours for Course 6">
                                          
                                          <option value="0" selected>0</option>
                                          
                                          <option value="1">1</option>
                                          
                                          <option value="2">2</option>
                                          
                                          <option value="3">3</option>
                                          
                                          <option value="4">4</option>
                                          
                                          <option value="5">5</option>
                                          </select>
                                       <label for="cfgr">Anticipated Grade:</label>
                                       <select name="cfgr" aria-label="Anticipated Grade for Course 6">
                                          
                                          <option value="0">WF</option>
                                          
                                          <option value="0">F</option>
                                          
                                          <option value="1">D</option>
                                          
                                          <option value="2">C</option>
                                          
                                          <option value="3">B</option>
                                          
                                          <option value="4" selected>A</option>
                                          </select>
                                       <br>
                                       
                                    </div>
                                    
                                    <div class="col-md-2 col-sm-2">Calculated Results:<br>
                                       <input type="text" name="result" size="5" class="results" readonly aria-label="Calculated Results">
                                       
                                    </div>
                                    
                                    
                                    <div class="row">
                                       
                                       <div class="col-md-12 col-sm-12 text-center">
                                          <input type="button" name="calculate" value="Calculate Your Raised GPA" onclick="compute(this.form)" class="button-outline">
                                          <input type="reset" name="reset" value="Clear" class="button-outline" aria-label="Clear Form">
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <hr class="styled_2">
                              
                              <h4>Credit Hours Needed to Raise GPA</h4>
                              
                              <div class="box_style_3">
                                 
                                 <div class="row margin-30">
                                    
                                    <div class="col-md-10 col-sm-10">
                                       <label for="CGPA3">I would like to raise my cumulative GPA to</label>
                                       <input type="text" name="CGPA3" size="4" value="0" aria-label="Desired GPA"><br><label for="PGPA">If I can maintain a</label>
                                       <input type="text" name="PGPA" size="4" value="4.0" aria-label="Maintain GPA">
                                       average from now on,(e.g. 3.5, 2.5,...) 
                                    </div>
                                    
                                    <div class="col-md-2 col-sm-2">
                                       <label for="result2">Credit Hours to raise my GPA to this new level</label><br>
                                       <input type="text" name="result2" size="8" class="results" readonly aria-label="Credit Hours Needed to Raise GPA">
                                       
                                    </div>
                                    
                                    <div class="row">
                                       
                                       <div class="col-md-12 col-sm-12 text-center">
                                          <input type="button" name="calculate" value="Calculate Your Credit Hours" onclick="compute2(this.form)" class="button-outline">
                                          <input type="reset" name="reset" value="Clear" class="button-outline" aria-label="Clear Form">
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <hr class="styled_2">
                              
                              
                              <h3>Average Needed to Raise GPA</h3>
                              
                              <table class="table ">
                                 
                                 <tr>
                                    
                                    <td>&nbsp;</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>I am currently taking
                                       <input type="text" name="PGPAB" size="4" value="0" aria-label="Credit Hours Currently Taking">
                                       credit hours.
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>If I want to raise my cumulative GPA to
                                       <input type="text" name="CGPA3B" size="4" value="0" aria-label="Want to Raise GPA to">
                                       at the end of this term,
                                    </td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>what will I have to average on my current courses to reach this goal?</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <td>
                                       <input type="button" value="Calculate" onclick="compute3(this.form)" class="button1">
                                       <input type="text" name="result3" size="8" class="results" readonly aria-label="Average needed in Current Courses">
                                       
                                    </td>
                                    
                                 </tr>
                                 
                              </table>
                              
                           </form>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="col-md-3">
                     
                     <aside> <a href="http://net4.valenciacollege.edu/forms/advising-counseling/contact.cfm" target="_blank"><img alt="Contact an Advisor" border="0" height="60" src="button-contact-advisor.png" width="245"></a> <br>
                        <br>
                        <a href="../support/howto/atlas-advising-chat-setup.html"><img alt="Advising Chat" border="0" height="60" src="button-advising-chat.png" width="245"></a>
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        <div> 
                           
                           
                           
                           
                           <div> Advising and Counseling </div>
                           <br>
                           
                           <div>
                              
                              <div>
                                 
                                 <div> </div>
                                 
                                 <div>
                                    
                                    <div><a href="mailto:advising@valenciacollege.edu">advising@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 8am to 6pm<br>
                                       Friday: 9am to 5pm<br>
                                       Friday (Summer Hours): 9am to 12pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong> East Campus Advising and Counseling </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 210</div>
                                    
                                 </div>
                                 
                                 <div> </div>
                                 
                                 <div> </div>
                                 
                                 <div>
                                    
                                    <div><strong> Lake Nona Campus Advising and Counseling </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 149</div>
                                    
                                 </div>
                                 
                                 <div> </div>
                                 
                                 <div> </div>
                                 
                                 <div>
                                    
                                    <div><strong> Osceola Campus Advising and Counseling </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 2, Rm 140</div>
                                    
                                 </div>
                                 
                                 <div> </div>
                                 
                                 <div> </div>
                                 
                                 <div>
                                    
                                    <div><strong> Poinciana Campus Student Services (Advising) </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Room 101</div>
                                    
                                 </div>
                                 
                                 <div> </div>
                                 
                                 <div> </div>
                                 
                                 <div>
                                    
                                    <div><strong> West Campus Advising and Counseling </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 110</div>
                                    
                                 </div>
                                 
                                 <div> </div>
                                 
                                 <div> </div>
                                 
                                 <div>
                                    
                                    <div><strong> Winter Park Campus Advising and Counseling </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206</div>
                                    
                                 </div>
                                 
                                 <div> </div>
                                 
                                 <div> </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/advising-counseling/gpa-calculators-bad.pcf">©</a>
      </div>
   </body>
</html>