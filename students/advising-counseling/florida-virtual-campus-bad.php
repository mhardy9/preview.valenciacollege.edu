<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Florida Virtual Campus  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/advising-counseling/florida-virtual-campus-bad.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/advising-counseling/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/advising-counseling/">Advising Counseling</a></li>
               <li>Florida Virtual Campus </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div id="preloader"></div>
               &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
               
               <a href="javascript:" id="return-to-top" aria-label="Return to top">
                  <i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i>
                  </a>
               
               =======
               <a href="javascript:" id="return-to-top" aria-label="Return to top">
                  <i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i>
                  </a>
               
               
               <div class="header-alert" id="valencia-alert"></div>
               &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
               &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; fea88b4764015604ad30f5e4d5de1ce47c588a31
               =======
               &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; 1c7cf389237c1dd4755e37315e933d29961ba73f
               
               
               <div class="header-chrome desktop-only">
                  
                  <div class="container">
                     
                     <nav>
                        
                        <div class="row">
                           
                           <div class="chromeNav col-lg-9 col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Main Navigation">
                              
                              <ul>
                                 
                                 <li class="submenu">
                                    <a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                    
                                    <ul>
                                       
                                       <li><a href="/academics/current-students/index.html">Current Students</a></li>
                                       
                                       <li><a href="https://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                       
                                       <li><a href="https://international.valenciacollege.edu">International Students</a></li>
                                       
                                       <li><a href="/academics/military-veterans/index.html">Military &amp;
                                             Veterans</a></li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li><a href="/academics/continuing-education/index.html">Continuing Education</a></li>
                                 
                                 <li><a href="/EMPLOYEES/faculty-staff.html">Faculty &amp; Staff</a></li>
                                 
                                 <li><a href="/FOUNDATION/alumni/index.html">Alumni &amp; Foundation</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="chromeNav col-md-3 col-sm-3 col-xs-3 pull-right">
                              
                              <ul>
                                 
                                 <li>
                                    <a href="#" data-toggle="modal" data-target="#login">Login <i class="far fa-angle-down" aria-hidden="true"></i></a>
                                    
                                    <ul>
                                       
                                       <li class="fa-2x"><a href="https://atlas.valenciacollege.edu/"><i class="far fa-user-circle-o" aria-hidden="true">&nbsp;</i> Atlas</a></li>
                                       
                                       <li>
                                          <a href="https://valenciaalumniconnect.com/"><i class="far fa-users" aria-hidden="true">&nbsp;</i> Alumni Connect</a>
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="https://login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"><i class="valencia-font icon-o365" aria-hidden="true">&nbsp;</i> Office 365</a>
                                          
                                       </li>
                                       
                                       
                                       <li><a href="http://learn.valenciacollege.edu"><i class="far fa-graduation-cap" aria-hidden="true">&nbsp;</i>Valencia Online</a></li>
                                       
                                       <li><a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"><i class="far fa-envelope" aria-hidden="true">&nbsp;</i> Webmail</a></li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="https://search.valenciacollege.edu" id="search_bt"><i class="far fa-search" aria-hidden="true"></i><span>Search</span></a>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                     </nav>
                     
                  </div>
                  
               </div>
               
               
               <div class="header-site"></div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/advising-counseling/florida-virtual-campus-bad.pcf">©</a>
      </div>
   </body>
</html>