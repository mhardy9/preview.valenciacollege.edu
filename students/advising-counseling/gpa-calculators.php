<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advising &amp; Counseling  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/advising-counseling/gpa-calculators.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/advising-counseling/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Advising &amp; Counseling</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/advising-counseling/">Advising Counseling</a></li>
               <li>Advising &amp; Counseling </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>GPA Calculators</h2>
                        
                        
                        <div class="box_style_3">
                           
                           <form aria-label="Semester GPA Calculator" method="post" name="gpadata" id="gpadata">
                              
                              <h3>Semester GPA Calculator</h3>
                              
                              
                              <table class="table table">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <th>Course</th>
                                       
                                       <th>Credit Hours</th>
                                       
                                       <th>Grade</th>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 1" name="course1" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 1" name="credithours1" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 1" name="grade1">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 2" name="course2" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 2" name="credithours2" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 2" name="grade2">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 3" name="course3" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 3" name="credithours3" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 3" name="grade3">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 4" name="course4" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 4" name="credithours4" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 4" name="grade4">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 5" name="course5" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 5" name="credithours5" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 5" name="grade5">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 6" name="course6" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 6" name="credithours6" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 6" name="grade6">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input aria-label="Name of Course 7" name="course7" size="30" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <input aria-label="Credit Hours for Course 7" name="credithours7" size="5" type="text">
                                          
                                       </td>
                                       
                                       <td>
                                          <select aria-label="Credit Hours for Course 7" name="grade7">
                                             
                                             <option selected value="-1">Select</option>
                                             
                                             <option value="4.0">A</option>
                                             
                                             <option value="3.0">B</option>
                                             
                                             <option value="2.0">C</option>
                                             
                                             <option value="1.0">D</option>
                                             
                                             <option value="0.0">F</option>
                                             
                                             <option value="-1">W</option>
                                             
                                             <option value="-1">WP</option>
                                             
                                             <option value="0.0">WF</option>
                                             </select>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              
                              
                              <div class="text-center">
                                 <label>
                                    Your Calculated GPA:
                                    <input name="gparesult" readonly size="5" type="text">
                                    </label>
                                 
                              </div>
                              
                              
                              <div class="text-center">
                                 <input name="calculate" onclick="gpacalculator(this.form)" type="button" value="Calculate Your GPA" class="button-outline">
                                 <input aria-label="Clear Form" name="reset" type="reset" value="Clear" class="button-outline">
                                 
                              </div>
                              
                           </form>
                           
                        </div>
                        
                        
                        <p>Semester GPA (Grade Point Average) is calculated by dividing the total grade points
                           earned by the total number of semester credit hours.
                        </p>
                        
                        <blockquote>
                           
                           <p>Total Grade Points/Total Credit Hours = GPA</p>
                           
                        </blockquote>
                        
                        <h4>EXAMPLE</h4> Your Grade Point Average (GPA) is a weighted average of all your course work. This
                        means that a grade in a four-hour course will change your GPA more than the same grade
                        in a three-hour course. It also means that your GPA gets harder to change when you
                        have taken more courses. To calculate your estimated GPA, you would do the following:
                        
                        <p>For each course you multiply the course credit hours times the grade you receive to
                           get the "grade points" for that course. Your grades have the following numerical equivalents;
                           each A is 4, each B is 3, each C is 2, each D is 1, each F is 0 and each WF is 0.
                           W and WP do not receive grade points and do not have an effect on the GPA. Add up
                           the grade points for each course and divide the results by the total credit hours
                           to get your semester's GPA <span>(remember to exclude Ws and WPs from your total credit hours)</span>. For example, suppose you were taking the following courses:
                        </p>
                        
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th>Course</th>
                                 
                                 <th>Credit Hours</th>
                                 
                                 <th>Letter Grade</th>
                                 
                                 <th>Grade Points</th>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1. ENC 1102</td>
                                 
                                 <td>3</td>
                                 
                                 <td>A</td>
                                 
                                 <td>12</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>2. MAC 2311</td>
                                 
                                 <td>5</td>
                                 
                                 <td>C</td>
                                 
                                 <td>10</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3. PEM 1171</td>
                                 
                                 <td>2</td>
                                 
                                 <td>B</td>
                                 
                                 <td>6</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>4. SPC 1600</td>
                                 
                                 <td>3</td>
                                 
                                 <td>D</td>
                                 
                                 <td>3</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td><strong>Total</strong></td>
                                 
                                 <td><strong>13</strong></td>
                                 
                                 <td>&nbsp;</td>
                                 
                                 <td><strong>31</strong></td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        <p>Your grade point total for the term (31) is divided by your total credit hours (13)
                           which equals your GPA (2.38).
                        </p>
                        
                        <p>If you want to raise your GPA, an additional calculator below helps you determine
                           how many credit hours and what grade average you will need to raise your current GPA.
                        </p>
                        
                        
                        
                        <h3>Raise Your GPA</h3>
                        
                        <form action="none" name="GPA" id="GPA">
                           
                           <div class="box_style_3">
                              
                              <table class="table table">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <td><strong>In order to answer these questions you need to supply the following information:</strong></td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Current Cumulative GPA:
                                          <input aria-label="Current Cumulative GPA" name="CGPA" size="4" type="TEXT" value="0">
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>Total Number of Attempted Credit Hours to Date:
                                          <input aria-label="Total Number of Attempted Credit Hours to Date" name="TTD" size="3" type="TEXT" value="0">
                                          <br>
                                          <span>(<strong>DO NOT INCLUDE</strong> current term hours and Ws and WPs)</span> 
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <br> Enter the credit hours and expected grade for (up to six) courses that you are presently
                                          taking below and press CALCULATE to find your predicted new cumulative GPA at the
                                          end of this term.
                                          <br>
                                          <br>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <strong>Course 1:</strong>
                                          
                                          <table class="table table">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td> Credit Hours
                                                      <select aria-label="Credit Hours for Course 1" name="cach">
                                                         
                                                         <option selected value="0">0</option>
                                                         
                                                         <option value="1">1</option>
                                                         
                                                         <option value="2">2</option>
                                                         
                                                         <option value="3">3</option>
                                                         
                                                         <option value="4">4</option>
                                                         
                                                         <option value="5">5</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                   <td>And your anticipated grade:
                                                      <select aria-label="Anticipated Grade for Course 1" name="cagr">
                                                         
                                                         <option value="0">WF</option>
                                                         
                                                         <option value="0">F</option>
                                                         
                                                         <option value="1">D</option>
                                                         
                                                         <option value="2">C</option>
                                                         
                                                         <option value="3">B</option>
                                                         
                                                         <option selected value="4">A</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <strong>Course 2:</strong>
                                          
                                          <table class="table table">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td> Credit Hours
                                                      <select aria-label="Credit Hours for Course 2" name="cbch">
                                                         
                                                         <option selected value="0">0</option>
                                                         
                                                         <option value="1">1</option>
                                                         
                                                         <option value="2">2</option>
                                                         
                                                         <option value="3">3</option>
                                                         
                                                         <option value="4">4</option>
                                                         
                                                         <option value="5">5</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                   <td>And your anticipated grade:
                                                      <select aria-label="Anticipated Grade for Course 2" name="cbgr">
                                                         
                                                         <option value="0">WF</option>
                                                         
                                                         <option value="0">F</option>
                                                         
                                                         <option value="1">D</option>
                                                         
                                                         <option value="2">C</option>
                                                         
                                                         <option value="3">B</option>
                                                         
                                                         <option selected value="4">A</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <strong>Course 3:</strong>
                                          
                                          <table class="table table">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td> Credit Hours
                                                      <select aria-label="Credit Hours for Course 3" name="ccch">
                                                         
                                                         <option selected value="0">0</option>
                                                         
                                                         <option value="1">1</option>
                                                         
                                                         <option value="2">2</option>
                                                         
                                                         <option value="3">3</option>
                                                         
                                                         <option value="4">4</option>
                                                         
                                                         <option value="5">5</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                   <td>And your anticipated grade:
                                                      <select aria-label="Anticipated Grade for Course 3" name="ccgr">
                                                         
                                                         <option value="0">WF</option>
                                                         
                                                         <option value="0">F</option>
                                                         
                                                         <option value="1">D</option>
                                                         
                                                         <option value="2">C</option>
                                                         
                                                         <option value="3">B</option>
                                                         
                                                         <option selected value="4">A</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <strong>Course 4:</strong>
                                          
                                          <table class="table table">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td> Credit Hours
                                                      <select aria-label="Credit Hours for Course 4" name="cdch">
                                                         
                                                         <option selected value="0">0</option>
                                                         
                                                         <option value="1">1</option>
                                                         
                                                         <option value="2">2</option>
                                                         
                                                         <option value="3">3</option>
                                                         
                                                         <option value="4">4</option>
                                                         
                                                         <option value="5">5</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                   <td>And your anticipated grade:
                                                      <select aria-label="Anticipated Grade for Course 4" name="cdgr">
                                                         
                                                         <option value="0">WF</option>
                                                         
                                                         <option value="0">F</option>
                                                         
                                                         <option value="1">D</option>
                                                         
                                                         <option value="2">C</option>
                                                         
                                                         <option value="3">B</option>
                                                         
                                                         <option selected value="4">A</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <strong>Course 5:</strong>
                                          
                                          <table class="table table">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td> Credit Hours
                                                      <select aria-label="Credit Hours for Course 5" name="cech">
                                                         
                                                         <option selected value="0">0</option>
                                                         
                                                         <option value="1">1</option>
                                                         
                                                         <option value="2">2</option>
                                                         
                                                         <option value="3">3</option>
                                                         
                                                         <option value="4">4</option>
                                                         
                                                         <option value="5">5</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                   <td>And your anticipated grade:
                                                      <select aria-label="Anticipated Grade for Course 5" name="cegr">
                                                         
                                                         <option value="0">WF</option>
                                                         
                                                         <option value="0">F</option>
                                                         
                                                         <option value="1">D</option>
                                                         
                                                         <option value="2">C</option>
                                                         
                                                         <option value="3">B</option>
                                                         
                                                         <option selected value="4">A</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <strong>Course 6:</strong>
                                          
                                          <table class="table table">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td> Credit Hours
                                                      <select aria-label="Credit Hours for Course 6" name="cfch">
                                                         
                                                         <option selected value="0">0</option>
                                                         
                                                         <option value="1">1</option>
                                                         
                                                         <option value="2">2</option>
                                                         
                                                         <option value="3">3</option>
                                                         
                                                         <option value="4">4</option>
                                                         
                                                         <option value="5">5</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                   <td>And your anticipated grade:
                                                      <select aria-label="Anticipated Grade for Course 6" name="cfgr">
                                                         
                                                         <option value="0">WF</option>
                                                         
                                                         <option value="0">F</option>
                                                         
                                                         <option value="1">D</option>
                                                         
                                                         <option value="2">C</option>
                                                         
                                                         <option value="3">B</option>
                                                         
                                                         <option selected value="4">A</option>
                                                         </select>
                                                      
                                                   </td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <label>
                                             Calculated Results:
                                             <input name="result" readonly size="5" type="text">
                                             </label>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input onclick="compute(this.form)" type="button" value="Calculate GPA" class="button-outline">
                                          <input aria-label="Reset Form" type="RESET" value="Reset Form" class="button-outline">
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              
                              
                              <h4>Credit Hours Needed to Raise GPA</h4>
                              
                              <table class="table table">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <td>I would like to raise my cumulative GPA to
                                          <input aria-label="Desired GPA" name="CGPA3" size="4" type="text" value="0">
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>If I can maintain a
                                          <input aria-label="Maintain GPA" name="PGPA" size="4" type="text" value="4.0"> average from now on,(e.g. 3.5, 2.5,...)
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>how many credit hours will it take to raise my GPA to this new level?</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input onclick="compute2(this.form)" type="button" value="Calculate" class="button-outline">
                                          <label>
                                             Credit Hours Needed to Raise GPA
                                             <input name="result2" readonly size="8" type="text">
                                             </label>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              
                              
                              <h4>Average Needed to Raise GPA</h4>
                              
                              <table class="table table">
                                 
                                 <tbody>
                                    
                                    <tr>
                                       
                                       <td>I am currently taking
                                          <input aria-label="Credit Hours Currently Taking" name="PGPAB" size="4" type="text" value="0"> credit hours.
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>If I want to raise my cumulative GPA to
                                          <input aria-label="Want to Raise GPA to" name="CGPA3B" size="4" type="text" value="0"> at the end of this term,
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>what will I have to average on my current courses to reach this goal?</td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <td>
                                          <input onclick="compute3(this.form)" type="button" value="Calculate" class="button-outline">
                                          <label>
                                             Average needed in Current Courses
                                             <input name="result3" readonly size="8" type="text">
                                             </label>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </tbody>
                                 
                              </table>
                              
                           </div>
                           
                        </form>
                        
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/advising-counseling/contact.cfm">Contact an Advisor</a></p>
                        
                        <p><a href="http://valenciacollege.edu/support/howto/atlas-advising-chat-setup.cfm">Advising Chat</a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Location, Contact &amp; Hours</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tr">
                              <a href="mailto:advising@valenciacollege.edu">advising@valenciacollege.edu</a>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Monday - Thursday: 8am to 6pm
                              <br>Friday: 9am to 5pm
                              <br>Friday (Summer Hours): 9am to 12pm
                              
                           </div>
                           
                        </div>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tr">
                              <strong>East Campus Advising and Counseling </strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 5, Rm 210
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Lake Nona Campus Advising and Counseling </strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 1, Rm 149
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Osceola Campus Advising and Counseling</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 2, Rm 140
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Poinciana Campus Student Services (Advising)</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Room 101
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>West Campus Advising and Counseling</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              SSB, Rm 110
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Winter Park Campus Advising and Counseling</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 1, Rm 206
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/advising-counseling/gpa-calculators.pcf">©</a>
      </div>
   </body>
</html>