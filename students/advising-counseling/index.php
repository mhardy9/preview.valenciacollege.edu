<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advising &amp; Counseling  | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/advising-counseling/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/advising-counseling/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Advising &amp; Counseling</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Advising Counseling</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Advising &amp; Counseling</h2>
                        
                        
                        <h3>Mission Statement</h3>
                        
                        
                        <p>Advising and Counseling is system of shared responsibilities between students and
                           the College that results in social and academic integration, education and career
                           plans, and the acquisition of study and life skills.
                        </p>
                        
                        
                        <h3>Advisors and Counselors</h3>
                        
                        
                        <p>Advisors and counselors are available on every campus to meet with students on a walk-in
                           basis to provide advising on career and academic planning, successful transition to
                           the university, and how to enhance college success skills.
                        </p>
                        
                     </div>
                     
                     
                     <aside class="col-md-3">
                        
                        <p><a href="http://net4.valenciacollege.edu/forms/advising-counseling/contact.cfm">Contact an Advisor</a></p>
                        
                        <p><a href="http://valenciacollege.edu/support/howto/atlas-advising-chat-setup.cfm">Advising Chat</a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Location, Contact &amp; Hours</h3>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tr">
                              <a href="mailto:advising@valenciacollege.edu">advising@valenciacollege.edu</a>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Monday - Thursday: 8am to 6pm
                              <br>Friday: 9am to 5pm
                              <br>Friday (Summer Hours): 9am to 12pm
                              
                           </div>
                           
                        </div>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tr">
                              <strong>East Campus Advising and Counseling </strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 5, Rm 210
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Lake Nona Campus Advising and Counseling </strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 1, Rm 149
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Osceola Campus Advising and Counseling</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 2, Rm 140
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Poinciana Campus Student Services (Advising)</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Room 101
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>West Campus Advising and Counseling</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              SSB, Rm 110
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              <strong>Winter Park Campus Advising and Counseling</strong>
                              
                           </div>
                           
                           <div data-old-tag="tr">
                              Bldg 1, Rm 206
                              
                           </div>
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/advising-counseling/index.pcf">©</a>
      </div>
   </body>
</html>