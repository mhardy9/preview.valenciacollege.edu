<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Sexual Harassment / Sexual Assault (Title IX)  | Valencia College</title>
      <meta name="Description" content="Sexual Harassment / Sexual Assault (Title IX) | Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, dispute, sexual, harassment, assault">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/sexual-harassment-assault/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/sexual-harassment-assault/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li>Sexual Harassment Assault</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Sexual Harassment / Sexual Assault (Title IX)</h2>
                        
                        
                        <p>Valencia College, in accordance with Title IX of the  Educational Amendments of 1972,
                           is committed to ensuring that current and  future students, faculty, and staff are
                           not discriminated against and ensures a  learning environment free from all forms
                           of harassment, including sexual  harassment, discrimination, or intimidation.<br>
                           Valencia College prohibits sex discrimination and sexual  misconduct, to include sexual
                           harassment and sexual violence. Sexual harassment  is a term with specific legal implications.
                           Sexual harassment involves a  variety of behaviors such as unwelcome sexual advances,
                           requests for sexual  favors, and other verbal and/or physical harassment. Sexual violence
                           refers to  conduct of a sexual nature that occurs against a person’s will or without
                           a  person’s explicit consent to include circumstances where consent cannot be  given.
                           
                        </p>
                        
                        <p> The College also prohibits gender-based harassment. Acts of  a verbal, nonverbal,
                           or physically intimidating nature centered on sex and/or  sex-stereotyping will not
                           be tolerated in the Valencia community. 
                        </p>
                        
                        <p> We strongly encourage all members of our College community  to seek support for and
                           report all sex discrimination and sexual misconduct to  Campus Security, the Title
                           IX Coordinator, or any named Title IX Deputy  Coordinator. The roles and responsibilities
                           of these individuals are to assist  in removing the misconduct, preventing its recurrence,
                           and addressing the  effects. All complaints, regardless of where reported, will be
                           relayed to and  evaluated by the College’s Title IX Coordinator. 
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        
                        <h3 style="font-weight: bold">Valencia Campus Security</h3>
                        
                        <table class="table ">
                           
                           <tr>
                              
                              <td>West Campus</td>
                              
                              <td>407-582-1000</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>East Campus</td>
                              
                              <td>407-582-2000</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Lake Nona Campus</td>
                              
                              <td>407-582-7000</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Osceola Campus</td>
                              
                              <td>407-582-4000</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Winter Park Campus</td>
                              
                              <td>407-582-6000</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>District Office</td>
                              
                              <td>407-582-3000</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Criminal Justice Institute</td>
                              
                              <td>407-582-8000</td>
                              
                           </tr>
                           
                        </table>
                        
                        
                        <h3 style="font-weight: bold">Title IX  Coordinator and Deputy Coordinators</h3>
                        
                        <table class="table ">
                           
                           <tr>
                              
                              <th colspan="4" style="background-color: #e6e7e9">Title IX Coordinator</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Title IX Coordinator </td>
                              
                              <td>
                                 <a href="../../../contact/Search_Detail2.cfm-ID=rkane8.html">Ryan Kane</a>
                                 
                              </td>
                              
                              <td>Title IX/EO  Officer</td>
                              
                              <td>407-582-3421<br>
                                 District Office
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator&nbsp;</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=wtaylor17.html">Ben Taylor</a></td>
                              
                              <td>Assistant Director, Title IX/EO</td>
                              
                              <td>407-582-3454 District  Office</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator&nbsp;</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=cpostell2.html">Chanda Postell</a></td>
                              
                              <td>Assistant Director, Title IX/EO</td>
                              
                              <td>407-582-3422 District Office</td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th colspan="4" style="background-color: #e6e7e9">East, Winter Park, and School of Public Safety</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=jcorderman.html">Julie Corderman</a></td>
                              
                              <td>Director, Student Services</td>
                              
                              <td>407-582-6868<br>
                                 Winter Park 210A
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td>
                                 <a href="../../../contact/Search_Detail2.cfm-ID=jsarrubbo.html">Joe Sarrubbo</a>
                                 
                              </td>
                              
                              <td>Dean of Students </td>
                              
                              <td>407-582-2586<br>
                                 East 5-210L
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=rridore.html">Ruth Ridore</a></td>
                              
                              <td>Campus Director OD/HR, EAC, WPK, and SPS </td>
                              
                              <td>407-582-2760<br>
                                 East 7-110B
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=cevans53.html">Corey Evans</a></td>
                              
                              <td>Coord, Student Conduct &amp; Academic Success</td>
                              
                              <td>407-582-2346 <br>
                                 East 5-210Q
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=rpigman.html">Rob Pigman</a></td>
                              
                              <td>Assistant Director, East, SPS, WPC</td>
                              
                              <td>407-582-2346 <br>
                                 East 5-220
                              </td>
                              
                           </tr>          
                           
                           <tr>
                              
                              <th colspan="4" style="background-color: #e6e7e9">Lake Nona and Osceola</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=msmith225.html">Melinda Smith</a></td>
                              
                              <td>Director, Student Services</td>
                              
                              <td>407-582-7780<br>
                                 Lake Nona 1-149C
                              </td>
                              
                           </tr>          
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=jszentmiklosi.html">Jill Szentmiklosi </a></td>
                              
                              <td>Dean of Students</td>
                              
                              <td>407-582-4142<br>
                                 Osceola 2-140D
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=ljohnston.html">Lora Lee Johnston</a></td>
                              
                              <td>Coord, Student Conduct &amp; Academic Success</td>
                              
                              <td>321-682-4093<br>
                                 OSC 2-105
                              </td>
                              
                           </tr>          
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=lsuarez.html">Lisandra Suarez Lopez</a></td>
                              
                              <td>Campus Director OD/HR, OSC &amp;LNC</td>
                              
                              <td>321-682-4710<br>
                                 OSC 1-141C
                              </td>
                              
                           </tr>
                           
                           
                           <tr>
                              
                              <td>Deputy Coordinator </td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=mellis33.html">Misty Ellis Peasley</a></td>
                              
                              <td>Assistant Director, Security Osceola/LNC </td>
                              
                              <td>407-582-1047 <br>
                                 Osceola 2-109B
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <th colspan="4" style="background-color: #e6e7e9">West and District Office</th>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=msever.html">Michelle Sever</a></td>
                              
                              <td>Director, HR Policy &amp; Employee Relations</td>
                              
                              <td>407-582-8256<br>
                                 District Office 267
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=lbojalad1.html">Lauren Bojalad</a></td>
                              
                              <td>Assistant Director, Employee Relations</td>
                              
                              <td>407-582-8125<br>
                                 DO 269
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=cmcknight5.html">Carla McKnight</a></td>
                              
                              <td>Campus Director OD/HR</td>
                              
                              <td>407-582-1756<br> 
                                 West 6-306B
                              </td>
                              
                           </tr>          
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=aking3.html">Art King</a></td>
                              
                              <td>Asst Dir, Security</td>
                              
                              <td>407-582-1327&nbsp;<br> 
                                 West SSB 170
                              </td>
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=blion.html">Ben  Lion</a></td>
                              
                              <td>Dean of  Students</td>
                              
                              <td>407-582-1388<br>
                                 SSB 110
                              </td>
                              
                              
                           </tr>
                           
                           <tr>
                              
                              <td>Deputy Coordinator</td>
                              
                              <td><a href="../../../contact/Search_Detail2.cfm-ID=jsininger.html">Jason Sininger</a></td>
                              
                              <td>Coord Student Conduct &amp; Academic  Success</td>
                              
                              <td>407-582-1557<br>
                                 SSB 133
                              </td>
                              
                           </tr>
                           
                        </table>
                        
                        <hr class="styled_2">
                        
                        
                        <p>For additional support, you may reach out to BayCare Student  Assistance program at
                           1-800-878-5470 for support, advice, or someone to listen.  These services are available
                           24 hours a day, seven days a week. 
                        </p>
                        
                        
                        <h3 style="font-weight: bold">BayCare Student  Assistance Program</h3>
                        
                        <p>BayCare is a free service available 24 hours a day, seven  days a week to Valencia
                           students. For support, advice, or someone to listen  please contact the BayCare program
                           at 1-800-878-5470.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3 style="font-weight: bold">Victim Service Center of Central Florida</h3>
                        
                        <p>Victim Service Center  provides &nbsp;individualized services and resources to victims
                           of sexual  assault, violent crime and traumatic circumstances, through crisis response,
                           advocacy, therapy and community awareness.
                        </p>
                        
                        <p>407-254-9415<br>
                           <a href="http://www.victimservicecenter.com/" target="_blank">http://www.victimservicecenter.com/</a></p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3 style="font-weight: bold">Harbor House of Central Florida</h3>
                        
                        <p>  Harbor House works to prevent and eliminate domestic abuse  in Central Florida by
                           providing critical life-saving services to survivors,  implementing and advancing
                           best practices, and educating and engaging the  community in a united front.
                        </p>
                        
                        <p>407-886-2856<br>
                           <a href="http://www.harborhousefl.com">www.harborhousefl.com</a></p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3 style="font-weight: bold">Help Now of Osceola</h3>
                        
                        <p>Help Now, Inc. provides temporary safe shelter for survivors  of domestic abuse facing
                           imminent danger. Help Now is committed to supporting  individuals as they establish
                           violence free living. Our shelter exists in an  undisclosed location, and all visitors,
                           residents, and staff must sign and  uphold a confidentiality statement preventing
                           them from disclosing the location  of the shelter. Help Now will neither confirm nor
                           deny whether any person is  residing at our shelter. <br>
                           <br>
                           Help Now Domestic Abuse Advocates provide quality domestic abuse advocacy to  shelter
                           clients and their children, as well as to individuals whom call the  Domestic Abuse
                           Crisis Line. The goal of the advocate is to empower survivors to  become comfortable
                           with making decisions that are right for them.
                        </p>
                        
                        <p>407-874-8562<br>
                           <a href="http://www.helpnowshelter.org">www.helpnowshelter.org</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/sexual-harassment-assault/index.pcf">©</a>
      </div>
   </body>
</html>