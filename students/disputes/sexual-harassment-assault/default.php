<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Dispute Resolution | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/sexual-harassment-assault/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/sexual-harassment-assault/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li><a href="/students/disputes/sexual-harassment-assault/">Sexual Harassment Assault</a></li>
               <li>Student Dispute Resolution</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Sexual Harassment / Sexual Assault (Title IX)</h2>
                        
                        
                        <p>Valencia College, in accordance with Title IX of the  Educational Amendments of 1972,
                           is committed to ensuring that current and  future students, faculty, and staff are
                           not discriminated against and ensures a  learning environment free from all forms
                           of harassment, including sexual  harassment, discrimination, or intimidation.<br>
                           Valencia College prohibits sex discrimination and sexual  misconduct, to include sexual
                           harassment and sexual violence. Sexual harassment  is a term with specific legal implications.
                           Sexual harassment involves a  variety of behaviors such as unwelcome sexual advances,
                           requests for sexual  favors, and other verbal and/or physical harassment. Sexual violence
                           refers to  conduct of a sexual nature that occurs against a person’s will or without
                           a  person’s explicit consent to include circumstances where consent cannot be  given.
                           
                        </p>
                        
                        <p> The College also prohibits gender-based harassment. Acts of  a verbal, nonverbal,
                           or physically intimidating nature centered on sex and/or  sex-stereotyping will not
                           be tolerated in the Valencia community. 
                        </p>
                        
                        <p> We strongly encourage all members of our College community  to seek support for and
                           report all sex discrimination and sexual misconduct to  Campus Security, the Title
                           IX Coordinator, or any named Title IX Deputy  Coordinator. The roles and responsibilities
                           of these individuals are to assist  in removing the misconduct, preventing its recurrence,
                           and addressing the  effects. All complaints, regardless of where reported, will be
                           relayed to and  evaluated by the College’s Title IX Coordinator. 
                        </p>
                        
                        <h3>Valencia Campus Security</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">West Campus</div>
                                 
                                 <div data-old-tag="td">407-582-1000</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">East Campus</div>
                                 
                                 <div data-old-tag="td">407-582-2000</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Lake Nona Campus</div>
                                 
                                 <div data-old-tag="td">407-582-7000</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Osceola Campus</div>
                                 
                                 <div data-old-tag="td">407-582-4000</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Winter Park Campus</div>
                                 
                                 <div data-old-tag="td">407-582-6000</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">District Office</div>
                                 
                                 <div data-old-tag="td">407-582-3000</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Criminal Justice Institute</div>
                                 
                                 <div data-old-tag="td">407-582-8000</div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <h3> Title IX  Coordinator and Deputy Coordinators</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Title IX Coordinator</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Title IX Coordinator </div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=rkane8.html">Ryan Kane</a></div>
                                 
                                 <div data-old-tag="td">Title IX/EO  Officer</div>
                                 
                                 <div data-old-tag="td">407-582-3421<br>
                                    District Office
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator&nbsp;</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=wtaylor17.html">Ben Taylor</a></div>
                                 
                                 <div data-old-tag="td">Assistant Director, Title IX/EO</div>
                                 
                                 <div data-old-tag="td">407-582-3454 District  Office</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator&nbsp;</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=cpostell2.html">Chanda Postell</a></div>
                                 
                                 <div data-old-tag="td">Assistant Director, Title IX/EO</div>
                                 
                                 <div data-old-tag="td">407-582-3422 District Office</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">East, Winter Park, and School of Public Safety</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=jcorderman.html">Julie Corderman</a></div>
                                 
                                 <div data-old-tag="td">Director, Student Services</div>
                                 
                                 <div data-old-tag="td">407-582-6868<br>
                                    Winter Park 210A
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=jsarrubbo.html">Joe Sarrubbo</a></div>
                                 
                                 <div data-old-tag="td">Dean of Students </div>
                                 
                                 <div data-old-tag="td">407-582-2586<br>
                                    East 5-210L
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=rridore.html">Ruth Ridore</a></div>
                                 
                                 <div data-old-tag="td">Campus Director OD/HR, EAC, WPK, and SPS </div>
                                 
                                 <div data-old-tag="td">407-582-2760<br>
                                    East 7-110B
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=cevans53.html">Corey Evans</a></div>
                                 
                                 <div data-old-tag="td">Coord, Student Conduct &amp; Academic Success</div>
                                 
                                 <div data-old-tag="td">407-582-2346 <br>
                                    East 5-210Q
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=rpigman.html">Rob Pigman</a></div>
                                 
                                 <div data-old-tag="td">Assistant Director, East, SPS, WPC</div>
                                 
                                 <div data-old-tag="td">407-582-2346 <br>
                                    East 5-220
                                 </div>
                                 
                              </div>          
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Lake Nona and Osceola</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=msmith225.html">Melinda Smith</a></div>
                                 
                                 <div data-old-tag="td">Director, Student Services</div>
                                 
                                 <div data-old-tag="td">407-582-7780<br>
                                    Lake Nona 1-149C
                                 </div>
                                 
                              </div>          
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=jszentmiklosi.html">Jill Szentmiklosi </a></div>
                                 
                                 <div data-old-tag="td">Dean of Students</div>
                                 
                                 <div data-old-tag="td">407-582-4142<br>
                                    Osceola 2-140D
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=ljohnston.html">Lora Lee Johnston</a></div>
                                 
                                 <div data-old-tag="td">Coord, Student Conduct &amp; Academic Success</div>
                                 
                                 <div data-old-tag="td">321-682-4093<br>
                                    OSC 2-105
                                 </div>
                                 
                              </div>          
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=lsuarez.html">Lisandra Suarez Lopez</a></div>
                                 
                                 <div data-old-tag="td">Campus Director OD/HR, OSC &amp;LNC</div>
                                 
                                 <div data-old-tag="td">321-682-4710<br>
                                    OSC 1-141C
                                 </div>
                                 
                              </div>
                              
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator </div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=mellis33.html">Misty Ellis Peasley</a></div>
                                 
                                 <div data-old-tag="td">Assistant Director, Security Osceola/LNC </div>
                                 
                                 <div data-old-tag="td">407-582-1047 <br>
                                    Osceola 2-109B
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">West and District Office</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=msever.html">Michelle Sever</a></div>
                                 
                                 <div data-old-tag="td">Director, HR Policy &amp; Employee Relations</div>
                                 
                                 <div data-old-tag="td">407-582-8256<br>
                                    District Office 267
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=lbojalad1.html">Lauren Bojalad</a></div>
                                 
                                 <div data-old-tag="td">Assistant Director, Employee Relations</div>
                                 
                                 <div data-old-tag="td">407-582-8125<br>
                                    DO 269
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=cmcknight5.html">Carla McKnight</a></div>
                                 
                                 <div data-old-tag="td">Campus Director OD/HR</div>
                                 
                                 <div data-old-tag="td">407-582-1756<br> 
                                    West 6-306B
                                 </div>
                                 
                              </div>          
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=aking3.html">Art King</a></div>
                                 
                                 <div data-old-tag="td">Asst Dir, Security</div>
                                 
                                 <div data-old-tag="td">407-582-1327&nbsp;<br> 
                                    West SSB 170
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=blion.html">Ben  Lion</a></div>
                                 
                                 <div data-old-tag="td">Dean of  Students</div>
                                 
                                 <div data-old-tag="td">407-582-1388<br>
                                    SSB 110
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Deputy Coordinator</div>
                                 
                                 <div data-old-tag="td"><a href="../../../contact/Search_Detail2.cfm-ID=jsininger.html">Jason Sininger</a></div>
                                 
                                 <div data-old-tag="td">Coord Student Conduct &amp; Academic  Success</div>
                                 
                                 <div data-old-tag="td">407-582-1557<br>
                                    SSB 133
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>For additional support, you may reach out to BayCare Student  Assistance program at
                           1-800-878-5470 for support, advice, or someone to listen.  These services are available
                           24 hours a day, seven days a week. 
                        </p>
                        
                        <h3>BayCare Student  Assistance Program</h3>
                        
                        <p>BayCare is a free service available 24 hours a day, seven  days a week to Valencia
                           students. For support, advice, or someone to listen  please contact the BayCare program
                           at 1-800-878-5470.
                        </p>
                        
                        
                        <h3>Victim Service Center of Central Florida</h3>
                        
                        <p>Victim Service Center  provides &nbsp;individualized services and resources to victims
                           of sexual  assault, violent crime and traumatic circumstances, through crisis response,
                           advocacy, therapy and community awareness.
                        </p>
                        
                        <p>407-254-9415<br>
                           <a href="http://www.victimservicecenter.com/" target="_blank">http://www.victimservicecenter.com/</a></p>
                        
                        
                        <h3>Harbor House of Central Florida</h3>
                        
                        <p>  Harbor House works to prevent and eliminate domestic abuse  in Central Florida by
                           providing critical life-saving services to survivors,  implementing and advancing
                           best practices, and educating and engaging the  community in a united front.
                        </p>
                        
                        <p>407-886-2856<br>
                           <a href="http://www.harborhousefl.com">www.harborhousefl.com</a></p>
                        
                        
                        <h3>Help Now of Osceola</h3>
                        
                        <p>  Help Now, Inc. provides temporary safe shelter for survivors  of domestic abuse
                           facing imminent danger. Help Now is committed to supporting  individuals as they establish
                           violence free living. Our shelter exists in an  undisclosed location, and all visitors,
                           residents, and staff must sign and  uphold a confidentiality statement preventing
                           them from disclosing the location  of the shelter. Help Now will neither confirm nor
                           deny whether any person is  residing at our shelter. <br>
                           <br>
                           Help Now Domestic Abuse Advocates provide quality domestic abuse advocacy to  shelter
                           clients and their children, as well as to individuals whom call the  Domestic Abuse
                           Crisis Line. The goal of the advocate is to empower survivors to  become comfortable
                           with making decisions that are right for them.
                        </p>
                        
                        <p>407-874-8562<br>
                           <a href="http://www.helpnowshelter.org">www.helpnowshelter.org</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           <a href="http://net4.valenciacollege.edu/forms/students/disputes/civil-rights-discrimination/form.cfm" target="_blank">EXPRESS YOUR CONCERN</a>
                           <a href="default.html">LEARN MORE</a>
                           <a href="../../../about/general-counsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">POLICY</a>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/sexual-harassment-assault/default.pcf">©</a>
      </div>
   </body>
</html>