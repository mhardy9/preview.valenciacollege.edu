
<ul>
	<li><a href="../index.php">Student Dispute Resolution</a></li>
	<li class="submenu megamenu">
		<a class="show-submenu-mega" href="javascript:void(0);">Resources <i aria-hidden="true" class="far fa-angle-down"></i></a>

		<ul>
			<li><a href="../index.php">Student Dispute Resolution</a></li>
			<li><a href="index.php">Final Grade Disputes</a></li>
			<li><a href="../academic/index.php">Non Final Grade and Other Academic Disputes</a></li>
			<li><a href="../administrative/index.php">Student Administrative Complaint Resolution</a></li>
			<li><a href="../civil-rights-discrimination/index.php">Civil Rights Discrimination</a></li>
			<li><a href="../sexual-harassment-assault/index.php">Sexual Harassment / Sexual Assault (Title IX)</a></li>
			<li><a href="../conduct/index.php">Student Code of Conduct</a></li>
		</ul>

	</li>
</ul>

