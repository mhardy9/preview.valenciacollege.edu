<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Dispute Resolution | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/civil-rights-discrimination/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/civil-rights-discrimination/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li><a href="/students/disputes/civil-rights-discrimination/">Civil Rights Discrimination</a></li>
               <li>Student Dispute Resolution</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Civil Rights Discrimination</h2>
                        
                        
                        <p>Valencia is  committed to ensuring equal access and equal opportunity for students
                           and  staff. The Office of the Vice President for Organizational Development and  Human
                           Resources operates to assist Valencia in fulfilling its commitment to  provide equal
                           educational opportunities to its diverse student population. The  Organizational Development
                           and Human Resources Staff provides comprehensive and  inclusive programs and services
                           to benefit Valencia’s student body and the  entire community and develops training
                           and support programs to enhance staff  and faculty recruitment, growth and advancement.
                           This office also investigates  and seeks solutions to concerns regarding harassment
                           and discrimination within  the college. Finally, the Office of Organizational Development
                           and Human  Resources works with the college staff and various community partners to
                           achieve diversity within the institution’s workforce.
                        </p>
                        
                        <p>Valencia College is an equal opportunity institution. We  provide equal opportunity
                           for employment and educational services to all  individuals as it relates to admission
                           to the College or to programs, any aid,  benefit, or service to students or wages
                           and other terms, conditions or  privileges of employment, without regard to race,
                           color, ethnicity, national  origin, age, religion, disability, marital status, gender,
                           sexual orientation,  genetic information and any other factor prohibited under applicable
                           federal,  state, and local civil rights laws, rules and regulations.
                        </p>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <div>
                           <a href="http://net4.valenciacollege.edu/forms/students/disputes/civil-rights-discrimination/form.cfm" target="_blank">EXPRESS YOUR CONCERN</a>
                           <a href="default.html">LEARN MORE</a>
                           <a href="../../../about/general-counsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">POLICY</a>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/civil-rights-discrimination/default.pcf">©</a>
      </div>
   </body>
</html>