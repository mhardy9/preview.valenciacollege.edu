<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Dispute Resolution | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational, student, disputes, conduct">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/conduct/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/conduct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li><a href="/students/disputes/conduct/">Conduct</a></li>
               <li>Student Dispute Resolution</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3><strong>Student Conduct</strong></h3>
                        
                        
                        
                        <p>Valencia College is dedicated to the  advancement of knowledge and learning and to
                           the development of responsible  personal and social conduct. The primary purpose for
                           the maintenance of  discipline in the College setting is to support a civil environment
                           conducive  to learning and inquiry.<a href=""></a></p>
                        
                        
                        <hr class="styled_2">           
                        
                        
                        <h3><strong>Campus Civility</strong></h3>
                        
                        <p>Civility is being polite, courteous, and considerate towards  others. More likely
                           than not, you will encounter individuals that have  differing perceptions. A civil
                           person should recognize differences and welcome  the chance to learn instead of being
                           judgmental and attacking individuals based  on their differences. Remember, maintaining
                           civility on campus is an individual  effort that benefits the whole community.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Reporting  an Incident</strong></h3>
                        
                        <p> Have you witnessed or been affected by a violation of the Student  Code of Conduct?
                           To file a complaint, you may go to the Security Services Office or file a report online
                           through this link:
                        </p>
                        
                        <p><a href="http://valenciacollege.edu/security/incidentReporting.cfm" class="button color1 width_120 inline narrow">Report an Incident</a></p>
                        
                        
                        <hr class="styled_2">            
                        
                        
                        <h3><strong>The Student Conduct Review Process</strong></h3>
                        
                        <p>Once a complaint is filed, the Dean of Students or designee  will review the complaint
                           and consult with relevant parties regarding the  incident. The Dean of Students or
                           designee will then follow-up with the Student  Conduct Review Process as outlined
                           in the <a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>.
                        </p>
                        
                        <p>If the Dean of Students or designee determines that a student is  in violation of
                           the Student Code, a formal charge will be sent to the student's  Atlas e-mail. Included
                           in the charge will be a request to meet in a preliminary  meeting to discuss the incident
                           and determine responsibility. Based on the  severity of the alleged violation and
                           the information gathered in the  preliminary meeting, the Dean of Students or designee
                           may resolve the matter  immediately or forward the case to mediation or a disciplinary
                           conference.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Student  and Victim Protections</strong></h3>
                        
                        <p>In order to ensure fairness during the Student Conduct Review  Process, Valencia College
                           provides certain protections/rights to charged  students and victims. For a complete
                           list of these protections, please refer to  the <a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>.
                        </p>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/conduct/default.pcf">©</a>
      </div>
   </body>
</html>