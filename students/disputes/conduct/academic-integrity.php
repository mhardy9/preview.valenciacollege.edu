<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Academic Integrity  | Valencia College</title>
      <meta name="Description" content="Academic Integrity | Student Conduct | Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, disputes, conduct, academic, integrity">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/conduct/academic-integrity.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/conduct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		Student Conduct
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li><a href="/students/disputes/conduct/">Conduct</a></li>
               <li>Academic Integrity </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h3><strong>Academic Integrity</strong></h3>
                        
                        
                        
                        <p>All forms of academic dishonesty are prohibited at Valencia College. Academic dishonesty
                           includes, but is not limited to, acts or attempted acts of plagiarism, cheating, furnishing
                           false information, forgery, alteration or misuse of documents, misconduct during a
                           testing situation, facilitating academic dishonesty, and misuse of identification
                           with intent to defraud or deceive.
                        </p>
                        
                        <p>Students may be subject to both the&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct&nbsp;</a>and academic sanctions as determined in the academic judgment of the professor in
                           cases where there is a combination of alleged violations of academic and non-academic
                           regulations.
                        </p>
                        
                        <p>Any student determined by the professor to have been responsible for engaging in an
                           act of academic dishonesty shall be subject to a range of academic penalties (apart
                           from any sanctions that may be imposed pursuant to the Code) as determined by the
                           professor which may include, but not be limited to, one or more of the following:
                           loss of credit for an assignment, examination, or project; a reduction in the course
                           grade; or a grade of "F" in the course.
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/conduct/academic-integrity.pcf">©</a>
      </div>
   </body>
</html>