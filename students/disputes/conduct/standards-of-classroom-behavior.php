<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Standards of Classroom Behavior  | Valencia College</title>
      <meta name="Description" content="Standards of Classroom Behavior | Student Conduct | Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, disputes, conduct, standards, classroom, behavior">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/conduct/standards-of-classroom-behavior.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/conduct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		Student Conduct
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li><a href="/students/disputes/conduct/">Conduct</a></li>
               <li>Standards of Classroom Behavior </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        <h3><strong>Standards of Classroom Behavior</strong></h3>
                        
                        
                        
                        <p>Primary responsibility for managing the classroom environment rests with the faculty.
                           Faculty members are authorized to define, communicate, and enforce appropriate standards
                           of decorum in classrooms, offices, and other instructional areas under their supervision.
                        </p>
                        
                        <p>Students who engage in any prohibited or unlawful acts that result in disruption of
                           a class may be directed by the faculty member to leave the class for the remainder
                           of the class period. Longer suspensions from class or dismissal on disciplinary grounds
                           must be preceded by a disciplinary conference or hearing, as set forth in the&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>.
                        </p>
                        
                        <p>Examples of such disruptive or distracting activities include, but are not limited
                           to, the following:
                        </p>
                        
                        <ol>
                           
                           <li>Activities that are inconsistent with commonly acceptable classroom behavior and which
                              are not conducive to the learning experience, such as: excessive tardiness, leaving
                              and returning during class, and early departure when not previously authorized.
                           </li>
                           
                           <li>Activities which violate previously prescribed classroom guidelines or constitute
                              an unreasonable interruption of the learning process.
                           </li>
                           
                           <li>Side discussions which are irrelevant to the subject matter of the class, that distract
                              from the learning process, or impede, hinder, or inhibit the ability of the students
                              to obtain the full benefit of the educational presentation.
                           </li>
                           
                        </ol>
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/conduct/standards-of-classroom-behavior.pcf">©</a>
      </div>
   </body>
</html>