<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Conduct  | Valencia College</title>
      <meta name="Description" content="Student Conduct | Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, disputes, conduct">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/conduct/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/conduct/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		Student Conduct
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li>Conduct</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h3><strong>Student Conduct</strong></h3>
                        
                        
                        
                        <p>Valencia College is dedicated to the  advancement of knowledge and learning and to
                           the development of responsible  personal and social conduct. The primary purpose for
                           the maintenance of  discipline in the College setting is to support a civil environment
                           conducive  to learning and inquiry.<a href=""></a></p>
                        
                        
                        <hr class="styled_2">           
                        
                        
                        <h3><strong>Campus Civility</strong></h3>
                        
                        <p>Civility is being polite, courteous, and considerate towards  others. More likely
                           than not, you will encounter individuals that have  differing perceptions. A civil
                           person should recognize differences and welcome  the chance to learn instead of being
                           judgmental and attacking individuals based  on their differences. Remember, maintaining
                           civility on campus is an individual  effort that benefits the whole community.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Reporting  an Incident</strong></h3>
                        
                        <p> Have you witnessed or been affected by a violation of the Student  Code of Conduct?
                           To file a complaint, you may go to the Security Services Office or file a report online
                           through this link:
                        </p>
                        
                        <p><a href="http://valenciacollege.edu/security/incidentReporting.cfm" class="button color1 width_120 inline narrow">Report an Incident</a></p>
                        
                        
                        <hr class="styled_2">            
                        
                        
                        <h3><strong>The Student Conduct Review Process</strong></h3>
                        
                        <p>Once a complaint is filed, the Dean of Students or designee  will review the complaint
                           and consult with relevant parties regarding the  incident. The Dean of Students or
                           designee will then follow-up with the Student  Conduct Review Process as outlined
                           in the <a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>.
                        </p>
                        
                        <p>If the Dean of Students or designee determines that a student is  in violation of
                           the Student Code, a formal charge will be sent to the student's  Atlas e-mail. Included
                           in the charge will be a request to meet in a preliminary  meeting to discuss the incident
                           and determine responsibility. Based on the  severity of the alleged violation and
                           the information gathered in the  preliminary meeting, the Dean of Students or designee
                           may resolve the matter  immediately or forward the case to mediation or a disciplinary
                           conference.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Student and Victim Protections</strong></h3>
                        
                        <p>In order to ensure fairness during the Student Conduct Review  Process, Valencia College
                           provides certain protections/rights to charged  students and victims. For a complete
                           list of these protections, please refer to  the <a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>.
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        <a href="http://valenciacollege.edu/students/conduct/documents/student-conduct-hearing-checklist.pdf" class="button color2">Student Conduct Hearing Checklist</a>
                        
                        <a href="http://valenciacollege.edu/generalcounsel/policy/documents/Volume8/8-03-Student-Code-of-Conduct.pdf" class="button color2">Policy: Student Code of Conduct</a>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Staff &amp; Faculty Resources</strong></h3>
                        
                        <ul>
                           
                           <li><a href="/faculty/development/">Faculty Development <br>
                                 (classroom management)</a></li>
                           
                           <li><a href="/departments/">Academic Deans</a></li>
                           
                           <li>Deans of Students</li>
                           
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3><strong>Student Resources</strong></h3>
                        
                        <ul>
                           
                           <li><a href="http://catalog.valenciacollege.edu/studentservices/baycarestudentassistanceservices/">BayCare</a></li>
                           
                           <li><a href="/learning-support/tutoring">Tutoring Services</a></li>
                           
                           <li><a href="/advising-counseling">Advising</a></li>
                           
                           <li><a href="/careercenter">Career Center</a></li>
                           
                        </ul>
                        
                        
                        
                        <div class="container margin-60">
                           
                           <div class="main-title">
                              
                              <h2>
                                 Frequently Asked Questions
                                 
                              </h2>
                              
                           </div>
                           
                           <div class="row">
                              
                              <div class="col-md-4">
                                 
                                 <div class="box_style_2">
                                    
                                    <h3>
                                       I JUST RECEIVED AN E-MAIL FROM VALENCIA COLLEGE REGARDING STUDENT CONDUCT. WHAT IS
                                       IT / WHAT SHOULD I DO?
                                       
                                    </h3>
                                    
                                    <p>If you have received an e-mail from the Dean of Students or Coordinator of Student
                                       Conduct, this means that you we have received a report and you have been charged with
                                       violating the&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>. Follow-up by responding to the e-mail or by calling the sender to schedule an appointment.
                                       See procedures section of the Student Code of Conduct to see the overall process.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-md-4">
                                 
                                 <div class="box_style_2">
                                    
                                    <h4>WHAT HAPPENS IF I DO NOT MAKE AN APPOINTMENT OR SKIP MY APPOINTMENT?</h4>
                                    
                                    <p>The Dean of Students or designee will place a hold on your record upon receiving an
                                       incident report. The hold will remain on your record and will prevent you from registering
                                       for future semesters. Skipping a disciplinary conference means that you will not be
                                       able to tell your side of the story. It is in your best interest to attend all meetings
                                       that have been scheduled.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-md-4">
                                 
                                 <div class="box_style_2">
                                    
                                    <h4>WHAT WILL HAPPEN AT THE PRELIMINARY MEETING?</h4>
                                    
                                    <p>The preliminary meeting is usually a conversation meant to get your side of the story,
                                       learn about your goals and aspirations, and to educate you on the student conduct
                                       review process.
                                    </p>
                                    
                                    
                                    <h4>WHAT IF I DISAGREE WITH THE DECISION?</h4>
                                    
                                    <p>If you disagree with the committee's decision, you have the chance to appeal with
                                       the Vice President of Student Affairs. See the Appeals section in the&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>. Note: Appeals are only for sanctions of Suspension and Expulsion/Dismissal.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-4">
                                 
                                 <div class="box_style_2">
                                    
                                    <h4>WHAT HAPPENS AFTER A PRELIMINARY MEETING?</h4>
                                    
                                    <p>If the Dean of Students or designee determines that a student is not in violation,
                                       then he or she will dismiss the case or recommend mediation. If the Dean of Students
                                       or designee determines that a student is in violation of the&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct&nbsp;</a>, a formal charge will be sent to the student's Atlas e-mail. Included in the charge
                                       will be a request to meet in a disciplinary conference to discuss the incident and
                                       determine responsibility.
                                    </p>
                                    
                                    <p>For students accepting responsibility, it is possible to forgo the disciplinary conference.
                                       You may ask the Dean of Students or designee about waiving your right to a disciplinary
                                       conference.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-md-4">
                                 
                                 <div class="box_style_2">
                                    
                                    <h4>WHAT IF I AM FOUND IN VIOLATION FOR VIOLATING THE RULES?</h4>
                                    
                                    <p>If you are found to be in violation of the&nbsp;<a href="http://valenciacollege.edu/generalcounsel/policy/documents/8-03-Student-Code-of-Conduct-Amended.pdf">Student Code of Conduct</a>, you will receive a sanction that is appropriate to the offense. Our personal philosophy
                                       is to educate rather than to punish the student. You can expect to have a disciplinary
                                       sanction and an educational sanction. Disciplinary sanctions include warning, probation,
                                       suspension, or expulsion. Educational sanctions can include community service, writing
                                       reflection papers, or attending workshops.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div class="col-md-4">
                                 
                                 <div class="box_style_2">
                                    
                                    <h4>WHAT HAPPENS IF I GET CAUGHT FOR AN ACT OF ACADEMIC MISCONDUCT?</h4>
                                    
                                    <p>Any student determined by the professor to have been responsible for engaging in an
                                       act of academic dishonesty shall be subject to a range of academic penalties (apart
                                       from any sanctions that may be imposed pursuant to the Code) as determined by the
                                       professor which may include, but not be limited to, one or more of the following:
                                       loss of credit for an assignment, examination, or project; a reduction in the course
                                       grade; or a grade of "F" in the course. Students may be subject to both the Student
                                       Conduct Code and academic sanctions.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/conduct/index.pcf">©</a>
      </div>
   </body>
</html>