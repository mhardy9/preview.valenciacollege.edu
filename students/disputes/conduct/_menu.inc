<ul>
	<li><a href="/students/disputes/conduct/index.php">Student Conduct</a></li>
	<li class="submenu megamenu">
		<a class="show-submenu-mega" href="javascript:void(0);">Resources <i aria-hidden="true" class="far fa-angle-down"></i></a>
		<ul>
			<li><a href="/students/disputes/conduct/academic-integrity.php">Academic Integrity</a></li>
			<li><a href="/students/disputes/conduct/standards-of-classroom-behavior.php">Standards of Classroom Behavior</a></li>

		</ul>
	</li>
</ul>

