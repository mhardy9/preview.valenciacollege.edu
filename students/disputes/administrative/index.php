<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Administrative Complaint Resolution  | Valencia College</title>
      <meta name="Description" content="Student Administrative Complaint Resolution | Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, dispute, administrative">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/administrative/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/administrative/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li>Administrative</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Student Administrative Complaint Resolution</h2>
                        
                        
                        <p>Valencia front line learning leaders are a resource for students seeking assistance
                           in resolving issues with non-academic matters. The chart below lists the appropriate
                           staff leaders who can assist with designated issues. Students may follow the progression
                           of staff assistance in an administrative area, starting with the first response level,
                           to bring resolution to the issue.
                        </p>
                        
                        
                        <p>The College will make every effort to resolve conflicts by informal means. Students
                           should expect to be treated with respect and dignity, receive a timely response, and
                           have the issues handled in a private as possible manner. The College expects the student
                           to bring up any problems early, give clear and detailed information, follow applicable
                           procedures and be respectful of the people who are involved. Formal procedures for
                           appealing administrative and academic decisions can be found in the <a href="../../catalog/index.html">College Catalog</a> (available online). An individual who believes he or she has been discriminated against
                           should refer to
                           <a href="../../../about/general-counsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf">College Policy  [6+Hx28:2-03], Investigating and Resolving Discrimination, Harassment,
                              and Sexual Harassment Complaints</a>.
                        </p>
                        
                        
                        <h3>Administrative Complaint Procedure</h3>
                        
                        <p>The checklist was developed as a tool to aid students in the completion of the Administrative
                           Complaint Resolution process. Before you file an administrative complaint, please
                           make sure that you complete the following steps:
                        </p>
                        
                        <ul>
                           
                           <li>Submit an Administrative Complaint form within 60 days of administrative complaint.</li>
                           
                           <li>Attach additional documentation as needed to the Administrative Complain form.</li>
                           
                           <li>State the resolution you are looking for, be very specific.</li>
                           
                           <li>Make a copy of your Administrative Complaint form for your records.            </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/administrative/index.pcf">©</a>
      </div>
   </body>
</html>