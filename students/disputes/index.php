<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Dispute Resolution  | Valencia College</title>
      <meta name="Description" content="Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, dispute, resolution">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Disputes</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div>  
                           
                           <p>An <a href="academic/ombudsman.html">Ombudsman</a> provides a safe and comfortable environment for students to discuss complaints, concerns
                              or problems privately. Please visit the Ombudsman page for more information.
                           </p>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Final Grade Disputes</strong></h3>
                           
                           <p>To appeal your final grade or learn more about Valencia's Academic Grade Dispute Policy
                              and Procedure.
                           </p>
                           
                           
                           <ul>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/students/disputes/grade/form.cfm" target="_blank">Express Your Concern</a></li>
                              
                              <li><a href="grade/index.html">Learn More</a></li>
                              
                              <li><a href="../../about/general-counsel/policy/documents/volume8/8-10-Student-Academic-Dispute-and-Administrative-Complaint-Resolution.pdf" target="_blank">Policy</a></li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Non Final Grade and Other Academic Disputes</strong></h3>
                           
                           <p>For guidance and support related to accessibility of courses and credit granted toward
                              a degree. 
                           </p>
                           
                           
                           <ul>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/students/disputes/academic/form.cfm" target="_blank">Express Your Concern</a></li>
                              
                              <li><a href="academic/index.html">Learn More</a></li>
                              
                              <li><a href="../../about/general-counsel/policy/documents/volume8/8-10-Student-Academic-Dispute-and-Administrative-Complaint-Resolution.pdf" target="_blank">Policy</a></li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Student Administrative Complaint Resolution</strong></h3>
                           
                           <p>To find assistance in resolving issues with non-academic matters in college service
                              areas, including, but not limited to: financial aid, residency, bookstore, orientation,
                              and parking.
                           </p>
                           
                           
                           <ul>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/students/disputes/administrative/form.cfm" target="_blank">Express Your Concern</a></li>
                              
                              <li><a href="administrative/index.html">Learn More</a></li>
                              
                              <li><a href="../../about/general-counsel/policy/documents/volume8/8-10-Student-Academic-Dispute-and-Administrative-Complaint-Resolution.pdf" target="_blank">Policy</a></li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Civil Rights Discrimination</strong></h3>
                           
                           <p>To report a concern of discrimination and/or  harassment.</p>
                           
                           
                           <ul>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/students/disputes/civil-rights-discrimination/form.cfm" target="_blank">Express Your Concern</a></li>
                              
                              <li><a href="civil-rights-discrimination/index.html">Learn More</a></li>
                              
                              <li><a href="../../about/general-counsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">Policy</a></li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Sexual Harassment / Sexual Assault (Title IX)</strong></h3>
                           
                           <p>To report a concern of sexual harassment and/or sexual  violence.</p>
                           
                           
                           <ul>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/students/disputes/sexual-harassment-assault/form.cfm" target="_blank">Express Your Concern</a></li>
                              
                              <li><a href="sexual-harassment-assault/index.html">Learn More</a></li>
                              
                              <li><a href="../../about/general-counsel/policy/documents/volume2/2-01-Discrimination-Harassment-and-Related-Conduct.pdf" target="_blank">Policy</a></li>
                              
                           </ul>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h3><strong>Student Code of Conduct</strong></h3>
                           
                           <p>To report a violation of the Student Code of Conduct, including but not limited to:
                              theft, dishonesty, disruptive conduct, hazing, etc.
                           </p>
                           
                           
                           <ul>
                              
                              <li><a href="http://net4.valenciacollege.edu/forms/students/disputes/conduct/form.cfm" target="_blank">Express Your Concern</a></li>
                              
                              <li><a href="conduct/index.html">Learn More</a></li>
                              
                              <li><a href="../../about/general-counsel/policy/documents/volume8/8-03-Student-Code-of-Conduct.pdf" target="_blank">Policy</a></li>
                              
                           </ul>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/index.pcf">©</a>
      </div>
   </body>
</html>