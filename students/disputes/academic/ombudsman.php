<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Ombudsman  | Valencia College</title>
      <meta name="Description" content="Ombudsman | Student Dispute Resolution">
      <meta name="Keywords" content="college, school, educational, student, dispute, ombudsman">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/disputes/academic/ombudsman.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/disputes/academic/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Student Dispute Resolution</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/disputes/">Disputes</a></li>
               <li><a href="/students/disputes/academic/">Academic</a></li>
               <li>Ombudsman </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Purpose of the Ombudsman</h2>
                        
                        <img src="arrow.gif">An Ombudsman provides a safe and comfortable environment for students to discuss complaints,
                        concerns or problems privately. When appropriate, the office will initiate an informal
                        intervention with the goal of facilitating a resolution that is acceptable to all
                        parties involved.  The ombudsman acts as an independent, impartial resource. If a
                        matter cannot be resolved through this office, a referral will be made. When appropriate,
                        the office can make recommendations regarding policy review and change.<br>
                        <br>
                        
                        
                        <h2>Role of the Ombudsman</h2>
                        
                        <img src="arrow.gif"> The Office of the Ombudsman offers an alternative opportunity to resolve complaints,
                        concerns or problems in a timely and private manner. Users of the office are provided
                        a place to explore options to make informed decisions. The Office of the Ombudsman
                        does not replace or substitute for formal grievance, investigative or appeal processes
                        made available by the College. Also, the office does not have any authority to make
                        decisions or enact policy.  Use of the Ombudsman Office is not a substitute for formal
                        procedures, such as filing a final grade grievance. Any communication with the Ombudsman
                        Office is off-the-record and does not put the College on notice of a problem. If students
                        wish to place the College on notice, the ombudsman can provide information about how
                        to do so. Ombudsmen follow no prescribed sequences of steps, and do not participate
                        in any formal grievance process. The ombudsman does not advocate for either party
                        and cannot assist a student who is represented by legal counsel.<br><br>
                        
                        <img src="arrow.gif">An ombudsman does not make, change, or set aside policy or previous administrative
                        decisions, nor does an ombudsman serve to determine the rights of others or to unilaterally
                        resolve conflicts. Rather, knowledge of the facts of a situation, plus reason, persuasion,
                        and familiarity with the system are adequate to resolve those problems in which a
                        mistake was made, where a practice was applied unfairly, or where poor judgment was
                        exercised in reaching a decision.<br><br>
                        
                        <strong>The Ombudsman WILL:</strong>
                        
                        <ul>
                           
                           <li>Listen and discuss questions, issues, and concerns  </li>
                           
                           <li>Be an advocate for fairness</li>
                           
                           <li>Help develop and evaluate various options to address concerns  </li>
                           
                           <li>Answer questions or help find others who can develop and evaluate various options
                              to address concerns 
                           </li>
                           
                           <li>Explain College policies and procedures  </li>
                           
                           <li>Facilitate communication between people  </li>
                           
                           <li>Advise individuals about steps to resolve problems informally </li>
                           
                           <li>Advise individuals about formal and administrative options  </li>
                           
                           <li>Mediate disputes to seek "win-win" resolution of problems </li>
                           
                           <li>Make appropriate referrals when informal options don't work </li> 
                           
                           <li>Point out patterns of problems/complaints to administrators </li>
                           
                        </ul> 
                        <strong>The Ombudsman WILL NOT:</strong>
                        
                        <ul>
                           
                           <li>Replace or circumvent existing channels</li>
                           
                           <li>Direct any College office to change a decision</li>
                           
                           <li>Make decisions for the student</li>
                           
                           <li>Have a stake in outcomes</li>
                           
                           <li>Set aside rules and regulations</li>
                           
                           <li>Participate in formal grievance processes  </li>
                           
                           <li>Make decisions for College faculty/administrators  </li>
                           
                           <li>Determine "guilt" or "innocence" of those accused of wrong-doing  </li>
                           
                           <li>Assign sanctions to students  </li>
                           
                           <li>Receive official "notice" for the College about issues </li>
                           
                           <li>Give legal advice</li>
                           
                        </ul>
                        
                        
                        
                        <h2>Campus Ombudsman</h2>
                        
                        
                        
                        <div>
                           
                           <h3>East Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=kmalmos.html">Keith  Malmos, Professor of Biology</a></p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=koverhiser.html">Kurt  Overhiser, Professor of Mathematics</a></p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=zhawk1.html">Zachary Hawk, Mgr, Campus (Evening/Weekend)</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Lake Nona Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=dhollister.html">Debra  Hollister, Professor of Psychology</a> 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Osceola Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=rcolburn.html">Ronald  Colburn, Professor of Speech</a> 
                                 </p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=ocuan.html">Oscar Cuan,  Professor of Speech</a></p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=schater.html">Samira  Chater, Professor  of French &amp; Spanish</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>West Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=jalexander.html">Joan  Alexander, Professor of Computer Programming and Analytics</a> 
                                 </p>
                                 
                                 <p> <a href="../../../contact/Search_Detail2.cfm-ID=bbond.html">Beverly Bond,  Professor of Radiography</a></p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=lhoward13.html">Lynn Howard, Professor of Mathematics</a> 
                                 </p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=glindbeck.html">Graeme  Lindbeck, Professor of Biology</a> 
                                 </p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=gmorales.html">Gustavo  Morales, Professor of Geology</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <h3>Winter Park Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=cborglum.html">Chris  Borglum, Professor of English</a> 
                                 </p>
                                 
                                 <p><a href="../../../contact/Search_Detail2.cfm-ID=jniss.html">John Niss,  Professor of Mathematics</a></p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/disputes/academic/ombudsman.pcf">©</a>
      </div>
   </body>
</html>