<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Transitions Planning  | Valencia College</title>
      <meta name="Description" content="Transitions Planning">
      <meta name="Keywords" content="college, school, educational, transitions, planning">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/transitions-planning/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/transitions-planning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Transitions Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Transitions Planning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>What We Do</h2>
                        
                        <p>The Transitions Planning Team offers resources to help prepare students for life after
                           high school, whether that includes college or industry certification. We focus on
                           creating gateways to student success from the start through signature events, targeted
                           outreach, and community partnerships. These are just a few of the ways we lend you
                           our support.
                        </p>
                        
                        <hr class="styled_2">
                        
                        <h3>High School Outreach</h3>
                        
                        <p>During the fall and spring of the academic year, Valencia Coordinators go to schools
                           to visit with high school juniors and seniors and share enrollment information through
                           interactive presentations and college transition help labs.
                        </p>
                        
                        <p><a title="High School Outreach" href="/students/transitions-planning/high-school-outreach.php">Learn More</a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Campus Programs &amp; Experiences</h3>
                        
                        <p>With an emphasis on careers, campus based tours and immersive experiences include
                           information related to enrollment, degree program options and preparing for the transition
                           to college. Students in middle and high school grades are engaged in interactive academic
                           experiences, exposing them to a world of career options.
                        </p>
                        
                        <p><a title="Campus Programs &amp; Experiences" href="/students/transitions-planning/campus-programs-experiences.php">Learn More</a></p>
                        
                        <hr class="styled_2">
                        
                        <h3>Signature Events</h3>
                        
                        <p>Our signature events provide fellow educators, community partners, and prospective
                           students and their families with additional resources and opportunities to learn about
                           post-secondary options.
                        </p>
                        
                        <p><a title="Signature Events" href="/students/transitions-planning/signature-events.php">Learn More<br></a></p>
                        
                        <hr>
                        
                        <h3 class="v-red"><br><br><a href="http://net1.valenciacollege.edu/future-students/visit-valencia/">Sign up for a Campus Tour or Information Session Today!</a></h3>
                        
                        <p class="v-red">&nbsp;</p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/transitions-planning/index.pcf">©</a>
      </div>
   </body>
</html>