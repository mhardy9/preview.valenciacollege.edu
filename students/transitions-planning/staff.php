<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Meet the Team  | Valencia College</title>
      <meta name="Description" content="Meet the Team | Transitions Planning | Valencia College">
      <meta name="Keywords" content="college, school, educational, transitions, planning, team">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/transitions-planning/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/transitions-planning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Transitions Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/transitions-planning/">Transitions Planning</a></li>
               <li>Meet the Team </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>College Transitions Team | Serving Orange and Osceola Counties</h2>
                        
                        <p>Niurka Ferrer, Director, Transition Planning <br> (407) 582-1037<br> <a href="mailto:nferrer4@valenciacollege.edu">nferrer4@valenciacollege.edu</a></p>
                        
                        <p>Latasha Graham, Assistant Director, Transition Planning<br> West Campus <br> (407) 582-1434<br> <a href="mailto:lgraham18@valenciacollege.edu">lgraham18@valenciacollege.edu</a></p>
                        
                        <p>Abby Nobili, Assistant Director, Transition Planning <br> East Campus<br> (407) 582-2001<br> <a href="mailto:anobili1@valenciacollege.edu">anobili1@valenciacollege.edu</a></p>
                        
                        <p>Lorena Guardia, Assistant Director, Transition Planning<br> Osceola Campus <br> (407) 582-4802 <br> <a href="mailto:lguardia@valenciacollege.edu">lguardia@valenciacollege.edu</a></p>
                        
                        <p>Ashley Spry, Immersive Coordinator, Transition Planning<br> West Campus<br> (407) 582-2785<br> <a href="mailto:aspry2@valenciacollege.edu">aspry2@valenciacollege.edu</a></p>
                        
                        <p><span>Carnard Johnson, Outreach Coordinator, Transition Planning</span><br><span>West Campus</span><br><span>407-582-5150</span><br><a href="mailto:cjohnson175@valenciacollege.edu" rel="noreferrer">cjohnson175@valenciacollege.edu</a></p>
                        
                        <p>Omarys Hernandez, Program &amp; Communications Coordinator<br> West Campus <br> (407) 582-1291<br> <a href="mailto:ohernandez23@valenciacollege.edu">ohernandez23@valenciacollege.edu</a></p>
                        
                        <p>Aira Pierce, Staff Assistant<br> West Campus<br> (407) 582 - 1318<br> <a href="mailto:apierce12@valenciacollege.edu" rel="noreferrer">apierce12@valenciacollege.edu</a></p>
                        
                        <p dir="auto">Edith Montes, Immersive Coordinator, Transition Planning<br> East Campus<br> 407-582-2787<br> <a href="mailto:emontes6@valenciacollege.edu" rel="noreferrer">emontes6@valenciacollege.edu</a></p>
                        
                        <p dir="auto">Alania Hodge, Outreach Coordinator, Transition Planning<br> East Campus<br> (407) 582-2001<br> <a href="mailto:ahodge4@valenciacollege.edu">ahodge4@valenciacollege.edu</a></p>
                        
                        <p dir="auto"><span>Jessica Farnan, Immersive Coordinator, Transitions Planning</span><br><span>Osceola County</span><br><span>(321) 682-4556</span><br><a href="mailto:jfarnan@valenciacollege.edu" rel="noreferrer">jfarnan@valenciacollege.edu</a></p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/transitions-planning/staff.pcf">©</a>
      </div>
   </body>
</html>