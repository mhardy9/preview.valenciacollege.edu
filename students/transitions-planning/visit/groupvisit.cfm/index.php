<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visit Valencia | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/transitions-planning/visit/groupvisit.cfm/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/transitions-planning/visit/groupvisit.cfm/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Visit Valencia</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/transitions-planning/">Transitions Planning</a></li>
               <li><a href="/students/transitions-planning/visit/">Visit</a></li>
               <li>Groupvisit.cfm</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="http://valenciacollege.edu/transitions/visit/"></a>
                        
                        
                        
                        
                        <h2>On-Campus Experiences</h2>
                        
                        <p>Experiences are designed for school or community groups of 15 or more and are scheduled
                           from 10:30am - 12:30pm on Tuesdays and Thursdays. 
                        </p>
                        
                        
                        <p>Group experiences differ by campus due to space availability. A ratio of 1 chaperone
                           per 10 students is required. 
                        </p>
                        
                        
                        <h3>Experience Valencia</h3>
                        
                        <p>Designed for groups in grades 9-12th to explore the campus while discovering Valencia's
                           programs and preparing for the transition to college, including steps to enrollment.
                        </p>
                        
                        
                        <h3>Explore Valencia</h3>
                        
                        <p>Designed for students in grades 6-8th to tour the Valencia campus and learn about
                           the educational pathways and career options at Valencia, and how to prepare for college
                           now.
                        </p>
                        
                        
                        <p>To book a visit, please select one of the following campuses to contact a Transitions
                           Coordinator. 
                        </p>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><a href="https://calendly.com/immersive_orange" target="_blank">East</a></div>
                                 
                                 
                                 <div data-old-tag="td"><a href="https://calendly.com/immersive_west" target="_blank">West</a></div>
                                 
                                 
                                 <div data-old-tag="td"><a href="https://calendly.com/immersive_osceola" target="_blank">Osceola</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p>Spring semester visit dates begin January 9, 2017 and are available until May 19,
                           2017.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Valencia College Closed Dates</h3>
                        
                        
                        <p>Please review the following Holiday and College Break schedule to assist you in planning
                           for your visit:
                        </p>
                        
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                        
                        
                        <p><br>
                           
                           *Valencia College closes at 12 noon on Fridays during Summer hours.
                           
                        </p>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/transitions-planning/visit/groupvisit.cfm/index.pcf">©</a>
      </div>
   </body>
</html>