<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Counselor Day | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/transitions-planning/counselorday/default.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/transitions-planning/counselorday/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Counselor Day</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/transitions-planning/">Transitions Planning</a></li>
               <li><a href="/students/transitions-planning/counselorday/">Counselorday</a></li>
               <li>Counselor Day</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"> 
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        <a href="../collegenight/index.html"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        
                        <h2><strong>Valencia College Counselor Day 2013</strong></h2>
                        
                        <h3><strong>Osceola Campus | Friday, November 15, 2013</strong></h3>
                        
                        <h3><strong>West Campus | Thursday, November 7, 2013</strong></h3>
                        
                        <p>The Transitions Team at Valencia College invites you to a time of learning and development
                           designed specifically for counselors and educational partners.
                        </p>
                        
                        <p>Event Registration is by invitation only. Please register with your email address
                           to attend this event. Click on the event link for more information and to save your
                           seat! 
                        </p>
                        
                        <p>We look forward to seeing you!</p>
                        
                        <p>--The Transitions Team at Valencia College</p>
                        
                        <hr>
                        
                        <div>
                           
                           <p>NOTE: Unfortunately, due to time constraints, anyone who signs up after 8:00 am on
                              Friday November 1, 2013 will not be able to receive a preprinted name badge.
                           </p>
                           
                           <p>Registration is by Invitation only. Please click the link below to request an invite
                              stating which date you would like to attend.<br>
                              
                           </p>
                           
                        </div>
                        
                        
                        <div>            
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       <div><span><a href="mailto:lgraham18@valenciacollege.edu?subject=Request%20for%20Counselor%20Day%20Invitation">Request an Invite</a> <s><br>
                                                </s></span></div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        
                        <p><br>
                           
                        </p>
                        
                        <p><a href="default.html#top">TOP</a></p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/transitions-planning/counselorday/default.pcf">©</a>
      </div>
   </body>
</html>