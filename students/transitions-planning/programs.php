<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Transitions Planning | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/transitions-planning/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/transitions-planning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Transitions Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/transitions-planning/">Transitions Planning</a></li>
               <li>Transitions Planning</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        
                        <h2>Programs</h2>
                        
                        <p><strong><u><a href="collegenight.html">College Night</a><br>
                                 </u></strong>The Central Florida community is invited to visit Valencia to meet with more than
                           130 college representatives in a college fair forum during two evenings in October.
                           
                        </p>
                        
                        <p><strong><u><a href="counselorday/index.html">Counselor Day</a><br>
                                 </u></strong>High school counselors from Orange and Osceola counties are invited to visit Valencia
                           in the fall to participate in a full day of professional development to support their
                           work with students.
                        </p>
                        
                        <p><u><strong>High School Outreach </strong></u><br>
                           During the fall and spring of the academic year, Valencia Coordinators will visit
                           with high school juniors and seniors to share enrollment information. Please <a href="http://net4.valenciacollege.edu/forms/transitions/contact.cfm" target="_blank">contact us</a> if your school is not listed and you would be interested in a high school visit by
                           Valencia.
                        </p>
                        
                        <p><strong><u>Open House <br>
                                 </u></strong>Future students and their families are encouraged to attend one of our open houses
                           in the Spring on our East, West, and Osceola Campuses. Come to learn about our academic
                           programs, student clubs, and get a tour of the campus. 
                        </p>
                        
                        <p><u><strong>Middle School Visit<br>
                                 </strong></u> Middle school students are invited to attend a fun event designed to give students
                           the opportunity to interact with various career programs. Students will also learn
                           about their own career interests by completing a career assessment and will have the
                           opportunity to explore our campus on a tour. 
                        </p>
                        
                        <p><u><strong>High School Visit </strong></u><br>
                           High school 9-11th graders are invited to participate in this interactive program
                           which tests their college smarts in the admissions process. With an emphasis on readiness,
                           the program will also include information related to career and degree programs at
                           Valencia and tips for conducting their college search.  A campus tour will conclude
                           the program.
                        </p>
                        
                        <p><strong><u><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Information Session</a></u></strong><br>
                           Future students and their families are encouraged to attend an information session
                           to learn more about Valencia College. Information sessions last approximately one
                           and a half hours and include an informational presentation on our academic programs,
                           financial aid, the Honors program, and the admission process. A question and answer
                           session will follow each presentation.  A campus tour will begin your information
                           session led by a current student leader. To schedule your information session, <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">click here</a>.
                        </p>
                        
                        <p><strong><u>College Fairs<br>
                                 </u></strong>The transitions team also attends numerous fairs throughout Central Florida that are
                           offered by community and business partners. In addition, as a member of the Central
                           Florida Higher Education Alliance we coordinate fairs with organizations and attend
                           fairs that other Alliance members organize.
                        </p>
                        
                        
                        <p><u><strong>College Prep  Day</strong></u><br>
                           Valencia invites ethnically diverse students and foster youth each year to a college
                           planning workshop. Sessions cover steps to enrollment for college, financial aid,
                           testing strategies (SAT &amp; ACT), and special sessions just for parents and guardians.
                        </p>
                        
                        <p><u><strong>Seniors at Valencia</strong></u><br>
                           High school seniors are invited to visit Valencia to learn the about our steps to
                           enrollment, Florida state residency, financial aid, UCF Direct Connect, student clubs,
                           and other campus support programs. Part of the program will include a campus tour
                           led by a current student leader.            
                        </p>
                        
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/transitions-planning/programs.pcf">©</a>
      </div>
   </body>
</html>