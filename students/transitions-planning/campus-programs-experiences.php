<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Campus Programs and Experiences  | Valencia College</title>
      <meta name="Description" content="Campus Programs and Experiences | Transitions Planning | Valencia College">
      <meta name="Keywords" content="college, school, educational, transitions, planning, campus, programs, experiences">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/transitions-planning/campus-programs-experiences.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/transitions-planning/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Transitions Planning</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/transitions-planning/">Transitions Planning</a></li>
               <li>Campus Programs and Experiences </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        
                        
                        <h2>Campus Programs and Experiences</h2>
                        Listed below are the on campus programs and experiences we offer aimed at exposing
                        prospective students to the variety of career options offered at Valencia.
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>CAMPUS TOURS </h3>
                        
                        <p>Individual students and families have an opportunity to go on a 45 minute walking
                           tour led by a Valencia Student leader.<br>
                           
                        </p>
                        
                        <h3>INFORMATION SESSIONS </h3>
                        
                        <p>A Valencia Information Session covers all the general topics you need to know about
                           in a 60 minute interactive presentation and includes a question and answer session.
                        </p>
                        
                        <p>To sign up for a campus tour or an information session or click <a data-content="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=2.112014340.1052748095.1500294312-454859190.1492430578" data-type="external" href="http://preview.valenciacollege.edu/future-students/visit-valencia/?_ga=2.112014340.1052748095.1500294312-454859190.1492430578" target="_blank">here</a>.            
                        </p>
                        
                        <h3>THE VALENCIA EXPERIENCE</h3>
                        
                        <p><strong>Explore Valencia: </strong>Designed for school or community groups of 15 or more students in grades 6th - 8th.
                           Students have the opportunity to tour the campus, engage with faculty and staff and
                           learn about the educational path they should pursue in order to reach their ultimate
                           goal. <br>
                           <br>
                           <strong>Experience Valencia:</strong> Designed for school or community groups of 15 or more students in grades 9th - 12th.
                           Students have the opportunity to tour the campus, engage with faculty and staff, discover
                           the degree programs offered and learn about next steps to enrollment. 
                        </p>
                        
                        <p><strong>College Prep Day:</strong> To schedule an on campus experience for your group of 15 or more students click on
                           the appropriate link below
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a data-content="https://calendly.com/immersive_orange" data-type="external" href="https://calendly.com/immersive_west" target="_blank">The Valencia Experience | West Campus</a><br>
                              
                           </li>
                           
                           <li>
                              <a data-content="https://calendly.com/immersive_orange" data-type="external" href="https://calendly.com/valenciatransitions_east" target="_blank">The Valencia Experience | East Campus</a><br>
                              
                           </li>
                           
                           <li>
                              <a data-content="https://calendly.com/immersive_osceola" data-type="external" href="https://calendly.com/immersive_osceola" target="_blank">The Valencia Experience | Osceola Campus</a>            
                           </li>
                           
                        </ul>
                        
                        <h3>COLLEGE PREP DAY</h3>
                        
                        <p>Orange and Osceola County middle and high school students, foster youth, special school
                           target populations and community groups are invited for annual college-planning and
                           career exploration events throughout all campuses. Sessions cover preparing for high
                           school, college readiness, steps to enrollment for college, financial aid, testing
                           strategies, and career exploration activities aligned to the 120+ Career Programs
                           offered at Valencia.
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/transitions-planning/campus-programs-experiences.pcf">©</a>
      </div>
   </body>
</html>