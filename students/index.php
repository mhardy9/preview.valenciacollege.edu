<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Current Students | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>&nbsp;&nbsp;Current Students</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li>Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Current Students</h2>
                        
                        
                        <h3>Academics</h3>
                        
                        
                        <ul>
                           
                           <li><a href="/aadegrees/articulationagreements.cfm">Articulation</a></li>
                           
                           <li><a href="/competencies/">Core Competencies</a></li>
                           
                           <li><a href="/students/courses.cfm">Courses Offered</a></li>
                           
                           <li><a href="http://net5.valenciacollege.edu/schedule/">Credit Class Schedule</a></li>
                           
                           <li><a href="/programs/">Degree &amp; Career Programs</a></li>
                           
                           <li><a href="/departments/">Departments</a></li>
                           
                           <li><a href="/oit/learning-technology-services/student-resources/">Distance Learning</a></li>
                           
                           <li><a href="/programs/">Educational Enhancements</a></li>
                           
                           <li><a href="/epi/">Educator Preparation<br>Institute (EPI)</a></li>
                           
                           <li><a href="http://faculty.valenciacollege.edu/">Faculty Web Sites</a></li>
                           
                           <li><a href="https://learn.valenciacollege.edu//">Blackboard Learn</a></li>
                           
                           <li><a href="/studentsuccess/">Student Success</a></li>
                           
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>General Information</h3>
                        
                        
                        <ul>
                           
                           <li><a href="/admissions-records/">Admissions</a></li>
                           
                           <li><a href="/support/">Ask Valencia</a></li>
                           
                           <li><a href="/artsandentertainment/gallery/">Art Gallery</a></li>
                           
                           <li><a href="/calendar/">Calendar</a></li>
                           
                           <li><a href="/dining/">Campus Dining</a></li>
                           
                           <li><a href="/locations-store/">Campus Store</a></li>
                           
                           <li><a href="/catalog/">Catalog</a></li>
                           
                           <li><a href="/labs/">Computer Lab Hours</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/why-valencia/consumer-information/">Consumer Information</a></li>
                           
                           <li><a href="/hr/equalaccessequalopportunity.cfm">Equal Opportunity Employer</a></li>
                           
                           <li><a href="/library/">Library</a></li>
                           
                           <li><a href="/generalcounsel/">Policy Manual</a></li>
                           
                           <li><a href="/security/">Security</a></li>
                           
                           <li><a href="https://valenciacollege.edu/students/disputes/" title="Student Conflict Resolution">Student Conflict Resolution</a></li>
                           
                           <li><a href="/pdf/studenthandbook.pdf" target="_blank">Student Handbook Print Version (pdf)</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia</a></li>
                           
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Learning Support</h3>
                        
                        
                        <ul>
                           
                           <li><a href="/learning-support/browse-by-campus.cfm">Learning Centers</a></li>
                           
                           <li><a href="/servicelearning/">Service Learning</a></li>  
                           
                           <li><a href="/learning-support/skillshops.cfm">Skillshops</a></li>
                           
                           <li><a href="/supplemental-learning/">Supplemental Learning</a></li>
                           
                           <li><a href="/testing-center/">Testing Center</a></li>
                           
                           <li><a href="/learning-support/browse-by-campus.cfm">Tutoring &amp; Academic Help</a></li>                   
                           
                           <li><a href="/learning-support/writing-consultations.cfm">Writing Consultations</a></li>                                      
                           
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <h3>Student Services</h3>
                        
                        
                        <ul>
                           
                           <li><a href="/advising-center/">Advising Center</a></li>
                           
                           <li><a href="/answer-center/">Answer Center</a></li>
                           
                           <li><a href="/assessments/">Assessment</a></li>
                           
                           <li><a href="/bridges/">Bridges to Success</a></li>
                           
                           <li><a href="/businessoffice/">Business Office</a></li>
                           
                           <li><a href="/careercenter/">Career Development</a></li>
                           
                           <li><a href="/helpas/">Career Program Advisors</a></li>
                           
                           <li><a href="/student-development/clubs/">Clubs &amp; Organizations</a></li>
                           
                           <li><a href="/finaid/">Financial Aid Services</a></li>
                           
                           <li><a href="/graduation/">Graduation/Transfers</a></li>
                           
                           <li><a href="/honors/">Honors Program</a></li>
                           
                           <li><a href="/international/">International Students</a></li>
                           
                           <li><a href="/internship/">Internship &amp; Workforce Services</a></li>
                           
                           <li><a href="/lifemap/">Lifemap</a></li>
                           
                           <li><a href="/orientation/">New Student Orientation</a></li>
                           
                           <li><a href="/admissions-records/">Records/Transcripts</a></li>
                           
                           <li><a href="/osd/">Students with Disabilities</a></li>
                           
                           <li><a href="/studentaffairs/">Student Affairs</a></li>
                           
                           <li><a href="/studentdev/">Student Development</a></li>
                           
                           <li><a href="/student-services/">Student Services</a></li>
                           
                           <li><a href="/international/studyabroad/">Study Abroad and Global Experiences</a></li>
                           
                           
                           <li><a href="http://www.valenciavoice.com/" class="icon_for_external" target="_blank">Valencia Voice</a></li>
                           
                           <li><a href="/veterans-affairs/">Veteran Affairs</a></li>
                           
                        </ul>
                        
                        
                        <hr class="styled_2">
                        
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td>
                                    <a href="http://www.youtube.com/embed/--aVeXRzyqw" target="_blank" class="youtube cboxElement"><img src="/_resources/img/students/current-students/todays-classrooms2-550x350.png" alt="Today's College Classroom" title="Today's College Classroom" width="550" height="350"></a>
                                    
                                    
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>
                                    
                                    
                                    <table class="table ">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td valign="middle">
                                                <a href="http://valencia.org/giving/dollarsforscholars.cfm?utm_source=EDU&amp;utm_medium=banner-small&amp;utm_content=15ALU005&amp;utm_campaign=ALU-DollarsforScholars" target="_blank"><img src="/_resources/img/students/current-students/15ALU005-alumni-giving-edu-small-banner.jpg" width="270" height="60" alt="DollarsforScholars"></a>
                                                
                                             </td>
                                             
                                             
                                             <td valign="middle"><a href="http://net4.valenciacollege.edu/promos/Internal/DeansPresidentsList.cfm"><img src="/_resources/img/students/current-students/deansList.png" alt="Dean's List &amp; President's List" width="267" height="60" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td colspan="2">&nbsp;</td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td valign="middle"><a href="http://net4.valenciacollege.edu/promos/Internal/valencia-voice.cfm" target="_blank"><img src="/_resources/img/students/current-students/valencia-voice.png" alt="Valencia Voice - Student News" width="270" height="60" border="0"></a></td>
                                             
                                             <td valign="middle"><a href="http://net4.valenciacollege.edu/promos/Internal/HigherOne.cfm"><img src="/_resources/img/students/current-students/higherOne2.gif" alt="Valencia Debit Card" width="270" height="60" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td colspan="2">&nbsp;</td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td valign="middle"><a href="http://net4.valenciacollege.edu/promos/Internal/DirectConnect.cfm" target="_blank"><img src="/_resources/img/students/current-students/directConnectBanner270x60.jpg" alt="Direct Connect UCF" width="270" height="60" border="0"></a></td>
                                             
                                             <td valign="middle"><a href="http://net4.valenciacollege.edu/promos/Internal/FlexStartCourses.cfm"><img src="/_resources/img/students/current-students/flexStartClasses.jpg" alt="Register for Flex Start Courses Today" width="270" height="60" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td colspan="2">&nbsp;</td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td valign="middle"><a href="http://net4.valenciacollege.edu/promos/Internal/FinancialAidVideos.cfm" target="_blank"><img src="/_resources/img/students/current-students/FinancialAidTV.jpg" alt="Financial Aid Videos on Demand" width="270" height="60" border="0"></a></td>
                                             
                                             <td valign="middle"><a href="http://valenciacollege.edu/safety" target="_blank" title="Safety &amp; Security"><img class="border-gray" src="/_resources/img/students/current-students/valencia-alerts-270x60.png" alt="Safety &amp; Security" width="270" height="60" border="0"></a></td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td colspan="2">&nbsp;</td>
                                             
                                          </tr>  
                                          
                                          
                                          <tr>
                                             
                                             <td valign="middle"><a href="https://valenciaalumniconnect.com/?utm_source=HOMEPAGE&amp;utm_medium=SPOTLIGHT&amp;utm_content=15ALU016&amp;utm_campaign=ALU" target="_blank"><img src="/_resources/img/students/current-students/alumni-connect-tile-ad-students.gif" width="270" height="60" alt="Alumni Connect" border="0"></a></td>
                                             
                                             <td valign="middle">&nbsp;</td>
                                             
                                          </tr>
                                          
                                          
                                          <tr>
                                             
                                             <td valign="middle">&nbsp;</td>
                                             
                                             <td valign="middle">&nbsp;</td>
                                             
                                          </tr>
                                          
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/index.pcf">©</a>
      </div>
   </body>
</html>