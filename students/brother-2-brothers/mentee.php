<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Mentee | Valencia College</title>
      <meta name="Description" content="Brother 2 Brothers">
      <meta name="Keywords" content="brother2brothers, brother 2 brothers, B2B, valencia college, educational, brother, brothers">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/brother-2-brothers/mentee.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/brother-2-brothers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Brother 2 Brothers </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/brother-2-brothers/">Brother 2 Brothers </a></li>
               <li>Mentee</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_1">
                           		
                           <h2>Mentee</h2>
                           		
                           <h3>The medical definition of CPR:</h3>
                           		
                           <p> <img src=" /students/brother-2-brothers/images/brother-2-brothers-logo.png" alt="Brother 2 Brothers Logo" class="img-responsive" align="right">
                              		An emergency procedure in which the heart and lungs are revived by manually compressing
                              the chest overlying the heart and forcing air into the lungs. In other words, cardiopulmonary
                              resuscitation.
                           </p>
                           	
                           		
                           <hr class="styled_2">
                           	
                           	
                           <h3>Brother 2 Brothers' definition of CPR:</h3>
                           	
                           <p>An emergency response in which a student's attitude and aspirations are rehabilitated
                              through the implementation of a structured support system that encourages success
                              in all aspects of life and academics. In our usage, CPR stands for communicate, prepare
                              and recover, and it embodies the process in which mentors help to resuscitate students
                              in need.
                           </p>
                           	
                           <p><strong>Communicate:</strong>&nbsp;Mentors will actively monitor mentees' GPAs. In the event that a mentee's GPA falls
                              below 2.5, they will proactively reach out with tools and resources to help and, perhaps
                              most importantly, engage the mentee in conservation about what's going on.
                           </p>
                           	
                           <p><strong>Prepare:</strong>&nbsp;Mentees will be required to attend tutoring based on their academic needs twice weekly,
                              helping to prevent recurring issues.
                           </p>
                           	
                           <p><strong>Recover:</strong>&nbsp;Mentors will help students celebrate their hard work and find ways to be even more
                              successful in the future.
                           </p>
                           	
                           <p>When others might use terms like academic probation, Brother 2 Brothers prefers to
                              approach students earning poor grades with a more positive spin. Instead of focusing
                              on the consequences through traditional terminology like probation, Brother 2 Brothers
                              focuses on CPR to revive students when they feel like the world is falling in on them
                              by providing the support, structure and resources they need to get back on track.
                           </p>
                           
                           			
                           
                           <hr class="styled_2">
                           			
                           		
                           <div class="container margin-60">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="wrapper_indent">
                                       						
                                       <h3 class="v-red cap">Establishing and Maintaining Boundaries</h3>
                                       					
                                       <p>Boundaries are important in every relationship, and especially so in a mentoring relationship
                                          where your mentee will depend on you to model the behaviors he needs to adopt. By
                                          establishing consistent boundaries early in your conversations with your mentee and
                                          applying them consistently throughout your interactions, you provide him with a head
                                          start on achieving the personal growth he seeks, as well as protecting yourself from
                                          unforeseen outcomes and challenging your expectations.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;"><img src="/students/brother-2-brothers/images/brother-2-brothers-mentoring-mentee.jpg" alt="Brother 2 Brothers mentoring" class="img-responsive"></div>
                              </div>
                           </div>
                           		 
                           <hr class="styled_2">	
                           		
                           <h3>Locker Room Series</h3>
                           	
                           <p>The locker room has always been a safe haven for young men where candid conservations
                              on meaningful topics can be discussed in confidence. Inspired by that kind of real-talk
                              environment, Brother 2 Brothers has adopted the locker room model to offer a safe
                              space for male students to talk about the issues important to them - from current
                              events to personal finances.
                           </p>
                           	
                           <p>Throughout the Locker Room Series, Brother 2 Brothers aims to inspire mentees by following
                              tried-and-true principles from Valencia's Peace and Justice Institute:
                           </p>
                           	
                           <p>
                              <ul>
                                 		
                                 <li>Place collaborative relationship building as central to the work</li>
                                 		
                                 <li>Encourage a reflective practice to support meaning and purpose (mindfulness practice,
                                    emotional intelligence)
                                 </li>
                                 		
                                 <li>Address conflict as a source and opportunity for growth and transformation</li>
                                 		
                                 <li>Use the tools of dialogue and conversation (introduces the College's Principles for
                                    How We Treat Each Other)
                                 </li>
                                 		
                                 <li>Support an inclusive community in which all voices are heard and valued</li>
                                 		
                                 <li>Engage in the exploration of the "other" in acknowledgment of our inherent interdependence</li>
                                 		
                                 <li>Recognize that there can be no sustaining peace without justice for all</li>
                                 	
                              </ul>
                           </p>
                           			 
                           <hr class="styled_2">	
                           		
                           <h3>Mentee Card</h3>
                           		
                           <p>Mentees will be given cards to share with their professors on the first day of class.
                              These cards will inform teachers of the Brother 2 Brothers program and the commitments
                              our mentees will make as a condition of their participation, in order to help hold
                              them accountable for their behavior and their performance.
                           </p>
                           			
                           <hr class="styled_2">	
                           			<br>
                           		
                           <div class="container margin-60">
                              <div class="main-title">
                                 						
                                 <h2>Mentor/Mentee Relationships</h2>
                                 						
                                 <p>Finding common ground with your mentee and establishing trust in both directions.</p>
                                 					<img style="display: block; margin-left: auto; margin-right: auto;" src="/students/brother-2-brothers/images/brother-2-brothers-pin.png" alt="Brother 2 Brothers pin" class="img-responsive" align="center"></div>
                              						
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       										
                                       <h3>Stage 1</h3>
                                       										
                                       <p>When you are first matched with your mentee, there will naturally be a feeling-out
                                          period wherein you each learn to effectively communicate with the other. As first
                                          impressions are made and the positive aspects of the relationship are reinforced through
                                          preliminary conversations, a bonding process will begin. Finding common ground with
                                          your mentee and establishing trust in both directions will benefit the prescribed
                                          mentoring process that will begin in earnest in Stage 2.
                                       </p>
                                       									
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       										
                                       <h3>Stage 2</h3>
                                       									  
                                       <p>As you begin the prescribed mentoring process through scheduled sessions and collaboration
                                          with the entire Brother 2 Brothers family, you and your mentee will begin to challenge
                                          and test each other with the goal of allowing the mentee to consider his actions and
                                          attitudes more deeply. This may involve rethinking his first impressions of you, and
                                          vice versa, and in so doing may awaken difficult emotions. This should be embraced
                                          as a transformative process for both of you and leveraged as a step toward becoming
                                          more complete individuals.
                                       </p>
                                       									
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       										
                                       <h3>Stage 3</h3>
                                       									
                                       <p>As you and your mentee achieve a comfort with the prescribed mentoring process, you
                                          will begin to transition into a phase of natural, or "real" mentoring. Your relationship
                                          will move to a more equal footing from the more prescriptive dynamic you had established
                                          previously, and a bond between you will develop as trust flourishes.
                                       </p>
                                       									
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="box_style_2">
                                       									
                                       <h3>Stage 4</h3>
                                       									   
                                       <p>As your mentee prepares to graduate or transfer, your role as a mentor will be to
                                          help prepare him for the end of your mentoring relationship, as well as the challenges
                                          of the next phase of his life. This may lead to a deeper relationship between you,
                                          as the two of you will interact as peers regarding the mentee's future.
                                       </p>
                                       									
                                    </div>
                                 </div>
                              </div>
                              					
                           </div>
                           		
                           			
                           		
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/brother-2-brothers/mentee.pcf">©</a>
      </div>
   </body>
</html>