<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Brother 2 Brothers | Valencia College</title>
      <meta name="Description" content="Brother 2 Brothers">
      <meta name="Keywords" content="brother2brothers, brother 2 brothers, B2B, valencia college, educational, brother, brothers">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/brother-2-brothers/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/brother-2-brothers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Brother 2 Brothers </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Brother 2 Brothers </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="main-title">
                           <td class="main-title">
                              	<img src="/students/brother-2-brothers/images/brother-2-brothers-logo.png" alt="Brother 2 Brothers Logo"><br><br>
                              	
                              <h2>Brother 2 Brothers</h2>
                              
                              <p><span>Our mission is to empower and transform young men through mentoring in a collaborative
                                    culture dedicated to achievement and personal excellence.</span></p>
                              
                           </td>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-8 col-sm-8">
                              
                              <h3>Overview</h3>
                              	
                              <p>Brother 2 Brothers is a male mentoring program designed to help students discover
                                 their identity, overcome life's obstacles and achieve academic success with the help
                                 of a mentor who may share similar experiences. Through the combined effort of mentor
                                 and mentee, this program can help male students find the strength they need to become
                                 the men they want to be for themselves, their families and their communities.
                              </p>
                              	
                              <p>While the program includes structured, scheduled sessions to address common student
                                 challenges - such as student debt and financial planning - the program will also help
                                 participants develop genuine friendships and relationships with their mentors as mentor-mentee
                                 combinations share their histories and work toward a brighter future.
                              </p>
                              
                              
                              
                              <h3>Vision statement</h3>
                              	
                              <p>Brother 2 Brothers is committed to establishing meaningful learning relationships
                                 between men in higher education in order to transform lives and uplift individuals
                                 to excellence. B2B is also dedicated to removing barriers and creating connections,
                                 from the personal to the spiritual, that help to align aspirations and ambition with
                                 the knowledge and resources needed to achieve them. Through community partnerships,
                                 behavioral standards and academic accountability, our scholars will ascend to the
                                 highest levels of achievement.
                              </p>
                              
                              
                              <ul class="list_staff">
                                 <li>
                                    <figure><img src="/students/brother-2-brothers/images/sedrick-brinson.png" alt="Sedrick Brinson" class="img-circle"></figure>
                                    <h4>Sedrick Brinson</h4>
                                    <p>B2B Mentoring Program Chair</p>
                                 </li>
                              </ul>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <div class="box_style_4">
                                 
                                 <h4>Our Goals</h4>
                                 
                                 <p>Through community partnerships, behavioral standards and academic accountability,
                                    our scholars will ascend to the highest levels of achievement.
                                 </p>
                                 
                                 <ul class="list_order">
                                    <li><span>1</span>Develop students' moral character
                                    </li>
                                    <li><span>2</span>Help students discover their identity
                                    </li>
                                    <li><span>3</span>Foster a sense of community that will connect students to their purpose and their
                                       future
                                    </li>
                                    <li><span>4</span>Establish a sense of ownership
                                    </li>
                                    <li><span>5</span>Soar to the highest levels of achievement
                                    </li>
                                 </ul>
                                 
                              </div>
                           </div>
                        </div>
                        
                        <hr class="more_margin">
                        
                        
                        
                        <div class="main-title">
                           <td class="main-title">
                              
                              <h2>Representation of Colors</h2>
                              
                              <p><span>Each color corresponds to a different stage of the program</span></p>
                              
                           </td>
                        </div>
                        
                        <table class="table ou-one-column">
                           <caption><img style="display: block; margin-left: auto; margin-right: auto;" src="/students/brother-2-brothers/images/brother-2-brothers-pin.png" alt="Brother 2 Brothers pin" width="132" height="132" align="center"></caption>
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th></th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td class="main-column">
                                    
                                    <p>The three colors woven throughout the Brother 2 Brothers experience were selected
                                       to represent the journey of self-discovery and the long road to success that each
                                       mentee in the program will tackle. Each color corresponds to a different stage of
                                       the program as students experience the transformation from someone who is lost to
                                       someone who embodies academic and personal achievement.
                                    </p>
                                    
                                    <p><strong>Black:</strong>&nbsp;Representing the first stage of the program, black represents the fear, anger and
                                       animosity of a student who needs guidance to navigate college and the world of challenges
                                       before them.
                                    </p>
                                    
                                    <p><span style="color: purple;"><strong>Purple:</strong></span>&nbsp;The color purple represents mentees' transformation. In this case, purple is symbolic
                                       of the brightening of the darkness of the previous stage and signifies nobility, enlightenment,
                                       wisdom and honor.
                                    </p>
                                    
                                    <p><span style="color: gold;"><strong>Gold:</strong></span>&nbsp;Signifying the final stage of the program, gold represents the pinnacle of personal
                                       and academic achievement. At this stage, mentees will have become men who have achieved
                                       their goals and are ambassadors of Brother 2 Brothers' mission, vision and goals.
                                    </p>
                                    
                                 </td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        	  
                        <div class="container margin-30">
                           
                           <h4 class="stories"><strong>photo gallery</strong></h4>
                           
                        </div>
                        <div class="grid">
                           
                           <ul class="magnific-gallery">
                              <li>
                                 <figure><img src="/students/brother-2-brothers/images/brother-2-brothers-gallery1-thumb.png" alt="Image Gallery Picture 1"><figcaption>
                                       <div class="caption-content"><a href="/students/brother-2-brothers/images/brother-2-brothers-gallery1.png" title="Brother 2 Brothers audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Brother 2 Brothers </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                              <li>
                                 <figure><img src="/students/brother-2-brothers/images/brother-2-brothers-gallery2-thumb.png" alt="Image Gallery Picture 2"><figcaption>
                                       <div class="caption-content"><a href="/students/brother-2-brothers/images/brother-2-brothers-gallery2.png" title="Brother 2 Brothers audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Brother 2 Brothers </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                              <li>
                                 <figure><img src="/students/brother-2-brothers/images/brother-2-brothers-gallery3-thumb.png" alt="Image Gallery Picture 3"><figcaption>
                                       <div class="caption-content"><a href="/students/brother-2-brothers/images/brother-2-brothers-gallery3.png" title="Brother 2 Brothers audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Brother 2 Brothers </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                              <li>
                                 <figure><img src="/students/brother-2-brothers/images/brother-2-brothers-gallery4-thumb.png" alt="Image Gallery Picture 4"><figcaption>
                                       <div class="caption-content"><a href="/students/brother-2-brothers/images/brother-2-brothers-gallery4.png" title="Brother 2 Brothers audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Brother 2 Brothers </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                           </ul>
                           
                        </div>
                        
                        <hr>
                        
                        <h3>Questions?</h3>
                        
                        <p><a href="mailto:b2b@valenciacollege.edu">b2b@valenciacollege.edu</a></p>
                        
                        <hr>
                        
                        <h3>Resources</h3>
                        
                        <p><a class="icon-acrobat" href="/students/brother-2-brothers/documents/lunch-and-learn-handout-02-21-18.pdf" target="_blank">LUNCH &amp; LEARN</a><br> <a href="/students/brother-2-brothers/documents/beverly-tatum-ad.pdf" target="_blank">THE INCLUSIVE EXCELLENCE SPEAKER SERIES PRESENTS - Beverly Tatum Ph.D. Flyer</a></p>
                        	
                     </div>
                     	  
                  </div>
                  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/brother-2-brothers/index.pcf">©</a>
      </div>
   </body>
</html>