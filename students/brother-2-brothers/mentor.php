<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Mentor | Valencia College</title>
      <meta name="Description" content="Brother 2 Brothers">
      <meta name="Keywords" content="brother2brothers, brother 2 brothers, B2B, valencia college, educational, brother, brothers">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/brother-2-brothers/mentor.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/brother-2-brothers/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Brother 2 Brothers </h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/brother-2-brothers/">Brother 2 Brothers </a></li>
               <li>Mentor </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="box_style_1">
                           
                           <h2>Mentor</h2>
                           		
                           <h3>Mentor Commitments and Expectations</h3>
                           		
                           <p>
                              <ul> <img src=" /students/brother-2-brothers/images/brother-2-brothers-logo.png" alt="Brother 2 Brothers Logo" class="img-responsive" align="right">
                                 		
                                 <li>Maximum of two (2) mentees</li>
                                 		
                                 <li>Bi-weekly meetings with mentee to include Prescribed and Natural conversation (mandatory)</li>
                                 		
                                 <li>Monthly group meetings (optional)</li>
                                 		
                                 <li>Make yourself easily accessible to your mentee</li>
                                 		
                                 <li>Keep shared information confidential unless it is illegal or threatening to the student
                                    or others
                                 </li>
                                 		
                                 <li>Respect differences with your mentee and others, and demand the same of him</li>
                                 		
                                 <li>Assist your mentee in achieving goals that are meaningful to him and contribute to
                                    his learning and career plans
                                 </li>
                                 		
                                 <li>Be an example for your mentee by demonstrating appropriate dress and behavior</li>
                                 		
                                 <li>DO NOT give any form of medication (prescription or over the counter) to mentee</li>
                                 		
                                 <li>DO NOT transport any student in your personal vehicle</li>
                                 	
                              </ul>
                           </p>
                           	
                           		
                           <hr class="styled_2">
                           	 	
                           <h3>Communicate, Prepare and Recover (CPR)</h3>
                           	
                           <h4>CPR Goal</h4>
                           	
                           <p>CPR is a way of supporting students through any past obstacles they might otherwise
                              find insurmountable. This system was developed by Brother 2 Brothers not only to help
                              ensure students' success, but also to help them feel valued. Traditional terms like
                              "probation" can have very different meanings to individuals based on their unique
                              experiences, so we've sought to frame similar ideas in a more positive light. By performing
                              CPR, we seek to revive students in their time of greatest need and lead them to the
                              support, structure and resources that will help get them back on track.
                           </p>
                           	
                           <h4>B2B CPR</h4>
                           	
                           <p>CPR is an emergency response in which students' attitudes and aspirations are rehabilitated
                              through the implementation of a structured support system. This encourages success
                              in all aspects of their lives and academic careers. In our usage, CPR stands for Communicate,
                              Prepare and Recover.
                           </p>
                           	
                           <p></p>
                           	
                           <ul>
                              		
                              <li><strong>Communicate:</strong> Mentors will be aware of students' progress and will be able to offer immediate intervention
                                 if their GPA falls below a 2.5. Mentors will not only offer students the tools and
                                 resources they need to get back on track academically, but they will engage the students
                                 in a conversation to determine the cause of their diminished performance.
                              </li>
                              		
                              <li><strong>Prepare:</strong> Along with the mentoring conversation, students will be required to attend tutoring
                                 based on their academic needs twice weekly and have each visit signed off on by a
                                 Valencia employee in the academic resource area where they were assisted. This will
                                 help prevent the same issue from occurring again.
                              </li>
                              		
                              <li><strong>Recover:</strong> Once students have rehabilitated their academic performance, we will celebrate their
                                 diligent work as well as help them identify strategies to be more successful in the
                                 future.
                              </li>
                              	
                           </ul>
                           	
                           <p></p>
                           			
                           
                           <hr class="styled_2">
                           			
                           		
                           <div class="container margin-60">
                              <div class="row">
                                 <div class="col-md-6">
                                    <div class="wrapper_indent">
                                       						
                                       <h3 class="v-red cap">Establishing and Maintaining Boundaries</h3>
                                       					
                                       <p>Boundaries are important in every relationship, and especially so in a mentoring relationship
                                          where your mentee will depend on you to model the behaviors he needs to adopt. By
                                          establishing consistent boundaries early in your conversations with your mentee and
                                          applying them consistently throughout your interactions, you provide him with a head
                                          start on achieving the personal growth he seeks, as well as protecting yourself from
                                          unforeseen outcomes and challenging your expectations.
                                       </p>
                                       
                                    </div>
                                 </div>
                                 <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;"><img src="/students/brother-2-brothers/images/brother-2-brothers-mentoring-mentor.jpg" alt="Brother 2 Brothers mentoring" class="img-responsive"></div>
                              </div>
                           </div>
                           		 
                           <hr class="styled_2">	
                           		
                           <h3>Mentee Card</h3>
                           		
                           <p>Mentees will be given cards to share with their professors on the first day of class.
                              These cards will inform teachers of the Brother 2 Brothers program and the commitments
                              our mentees will make as a condition of their participation, in order to help hold
                              them accountable for their behavior and their performance.
                           </p>
                           			
                           <hr class="styled_2">	
                           			<br>
                           		
                           <div class="container margin-60">
                              <div class="main-title">
                                 						
                                 <h2>Mentor/Mentee Relationships</h2>
                                 						
                                 <p>Finding common ground with your mentee and establishing trust in both directions.</p>
                                 					<img style="display: block; margin-left: auto; margin-right: auto;" src="/students/brother-2-brothers/images/brother-2-brothers-pin.png" alt="Brother 2 Brothers pin" class="img-responsive" align="center"></div>
                              						
                              <div class="row">
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       										
                                       <h3>Stage 1</h3>
                                       										
                                       <p>When you are first matched with your mentee, there will naturally be a feeling-out
                                          period wherein you each learn to effectively communicate with the other. As first
                                          impressions are made and the positive aspects of the relationship are reinforced through
                                          preliminary conversations, a bonding process will begin. Finding common ground with
                                          your mentee and establishing trust in both directions will benefit the prescribed
                                          mentoring process that will begin in earnest in Stage 2.
                                       </p>
                                       									
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       										
                                       <h3>Stage 2</h3>
                                       									  
                                       <p>As you begin the prescribed mentoring process through scheduled sessions and collaboration
                                          with the entire Brother 2 Brothers family, you and your mentee will begin to challenge
                                          and test each other with the goal of allowing the mentee to consider his actions and
                                          attitudes more deeply. This may involve rethinking his first impressions of you, and
                                          vice versa, and in so doing may awaken difficult emotions. This should be embraced
                                          as a transformative process for both of you and leveraged as a step toward becoming
                                          more complete individuals.
                                       </p>
                                       									
                                    </div>
                                 </div>
                                 <div class="col-md-4">
                                    <div class="box_style_2">
                                       										
                                       <h3>Stage 3</h3>
                                       									
                                       <p>As you and your mentee achieve a comfort with the prescribed mentoring process, you
                                          will begin to transition into a phase of natural, or "real" mentoring. Your relationship
                                          will move to a more equal footing from the more prescriptive dynamic you had established
                                          previously, and a bond between you will develop as trust flourishes.
                                       </p>
                                       									
                                    </div>
                                 </div>
                              </div>
                              <div class="row">
                                 <div class="col-md-12">
                                    <div class="box_style_2">
                                       									
                                       <h3>Stage 4</h3>
                                       									   
                                       <p>As your mentee prepares to graduate or transfer, your role as a mentor will be to
                                          help prepare him for the end of your mentoring relationship, as well as the challenges
                                          of the next phase of his life. This may lead to a deeper relationship between you,
                                          as the two of you will interact as peers regarding the mentee's future.
                                       </p>
                                       									
                                    </div>
                                 </div>
                              </div>
                              					
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/brother-2-brothers/mentor.pcf">©</a>
      </div>
   </body>
</html>