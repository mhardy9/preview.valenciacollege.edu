<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/about-us.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>About Us</h2>
                        
                        <p>The Internship and Workforce Services team is dedicated to educating and preparing
                           Valencia students for Central Florida's dynamic job market. We are experts in the
                           industry, working to identify employment trends and how they will impact career choices
                           for students.&nbsp;
                        </p>
                        
                        <h3 dir="auto">Mission</h3>
                        
                        <p dir="auto">The mission of Internship &amp; Workforce Services is to facilitate a process for students
                           to engage in work-based learning experiences that allows them to integrate knowledge
                           and meaningful work experience through internships that prepare them for employment
                           after graduation.
                        </p>
                        
                        <p dir="auto">We partner with employers to provide high quality internship opportunities for students
                           to apply principles and theories learned in the classroom, develop essential work-place
                           skills, explore careers, and develop professional contacts.
                        </p>
                        
                        <h3>Services Offered</h3>
                        
                        <ul>
                           
                           <li>Assistance and resources to students to secure internships related to their major.</li>
                           
                           <li>Facilitate the process to enroll students in internship courses for academic credit.</li>
                           
                           <li>Maintain a Career Service Manager (Navigator) that students use to search for internships,
                              and part-time and full-time jobs.
                           </li>
                           
                           <li>Host job fairs, on-campus recruitment, internship-focused employer panels, information
                              sessions, and other professional networking events that provide opportunities for
                              students to connect directly with employers.
                           </li>
                           
                           <li>Provide several options for employers to engage with our talented students for internships
                              and employment opportunities, and professional development.
                           </li>
                           
                        </ul>
                        
                        <h3>Professional Affiliations</h3>
                        
                        <ul>
                           
                           <li>Cooperative Education &amp; Internship Association (CEIA) - <a href="http://www.ceiainc.org/" target="_blank" rel="nofollow noreferrer">http://www.ceiainc.org</a></li>
                           
                           <li>National Association of Colleges &amp; Employers (NACE) - <a href="http://www.naceweb.org/" target="_blank" rel="nofollow noreferrer">http://www.naceweb.org</a></li>
                           
                           <li>National Society for Experiential Education (NSEE) - <a href="http://www.nsee.org/" target="_blank" rel="nofollow noreferrer">http://www.nsee.org</a></li>
                           
                           <li>Southern Association of Colleges &amp; Employers (SoACE) - <a href="http://www.soace.org/" target="_blank" rel="nofollow noreferrer">http://www.soace.org</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/about-us.pcf">©</a>
      </div>
   </body>
</html>