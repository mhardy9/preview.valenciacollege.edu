<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/internship-fair.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub-header bg-interior">
         <div id="intro-txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Internship &amp; Part-time Job Fair Information</h2>
                        
                        <p><strong>Thursday, October 26, 2017<br> 10:00 am - 2:00 pm<br> Valencia College West Campus, Special Events Center</strong></p>
                        
                        <p><a href="/students/internship/students/job-fairs.html">Information for Students</a></p>
                        
                        <p><a href="/students/internship/employers/employer-information.html">Information for Employers</a></p>
                        
                        <h2>Sponsors</h2>
                        
                        <p>The Internship and Workforce Services office at Valencia College would like to thank
                           all of our generous sponsors who support the 2017 Internship &amp; Part-Time Job Fair.
                           We are extremely grateful for their assistance in making this event possible!
                        </p>
                        
                        <h3>Gold</h3>
                        
                        <h3>Silver</h3>
                        
                        <h2>&nbsp;</h2>
                        
                        <h3>Some Participating Employers and Areas of Recruitment</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">&nbsp;</div>
                              
                              <div data-old-tag="tr">&nbsp;</div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div class="banner"><i class="iconcustom-school"></i>
                                    
                                    <h3>Take a campus tour</h3>
                                    
                                    <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.</p>
                                    <a href="/academics/tour.html" target="" class="banner_bt">Start tour</a></div>
                                 
                                 <div class="bg-content magnific" style="url(/_resources/images/testimonial_1.jpg) no-repeat center center;">
                                    <div>
                                       <h3>Discover the Campus</h3>
                                       <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.</p>
                                    </div>
                                 </div>
                                 
                                 <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                       <a class="box_feat" href="/" target=""><i class="far fa-users"></i><h3>Qualified teachers</h3>
                                          <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                             cum no alii option, cu sit mazim libris.
                                          </p></a>
                                       
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                       <a class="box_feat" href="/" target=""><i class="far fa-desktop"></i><h3>Equipped class rooms</h3>
                                          <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                             cum no alii option, cu sit mazim libris.
                                          </p></a>
                                       
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                       <a class="box_feat" href="/" target=""><i class="far fa-cubes"></i><h3>Advanced teaching</h3>
                                          <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                             cum no alii option, cu sit mazim libris.
                                          </p></a>
                                       
                                    </div>
                                 </div>
                                 <div class="row">
                                    <div class="col-md-4 col-sm-4">
                                       <a class="box_feat" href="/" target=""><i class="far fa-rocket"></i><h3>Advanced study plans</h3>
                                          <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                             cum no alii option, cu sit mazim libris.
                                          </p></a>
                                       
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                       <a class="box_feat" href="/" target=""><i class="far fa-bullseye"></i><h3>Focus on target</h3>
                                          <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                             cum no alii option, cu sit mazim libris.
                                          </p></a>
                                       
                                    </div>
                                    <div class="col-md-4 col-sm-4">
                                       <a class="box_feat" href="/" target=""><i class="far fa-graduation-cap"></i><h3>Focus on success</h3>
                                          <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                             cum no alii option, cu sit mazim libris.
                                          </p></a>
                                       
                                    </div>
                                 </div>
                                 
                                 <div class="row">
                                    <div class="col-md-5">
                                       <ul class="list_4">
                                          <li>Putent vocibus</li>
                                          <li>Inani sapientem sit</li>
                                          <li>Nonumes maluisset delicatissimi</li>
                                       </ul>
                                    </div>
                                    <div class="col-md-5">
                                       <ul class="list_4">
                                          <li>Putent vocibus</li>
                                          <li>Inani sapientem sit</li>
                                          <li>Nonumes maluisset delicatissimi</li>
                                       </ul>
                                    </div>
                                 </div>
                                 
                                 <div>
                                    <h4 class="v-red cap"><strong>Contact us</strong></h4>
                                    <p>Visit us on campus at the Answer Center.</p>
                                    <div>
                                       <h5 class="val">Locations</h5><i class="far fa-map-marker fa-spacer"></i>East Building 5, Room 211<br><i class="far fa-map-marker fa-spacer"></i>Lake Nona Building 1, Room 149<br><i class="far fa-map-marker fa-spacer"></i>Osceola Building 2, Room 150<br><i class="far fa-map-marker fa-spacer"></i>West SSB, Room 106<br><i class="far fa-map-marker fa-spacer"></i>Winter Park Building 1, Room 210<br><h5 class="val">Hours</h5><i class="far fa-calendar fa-spacer"></i>Mon. – Thurs. 8 a.m. – 6 p.m.<br><i class="far fa-calendar fa-spacer"></i>Friday 9 a.m. – 5 p.m.<br><br><i class="far fa-envelope fa-spacer"></i>enrollment@valenciacollege.edu<br><i class="far fa-phone fa-spacer"></i>407-582-1507<br></div>
                                 </div>
                                 
                                 <ul class="ou-course-list">
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Mathemacis</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_2_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Sciences</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_3_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Litterature</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_4_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Arts</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                    <li>
                                       <div><a href="/" target="" title="">
                                             <figure><img src="/_resources/images/course_5_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                             <h3><strong>Music</strong> diploma
                                             </h3><small>Start 3 October 2015</small></a></div>
                                    </li>
                                 </ul>
                                 
                                 <table class="table table-striped add_bottom_30">
                                    <thead>
                                       <tr>
                                          <th>Discipline</th>
                                          <th>Course Name</th>
                                          <th>Course Number</th>
                                       </tr>
                                    </thead>
                                    <tbody>
                                       <tr>
                                          <td>Mathematics</td>
                                          <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201730&amp;subj_in=MAC&amp;crse_in=1114&amp;crn_in=30376">College Trigonometry</a></td>
                                          <td>MAC1114</td>
                                       </tr>
                                       <tr>
                                          <td>Mathematics</td>
                                          <td><a href="https://ssb-01.valenciacollege.edu:9050/pls/PROD/bwckschd.p_disp_listcrse?term_in=201730&amp;subj_in=STA&amp;crse_in=2023&amp;crn_in=32256">Statistical Methods</a></td>
                                          <td>STA2023</td>
                                       </tr>
                                    </tbody>
                                 </table>
                                 
                                 <div class="list_courses_tabs">
                                    <h2>Diploma Courses</h2>
                                    <ul>
                                       
                                       <li>
                                          <div><a href="/" target="" title="">
                                                <figure><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                                <h3><strong>Mathemacis</strong> diploma
                                                </h3><small>Start 3 October 2015</small></a></div>
                                       </li>
                                       <li>
                                          <div><a href="/" target="" title="">
                                                <figure><img src="/_resources/images/course_2_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                                <h3><strong>Sciences</strong> diploma
                                                </h3><small>Start 3 October 2015</small></a></div>
                                       </li>
                                       <li>
                                          <div><a href="/" target="" title="">
                                                <figure><img src="/_resources/images/course_3_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                                <h3><strong>Litterature</strong> diploma
                                                </h3><small>Start 3 October 2015</small></a></div>
                                       </li>
                                       <li>
                                          <div><a href="/" target="" title="">
                                                <figure><img src="/_resources/images/course_4_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                                <h3><strong>Arts</strong> diploma
                                                </h3><small>Start 3 October 2015</small></a></div>
                                       </li>
                                       <li>
                                          <div><a href="/" target="" title="">
                                                <figure><img src="/_resources/images/course_5_thumb.jpg" alt="Valencia image description" class="img-rounded"></figure>
                                                <h3><strong>Music</strong> diploma
                                                </h3><small>Start 3 October 2015</small></a></div>
                                       </li>
                                       
                                       <li>
                                          <div><a href="/" target="" title="" class="link-normal">View all Diploma courses</a></div>
                                       </li>
                                    </ul>
                                 </div>
                                 
                                 <div class="row"><span class="date_stacked col-md-2">09 <span>MAR</span></span><span>Proof of Florida Residency Deadline</span><span><a class="button-outline_small" href="/" target="">submit residency</a></span></div>
                                 
                                 <div class="deadlines">
                                    <h4 style="text-transform: uppercase; font-weight: 600">Important Deadlines</h4>
                                    
                                    <div class="row">
                                       <div class="date_stacked col-md-2">20 <span>Feb</span></div>
                                       <div class="col-md-10">Advanced Registration Begins Returning Students</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">09 <span>MAR</span></div>
                                       <div class="col-md-10">Open Registration Begins for New and Returning Students</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">13 <span>MAR</span></div>
                                       <div class="col-md-10">Financial Priority Deadline</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">19 <span>MAR</span></div>
                                       <div class="col-md-10">Fee Payment Deadline: FRIDAY 5:00 P.M.</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">23 <span>APR</span></div>
                                       <div class="col-md-10">Late Registration Begins: Fees Assessed With Initial Enroll</div>
                                    </div>
                                    
                                 </div>
                                 
                                 <div class="row">
                                    <div class="date_stacked col-md-2">20 <span>Feb</span></div>
                                    <div class="col-md-10">Advanced Registration Begins Returning Students</div>
                                 </div>
                                 <hr>
                                 <div class="row">
                                    <div class="date_stacked col-md-2">09 <span>MAR</span></div>
                                    <div class="col-md-10">Open Registration Begins for New and Returning Students</div>
                                 </div>
                                 <hr>
                                 <div class="row">
                                    <div class="date_stacked col-md-2">13 <span>MAR</span></div>
                                    <div class="col-md-10">Financial Priority Deadline</div>
                                 </div>
                                 <hr>
                                 <div class="row">
                                    <div class="date_stacked col-md-2">19 <span>MAR</span></div>
                                    <div class="col-md-10">Fee Payment Deadline: FRIDAY 5:00 P.M.</div>
                                 </div>
                                 <hr>
                                 <div class="row">
                                    <div class="date_stacked col-md-2">23 <span>APR</span></div>
                                    <div class="col-md-10">Late Registration Begins: Fees Assessed With Initial Enroll</div>
                                 </div>
                                 
                                 <div class="row">
                                    <div class="col-md-6">
                                       <ul class="list_3">
                                          <li><strong>D E D Higher Ed Administration</strong><p>
                                                								University of Florida
                                                							
                                             </p>
                                          </li>
                                          <li><strong>M Ed Mathematics</strong><p>
                                                								Campbell University
                                                							
                                             </p>
                                          </li>
                                       </ul>
                                    </div>
                                 </div>
                                 
                                 <p><img src="/_resources/images/locations.jpg" alt="Campus Area" class="img-responsive"></p>
                                 <h4>Campus Area</h4>
                                 <p>Lorem ipsum dolor sit amet, ne vis suas harum nonumy, at enim vocent delicatissimi
                                    eos, vocent inermis veritus mel no. Vix in offendit forensibus cotidieque.
                                 </p>
                                 
                                 <div class="container_gray_bg" id="home-feat-1">
                                    <div class="container">
                                       <div class="row">
                                          <div class="col-md-4 col-sm-4">
                                             
                                             <div class="home-feat-1_box"><a href="/" target="" title=""><img src="/_resources/images/home-feat-1_1.jpg" alt="Valencia image description" class="img-responsive"><div class="short_info">
                                                      <h3>Plan a visit</h3><i class="arrow_carrot-right_alt2"></i></div></a></div>
                                             
                                          </div>
                                          <div class="col-md-4 col-sm-4">
                                             
                                             <div class="home-feat-1_box"><a href="/" target="" title=""><img src="/_resources/images/home-feat-1_2.jpg" alt="Valencia image description" class="img-responsive"><div class="short_info">
                                                      <h3>Study Programs</h3><i class="arrow_carrot-right_alt2"></i></div></a></div>
                                             
                                          </div>
                                          <div class="col-md-4 col-sm-4">
                                             
                                             <div class="home-feat-1_box"><a href="/" target="" title=""><img src="/_resources/images/home-feat-1_3.jpg" alt="Valencia image description" class="img-responsive"><div class="short_info">
                                                      <h3>Admissions</h3><i class="arrow_carrot-right_alt2"></i></div></a></div>
                                             
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div class="container margin-60">
                                    <div class="main-title">
                                       
                                       <h2>Frequently questions</h2>
                                       
                                       <p>Cum doctus civibus efficiantur in. Nec id tempor imperdiet deterruisset.</p>
                                       
                                    </div>
                                    
                                    <div class="row">
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Et ius tota recusabo democritum?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Posse exerci volutpat has?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Te pri facete latine salutandi?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Et ius tota recusabo democritum?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Mediocritatem sea ex, nec id agam?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Te pri facete latine salutandi?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="row">
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Et ius tota recusabo democritum?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Posse exerci volutpat has?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Te pri facete latine salutandi?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Et ius tota recusabo democritum?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Mediocritatem sea ex, nec id agam?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-md-4">
                                          <div class="box_style_2">
                                             
                                             <h3>Te pri facete latine salutandi?</h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate.
                                                Ne his sint graeco detraxit, posse exerci volutpat has in.
                                             </p>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="container-fluid container_gray_bg">
                                       <div class="container margin-60">
                                          <div class="row">
                                             <div class="col-md-3">
                                                <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/" target="">
                                                   <p><img src="" alt="" class="img-responsive"></p>
                                                   <h4>Virtual Tour</h4>
                                                   <p>Don't have time to visit a Valencia Campus? Take our virtual tour to see all that
                                                      Valencia College has to offer.
                                                   </p></a>
                                                
                                             </div>
                                             <div class="col-md-3"><a href="http://www.youvisit.com/tour/valencia/valenciaeast?tourid=tour1&amp;pl=v&amp;m_prompt=1"><img class="img-responsive fullwidth" src="/students/internship/img/home/virtual-tour-east.jpg" alt="Virtual Tour East Campus"></a></div>
                                             <div class="col-md-3"><a href="http://www.youvisit.com/tour/valencia/osceola?tourid=tour1&amp;pl=v&amp;m_prompt=1"><img class="img-responsive fullwidth" src="/students/internship/img/home/virtual-tour-osceola.jpg" alt="Virtual Tour Osceola Campus"></a></div>
                                             <div class="col-md-3"><a href="http://www.youvisit.com/tour/valencia/valenciawest?tourid=tour1&amp;pl=v&amp;m_prompt=1"><img class="img-responsive fullwidth" src="/students/internship/img/home/virtual-tour-west.jpg" alt="Virtual Tour West Campus"></a></div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <ul class="magnific-gallery">
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_8.jpg" alt="Image Gallery Picture 8"><figcaption>
                                                <div class="caption-content"><a href="/_resources/images/gallery/large/pic_8.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_9.jpg" alt="Image Gallery Picture 9"><figcaption>
                                                <div class="caption-content"><a href="/_resources/images/gallery/large/pic_9.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_10.jpg" alt="Image Gallery Picture 10"><figcaption>
                                                <div class="caption-content"><a href="/_resources/images/gallery/large/pic_10.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_11.jpg" alt="Image Gallery Picture 1"><figcaption>
                                                <div class="caption-content"><a href="/_resources/images/gallery/large/pic_1.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                    </ul>
                                    
                                    <ul>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_8.jpg" alt="Image Gallery Picture 8"><figcaption>
                                                <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/alumni/ask-an-alum-orange-county-commissioner-emily-bonilla/" target="" title="Story title"><i class="far fa-external-link-square"></i><p> Story Title Link</p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_9.jpg" alt="Image Gallery Picture 9"><figcaption>
                                                <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/madelyn-young-1973-valencia-graduate-recalls-colleges-early-days/" target="" title="Story title"><i class="far fa-external-link-square"></i><p> Story Title Link</p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_10.jpg" alt="Image Gallery Picture 10"><figcaption>
                                                <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/alumni/qa-series-orange-county-commissioner-betsyvanderley/" target="" title="Story title"><i class="far fa-external-link-square"></i><p> Story Title Link</p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/gallery/pic_11.jpg" alt="Image Gallery Picture 1"><figcaption>
                                                <div class="story-content"><a href="http://news.valenciacollege.edu/about-valencia/taking-a-turn-for-the-better-inmates-graduate-from-valencias-construction-training-program/" target="" title="Story title"><i class="far fa-external-link-square"></i><p> Story Title Link</p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                    </ul>
                                    
                                    <div class="col-md-6 col-md-offset-3">
                                       <div id="graph"><img src="/_resources/images/graphic.jpg" class="wow zoomIn" data-wow-delay="0.1s" alt="Valencia image description"><div class="features step_1 wow flipInX" data-wow-delay="1s">
                                             
                                             <h4><strong>01.</strong> Students growth
                                             </h4>
                                             
                                             <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                                                Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                                             </p>
                                             
                                          </div>
                                          <div class="features step_2 wow flipInX" data-wow-delay="1.5s">
                                             
                                             <h4><strong>02.</strong> Best learning practice
                                             </h4>
                                             
                                             <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                                                Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                                             </p>
                                             
                                          </div>
                                          <div class="features step_3 wow flipInX" data-wow-delay="2s">
                                             
                                             <h4><strong>03.</strong> Focus on targets
                                             </h4>
                                             
                                             <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                                                Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                                             </p>
                                             
                                          </div>
                                          <div class="features step_4 wow flipInX" data-wow-delay="2.5s">
                                             
                                             <h4><strong>04.</strong> Interdisciplanary model
                                             </h4>
                                             
                                             <p>Lorem ipsum dolor sit amet, vix erat audiam ei. Cum doctus civibus efficiantur in.
                                                Nec id tempor imperdiet deterruisset, doctus volumus explicari qui ex.
                                             </p>
                                             
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <div class="main-title">
                                       <td class="main-title">
                                          
                                          <h2>Why choose Valencia ...</h2>
                                          
                                          <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                                          
                                       </td>
                                    </div>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <div class="list_news_tabs">
                                       <p><a href="/" target="" title=""><img src="/_resources/images/news_1_thumb.jpg" alt="Valencia image description" class="img-responsive"></a></p><span class="date_published">20 Agusut 2015</span><h3><a href="/" target="" title="">Success Stories for Valencia College in 2015's</a></h3>
                                       <p>Lorem ipsum dolor sit amet, ei tincidunt persequeris efficiantur vel, usu animal patrioque
                                          omittantur et. Timeam nostrud platonem nec ea, simul nihil delectus has ex.
                                       </p><a href="/" target="" title="" class="button small">Read more</a></div>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <ul class="list_order">
                                       <li><span>1</span>At errem clita officiis sed
                                       </li>
                                       <li><span>2</span>Sale inani sapientem sit at
                                       </li>
                                       <li><span>3</span>Deleniti invenire his ei
                                       </li>
                                       <li><span>4</span>Quod unum democritum per no
                                       </li>
                                       <li><span>5</span>Oportere efficiendi eu vim
                                       </li>
                                    </ul>
                                    
                                    <div class="bg-content testimonials">
                                       <div class="row">
                                          <div class="col-md-offset-1 col-md-10">
                                             <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                                                <ol class="carousel-indicators">
                                                   <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                                   <li data-target="#quote-carousel" data-slide-to="1"></li>
                                                   <li data-target="#quote-carousel" data-slide-to="2"></li>
                                                </ol>
                                                <div class="carousel-inner">
                                                   <div class="item active">
                                                      <blockquote>
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                                                            eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                                         </p>
                                                      </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Stefany</small></div>
                                                   <div class="item">
                                                      <blockquote>
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                                                            eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                                         </p>
                                                      </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_2.jpg" alt="Valencia image description">Karla</small></div>
                                                   <div class="item">
                                                      <blockquote>
                                                         <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                                                            eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                                         </p>
                                                      </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Maira</small></div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <div class="container margin-30">
                                       
                                       <h4 class="stories"><strong>photo gallery</strong></h4>
                                       
                                    </div>
                                    <div class="grid">
                                       
                                       <ul class="magnific-gallery">
                                          <li>
                                             <figure><img src="/_resources/images/gallery/pic_8.jpg" alt="Image Gallery Picture 8"><figcaption>
                                                   <div class="caption-content"><a href="/_resources/images/gallery/large/pic_8.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                                </figcaption>
                                             </figure>
                                          </li>
                                          <li>
                                             <figure><img src="/_resources/images/gallery/pic_9.jpg" alt="Image Gallery Picture 9"><figcaption>
                                                   <div class="caption-content"><a href="/_resources/images/gallery/large/pic_9.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                                </figcaption>
                                             </figure>
                                          </li>
                                          <li>
                                             <figure><img src="/_resources/images/gallery/pic_10.jpg" alt="Image Gallery Picture 10"><figcaption>
                                                   <div class="caption-content"><a href="/_resources/images/gallery/large/pic_10.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                                </figcaption>
                                             </figure>
                                          </li>
                                          <li>
                                             <figure><img src="/_resources/images/gallery/pic_11.jpg" alt="Image Gallery Picture 1"><figcaption>
                                                   <div class="caption-content"><a href="/_resources/images/gallery/large/pic_1.jpg" title="Photo title" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p> Your caption </p></a></div>
                                                </figcaption>
                                             </figure>
                                          </li>
                                       </ul>
                                       
                                    </div>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <section class="grid">
                                       <div class="container margin-60">
                                          <div class="main-title">
                                             
                                             <h2>Videos and Photos not full-width</h2>
                                             
                                             <p>Cum doctus civibus efficiantur in imperdiet deterruisset.</p>
                                             
                                          </div>
                                          <div class="row">
                                             
                                             <ul class="magnific-gallery">
                                                <li>
                                                   <figure><img src="/_resources/images/gallery/pic_2.jpg" alt="Valencia image description"><figcaption>
                                                         <div class="caption-content"><a href="https://vimeo.com/45830194" class="video_pop" title="Video Vimeo"><i class="far fa-film"></i><p> Your caption Link</p></a></div>
                                                      </figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure><img src="/_resources/images/gallery/pic_13.jpg" alt="Valencia image description"><figcaption>
                                                         <div class="caption-content"><a href="https://www.youtube.com/watch?v=Zz5cu72Gv5Y" class="video_pop" title="Video Youtube"><i class="far fa-film"></i><p> Your caption Link</p></a></div>
                                                      </figcaption>
                                                   </figure>
                                                </li>
                                                <li>
                                                   <figure><img src="/_resources/images/gallery/pic_4.jpg" alt="Valencia image description"><figcaption>
                                                         <div class="caption-content"><a href="https://vimeo.com/45830194" class="video_pop" title="Video Vimeo"><i class="far fa-film"></i><p> Your caption Link</p></a></div>
                                                      </figcaption>
                                                   </figure>
                                                </li>
                                             </ul>
                                             
                                          </div>
                                       </div>
                                    </section>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <div class="box_style_announcement_sidebar bg-grey">
                                       <div class="indent_title_in">
                                          <p class="add_bottom_30"></p>
                                          
                                          <h4>Announcement can go here.</h4>
                                          
                                          <h4>Search and register for classes.</h4>
                                          
                                       </div>
                                    </div>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <div id="full-slider-wrapper">
                                       <div id="layerslider" style="width:100%;height:650px;">
                                          <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;"><img src="/_resources/images/slides/slide_1.jpg" class="ls-bg" alt="Slide background"><h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Valencia <strong>Excellence</strong> in teaching
                                             </h3>
                                             <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">COLLEGE / UNIVERSITY / CAMPUS</p>
                                             <p class="ls-l" style="top:62%; left:50%;" data-ls="durationin:2000;delayin:1300;easingin:easeOutElastic;"><a class="button-intro" href="/students/internship/tour.html">Take a tour</a> <a class="button-intro outline" href="/students/internship/about.html">About us</a> 
                                             </p>
                                          </div>
                                          <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;"><img src="/_resources/images/slides/slide_2.jpg" class="ls-bg" alt="Slide background"><h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;">Valencia <strong>Qualified</strong> teachers
                                             </h3>
                                             <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">COLLEGE / UNIVERSITY / CAMPUS</p>
                                             <p class="ls-l" style="top:62%; left:50%;" data-ls="durationin:2000;delayin:1300;easingin:easeOutElastic;"><a class="button-intro" href="/students/internship/tour.html">Take a tour</a> <a class="button-intro outline" href="/students/internship/about.html">About us</a> 
                                             </p>
                                          </div>
                                          <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;"><img src="/_resources/images/slides/slide_3.jpg" class="ls-bg" alt="Slide background"><h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;"><strong>Great</strong> students community
                                             </h3>
                                             <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">COLLEGE / UNIVERSITY / CAMPUS</p>
                                             <p class="ls-l" style="top:62%; left:50%;" data-ls="durationin:2000;delayin:1300;easingin:easeOutElastic;"><a class="button-intro" href="/students/internship/tour.html">Take a tour</a> <a class="button-intro outline" href="/students/internship/about.html">About us</a> 
                                             </p>
                                          </div>
                                          <div class="ls-slide" data-ls="slidedelay: 5000; transition2d:5;"><img src="/_resources/images/slides/slide_4.jpg" class="ls-bg" alt="Slide background"><h3 class="ls-l slide_typo" style="top: 45%; left: 50%;" data-ls="offsetxin:0;durationin:2000;delayin:1000;easingin:easeOutElastic;rotatexin:90;transformoriginin:50% bottom 0;offsetxout:0;rotatexout:90;transformoriginout:50% bottom 0;"><strong>Well equiped</strong> classrooms
                                             </h3>
                                             <p class="ls-l slide_typo_2" style="top:52%; left:50%;" data-ls="durationin:2000;delayin:1000;easingin:easeOutElastic;">COLLEGE / UNIVERSITY / CAMPUS</p>
                                             <p class="ls-l" style="top:62%; left:50%;" data-ls="durationin:2000;delayin:1300;easingin:easeOutElastic;"><a class="button-intro" href="/students/internship/tour.html">Take a tour</a> <a class="button-intro outline" href="/students/internship/about.html">About us</a> 
                                             </p>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <ul class="list_staff">
                                       <li>
                                          <figure><img src="/_resources/images/teacher_1_thumb.jpg" alt="Valencia image description" class="img-circle"></figure>
                                          <h4>Maria Hegel</h4>
                                          <p>Director</p>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/teacher_2_thumb.jpg" alt="Valencia image description" class="img-circle"></figure>
                                          <h4>Tomas John</h4>
                                          <p>General Manager</p>
                                       </li>
                                       <li>
                                          <figure><img src="/_resources/images/teacher_3_thumb.jpg" alt="Valencia image description" class="img-circle"></figure>
                                          <h4>Frank Alberti</h4>
                                          <p>Student Manager</p>
                                       </li>
                                    </ul>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <div class="container-fluid container_gray_bg">
                                       <div class="container margin-30">
                                          <div class="row wrapper_indent">
                                             <div class="col-md-2 v-red">STEPS TO ENROLL</div>
                                             <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">1</span><span class="step_to_do_text">Apply for admission and financial aid</span><a href="#" target="" class="link-underline text-center mobile_only">more info</a></div>
                                             <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">2</span><span class="step_to_do_text">Create your ATLAS account</span><a href="#" target="" class="link-underline text-center mobile_only">more info</a></div>
                                             <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">3</span><span class="step_to_do_text">register for classes &amp; pay tuition and fees</span><a href="#" target="" class="link-underline text-center mobile_only">more info</a></div>
                                             <div class="col-md-2 step_to_do_vr"><span class="step_to_do_number">4</span><span class="step_to_do_text">get your students identification and parking decal</span><a href="#" target="" class="link-underline text-center mobile_only">more info</a></div>
                                             <div class="col-md-2"><span class="step_to_do_number">5</span><span class="step_to_do_text">buy your books and go to class</span><a href="#" target="" class="link-underline text-center mobile_only">more info</a></div>
                                          </div>
                                          <div class="row desktop_only">
                                             <div class="col-md-2"></div>
                                             <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                                             <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                                             <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                                             <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                                             <div class="col-md-2"><a href="#" target="" class="step_to_do_more">more info</a></div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                 </div>
                                 
                                 <p>&nbsp;</p>
                                 
                                 <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-sm-4">
                                          <div class="img_list"><a href="/" target=""><img src="/_resources/images/course_1.jpg" alt="Valencia image description"><div class="short_info">
                                                   <h3>MATHEMACIS</h3>
                                                </div></a></div>
                                       </div>
                                       <div class="clearfix visible-xs-block"></div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="course_list_desc">
                                             
                                             <h3><strong>Mathemacis</strong> diploma
                                             </h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate....
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2">
                                          <div class="details_list_col">
                                             <div><a href="/" target="" class="button-outline">Details</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-sm-4">
                                          <div class="img_list"><a href="/" target=""><img src="/_resources/images/course_2.jpg" alt="Valencia image description"><div class="short_info">
                                                   <h3>Sciences</h3>
                                                </div></a></div>
                                       </div>
                                       <div class="clearfix visible-xs-block"></div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="course_list_desc">
                                             
                                             <h3><strong>Sciences</strong> diploma
                                             </h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate....
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2">
                                          <div class="details_list_col">
                                             <div><a href="/" target="" class="button-outline">Details</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-sm-4">
                                          <div class="img_list"><a href="/" target=""><img src="/_resources/images/course_3.jpg" alt="Valencia image description"><div class="short_info">
                                                   <h3>Literature</h3>
                                                </div></a></div>
                                       </div>
                                       <div class="clearfix visible-xs-block"></div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="course_list_desc">
                                             
                                             <h3><strong>Literature</strong> diploma
                                             </h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate....
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2">
                                          <div class="details_list_col">
                                             <div><a href="/" target="" class="button-outline">Details</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-sm-4">
                                          <div class="img_list"><a href="/" target=""><img src="/_resources/images/course_4.jpg" alt="Valencia image description"><div class="short_info">
                                                   <h3>Arts</h3>
                                                </div></a></div>
                                       </div>
                                       <div class="clearfix visible-xs-block"></div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="course_list_desc">
                                             
                                             <h3><strong>Arts</strong> diploma
                                             </h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate....
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2">
                                          <div class="details_list_col">
                                             <div><a href="/" target="" class="button-outline">Details</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="strip_all_courses_list wow fadeIn" data-wow-delay="0.1s">
                                    <div class="row">
                                       <div class="col-lg-4 col-md-4 col-sm-4">
                                          <div class="img_list"><a href="/" target=""><img src="/_resources/images/course_5.jpg" alt="Valencia image description"><div class="short_info">
                                                   <h3>Music</h3>
                                                </div></a></div>
                                       </div>
                                       <div class="clearfix visible-xs-block"></div>
                                       <div class="col-lg-6 col-md-6 col-sm-6">
                                          <div class="course_list_desc">
                                             
                                             <h3><strong>Music</strong> diploma
                                             </h3>
                                             
                                             <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                                                Te pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate....
                                             </p>
                                             
                                          </div>
                                       </div>
                                       <div class="col-lg-2 col-md-2 col-sm-2">
                                          <div class="details_list_col">
                                             <div><a href="/" target="" class="button-outline">Details</a></div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <p>&nbsp;</p>
                                 
                                 <div id="tabs" class="tabs">
                                    <nav>
                                       <ul>
                                          <li><a href="#section-1"><i class="far fa-calendar"></i>Events</a></li>
                                          <li><a href="#section-2"><i class="far fa-newspaper-o"></i>News</a></li>
                                          <li><a href="#section-3"><i class="far fa-users"></i>Social</a></li>
                                       </ul>
                                    </nav>
                                    <div class="content">
                                       <section id="section-1">&nbsp;</section>
                                       <section id="section-2">&nbsp;</section>
                                       <section id="section-3">&nbsp;</section>
                                    </div>
                                 </div>
                                 
                                 <div class="container-fluid">
                                    <div class="container margin-60">
                                       <div class="row">
                                          <div class="col-md-4">&nbsp;column 1 of 3</div>
                                          <div class="col-md-4">column 2 of 3</div>
                                          <div class="col-md-4">column 3 of 3</div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <p>&nbsp;</p>
                                 
                                 <div class="trending"><img src="/_resources/images/home/trending-student-services.jpg" alt="Valencia image description" class="img-responsive"><div class="col-md-6"><a href="#" target="" class="link-underlined">Advising &amp; Counseling</a></div>
                                    <div class="col-md-6"><a href="#" target="" class="link-underlined">Answer Center</a></div>
                                    <div class="col-md-6"><a href="#" target="" class="link-underlined">Assessments</a></div>
                                    <div class="col-md-6"><a href="#" target="" class="link-underlined">Career Center</a></div>
                                    <div class="col-md-6"><a href="#" target="" class="link-underlined">Financial Aid Services</a></div>
                                    <div class="col-md-6"><a href="#" target="" class="link-underlined">Records/Transcripts</a></div>
                                    <div class="col-md-12 v-red"><strong>&gt;<a href="#" target=""> See More Student Services</a></strong></div>
                                 </div>
                                 
                                 <p>&nbsp;</p>
                                 
                                 <div class="container margin-60">
                                    <div class="indent_title_in">
                                       <h4 class="v-red cap">&nbsp;two column</h4>
                                    </div>
                                    <div class="row">
                                       <div class="col-md-6">
                                          <div class="wrapper_indent">column 1</div>
                                       </div>
                                       <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;">column 2</div>
                                    </div>
                                 </div>
                                 
                                 <div class="container-fluid container_gray_bg">
                                    <div class="container margin-30">
                                       <div class="indent_title_in">
                                          <h4 class="v-red cap">Florida residency</h4>
                                       </div>
                                       <div class="row">
                                          <div class="col-md-6">
                                             <div class="wrapper_indent">&nbsp;a1</div>
                                          </div>
                                          <div class="col-md-6" style="padding-left: 3em;margin-bottom:20px;">&nbsp;a2</div>
                                       </div>
                                    </div>
                                 </div>
                                 
                                 <div class="row">
                                    <div class="col-md-8 col-sm-8">&nbsp;main column</div>
                                    <div class="col-md-4 col-sm-4">
                                       <div class="box_style_4">side column</div>
                                    </div>
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        There are no events at this time.
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/internship-fair.pcf">©</a>
      </div>
   </body>
</html>