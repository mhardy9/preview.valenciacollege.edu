<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/pre-requisites.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12"><a id="content" name="content"></a>
                        
                        <h2>Internship Prerequisite Courses</h2>
                        
                        <div>
                           
                           <div>
                              
                              <h3>Accounting Technology (AA) - APA 2941</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>ACG 2021 - Principles of Accounting</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Accounting Technology (AS) - APA 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>APA 1111 - Basic Accounting<br> ACG 2021 - Principles of Accounting
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Anthropology (AA) - ANT 2941</h3>
                              
                              <div>
                                 
                                 <p>12 Credits including ANT 2000;&nbsp;<span>Program Director/Program Chair/Program Coordinator and/or Internship Office approval</span></p>
                                 
                                 <h3>Art and Fine Arts (AA) - ARH 2941</h3>
                                 
                                 <div>
                                    
                                    <p><span>One of the following Studio Art Classes: </span><br><span>ART 1300C - Drawing I or</span><br><span>ART 1201C - Design I or<span>&nbsp;</span></span><br><span>ART 2500C - Painting I or<span>&nbsp;</span></span><br><span>ART 2701C - Scultpure I or</span><br><span>ART 2750C - Ceramics I&nbsp;</span></p>
                                    
                                    <h3>Baking &amp; Pastry Management (AS) - FSS 2943</h3>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <p>FSS 2056 - Pastry Techniques</p>
                                          
                                          <h3>Biology (AA) - BSC 2941</h3>
                                          
                                          <div>
                                             
                                             <p><span>BSC 1010C - Fundamentals of Biology I</span></p>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Building Construction Technology (AA) - BCN 2941</h3>
                              
                              <div>
                                 
                                 <p><span>ETC 1251 - Engineering Materials and Processes</span></p>
                                 
                                 <h3>Building Construction Technology (AS) - BCN 2942</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>ETC 1251 - Engineering Materials and Processes&nbsp;<br>TAR 1120C - Architectural Drawing I
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Business (AA) - GEB 2941</h3>
                              
                              <div>
                                 
                                 <p><span>GEB 1011 - Introduction to Business or</span><br><span>ACG 2021 - Financial Accounting or<span>&nbsp;</span></span><br><span>BUL 2241 - Business Law I</span></p>
                                 
                                 <h3>Business (AS) - GEB 2942</h3>
                                 
                                 <div>
                                    
                                    <p>GEB 1011 - Introduction to Business or<br>ACG 2021 - Financial Accounting or<span>&nbsp;</span><br>BUL 2241 - Business Law I or<span>&nbsp;</span><br>ECO 2013 - Principles of Economics-Macro or<br>ECO 2023 - Principles of Economics-Micro
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Business Management (AA) - MAN 2941</h3>
                              
                              <div>
                                 
                                 <p>GEB 1011 - Introduction to Business</p>
                                 
                                 <h3>Business Management (AS) - MAN 2942</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>GEB 1011 - Introduction to Business<br>MAN 2021 - Principles of Management
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Business Marketing&nbsp;(AA) - MAR 2941</h3>
                              
                              <div>
                                 
                                 <p>GEB 1011 - Introduction to Business</p>
                                 
                                 <h3>Business Marketing (AS) - MAR 2942</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>GEB 1011 - Introduction to Business<br>MAR 2011 - Principles of Marketing
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Cardiovascular Technology (AS) - CVT 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>CVT 1840L - Cardiovascular Clinical Practicum I<br> CVT 1841L - Cardiovascular Clinical Practicum II<br> CVT 2842L - Cardiovascular Clinical Practicum III
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Chemistry (AA) - CHM 2941</h3>
                              
                              <div>
                                 
                                 <p><span>CHM 1025C - Introduction to General Chemistry or&nbsp;</span><br><span>CHM 1045C - General Chemistry with Qualitative Analysis</span></p>
                                 
                              </div>
                              
                              <h3>Civic Leadership (AA) - POS 2940</h3>
                              
                              <div>
                                 
                                 <p><span>Completion of any college-level course (preferably an INR or POS course) and a 2.5
                                       institutional or overall GPA</span></p>
                                 
                              </div>
                              
                              <h3>Civil Surveying / Engineering Technology&nbsp;(AA) - SUR 2941</h3>
                              
                              <div>
                                 
                                 <p><span>SUR 1101C - Basic Surveying Measurements</span></p>
                                 
                                 <h3>Civil Surveying / Engineering Technology (AS) - SUR 2942</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>ETD 1320 - Introduction to CADD<span>&nbsp;</span><br>SUR 1101C - Basic Surveying Measurements
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Communications (AA) - COM 2941</h3>
                              
                              <div>
                                 
                                 <p><span>ENC 1101 - Freshman Composition I or</span><br><span>ENC 1101H - Freshman Composition I - Honors or</span><br><span>SPC 1608 - Fundamentals of Speech<span>&nbsp;</span></span><br><span>AND </span><br><span>JOU 1100 - News Reporting or<span>&nbsp;</span></span><br><span>MMC 1000 - Survey of Mass Communications or<span>&nbsp;</span></span><br><span>MMC 2100 - Writing for Mass Communications</span></p>
                                 
                              </div>
                              
                              <h3>Computer Information Technology (AS) - CIS 2943</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>CTS1131C and CTS 1120 (Security) OR CTS1155 (Support)<br> **Or, Program Director/Chair/Internship and Workforce Services approval**
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Computer Programming (AS)&nbsp;- CIS 2942</h3>
                              
                              <div>
                                 
                                 <p>Complete at least one computer language or database management system course, with
                                    an average of at least 3.0 in all COP courses, OR Complete one advanced language or
                                    advanced database management system course.
                                 </p>
                                 
                                 <h3>Computer&nbsp;Studies (AA) - CGS 2941</h3>
                                 
                                 <div>
                                    
                                    <p><span>Know at least one computer language or database management system.</span></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Criminal Justice (AA) - CCJ 2941</h3>
                              
                              <div>
                                 
                                 <p><span>30 credits which include 15 credit hours of criminal justice courses and program directors
                                       approval.</span></p>
                                 
                              </div>
                              
                              <h3>Culinary Management (AS) - FSS 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>FOS 2201 - Food Service Sanitation Management<br> FSS 1203C - Quantity Food Production I <br> FSS 1240C - Classical Cuisine
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Dance (AA) - DAA 2942</h3>
                              
                              <div>
                                 
                                 <p><span>DAA 1101 - Modern Dance II or&nbsp;</span><br><span>DAA 1201 - Ballet II</span><br><span>AND/EITHER </span><br><span>DAA 2500 - Jazz Dance I or<span>&nbsp;</span></span><br><span>DAA 2501 - Jazz Dance II</span></p>
                                 
                              </div>
                              
                              <h3>Dance Performance (AA) - DAA 2941</h3>
                              
                              <div>
                                 
                                 <p><span>DAA 1101 Modern Dance II&nbsp;</span><br><span>DAA 1201 Ballet II</span></p>
                                 
                              </div>
                              
                              <h3>Digital Media Design (AA) - DIG 2941</h3>
                              
                              <div>
                                 
                                 <p><span>DIG 2000C Minimum grade of C or Program Chair Approval</span></p>
                                 
                                 <h3>Digital Media Design&nbsp;(AS) - DIG 2943</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>DIG2284C - Advanced Digital Video &amp; Sound or<span>&nbsp;</span><br>DIG2342C - Advanced Motion Graphics or<span>&nbsp;</span><br>DIG2561C - Project Management for Digital Media
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Drafting &amp; Design Technology (AA) - ETD 2941</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>EGN 1111C - Engineering Computer Graphics</p>
                                    
                                    <h3>Drafting &amp; Design Technology (AS) - ETD 2942</h3>
                                    
                                    <div>
                                       
                                       <div>
                                          
                                          <p>ETD 1320 - Introduction to CADD</p>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Earth and Physical Science (AA) - PSC 2941</h3>
                              
                              <div>
                                 
                                 <p><span>12 credits, 2.0 GPA or higher, Completion of College Mandated Courses or departmental
                                       approval</span></p>
                                 
                              </div>
                              
                              <h3>Economics (AA) - ECO 2941</h3>
                              
                              <div>
                                 
                                 <p><span>ECO 2013 - Principles of Economics - Macro or</span><br><span>ECO 2023 - Principles of Economics - Micro</span></p>
                                 
                              </div>
                              
                              <h3>Education (AA) - EDG 2941</h3>
                              
                              <div>
                                 
                                 <p>EDF 2005 Introduction to Education or<span>&nbsp;</span><br>EME 2040 Technology for Education with a co-requisite of EDG 2071
                                 </p>
                                 
                                 <p class="important-note-small">This internship requires at least an overall 2.5 GPA</p>
                                 
                              </div>
                              
                              <h3>Electronic Engineering Technology (AA) - EET 2941</h3>
                              
                              <div>
                                 
                                 <p><span>EET 1025C -&nbsp;Fundamentals of AC Circuits</span><br><span>CET 2113C - Digital Systems II</span><br><span>and Internship Office approval</span></p>
                                 
                                 <h3>Electronic Engineering Technology (AS) - EET 2942</h3>
                                 
                                 <div>
                                    
                                    <p><span>EET 1025C -&nbsp;Fundamentals of AC Circuits</span><br><span>CET 2113C - Digital Systems II</span><br><span>EET 1141C - Semiconductor Devices and Circuits</span><br><span>ETS 1210C - Introduction to Photonics</span></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Engineering (AA) - EGS 2941</h3>
                              
                              <div>
                                 
                                 <p><span>MAC 2311 - Calculus with Analytical Geometry I</span><br><span>EGS 1006 - Introduction to Engineering Profession</span><br><span>EGS 1007 - Engineering Concepts and Methods</span><br><span>EGS 2310 - Engineering Analysis - Statics</span></p>
                                 
                              </div>
                              
                              <h3>Film Technology&nbsp;(AA) - FIL 2941</h3>
                              
                              <div>
                                 
                                 <p><span>AA Students served under DIG 2941.</span></p>
                                 
                                 <h3>Film Technology (AS) - FIL 2942</h3>
                                 
                                 <div>
                                    
                                    <p><span>Completion of the limited access portion of the Valencia Film Production Technology
                                          Program, and Program Directors Approval</span></p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>French (AA) - FRE 2941</h3>
                              
                              <div>
                                 
                                 <p><span>FRE 1000 - Basic French or</span><br><span>FRE 1101 - Elementary French I</span></p>
                                 
                              </div>
                              
                              <h3>Graphic &amp; Interactive Design (AS) - GRA 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>GRA 2182C - Advanced Graphic Design II (grade of C or better)<br> or<br> GRA 2143C - Advanced Web Page Design (grade of C or better)
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Health (AA) - HSC 2941</h3>
                              
                              <div>
                                 
                                 <p><span>HSC 1004 - Professions of Caring or</span><br><span>HSC 1531 - Medical Terminology or</span><br><span>BSC 1010C - Fundamentals of Biology or</span><br><span>PSY 1012 - General Psychology</span></p>
                                 
                              </div>
                              
                              <h3>Hospitality and Tourism (AA) - HFT 2941</h3>
                              
                              <div>
                                 
                                 <p><span>HFT 1000 - Introduction to Hospitality and Tourism Industry</span><br><span>12 credits and program director's approval</span></p>
                                 
                                 <h3>Hospitality and Tourism (AS) - HFT 2942</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>HFT 1000 - Introduction to Hospitality and Tourism Industry or<span>&nbsp;</span><br>HFT 1410 - Front Office Management or<br>FSS 2251 - Food and Beverage Management
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Humanities (AA) - HUM 2941</h3>
                              
                              <div>
                                 
                                 <p><span>Satisfactory completion of all mandated courses in reading, mathematics, English,
                                       and English for Academic Purposes; 12 credits including 6 credits in Humanites, three
                                       of which must be a Gordon Rule writing course; and Internship Office approval.</span></p>
                                 
                              </div>
                              
                              <h3>Insurance (AS) - RMI 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>GEB1101 - Introduction to Business and<br> RMI1201 - Property and Liability or<br> RMI1521 - Principles of Insurance
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Landscape / Horticulture (AS) - HOS 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>ORH 1510 - Ornamental Plant Materials I<br> BOT 2501 - Plant Physiology<br> AOM 2012 - Types and Systems of Agricultural Ops.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Library Science (AA) - LIS 2941</h3>
                              
                              <div>
                                 
                                 <p><span>ENC 1101 - Freshman Composition I or&nbsp;</span><br><span>ENC 1101H - Freshman Composition I - Honors or<span>&nbsp;</span></span><br><span>2000 level Humanities course</span></p>
                                 
                              </div>
                              
                              <h3>Medical Office (AS) - OST 2943</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>OST 1611 - Medical Transcription or<br> OST 2453 - Medical Coding or <br> OST 2854 - **Microsoft Office<br> Or Program Director/Internship and Workforce Services approval
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Music Technology (AS) - MUM 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>MUM 2606 - Sound Recording II</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Networking (AS) - CET 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>CET1610C, CET2544C AND CET2830C AND one of the following:<br> CET2890C (Cyber) or CET2794C (Microsoft) or CET2615C (Cisco)<br> and 3.0 GPA or Program Director's/Internship Workforce Services' approval
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Office Systems Technology (AS) - OST 2944</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>OST 1110C – Keyboarding &amp; Documents Processing II<br> OST 2854 – Microsoft Office
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Paralegal Studies / Legal Assisting (AS)&nbsp;- PLA 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Taken in last semester of the Paralegal Studies Program.</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Physics (AA) - PHY 2941</h3>
                              
                              <div>
                                 
                                 <p><span>PHY 2048C - General Physics with Calculus I</span><br><span>MAC 2311 - Calculus with Analytic Geometry</span></p>
                                 
                              </div>
                              
                              <h3>Political Science (AA) - POS 2941</h3>
                              
                              <div>
                                 
                                 <p><span>POS 2041 - U.S. Government</span></p>
                                 
                              </div>
                              
                              <h3>Psychology (AA) - PSY 2941</h3>
                              
                              <div>
                                 
                                 <p><span>PSY 1012 General Psychology.&nbsp; Students participating in Elephant Watch also require
                                       department approval.</span></p>
                                 
                              </div>
                              
                              <h3>Radiography (AS) - RTE 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>2nd year RTE student and program director's approval required&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Reading - REA 2941</h3>
                              
                              <div>
                                 
                                 <p><span>ENC 1101 - Freshman Composition I or&nbsp;</span><br><span>REA 1105 - College Reading</span></p>
                                 
                                 <h3>Real Estate (AA) - REE 2941</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>REE 1000 - Real Estate Principles and Practices<br>REE 1400 - Florida Real Estate Law
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Real Estate (AS) - REE 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>REE 1000 - Real Estate Principles and Practices<br> REE 1400 - Florida Real Estate Law
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Restaurant Management (AS) - HFT 2943</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>HFT 1000 - Introduction to Hospitality and Tourism</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Sign Language - INT 2941</h3>
                              
                              <div>
                                 
                                 <p><span>INT 2201 - Interactive Interpreting II&nbsp;</span></p>
                                 
                              </div>
                              
                              <h3>Sociology - SYG 2941</h3>
                              
                              <div>
                                 
                                 <p><span>SYG 2000 - Introduction to Sociology</span></p>
                                 
                              </div>
                              
                              <h3>Sonography (AS) - SON 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>Satisfactory completion of all mandated courses in reading, mathematics, English (12
                                       credits and program director's approval)
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <h3>Spanish (AA) - SPN 2941</h3>
                              
                              <div>
                                 
                                 <p><span>SPN 2200 Intermediate Spanish I or&nbsp;</span><br><span>SPN 2201 Intermediate Spanish II or<span>&nbsp;</span></span><br><span>SPN 2201H Intermediate Spanish-Honors</span></p>
                                 
                              </div>
                              
                              <h3>Theater (AA) - TPA 2941</h3>
                              
                              <div>
                                 
                                 <p><span>TPA 2290 Technical Theater Production and&nbsp;</span><br><span>TPA 2220 Introduction to Stage Lighting or<span>&nbsp;</span></span><br><span>TPA 2260 Sound for the Stage or<span>&nbsp;</span></span><br><span>THE 1020 Introduction to Theater or<span>&nbsp;</span></span><br><span>TPA 1200 Basic Stagecraft</span></p>
                                 
                              </div>
                              
                              <h3>Theater (AS) - TPA 2942</h3>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <p>4 courses with TPA or MUM prefix to include TPA2290 or TPA2257</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <p><a href="#top">TOP</a></p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/pre-requisites.pcf">©</a>
      </div>
   </body>
</html>