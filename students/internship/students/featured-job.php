<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/featured-job.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Featured Job</h2>
                        
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-warning"></i><h3>Qualified teachers</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-ellipsis-v"></i><h3>Equipped class rooms</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-desktop"></i><h3>Advanced teaching</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-rocket"></i><h3>Advanced study plans</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-bullseye"></i><h3>Focus on target</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-graduation-cap"></i><h3>Focus on success</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Featured Job</h3>
                        
                        <div><img src="/students/internship/students/nike-swoosh-logo-100x55.png" alt="Nike Logo" width="100" height="55"> Nike doesn't do retail like most companies. Step into a store or shop online, and
                           the energy, enthusiasm and passion for sport are palpable. Nike Retail employees inspire
                           athletes of all abilities to tap into their potential. From the store floor to online
                           support, it's up to Nike Retail employees to know exactly what the consumer expects,
                           and then deliver far beyond it. <a href="/students/internship/students/featured-job.html"><span>Read more</span></a></div>
                        
                        <h3>Featured Internship</h3>
                        
                        <div><img src="/students/internship/students/LEGOLAND_Florida_Resort_100x38.png" alt="Lego Land Logo" width="100" height="38"> Become an intern with LEGOLAND® Florida Resort! Located in the heart of Central Florida,
                           it features a 150-acre interactive theme park with more than 50 rides, shows and attractions,
                           plus a Water Park. <a href="/students/internship/students/featured-internship.html"><span>Read more</span></a></div>
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/featured-job.pcf">©</a>
      </div>
   </body>
</html>