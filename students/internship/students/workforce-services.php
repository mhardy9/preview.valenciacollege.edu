<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/workforce-services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Students - Workforce Services</h2>
                        
                        <p>Whether you are a current student or a recent graduate, we can help you find employment.
                           The following tools and resources are available to assist you in your search.&nbsp;
                        </p>
                        
                        <h3>Off-Campus PT/FT Work</h3>
                        
                        <p>For Valencia students still in school, we post positions in <a href="http://valencia-csm.symplicity.com/students" target="_blank">Navigator</a>, our online job and internship database, for off-campus work.
                        </p>
                        
                        <h3>Job Search for Degree Completers</h3>
                        
                        <p>For Valencia students who have recently graduated or are about to graduate with an
                           A.S. Degree or a Technical Certificate, our coordinators work directly with you to
                           assist in your job search. <a href="http://net4.valenciacollege.edu/forms/internship/contact.cfm" target="_blank">Please contact the IWS office on your campus to schedule an appointment.</a></p>
                        
                        <h3>On-Campus Recruitment</h3>
                        
                        <p>Our department also scheduled on-campus recruitment sessions. You can meet face-to-face
                           with employers who are looking to hire students. It’s like a mini-job fair with one
                           or two employers. You can see what employers will be on which campus and when in the
                           Events tab of your <a href="http://valencia-csm.symplicity.com/students" target="_blank">Navigator</a> account.
                        </p>
                        
                        <p><a href="http://events.valenciacollege.edu/group/internship" target="_blank">Valencia's Events Calendar</a> has all scheduled recruitment dates on display.
                        </p>
                        
                        <h3>My Job Prospects</h3>
                        
                        <p>This is a feature found in your Atlas account under the My LifeMap tab. You can research
                           companies nation-wide to see what’s out there and even find contact information for
                           recruiters.
                        </p>
                        
                        <h3>Job Fairs</h3>
                        
                        <p>Valencia hosts job fairs throughout each year. We also advertise upcoming off-site
                           employer job fairs.&nbsp; Information about on campus&nbsp;<a href="/students/internship/students/workforce-services.php">job fairs</a>&nbsp;can be found on our site.
                        </p>
                        <a href="http://valencia-csm.symplicity.com/students" target="_blank">Search Positions</a></div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        There are no events at this time.
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/workforce-services.pcf">©</a>
      </div>
   </body>
</html>