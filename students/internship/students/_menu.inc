<div class="header header-site">
<div class="container">
<div class="row">
<div class="col-md-3 col-sm-3 col-xs-3" role="banner">
<div id="logo"><a href="/index.php"> <img src="/_resources/img/logo-vc.png" alt="Valencia College Logo" width="265" height="40" data-retina="true" /> </a></div>
</div>
<nav class="col-md-9 col-sm-9 col-xs-9" aria-label="Subsite Navigation" role="navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"> <span> Menu mobile </span> </a>
<div class="site-menu">
<div id="site_menu"><img src="/_resources/img/logo-vc.png" alt="Valencia College" width="265" height="40" data-retina="true" /></div>
<a id="close_in" class="open_close" href="#"> </a>
<ul class="mobile-only">
<li><a href="/search"> Search </a></li>
<li class="submenu"><a class="show-submenu" href="javascript:void(0);"> Login </a>
<ul>
<li><a href="https://atlas.valenciacollege.edu/"> Atlas </a></li>
<li><a href="https://atlas.valenciacollege.edu/"> Alumni Connect </a></li>
<li><a href="https://login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"> Office 365 </a></li>
<li><a href="https://learn.valenciacollege.edu"> Valencia &amp; Online </a></li>
<li><a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"> Webmail </a></li>
</ul>
</li>
<li><a class="show-submenu" href="javascript:void(0);"> Top Menu </a>
<ul>
<li><a href="/academics/current-students/index.php"> Current Students </a></li>
<li><a href="https://preview.valenciacollege.edu/students/future-students/"> Future Students </a></li>
<li><a href="https://international.valenciacollege.edu"> International Students </a></li>
<li><a href="/academics/military-veterans/index.php"> Military &amp; Veterans </a></li>
<li><a href="/academics/continuing-education/index.php"> Continuing Education </a></li>
<li><a href="/EMPLOYEES/faculty-staff.php"> Faculty &amp; Staff </a></li>
<li><a href="/FOUNDATION/alumni/index.php"> Alumni &amp; Foundation </a></li>
</ul>
</li>
</ul>
<ul>
<li><a href="/students/internship/index.php">Internship and Workforce Services</a></li>
<li class="submenu megamenu"><a class="show-submenu-mega" href="javascript:void(0);">Menu </a>
<div class="menu-wrapper c3">
<div class="col-md-4">
<h3>About Us</h3>
<ul>
<ul>
<li><a href="/students/internship/meet-the-staff.php">Meet the Staff</a></li>
<li><a href="/students/internship/hours-locations.php" target="_blank">Hours and Locations</a></li>
<li><a href="/students/internship/faq.php">FAQ</a></li>
</ul>
</ul>
<h3>Students</h3>
<ul>
<li><a href="/students/internship/students/internships.php">Internships</a></li>
<li><a href="/students/internship/students/workforce-services.php">Workforce Services</a></li>
<li><a href="/students/internship/students/job-fairs.php">Job Fair Information</a></li>
</ul>
</div>
<div class="col-md-4">
<h3>Employers</h3>
<ul>
<li><a href="/students/internship/employers/internships.php">Internships</a></li>
<li><a href="/students/internship/employers/pt-ft-jobs.php">PT / FT Jobs</a></li>
<li><a href="/students/internship/employers/employer-information.php">Job Fair Information</a></li>
<li><a href="/students/internship/employers/on-campus-recruitment.php">On-Campus Recruitment</a></li>
<li><a href="/students/internship/employers/policies-and-procedures.php">Policies &amp; Procedures</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/internship/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
</div>
</div>
</li>
</ul>
</div>
</nav></div>
</div>
</div>