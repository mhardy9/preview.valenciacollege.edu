<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/job-fairs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Spring 2018 Job Fair Student Information</h2>
                        
                        <p><strong> Thursday, March 29, 2018<br> 10:00 am - 2:00 pm<br> Valencia College West Campus, Special Events Center</strong></p>
                        
                        <h3>Job Fair Information</h3>
                        
                        <p><span>Internship and Workforce Services, in partnership with the Alumni Association and
                              Career Centers, is hosting the annual Spring Job Fair! Network with employers and
                              explore full-time and part-time job opportunities at the fair. The event is open to
                              all students and alumni.</span></p>
                        
                        <p><span>First impressions matter! Business casual or professional dress is required.</span></p>
                        
                        <p><span>To better prepare for the fair, there will be job fair preparation workshops on your
                              campus. Learn how to research companies and employers, what is professional attire,
                              and the importance of following up with employers after the fair.</span></p>
                        
                        <p><span>Check this page frequently for updates.</span></p>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Job Fair Preparation&nbsp;Workshops</h3>
                        
                        <h4>East Campus Job Fair Preparation Workshops</h4>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><strong>Date</strong></td>
                                 
                                 <td><strong>Workshop Title</strong></td>
                                 
                                 <td><strong>Location</strong></td>
                                 
                                 <td><strong>Time</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>January 31</td>
                                 
                                 <td>Workshop 1</td>
                                 
                                 <td>East Campus</td>
                                 
                                 <td>2:30 pm - 3:30 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 8</td>
                                 
                                 <td>Workshop 2</td>
                                 
                                 <td>East Campus</td>
                                 
                                 <td>10:00 am - 11:00 am</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 20</td>
                                 
                                 <td>Workshop 3</td>
                                 
                                 <td>East Campus</td>
                                 
                                 <td>11:00 am - 2:00 pm</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h4>&nbsp;</h4>
                        
                        <h4>West Campus Job Fair Preparation Workshops</h4>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><strong>Date</strong></td>
                                 
                                 <td><strong>Workshop Title</strong></td>
                                 
                                 <td><strong>Location</strong></td>
                                 
                                 <td><strong>Time</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 6</td>
                                 
                                 <td>Job Fair Prepare</td>
                                 
                                 <td>SSB 206G</td>
                                 
                                 <td>1:00 pm - 2:00 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 8</td>
                                 
                                 <td>Want a job?&nbsp; It starts with your resume.</td>
                                 
                                 <td>SSB 206G</td>
                                 
                                 <td>1:00 pm - 2:00 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 20</td>
                                 
                                 <td>&nbsp;Let's Get Professional / Job Fair Prepare</td>
                                 
                                 <td>HSB 105</td>
                                 
                                 <td>11:30 am - 2:00 pm&nbsp;</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 21</td>
                                 
                                 <td>Resume Review Days</td>
                                 
                                 <td>SSB 206 - Career Center</td>
                                 
                                 <td>10:00 am - 4:00 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 22</td>
                                 
                                 <td>You want me to do what?&nbsp; Talk to people?</td>
                                 
                                 <td>SSB 206G</td>
                                 
                                 <td>1:00 pm - 2:00 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 26</td>
                                 
                                 <td>Resume Review Days</td>
                                 
                                 <td><span>SSB 206 - Career Center</span></td>
                                 
                                 <td>10:00 am - 4:00 pm</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <h4><br>Osceola Campus Job Fair Preparation Workshops
                        </h4>
                        
                        <table class="table ">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td><strong>Date</strong></td>
                                 
                                 <td><strong>Workshop Title</strong></td>
                                 
                                 <td><strong>Location</strong></td>
                                 
                                 <td><strong>Time</strong></td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>January 31</td>
                                 
                                 <td>Resume Workshop</td>
                                 
                                 <td>2-125</td>
                                 
                                 <td>2:30 pm - 3:30 pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 8</td>
                                 
                                 <td>SkillScan Workshop</td>
                                 
                                 <td>2-125</td>
                                 
                                 <td>10:00 am - 11:00 am</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>March 20</td>
                                 
                                 <td>Job Fair Prepare</td>
                                 
                                 <td>4-105*</td>
                                 
                                 <td>11:00 am - 2:00 pm</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>*this location is pending</p>
                        
                        <h3>Frequently Asked Questions</h3>
                        
                        <p><strong>What should I bring to the fair?</strong><br> Bring your Valencia ID card or know your VID number and multiple copies of your resumes.
                        </p>
                        
                        <p><strong>Is there a dress code?</strong><br> Yes, students are required to dress professionally. Examples of this include closed-toe
                           shoes, dresses with jackets, suits, dress shirt with tie, slacks, or skirts with blouses.
                        </p>
                        
                        <p>Jeans, shorts, tank tops, sweats, flip flops, and sneakers are not allowed. Please
                           be aware, if you come to the fair inappropriately dressed, you may be turned away.
                           Employers expect your very best first impression and your appearance matters.
                        </p>
                        
                        <p><strong>Do I need to register to attend the fair?</strong><br> No, students and alumni do not need to register. Attendees will be asked to check-in
                           with staff upon arrival to receive a nametag, list of employers, and tote bag.
                        </p>
                        
                        <p><strong>Can I attend if I take classes at another campus?</strong><br> Yes, this event is open to all students college-wide, as well as alumni.
                        </p>
                        
                        <p><strong>What kinds of organizations participate in the fair?</strong><br> Representatives from a wide variety of industries attend the fair- Business, Information
                           Technology, Engineering, Hospitality, Public Safety, Health, Media and Communications,
                           Early Childhood Education, etc. <a href="/students/internship/internship-fair.html">List of employers.</a></p>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Featured Job</h3>
                        
                        <div>Featured Job</div>
                        
                        <h3>Featured Internship</h3>
                        
                        <div>&nbsp;Featured Internship</div>
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/job-fairs.pcf">©</a>
      </div>
   </body>
</html>