<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">&nbsp;</div>
                     
                     <div class="col-md-9">&nbsp;</div>
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Students</h2>
                        
                        <p>Welcome students!</p>
                        
                        <p>The Internship and Workforce Services office team works to guide students who have
                           an interest in participating in the Internship Program or need job search assistance.
                        </p>
                        
                        <p>Employers increasingly want to see related work experience in college graduates they
                           hire. Participating in the internship program will enable you to gain work-related
                           experience, increase valuable understanding of your field of study and apply what
                           you learned in the classroom while earning college credit(s) towards your degree.
                        </p>
                        
                        <p>Explore careers, develop new skills, connect with employers and make it happen!</p>
                        
                        <div class="banner"><i class="iconcustom-school"></i>
                           
                           <h3>Spring 2018 Job Fair</h3>
                           
                           <p><span>Internship and Workforce Services is hosting the Spring 2018 Job Fair in partnership
                                 with the Alumni Association and Career Centers.</span></p>
                           <a href="/students/internship/job-fair" target="" class="banner_bt">Job Fair Information</a></div>
                        
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-users"></i><h3>Internships</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-desktop"></i><h3>Course Prerequisites</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-cubes"></i><h3>"Special Internships"</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-rocket"></i><h3>Workforce Resources</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-bullseye"></i><h3>Focus on target</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-graduation-cap"></i><h3>Focus on success</h3>
                                 <p>Id mea congue dictas, nec et summo mazim impedit. Vim te audiam impetus interpretaris,
                                    cum no alii option, cu sit mazim libris.
                                 </p></a>
                              
                           </div>
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Featured Job</h3>
                        
                        <div>Featured Job</div>
                        
                        <h3>Featured Internship</h3>
                        
                        <div>Featured Internship</div>
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/index.pcf">©</a>
      </div>
   </body>
</html>