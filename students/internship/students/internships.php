<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/internships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Students - Internships</h2>
                        
                        <div>
                           
                           <div>
                              
                              <h3>Basics</h3>
                              
                              <p>An internship is a way for you to earn college credit while working in an actual position
                                 that relates to your major, giving you a chance to gain experience and make connections
                                 in a professional setting. You register for the internship class but instead of going
                                 to a classroom, you go to your internship job site.
                              </p>
                              
                              <p>Our department is here to:</p>
                              
                              <ul>
                                 
                                 <li>Determine whether you are academically ready for an internship</li>
                                 
                                 <li>Help you register for internship credit</li>
                                 
                                 <li>Give you access to internship postings from local employers</li>
                                 
                                 <li>Advise and coach you throughout the process</li>
                                 
                                 <li>Check out our <a href="http://events.valenciacollege.edu/group/internship" target="_blank"><strong>calendar of events</strong></a> for scheduled info sessions.
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div>
                              
                              <h3>&nbsp;</h3>
                              
                              <h3>Requirements</h3>
                              
                              <p>In order to register for an internship a student must have:</p>
                              
                              <ul>
                                 
                                 <li>12 credits <em>completed</em> (with grades) when you apply
                                 </li>
                                 
                                 <li>2.0 GPA (minimum)</li>
                                 
                                 <li>Reading/Writing course completed (at least one)</li>
                                 
                                 <li>Math course completed (based on your degree)</li>
                                 
                                 <li>Program-specific requirements:</li>
                                 
                              </ul>
                              
                              <p><a href="/students/internship/students/pre-requisites.php">Internship Prerequisite Finder Tool</a></p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>&nbsp;</h3>
                              
                              <h3>How to Apply</h3>
                              
                              <p>Registering for an internship is very easy!</p>
                              
                              <p><strong>STEP 1</strong><br> Log in to&nbsp;<a title="Navigator" href="http://valencia-csm.symplicity.com/students">Navigator</a>.
                              </p>
                              
                              <p><strong>STEP 2</strong><br> Go to "My Account" at the bottom left of the menu bar.
                              </p>
                              
                              <p><strong>STEP 3</strong><br> Click on "Academic (Internship Profile)" and fill out the form completely.
                              </p>
                              
                              <p><strong>STEP 4</strong><br> Allow 1-2 business days for receipt of your internship application information for
                                 review.
                              </p>
                              
                              <p><strong>STEP 5</strong><br> Schedule a meeting with an Internship Coordinator once your application has been
                                 approved.
                              </p>
                              
                           </div>
                           
                           <div>
                              
                              <h3>Calendar</h3>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="td"><strong>Internship Open Enrollment Periods</strong></div>
                                       
                                       <div data-old-tag="td">
                                          
                                          <table class="table ">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td><strong>Term</strong></td>
                                                   
                                                   <td><strong>Begins</strong></td>
                                                   
                                                   <td><strong>Ends</strong></td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td><span>Summer 2018 (2018-30)</span></td>
                                                   
                                                   <td><span>02/12/18</span></td>
                                                   
                                                   <td><span>03/30/18</span></td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td>Fall&nbsp;2018 (2019-10)</td>
                                                   
                                                   <td>06/25/18</td>
                                                   
                                                   <td>07/27/18</td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td>Spring 2019 (2019-20)</td>
                                                   
                                                   <td>10/15/18&nbsp;</td>
                                                   
                                                   <td>11/30/18&nbsp;</td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td>Summer 2019 (2019-30)</td>
                                                   
                                                   <td>02/11/19&nbsp;</td>
                                                   
                                                   <td>03/29/19&nbsp;</td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                          <p><strong><br>Internship Credit Registration Deadlines</strong></p>
                                          
                                          <table class="table ">
                                             
                                             <tbody>
                                                
                                                <tr>
                                                   
                                                   <td><strong>Term</strong></td>
                                                   
                                                   <td><strong>Last Day to Register</strong></td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td><span>Summer 2018 (2018-30)</span></td>
                                                   
                                                   <td><span>06/08/18</span></td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td><span>Fall</span><span>&nbsp;2018 (2019-10)</span></td>
                                                   
                                                   <td>09/28/18</td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td><span>Spring 2019 (2019-20)</span></td>
                                                   
                                                   <td>02/08/19</td>
                                                   
                                                </tr>
                                                
                                                <tr>
                                                   
                                                   <td><span>Summer 2019 (2019-30)</span></td>
                                                   
                                                   <td>06/07/19</td>
                                                   
                                                </tr>
                                                
                                             </tbody>
                                             
                                          </table>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Featured Job</h3>
                        
                        <div>Featured Job</div>
                        
                        <h3>Featured Internship</h3>
                        
                        <div>Featured Internship</div>
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/internships.pcf">©</a>
      </div>
   </body>
</html>