<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/internship-fair.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html">
                           
                           
                           
                           
                           </a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Internship &amp; Part-time Job Fair Student Information</h2>
                        
                        <p> <strong> Thursday, October  26, 2017<br>
                              10:00 am - 2:00 pm<br>
                              Valencia College West Campus, Special Events Center</strong></p>
                        
                        <p><br>
                           
                        </p>
                        
                        <h3 class="heading_divider_red">Internship  &amp; Part-time Job Fair</h3>
                        
                        <p>Join  Internship and Workforce Services as they host the first annual Fall Internship
                           &amp; Part-time Job Fair! Attending the fair is a great way to network and  connect with
                           diverse employers who are looking to fill a variety of internship  and employment
                           opportunities.
                        </p>
                        
                        <p> First impressions  matter! Professional attire is required. </p>
                        
                        <p> To better prepare  for the fair, attend any of the "Get Prepared, Get Hired!" workshops
                           on your  campus. Learn how to research companies and employers, what is professional
                           attire, and the importance of following up with employers after the fair.
                        </p>
                        
                        <p> Internship and Workforce Services is proud to host its first  annual Fall Internship
                           and Part-time Job Fair. 
                        </p>
                        
                        <p> Students! Prepare for  the Internship &amp; Part-time Job Fair with the "Get Prepared,
                           Get Hired!"  workshops.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <h3> "Get Hired, Get Prepared!" Workshops</h3>
                        
                        <h4>East</h4>
                        
                        <ul>
                           
                           <li>October 5, 201712 pm - 1 pm8-101 </li>
                           
                           <li>October 19, 20171 pm - 2 pm2-304 </li>
                           
                           <li>October 23, 201712 pm - 1 pm8-151 </li>
                           
                        </ul>
                        
                        <h4>West </h4>
                        
                        <ul>
                           
                           <li>October 11, 201712 pm - 1:30 pm5-114 </li>
                           
                           <li>October 17, 201712 pm - 1:30 pm5-114 </li>
                           
                           <li>October 25, 201712 pm - 1:30 pm1-154 </li>
                           
                        </ul>
                        
                        <h4>Osceola</h4>
                        
                        <ul>
                           
                           <li>October 11, 201711 pm - 12 pm1-219B </li>
                           
                           <li>October 18, 201711 pm - 12 pm1-219B </li>
                           
                           <li>October 25, 201711 pm - 12 pm1-219B </li>
                           
                        </ul>
                        
                        <p>To reserve a spot for East Campus workshops please call 407-582-2409.<br>
                           To reserve a spot for West Campus workshops please call 407-582-1034.<br>
                           To reserve a spot for Osceola Campus workshops please call 321-682-4832.
                        </p>
                        
                        <h4>
                           <a href="http://valenciacollege.edu/internship/students/documents/Job-Fair-Fall-2017-Schedule.pdf" target="_blank" class="icon-acrobat">"Get Hired, Get Prepared!" Workshop Flyer</a>            
                        </h4>
                        
                        <p>&nbsp;</p>
                        
                        <h3>Frequently Asked Questions</h3>
                        
                        <p><strong>What should I bring to the fair?</strong><br>
                           Bring your Valencia  ID card or know your VID number and multiple copies of your resumes.
                        </p>
                        
                        <p><strong>Is there a dress code?</strong><br>
                           Yes, students are  required to dress professionally.   Examples of this include closed-toe
                           shoes,  dresses with jackets, suits,   dress shirt with tie, slacks, or skirts with
                           blouses. 
                        </p>
                        
                        <p> Jeans, shorts, tank  tops, sweats, flip flops, and   sneakers are not allowed. Please
                           be aware, if you  come to the fair   inappropriately dressed, you may be turned away.
                           Employers  expect your   very best first impression and your appearance matters.
                        </p>
                        
                        <p> <strong>Do I need to register to attend the fair?</strong><br>
                           No, students and  alumni do not need to register.   Attendees will be asked to check-in
                           with staff  upon arrival to receive a   nametag, list of employers, and tote bag.
                        </p>
                        
                        <p> <strong>Can I attend if I take classes at another  campus?</strong><br>
                           Yes, this event is  open to all students college-wide, as well as alumni.
                        </p>
                        
                        <p> <strong>What kinds of organizations participate in  the fair?</strong><br>
                           Representatives  from a wide variety of industries   attend the fair- Business, Information
                           Technology, Engineering,   Hospitality, Public Safety, Health, Media and  Communications,
                           Early   Childhood Education, etc.  <a href="/STUDENTS/offices-services/internship/internship-fair.html">List of employers.</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                        There are no events at this time.
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/internship-fair.pcf">©</a>
      </div>
   </body>
</html>