<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/students/featured-internship.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/students/">Students</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        <a href="../index.html">
                           
                           
                           
                           
                           </a>
                        
                        
                        
                        
                        
                        
                        
                        <h2>Featured Internship</h2>
                        
                        
                        
                        <p> Become an intern with LEGOLAND® Florida Resort! Located in the heart of Central Florida,
                           it features a 150-acre interactive theme park with more than 50 rides, shows and attractions,
                           plus a Water Park. Now open, the LEGOLAND® Hotel is located just steps from the entrance
                           to the theme park. For more information and to apply to this position visit <a href="http://valencia-csm.symplicity.com/students/">Navigator</a> today! 
                        </p>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        
                        <h3>Featured Job</h3>
                        
                        <div>
                           <img alt="Nike Logo" height="55" src="nike-swoosh-logo-100x55.png" width="100">
                           Nike doesn't do retail like most companies. Step into a store or shop online, and
                           the energy, enthusiasm and passion for sport are palpable. Nike Retail employees inspire
                           athletes of all abilities to tap into their potential. From the store floor to online
                           support, it's up to Nike Retail employees to know exactly what the consumer expects,
                           and then deliver far beyond it.
                           <a href="featured-job.html"><span>Read more</span></a>
                           
                           
                        </div>
                        
                        
                        
                        <h3>Featured Internship</h3>
                        
                        <div>
                           <img alt="Lego Land Logo" height="38" src="LEGOLAND_Florida_Resort_100x38.png" width="100">
                           Become an intern with LEGOLAND® Florida Resort! Located in the heart of Central Florida,
                           it features a 150-acre interactive theme park with more than 50 rides, shows and attractions,
                           plus a Water Park.
                           <a href="featured-internship.html"><span>Read more</span></a>
                           
                        </div>
                        
                        
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        
                        
                        
                        
                        <div> 
                           <div> 
                              <ul> 
                                 <li> 
                                    <div> <span> Oct 18 9am </span> <a href="http://events.valenciacollege.edu/event/on-campus_recruitment_-_michael_kors_1531?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">On-Campus Recruitment - Michael Kors</a> 
                                    </div> 
                                    <div>Currently Hiring: PT Sales Associates PT Cashier PT Stock Associates</div> 
                                    <div>
                                       <span>Location:</span> <a href="http://events.valenciacollege.edu/westcampus?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">West Campus</a> 
                                    </div> 
                                 </li> 
                                 <li> 
                                    <div> <span> Oct 18 9am </span> <a href="http://events.valenciacollege.edu/event/reunion_resort_club?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">Reunion Resort &amp; Club</a> 
                                    </div> 
                                    <div>We are currently ramping up for season and are looking for multiple positions throughout
                                       our property.the following positions in which we have critical needs for currently:
                                       ...
                                    </div> 
                                    <div>
                                       <span>Location:</span> <a href="http://events.valenciacollege.edu/OsceolaCampus?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">Osceola Campus</a> 
                                    </div> 
                                 </li> 
                                 <li> 
                                    <div> <span> Oct 18 10am </span> <a href="http://events.valenciacollege.edu/event/city_year?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">City Year</a> 
                                    </div> 
                                    <div>I am recruiting for the AmeriCorps position. The duties of this role are listed below.
                                       Academic Support: Planning tutoring sessions and providing one-on-one or small...
                                    </div> 
                                    <div>
                                       <span>Location:</span> <a href="http://events.valenciacollege.edu/event/city_year?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">East Campus</a> 
                                    </div> 
                                 </li> 
                                 <li> 
                                    <div> <span> Oct 18 11am - Oct 25 </span> <a href="http://events.valenciacollege.edu/event/internship_get_prepared_get_hired_workshops?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">Internship "Get Prepared, Get Hired"! Workshops</a> 
                                    </div> 
                                    <div>Attend this Internship Workshop to find out what you can do to get prepared and hired
                                       for internships and jobs! Find out what opportunities are available for you!
                                    </div> 
                                    <div>
                                       <span>Location:</span> <a href="http://events.valenciacollege.edu/OsceolaCampus?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">Osceola Campus</a> 
                                    </div> 
                                 </li> 
                                 <li> 
                                    <div> <span> Oct 19 9am </span> <a href="http://events.valenciacollege.edu/event/on-campus_recruitment_-_peerless_production_group?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">On-Campus Recruitment - Peerless Production Group</a> 
                                    </div> 
                                    <div>This is our 4th year to operate the Snow Tubing Event at the annual ICE! at the Gaylord
                                       Palms Resort. We are searching for students to join our team as Snow Patrols. Snow...
                                    </div> 
                                    <div>
                                       <span>Location:</span> <a href="http://events.valenciacollege.edu/OsceolaCampus?utm_campaign=widget&amp;utm_medium=widget&amp;utm_source=Valencia+College">Osceola Campus</a> 
                                    </div> 
                                 </li> 
                              </ul> 
                           </div> 
                        </div>
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/students/featured-internship.pcf">©</a>
      </div>
   </body>
</html>