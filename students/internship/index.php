<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services   | Valencia College</title>
      <meta name="Description" content="Internship and Workforce Services">
      <meta name="Keywords" content="internship, intern, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Internship</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="bg_content testimonials">
                           <div class="row">
                              <div class="col-md-offset-1 col-md-10">
                                 <div class="carousel slide" data-ride="carousel" id="quote-carousel">
                                    <ol class="carousel-indicators">
                                       <li data-target="#quote-carousel" data-slide-to="0" class="active"></li>
                                       <li data-target="#quote-carousel" data-slide-to="1"></li>
                                       <li data-target="#quote-carousel" data-slide-to="2"></li>
                                    </ol>
                                    <div class="carousel-inner">
                                       <div class="item active">
                                          <blockquote>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                                                eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                             </p>
                                          </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Stefany</small></div>
                                       <div class="item">
                                          <blockquote>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                                                eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                             </p>
                                          </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_2.jpg" alt="Valencia image description">Karla</small></div>
                                       <div class="item">
                                          <blockquote>
                                             <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut rutrum elit in arcu blandit,
                                                eget pretium nisl accumsan. Sed ultricies commodo tortor, eu pretium mauris.
                                             </p>
                                          </blockquote><small><img class="img-circle" src="/_resources/images/testimonial_1.jpg" alt="Valencia image description">Maira</small></div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-8 col-sm-8">&nbsp;
                              
                              <h2>Welcome!</h2>
                              
                              <p><span>The Internship and Workforce Services team is dedicated to educating and preparing
                                    Valencia students for Central Florida's dynamic job market. We are experts in the
                                    industry, working to identify employment trends and how they will impact career choices
                                    for students.</span>&nbsp;
                              </p>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <div class="box_style_4">
                                 
                                 <div class="deadlines">
                                    <h4 style="text-transform: uppercase; font-weight: 600">Events</h4>
                                    
                                    <div class="row">
                                       <div class="date_stacked col-md-2">20 <span>Feb</span></div>
                                       <div class="col-md-10">Advanced Registration Begins Returning Students</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">09 <span>MAR</span></div>
                                       <div class="col-md-10">Open Registration Begins for New and Returning Students</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">13 <span>MAR</span></div>
                                       <div class="col-md-10">Financial Priority Deadline</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">19 <span>MAR</span></div>
                                       <div class="col-md-10">Fee Payment Deadline: FRIDAY 5:00 P.M.</div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                       <div class="date_stacked col-md-2">23 <span>APR</span></div>
                                       <div class="col-md-10">Late Registration Begins: Fees Assessed With Initial Enroll</div>
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                           </div>
                        </div>
                        
                        <div class="row">
                           
                           <div class="main-title">
                              
                              <div class="row">
                                 <div class="col-md-4 col-sm-4">
                                    <a class="box_feat" href="/students/internship/students/" target=""><i class="far fa-users"></i><h3>Students</h3>
                                       <p>How do I find an internship?&nbsp; How do I find a job?</p></a>
                                    
                                 </div>
                                 <div class="col-md-4 col-sm-4">
                                    <a class="box_feat" href="/students/internship/employers/" target=""><i class="far fa-id-card"></i><h3>Employers</h3>
                                       <p>Connect with Valencia students</p></a>
                                    
                                 </div>
                                 <div class="col-md-4 col-sm-4">
                                    <a class="box_feat" href="/students/internship/faculty-staff/" target=""><i class="far fa-university"></i><h3>Faculty/Staff</h3>
                                       <p>Resources for Faculty and Staff</p></a>
                                    
                                 </div>
                              </div>
                              
                           </div>
                           
                           <div class="banner"><i class="iconcustom-school"></i>
                              
                              <h3>Spring 2018 Job Fair</h3>
                              
                              <p>Valencia College - West Campus<br>Special Events Center - Building 8<br>1800 S. Kirkman Rd.<br>Orlando, FL 32835<br><br>Tuesday, March 29, 2018<br>10 am - 2 pm
                              </p>
                              <a href="/students/internship/job-fair/" target="" class="banner_bt">Job Fair Information</a></div>
                           
                           <p>&nbsp;</p>
                           
                           <h3>Header 3</h3>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/index.pcf">©</a>
      </div>
   </body>
</html>