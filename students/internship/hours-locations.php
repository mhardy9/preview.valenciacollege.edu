<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/hours-locations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Hours</h2>
                        
                        <ul>
                           
                           <li>Monday to Friday - 8 am to 5 pm</li>
                           
                           <li>Refer to the college's calendar for holidays and closures.</li>
                           
                        </ul>
                        
                        <h2>Locations</h2>
                        
                        <div class="container-fluid">
                           <div class="container margin-60">
                              <div class="row">
                                 <div class="col-md-4">
                                    
                                    <div>
                                       <h4 class="v-red cap"><strong>East Campus</strong></h4>
                                       <p></p>
                                       <div><br></div>
                                    </div>
                                    
                                    <p>Kerry Fleming, Internship Coordinator<br>701 N. Econlockhatchee Trail<br>Orlando, FL 32825<br>Building 5, Room 230<br><br>Phone: 407-582-2037<br>Fax: 407-582-8906<br>Email:&nbsp;<a href="mailto:kfleming22@valenciacollege.edu">kfleming22@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 <div class="col-md-4">
                                    
                                    <div>
                                       <h4 class="v-red cap"><strong>Osceola Campus</strong></h4>
                                       <p></p>
                                       <div><br></div>
                                    </div>
                                    
                                    <p>Xavier Humphrey, Internship Coordinator<br>1800 Denn John Lane<br>Kissimmee, FL 34744<br>Building 2, Room 125<br><br>Phone: 321-682-4196<br>Fax: 407-582-4899<br>Email: <a href="mailto:xhumphrey@valenciacollege.edu">xhumphrey@valenciacollege.edu</a></p>
                                    
                                 </div>
                                 <div class="col-md-4">
                                    
                                    <div>
                                       <h4 class="v-red cap"><strong>West Campus</strong></h4>
                                       <p></p>
                                       <div><br></div>
                                    </div>
                                    
                                    <p>Jessica Camilo, Internship Coordinator<br>1800 S. Kirkman Road<br>Orlando, FL 32835<br>Student Services Building, Room 235<br><br>Phone: 407-582-1035<br>Fax: 407-582-1864<br>Email: <a href="mailto:jcamilo4@valenciacollege.edu">jcamilo4@valenciacollege.edu</a></p>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="container-fluid">
                           <div class="container margin-60">
                              <div class="row">
                                 <div class="col-md-4">
                                    
                                    <div>
                                       <h4 class="v-red cap"><strong>Lake Nona Campus</strong></h4>
                                       <p></p>
                                       <div><br></div>
                                    </div>
                                    
                                    <p>Xavier Humphrey, Internship Coordinator<br>12350 Narcoossee Road<br>Orlando, FL 32832<br><br><span>Phone: 321-682-4196</span><br><span>Fax: 407-582-4899<br>Email:&nbsp;<a href="mailto:xhumphrey@valenciacollege.edu">xhumphrey@valenciacollege.edu</a><br><br><strong>Call for appointments.</strong></span></p>
                                    
                                 </div>
                                 <div class="col-md-4">
                                    
                                    <div>
                                       <h4 class="v-red cap"><strong>Poinciana Campus</strong></h4>
                                       <p></p>
                                       <div><br></div>
                                    </div>
                                    
                                    <p>Xavier Humphrey, Internship Coordinator<br>3255 Pleasant Hill Road<br>Kissimmee, FL 34746<br><br><span>Phone: 321-682-4196</span><br><span>Fax: 407-582-4899<br>Email:&nbsp;<a href="mailto:xhumphrey@valenciacollege.edu">xhumphrey@valenciacollege.edu</a><br><br></span><strong>Call for appointments.</strong></p>
                                    
                                 </div>
                                 <div class="col-md-4">
                                    
                                    <div>
                                       <h4 class="v-red cap"><strong>Winter Park Campus</strong></h4>
                                       <p></p>
                                       <div><br></div>
                                    </div>
                                    
                                    <p>Jessica Camilo, Internship Coordinator<br>850 W. Morse Blvd. Room 249<br>Winter Park, FL 32789<br><br><span>Phone: 407-582-1035</span><br><span>Fax: 407-582-1864<br>Email:&nbsp;<a href="mailto:jcamilo4@valenciacollege.edu">jcamilo4@valenciacollege.edu</a><br><br><strong>Call for appointments.</strong></span></p>
                                    
                                 </div>
                              </div>
                           </div>
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/hours-locations.pcf">©</a>
      </div>
   </body>
</html>