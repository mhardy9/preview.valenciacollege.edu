<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/employers/internships.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/employers/">Employers</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Employers - Internships</h2>
                        
                        <div>
                           
                           <h3>What is an internship?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><em>"An internship is a form of experiential learning that integrates knowledge and theory
                                       learned in the classroom with practical application and skills development in a professional
                                       setting. Internships give students the opportunity to gain valuable applied experience
                                       and make connections in professional fields they are considering for career paths;
                                       and give employers the opportunity to guide and evaluate talent."</em></p>
                                 
                                 <p>-- <em>The National Association of Colleges and Employers [NACE]</em></p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>How many hours can a student work?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Students are required to work a minimum of 80 hours per semester for every internship
                                    credit hour they receive from Valencia. Students may register for up to four internship
                                    credit hours while attending Valencia. <strong><em>Registration for credit is a requirement for the students.</em></strong> The employer determines the student's internship work schedule.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Do interns receive pay?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>We encourage employers to compensate interns.</p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>What is expected of employers participating in the internship program?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>We expect each employer to provide meaningful duties that would enhance the student's
                                    understanding of his/her career field. In addition, we solicit your feedback about
                                    the intern's job performance.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Can I hire the student at the end of the internship program?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Absolutely! Valencia encourages our employer partners to hire interns as long it does
                                    not impede the student's ability to complete their educational program.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>How do I get an intern?</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Before setting up an internship, employers are encouraged to contact the Internship
                                    &amp; Workforce Services Employer Relations Coordinator for additional information and
                                    program guidelines:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Kamla Charles - (407) 582-3428 or <a href="mailto:kbillings1@valenciacollege.edu?subject=How%20do%20I%20get%20an%20intern%3F">kbillings1@valenciacollege.edu</a></li>
                                    
                                 </ul>
                                 
                                 <p>Internship and Workforce Services Offices uses an online career management system,
                                    <a href="https://valencia-csm.symplicity.com/employers/?signin_tab=0&amp;clearlocalreg=1" target="_blank">Navigator</a>, for posting internships. Once an account is created and your position is posted,
                                    approved Valencia interns will send their resumes directly to you for review and consideration.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <div>
                           
                           <p><strong>Post Internships and Jobs in NAVIGATOR</strong></p>
                           
                           <ul>
                              
                              <li>Once your account is created, it comes to our staff for approval. Please allow two
                                 (2) business days for approval.
                              </li>
                              
                              <li>Once approved, you will receive an email with login/password information.</li>
                              
                              <li>In Navigator you can add new job postings and/or internships, request dates for on-campus
                                 recruitment, review resumes of applicants, check out upcoming events, and add or remove
                                 recruiters for your company.
                              </li>
                              
                              <li>All questions and concerns about Navigator will be personally received and answered
                                 by Debra Sembrano who can be reached at <a href="mailto:dsembrano@valenciacollege.edu">dsembrano@valenciacollege.edu</a>.
                              </li>
                              
                           </ul>
                           
                           <p><a href="https://valencia-csm.symplicity.com/employers/" target="_blank">Navigator</a></p>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3"></aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/employers/internships.pcf">©</a>
      </div>
   </body>
</html>