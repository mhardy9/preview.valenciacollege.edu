<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/employers/on-campus-recruitment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/employers/">Employers</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Employers - On-Campus Recruitment</h2>
                        
                        <p><strong>What is On-Campus Recruitment (OCR)?</strong><br> On-Campus Recruiting (OCR) is the service that enables organizations to come to the
                           campuses to recruit students for employment opportunities. Managed by Internship and
                           Workforce Services, OCR schedules many employers each year to visit any one of our
                           four campuses.
                        </p>
                        
                        <p><strong>How it works:</strong></p>
                        
                        <ul>
                           
                           <li>Our office provides a table and two chairs from 9:00 am until 2:00 pm., Mondays through
                              Thursdays.
                           </li>
                           
                           <li>Employers are welcome to visit each campus (<a href="/locations/map/east.php">East</a>, <a href="/locations/map/lake-nona.php">Lake Nona</a>, <a href="/locations/map/osceola.php">Osceola</a>, <a href="/locations/map/west.php">West</a>, <a href="/locations/map/winter-park.php">Winter Park</a>) twice per month.
                           </li>
                           
                           <li>The request will need to be submitted to our office at least 10 business days prior
                              to the date you wish to visit.
                           </li>
                           
                           <li>To update or modify a request that has already been submitted for processing contact
                              Kamla Charles at (407) 582-3428 or at <a href="mailto:kbillings1@valenciacollege.edu">kbillings1@valenciacollege.edu</a> for assistance.
                           </li>
                           
                           <li>Please allow at least two weeks from the moment the request is created to have your
                              request processed.
                           </li>
                           
                           <li>An email will be sent directly to you once your request has been processed.</li>
                           
                        </ul>
                        
                        <p><strong>Employer Recruitment Policies and Procedures :</strong><br> In an effort to ensure that students’ best interests and rights are protected, rules
                           and procedures have been established which define the parameters within which business
                           representatives can interact with students when seeking to fill jobs or internship
                           positions. Refer to our <a href="/students/internship/employers/policies-and-procedures.php">Employer Recruitment Policies and Procedures</a>.
                        </p>
                        
                        <p><strong>Submit a Request:</strong><br> Requests must be submitted through Navigator, our online jobs and internships database.
                        </p>
                        
                        <ul>
                           
                           <li>Log into/Create your company’s Navigator account</li>
                           
                           <li>Locate and click on the EVENTS tab</li>
                           
                           <li>From the EVENTS tab, select the sub-tab titled ON-CAMPUS RECRUITMENT</li>
                           
                           <li>To begin a new request, click on the "+Add New" button</li>
                           
                           <li>Complete the form and submit</li>
                           
                        </ul>
                        
                     </div>
                     
                     <aside class="col-md-3"></aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/employers/on-campus-recruitment.pcf">©</a>
      </div>
   </body>
</html>