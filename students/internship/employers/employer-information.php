<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/employers/employer-information.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/employers/">Employers</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <div>
                           
                           <h2>Spring 2018&nbsp;Job Fair Employer Information</h2>
                           
                           <h3>Thursday,&nbsp;March 29, 2018<br> 10 am to 2 pm<br> West Campus, Special Events Center
                           </h3>
                           
                           <p><span>Internship and Workforce Services is hosting the Spring 2018 Job Fair in partnership
                                 with the Alumni Association and Career Centers. Our Job Fair will provide you with
                                 the opportunity to engage with qualified candidates seeking full-time and part-time
                                 employment. The fair is open to all current students and alumni and attracts participants
                                 from all majors across the college.</span></p>
                           
                           <h3>Eligibility and Registration</h3>
                           
                           <p><span>In order to participate in the Job Fair, your organization must be recruiting for
                                 salaried or hourly paid employment opportunities or internships. Additionally, you
                                 must adhere to our&nbsp; <a href="/students/internship/employers/policies-and-procedures.php">Employer Recruitment Policies and Procedures</a>.</span></p>
                           
                           <p><span>New this year, we are offering Early Bird registration at $75. The early bird registration
                                 and payment deadline is February 2, 2018. Payment must be received by early bird deadline
                                 to qualify for early bird rate. Early bird registration includes the same perks as
                                 regular registration.</span></p>
                           
                           <p><span>Each regular registration is $100 and includes:</span></p>
                           
                           <ul>
                              
                              <li>Admissions for two company representatives*</li>
                              
                              <li>A six foot table with two chairs</li>
                              
                              <li>Continental breakfast and lunch</li>
                              
                              <li>Wireless access</li>
                              
                              <li>Company name and recruitment areas printed in student brochure</li>
                              
                           </ul>
                           
                           <p>*Two additional representatives may be admitted for an additional $25 each.</p>
                           
                           <p>Interested in becoming a Spring Job Fair sponsor? Select one of the sponsorship levels
                              during the registration process.&nbsp;&nbsp;<span>Click below to see what’s included in the different sponsorships.</span></p>
                           
                           <p>Spring 2018 Job Fair Employer Sponsorship Information</p>
                           
                           <h3><a title="Employer Registration for Job Fair" href="https://valenciacc.ut1.qualtrics.com/jfe/form/SV_5AX6iEPnOQlP5WZ?FM=WB">Register for the Job Fair</a></h3>
                           
                           <p>&nbsp;</p>
                           
                           <h3>Payment Deadline and Cancellation Policy</h3>
                           
                           <p><span>Online registration will close on Friday, March 2, 2018 at 5pm. After you receive
                                 your event invoice, please pay for your registration. We accept payment via credit
                                 card or check.&nbsp;</span><strong>Note, registration is not complete until payment is received.</strong><span><span>&nbsp;</span></span><strong>Early bird registration and payment deadline is February 2, 2018. If payment is not
                                 received by this date, your organization will forfeit the early bird rate and will
                                 be required to pay the regular rate of $100 to register for the job fair.</strong></p>
                           
                           <p><span>Registration cancellation/refunds MUST be made by email to Kamla Charles at&nbsp;</span><a href="mailto:kbillings1@valenciacollege.edu">kbillings1@valenciacollege.edu</a><span>.<span>&nbsp;</span></span><strong>No refunds will be made after March 2, 2018</strong><span>. This includes registering for the event and not showing the day of.</span></p>
                           
                           <p><span>For more information, contact Employer Relations Coordinator, Kamla Charles at&nbsp;</span><a href="mailto:kbillings1@valenciacollege.edu">kbillings1@valenciacollege.edu</a><span>&nbsp;or (407) 582-3428.</span></p>
                           
                        </div>
                        
                        <p><a href="#top">TOP</a></p>
                        
                     </div>
                     
                     <aside class="col-md-3"></aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/employers/employer-information.pcf">©</a>
      </div>
   </body>
</html>