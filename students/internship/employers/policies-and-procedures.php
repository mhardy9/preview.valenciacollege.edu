<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/employers/policies-and-procedures.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/employers/">Employers</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Employer Recruitment Policies and Procedures</h2>
                        
                        <p>Valencia College's Internship and Workforce Services thanks you for your interest
                           in our students. Our goal is to provide the best possible service to our employers,
                           as well as provide excellent opportunities for our students. The following rules and
                           procedures have been established which define the parameters within which business
                           representatives can interact with students when seeking to fill jobs or internship
                           positions. The policies apply to both on campus recruiting activities and the use
                           of <a href="http://valencia-csm.symplicity.com/employers/">Navigator</a> - Valencia College's electronic job and internship posting system.
                        </p>
                        
                        <div>
                           
                           <h3>Programs and Services Offered</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>In support of Valencia College's mission of providing opportunities for academic,
                                    technical, and lifelong learning in a collaborative culture dedicated to inquiry,
                                    results, and excellence, Internship and Workforce Services delivers a variety of services
                                    related to employment, career exploration, and work experience. These services include,
                                    but are not limited to:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Internships for Academic Credit</li>
                                    
                                    <li>Job Fairs and Networking Events</li>
                                    
                                    <li><a title="Navigator" href="http://valencia-csm.symplicity.com/employers/">Navigator</a></li>
                                    
                                    <li>Company Student Tours</li>
                                    
                                    <li><a title="On Campus Recruitment" href="/students/internship/employers/on-campus-recruitment.php">On-campus Recruitment</a></li>
                                    
                                    <li>On-campus Interviewing</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Eligibility for Partnerships</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>The mission of Valencia College's Internship and Workforce Services office is to connect
                                    students seeking pre- and post-graduation career opportunities with employers. We
                                    are not a placement office, but rather, we provide an opportunity for employers to
                                    engage with and recruit their future workforce and students to connect to employment
                                    and internship experiences. Valencia College Internship and Workforce Services abides
                                    by the <a title="NACE" href="http://www.naceweb.org/principles/" target="_blank">National Association of Colleges and Employers (NACE) Principles for Professional
                                       Practice</a> and expects employers to do the same. Internship and Workforce Services reserves
                                    the right to accept or deny any positions or recruiting organizations based upon the
                                    needs and interests of our students. Additionally, Internship and Workforce Services
                                    reserves the right to deny and/or remove access rights, without notification and in
                                    Valencia's sole discretion, for any organization or employer that fails, at any time,
                                    to meet the requirements listed below and/or fails to adequately comply with the policies
                                    and procedures of Valencia College, NACE, or applicable law.
                                 </p>
                                 
                                 <p>By utilizing services and participating in events hosted by Internship and Workforce
                                    Services, employers and organizations agree:
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>That Internship and Workforce Services reserves the right, in its sole discretion,
                                       to deny to any recruiter, vendor, employer, schools, or organization participation
                                       in any recruitment event or service including, but not limited to: on-campus recruiting,
                                       employment postings, job fairs, or networking events.
                                    </li>
                                    
                                    <li>Not to take or use photos or videos of events or students without prior, proper written
                                       consent.
                                    </li>
                                    
                                    <li>That many student education records are protected by the Family Educational Rights
                                       and Privacy Act (FERPA) and federal regulations issues pursuant thereto, and by Florida
                                       Statute Section 1002.22, and that generally, written student consent must be obtained
                                       before releasing personally identifiable student education records to anyone other
                                       than Valencia. Valencia agrees to provide guidance to employers with respect to complying
                                       with the provisions of FERPA, and employers agree to treat all student education records
                                       specifically identified as such confidentially and not to disclose to such records
                                       except to Valencia or as required or permitted by law. Under no circumstances may
                                       student information be sold to any third party or used for data mining or other marketing
                                       purposes. Failure to comply with these requirements may constitute grounds for immediate
                                       dismissal from all Internship and Workforce Services related services and events.
                                    </li>
                                    
                                    <li>All job opportunities are posted at the discretion of Internship and Workforce Services
                                       through Navigator- the electronic job posting database, which is provided free of
                                       charge to qualified employers and to all Valencia College students and alumni.
                                    </li>
                                    
                                    <li>That Internship and Workforce Services makes no guarantees about and will not be held
                                       responsible for a particular student's suitability or performance.
                                    </li>
                                    
                                    <li>To adhere to the policies and procedures as laid out by Valencia College's Internship
                                       and Workforce Services in conjunction with federal and state EEO laws, regulations,
                                       standards, and guidelines, Valencia Policies and Procedures, as well as the Principles
                                       of Professional Practice outlined by the National Association of Colleges and Employers
                                       (NACE).
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Job and Internship Listing</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Valencia's Internship and Workforce Services office uses Navigator, an online database
                                    for posting permanent part-time and full-time job positions, as well as internships.
                                    Advertisement of a job vacancy or internship position on Navigator does not indicate
                                    an endorsement or recommendation by Valencia College or Internship and Workforce Services.
                                    The Internship and Workforce Services office does not act as a placement agent and
                                    cannot guarantee student or alumni applicants for job vacancies and/or internship
                                    positions. Additionally, Internship and Workforce Services reserves the right to approve
                                    jobs and internships that will enhance the students' overall career experience
                                 </p>
                                 
                                 <p>The following positions will not be posted:</p>
                                 
                                 <ul>
                                    
                                    <li>Positions that appear to discriminate against applicants on the basis of race, color,
                                       religion, sex, national origin, age, disability, gender, genetic information or any
                                       other protected category recognized by Valencia's policies or applicable law.
                                    </li>
                                    
                                    <li>Positions that require any form of financial investment from students prior to and/or
                                       during employment.
                                    </li>
                                    
                                    <li>Positions that compensate exclusively on commission.</li>
                                    
                                    <li>Positions that require employment in a private home. (i.e. Personal assistant, caregiver,
                                       babysitting, in-home tutoring, etc.)
                                    </li>
                                    
                                    <li>Positions that, in Valencia's discretion, are not appropriate opportunities for our
                                       students and/or do not comply with requirements stated in this document.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>All employer profile registrations and job and internship posting are set to pending
                                    status until approved by our staff, which takes approximately two business days.
                                 </p>
                                 
                                 <p>Full-time and part-time employment opportunities, as well as internships are allowed
                                    to be posted in Navigator for six months, after which the posting will expire. It
                                    is the responsibility of the employer to login to Navigator to repost or extend the
                                    posting if additional time is needed for recruitment purposes.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>On-campus Recruitment (OCR)</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Potential employers seeking to conduct on-campus student recruiting; which includes
                                    but is not limited to conducting prescheduled interviews, setting up a table in a
                                    common area, etc. must contact Internship and Workforce Services for permission and
                                    to schedule an appropriate date, time, and location.
                                 </p>
                                 
                                 <p>Employers conducting on-campus recruiting may not require any up front associated
                                    costs for students related to the students being employed or being considered for
                                    employment. This includes but is not limited to membership fees, startup fees, fees
                                    for lessons or training, licensing fees, portfolio fees, placement fees, and the purchase
                                    of tools, samples, or sales kits.
                                 </p>
                                 
                                 <p>On-campus recruitment activities may not be combined with the marketing and/or sale
                                    of products or services to students.
                                 </p>
                                 
                                 <p>When recruiting from a table which is provided in a common area, employers must restrict
                                    all recruiting activities to the immediate vicinity of the table and refrain from
                                    distributing flyers or approaching students in other nearby areas unless separate
                                    permission is granted by Valencia pursuant to its policies.
                                 </p>
                                 
                                 <p>The college retains the right to demand recruiters vacate college property if they
                                    fail to comply with any of the above procedures or disturb the educational environment.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Third-Party Recruiters</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>Definition of Third-Party Recruiter: In accordance with NACE Principles of Professional
                                    Conduct, thirdparty recruiters are defined as agencies, organizations, or individuals
                                    recruiting candidates for temporary, part-time, or full-time employment opportunities
                                    other than for their own needs. This includes entities that refer or recruit for profit
                                    or not for profit, and it includes agencies that collect student information to be
                                    disclosed to employers for purposes of recruitment and employment.
                                 </p>
                                 
                                 <p>In providing services for third-party recruiters, our policy requires that:</p>
                                 
                                 <ul>
                                    
                                    <li>Third-party recruiters must identify themselves as such within Navigator.</li>
                                    
                                    <li>Third-party recruiters may be asked to identify the client company they are recruiting
                                       for.
                                    </li>
                                    
                                    <li>Third-party recruiters who charge students for services or require any other upfront
                                       costs to students are prohibited.
                                    </li>
                                    
                                    <li>Third-party recruiters will not disclose to any employer, including the client-employers,
                                       any student information without obtaining prior written consent from the student.
                                       Under no circumstances can student information be disclosed other than for the original
                                       recruiting purposes nor can it be sold or provided to other entities.
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3>Resources</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p>NACE Principles for Professional Practice for Career Services and Employment Professionals:
                                    <a title="nanceweb" href="http://www.naceweb.org/principles/">http://www.naceweb.org/principles/</a></p>
                                 
                                 <p>Equal Employment Opportunity Standards: <a title="EEOC" href="https://www.eeoc.gov/laws/statutes/index.cfm">https://www.eeoc.gov/laws/statutes/index.cfm</a></p>
                                 
                                 <p>Family Educational Rights and Privacy Act (FERPA): <a title="FERPA" href="http://www2.ed.gov/policy/gen/guid/fpco/ferpa/index.html">http://www2.ed.gov/policy/gen/guid/fpco/ferpa/index.html</a></p>
                                 
                                 <p>Valencia College Policy Manual: <a title="Policy manual" href="/students/about/general-counsel/policy/index.html">http://valenciacollege.edu/generalcounsel/policy/</a></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3"></aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/employers/policies-and-procedures.pcf">©</a>
      </div>
   </body>
</html>