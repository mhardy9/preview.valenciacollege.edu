<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/employers/pt-ft-jobs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li><a href="/students/internship/employers/">Employers</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Employers - Post a&nbsp;Job or Internship</h2>
                        
                        <p>Create connections with current Valencia students, prospective graduates, and alumni
                           with&nbsp;<a title="Navigator" href="http://valencia-csm.symplicity.com/employers/">Navigator</a>.
                        </p>
                        
                        <h3>Registering and Posting in Navigator</h3>
                        
                        <ol>
                           
                           <li>Go to <a href="http://valencia-csm.symplicity.com/employers/">Navigator</a>.
                           </li>
                           
                           <li>Click on "Sign Up" or "Sign Up &amp; Post Internship/Job"</li>
                           
                           <li>Enter your&nbsp;organization's information and click "Submit."</li>
                           
                           <li>If you clicked "Sign Up &amp; Post Internship/Job," you will be able to post at the same
                              time as your registration.
                           </li>
                           
                        </ol>
                        
                        <h3>Information About&nbsp;for Registering and Posting in Navigator</h3>
                        
                        <ul>
                           
                           <li>Once your account is created, it comes to our staff for approval.<strong> Please allow two (2) business days for approval.</strong></li>
                           
                           <li>In Navigator, you are able to do the following:
                              
                              <ul>
                                 
                                 <li>post part-time, full-time, and internships</li>
                                 
                                 <li>review submitted resumes</li>
                                 
                                 <li>make offers</li>
                                 
                                 <li>register for on-campus recruitment</li>
                                 
                                 <li>check out upcoming events</li>
                                 
                                 <li>add or remove recruiters for your company</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Requesting On-Campus Recruitment in Navigator</h3>
                        
                        <ol>
                           
                           <li>Log in to your organization's&nbsp;<a href="http://valencia-csm.symplicity.com/employers/">Navigator</a>&nbsp;account.&nbsp;&nbsp;
                           </li>
                           
                           <li>Locate and click on the "Events" tab.</li>
                           
                           <li>Under "Events," select the sub-tab titled "On-Campus Recruitment."</li>
                           
                           <li>Click on "Request On-Campus Recruitment."</li>
                           
                        </ol>
                        
                        <div>
                           
                           <h3>Notes&nbsp;About Navigator</h3>
                           
                           <ul>
                              
                              <li>Allow two (2) business days for registration and job/internship post approval.</li>
                              
                              <li>On-campus recruitment requests must be submitted at least <strong>10 business days prior</strong> to date your organization wishes to visit.
                              </li>
                              
                              <li>Valencia College does not charge employers for this service.</li>
                              
                              <li>Positions are posted online only.&nbsp; No paper flyers will be distributed on campus.</li>
                              
                              <li>Email blasts are periodically sent to students who meet criteria for positions posted.</li>
                              
                              <li>Usage of Navigator is subject to our <a href="/students/internship/policies-and-procedures.php">Policies and Procedures</a>.
                              </li>
                              
                           </ul>
                           
                           <h3>Navigator Assistance</h3>
                           
                           <p>&nbsp;If you require assistance, please contact Debra Sembrano.</p>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3"></aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/employers/pt-ft-jobs.pcf">©</a>
      </div>
   </body>
</html>