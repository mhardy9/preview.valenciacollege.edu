<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/employers/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Employers</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Employers</h2>
                        
                        <p>We are delighted that you are interested in hiring Valencia College’s students to
                           fulfill your workforce needs or provide internship opportunities. The Internship and
                           Workforce Services Office provides different methods for recruiters to reach out to
                           our student population on all campuses. We can assist with the following:
                        </p>
                        
                        <ul>
                           
                           <li>Posting your internships, part-time, and full-time opportunities</li>
                           
                           <li>Scheduling individual on-campus recruitment sessions</li>
                           
                           <li>Hiring almost/recent grads for permanent, degree-specific positions</li>
                           
                        </ul>
                        
                        <p>&nbsp;</p>
                        
                        <div class="banner"><i class="iconcustom-school"></i>
                           
                           <h3>Spring 2018 Job Fair</h3>
                           
                           <p><span>Internship and Workforce Services is hosting the Spring 2018 Job Fair in partnership
                                 with the Alumni Association and Career Centers. Our Job Fair will provide you with
                                 the opportunity to engage with qualified candidates seeking full-time and part-time
                                 employment.</span></p>
                           <a href="/students/internship/job-fair.php" target="" class="banner_bt">Job Fair Information</a></div>
                        <br>&nbsp;
                        
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-desktop"></i><h3>Post a Job or Internship</h3>
                                 <p>let students and alumni know your company is hiring through Navigator</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-plug"></i><h3>Connect with Students</h3>
                                 <p>learn about the various ways to connect with Valencia students</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-calendar"></i><h3>2017-2018 Recruitment Calendar</h3>
                                 <p>know when are the best times to recruit at Valencia</p></a>
                              
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-users"></i><h3>On-Campus Recruitment</h3>
                                 <p>come to campus to recruit Valencia students</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-microphone"></i><h3>Advisory Board</h3>
                                 <p>provides expertise about the work-based learning experience for students</p></a>
                              
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <a class="box_feat" href="/" target=""><i class="far fa-shield"></i><h3>Policies and Procedures</h3>
                                 <p>rules for recruiting from Valencia College</p></a>
                              
                           </div>
                        </div>
                        
                        <p>&nbsp;</p>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h2>Testimonials</h2>
                        
                        <p><em>I think they do a great job with find a student who is looking for an internship.</em></p>
                        
                        <p>- Random person</p>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/employers/index.pcf">©</a>
      </div>
   </body>
</html>