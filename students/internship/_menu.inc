<ul>
<li><a href="/students/internship/index.php">Internship and Workforce Services</a></li>
<li class="submenu"><a class="show-submenu" href="/students/internship/about-us.php">About Us</a>
<ul>
<li><a href="/students/internship/meet-the-staff.php">Meet the Staff</a></li>
<li><a href="/students/internship/faq.php">FAQ</a></li>
<li><a href="/students/internship/hours-locations.php">Hours and Locations</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/internship/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
</li>
<li class="submenu"><a class="show-submenu" href="/students/internship/students/index.php">Students</a>
<ul>
<li><a href="/students/internship/students/internships.php">Internships</a></li>
<li><a href="/students/internship/students/workforce-services.php">Workforce Services</a></li>
<li><a href="/students/internship/students/job-fairs.php">Job Fair Information</a></li>
</ul>
</li>
<li class="submenu"><a class="show-submenu" href="/students/internship/employers/index.php">Employers</a>
<ul>
<li><a href="/students/internship/employers/internships.php">Internships</a></li>
<li><a href="/students/internship/employers/pt-ft-jobs.php">PT / FT Jobs</a></li>
<li><a href="/students/internship/employers/employer-information.php">Job Fair Information</a></li>
<li><a href="/students/internship/employers/on-campus-recruitment.php">On-Campus Recruitment</a></li>
<li><a href="/students/internship/employers/policies-and-procedures.php">Policies &amp; Procedures</a></li>
</ul>
</li>
</ul>