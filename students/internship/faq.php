<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/faq.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Frequently Asked Questions</h2>
                        
                        <div>
                           
                           <div>
                              
                              <h3><a href="#Students">Student FAQ</a> | <a href="#Employers">Employer FAQ</a></h3>
                              
                              <h3>&nbsp;</h3>
                              
                              <h3><a id="Students">Students</a></h3>
                              
                              <div>
                                 
                                 <h3>I'm having trouble logging in to Navigator.</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Be sure you are using your Atlas user name (not email address) and password. If you
                                          are still unable to log in, please contact Debra at <a href="mailto:dsembrano@valenciacollege.edu"><strong>dsembrano@valenciacollege.edu</strong></a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>Are there internships in my program area?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The internship opportunities that are available change every semester. If you would
                                          like to learn more about the internship program, please contact the internship office
                                          on your campus. To check for internship opportunities, log in to <a href="http://valencia-csm.symplicity.com/students/">Navigator</a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>What are the eligibility requirements for the internship program?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>You have to be a Valencia student and able to register for internship credits. You
                                          also have to have at least 12 credits completed, a 2.0 or higher GPA, basic reading
                                          and math courses completed (based on your degree), and the program-specific requirement
                                          for your major found in the&nbsp;<a href="/students/internship/students/pre-requisites.php">Internship Prerequisites Tool</a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>Why can't I fill out the internship profile (right now)?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The profile opens and closes based on our open enrollment dates:</p>
                                       
                                       <div data-old-tag="table">&nbsp;</div>
                                       
                                       <p>If you have further questions about filling out your internship profile in <a href="http://valencia-csm.symplicity.com/students/">Navigator</a>, please contact Debra at<strong> <a href="mailto:dsembrano@valenciacollege.edu">dsembrano@valenciacollege.edu</a>.</strong></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>It's after the deadline, but I have a position. Can I register for an internship?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>You will need to speak with an internship coordinator to request special permission
                                          to register after our deadline. <a href="http://net4.valenciacollege.edu/forms/internship/contact.cfm" target="_blank">Contact Us</a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>How do I find an on-campus job?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>The Internship and Workforce Services Office does not handle on-campus employment.
                                          Please contact Financial Aid to learn about the Work Study program if you are interested
                                          in working on campus as a student.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>Why was my internship application denied?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>For clarification on any internship eligibility decision, please contact the internship
                                          coordinator from whom you received the eligibility message stating that you were not
                                          approved.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>Why can't I register for the internship?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>You must follow the enrollment procedures in order to register for any internship
                                          course. Your first step is to complete an application. Details can be found in the&nbsp;<a href="/students/internship/students/internships.php">student internship page</a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>How do I get involved with Workforce?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Our department is not associated with CareerSource (formerly Workforce Central Florida).
                                          We assist students by offering workforce services in the form of job postings and
                                          employment opportunities. You may find details on what we offer on the&nbsp;<a href="/students/internship/students/workforce-services.php">workforce services page</a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>I submitted an internship application. What is my next step?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>You will receive an email from the internship coordinator once a decision has been
                                          made about your eligibility to participate in the internship program. If you would
                                          like to confirm that your application has been received, please contact Debra at <a href="mailto:dsembrano@valenciacollege.edu"><strong>dsembrano@valenciacollege.edu</strong></a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <h3><a id="Employers">Employers</a></h3>
                              
                              <div>
                                 
                                 <h3>How can I build my brand on campus?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Employers are encouraged to connect with Valencia students, faculty, and staff in
                                          a variety of ways. The Internship &amp; Workforce Services office can assist in facilitating
                                          efforts including, but not limited to:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Annual Job Fair</li>
                                          
                                          <li>Employer Panel Discussions</li>
                                          
                                          <li>Facility Tours and Site Visits</li>
                                          
                                          <li>Sponsorship of Events</li>
                                          
                                          <li>Sponsorship of Faculty Meetings</li>
                                          
                                          <li>Advisory Board Membership</li>
                                          
                                       </ul>
                                       
                                       <p>Please contact our Employer Relations Coordinator <a href="mailto:kbillings1@valenciacollege.edu">Kamla Charles</a>, to find out more information about these and other partnetship opportunities.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>How can I come on campus to recruit students?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>All On-Campus Recruiting (OCR) requests must be submitted through Navigator, our online
                                          jobs and internships database. Learn more about how you can begin this process by
                                          visiting the <a href="/students/internship/employers/on-campus-recruitment.html">On-Campus Recruitment</a> page. Please contact <a href="mailto:kbillings1@valenciacollege.edu?subject=On-Campus Recruiting">Kamla Charles</a> for more information.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>How can I post PT/FT jobs?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>You may post jobs in Navigator, our online database. To post a job, <a href="https://valencia-csm.symplicity.com/employers/index.php" target="_blank"><strong>please create an account. </strong></a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>How do I hire an intern?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Please contact one of the <a href="/students/internship/meet-the-staff.php">Internship &amp; Workforce Services Coordinators</a> to learn more about our internship process.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <h3>How can I participate in a job fair?</h3>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <p>Valencia will host an annual Job Fair every Spring semester. Employers from all industries
                                          are invited to participate in this event. Please contact <a href="mailto:kbillings1@valenciacollege.edu">Kamla Charles</a> for more information.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        There are no events at this time.
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/faq.pcf">©</a>
      </div>
   </body>
</html>