<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Internship and Workforce Services | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/internship/meet-the-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/internship/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Internship and Workforce Services</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/internship/">Internship</a></li>
               <li>Internship and Workforce Services</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9"><a id="content" name="content"></a>
                        
                        <h2>Meet the Staff</h2>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <h3 style="text-align: left;">District Office Team</h3>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <table class="table ">
                                       
                                       <tbody>
                                          
                                          <tr>
                                             
                                             <td>
                                                
                                                <p><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" width="150" height="150"></p>
                                                
                                                <p>150 x 150</p>
                                                
                                             </td>
                                             
                                             <td>
                                                
                                                <p><strong>Michelle Terrell</strong><br><em>Director, Internship and Workforce Services</em><br><a href="mailto:mterrell8@valenciacollege.edu">mterrell8@valenciacollege.edu</a></p>
                                                
                                                <p>Michelle first worked for Valencia in various positions from 1999 – 2005 and returned
                                                   to the Career and Workforce Education team at Valencia in 2015. She is currently serving
                                                   as the Director of Internship and Workforce Services. Michelle has spent the past
                                                   23 years serving and assisting students in the areas of workforce education, college
                                                   and career planning, and graduate education.
                                                </p>
                                                
                                                <p>Michelle holds a Master in Science degree in Counseling from Alabama State University,
                                                   a Master in Arts degree in Human Resources Management from Webster University, and
                                                   a Bachelor of Science degree in Psychology from Troy University.
                                                </p>
                                                
                                                <p>In her spare time, Michelle enjoys traveling, reading, spending time with family and
                                                   volunteering.
                                                </p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" width="150" height="150"></td>
                                             
                                             <td>
                                                
                                                <p><strong>Kamla Charles</strong><br><em>Coordinator, Employer Relations</em><br><a href="mailto:kbillings1@valenciacollege.edu">kbillings1@valenciacollege.edu</a>&nbsp;
                                                </p>
                                                
                                                <p>Kamla began working at Valencia College in February 2014 as a Career Advisor on the
                                                   West campus. Currently, she serves as the Coordinator of Employer Relations college-wide.
                                                   She is responsible for building, developing, and maintaining effective relationships
                                                   with employers and community partners in the Central Florida region. Her efforts are
                                                   aimed at creating and expanding internship, employment, and other work-based learning
                                                   experiences for Valencia students.
                                                </p>
                                                
                                                <p>She holds a Master of Science in College Student Affairs from Nova Southeastern University
                                                   and a Bachelor of Science in Family, Youth, and Community Sciences from the University
                                                   of Florida.
                                                </p>
                                                
                                                <p>Kamla enjoys cooking, crafting, and the fine arts. In her spare time she likes traveling
                                                   and hopes to visit at least one new destination each year.
                                                </p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" width="150" height="150"></td>
                                             
                                             <td>
                                                
                                                <p><strong>Debra Sembrano</strong><br><em>Technical Specialist</em><br><a href="mailto:dsembrano@valenciacollege.edu">dsembrano@valenciacollege.edu</a></p>
                                                
                                                <p>Debra joined Valencia College in January 2016 as the Technical Specialist for Internship
                                                   and Workforce Services. She is responsible for managing and supporting the software
                                                   used to connect students and employers for internship and employment opportunities,
                                                   designing reports and queries, analyzing student data, developing documentation, and
                                                   overseeing the Internship and Workforce Services and Career and Workforce Education
                                                   websites.
                                                </p>
                                                
                                                <p>She has a Bachelor of Science degree in Management Information Systems and a Bachelor
                                                   of Arts in Music degree both from Florida State University.
                                                </p>
                                                
                                                <p>Debra enjoys traveling and learning about technology. She has backpacked through Europe
                                                   and Asia.&nbsp;
                                                </p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                          <tr>
                                             
                                             <td><img src="/_resources/images/course_1_thumb.jpg" alt="Valencia image description" width="150" height="150"></td>
                                             
                                             <td>
                                                
                                                <p><strong>Janice Callaway</strong><br><em>Implementation Coordinator</em><br><a href="mailto:jcallaway5@valenciacollege.edu">jcallaway5@valenciacollege.edu</a></p>
                                                
                                                <p>Janice came to Valencia as a temp for 5 weeks with the Internship &amp; Placement Office
                                                   in 2006. She was offered the position of part-time staff assistant in July of that
                                                   year. In July 2011 she was offered the position of Adminstrative Assistant. In August
                                                   2016, she was promoted to Implementation Coordinator.
                                                </p>
                                                
                                                <p>In her spare time she loves spending time with her family and enjoys gardening. She
                                                   welcomed the newest addition to her family, her great grand-daughter (Bianca) in February
                                                   2015
                                                </p>
                                                
                                             </td>
                                             
                                          </tr>
                                          
                                       </tbody>
                                       
                                    </table>
                                    
                                    <p>&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>&nbsp;</p>
                                    
                                    <hr>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p>&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>East Campus Team</h3>
                        
                        <div class="row">
                           <div class="col-md-10">
                              <ul class="list_4"></ul>
                           </div>
                        </div>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong><a href="/students/contact/Search_Detail2.cfm-ID=kfleming22.html">Kerry Fleming</a>, Coordinator, Internship and Workforce Services</strong></p>
                                    
                                    <p>Kerry started working at Valencia College in January 2015 with 18+ years in higher
                                       education, mainly in employer relations and career services.
                                    </p>
                                    
                                    <p>Kerry serves as a liaison at the Valencia East Campus Internship and Workforce Services
                                       Office, where she manages services to students and employers. &nbsp;She facilitates student
                                       learning by assisting students in securing an appropriate internship that will enhance
                                       their overall academic experience and how to conduct a successful job search. She
                                       coordinates, develops and recruits employers for degree related internships, along
                                       with identifying and responding to industry requests regarding internships. She also
                                       works alongside faculty and academic departments to identify and promote internships
                                       for specific majors.
                                    </p>
                                    
                                    <p>Kerry holds a Bachelor’s degree in Business Administration from Tuskegee, Alabama.&nbsp;</p>
                                    
                                    <hr>
                                    
                                    <p>&nbsp;</p>
                                    
                                    <p><strong><a href="http://valenciacollege.edu/contact/Search_Detail2.cfm?ID=jwallace37">Janine Wallace</a>, Staff Assistant II</strong></p>
                                    
                                    <p dir="auto">Janine is a New Jersey native who permanently moved to Orlando in 1996. She is also
                                       a 7-year Air Force Veteran with an extensive background in Aviation Management, Training
                                       and Administrative Support. She graduated from Valencia College with an AA in General
                                       Studies in April 2017. She is currently attending UCF continuing toward a BS in Psychology
                                       and a MS in Social Work. She is also a substitute teacher for Orange County Public
                                       Schools.
                                    </p>
                                    
                                    <p dir="auto">Janine enjoys spending time with her friends and family, fashion, and watching crime
                                       TV.
                                    </p>
                                    
                                    <hr>
                                    
                                    <p dir="auto">&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Osceola and Lake Nona Campus Team</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong><a href="/students/contact/Search_Detail2.cfm-ID=xhumphrey.html">Xavier Humphrey</a>, Coordinator, Internship and Workforce Services</strong></p>
                                    
                                    <p>Xavier Humphrey is the Osceola Campus Internship and Workforce Services Coordinator.
                                       Xavier initially joined Valencia College in August 2013 as a Career Advisor. Xavier
                                       is responsible for coordinating services for students and employers that will enrich
                                       student learning preparation for successful transition into the workforce. This includes
                                       promotion of services to students, faculty and staff in addition to matching students
                                       to internships that correlates to their major and interests.
                                    </p>
                                    
                                    <p>Xavier is a graduate from Valencia College, holds a Bachelor’s degree in Technical
                                       Education and Industry Training and Masters in Career and Technical Education both
                                       from University of Central Florida. Additionally, Xavier holds an Associate’s degree
                                       in Film and Video from Florida Metropolitan University, a Nursing Assistant Certificate
                                       from Orlando Technical College and is a Myers Briggs Personality Type Indicator Certified
                                       Practioner. Xavier’s diverse education and industry experience will be beneficial
                                       to students as he provides them with internship opportunities.
                                    </p>
                                    
                                    <p>Xavier enjoys sports, gaming, comics, exercise in his spare time and hopes to start
                                       traveling more.
                                    </p>
                                    
                                    <hr>
                                    
                                    <p>&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>&nbsp;&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>West and Winter Park Campus&nbsp;Team</h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong><a href="/students/contact/Search_Detail2.cfm-ID=jcamilo4.html">Jessica Camilo</a>, Coordinator, Internship and Workforce Services</strong></p>
                                    
                                    <p>Jessica Camilo joined Valencia College in July 2016 after relocating back to Orlando,
                                       Florida from New York City. &nbsp;While working at a community college in New York, she
                                       was responsible for academic program design, development and implementation that helped
                                       students with low grade point averages, financial issues or other unforeseen circumstances
                                       they encountered while being a college student. She has conducted and created several
                                       leadership and development workshops and trainings focusing on time management, leadership,
                                       social skills and customer service.
                                    </p>
                                    
                                    <p>In her role as the Internship and Workforce Coordinator, she is responsible for collaborating
                                       with faculty and staff to develop and provide resources to students to assist in obtaining
                                       internships and job opportunities. She assists students in obtaining a suitable internship
                                       that will help develop their social and networking skills, while preparing them for
                                       their career and professional goals. &nbsp;
                                    </p>
                                    
                                    <p>She obtained her bachelors of science in Management from Bethune-Cookman University,
                                       in Daytona Beach Florida, as well as an MBA from Nova Southeastern University. She
                                       also carries a specialized certification in Human Resource Management. Jessica has
                                       been working in the field of higher education for over 6 years, with experience in
                                       the areas of admission, enrollment, student affairs, international students, career
                                       services and program development.
                                    </p>
                                    
                                    <p>During her free time, Jessica enjoys exercising, cooking and reading. She also loves
                                       exploring and sightseeing.
                                    </p>
                                    
                                    <hr>
                                    
                                    <p>&nbsp;</p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong><a href="/students/contact/Search_Detail2.cfm-ID=mparada.html">Martha Parada</a>, Staff Assistant II </strong></p>
                                    
                                    <p>Martha joined Valencia College in August 2011 as a full-time student in the English
                                       as a Second Language program. In December 2015, she graduated from Valencia College
                                       with an Associate Degree in Office Administration. In addition, Ms. Parada represented
                                       the Business &amp; Hospitality Division as the Distinguished Graduate in April 2016 for
                                       outstanding academic achievement. Martha is currently pursuing an AA Degree at Valencia
                                       College with the intention to DirectConnect to UCF to obtain a bachelor’s degree.
                                    </p>
                                    
                                    <p>In February 2016, she obtained employment with Valencia College as Staff Assistant
                                       II in the Internship and Workforce Services Office. She is currently providing administrative
                                       support for the West Campus Internship Office.
                                    </p>
                                    
                                    <p>In her spare time, she enjoys exercising and traveling. She is also an active citizen
                                       with a great commitment for community service at her son’s high school.
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h3>Deadlines &amp; Career Events</h3>
                        There are no events at this time.
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/internship/meet-the-staff.pcf">©</a>
      </div>
   </body>
</html>