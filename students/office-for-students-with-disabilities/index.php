<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities  | Valencia College</title>
      <meta name="Description" content="Office for Students with Disabilities exists to determine and ensure reasonable and appropriate accommodations and modifications for qualified students with documented disabilities, to assist students in self-advocacy, to educate the Valencia community about disabilities, and to ensure compliance with the ADAAA, and Section 504 of the Rehabilitation Act.">
      <meta name="Keywords" content="students, disabilities, office, osd, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Office For Students With Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60">
                  
                  <div class="row">
                     
                     <h2>Office for Students with Disabilities (OSD)</h2>
                     
                     <p>The Office for Students with Disabilities (OSD) is committed to the fulfillment of
                        equal educational opportunity, autonomy, and full inclusion for students with disabilities.
                     </p>
                     
                     <p dir="auto"><strong>Commencement:&nbsp;</strong><br>If you or one of your guests requires disability related accommodations to participate
                        in Commencement, please call 407-582-2229 by April 17.&nbsp; Please note that the arena
                        has wheelchair accessible seating for guests.
                     </p>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <h3>Student Handbook</h3>
                        
                        <p>Access information on policies, accommodations and student responsibilities.</p>
                        
                        <div class="button-outline">Download</div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <h3>Deaf &amp; Hard of Hearing Services</h3>
                        
                        <p>Provide equal educational access and opportunity to Deaf and Hard of Hearing students
                           through the provision of professional interpreting, speech to text, and advising services.
                        </p>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <h3>Assistive Technology</h3>
                        
                        <p>In order to access course materials, some students may need to utilize assistive technology.</p>
                        
                        <div class="button-outline">Get Help</div>
                        
                     </div>
                     
                  </div>
                  
                  <h3>Locations</h3>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class=" box_style_4">
                           
                           <h4>East Campus</h4>
                           
                           <ul>
                              
                              <li>Building 5, Room 216</li>
                              
                              <li>Email: <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a></li>
                              
                              <li>Phone: 407-582-2229</li>
                              
                              <li>Fax: 407-582-8908</li>
                              
                              <li>TTY: 407-582-1222</li>
                              
                              <li>Sorenson Video Relay Service (SVRS):
                                 
                                 <ul>
                                    
                                    <li>Building 5, Room 216 (OSD), 407-374-1564</li>
                                    
                                    <li>Building 4, Room 101 (ASC), 407-374-1561</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Accessibility Map: <a href="/students/office-for-students-with-disabilities/documents/access-map-east-campus.pdf" target="_blank">DOWNLOAD</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class=" box_style_4">
                           
                           <h4>Lake Nona Campus</h4>
                           
                           <ul>
                              
                              <li>Email: <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a></li>
                              
                              <li>Phone: 407-582-2229</li>
                              
                              <li>Fax: 407-582-8908</li>
                              
                              <li>TTY: 407-582-1222</li>
                              
                              <li>Sorenson Video Relay Service (SVRS): Building 1, Room 230 (Tutoring Center), 407-233-0734</li>
                              
                              <li>Accessibility Map: <a href="/students/office-for-students-with-disabilities/documents/access-map-lake-nona-campus.pdf" target="_blank">DOWNLOAD</a></li>
                              
                           </ul>
                           <br><br><br></div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class=" box_style_4">
                           
                           <h4>Osceola Campus</h4>
                           
                           <ul>
                              
                              <li>Building 2, Room 102</li>
                              
                              <li>Email: <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a></li>
                              
                              <li>Phone: 407-582-4167</li>
                              
                              <li>Fax: 407-582-4804</li>
                              
                              <li>TY: 407-582-1222</li>
                              
                              <li>Sorenson Video Relay Service (SVRS):
                                 
                                 <ul>
                                    
                                    <li>Building 2, Room 102 (OSD), 321-250-5059</li>
                                    
                                    <li>Building 3, Room 100 (Tutoring Center), 321-250-9867</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Accessibility Map <a href="/students/office-for-students-with-disabilities/documents/access-map-osceola.pdf" target="_blank">DOWNLOAD</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <div class="row">
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class=" box_style_4">
                           
                           <h4>Poinciana Campus</h4>
                           
                           <ul>
                              
                              <li>Email: <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a></li>
                              
                              <li>Phone: 407-582-4167</li>
                              
                              <li>Fax: 407-582-4804</li>
                              
                              <li>TTY: 407-582-1222</li>
                              
                              <li>Sorenson Video Relay Service (SVRS): Building 1, Room 231 (Tutoring Center), 321-250-2933</li>
                              
                              <li>Accessibility Map <a href="/locations/map/documents/valencia-college-campus-map-poinciana.pdf" target="_blank">DOWNLOAD</a></li>
                              
                           </ul>
                           <br><br><br><br></div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class=" box_style_4">
                           
                           <h4>West Campus</h4>
                           
                           <ul>
                              
                              <li>Student Services Building (SSB), Room 102</li>
                              
                              <li>Email: <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a></li>
                              
                              <li>Phone: 407-582-1523</li>
                              
                              <li>Fax: 407-582-1326</li>
                              
                              <li>TTY: 407-582-1222</li>
                              
                              <li>Sorenson Video Relay Service (SVRS):
                                 
                                 <ul>
                                    
                                    <li>Student Services Building (SSB), Room 102 (OSD), VRS: 407-992-8941</li>
                                    
                                    <li>Building 6, Room 101 (Computer Lab), 321-558-7166</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Accessibility Map <a href="/students/office-for-students-with-disabilities/documents/access-map-west-campus.pdf" target="_blank">DOWNLOAD</a></li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     <div class="col-md-4 col-sm-4">
                        
                        <div class=" box_style_4">
                           
                           <h4>Winter Park Campus</h4>
                           
                           <ul>
                              
                              <li>Building 1, Room 212</li>
                              
                              <li>Email: <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a></li>
                              
                              <li>Phone: 407-582-6887</li>
                              
                              <li>Fax: 407-582-6841</li>
                              
                              <li>TTY: 407-582-1222</li>
                              
                              <li>Sorenson Video Relay Service (SVRS): Building 1, Room 136 (CSSC), 321-280-2775</li>
                              
                              <li>Accessibility Map <a href="/students/office-for-students-with-disabilities/documents/access-map-winter-park.pdf" target="_blank">DOWNLOAD</a></li>
                              
                           </ul>
                           <br><br><br><br></div>
                        
                     </div>
                     
                  </div>
                  
                  <p>Have some questions about Sorenson ntouch VP? Review these <a href="/students/office-for-students-with-disabilities/documents/Sorenson-ntouch-VP-FAQ.pdf">Frequently Asked Questions</a> to learn more about the Sorenson ntouch VP or <a href="http://www.sorensonvrs.com/ntouch">visit the Sorenson ntouch website</a>.
                  </p>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/index.pcf">©</a>
      </div>
   </body>
</html>