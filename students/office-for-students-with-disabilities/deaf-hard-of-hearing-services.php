<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/deaf-hard-of-hearing-services.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <main>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>Deaf and Hard of Hearing Services</h2>
                                       
                                       
                                       
                                       
                                       <p>Deaf and Hard of Hearing Services (DHHS) exist to provide equal educational access
                                          and opportunity to Deaf and Hard of Hearing students through the provision of professional
                                          interpreting, speech to text, and advising services.
                                       </p>
                                       
                                       
                                       <p>The goals of DHHS are to assist Deaf and Hard of Hearing students to</p>
                                       
                                       <ul>
                                          
                                          <li>persist in their education to graduation or transfer</li>
                                          
                                          <li>develop self-advocacy skills</li>
                                          
                                          <li>achieve independence through access to all Valencia programs and services</li>
                                          
                                       </ul>
                                       
                                       <p>In order to achieve these goals, we strive to work collaboratively with all members
                                          of the campus community.
                                       </p>
                                       
                                       <h3>Confidentiality</h3>
                                       
                                       
                                       <p>We adhere to strict right-to-privacy guidelines and ethical standards, including the
                                          NAD-RID Code of Professional Conduct and the Family Educational Rights and Privacy
                                          Act (FERPA). Educational information may be shared with Valencia faculty and staff
                                          on a need to know basis according to FERPA guidelines. No information will be divulged
                                          to any outside parties without proper releases.
                                       </p>
                                       
                                       
                                       <h3>Services may include</h3>
                                       
                                       <ul>
                                          
                                          <li>Interpreters</li>
                                          
                                          <li>C-Print -meaning-for-meaning real-time speech-to-text transcription</li>
                                          
                                          <li>Typewell meaning- for-meaning real-time speech-to-text transcription</li>
                                          
                                          <li>CART real-time speech-to-text transcription</li>
                                          
                                          <li>Voice Recognition speech-to-text transcription</li>
                                          
                                          <li>Assistive Listening Device loans</li>
                                          
                                          <li>Collaboration with OSD advisors to provide assistance with admission, educational
                                             and career plans, course selection, and registration
                                          </li>
                                          
                                       </ul>
                                       
                                       <h3>When is it Important to Contact Us?</h3>
                                       
                                       
                                       <p>We support you in your independence as a college student. However, sometimes it is
                                          important that you contact us. You should always contact us:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Before you register for classes.</li>
                                          
                                          <li>After you register for classes to request services.</li>
                                          
                                          <li>If you need an interpreter for a special event (meeting, tutoring, other activity).</li>
                                          
                                          <li>If you are having a hard time in a class.</li>
                                          
                                          <li>If you want to withdraw from a class.</li>
                                          
                                          <li>If you will miss a class or special event and need to cancel your interpreter or speech
                                             to text provider.
                                          </li>
                                          
                                          <li>If you have a problem with your services (for example, if you are not satisfied with
                                             your interpreter, speech to text provider, or notetaker).
                                          </li>
                                          
                                          <li>If you have a problem with your accommodations.</li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Student Requests for Services</h3>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div><a href="http://net4.valenciacollege.edu/forms/osd/interpreter_request.cfm"> <i aria-hidden="true"></i>
                                          
                                          <h3>Interpreter / Speech-to-Text Request</h3>
                                          
                                          <p>To request interpreter or speech-to-text services,please complete this form linked
                                             below<br>
                                             <br>
                                             
                                          </p>
                                          <button>Request</button>
                                          </a></div>
                                    
                                    <div><a href="http://net4.valenciacollege.edu/forms/osd/sub_request.cfm"> <i aria-hidden="true"></i>
                                          
                                          <h3>Sub Request</h3>
                                          
                                          <p>To request a sub, please complete this form<br>
                                             <br>
                                             
                                          </p>
                                          <button>Request</button>
                                          </a></div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <h3>Interpreter/Speech to Text Request Details</h3>
                                 
                                 
                                 <p>Please make your request at least 3 business days in advance. If you make your request
                                    less than3 business days in advance we will do our best, but cannot guarantee that
                                    a service provider will be available. Making a request on Friday afternoon for Monday
                                    morning does not constitute 72 business hours notice. The Office for Students with
                                    Disabilities closes at 5:00 PM on Friday and opens again at 8:00 AM on Monday.
                                 </p>
                                 
                                 
                                 <p>Please note: If you are a current or prospective employee of Valencia College, please
                                    make all requests for interpreter/speech to text services to our Human Resources department.
                                 </p>
                                 
                                 <h3>To request a service provider via e-mail</h3>
                                 
                                 <ul>
                                    
                                    <li>Please complete the <a href="http://net4.valenciacollege.edu/forms/osd/interpreter_request.cfm">Interpreter Request Form</a>. All fields are required.
                                    </li>
                                    
                                    <li>Make sure to fill in the beginning time and the ending time of the event. </li>
                                    
                                    <li>If you are requesting an interpreter for a meeting, please provide details on what
                                       you will be meeting about, so that an appropriate interpreter can be scheduled.
                                    </li>
                                    
                                 </ul>
                                 
                                 <p>
                                    You will receive an e-mail with confirmation that your request has been received and
                                    if possible the names of your service provider(s).
                                 </p>
                                 
                                 
                                 <p>If you have any questions, please the Office for Students with Disabilities.</p>
                                 
                                 <div data-old-tag="table">
                                    
                                    <div data-old-tag="tbody">
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">Info</div>
                                          
                                          <div data-old-tag="th">East Campus</div>
                                          
                                          <div data-old-tag="th">Lake Nona Campus</div>
                                          
                                          <div data-old-tag="th">West Campus</div>
                                          
                                          <div data-old-tag="th">Osceola Campus</div>
                                          
                                          <div data-old-tag="th">Winter Park Campus</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">Room</div>
                                          
                                          <div data-old-tag="td">5-216</div>
                                          
                                          
                                          <div data-old-tag="td">SSB-102</div>
                                          
                                          <div data-old-tag="td">2-102</div>
                                          
                                          <div data-old-tag="td">1-212</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">Email</div>
                                          
                                          <div data-old-tag="td">
                                             <i aria-hidden="true"></i><a href="mailto:osdeast@valenciacollege.edu">OSD East</a>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <i aria-hidden="true"></i><a href="mailto:osdlnc@valenciacollege.edu">OSD Lake Nona</a>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <i aria-hidden="true"></i><a href="mailto:osdwest@valenciacollege.edu">OSD West</a>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <i aria-hidden="true"></i><a href="mailto:osdosc@valenciacollege.edu">OSD Osceola</a>
                                             
                                          </div>
                                          
                                          <div data-old-tag="td">
                                             <i aria-hidden="true"></i><a href="mailto:osdwpc@valenciacollege.edu">OSD Winter Park</a>
                                             
                                          </div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">Phone</div>
                                          
                                          <div data-old-tag="td">407-582-2229</div>
                                          
                                          <div data-old-tag="td">407-582-2229</div>
                                          
                                          <div data-old-tag="td">407-582-1523</div>
                                          
                                          <div data-old-tag="td">407-582-4167</div>
                                          
                                          <div data-old-tag="td">407-582-6887</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">Fax</div>
                                          
                                          <div data-old-tag="td">407-582-8908</div>
                                          
                                          <div data-old-tag="td">407-582-8908</div>
                                          
                                          <div data-old-tag="td">407-582-1326</div>
                                          
                                          <div data-old-tag="td">407-582-4804</div>
                                          
                                          <div data-old-tag="td">407-582-6841</div>
                                          
                                       </div>
                                       
                                       <div data-old-tag="tr">
                                          
                                          <div data-old-tag="th">TTY</div>
                                          
                                          <div data-old-tag="td">407-582-1222</div>
                                          
                                          <div data-old-tag="td">407-582-1222</div>
                                          
                                          <div data-old-tag="td">407-582-1222</div>
                                          
                                          <div data-old-tag="td">407-582-1222</div>
                                          
                                          <div data-old-tag="td">407-582-1222</div>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </main>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/deaf-hard-of-hearing-services.pcf">©</a>
      </div>
   </body>
</html>