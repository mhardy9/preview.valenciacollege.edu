<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/future-students.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <main>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>Future Students</h2>
                                       
                                       <h3>Transitioning to College</h3>
                                       
                                       <p>Welcome to Valencia College Office for Students with Disabilities (OSD)! In addition
                                          to the general challenges all students entering college face, students with disabilities
                                          transitioning to college need to be aware of changes in their rights and responsibilities
                                          and the procedures to receive disability services. 
                                       </p>
                                       
                                       <h3>What are some things you need to consider?</h3>
                                       
                                       <p>Diploma Options: Typically a standard diploma is required in order to be a degree
                                          seeking student at Valencia, although there may be some provisional status for a student
                                          without a standard diploma. OSD speaks to prospective students as well as current
                                          students. Call us early with questions. High School students are encouraged to plan
                                          ahead. 
                                       </p>
                                       
                                       <p>Documentation of a Disability: In order to establish eligibility for accommodations,
                                          a student must provide current documentation of a disability that significantly impacts
                                          him/her in a major life function, such as learning, reading, seeing, hearing, walking,
                                          etc. In most cases the Individualized Education Plan is not sufficient documentation;
                                          OSD may require current evidence or testing from a qualified diagnostician. 
                                       </p>
                                       
                                       <p>For more information about what students with disabilities need to know about transitioning
                                          to college, review the following information: <a href="http://www2.ed.gov/about/offices/list/ocr/transition.html">Students with Disabilities Preparing for Postsecondary Education: Know your rights
                                             and Responsibilities</a></p>
                                       
                                       <h3>Eligibility</h3>
                                       
                                       <p>Students seeking accommodations are required to provide recent documentation from
                                          an appropriate health care provider or professional.  If documentation is incomplete,
                                          or otherwise inadequate to determine the disability and/or reasonable accommodations,
                                          the Office for Students with Disabilities may require additional documentation. 
                                       </p>
                                       
                                       <p>To be eligible for disability-related services, students must have a documented disability
                                          as defined by the Federal Rehabilitation Act of 1973, Section 504, and/or the Americans
                                          with Disabilities Act of 1990/2008. A person with a disability is an individual with
                                          a physical or mental impairment that substantially limits one or more major life activities
                                          (i.e. seeing, walking, talking); or has a record of such an impairment; or is regarded
                                          as having such an impairment.
                                       </p>
                                       
                                       <p>This definition includes, but is not limited to</p>
                                       
                                       <ul>
                                          
                                          <li>Persons with mobility impairments </li>
                                          
                                          <li>Persons with learning disabilities</li>
                                          
                                          <li>Persons who are deaf or hard of hearing </li>
                                          
                                          <li>Persons who are blind or have low vision </li>
                                          
                                          <li>Persons with one or many serious contagious and non-contagious <br>
                                             diseases, including AIDS, epilepsy, cancer and tuberculosis
                                          </li>
                                          
                                          <li>Persons with psychological disorders </li>
                                          
                                       </ul>
                                       
                                       <h3>Self-Identification </h3>
                                       
                                       <p>The first step in the eligibility process is to submit appropriate documentation of
                                          disability to the Office for Students with Disabilities (OSD). The College must ensure
                                          that the student is  a person with a disability and that we can provide a rationale
                                          for reasonable accommodations. Your OSD advisor will meet with you to discuss how
                                          your disability impacts your learning and how we can assist. Typically, documentation
                                          is also required. 
                                       </p>
                                       
                                       <h3>OSD Registration Process </h3>
                                       
                                       <p>In order to request accommodations from the College, the student must complete the
                                          following steps in the Office for Students with Disabilities registration process:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li>Meet with the OSD advisor, by appointment or by walk in, to discuss your situation.
                                             In most cases it is recommended that you send documentation ahead of time so we can
                                             review it. Be sure to explain to your advisor how your disability impacts you and
                                             what accommodations you are requesting. 
                                          </li>
                                          
                                          <li>Provide appropriate documentation of the disability for which accommodations are being
                                             requested.
                                          </li>
                                          
                                          <li>Complete the self assessment (available at your first meeting) and other paperwork
                                             as needed. 
                                          </li>
                                          
                                       </ul>
                                       
                                       <p>Until all steps of the registration process have been completed, the student will
                                          be considered pending with OSD and will not be eligible for services or accommodations.
                                          Incomplete files will be kept for one term and then destroyed. The student may, however,
                                          begin the process again at any time.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                              <div>
                                 
                                 <h2>Frequently Asked Questions</h2>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>I have an IEP from my high school. Isn't that enough to get me services at the OSD?</h3>
                                       
                                       <p>Typically it is not sufficient documentation. Depending on the documentation in that
                                          IEP, we may or may not be able to provide services based on the IEP. Colleges may
                                          set reasonable documentation requirements, specifically if the disability does not
                                          present an obvious impact. Please review the following information: <a href="http://www2.ed.gov/about/offices/list/ocr/transition.html">Students with Disabilities Preparing for Postsecondary Education: Know your rights
                                             and Responsibilities</a></p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>What documentation do I need to provide?</h3>
                                       
                                       <p>Depending on the disability, the documentation requirements vary.  Please review the
                                          <a href="documentation.html">Valencia College Documentation Guidelines</a>. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>If there is a cost to secure this documentation, who pays for it?</h3>
                                       
                                       <p>The cost is considered to be part of the cost of going to college and in fact, can
                                          be counted in the FAFSA application as a college related cost. Thus, the student is
                                          responsible for providing OSD with the appropriate documentation. Valencia does provide
                                          to students a counseling service through Bay Care. This may be a resource for you
                                          to obtain documentation, depending on the disability. <a href="http://www.rehabworks.org">Vocational Rehabilitation </a> may also be another resource to obtain the required documentation. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Must I go through the OSD process? Can't I just talk to my professors individually
                                          for accommodations? 
                                       </h3>
                                       
                                       <p>Professors at Valencia are well informed that they are not required to provide any
                                          accommodations unless the student registers with and provides appropriate Letters
                                          of Accommodation from the OSD. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>I want to try it without accommodations first but if I have problems, can't I just
                                          come back and get help from OSD? 
                                       </h3>
                                       
                                       <p>Of course, that is the student's option.  However, understand that we can't go back
                                          and re-do any grades, assignments, etc. Often also it takes about a week or more to
                                          set up the services. Plan ahead.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>What are my options if I do not have a standard high school diploma?</h3>
                                       
                                       <p>If a student does not have a standard high school diploma, we highly recommend that
                                          the student consider returning to high school, if that option is open to him/her or
                                          consider taking the GED. There are schools in the area that provide GED Prep courses
                                          such as Orange Technical College and Seminole State College.   Students with special
                                          diploma may be eligible to take certain courses as a provisional student or personal
                                          interest on an audit basis here at Valencia but they cannot be degree seeking or eligible
                                          for financial aid.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>What kind of accommodations does the OSD provide?</h3>
                                       
                                       <p>Under the federal laws and regulations, we provide modifications, academic adjustments,
                                          and auxiliary aids based upon the disabling condition and the documentation provided
                                          to us. We cannot provide accommodations that alter the essential functions of a program.
                                          We can, however, review the documentation and speak with the student about what types
                                          of accommodations have helped in the past and are supported by the disability documentation.
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Can I come to OSD for Academic Advising or other Valencia related needs?</h3>
                                       
                                       <p>Absolutely! We encourage students to register with OSD and utilize the advising services.
                                          While we cannot assist with all processes, we may be able to direct you to the appropriate
                                          help.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>I signed the FERPA form for my parents. Does these mean they can register me in class
                                          and with the OSD?
                                       </h3>
                                       
                                       <p>While we welcome parents and their assistance in the student's success, we cannot
                                          conduct business on the student's behalf with any third party. Thus as the college
                                          student, you must participate in the process. We can share certain information with
                                          them but they cannot conduct business on your behalf. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>I'm a client of Vocational Rehabilitation and need some help getting through their
                                          process. Can you all help me? 
                                       </h3>
                                       
                                       <p>OSD will assist you in faxing the required information to your VR counselor; understand
                                          that some of the required information is available through your ATLAS account. If
                                          you have complaints about VR or need assistance with the process you can contact:
                                       </p>
                                       
                                       <ul>
                                          
                                          <li> <i aria-hidden="true"></i> 866-515-3692
                                          </li>
                                          
                                          <li>
                                             <i aria-hidden="true"></i> <a href="mailto:ombudsman@vr.fldoe.org">ombudsman@vr.fldoe.org</a>
                                             
                                          </li>
                                          
                                          <li>
                                             <i aria-hidden="true"></i> <a href="http://www.rehabworks.org/ombudsman.shtml">Website</a>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Will my transcripts reveal that I am registered with the OSD or otherwise indicate
                                          my disability? 
                                       </h3>
                                       
                                       <p>No!</p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>What do I do if my professor doesn't give me the approved accommodations on the letter
                                          from the OSD? 
                                       </h3>
                                       
                                       <p>Be sure that the professor received your OSD Letter of Accommodation and then immediately
                                          notify our OSD advisor or the Director. OSD will contact the professor and resolve
                                          the problem. It is the student's responsibility to inform OSD. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Can I get a tutor? </h3>
                                       
                                       <p>Each campus has onsite learning support centers and there are tutors available online
                                          through your ATLAS account. The OSD doesn't provide tutoring services; if you need
                                          accommodations to access tutoring services, please inform your OSD advisor. All students
                                          can utilize the tutoring centers; there is no additional cost for tutoring services.
                                          
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           
                        </main>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/future-students.pcf">©</a>
      </div>
   </body>
</html>