<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <div>
                           
                           <h2>Staff</h2>
                           
                           <h3>College Wide</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Deborah Larew - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Dr. Deborah Larew</h4>
                                       
                                       <p><i aria-hidden="true"></i>  Director<br>
                                          <i aria-hidden="true"></i>  Directs Disability Services college wide.<br>
                                          <i aria-hidden="true"></i>  407-582-2236<br>
                                          <i aria-hidden="true"></i>  <a href="mailto:dlarew@valenciacollege.edu">dlarew@valenciacollege.edu </a></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <figure><img alt="Mary Asbury - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Mary Asbury</h4>
                                       
                                       <p><i aria-hidden="true"></i>  Interim Assistant Director<br>
                                          <i aria-hidden="true"></i>  Manages Deaf and Hard of Hearing  Services college wide.<br>
                                          <i aria-hidden="true"></i>  407-582-2564<br>
                                          <i aria-hidden="true"></i>  <a href="mailto:masbury@valenciacollege.edu">masbury@valenciacollege.edu</a></p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <figure><img alt="Leah Anton - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Leah Anton</h4>
                                       
                                       <p><i aria-hidden="true"></i>  Staff Assistant II<br>
                                          <i aria-hidden="true"></i>  Provides administrative assistance to the Director.<br>
                                          <i aria-hidden="true"></i>  407-582-2934<br>
                                          <i aria-hidden="true"></i>  <a href="mailto:lanton@valenciacollege.edu">lanton@valenciacollege.edu</a></p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Donna Kimmeth - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Donna Kimmeth</h4>
                                       
                                       <p><i aria-hidden="true"></i>  Coordinator Deaf/HH Services <br>
                                          <i aria-hidden="true"></i>  Provides Cprint services and Service Provider scheduling<br>
                                          <i aria-hidden="true"></i>  407-582-2177<br>
                                          <i aria-hidden="true"></i>  <a href="mailto:dkimmeth@valenciacollege.edu">dkimmeth@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <figure><img alt="Robert Stefanov - No Photo Avatar" src="no-photo-male-thumb.png"></figure>
                                       
                                       <h4>Robert Stefanov</h4>
                                       
                                       <p><i aria-hidden="true"></i>  Assistive Technology Specialist<br>
                                          <i aria-hidden="true"></i>  407-582-1005<br>
                                          <i aria-hidden="true"></i>  <a href="mailto:rstefanov@valenciacollege.edu">rstefanov@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <hr>
                           
                           <h3>East Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Mary Asbury - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Mary Asbury</h4>
                                       
                                       <p><i aria-hidden="true"></i>  Coordinator - Disability Services<br>
                                          <i aria-hidden="true"></i>  407-582-2564<br>
                                          <i aria-hidden="true"></i>  <a href="mailto:masbury@valenciacollege.edu">masbury@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <figure><img alt="Chris Cuevas - No Photo Avatar" src="no-photo-male-thumb.png"></figure>
                                       
                                       <h4>Chris Cuevas</h4>
                                       
                                       <p><i aria-hidden="true"></i> Academic Advisor<br>
                                          <i aria-hidden="true"></i>  407-582-2308<br>
                                          <i aria-hidden="true"></i> <a href="mailto:ccuevas14@valenciacollege.edu">ccuevas14@valenciacollege.edu</a></p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Brandon Suggs - No Photo Avatar" src="no-photo-male-thumb.png"></figure>
                                       
                                       <h4>Brandon Suggs</h4>
                                       
                                       <p><i aria-hidden="true"></i> Staff Assistant I <br>
                                          <i aria-hidden="true"></i> Provides office support for the East Campus office.<br>
                                          <i aria-hidden="true"></i>  407-582-2229<br>
                                          <i aria-hidden="true"></i> <a href="mailto:bsuggs3@valenciacollege.edu">bsuggs3@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <hr>
                           
                           <h3>Osceola Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Nikkia Gumbs - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Nikkia Gumbs</h4>
                                       
                                       <p><i aria-hidden="true"></i> Coordinator<br>
                                          <i aria-hidden="true"></i> Manages Osceola Campus office.<br>
                                          <i aria-hidden="true"></i>  407-582-4094<br>
                                          <i aria-hidden="true"></i> <a href="mailto:ngumbs@valenciacollege.edu">ngumbs@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <figure><img alt="Brittany Bowers - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Brittany Bowers</h4>
                                       
                                       <p><i aria-hidden="true"></i> Academic Advisor<br>
                                          <i aria-hidden="true"></i>  407-582-4365<br>
                                          <i aria-hidden="true"></i> <a href="mailto:bbowers8@valenciacollege.edu">bbowers8@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Brionna Nunn - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Brionna Nunn</h4>
                                       
                                       <p><i aria-hidden="true"></i> Staff Assistant I<br>
                                          <i aria-hidden="true"></i> Provides office support for the Osceola Campus office.<br>
                                          <i aria-hidden="true"></i>  407-582-4167<br>
                                          <i aria-hidden="true"></i> <a href="mailto:bnunn@valenciacollege.edu">bnunn@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <hr>
                           
                           <h3>West Campus</h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Lyndsey Durning - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Lyndsey Durning</h4>
                                       
                                       <p><i aria-hidden="true"></i> Coordinator<br>
                                          <i aria-hidden="true"></i> Manages West Campus office<br>
                                          <i aria-hidden="true"></i>  407-582-1539<br>
                                          <i aria-hidden="true"></i> <a href="mailto:ldurning1@valenciacollege.edu">ldurning1@valenciacollege.edu</a><br>
                                          <i aria-hidden="true"></i> Location 
                                       </p>
                                       
                                    </li>
                                    
                                    <li>
                                       
                                       <figure><img alt="Robyn Borders - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Robyn Borders </h4>
                                       
                                       <p><i aria-hidden="true"></i> Academic Advisor<br>
                                          <i aria-hidden="true"></i>  407-582-153<br>
                                          <i aria-hidden="true"></i> <a href="mailto:rborders3@valenciacollege.edu">rborders3@valenciacollege.edu</a><br>
                                          <i aria-hidden="true"></i> Location 
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>
                                       
                                       <figure><img alt="Cecilia Guzman - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                       
                                       <h4>Cecilia Guzman</h4>
                                       
                                       <p><i aria-hidden="true"></i> Staff Assistant I<br>
                                          <i aria-hidden="true"></i> Provides office support for the West Campus office.<br>
                                          <i aria-hidden="true"></i>  407-582-1523<br>
                                          <i aria-hidden="true"></i> <a href="mailto:cdalmau@valenciacollege.edu">cdalmau@valenciacollege.edu</a> 
                                       </p>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <hr>
                           
                           <h3>Winter Park Campus</h3>
                           
                           <div>
                              
                              <ul>
                                 
                                 <li>
                                    
                                    <figure><img alt="Linda Firmani - No Photo Avatar" src="no-photo-female-thumb.png"></figure>
                                    
                                    <h4>Linda Firmani</h4>
                                    
                                    <p><i aria-hidden="true"></i> Counselor<br>
                                       <i aria-hidden="true"></i> Advises AA and AS degree-seeking, Technical Certificate seeking, transient, and undecided
                                       students.<br>
                                       <i aria-hidden="true"></i>  407-582-6887<br>
                                       <i aria-hidden="true"></i> <a href="mailto:lfirmani@valenciacollege.edu">emalfirmaniil@valenciacollege.edu</a><br>
                                       <i aria-hidden="true"></i> Location 
                                    </p>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/staff.pcf">©</a>
      </div>
   </body>
</html>