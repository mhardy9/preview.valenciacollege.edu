<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/documentation.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <main>
                           
                           
                           <div>
                              
                              <h2>Documentation Guidelines</h2>
                              
                              <p>In order for the OSD to accommodate students at Valencia College, students MUST register
                                 with the OSD. An Intake Form must be completed by the student AND appropriate documentation
                                 must be submitted by a doctor and/or a licensed professional to verify the presence
                                 and impact of their disability. 
                              </p>
                              
                              <p>Appropriate clinical documentation should substantiate  the disability and present
                                 evidence to establish a rationale supporting  the need for accommodations. <strong>A school plan such as an Individualized  Education Program (IEP) or a 504 plan is
                                    insufficient documentation  in and of itself but can be included as part of a more
                                    comprehensive evaluative report.</strong> If the requested accommodations are  not clearly identified in the diagnostic report,
                                 the OSD will seek clarification and, if necessary,  more information. The OSD will
                                 make final determination of whether appropriate and reasonable accommodations  are
                                 warranted and can be provided to the individual.
                              </p>
                              
                              <p>For conditions that are subject to change over  time (including health related disorders
                                 or psychological disabilities) the student may be asked to provide  updated documentation
                                 for his/her file on an on-going basis in order  for accommodations to be continued.
                                 
                              </p>
                              
                              <p>OSD forms are available on all  four campuses in the OSD office</p>
                              
                              
                              <p><a href="http://www2.ed.gov/about/offices/list/ocr/transitionguide.html">U.S. Department of Education: Office of Civil Rights</a></p>
                              
                              
                              <h3>Documentation Requirements</h3>              
                              
                              <p>The Office for Students with Disabilities will follow a timeline that allows for one
                                 week (from the date we receive the report) to review the report and/or diagnostic
                                 evaluation: after which, we will schedule an appointment to meet with the student
                                 and discuss the request and appropriate accommodations (if necessary).
                              </p>
                              
                              
                              <p>The Office for Students with Disabilities on every campus uses the same documentation
                                 guidelines. Please see the table below for contact information for your campus.
                              </p>
                              
                              <div data-old-tag="table">
                                 
                                 <div data-old-tag="tbody">
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">Info</div>
                                       
                                       <div data-old-tag="th">East Campus</div>
                                       
                                       <div data-old-tag="th">Lake Nona Campus</div>
                                       
                                       <div data-old-tag="th">West Campus</div>
                                       
                                       <div data-old-tag="th">Osceola Campus</div>
                                       
                                       <div data-old-tag="th">Winter Park Campus</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">Room</div>
                                       
                                       <div data-old-tag="td">5-216</div>
                                       
                                       
                                       <div data-old-tag="td">SSB-102</div>
                                       
                                       <div data-old-tag="td">2-102</div>
                                       
                                       <div data-old-tag="td">1-212</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">Email</div>
                                       
                                       <div data-old-tag="td">
                                          <i aria-hidden="true"></i> <a href="mailto:osdeast@valenciacollege.edu">OSD East</a>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <i aria-hidden="true"></i> <a href="mailto:osdlnc@valenciacollege.edu">OSD Lake Nona</a>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <i aria-hidden="true"></i> <a href="mailto:osdwest@valenciacollege.edu">OSD West</a>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <i aria-hidden="true"></i> <a href="mailto:osdosc@valenciacollege.edu">OSD Osceola</a>
                                          
                                       </div>
                                       
                                       <div data-old-tag="td">
                                          <i aria-hidden="true"></i> <a href="mailto:osdwpc@valenciacollege.edu">OSD Winter Park</a>
                                          
                                       </div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">Phone</div>
                                       
                                       <div data-old-tag="td">407-582-2229</div>
                                       
                                       <div data-old-tag="td">407-582-2229</div>
                                       
                                       <div data-old-tag="td">407-582-1523</div>
                                       
                                       <div data-old-tag="td">407-582-4167</div>
                                       
                                       <div data-old-tag="td">407-582-6887</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">Fax</div>
                                       
                                       <div data-old-tag="td">407-582-8908</div>
                                       
                                       <div data-old-tag="td">407-582-8908</div>
                                       
                                       <div data-old-tag="td">407-582-1326</div>
                                       
                                       <div data-old-tag="td">407-582-4804</div>
                                       
                                       <div data-old-tag="td">407-582-6841</div>
                                       
                                    </div>
                                    
                                    <div data-old-tag="tr">
                                       
                                       <div data-old-tag="th">TTY</div>
                                       
                                       <div data-old-tag="td">407-582-1222</div>
                                       
                                       <div data-old-tag="td">407-582-1222</div>
                                       
                                       <div data-old-tag="td">407-582-1222</div>
                                       
                                       <div data-old-tag="td">407-582-1222</div>
                                       
                                       <div data-old-tag="td">407-582-1222</div>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              <h3>Disability Documentation Guidelines</h3>
                              
                              <p>In order to provide reasonable, effective and appropriate academic accommodations
                                 to students at Valencia College who have disabilities, The Office for Students with
                                 Disabilities (OSD) requires students to provide recent, relevant and comprehensive
                                 medical documentation of the disability and the disability's impact on the student's
                                 participation in a course, program, or activity.
                              </p>
                              
                              
                              <blockquote>"A reasonable accommodation is a modification or adjustment to a course, program,
                                 service, or activity that enables a qualified student with a disability to obtain
                                 equal access. Equal access means an opportunity to attain the same level of performance
                                 or to enjoy equal benefits and privileges as are available to a similarly situated
                                 student without a disability. Only the peripheral requirements of courses, programs,
                                 and activities are subject to modification; essential elements of courses, programs,
                                 and activities must remain intact."<br>~ Section 504 of the Vocational Rehabilitation Act &amp; the Americans with Disabilities
                                 Act (ADA)
                              </blockquote>
                              
                              
                              <p>The student will also have the opportunity to discuss the impact of the disability
                                 on his or her academic performance, as well as discuss what accommodation has worked
                                 and what has not been effective.
                              </p>
                              
                              
                              <p>Therefore, the documentation provided by the student must include the following information:</p>
                              
                              <h3>Documentation from Professionals</h3>
                              
                              <ul>
                                 
                                 <li>Documentation reflects a specific diagnosis or condition and the current functional
                                    limitations or academic barriers presented by the disability; i.e., how does the disability
                                    create a barrier for the student academically?
                                 </li>
                                 
                                 <li>Include a DSM-IV code where appropriate.</li>
                                 
                                 <li>If medications are taken, identify the side effects that impact academic performance.</li>
                                 
                                 <li>Typed letters on official letterhead, psycho-educational evaluation reports (with
                                    scores), or neuro-psychological evaluation reports (with scores) are preferred. Letters
                                    must be signed by an evaluator qualified to make the diagnosis, and include information
                                    about license or certification, background and area of specialization.
                                 </li>
                                 
                                 <li>Documentation must be dated and signed by the physician or evaluator.</li>
                                 
                              </ul>
                              
                              <h3>Incomplete or Insufficient Documentation of Disability</h3>
                              
                              <ul>
                                 
                                 <li>Hand written notes on prescription (Rx) pads are not sufficient.</li>
                                 
                                 <li>Individualized Educational Plans (IEP) and 504 Plans, although providing historical
                                    evidence of services and accommodations, are generally not considered sufficient documentation,
                                    unless they contain required information. However, may be used as a supplement to
                                    more current documentation.
                                 </li>
                                 
                                 <li>Documentation written by family members is not sufficient.</li>
                                 
                              </ul>
                              
                              <h3>Guidelines for documenting disability by category</h3>
                              
                              <div>
                                 
                                 <h4>Attention Deficit/Hyperactive Disorder (ADHD) or Attention Deficit Disorder (ADD)</h4>
                                 
                                 <ul>
                                    
                                    <li>Summary of assessment procedures and evaluation instruments used to determine the
                                       diagnosis. A summary from a physician who has been treating the student for ADHD is
                                       also acceptable.
                                    </li>
                                    
                                    <li>Information regarding medications prescribed and possible side effects that may impact
                                       academic performance.
                                    </li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the ADHD or
                                       ADD in the academic environment is crucial; i.e. "How does ADHD or ADD impair the
                                       student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Autism Spectrum Disorder</h4>
                                 
                                 <ul>
                                    
                                    <li>Summary of assessment procedures and evaluation instruments used to determine the
                                       diagnosis. A summary from a professional practitioner who has been treating the student
                                       is also acceptable.
                                    </li>
                                    
                                    <li>Information regarding medications prescribed and possible side effects that may impact
                                       academic performance.
                                    </li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to Asperger's Syndrome
                                       or Autism Spectrum Disorder in the academic environment is crucial; i.e. "How does
                                       the Asperger's or Autism Spectrum disorder impair the student's ability to learn?"
                                    </li>
                                    
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 <h4>Blind or Low Vision</h4>
                                 
                                 
                                 <ul>
                                    
                                    <li>Letter or report from an ophthalmologist or optometrist.</li>
                                    
                                    <li>Letter or documentation from an agency specializing is working with and assisting
                                       individual who are blind or have low vision, i.e. Division of Blind Services. Information
                                       regarding functional limitations or barriers connected to the student's vision loss
                                       in the academic environment is crucial; i.e. "How does vision loss or blindness impair
                                       the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                    <li>A visual impairment is defined by the State of Florida as disorders in the structure
                                       and function of the eye as manifested by at least one of the following: visual acuity
                                       of 20/70 or less in the better eye after the best possible correction, a peripheral
                                       field so constricted that it affects one's ability to function in an educational setting,
                                       or a progressive loss of vision which may affect one's ability to function in an educational
                                       setting. Examples include, but are not limited to, cataracts, glaucoma, nystagmus,
                                       retinal detachment, retinitis pigmentosa, and strabismus.
                                    </li>
                                    
                                 </ul>
                                 
                                 <h4>Deaf or Hard of Hearing</h4>
                                 
                                 <ul>
                                    
                                    <li>Letter or report from an audiologist or otolaryngologist.</li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       hearing loss in the academic environment is crucial; i.e. "How does the deafness or
                                       loss of hearing impair the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program (sign language interpreter, real time captioning, note takers, etc.).
                                    </li>
                                    
                                    <li>A hearing loss is defined by the State of Florida as a loss of thirty (30) decibels
                                       or greater, pure tone average of 500, 1000, and 4000 (Hz), unaided, in the better
                                       ear. Examples include, but are not limited to, conductive hearing impairment or deafness,
                                       sensorineural hearing impairment or deafness, and high or low tone hearing loss or
                                       deafness, and acoustic trauma hearing loss or deafness.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Orthopedic</h4>
                                 
                                 
                                 <ul>
                                    
                                    <li>Letter from a physician qualified to diagnose and treat the condition.</li>
                                    
                                    <li>Identifying the specific orthopedic condition is preferred.</li>
                                    
                                    <li>Information about side effects of medications prescribed for treatment.</li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       medical disability in the academic environment is crucial; i.e. "How does the disability
                                       impair the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Other Health Disabilities</h4>
                                 
                                 <ul>
                                    
                                    <li>Letter from a physician qualified to diagnose and treat the condition.</li>
                                    
                                    <li>Identifying the specific medical condition is preferred.</li>
                                    
                                    <li>Information about side effects of medications prescribed for the treatment.</li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       medical disability in the academic environment is crucial; i.e. "How does the disability
                                       impair the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Psychological/Emotional/Behavioral</h4>
                                 
                                 <ul>
                                    
                                    <li>Letter from a physician, psychologist, psychiatrist, licensed social worker, or licensed
                                       mental health counselor, qualified to diagnose and treat the condition.
                                    </li>
                                    
                                    <li>Identifying the specific psychological/emotional/behavioral disability is preferred.</li>
                                    
                                    <li>Information about side effects of medications prescribed for treatment.</li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       psychological/emotional/behavioral disability in the academic environment is crucial;
                                       i.e. "How does the disability impair the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 <h4>Specific Learning Disability</h4>
                                 
                                 <ul>
                                    
                                    <li>Psycho-educational evaluation or neuro-psychological evaluation.</li>
                                    
                                    <li>Evaluations based on adult norms are preferred.</li>
                                    
                                    <li>If evaluations are more than 5 years old or based on children's norms, an addendum
                                       may be requested. This to confirm academic barriers are still present and/or if additional
                                       academic barriers are presented.
                                    </li>
                                    
                                    <li>IQ evaluation narrative, scores, and sub-test scores are helpful in determining reasonable
                                       accommodations. Note: Brief screening measurements are not sufficient
                                    </li>
                                    
                                    <li>Recommended IQ evaluation:
                                       
                                       <ul>
                                          
                                          <li>Weschler Adult Intelligence Scale (WAIS-IV)</li>
                                          
                                          <li>Reynolds (RAIS)</li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    <li>Academic Achievement evaluation narrative, scores, and sub-test scores are required
                                       in determining reasonable accommodations.
                                    </li>
                                    
                                    
                                    <li>Recommended Test for Achievement:
                                       
                                       <ul>
                                          
                                          <li>Woodcock-Johnson WJ-lll (Achievement Test)</li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    
                                    <li>Cognitive Processing narrative, scores, and sub-test scores are helpful in determining
                                       reasonable accommodations.
                                    </li>
                                    
                                    
                                    <li>Recommended Test for cognitive processing:
                                       
                                       <ul>
                                          
                                          <li>Woodcock-Johnson WJ-lll â€“ Cognitive Battery</li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       learning disability in the academic environment is crucial; i.e. "How does the specific
                                       learning disability impair the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Speech/Language</h4>
                                 
                                 <ul>
                                    
                                    <li>Letter from a physician or practitioner qualified to diagnose and treat the disorder.</li>
                                    
                                    <li>Identifying the specific speech/language disorder is preferred.</li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       speech/language disability in the academic environment is crucial; i.e. "How does
                                       the disability impair the student's ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                                 
                                 <h4>Traumatic Brain Injury</h4>
                                 
                                 <ul>
                                    
                                    <li>Psycho-educational evaluation or neuro-psychological evaluation is helpful in determining
                                       reasonable accommodations.
                                    </li>
                                    
                                    <li>IQ evaluation narrative, scores, and sub-test scores are helpful in determining reasonable
                                       accommodations.
                                    </li>
                                    
                                    <li>Academic Achievement evaluation narrative, scores, and sub-test scores are helpful
                                       in determining reasonable accommodations.
                                    </li>
                                    
                                    <li>Cognitive Processing narrative, scores, and sub-test scores are helpful in determining
                                       reasonable accommodations.
                                    </li>
                                    
                                    <li>Evaluations based on adult norms are preferred.</li>
                                    
                                    <li>If the above evaluations are not available, a letter from a physician or practitioner
                                       qualified to diagnose and treat a TBI.
                                    </li>
                                    
                                    <li>Information regarding functional limitations or barriers connected to the student's
                                       TBI in the academic environment is crucial; i.e. "How does the TBI impair the student's
                                       ability to learn?"
                                    </li>
                                    
                                    <li>Recommended reasonable accommodations that will provide effective access to the student's
                                       academic program.
                                    </li>
                                    
                                 </ul>
                                 
                              </div>  
                              
                           </div>
                           
                           
                           
                        </main>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/documentation.pcf">©</a>
      </div>
   </body>
</html>