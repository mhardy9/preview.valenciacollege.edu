<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/assistive-technology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <main>
                           
                           
                           <div>
                              
                              <h2>Assistive Technology Services</h2>
                              
                              <p>In order  to access course materials, some students may need to utilize assistive
                                 technology. Students seeking this accommodation must first provide documentation of
                                 a print related disability and be registered with the Office for Students with Disabilities.
                                 
                              </p>
                              
                              <p>Assessment of the student's eligibility, needs and recommendations for appropriate
                                 assistive technologies will include: 
                              </p>
                              
                              <ul>
                                 
                                 <li>A review of the student's documentation of disability by a qualified staff member
                                    in the Office for Students with Disabilities
                                 </li>
                                 
                                 <li>An interactive conversation between the student and the Office for Students with Disabilities.
                                    
                                 </li>
                                 
                              </ul>
                              
                              <p>Training in the use of assistive technology is available by appointment. To schedule
                                 a training session please contact the <a href="mailto:osd-at@valenciacollege.edu">Assistive Technology Services</a> team.
                              </p>
                              
                              <h3>Technology Available for Loan</h3>
                              
                              <p>Valencia offers the following assistive equipment collegewide for loan to eligible
                                 students on a first-come-first-served basis. If you are interested in a demonstration
                                 or would like one of the following for loan please contact the <a href="mailto:osd-at@valenciacollege.edu">Assistive Technology Services</a> team.
                              </p>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="http://www.freedomscientific.com/">ONYX Desktop Magnification Camera</a>by Freedom Scientific
                                    
                                    <ul>
                                       
                                       <li>A  camera system designed for viewing materials at a distance as well as magnifying
                                          documents such as reading materials and classroom assignments.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://comtek.com/">AT-216 Personal FM System</a> by Comtek
                                    
                                    <ul>
                                       
                                       <li> A personal FM system designed to amplify desired sounds and allow students to participate
                                          in everyday listening situations without strain.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 
                                 <li>
                                    <a href="http://www.livescribe.com/en-us/">Echo Smartpens</a> by Livescribe
                                    
                                    <ul>
                                       
                                       <li>Record and play back everything you've written and heard after having taken notes
                                          using this pen.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.livescribe.com/en-us/">Livescribe 3</a> by Livescribe
                                    
                                    <ul>
                                       
                                       <li>Using Bluetooth Smart wireless technology, easily pair notes you've written or recorded
                                          to your tablet or smartphone
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.abisee.com/ZoomTWIX.html">Zoom-Twix</a>by ABiSee
                                    
                                    
                                    <ul>
                                       
                                       <li>An instant digital scanner, built with OCR capability, and a zoomable long distance
                                          camera designed to capture what's on a blackboard, whiteboard, on your desk or in
                                          the room all at any magnification, all controlled from your laptop's keyboard.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="https://us.optelec.com/">Optelec Compact 4 HD</a> by Optelec
                                    
                                    
                                    <ul>
                                       
                                       <li>The first high-definition video magnifier that uniquely combines all the benefits
                                          of a hand-held and dome magnifier in a single design for a comfortable reading experience.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.freedomscientific.com/">Ruby XL HD</a> by Freedom Scientific
                                    
                                    <ul>
                                       
                                       <li>A  digital magnifier that pairs crystal clear, high-definition resolution with a lightweight
                                          portable design. 
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>Laptops
                                    
                                    <ul>
                                       
                                       <li> Used for remote speech-to-text services, such as C-Print or CART, in the classroom
                                          and for special events. 
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <h3>Technology Available for Everyone</h3>
                              
                              <p>The college offers the following assistive technology collegewide for use. If you
                                 are interested in a demonstration of any of our assistive technology please contact
                                 the <a href="mailto:osd-at@valenciacollege.edu">Assistive Technology Services</a> team. 
                              </p>
                              
                              <ul>
                                 
                                 <li>
                                    <a href="http://www.sorenson.com/">Sorenson ntouch VideoPhone</a> by Sorenson
                                    
                                    <ul>
                                       
                                       <li>Deaf individuals can make and receive Sorenson Video Relay Service (SVRS) calls, as
                                          well as make direct calls to other videophone users. Locations vary, please contact
                                          the OSD for further information.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.freedomscientific.com/">SARA Scanning and Reading Appliance</a> by Freedom Scientific
                                    
                                    <ul>
                                       
                                       <li>A stand-alone scanner which enables the reading of a wide variety of printed materials
                                          such as textbooks, handouts, and so much more.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>CCTV
                                    
                                    <ul>
                                       
                                       <li>A stationary magnification system designed to enlarge documents such as textbooks
                                          and handouts. Locations and models vary. Please contact the OSD for further information.
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>Computers with Assistive Technology
                                    
                                    <ul>
                                       
                                       <li>Valencia strives to provide all students with access and has equipped each lab with
                                          assistive technology to serve our diverse student population.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                              <h3>Assistive Software</h3>
                              
                              <p>The college offers the following assistive software collegewide. Some of the licenses
                                 are available on most open access computers, and some of the licenses are more limited.
                              </p>
                              
                              <ul>
                                 <li>
                                    <a href="http://www.freedomscientific.com/">JAWS Screen Reader Software</a> by Freedom Scientific
                                    
                                    <ul>
                                       
                                       <li>JAWS is designed to read aloud what is on the computer screen and to provide users
                                          with a unique set of navigational tools to allow access to all screen and web content.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.freedomscientific.com/">MAGic Screen Magnification Software</a> by Freedom Scientific
                                    
                                    <ul>
                                       
                                       <li>A screen magnification system with built-in enhancements to provide the user with
                                          access to all screen and web content.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.kurzweiledu.com/default.html">Kurzweil 3000 Software</a> by Kurzweil Educational Systems
                                    
                                    <ul>
                                       
                                       <li>A comprehensive software that supports struggling students by providing tools to support
                                          reading, writing, studying, and test taking.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.claroread.com/">Claro Read Software</a> by Claro Software
                                    
                                    <ul>
                                       
                                       <li>A multi-sensory software solution designed to support reading, writing and studying
                                          skills. This software is available to all Valencia Students at no cost. This software
                                          is also available for both the PC and the MAC. To have this software installed onto
                                          your computer please schedule an appointment with the Office for Students with Disabilities.
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                                 <li>
                                    <a href="http://www.nuance.com/index.htm">Dragon Naturally Speaking Software</a> by Nuance
                                    
                                    <ul>
                                       
                                       <li>A speech recognition software that allows students to express their ideas in writing
                                          more quickly and easily.
                                       </li>
                                       
                                    </ul>
                                    
                                 </li>
                                 
                              </ul>
                              
                              
                           </div>
                           
                           
                           
                        </main>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/assistive-technology.pcf">©</a>
      </div>
   </body>
</html>