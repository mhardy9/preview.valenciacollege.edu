<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/faculty-toolbox.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        
                        
                        
                        <main>
                           
                           <div>
                              
                              
                              <h2>Faculty Toolbox</h2>
                              
                              <div>
                                 
                                 <div> <a href="documents/OSD-faculty-guide.pdf"> <i aria-hidden="true"></i>
                                       
                                       <h3> Faculty Resource Guide </h3>
                                       <br><br>
                                       
                                       <p>A Desk Reference Guide For Faculty prepared by the Office for Students with Disabilities
                                          
                                       </p>
                                       </a>
                                    
                                 </div>
                                 
                                 <div>  <a href="http://salsa.usu.edu/"> <i aria-hidden="true"></i>
                                       
                                       <h3> Build an Accessible Syllabus </h3>
                                       
                                       <p>Enter your syllabus information in a template, and produce a fully accessible syllabus
                                          for online instruction.
                                       </p>
                                       </a> 
                                 </div>
                                 
                                 <div>  <a href="https://www.easytestmaker.com/"> <i aria-hidden="true"></i>
                                       
                                       <h3> Easy Test Maker </h3>
                                       <br><br>
                                       
                                       <p>Build tests with various question types and produce a PDF that OSD can use to complete
                                          a Testing Request. 
                                       </p>
                                       </a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div> 
                                    <h3>ADA Resources</h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.apa.org/pi/disability/dart/legal/ada-basics.aspx">ADA Basics</a></li>
                                       
                                       <li><a href="http://ds.oregonstate.edu/faculty-information-americans-disabilities-act">Faculty Information on the Americans with Disabilities Act</a></li>
                                       
                                       
                                       <li><a href="http://disability.tamu.edu/facultyguide/roles">Faculty Roles and Responsibilities</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <h3>Accessibility Tools</h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://wave.webaim.org/">Web Accessibility Evaluation Tool (WAVE) </a></li>
                                       
                                       <li><a href="documents/osd-creating-an-accessible-syllabus.docx">Create an accessible syllabus from scratch in Microsoft Word </a></li>
                                       
                                       <li><a href="documents/osd-accessible-syllabus-framework.docx">Pre-formatted syllabus template to produce an accessible document</a></li>
                                       
                                       <li><a href="http://webaim.org/articles/tools/">WebAIM</a></li>
                                       
                                       <li><a href="http://www.paciellogroup.com/">The Paciello Group</a></li>
                                       
                                       <li><a href="http://fae20.cita.illinois.edu/"> Functional Accessibility Evaluator </a></li>
                                       
                                       <li><a href="http://www.508checker.com/">508 Checker</a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <h3>Additional Resources</h3>
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.adobe.com/accessibility.html">Adobe Accessibility</a></li>
                                       
                                       <li><a href="https://www.apple.com/accessibility/">Apple Accessibility</a></li>
                                       
                                       
                                       <li><a href="http://www.blackboard.com/Platforms/Learn/Resources/Accessibility.aspx">Blackboard Accessibility</a></li>
                                       
                                       <li><a href="http://www.techsmith.com/camtasia.html">Camtasia Studio</a></li>
                                       
                                       
                                       <li><a href="http://p3.csun.edu/">CSUN National Center on Deafness</a></li>
                                       
                                       <li><a href="https://www.google.com/accessibility/">Google Accessibility</a></li>
                                       
                                       <li><a href="http://www.microsoft.com/enable/">Microsoft Accessibility</a></li>
                                       
                                       <li>
                                          <a href="http://www.pepnet.org/MAD-DHOH">Tips for Teaching Students Who are Deaf</a> 
                                       </li>
                                       
                                       <li><a href="http://www.pepnet.org/resources/tipsheetclassroom">Tips for using AV materials with visual learners </a></li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Accessible Course Materials</h3>
                                    
                                 </div>
                                 
                                 <h4>What to Know When Considering Materials</h4>
                                 
                                 <p>Selecting textbooks and online content is an important part of designing instruction.
                                    By reviewing the accessibility of materials before purchase, faculty are able to support
                                    equitable access and reduce the amount of adjustments that may need to be made to
                                    materials. There are some simple questions that you can ask of publishers to learn
                                    more about accessibility of their materials. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Are your books available in an accessible, electronic format for students with print
                                       disabilities? (Publishers should be able to tell you that they do provide books in
                                       accessible formats, such as PDF, HTML, or Daisy)
                                    </li>
                                    
                                    <li>Can you please send me the 508 Compliance statement for the materials we are requesting?
                                       (This statement clearly outlines what is and what isn't accessible. Feel free to contact
                                       the OSD on your campus for assistance with reading this statement.)
                                    </li>
                                    
                                    <li>Are all video materials in online support materials captioned or do they have a full
                                       transcript? If they have a full transcript, will you provide us with permissions to
                                       make the videos captioned using the transcript? (By using materials that are already
                                       captioned, you do not have to invest the time to manually caption materials.)
                                    </li>
                                    
                                    <li>Are all online lessons compatible with a screen reader, such as JAWS? (If all students,
                                       including those with visual impairments, can access the online materials, you will
                                       not have to supplement your lessons with alternative assignments)
                                    </li>
                                    
                                 </ul>            
                                 
                                 <p>These are questions to ask as your selection process is starting. Please remember
                                    that the OSD is available to you as a resource as you move forward in the important
                                    task of selecting class materials. 
                                 </p>
                                 
                                 
                                 <h4>Captioning Materials in YouTube</h4>
                                 
                                 <p>YouTube is a great resource for multi-media materials. However, please be sure that
                                    any videos you use in class or in an online course are correctly captioned. If you
                                    are using your own materials, you can use YouTube to assist with captioining. NOTE:
                                    Please do not use auto captions without editing content. 
                                 </p>
                                 
                                 <ul>
                                    
                                    <li>Set up an account. You can do this in Google or at Youtube.com.</li>
                                    
                                    <li>Upload a short video into your account, so that you can edit the material. </li>
                                    
                                    <li>Create a Transcript File
                                       
                                       <ul>
                                          
                                          <li>Listen to your video, and type what is said, using the formatting rules below.</li>
                                          
                                          <li>Type the text of what was said in your video and save it as a plain text file (.txt).
                                             You can convertother formats (like Microsoft Word, HTML, PDF) into a plain text file
                                             or you can use native programs on your computer like TextEdit or Notepad.
                                          </li>
                                          
                                          <li>In order to get the best results, use these formatting tips:
                                             
                                             <ul>
                                                
                                                <li>Use a blank line to force the start of a new caption.</li>
                                                
                                                <li>Use square brackets to designate background sounds. For example, [music] or [laughter].</li>
                                                
                                                <li>Add &amp;gt;&amp;gt; to identify speakers or change of speaker.</li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li>Transcribe and Set Timings
                                             
                                             <ul>
                                                
                                                <li>You can transcribe your video and automatically line up your text with the speech
                                                   in the video.
                                                </li>
                                                
                                                <li>A transcript contains the text of what is said in a video, but no time code information,
                                                   so you need to set the timing to sync with your video.
                                                </li>
                                                
                                                <li>Since the transcript text is automatically synchronized to your video, the transcript
                                                   must be in a language supported by our speech recognition technology and in the same
                                                   language that's spoken in the video. Transcripts are not recommended for videos that
                                                   are over an hour long or have poor audio quality.
                                                </li>
                                                
                                                <li>Choose the language for the subtitles or closed captions you want to create. You can
                                                   use the search bar to find languages that don't automatically show in the list.
                                                </li>
                                                
                                                <li>Select <strong>Create new subtitles or CC</strong>.
                                                </li>
                                                
                                                <li>Underneath the video, click <strong>Transcribe and set timings</strong>.
                                                </li>
                                                
                                                <li>Type all of the spoken audio in the text field. If you already have a script in word
                                                   or .txt, you can copy and paste it. 
                                                </li>
                                                
                                                <li>If you're creating closed captions, make sure to incorporate sound cues like[music]or[applause]to
                                                   identify background sounds.
                                                </li>
                                                
                                                <li>Click <strong>Set timings</strong>to sync your transcript with the video.
                                                </li>
                                                
                                                <li>Setting the timings can take a few minutes. While you wait, you'll be brought back
                                                   to the video tracklist.Once it'sready, your transcription will automatically be published
                                                   on your video.
                                                </li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li>Editing Your Captions
                                             
                                             <ul>
                                                
                                                <li>Go to your <a href="https://www.youtube.com/my_videos">Video Manager</a>.
                                                </li>
                                                
                                                <li>Next to the video you want to edit captions for, click <strong>Edit &amp;gt; Subtitles and CC</strong>.
                                                </li>
                                                
                                                <li>Click on the caption track you want to edit.</li>
                                                
                                                <li>Click inside any line in the caption track panel and edit the text.</li>
                                                
                                                <li>Click <strong>Save changes</strong>.
                                                </li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                       </ul>
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h3>Selecting Accessible Course Materials</h3>
                                       
                                    </div>
                                    
                                    <div> <span>1</span><span>Select textbook and/or electronic materials to be purchased</span> 
                                    </div>
                                    
                                    <div> <span>2</span><span>Contact publisher to request 508 Compliance Statement (VPAT) </span> 
                                    </div>
                                    
                                    <div> <span>3</span><span>Ask <a href="documents/accessibility-questions-for-publishers.pdf">these questions</a> regarding accessibility</span> 
                                    </div>
                                    
                                    <div> <span>4</span><span>Review VPAT statement, publisher answers and contact OSD with any questions</span> 
                                    </div>
                                    
                                    <div> <span>5</span><span>Contact publisher to determine if noted accessibility errors can be fixed.</span> 
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <div>
                              
                              <div>
                                 
                                 <h3>Just In Time Training</h3>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="Disability 101" src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/watch?v=lLDZnaKxFRI" title="Disability 101"> <i aria-hidden="true"></i>
                                                   
                                                   <p>Disability 101</p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>This video is designed to provide basic information about the Office for Students
                                          with Disabilities (OSD) and a broad overview of high incidence disabilities.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="ADA Basics" src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/watch?v=-KEaOlQt6PY" title="ADA Basics"> <i aria-hidden="true"></i>
                                                   
                                                   <p>ADA Basics</p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>This video is designed to provide basic information about the Office for Students
                                          with Disabilities (OSD) and a broad overview of high incidence disabilities.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="Testing Center Accommodation" src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/watch?v=glAEuBihEeU" title="Testing Center Accommodation"> <i aria-hidden="true"></i>
                                                   
                                                   <p>Testing Center Accommodation</p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>The Testing Center on each campus is able to provide accommodations to Valencia students.
                                          Accommodations are designed to allow students to demonstrate their mastery of course
                                          material.Click here to learn more about the Testing Center.
                                       </p>
                                       
                                    </div> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="Deaf &amp; Hard of Hearing Students" src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/watch?v=F6RLVcBmbRc" title="Deaf &amp; Hard of Hearing Students"> <i aria-hidden="true"></i>
                                                   
                                                   <p>Deaf &amp; Hard of Hearing Students</p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>Deaf students regularly attend Valencia college. This video will review strategies
                                          you can use in your classroom to best support students who are Deaf and hard of hearing.
                                          Interpreters, captioning, and assessment concerns will all be addressed.Deaf and Hard
                                          of Hearing Students 
                                       </p>
                                       
                                    </div> 
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="Note Taking" src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/embed/t48Tl6l3ehU" title="Note Taking"> <i aria-hidden="true"></i>
                                                   
                                                   <p>Note Taking</p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>Note taking is a common accommodation. Watch the video in this module to learn why
                                          note taking is so important to accessing your course.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="Accessible Texts &amp; Software " src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/watch?v=ui5UjK565gk" title="Accessible Texts &amp; Software "> <i aria-hidden="true"></i>
                                                   
                                                   <p>Accessible Texts &amp; Software </p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>This module will briefly cover what faculty should know when considering new educational
                                          materials, software, or textbooks.
                                       </p>
                                       
                                    </div> 
                                 </div>
                                 
                              </div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <figure> <img alt="Visual Impairments" src="0.jpg">
                                          
                                          <figcaption>
                                             
                                             <div> <a href="https://www.youtube.com/watch?v=4Vn-sFbXEjw" title="Visual Impairments"> <i aria-hidden="true"></i>
                                                   
                                                   <p>Visual Impairments</p>
                                                   </a> 
                                             </div>
                                             
                                          </figcaption>
                                          
                                       </figure>
                                       
                                       <p>The information here covers the varying types of vision impairment, the technology
                                          available to provide support, as well as instructional considerations to consider
                                          for your course. Learn how to support students with Visual Impairments. 
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <h3>Research &amp; Scholarly Articles</h3>
                                    
                                 </div>
                                 
                                 <div> <a href="http://moritzlaw.osu.edu/students/groups/oslj/files/2012/03/62.1.asch_.pdf">Critical Race Theory, Feminism, and Disability: Reflections on Social Justice and
                                       Personal Identity</a>
                                    
                                    <p>Since the passage of the Americans with Disabilities Act, those who fight for disability
                                       rights can acknowledge some progress in the situation of people with disabilities
                                       but can also recognize that the insights from critical race theory and feminism have
                                       lessons for the disability rights movement as well. This article considers the application
                                       of critical race theory and feminist theory to such topics as who should be able to
                                       use the anti-discrimination provisions of the ADA, how to evaluate the interaction
                                       of impairment with environment, differences among impairments and environments and
                                       their implications for inclusion of people with disabilities in society, the merits
                                       of integration as a goal, and disability-consciousness as part of personal identity.
                                    </p>
                                    
                                    <p> <strong>Asch, A. (2001). Critical Race Theory, Feminism, and Disability: Reflections on Social
                                          Justice and Personal Identity.&nbsp;</strong><em><strong>Ohio State Law Journal,</strong></em><strong>&nbsp;</strong><em><strong>62</strong></em><strong>(1), 391-423.</strong> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div> <a href="https://books.google.com/books?hl=en&amp;lr=&amp;id=Y3Y9BAAAQBAJ&amp;oi=fnd&amp;pg=PP1&amp;dq=universal+design+and+outcomes+and+higher+education&amp;ots=dHcceXdT4n&amp;sig=tQ8M0ZvXo0J7L8QXsTw5xAAQ-Ss#v=onepage&amp;q=universal%20design%20and%20outcomes%20and%20higher%20education&amp;f=false">Making Engagement Equitable for Students in U.S. Higher  Education </a>
                                    
                                    <p>As American higher education continues to become increasingly diverse, so too will
                                       the needs and challenges faced by our students. Perhaps planning and organizing was
                                       simpler when the overwhelming majority of students was white, male, heterosexual,
                                       Christian, and economically stable. However, higher education can no longer depend
                                       on uniformity among its students. Contemporary students are different in how they
                                       experience and respond to their campuses, both in the classroom and out of the classroom.
                                       This chapter focuses on using Universal Design to meet the needs of a growingly diverse
                                       student population. 
                                    </p>
                                    
                                    <p> <strong>Harper, S. R. (2015). Making Engagement Equitable for Students in U.S. Higher Education.
                                          In S. J. Quaye (Ed.),&nbsp;</strong><em><strong>Student Engagement in Higher Education: Theoretical Perspectives and Practical Approaches
                                             for Diverse Populations</strong></em><strong>&nbsp;(pp. 1-14). New York, NY: Routledge.</strong> 
                                    </p>
                                    
                                 </div>
                                 
                                 <div> <a href="documents/article-perceptions-psychological-disabilities.pdf">DSS and Accommodations in Higher Education: Perceptions of Students with Psychological
                                       Disabilities</a>
                                    
                                    <p> The number of individuals with psychological disabilities attending colleges and
                                       universities has increased steadily over the last decade. However, students with psychological
                                       disabilities are less likely to complete their college programs than their non-disabled
                                       peers and peers with other types of disabilities. This qualitative study explored
                                       how college students with psychological disabilities utilize assistance provided by
                                       Disability Support Services (DSS), including accommodations, in order to reach their
                                       postsecondary goals and examined how these students perceived and described the impact
                                       of these services. The researcher conducted in-depth interviews with 16 participants
                                       and utilized grounded theory research methods to collect and analyze data. Various
                                       themes emerged from the study, including benefits and challenges of using accommodations,
                                       the role of DSS on participants’ academic experiences, and issues regarding disclosure
                                       and stigma. 
                                    </p>
                                    
                                    <p> <strong>Stein, K. F. (2013). DSS and Accommodations in Higher Education: Perceptions of Students
                                          with Psychological Disabilities.&nbsp;</strong><em><strong>Journal of Postsecondary Education and Disability,</strong></em><strong>&nbsp;</strong><em><strong>26</strong></em><strong>(2), 145-161.</strong> 
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </main>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/faculty-toolbox.pcf">©</a>
      </div>
   </body>
</html>