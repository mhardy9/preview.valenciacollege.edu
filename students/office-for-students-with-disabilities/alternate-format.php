<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/alternate-format.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <main>
                           
                           
                           <div>
                              
                              <h2>Alternate Format Materials</h2>
                              
                              <p>The Office for Students with Disabilities is able to provide materials in an alternate
                                 format to eligible students with pre-approved and qualifying disabilities. Various
                                 formats can be made available.  Provision of alternate format materials is dependent
                                 on documentation of disability. Students are responsisble for requesting alternative
                                 formats for textbooks that they will need. Please be aware that alternative format
                                 production can take several weeks, therefore, be sure to follow the procedure below.
                              </p>
                              
                              <h3>Requesting Materials in an Alternate Format</h3>
                              
                              <p> To receive course materials in an alternate format students must first be registered
                                 with the Office for Students with Disabilities and be approved for the use of this
                                 accommodation. The following steps detail the process for requesting materials in
                                 alternate format:
                              </p>
                              
                              
                              <ol>
                                 
                                 <li>During the intake meeting a member of the Office for Students with Disabilities will
                                    review the student's documentation of disability and verify a print disability and
                                    eligibility for this accommodation.
                                 </li>
                                 
                                 <li>Students will meet with the Assistant Director of the Office for Students with Disabilities
                                    to discuss which alternative formats will best provide access. 
                                 </li>
                                 
                                 <li>Students may meet with the Assistive Technology Specialist for training on how to
                                    use the technology and where to access that technology on campus.
                                 </li>
                                 
                                 <li>Requests for alternate format materials can be made by submitting an <a href="http://net4.valenciacollege.edu/forms/osd/assistive-technology-services/alternate_text_request.cfm" target="_blank">Alternate Format Request Form</a>.
                                 </li>
                                 
                                 <li>Students will be required to purchase their textbooks and provide the Office for Students
                                    with Disabilities proof of purchase. This may be done in person or via email.
                                 </li>
                                 
                              </ol>
                              
                              <p>Students are advised to discuss any questions or concerns about this accommodation
                                 with the Assistive Technology Specialist or the Assistant Director of the Office for
                                 Students with Disabilities. 
                              </p>
                              
                              
                              
                              <p>Students are reminded the production of alternate format materials can take up to
                                 30 business days from the date the materials become available for production. The
                                 management of requests for materials to be converted in an alternate format are operated
                                 on a first in, first out basis, the earlier the request is received in the sooner
                                 they are processed and sent out. Materials received less than 30 days prior to the
                                 start of a term will be converted and delivered as quickly as possible, however there
                                 is no guarantee they will be delivered by the start of classes. 
                              </p>
                              
                              <p>Students who qualify for this accommodation are strongly encouraged to obtain memberships
                                 with <a href="https://www.learningally.org/">Learning Ally</a> and <a href="https://www.bookshare.org/">Bookshare</a>. 
                              </p>
                              
                              <p>The Office for Students with Disabilities will make every effort to provide the format
                                 the student prefers, however we cannot guarantee that the preferred format will be
                                 available. In this case, an  effective alternative can be provided. 
                              </p>
                              
                              <h3>Alternate Format Agreement Policy</h3>
                              
                              <p>Students are advised to review the alternate format agreement policy prior to requesting
                                 materials in an alternate format.
                              </p>
                              
                              <ul>
                                 
                                 <li> I agree to contact the Office for Students with Disabilities if any of my contact
                                    information changes, this includes phone numbers, name changes, etc.
                                 </li>
                                 
                                 <li>I agree to notify the Office for Students with Disabilities if I drop a class and
                                    I will immediately return all alternate format materials for that class.
                                 </li>
                                 
                                 <li>I agree to notify the Office for Students with Disabilities if my class will use a
                                    different textbook than the one(s) I requested or if my alternate format requests
                                    change in any other way.
                                 </li>
                                 
                                 <li>I agree to check my alternate format materials as soon as I receive them and to contact
                                    the Office for Students with Disabilities immediately if there are issues.
                                 </li>
                                 
                                 <li>I agree to return all alternate format materials to the Office for Students with Disabilities
                                    and to delete them from my computer and/or devices at the end of the term.
                                 </li>
                                 
                                 <li>I understand that turn-around time is 30 business days from the date the material
                                    is available for production and that the material is provided on a first come-first
                                    served basis.
                                 </li>
                                 
                                 <li>I understand that the Office for Students with Disabilities makes every effort to
                                    provide alternate format materials in my preferred format but cannot guarantee that
                                    the preferred format will be available. In that case, I will receive an effective
                                    alternative.
                                 </li>
                                 
                                 <li>I understand that this accommodation is based on documentation I have provided to
                                    the Office for Students with Disabilities.
                                 </li>
                                 
                                 <li>I understand that if my files are provided by a publisher, I must present a receipt
                                    for each book before I can receive my alternate format materials or notification that
                                    they are ready.
                                 </li>
                                 
                                 <li>I understand that when my requested materials are ready, I will be contacted via my
                                    Atlas email.
                                 </li>
                                 
                                 <li>I confirm that I am registered for the classes for which I am requesting alternate
                                    format.
                                 </li>
                                 
                                 <li>I acknowledge that the Office for Students with Disabilities can only provide me with
                                    textbooks in an alternate format for classes that I am currently registered for.
                                 </li>
                                 
                                 <li>I understand that failure to abide by this agreement may constitute a violation of
                                    copyright laws and of the Valencia College Student Code of Conduct. Therefore, failure
                                    to abide by this agreement may result in disciplinary action as outlined in the Student
                                    Handbook, the College Catalog, and on the Valencia website under College Policies.
                                 </li>
                                 
                              </ul>            
                              
                              <h3>Alternate Format Resources for Students</h3>
                              
                              
                              <ul>
                                 
                                 <li><a href="http://www.loc.gov/nls/">National Library Services for the Blind and Physically Handicapped</a></li>
                                 
                                 <li><a href="https://www.bookshare.org/">Bookshare</a></li>
                                 
                                 <li><a href="https://www.learningally.org/">Learning Ally</a></li>
                                 
                                 <li><a href="http://www.gutenberg.org/wiki/Main_Page">Gutenberg Library</a></li>
                                 
                                 <li><a href="http://www.audible.com/">Audible</a></li>
                                 
                                 <li><a href="http://librivox.org/">LibriVox</a></li>
                                 
                                 <li><a href="http://louis.aph.org/catalog/CategoryInfo.aspx?cid=152">The American Printing House for the Blind</a></li>
                                 
                                 <li><a href="https://kindle.amazon.com/">Kindle</a></li>
                                 
                              </ul>
                              
                              
                           </div>
                           
                           
                        </main>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/alternate-format.pcf">©</a>
      </div>
   </body>
</html>