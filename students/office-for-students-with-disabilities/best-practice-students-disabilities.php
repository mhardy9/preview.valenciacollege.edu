<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office for Students with Disabilities | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/office-for-students-with-disabilities/best-practice-students-disabilities.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/office-for-students-with-disabilities/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Office for Students with Disabilities</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/office-for-students-with-disabilities/">Office For Students With Disabilities</a></li>
               <li>Office for Students with Disabilities</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        <main>
                           
                           <div>
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div>
                                       
                                       <h2>Best Practices for Working with Students with Disabilities</h2>
                                       
                                       <h3>Working with Students who have Learning Disabilities</h3>
                                       
                                       <ul>
                                          
                                          <li> Pause and ask questions throughout lesson to check for understanding. Access students'
                                             prior knowledge. This helps them relate similar concepts. 
                                          </li>
                                          
                                          <li>Give examples of key concepts.</li>
                                          
                                          <li> If possible, provide presentations or lecture materials on line. Guided notes are
                                             helpful as well. 
                                          </li>
                                          
                                          <li>Provide examples of good projects/papers. </li>
                                          
                                          <li>Provide a study guide or practice exams that familiarize students with the format
                                             of the test. 
                                          </li>
                                          
                                          <li>Provide a list of all of the learning objectives that the student is expected to master
                                             for the test. 
                                          </li>
                                          
                                          <li>Allow time for an early draft of a paper or projects to be turned in for feedback.
                                             
                                          </li>
                                          
                                          <li>Use multiple formats: Visual aids, three-dimensional models, charts or graphics, group
                                             projects, visual stimuli, audio and video content to accommodate different learning
                                             styles. 
                                          </li>
                                          
                                          <li>Allow audio recording of lectures.</li>
                                          
                                          <li> Consider that students with reading disabilities may not wish to read out loud in
                                             class. 
                                          </li>
                                          
                                          <li>Encourage the student to visit during office hours for clarification of content. </li>
                                          
                                       </ul>
                                       
                                       <h3>Working with Students who are Blind or have Low Vision </h3>
                                       
                                       <ul>
                                          
                                          <li>If possible, have textbook info and syllabus available early for conversion into alternate
                                             format. 
                                          </li>
                                          
                                          <li>Consult with OSD staff on any lecture materials, assignments, or tests that can be
                                             converted for the student. 
                                          </li>
                                          
                                          <li>Consider obstacles in the classroom that might present a problem. </li>
                                          
                                          <li>Student may need information about the physical layout of the classroom. </li>
                                          
                                          <li>Consider the impact of lighting on the student's ability to see. </li>
                                          
                                          <li>Give clear verbal descriptions of visual materials. Allow audio recording of lectures.</li>
                                          
                                          <li>Address students by name, so they know you are talking to them. </li>
                                          
                                          <li>Speak in a normal tone and speed of voice.</li>
                                          
                                          <li>Don't assume that your help is wanted, or needed, rather ask if the student would
                                             like your help.
                                          </li>
                                          
                                          <li>Face the student when you speak. </li>
                                          
                                       </ul>
                                       
                                       <h3>Working with Students who are Deaf or Hard of Hearing </h3>
                                       
                                       <ul>
                                          
                                          <li>Speak directly to the student and not the interpreter or captionist. </li>
                                          
                                          <li>Face the class when speaking; don't face the board when you speak.</li>
                                          
                                          <li>Give the student time to look back and forth between any materials he/she needs to
                                             view, the interpreter or the captionist's screen, and you. 
                                          </li>
                                          
                                          <li>When students make comments in class or ask questions, repeat the questions before
                                             answering, or phrase your answers in such a way that the questions are obvious. 
                                          </li>
                                          
                                          <li>Use visual aids and reference whenever possible. </li>
                                          
                                          <li>Provide videos and slides with <strong>captioning</strong>. If captioning is not available, supply a transcript of the content.
                                          </li>
                                          
                                          <li> If possible, make any PowerPoint presentations or lecture materials available for
                                             student access. 
                                          </li>
                                          
                                          <li>Provide a written supplement to oral instructions, assignments, and directions. </li>
                                          
                                          <li>Encourage students in class to speak one at a time. </li>
                                          
                                          <li>Consider the impact of lighting on the student's ability to see your face (lips, expressions
                                             and gestures). 
                                          </li>
                                          
                                       </ul>
                                       
                                       <h3>Working with Students with Attention or Processing Disabilities</h3>
                                       
                                       <ul>
                                          
                                          <li>Use consistent, predictable content delivery in your online environment </li>
                                          
                                          <li>Encourage the use of a personal planner</li>
                                          
                                          <li>Provide copies of class presentations in your course shell</li>
                                          
                                          <li>If the student opens the conversation, ask questions about what strategies they use
                                             in class 
                                          </li>
                                          
                                       </ul>
                                       
                                       <h3>Working with Students with Cognitive Disabilities</h3>
                                       
                                       <ul>
                                          
                                          <li>Make no assumptions about the student's abilities</li>
                                          
                                          <li>The student may need extra time to process information</li>
                                          
                                          <li>Don't take a lack of immediate response personally; information overload can take
                                             a few moments to work through
                                          </li>
                                          
                                          <li>Allow for different styles of processing information </li>
                                          
                                       </ul>
                                       
                                       <p>Please contact the <a href="default.html#OSD-Locations">Office for Students with Disabilities</a> for more information or technical assistance. 
                                       </p>
                                       
                                       <p>For more assistance with working with students with disabilities, please refer to
                                          the <a href="documents/OSD-faculty-guide.pdf">Faculty Resource Guide</a>.
                                       </p>
                                       
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                           </div>
                           
                           
                           
                        </main>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <div>
                           <a href="documents/osd-student-handbook.pdf" target="_blank" title="2016 - 2017 Valencia Office for Students with Disabilities Student Handbook">
                              <button type="button">
                                 
                                 <h1>
                                    <span>STUDENT</span><br><span>HANDBOOK</span>
                                    
                                 </h1>
                                 </button>
                              </a>
                           
                           <p>Access information on policies, accommodations and student responsibilities.</p>
                           
                        </div>
                        
                        
                        
                        
                        <h2>Location, Contact, &amp; Hours</h2>
                        
                        
                        <div> 
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              Office for Students with Disabilities 
                              
                           </div>
                           <br>
                           
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">College closed in observance of Labor Day September, 4th 2017. <br>
                                       Monday - Thursday: 8am to 5pm<br>
                                       Friday 9am to 5pm
                                    </div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div data-old-tag="table">
                              
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          East Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 5, Rm 216</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdeast@valenciacollege.edu">osdeast@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Tuesday: 8:00 am to 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Lake Nona Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2229</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdlnc@valenciacollege.edu">osdlnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Call for appointments<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Osceola Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 2, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdosc@valenciacollege.edu">osdosc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Wednesday: 8:00 am - 6:00 pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          West Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB, Rm 102</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1523 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwest@valenciacollege.edu">osdwest@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Thursday: 8:00 am to 6:00 pm <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong>
                                          Winter Park Campus Office for Students with Disabilities 
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Bldg 1, Rm 212</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4167 // TTY 407-582-1222</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr"> 
                                    
                                    <div data-old-tag="td">
                                       <a href="mailto:osdwpc@valenciacollege.edu">osdwpc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/office-for-students-with-disabilities/best-practice-students-disabilities.pcf">©</a>
      </div>
   </body>
</html>