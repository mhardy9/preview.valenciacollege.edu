<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty &amp; Staff  | Valencia College</title>
      <meta name="Description" content="Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful&amp;amp;#10;      environment on campus and within our community - one where conflict leads to growth and transformation rather than&amp;amp;#10;      violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/faculty-staff/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Faculty &amp; Staff</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1">
                           					
                           <h2>Faculty &amp; Staff</h2>
                           					
                           <p></p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <p>The Peace and Justice Institute provides faculty and staff diverse opportunities to
                              engage in visioning, designing, creating and organizing the work of PJI. Faculty and
                              staff can become leaders in their area by implementing the Principles for How We Treat
                              Each Other in their classroom curriculum, academic division or area of work at the
                              College.
                           </p>
                           					
                           <p>PJI offers Valencia faculty and staff multiple entry points and opportunities for
                              partnership and collaboration, be it through designing a peace and justice course
                              through the curriculum or participating in the development of the co-curricular experience.
                              Employees are invited and encouraged to attend and participate in the the creation
                              and organization of Global Peace Week, Conversation on Justice, the Peace Breakfasts,
                              film screenings, speaker events, and the diverse workshops and trainings offered throughout
                              the year.
                           </p>
                           					
                           <p>The calendar of events features workshops, film screenings, guest speakers and scholars,
                              among other important events available for all Valencia employees. Additionally, opportunities
                              are available for faculty and staff to take leadership roles in the office of the
                              Peace and Justice Institute. Contact PJI for more information.
                           </p>
                           					
                           <p>Essentially the Peace and Justice Institute flourishes as a collaborative community
                              of practitioners bringing their diverse talents and gifts to the table. All are welcome
                              to be a part of PJI.
                           </p>
                           					
                           <ul>
                              						
                              <li>
                                 							<a class="headSub_1" href="faculty-curriculum.php">Faculty Curriculum</a>
                                 						
                              </li>
                              						
                              <li>
                                 							<a class="headSub_1" href="staff-curriculum.php">Staff Curriculum</a>
                                 						
                              </li>
                              					
                           </ul>
                           					
                           <p>&nbsp;</p>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/faculty-staff/index.pcf">©</a>
      </div>
   </body>
</html>