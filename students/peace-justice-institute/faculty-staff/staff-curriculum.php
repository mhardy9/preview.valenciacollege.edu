<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Staff Curriculum  | Valencia College</title>
      <meta name="Description" content="Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful&amp;amp;#10;      environment on campus and within our community - one where conflict leads to growth and transformation rather than&amp;amp;#10;      violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/faculty-staff/staff-curriculum.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/faculty-staff/">Faculty &amp; Staff</a></li>
               <li>Staff Curriculum </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1">
                           					
                           <div class="indent_title_in">
                              						
                              <h2>Staff Curriculum</h2>
                              						
                              <p></p>
                              					
                           </div>
                           					
                           <div class="wrapper_indent">
                              						
                              <p>The Valencia College Peace and Justice Institute regards every individual as a rising
                                 peace and justice practitioner and provides the tools for all to be leaders of socially
                                 inclusive change and innovation in their circle of influence. The commitments of a
                                 peace and justice practitioner are a pathway to building The Culture of Peace and
                                 an invitation to becoming an agent of change.<br>
                                 						The commitments of a peace and justice practitioner include the following:
                              </p>
                              						
                              <ul>
                                 							
                                 <li>Places relationship as central to the work, focusing on the culture of collaboration</li>
                                 							
                                 <li>Encourages a reflective practice to support self-awareness, meaning and purpose, including
                                    mindfulness practice and emotional intelligence
                                 </li>
                                 							
                                 <li>Addresses conflict as a source and opportunity for growth and transformation</li>
                                 							
                                 <li>Uses the tools of story, dialogue, and peaceful communication while practicing the
                                    Principles for How We Treat Each Other
                                 </li>
                                 							
                                 <li>Supports a community of inclusive excellence in which all voices are heard and valued</li>
                                 							
                                 <li>Engages in the exploration of the “other” with an acknowledgment of our inherent interdependence</li>
                                 							
                                 <li>Recognizes that there can be no peace without justice for all</li>
                                 						
                              </ul>
                              						
                              <p>A staff curriculum at Valencia College is currently in development. Current programs
                                 that are available to staff members are included below.<br> Please contact PJI at <a href="mailto:peaceandjustice@valenciacollege.edu">peaceandjustice@valenciacollege.edu</a> for more information.
                              </p>
                              					
                           </div>
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p></p>
                                 							
                                 <h4></h4>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <hr class="styled_2">
                           					
                           <div class="box_style_1">
                              						
                              <h3>Peace and Justice Spring Retreat</h3>
                              						
                              <p></p>
                              					
                           </div>
                           					
                           <div class="wrapper_indent">
                              						
                              <p></p>
                              						
                              <div class="row">
                                 							
                                 <div class="col-md-12">
                                    								
                                    <p>Contemplative pedagogy involves a wide range of teaching methods designed to cultivate
                                       a capacity for deeper awareness, concentration, and insight that create demonstrable
                                       neurobiological changes. Participants will be introduced to the nature, history, and
                                       status of contemplative practices used in mainstream education as a complementary
                                       pedagogic tool that fosters depth in learning. Participants will have the opportunity
                                       to experiment with and create a contemplative-based classroom practice and reflect
                                       upon how contemplation may innovatively meet the needs of today's students.
                                    </p>
                                    								
                                    <p>The next retreat is taking place Friday, February 24 - Saturday, February 25, 2017.
                                       <br>Please contact the Peace and Justice Institute for more information.<br>
                                       								<a href="/students/peace-justice-institute/documents/retreat-ad.pdf">Spring Faculty and Staff Retreat PDF</a></p>
                                    							
                                 </div>
                                 						
                              </div>
                              					
                           </div>
                           					
                           <hr class="styled_2">
                           					
                           <div class="box_style_1">
                              						
                              <h3>SEED Seminar</h3>
                              					
                           </div>
                           					
                           <div class="wrapper_indent">
                              						
                              <div class="row">
                                 							
                                 <div class="col-md-12">
                                    								
                                    <p>SEED stands for Seeking Educational Equity and Diversity and is a national project,
                                       founded 27 years ago by Dr. Peggy McIntosh of Wellesley College. It utilizes a cohort-based
                                       monthly seminar model with the intention of creating gender fair, multiculturally
                                       equitable, and globally informed educational spaces and workplaces. SEED differs from
                                       other diversity programs in that SEED leaders do not lecture. Instead they lead their
                                       own colleagues in experiential, interactive exercises and conversation often stimulated
                                       by films and readings. The monthly seminars deepen participants' understanding of
                                       themselves, expand their knowledge of the world, and point the way to making schools
                                       more inclusive.
                                    </p>
                                    								
                                    <p>This is a cohort experience for faculty and staff. Applications are available each
                                       fall through the Grove. <br>If you have any questions about SEED at Valencia, please contact <a href="http://thegrove.valenciacollege.edu/featured-colleague-jenny-charriez/" target="blank">Jenny Charriez</a>, director, employee development and inclusion, at <a href="mailto:jnevarez@valenciacollege.edu">jnevarez@valenciacollege.edu</a> or extension 8250.
                                    </p>
                                    							
                                 </div>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/faculty-staff/staff-curriculum.pcf">©</a>
      </div>
   </body>
</html>