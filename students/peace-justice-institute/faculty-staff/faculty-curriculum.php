<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Curriculum  | Valencia College</title>
      <meta name="Description" content="Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful&amp;amp;#10;      environment on campus and within our community - one where conflict leads to growth and transformation rather than&amp;amp;#10;      violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/faculty-staff/faculty-curriculum.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/faculty-staff/">Faculty &amp; Staff</a></li>
               <li>Faculty Curriculum </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">	
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1">
                           					
                           <h2>Faculty Curriculum</h2>
                           					
                           <p>Peace and Justice Pedagogy</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <p>"Good teaching cannot be reduced to technique; good teaching comes from the identity
                              and integrity of the teacher." <em>- Parker J. Palmer</em></p>
                           					
                           <p>The Peace and Justice Institute at Valencia College promotes peace and justice for
                              all. Our aim is to nurture an inclusive, caring and respectful environment on campus
                              and within our community--one where conflict leads to growth and transformation, rather
                              than violence or aggression.
                           </p>
                           					
                           <p>Acknowledging that the pedagogy of a peace and justice course integrates peace practices,
                              faculty who intend to teach a peace and justice course (i.e. Introduction to Peace
                              Studies, The Psychology of Peace, etc.) or to integrate modules within existing courses
                              with peace and justice themes are encouraged to engage in these faculty development
                              courses.
                           </p>
                           					
                           <p>The Valencia College Peace and Justice Institute regards every individual as a rising
                              peace and justice practitioner and provides the tools for all to be leaders of socially
                              inclusive change and innovation in their circle of influence. The commitments of a
                              peace and justice practitioner are a pathway to building The Culture of Peace and
                              an invitation to becoming an agent of change.
                           </p>
                           					
                           <p>The commitments of a peace and justice practitioner include the following:</p>
                           					
                           <ul>
                              						
                              <li>Places relationship as central to the work, focusing on the culture of collaboration</li>
                              						
                              <li>Encourages a reflective practice to support self-awareness, meaning and purpose, including
                                 mindfulness practice and emotional intelligence
                              </li>
                              						
                              <li>Addresses conflict as a source and opportunity for growth and transformation</li>
                              						
                              <li>Uses the tools of story, dialogue, and peaceful communication while practicing the
                                 Principles for How We Treat Each Other
                              </li>
                              						
                              <li>Supports a community of inclusive excellence in which all voices are heard and valued</li>
                              						
                              <li>Engages in the exploration of the “other” with an acknowledgement of our inherent
                                 interdependence
                              </li>
                              						
                              <li>Recognizes that there can be no peace without justice for all</li>
                              					
                           </ul>
                           					
                           <p>These high impact practices for peace and justice pedagogy at Valencia create classrooms
                              that model the culture of peace and stand as models for communities of peace.
                           </p>
                           				
                        </div>
                        				
                        <div class="row">
                           					&nbsp;
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 7311 Creating a Safe Space</h3>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p>INDV 7311 Creating a Safe Space<br>
                                    							<strong>KICKOFF</strong><br>
                                    							<strong>Thursday, October 20 EC 3-113, 1:00 PM - 5:00 PM</strong><br>
                                    							<strong>Friday, October 21 WC 11-106 1:00 PM - 5:00 PM</strong><br>
                                    							<strong>WRAP UP</strong><br>
                                    							<strong>Thursday, January 12 EC 3-113 2:00 PM - 4:00 PM</strong><br>
                                    							<strong>Friday, January 13 WC 11-106 2:00 PM - 4:00 PM</strong></p>
                                 							
                                 <p>This course will demonstrate techniques for establishing a respectful and inclusive
                                    environment that promotes healthy classroom dialogue. Dialogue can assist students
                                    in moving information from a memorized or theoretical understanding, to a more integrated,
                                    tangible and lasting knowledge. Participants will engage in best practices designed
                                    to promote discussion as a pedagogical tool. In registering for this course, you are
                                    committing to attend 2 meetings across two terms (one 4-hour and one 2-hour), complete
                                    the required reading, and integrate a conversation activity into your class.
                                 </p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 2151 Inclusion and Diversity</h3>
                           					
                           <p>&nbsp;SEED stands for Seeking Educational Equity and Diversity and is a national project,
                              founded 27 years ago by Dr. Peggy McIntosh of Wellesley College. It utilizes a cohort-based
                              monthly seminar model with the intention of creating gender fair, multiculturally
                              equitable, and globally informed educational spaces and workplaces. SEED differs from
                              other diversity programs in that SEED leaders do not lecture. Instead they lead their
                              own colleagues in experiential, interactive exercises and conversation often stimulated
                              by films and readings. The monthly seminars deepen participants' understanding of
                              themselves, expand their knowledge of the world, and point the way to making schools
                              more inclusive.
                           </p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p>This is a cohort experience for faculty and staff. Applications are available each
                                    fall through the Grove. If you have any questions about SEED at Valencia, please contact
                                    <a href="http://thegrove.valenciacollege.edu/featured-colleague-jenny-charriez/" target="blank">Jenny Charriez</a>, director, employee development and inclusion, at <a href="mailto:jnevarez@valenciacollege.edu">jnevarez@valenciacollege.edu</a> or extension 8250.
                                 </p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 2151 Inclusion and Diversity</h3>
                           					
                           <p>&nbsp;INDV 2151 Inclusion and Diversity</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p><strong>KICKOFF</strong><br>
                                    							<strong>Friday, January 27 WC 11-106 1:00 PM - 4:00 PM</strong><br>
                                    							<strong>WRAP UP</strong><br>
                                    							<strong>Friday, March 3 WC 11-106 1:00 PM - 4:00 PM</strong></p>
                                 							
                                 <p>The purpose of this course is to invite faculty participants to begin/continue to
                                    examine their professional and personal experiences using the lenses of race and ethnicity.
                                    You will be afforded the opportunity to tell your story while hearing the stories
                                    of others. In order to create a hospitable and accountable community, our journey
                                    will be guided by the 13 Principles of How We Treat Each Other of the Peace and Justice
                                    Institute.<br>
                                    							Participants will meet face-to-face and interact online with their colleagues
                                    using personal stories, assigned readings, discussion forums, videos and a field experience.
                                    In addition, each participant will design a new or transformed lesson/unit plan to
                                    be shared with peers. As a result of completing this course, participants will be
                                    better equipped with tools to work effectively in a pluralistic society, by improving
                                    the multicultural experience for themselves and their students. This is a hybrid course.
                                 </p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 7312 Mindfulness Tools for Education</h3>
                           					
                           <p>&nbsp;INDV 7312 Mindfulness Tools for Education&nbsp;</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p><strong>INDV 7312 Mindfulness Tools for Education</strong><br>
                                    							<strong>Thursday, February 2 EC 3-113 1:00 PM - 4:00 PM</strong><br>
                                    							<strong>Friday, February 3 WC 4-236 1:00 PM - 4:00 PM</strong></p>
                                 							
                                 <p>&nbsp;</p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 7312 Mindfulness Tools for Education</h3>
                           					
                           <p>&nbsp;INDV 7312 Mindfulness Tools for Education&nbsp;</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p><strong>INDV 7312 Mindfulness Tools for Education</strong><br>
                                    							<strong>Thursday, February 2 EC 3-113 1:00 PM - 4:00 PM</strong><br>
                                    							<strong>Friday, February 3 WC 4-236 1:00 PM - 4:00 PM</strong></p>
                                 							
                                 <p>&nbsp;</p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 7316 How We Treat Each Other</h3>
                           					
                           <p>&nbsp;INDV 7316 How We Treat Each Other</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							<strong>Thursday, October 6, 2016; EC 8-105D; 2:00-4:00 pm</strong><br>
                                 							<strong>OR</strong><br>
                                 							<strong>Friday, October 7, 2016; WC 11-218; 2:00-4:00 pm</strong><br>
                                 							
                                 <p>&nbsp;</p>
                                 							
                                 <p>How We Treat Each Other gives participants effective tools for engaging in difficult
                                    conversations, empathetic listening, perspective talking, self-reflection and relationship
                                    building. By using the Principles and their practices, we increase our capacity to
                                    be peace builders. Participants will learn how to implement these techniques in Valencia's
                                    varied learning environments.
                                 </p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>INDV 2255 Multiple Perspectives</h3>
                           					
                           <p>INDV 2255 Multiple Perspectives<br>
                              					<strong>Thursday, February 23 OC 4-105 2:00 PM - 4:00 PM</strong><br>
                              					<strong>Friday, February 24 EC 3-113 2:00 PM - 4:00 PM</strong></p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					&nbsp;
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>NDV 7310 Working with Conflict</h3>
                           					
                           <p>INDV 7310 Working with Conflict<br>
                              					<strong>Friday, March 10 WC 11-106 1:00 PM - 5:00 PM</strong></p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p>In this course, participants will learn about the components, roles and needs active
                                    in conflict, identify their own conflict style, and be introduced to conflict resolution
                                    practices for the purpose of more productive and positive outcomes. Participants will
                                    engage in various exercises, including a self-assessments and mock negotiation for
                                    the purpose of integrating these skills into a working knowledge.
                                 </p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>Peace and Justice Spring Retreat</h3>
                           					
                           <p>Peace and Justice Spring Retreat</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p>Contemplative pedagogy involves a wide range of teaching methods designed to cultivate
                                    a capacity for deeper awareness, concentration, and insight that create demonstrable
                                    neurobiological changes. Participants will be introduced to the nature, history, and
                                    status of contemplative practices used in mainstream education as a complementary
                                    pedagogic tool that fosters depth in learning. Participants will have the opportunity
                                    to experiment with and create a contemplative-based classroom practice and reflect
                                    upon how contemplation may innovatively meet the needs of today's students.
                                 </p>
                                 							
                                 <p>The next retreat is taking place Friday, February 24 - Saturday, February 25, 2017.
                                    Please contact the Peace and Justice Institute for more information.<br>
                                    							<a href="/documents/studets/offices-services/peace-justice-institute/RetreatAd.pdf">Spring Faculty and Staff Retreat PDF</a></p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        				
                        <hr class="styled_2">
                        				
                        <div class="box_style_1">
                           					
                           <h3>SEED Seminar</h3>
                           					
                           <p>&nbsp;SEED Seminar</p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <div class="row">
                              						
                              <div class="col-md-12">
                                 							
                                 <p>SEED stands for Seeking Educational Equity and Diversity and is a national project,
                                    founded 27 years ago by Dr. Peggy McIntosh of Wellesley College. It utilizes a cohort-based
                                    monthly seminar model with the intention of creating gender fair, multiculturally
                                    equitable, and globally informed educational spaces and workplaces. SEED differs from
                                    other diversity programs in that SEED leaders do not lecture. Instead they lead their
                                    own colleagues in experiential, interactive exercises and conversation often stimulated
                                    by films and readings. The monthly seminars deepen participants' understanding of
                                    themselves, expand their knowledge of the world, and point the way to making schools
                                    more inclusive.
                                 </p>
                                 							
                                 <p>This is a cohort experience for faculty and staff. Applications are available each
                                    fall through the Grove. If you have any questions about SEED at Valencia, please contact
                                    Jenny Charriez, director, employee development and inclusion, at <a href="mailto:jnevarez@valenciacollege.edu">jnevarez@valenciacollege.edu</a> or extension 8250.
                                 </p>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/faculty-staff/faculty-curriculum.pcf">©</a>
      </div>
   </body>
</html>