<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/events/past-events.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/events/">Events</a></li>
               <li>Peace and Justice Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2> Past Events</h2>
                           
                           
                           
                           
                           These are past events starting from October 18, 2016.
                           
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/hispanic_heritage_month_celebration" target="_blank" title="Come join the Campus Activities Board for our annual Hispanic Heritage Celebration at the SSB Patio. Music, entertainment, giveaways, and food will be provided.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Hispanic Heritage Month Celebration at West Campus Student Services Building (SSB)</span> <br>
                                    
                                    
                                    <span>
                                       <strong>October 27, 2016</strong>
                                       
                                       at 12:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/el_dia_de_los_muertosopen_mic" target="_blank" title="Share your stories, poems, and art!">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>El Dia de los Muertos: Open Mic at West Campus Building 6 – Raymer F. Maguire, Jr.
                                       Library</span> <br>
                                    
                                    
                                    <span>
                                       <strong>November 01, 2016</strong>
                                       
                                       at 1:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/el_dia_de_los_muertos_art_and_poetry" target="_blank" title="Lecture, Food, and Music 6pm in 5-111  Dia de los Muertos (Day of the Dead) is a celebration of life. It is a time to come together and reflect on the lives of loved ones...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>El Dia de los Muertos: Art and Poetry at West Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>November 02, 2016</strong>
                                       
                                       at 6:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/poinciana_winter_fun_parade_and_festival" target="_blank" title="The annual Winter Fun 2016 Holiday Parade, hosted by the Association of Poinciana Villages, is open to the entire counties of Osceola and Polk. The festival will be at Vance...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Poinciana Winter Fun Parade and Festival at Vance Harmon Park</span> <br>
                                    
                                    
                                    <span>
                                       <strong>December 03, 2016</strong>
                                       
                                       at 11:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/bridges_to_success_movie_night_black_america_since_mlk_and_still_i_rise" target="_blank" title="Bridges to Success will host a college wide “Movie Night” on February 28th of a documentary recently aired on PBS – Black America Since MLK: And Still I Rise. This event will...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Bridges to Success:  Movie Night: Black America Since MLK:  And Still I Rise at West
                                       Campus Health Sciences Building (HSB)</span> <br>
                                    
                                    
                                    <span>
                                       <strong>February 03, 2017</strong>
                                       
                                       at 8:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/the_forgotten_stories_of_muslims_who_saved_jewish_people_during_the_holocaust" target="_blank" title="Professor Aby Boumarate, Nicole  Valentino, and Dr. Yasmeen Qadri will present at this workshop which will focus on the Upstanders model and will educate students about the...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>The Forgotten Stories of Muslims Who Saved Jewish People during the Holocaust at East
                                       Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>February 22, 2017</strong>
                                       
                                       at 11:30 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/wpc_honors_black_history_month" target="_blank" title="Come learn about and celebrate the contributions and successes of Black leaders to science, technology, politics, peace, arts, and culture in American and World history. Test...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>WPC Honors Black History Month at Winter Park Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>February 23, 2017</strong>
                                       
                                       at 10:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/soul_session_8956" target="_blank" title="Celebrating African American culture. We will provide food, enternatinment, and a timeline of historic figures.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Soul Session at West Campus Health Sciences Building (HSB)</span> <br>
                                    
                                    
                                    <span>
                                       <strong>February 23, 2017</strong>
                                       
                                       at 1:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/international_debate_showcase_2714" target="_blank" title="Please VGD students bring your official VGD passport  to record your hours.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>International Debate Showcase at West Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>February 23, 2017</strong>
                                       
                                       at 3:30 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/exhibition_tour_the_black_figure_in_the_european_imaginary" target="_blank" title="Please VGD Students bring your VGD official passport to record your hours.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Exhibition Tour: the Black Figure in the European Imaginary at Rollins College</span> <br>
                                    
                                    
                                    <span>
                                       <strong>February 24, 2017</strong>
                                       
                                       at 11:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/temple_university_japan_info-session_5600" target="_blank" title="Temple University Japan Campus (TUJ) provides students with a full four-year American degree program in Tokyo. Classes are done all in English while you study in Japan.  We...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Temple University Japan Info-session at East Campus Building 3</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 01, 2017</strong>
                                       
                                       at 11:30 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/cuba_at_a_crosstoads_lost_and_found" target="_blank" title="You are invited to meet Cuban Artist Atner Cadalso for a moderated exhibit of his work illuminating contemporary Cuba.  Presented by Professor Richard Sansone with support...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Cuba at a Crossroads: Lost and Found at West Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 01, 2017</strong>
                                       
                                       at 5:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/temple_university_japan_info-session_2597" target="_blank" title="Temple University Japan Campus (TUJ) provides students with a full four-year American degree program in Tokyo. Classes are done all in English while you study in Japan.  We...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Temple University Japan Info-session at Osceola Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 06, 2017</strong>
                                       
                                       at 2:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/conflict_around_the_world_let_peace_begin_with_me" target="_blank" title="This event  will display information about countries struck with war around the world. Education majors from Diversity class will host this event as part of their course...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Conflict Around the World: Let Peace Begin with Me at East Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 08, 2017</strong>
                                       
                                       at 11:30 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/peace_breakfast_an_interfaith_gathering_8952" target="_blank" title="This event is sponsored by PJI.  Please VGD students bring your passport to record your time.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Peace Breakfast: An Interfaith Gathering at East Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 21, 2017</strong>
                                       
                                       at 8:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/coalition_for_the_homeless" target="_blank" title="This event is sponsored by PJI.  Please VGD students bring your passport to record your time.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Coalition for the Homeless at East Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 25, 2017</strong>
                                       
                                       at 8:30 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/movie_night_-_bernice_osceola" target="_blank" title="Valencia College Office of Student Development and Professor Richard Sansone invite you to meet Producer Elisa Tolomelli and view her latest film at a soft opening. This is a...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Movie Night - Bernice at Osceola Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 30, 2017</strong>
                                       
                                       at 6:30 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/valencia_night_at_the_hindu_society_of_central_florida_3676" target="_blank" title="Would you like to learn more about Hinduism and explore ways in which our community can learn from and better connect with each other?  Please join the Valencia College Peace...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Valencia Night at the Hindu Society of Central Florida at Hindu Society of Central
                                       Florida</span> <br>
                                    
                                    
                                    <span>
                                       <strong>March 31, 2017</strong>
                                       
                                       at 5:30 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/foreign_film_festival" target="_blank" title="The Language Department at Osceola campus will be hosting the Foreign Film Festival in the auditorium of building 1 from 6-8pm.   April 5th, 2017:  La Misma Luna &amp;quot;Nothing is...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Foreign Film Festival at Osceola Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>April 01, 2017</strong>
                                       
                                       at 6:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/japanese-american_internment_camps" target="_blank" title="Valencia Global Distinction welcomes guest speaker Howard Sugiuchi, a former child internee of the Japanese-American Internment Camps.  Mr. Sugiuchi, a United States Veteran,...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Japanese-American Internment Camps at East Campus Building 8</span> <br>
                                    
                                    
                                    <span>
                                       <strong>April 05, 2017</strong>
                                       
                                       at 11:30 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/movie_night_-_bernice" target="_blank" title="Valencia College Office of Student Development and Professor Richard Sansone invite you to meet Producer Elisa Tolomelli and view her latest film at a soft opening. This is a...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Movie Night - Bernice at West Campus Building 3</span> <br>
                                    
                                    
                                    <span>
                                       <strong>April 06, 2017</strong>
                                       
                                       at 6:30 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/understanding_autism_-_special_guest_lecture_dr_teresa_daly_director_of_the_ucf_center_for_autism_and_related_disabilities" target="_blank" title="Join the Office for Students with Disabilities (OSD) as we host our special guest lecturer Dr. Teresa Daly, Director of the University of Central Florida's Center for Autism...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Understanding Autism - Special Guest Lecture: Dr. Teresa Daly, Director of the UCF
                                       Center for Autism and Related Disabilities at East Campus Building 8</span> <br>
                                    
                                    
                                    <span>
                                       <strong>April 13, 2017</strong>
                                       
                                       at 1:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/goodevil_art_show" target="_blank" title="Anyone can submit works of art between March 15-April 1. Art will be displayed all day on April 21. We will have an elegant reception at 6pm with light refreshments. Dress...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Good&amp;Evil Art Show at West Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>April 21, 2017</strong>
                                       
                                       at 6:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/information_table_apv_summer_luau" target="_blank" title="Staff members will be available to assist prospective and current students interested in attending the Poinciana campus this fall with general questions.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Information Table @ APV Summer Luau  at APV Community Center</span> <br>
                                    
                                    
                                    <span>
                                       <strong>June 24, 2017</strong>
                                       
                                       at 6:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/shopping_bag_giveaway_7860" target="_blank" title="Valencia College representatives will be available to offer information on the new Poinciana Campus.  Recyclable shopping bags will be given away on a first-come, first-serve...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Shopping Bag Giveaway at Publix Bellalago</span> <br>
                                    
                                    
                                    <span>
                                       <strong>July 08, 2017</strong>
                                       
                                       at 12:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/hispanic_heritage_month" target="_blank" title="Come celebrate hispanic cultures from the 21 hispanic countries around the world. It will feature a dance performance, novelty acts, and more!">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Hispanic Heritage Month at West Campus Student Services Building (SSB)</span> <br>
                                    
                                    
                                    <span>
                                       <strong>September 21, 2017</strong>
                                       
                                       at 12:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/global_peace_week-_strategies_for_prejudice_reduction" target="_blank" title="Prejudice is a fast spreading and contagious disease and we will teach you how to cure it! Come and learn strategies for prejudice reduction from experts and enjoy the...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Global Peace Week - Strategies for Prejudice Reduction at East Campus Building 8</span> <br>
                                    
                                    
                                    <span>
                                       <strong>September 27, 2017</strong>
                                       
                                       at 11:30 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/medieval_renaissance_fair" target="_blank" title="Take an interactive virtual tour through Medieval and Renaissance history. Play some medieval games, see displays featuring Leonardo da Vinci's amazing inventions, and enjoy...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Medieval &amp; Renaissance Fair at Winter Park Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>September 28, 2017</strong>
                                       
                                       at 11:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/hispanic_heritage_day_4471" target="_blank" title="Celebrate the traditions of our Hispanic Heritage community! Join in as we trace our traditions through Latin American foods and activities.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Hispanic Heritage Day at Lake Nona Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>October 03, 2017</strong>
                                       
                                       at 11:00 AM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/film_latinos_beyond_reel" target="_blank" title="Latinos Beyond Reel: Challenging a Media Stereotype. Join us for this documentary and discussion.">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Film: Latinos Beyond Reel at West Campus Building 6 – Raymer F. Maguire, Jr. Library</span> <br>
                                    
                                    
                                    <span>
                                       <strong>October 03, 2017</strong>
                                       
                                       at 1:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/poincianas_first_truck_it_wednesday" target="_blank" title="Mi Casita Food Truck will be in town to serve some of the tastiest Latin and International - Ecuadorian Style Food!  Menu options available at Poinciana Campus Front Desk...">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Poinciana's First "Truck It" Wednesday at Poinciana Campus</span> <br>
                                    
                                    
                                    <span>
                                       <strong>October 04, 2017</strong>
                                       
                                       at 4:00 PM</span>
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                        </section>    
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank">DONATE</a>
                        <a href="../documents/Global-Peace-Week-2017-Master-Schedule.pdf" target="_blank">Global Peace Week Schedule</a>
                        
                        
                        
                        
                        
                        
                        
                        <center></center>
                        
                        
                        
                        <div>
                           
                           <ol><em>
                                 <center>Goals of the Institute are to:</center></em></ol>
                           
                           <div> 
                              <ol>1. Create and teach a <a href="../students/curriculum.html">Peace and Justice Studies</a> curriculum <br> 2. Sponsor and collaborate on peace and justice events<br>
                                 3. Foster a connection with Valencia's A.S. programs in peace studies and conflict
                                 transformation<br>
                                 4. Offer community outreach in peace and justice<br>
                                 5. Engage in realizing Valencia's core competencies<br>
                                 
                              </ol>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/events/past-events.pcf">©</a>
      </div>
   </body>
</html>