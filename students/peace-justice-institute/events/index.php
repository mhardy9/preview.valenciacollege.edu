<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/events/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Events</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>
                           
                           <h2>Events</h2>
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/welcome_to_the_peace_and_justice_institute" target="_blank">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Welcome to the Peace and Justice Institute at East Campus Building 5</span> <br>
                                    
                                    
                                    <span>
                                       <strong>October 25</strong>
                                       
                                       at 11:00 AM</span> 
                                    
                                    <p>Promotion of student programs for the Peace and Justice Institute.</p>
                                    
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/fall_fest_service_project" target="_blank">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Fall Fest Service Project  at Coalition for the Homeless of Central Florida </span> <br>
                                    
                                    
                                    <span>
                                       <strong>October 28</strong>
                                       
                                       at 8:30 AM</span> 
                                    
                                    <p>Join Valencia clubs and groups across the campuses as we come together to bring a
                                       day of fun and fall festivities to the children and families in residency at the Coalition...
                                    </p>
                                    
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/welcome_to_the_peace_and_justice_institute_2768" target="_blank">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Welcome to the Peace and Justice Institute at East Campus Building 5</span> <br>
                                    
                                    
                                    <span>
                                       <strong>November 02</strong>
                                       
                                       at 11:00 AM</span> 
                                    
                                    <p>Promoting student programs at the Peace and Justice Institute</p>
                                    
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/valencia_night_at_the_sikh_society_of_central_florida" target="_blank">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Valencia Night at the Sikh Society of Central Florida at Sikh Society of Central Florida
                                       </span> <br>
                                    
                                    
                                    <span>
                                       <strong>November 03</strong>
                                       
                                       at 5:30 PM</span> 
                                    
                                    <p>Would you like to learn more about Sikhism and explore ways in which our community
                                       can learn from and better connect with each other? Please join Valencia students,
                                       faculty,...
                                    </p>
                                    
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           
                           
                           <div>
                              
                              <a href="http://events.valenciacollege.edu/event/welcome_to_the_peace_and_justice_institute_8444" target="_blank">
                                 
                                 
                                 
                                 
                                 <div>
                                    
                                    <span>Welcome to the Peace and Justice Institute at East Campus Building 5</span> <br>
                                    
                                    
                                    <span>
                                       <strong>November 06</strong>
                                       
                                       at 11:00 AM</span> 
                                    
                                    <p>Promoting student programs at the Peace and Justice Institute</p>
                                    
                                    
                                    
                                 </div>
                                 </a>
                              
                           </div>
                           
                           
                           <a href="http://events.valenciacollege.edu/group/PJI" target="_blank">More PJI Events</a>
                           
                           
                           
                        </section>    
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank">DONATE</a>
                        <a href="../documents/Global-Peace-Week-2017-Master-Schedule.pdf" target="_blank">Global Peace Week Schedule</a>
                        
                        
                        
                        
                        
                        
                        
                        <center></center>
                        
                        
                        
                        <div>
                           
                           <ol><em>
                                 <center>Goals of the Institute are to:</center></em></ol>
                           
                           <div> 
                              <ol>1. Create and teach a <a href="../students/curriculum.html">Peace and Justice Studies</a> curriculum <br> 2. Sponsor and collaborate on peace and justice events<br>
                                 3. Foster a connection with Valencia's A.S. programs in peace studies and conflict
                                 transformation<br>
                                 4. Offer community outreach in peace and justice<br>
                                 5. Engage in realizing Valencia's core competencies<br>
                                 
                              </ol>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/events/index.pcf">©</a>
      </div>
   </body>
</html>