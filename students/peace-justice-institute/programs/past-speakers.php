<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Past Speakers | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/programs/past-speakers.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/programs/">Programs</a></li>
               <li>Past Speakers </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Past Speakers </h2>
                        
                        
                        <div>
                           
                           
                           <hr class="styled_2">
                           
                           <h3>Ambassador Anwarul K. Chowdhury: November 5-7, 2013</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-ambassador-chowdhury.png" title="Ambassador Anwarul K. Chowdhury" class="img-responsive img-circle"><p><strong>Ambassador Anwarul K. Chowdhury</strong>: November 5-7, 2013
                                    Ambassador Anwarul K. Chowdhury, till recently the Senior Special Advisor to the UN
                                    General Assembly President, has devoted many years as an inspirational champion for
                                    sustainable peace and development and ardently advancing the cause of the global movement
                                    for the culture of peace that has energized civil society all over the world.
                                 </p>
                                 
                                 
                                 <p>As a career diplomat, Permanent Representative to United Nations, President of the
                                    UN Security Council, President of UNICEF Board, UN Under-Secretary-General, and recipient
                                    of the U Thant Peace Award, UNESCO Gandhi Gold Medal for Culture of Peace, Spirit
                                    of the UN Award and University of Massachusetts Boston Chancellor's Medal for Global
                                    Leadership for Peace, Ambassador Chowdhury has a wealth of experience in the critical
                                    issues of our time - peace, sustainable development, and human rights.
                                 </p>
                                 
                                 
                                 <p>Ambassador Chowdhury's legacy and leadership in advancing the best interest of the
                                    global community are boldly imprinted in his pioneering initiatives at the United
                                    Nations General Assembly in 1999 for adoption of the landmark Declaration and Programme
                                    of Action on a Culture of Peace and in 1998 for the proclamation of the "International
                                    Decade for Culture of Peace and Nonviolence for the Children of the World (2001-2010)".
                                 </p>
                                 
                                 
                                 <p>Equally pioneering is his initiative in March 2000 as the President of the Security
                                    Council that achieved the political and conceptual breakthrough leading to the adoption
                                    of the groundbreaking UN Security Council Resolution 1325 which for the first time
                                    recognized the role and contribution of women in the area of peace and security.
                                 </p>
                                 
                                 
                                 <p>He served as Ambassador and Permanent Representative of Bangladesh to the United Nations
                                    in New York from 1996 to 2001 and as the Under-Secretary-General and High Representative
                                    of the United Nations, responsible for the most vulnerable countries of the world
                                    from 2002 to 2007.
                                 </p>
                                 
                                 
                                 <p>He has been the Chair of the International Drafting Committee on the Human Right to
                                    Peace, an initiative coordinated from Geneva.
                                 </p>
                                 
                                 
                                 <p>In March 2003, the Soka University of Tokyo, Japan conferred to Ambassador Chowdhury
                                    an Honorary Doctorate for his work on women's issues, child rights and culture of
                                    peace as well as for the strengthening of the United Nations. In May 2012, he received
                                    a Doctor of Humane Letters honoris causa degree from the Saint Peters University of
                                    the United States.
                                 </p>
                                 
                                 
                                 <p>He is the Honorary Chair of the International Day of Peace NGO Committee at the UN,
                                    New York and Chairman of the Global Forum on Human Settlements, both since 2008. He
                                    has been a part of the 12-member Wisdom Council of the Summer of Peace 2012, a world-wide
                                    participatory initiative to advance the Culture of Peace. He is continuing in the
                                    Council for 2013.
                                 </p>
                                 
                                 
                                 <p>He is among the five-member Board of Trustees of the New York City Peace Museum and
                                    a member of the Advisory Council of the National Peace Academy in the US.  He is the
                                    founding Co-Chair of the International Ecological Safety Collaborative Organisation
                                    (IESCO).
                                 </p>
                                 
                                 
                                 <p>He has been decorated by the Government of Burkina Faso in west Africa with the country's
                                    highest honour "L'Ordre Nacionale" in 2007 in Ouagadougou for his championship of
                                    the cause of the most vulnerable countries.
                                    Dr. Chowdhury has structured curricula and taught courses on "The Culture of Peace"
                                    at the Soka University of America and the City University of New York in 2008 and
                                    2009. He also served as an Adjunct Professor at the School of Diplomacy, Seton Hall
                                    University of the United States. He is an Honorary Patron of the Committee on Teaching
                                    About the UN (CTAUN), New York.
                                 </p>
                                 
                                 
                                 <p>Public speaking and advocacy for sustainable peace keep him engaged at the present
                                    time.
                                 </p>    
                                 
                                 
                                 <p>Ambassador Chowdhury visited Valencia in November 2013 to host several speaking engagements
                                    on promoting The Culture of Peace. 
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Angela King: September 28-October 2, 2015</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-angela-king.png" title="Angela King" class="img-responsive img-circle">
                                 
                                 <p><strong>Angela King: September 28-October 2, 2015</strong><br>
                                    Angela King is the Deputy Director of Life After Hate. Growing up in South Florida,
                                    Angela King struggled with her identity. She became confused about the messages she
                                    received from her church and family on issues like sexual identity and racial stereotypes.
                                    Disenfranchised, Angela began acting out and felt welcomed for the first time by a
                                    group of racist skinheads. Though the 1995 Oklahoma City bombing made Angela reconsider
                                    her beliefs, she knew that abandoning her skinhead affiliates would result in retaliation.
                                 </p>
                                 
                                 
                                 <p>Angela was arrested in 1998 and sentenced to six years in prison for her part in an
                                    armed robbery of a Jewish-owned store. Angela was released from prison three years
                                    early, in 2001, for good behavior and cooperation with the authorities. She has since
                                    graduated from the University of Central Florida with an M.A. in Interdisciplinary
                                    Studies. Angela routinely works as a keynote speaker, consultant, and character educator
                                    in schools, communities, religious centers and elsewhere.
                                 </p>
                                 
                                 
                                 <p>Angela King visited Valencia during Global Peace Week in the fall of 2015 for speaking
                                    engagements on Life After Hate.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           <h3>David J. Smith: August 23-24, 2013</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-david-smith.png" title="David J. Smith" class="img-responsive img-circle">
                                 
                                 <p><strong>David J. Smith</strong> is an authority on peacebuilding, conflict resolution, and civic and global education.
                                    He has over 30 years' experience as an educational consultant, lawyer, mediator, college
                                    professor, trainer, senior program officer and manager, and author. David supports
                                    educators and professionals in developing institution-wide initiatives and student
                                    activities promoting civic, peace, and conflict awareness. He works with groups and
                                    individuals in need of career and conflict coaching, mediation, and conflict engagement
                                    assistance and has consulted with nearly 200 colleges around the U.S. and has given
                                    over 500 talks on peacebuilding, conflict resolution, and international education.
                                    He is the president of the Forage Center for Peacebuilding and Humanitarian Education,
                                    Inc., 501c3 not-for-profit that offers experiential learning opportunities for students
                                    and professionals. He has taught at Harford Community College, Goucher College, Georgetown
                                    University, and currently at the School for Conflict Analysis and Resolution at George
                                    Mason University.
                                 </p>
                                 
                                 
                                 <p> He was a U.S. Fulbright Scholar at the University of Tartu (Estonia). He is the recipient
                                    of  the William J. Kreidler Award for Distinguished Service to the field of Conflict
                                    Resolution given by the Association for Conflict Resolution and the inaugural Global
                                    Education Award for Outstanding Voluntary Service Leadership given by the World Affairs
                                    Council/Washington, DC. David is past chair of the Rockville, Maryland Human Rights
                                    Commission, where he received the Community Mediator of the Year Award. While at the
                                    U.S. Institute of Peace his efforts resulted in the expansion of peace and conflict
                                    approaches in higher education, especially at the community college level.  His clients
                                    have included the Fulbright Association, where he led its diversity initiative, and
                                    Street Law, Inc., where he spearheaded its community college efforts.
                                 </p>
                                 
                                 
                                 
                                 <p> Smith is the author of Legal Research and Writing (Cengage, 1996) and editor of Peacebuilding
                                    in Community Colleges: A Teaching Resource (USIP Press, 2013).  He recently published
                                    Peace Jobs: A Student's Guide to Starting a Career Working for Peace (Information
                                    Age Press, 2016). He has published in the Chronicle of Higher Education, Conflict
                                    Resolution Quarterly, Community College Journal, Journal of Peace Education, Huffington
                                    Post, International Herald Tribune, and Baltimore Sun.  He is a graduate of American
                                    University (BA, political science &amp; urban affairs), George Mason University (MS, conflict
                                    analysis &amp; resolution), and the University of Baltimore (JD).
                                 </p>
                                 
                                 
                                 <p>David J. Smith visited Valencia in August 2013 to consult with the Peace and Justice
                                    Institute about Valencia's peace and justice studies curriculum.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Desmond Tutu Peace Foundation: November 2-3, 2015</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-desmund-tutu.png" title="Desmond Tutu" class="img-responsive">
                                 
                                 <p><strong>The Desmond Tutu Peace Foundation</strong> envisions a world in which everyone values human dignity and our interconnectedness.
                                    Real peace will prevail when there is a culture of peace in the USA and around the
                                    world.  That culture originates in the hearts and minds of our youth, the future leaders
                                    and peace-makers of the world. Our programs are based on the teachings of Desmond
                                    Tutu who has dedicated his life to reshaping conversations about peace, equality,
                                    and forgiveness. The philosophy of Ubuntu guides us with its meaning of We are all
                                    connected. What affects one of us affects us all.
                                 </p>
                                 
                                 
                                 <p> ENC 1102: Freshman Composition II: Perspectives on War, Philosophies of Peace<br>
                                    Through documentaries, photography, poetry, art, literature, philosophy, and non-fiction
                                    works,  this online course explores multiple perspectives of warfare and introduces
                                    philosophies of peace to build pathways of healing. Application of skills learned
                                    in ENC 1101 is expected, while there is an emphasis on style; use of library; reading
                                    and evaluating available sources, along with planning, writing, and documenting a
                                    short research paper. This is a Gordon Rule course in which the student is required
                                    to demonstrate college-level writing skills through multiple assignments.
                                 </p>
                                 
                                 
                                 <p>Archbishop Desmond Tutu's granddaughter, a feminist and civil rights activist, visited
                                    Valencia College on November 2-3, 2015 to help the Desmond Tutu Peace Foundation launch
                                    a new initiative, Peace3 aimed at educating young adults about peace and civil rights.
                                    The Peace3 Program, which focuses on educating young adults on Peace Within, Peace
                                    Between and Peace Among, is a series of online and digital experiences. 
                                 </p>
                                 
                                 
                                 <p>In addition to Tutu-Burris, the events featured conversations with Episcopal priest
                                    and gay activist Robert V. Taylor, who also grew up in South Africa, Donna Blackwell
                                    of the Desmond Tutu Peace Foundation, and community activists and student peace leaders
                                    from the Orlando area.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Dr. Peggy McIntosh: October 24-25, 2013 &amp; November 20-21, 2014</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-peggy-mcintosh.png" title="Dr. Peggy McIntosh" class="img-responsive img-circle">
                                 
                                 <p><strong>Dr. Peggy McIntosh</strong>: October 24-25, 2013 and November 20-21, 2014
                                    Internationally recognized for her groundbreaking work in issues of privilege and
                                    oppression, Dr. Peggy McIntosh is associate director of the Wellesley Centers for
                                    Women and founder of the National SEED Project (Seeking Educational Equity and Diversity).
                                    McIntosh is best known for authoring Unpacking the Knapsack of White Privilege (1988),
                                    an article that launched a national discussion now central to dialogues on race. Speaking
                                    about her National Center for Peace Amity Award, McIntosh reflected, "I think that
                                    'amity,' a word of peace, is an appropriate word for the effect that my work has had
                                    on race relations in the U.S., for my analysis is not about shame, blame or guilt.
                                    We did not invent the social systems we were born into. My question is...How can we
                                    use systems of unearned advantage to weaken systems of unearned unearned advantage?"
                                    
                                 </p>
                                 
                                 
                                 <p>Dr. Peggy McIntosh visited Valencia in October 2013 and November 2014 for speaking
                                    engagements and student, faculty/staff, and community workshops on the topics of biases,
                                    white privilege, and diversity. 
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Father Oliver Williams: October 29, 2015</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-oliver-williams.png" title="Father Oliver Williams" class="img-responsive img-circle">
                                 
                                 <p><strong>Father Oliver Williams</strong> is a professor of management in the Notre Dame School of Business, a fellow with
                                    the Kroc Institute for International Peace Studies, and the Director of the Center
                                    for Ethics and Religious Values in Business. He is a Global Leader of social entrepreneurship
                                    and ethical business practices and  has also been on the Board of Directors for the
                                    United Nations Global Compact Foundation. During his visit to Valencia, Fr. Williams
                                    gave a lecture titled, Can You Do Well While Doing Good? The lecture was part of Hesburgh
                                    Lecture Series, in partnership with the Notre Dame Club of Greater Orlando.
                                 </p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           <hr class="styled_2">
                           <h3>Iron Eagle: Numerous Visits to the College</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-ironeagle.png" title="Iron Eagle" class="img-responsive img-circle">
                                 
                                 <p>Iron Eagle is a traditional Sun dancer who studied under Bear Paw (Apache Medicine
                                    Man) &amp; Two Tree (Lakota Medicine Man). His grandfather and mother, both Chiricahua
                                    Apaches, taught him the Traditional and Spiritual ways of the Native American. Through
                                    beautiful Native American music, dance, and teachings, Iron Eagle has left a positive
                                    impact at Valencia each time he visits.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           <h3>Lee Mun Wah: March 30, 2015</h3>
                           
                           <div>
                              
                              <div>
                                 <img src="/students/peace-justice-institute/images/psp-leemun.png" title="Lee Mun Wah" class="img-responsive img-circle"><p><strong>Lee Mun Wah: March 30, 2015</strong><br>
                                    
                                 </p>
                                 
                                 <p>Lee Mun Wah is an internationally renowned Chinese American documentary filmmaker,
                                    author, poet, Asian folkteller, educator, community therapist and master diversity
                                    trainer. For more than 25 years he was a resource specialist and counselor in the
                                    San Francisco Unified School District. He later became a consultant to private schools,
                                    working with students that had severe learning and behavioral issues. Lee Mun Wah
                                    is now the Executive Director of Stirfry Seminars &amp; Consulting, a diversity training
                                    company that provides educational tools and workshops on issues pertaining to cross-cultural
                                    communication and awareness, mindful facilitation, and conflict meditation techniques.
                                 </p> 
                                 
                                 
                                 <p>It is Lee Mun Wah's belief that we cannot wait until tomorrow for some charismatic
                                    leader to appear who will bring us all together. We each must take a stand and personally
                                    participate in this important journey of confronting our fears and beginning a conversation
                                    not only with those we love but also with those we have been taught to fear. We cannot
                                    continue being separate and unequal without there being a cost to each and every generation.
                                    Our survival and the very future of our children depend on all of us embracing our
                                    differences as well as our mutuality. If we can accomplish this in our lifetime, we
                                    can then look back and know that we have found a way to live together authentically
                                    and harmoniously, using and honoring all of our gifts and special contributions. To
                                    Lee Mun Wah, that is the true meaning of multiculturalism. 
                                 </p>  
                                 
                                 
                                 <p>Lee Mun Wah visited Valencia on March 30, 2015 to host speaking engagements for students,
                                    faculty, staff, and community members and to host a film screening of If These Halls
                                    Could Talk.
                                 </p>
                                 
                                 
                                 <p>Iron Eagle: Numerous Visits to the College 
                                    Iron Eagle is a traditional Sun dancer who studied under Bear Paw (Apache Medicine
                                    Man) &amp; Two Tree (Lakota Medicine Man). His grandfather and mother, both Chiricahua
                                    Apaches, taught him the Traditional and Spiritual ways of the Native American. Through
                                    beautiful Native American music, dance, and teachings, Iron Eagle has left a positive
                                    impact at Valencia each time he visits. 
                                 </p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                           
                           
                           
                        </div>     
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/programs/past-speakers.pcf">©</a>
      </div>
   </body>
</html>