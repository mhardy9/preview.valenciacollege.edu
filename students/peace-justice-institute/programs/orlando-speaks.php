<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Orlando Speaks: Community Conversations | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/programs/orlando-speaks.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/programs/">Programs</a></li>
               <li>Orlando Speaks: Community Conversations</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Orlando Speaks: Community Conversations</h2>
                        				
                        <p>In a report issued by the Department of Justice, dialogue is called for as urgently
                           needed to quell civil unrest and serve as a preventative measure to potential violence.
                           Orlando Speaks, designed in partnership with the City of Orlando and facilitated by
                           the Peace and Justice Institute of Valencia College, is an answer to that call. Under
                           the leadership of Mayor Buddy Dyer, and in partnership with Valencia College's Peace
                           and Justice Institute, the City of Orlando is taking steps to bridge divides and create
                           authentic dialogue between citizens and police. In response to concerns about excessive
                           use of force and inequitable policing practices in communities of color and poor communities,
                           the Mayor, Chief of Police and City Commissioners of Orlando agreed to bring citizens
                           and officers together to have the difficult conversation about how we treat each other
                           and how we can strengthen trust.
                        </p>
                        				
                        <p>These custom designed community dialogues, originally launched as Orlando Speaks,
                           and generally referred to as <strong>Our</strong> <strong>Community Speaks</strong>, establish a safe space for dialogue where authentic voices come together to share
                           stories, fears, and concerns, with an eye toward finding common ground among two groups
                           that often struggle together - police and citizens. <strong>Our</strong> <strong>Community Speaks</strong> is a series of interactive workshops that utilizes innovative communication strategies
                           to foster ongoing citizen engagement and conversation between residents, the City
                           and the local Police Department to better serve and celebrate the community's diversity.
                           Workshops are custom designed so as to be timely and meet local community needs.
                        </p>
                        				
                        <p>Recognizing that unconscious bias can be overcome by cross-cultural communication,
                           <strong>Our</strong> <strong>Community Speaks</strong> creates safe spaces for authentic dialogue about our experiences of difference, including
                           but not limited to race. By examining existing cultural and systemic structures, the
                           experience increases understanding and positive change within the community. Having
                           these conversations is critical to moving a City forward, bringing the community closer
                           together, and ensuring that the City remains safe, inclusive and accepting of all.
                        </p>
                        				
                        <p>The goal of <strong>Our</strong> <strong>Community Speaks</strong> is to bring voices to the table that don't often hear one another - and to create
                           a setting where the truth can be heard - through dialogue not debate. Understanding
                           that relationships across lines of difference are essential for the possibility of
                           social transformation, <strong>Our</strong> <strong>Community Speaks</strong> works toward the following goals:<br></p>
                        				
                        <ul>
                           					
                           <li>Create a safe space for open, honest communication</li>
                           					
                           <li>Strengthen interpersonal relationships through the sharing of personal stories and
                              experiences
                           </li>
                           					
                           <li>Develop trust and sensitivity to support interactions with one another</li>
                           					
                           <li>Expand citizen engagement</li>
                           					
                           <li>Increase awareness and understanding of policing practices</li>
                           					
                           <li>Strengthen police legitimacy</li>
                           				
                        </ul>
                        				
                        <p>Surprisingly, many Police are eager to participate in <strong>Our</strong> <strong>Community Speaks</strong>, with volunteer attendance often exceeding the need. Citizens from all walks of life
                           bring their voices to the table, demonstrating communities ready to take action.
                        </p>
                        				
                        <p><strong>Our</strong> <strong>Community Speaks</strong> provides guidelines for talking that invite participants to speak their truth, suspend
                           judgment and turn to wonder, among other practices. The program structures the time
                           in such a way that everyone in the room is an active participant - not a mere bystander
                           listening to others speak. "All People. All Voices. All Matter." is the vision for
                           the work and for the Peace and Justice Institute (PJI). Cooperation and partnership
                           between city government, including the Mayor, the Chief of Police, leadership from
                           the local police department, community stakeholders and PJI fosters a meaningful program
                           with positive outcomes.
                        </p>
                        				
                        <p>Participant feedback from both officers and citizens has been overwhelmingly positive.
                           One resident spoke about the event saying: "I recently had the pleasure of attending
                           "Orlando Speaks" with my coworkers and members of our organization, Organize Now.
                           All of us were incredibly impressed with the presentation and left feeling inspired
                           and ready to take even more action in our community. Thank you for the work that you
                           do in helping to empower our community."
                        </p>
                        				
                        <p><strong>Our</strong> <strong>Community Speaks</strong> creates authentic dialogue, strengthens relationships between police and citizens,
                           and helps communities embrace diversity and foster inclusive excellence among us.
                        </p>
                        				
                        <p>To read more feedback from Orlando Speaks, please <a href="https://www.dropbox.com/s/cun7l8tifq67drp/Orlando%20Speaks%20Participant%20Comments.pdf?dl=0" target="_blank">follow this link.</a></p>
                        				
                        <p><strong>Our</strong> <strong>Community Speaks</strong> is a fee based program. For more information contact Rachel Allen, Peace and Justice
                           Institute Coordinator at <a href="mailto:Rallen39@valenciacollege.">Rallen39@valenciacollege.edu</a> a or call <a href="tel:407-582-2709">407-582-2709</a>.
                        </p>
                        				
                        <p>Learn the story of how 2 women from different backgrounds met and became best friends
                           through Orlando Speaks.
                        </p>
                        				
                        <hr class="styled_2">
                        				
                        <h3>Orlando Speaks (OS) Data</h3>
                        				
                        <p>Percent of participants who agreed or strongly agreed that they felt engaged during
                           the workshop:<br>
                           				<br>
                           				OS1 - 94% OS2 - 97% OS3 - 99.8% OS4 - 98.8% OS5 - 96%
                        </p>
                        				
                        <p>Percent of participants who agreed or strongly agreed that the information covered
                           in the session seemed relevant and pertinent to issues we face today:<br>
                           				<br>
                           				OS1 - 94% OS2 - 94% OS3 - 97.7 OS4 - 95.4% OS5 - 96%
                        </p>
                        				
                        <p>Of the 113 responses from the most recent Orlando Speaks (10-19-2016), <strong>112</strong> participants would recommend this event to others. <strong>This is a 99% positive response.</strong></p>
                        				
                        <p>Throughout four sessions of Orlando Speaks, the most frequently cited words used to
                           describe the event include:
                        </p>
                        				
                        <ul>
                           					
                           <li>Collaborative</li>
                           					
                           <li>Insightful</li>
                           					
                           <li>Engaging</li>
                           					
                           <li>Aware</li>
                           					
                           <li>Informative</li>
                           					
                           <li>Honest</li>
                           					
                           <li>Eye-opening</li>
                           					
                           <li>Understanding</li>
                           					
                           <li>Enlightening</li>
                           				
                        </ul>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/programs/orlando-speaks.pcf">©</a>
      </div>
   </body>
</html>