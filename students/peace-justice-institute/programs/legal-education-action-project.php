<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Legal Education Action Project (LEAP) | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/programs/legal-education-action-project.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/programs/">Programs</a></li>
               <li>Legal Education Action Project (LEAP)</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Legal Education Action Project (LEAP) </h2>
                        
                        <p>“In a hurried world where we sometimes forget to look beyond people’s words and actions
                           and discover the 'why' behind what people say and do, LEAP helped me become a better
                           attorney and in essence a better person, by delving into those 'whys' to understand
                           what is really happening when people say the things they say and do the things they
                           do.” <br> ~ Wendy Mara<br> Attorney and LEAP participant
                        </p>
                        
                        <p><strong>Our Mission</strong></p>
                        
                        <p>LEAP is committed to helping our participants learn effective ways to peacefully navigate
                           conflict in thoughtful, transformative ways.
                        </p>
                        
                        <p><strong>Who we serve</strong></p>
                        
                        <p>LEAP serves legal professionals and those professionals working in legal-related processes
                           such as collaborative divorce and mediation.
                        </p>
                        
                        <p><strong>Why LEAP?</strong></p>
                        
                        <p>As problem-solvers, lawyers are tasked with working through disputes in a system that
                           is inherently adversarial. Quite often the nature of this system elicits behaviors
                           that are not in alignment with the decency, dignity, courtesy, and professionalism
                           The Florida Bar encourages of its members. In this challenging legal landscape, lawyers
                           and paralegals need to learn tools that support the Bar's aspirational goals for conduct.
                        </p>
                        
                        <p>As helpers, legal professionals and those professionals working in legal-related processes
                           are faced with situations that demand an ever-broadening scope of skills in order
                           to facilitate civilized processes and successful outcomes. Increasing levels of dispute
                           in society and within the professional community require conflict navigation expertise
                           not only to support better experiences for clients, but also to support greater wellness
                           and balance for the professional.
                        </p>
                        
                        <p><strong>Quotes from participants</strong></p>
                        
                        <p>“I was inspired to action after attending&nbsp;<em>The Principles of How We Treat Each Other</em>. The instructors were clearly committed to helping advance civility and kindness
                           to each other as well as ourselves – true architects of change!” &nbsp; <br>~ Brenda London, Attorney and Law Professor
                        </p>
                        
                        <p>“I enjoyed the seminar I attended very much as it forced me to put my busy life on
                           pause and meaningfully reflect on topics of the utmost importance.&nbsp; One exercise in
                           particular had a deep impact.&nbsp;We revisited why we went to law school in the first
                           place and the type of professional we wanted to be.&nbsp;Sometimes we lose our focus and
                           this seminar helped me put everything in the proper perspective.” <br>~ Wendy Toscano, Valencia College Paralegal Studies Chair/Professor and Attorney
                        </p>
                        
                        <p>"There just aren't many opportunities for [lawyers] to learn how to be better people.
                           To have people participate and feel is much more meaningful than concepts"<br>~ Jill Kelso, Attorney
                        </p>
                        <br>
                        
                        <p><strong>Courses</strong></p>
                        
                        <ul>
                           
                           <li>Principles for How We Treat Each Other (1-hour Florida Bar CLE Professionalism)</li>
                           
                           <li>Principles for How We Treat Each Other (3-hour Florida Bar CLE Professionalism)</li>
                           
                           <li>Authentic Advocacy (4.5 hours Florida Bar CLE General OR 2.5 Professionalism, 1.0
                              Mental Illness, and .5 Bias Elimination)
                           </li>
                           
                           <li>Navigating Bias Series (Florida Bar CLE pending)
                              
                              <ul>
                                 
                                 <li>Navigating Implicit Bias, April 20, 2018</li>
                                 
                                 <li>Navigating Fundamental Attribution Error, May 18, 2018</li>
                                 
                                 <li>Navigating Confirmation Bias, June 15, 2018</li>
                                 
                              </ul>
                              
                           </li>
                           
                        </ul>
                        
                        <p>For more information on custom-designed courses for your firm or team, please contact
                           Kiki Grossman, J.D., LL.M. at <a href="mailto:kgrossman2@valenciacollege.edu">kgrossman2@valenciacollege.edu</a></p>
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/programs/legal-education-action-project.pcf">©</a>
      </div>
   </body>
</html>