<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Professional Continuing Education | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/programs/continuing-ed.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/programs/">Programs</a></li>
               <li>Professional Continuing Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Professional Continuing Education</h2>
                        
                        <p>Committed to advancing the culture of peace in Central Florida.
                           Valencia provides peace and justice development, education and training, with an emphasis
                           in conflict transformation for individuals and organizations. Our learning opportunities
                           not only educate and develop skills, but also encourage practical application of what
                           is learned.  
                        </p>
                        
                        <p><strong>As a Peace and Justice Practitioner you join a community of individuals who:</strong><br>
                           
                        </p>
                        
                        <ul>
                           
                           <li>Place collaborative relationship as central to the work; focusing on the culture of
                              collaboration
                           </li>
                           
                           <li> Encourage a reflective practice to support meaning and purpose (mindfulness practice,
                              emotional     
                              intelligence)
                           </li>
                           
                           <li> Address conflict as a source and opportunity for growth and transformation</li>
                           
                           <li> Use the tools of dialogue, discussion and conversation (introduces the Principles
                              for How We Treat Each 
                              Other)
                           </li>
                           
                           <li>Support a community of inclusive excellence in which all voices are heard and valued;
                              engage in the
                              exploration of the "other" with an acknowledgement of our inherent interdependence.
                           </li>
                           
                           <li>Recognize that there can be no peace without justice for all.</li>
                           
                        </ul>
                        
                        
                        
                        <p><strong>As a result of these learning experiences you will:</strong><br>
                           
                        </p>
                        
                        <ul>
                           
                           <li>Strengthen relationships through skillful conflict engagement and transformation</li>
                           
                           <li> Increase productivity within your group</li>
                           
                           <li> Build a community of colleagues</li>
                           
                           <li> Become a steward of civic engagement and sustainability</li>
                           
                           <li>Raise awareness and consciousness by awakening to the work of peace and justice</li>
                           
                           <li>Gain the resources needed to be a peacebuilder and leader of social change</li>
                           
                        </ul>
                        
                        
                        
                        
                        <p><strong>Featured Courses:</strong><br>
                           
                        </p>
                        
                        <ul>
                           
                           <li>The Principles for How We Treat Each Other</li>
                           
                           <li>Working with Conflict</li>
                           
                           <li>Conversations in Inclusiveness: Understanding Inclusive Excellence</li>
                           
                           <li>The Danger of a Single Story: Racial Identity Development</li>
                           
                           <li>Creating Safe Space for Dialogue</li>
                           
                           <li>Mindfulness and the Reflective Practitioner</li>
                           
                        </ul>
                        
                        
                        <a href="https://valenciacollege.edu/pji/community/">Featured Programs: Our Community Speaks</a> 
                        
                        
                        
                        
                        
                     </div> 
                     
                  </div>
                  
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/programs/continuing-ed.pcf">©</a>
      </div>
   </body>
</html>