<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace &amp; Justice Institute  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia Practices of peace and nonviolence through our dedicated partnerships with local organizations">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Peace &amp; Justice Institute </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               
               <div class="container margin_60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Programs</h2>
                        
                        <p>practices of peace and nonviolence through our dedicated partnerships with local organizations</p>
                        
                        
                        <p>The Peace and Justice Institute offers a variety of programs to help create an inclusive
                           and caring
                           community built on the pillars of respect and nonviolence. PJI continues to partner
                           with Valencia's
                           Continuing Education program to afford members of the community, faculty, and staff
                           opportunities to
                           further their knowledge and understanding of peace studies and community building.
                        </p>
                        
                        <p>Every fall, PJI hosts Global Peace Week, a celebration of peace education, music,
                           arts, and crafts. This
                           week offers those in attendance the opportunity to learn about various peace practices,
                           engage in the
                           exploration of self, focus on the culture of collaboration and support a community
                           of inclusive excellence
                           where all voices are heard and valued.
                        </p>
                        
                        
                        <p>Each spring, PJI hosts our week long Conversation on Justice, a program that allows
                           our community to
                           discuss contentious issues - ranging from violence, immigration and economic inequality
                           to food
                           insecurity, race, and sexual orientation. Through the power of transformative stories,
                           facilitated-discussions, art projects, award-winning films, and more these critical
                           conversations are able
                           to be addressed with the ultimate goal of promoting a culture of peace and understanding.
                        </p>
                        
                        
                        <p>PJI provides opportunities for the community to empower others and apply the practices
                           of peace and
                           nonviolence through our dedicated partnerships with local organizations that aims
                           to address inequity and
                           work toward conflict resolution. Through these community partnerships the mission
                           of the Peace and Justice
                           Institute remains steadfast and provides the opportunity to extend our practices into
                           our local and global
                           communities.
                        </p>
                        
                        <p>&nbsp;</p>
                        
                        <div class="row">
                           
                           <div class="col-md-5 col-sm-5">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Continuing Ed</a></li>
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Global Peace Week</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Conversations on Justice</a></li>
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Global Outreach</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <hr>
                     
                     <div>
                        
                        <h3>Continuing Ed</h3>
                        
                        <p>Professional Continuing Education</p>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Committed to advancing the culture of peace in Central Florida. Valencia provides
                           peace and justice
                           development, education and training, with an emphasis in conflict transformation for
                           individuals and
                           organizations. Our learning opportunities not only educate and develop skills, but
                           also encourage
                           practical application of what is learned. 
                        </p>
                        
                        <p><strong>As a Peace and Justice Practitioner you join a community of individuals who:</strong><br>
                           
                        </p>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <ul class="list_style_1">
                                 
                                 <li>Place collaborative relationship as central to the work; focusing on the culture of
                                    collaboration
                                    
                                 </li>
                                 
                                 <li>Encourage a reflective practice to support meaning and purpose (mindfulness practice,
                                    emotional
                                    intelligence)
                                    
                                 </li>
                                 
                                 <li>Address conflict as a source and opportunity for growth and transformation</li>
                                 
                                 <li>Use the tools of dialogue, discussion and conversation (introduces the Principles
                                    for How We
                                    Treat Each Other)
                                    
                                 </li>
                                 
                                 <li>Support a community of inclusive excellence in which all voices are heard and valued;
                                    engage in
                                    the exploration of the "other" with an acknowledgement of our inherent interdependence.
                                    
                                 </li>
                                 
                                 <li>Recognize that there can be no peace without justice for all.</li>
                                 
                              </ul>
                              
                              <p><strong>As a result of these learning experiences you will:</strong><br>
                                 
                              </p>
                              
                              <ul class="list_style_1">
                                 
                                 <li>Place collaborative relationship as central to the work; focusing on the culture of
                                    collaboration
                                 </li>
                                 
                                 <li>Encourage a reflective practice to support meaning and purpose (mindfulness practice,
                                    emotional
                                    intelligence)
                                    
                                 </li>
                                 
                                 <li>Address conflict as a source and opportunity for growth and transformation</li>
                                 
                                 <li>Use the tools of dialogue, discussion and conversation (introduces the Principles
                                    for How We Treat
                                    Each Other)
                                    
                                 </li>
                                 
                                 <li>Support a community of inclusive excellence in which all voices are heard and valued;
                                    engage in the
                                    exploration of the "other" with an acknowledgement of our inherent interdependence.
                                    
                                 </li>
                                 
                                 <li>Recognize that there can be no peace without justice for all.</li>
                                 
                              </ul>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                     <hr>
                     
                     <div>
                        
                        <h3>Global Peace Week</h3>
                        
                        <p>International Day of Peace with Global Peace Week </p>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Each September, the Peace and Justice Institute celebrates International Day of Peace
                           with Global Peace Week
                           festivities and events including peace education, music, arts and crafts, guest speakers,
                           and free film
                           screenings in partnership with the Global Peace Film Festival.
                        </p>
                        <a href="https://www.dropbox.com/s/jm0yujvriibokpe/Global%20Peace%20Week%202016%20Master%20Schedule%20of%20Events.pdf?dl=0" target="_blank"><img src="/_resources/img/students/peace-justice-institute/globalpeaceweek.png" alt="Global Peace Week" width="200" height="200" border="0" align="right" title="Global Peace Week"></a>
                        
                        <p>The United Nations sanctioned International Peace Day is September 21st. The mission
                           of International Peace
                           Day is to have at least one non-violent, cease-fire day throughout the world. Visit
                           Peace One Day for
                           background on how International Peace Day came into being and for more information
                           on what you can do to help
                           accomplish a global change.
                        </p>
                        
                        <p>Faculty, staff and students are invited to participate in the creation, planning and
                           implementation of Global
                           Peace Week.
                        </p>
                        
                        <p><strong>For the full schedule of events taking place during Global Peace Week, click the peace
                              sign image or
                              <a href="https://www.dropbox.com/s/jm0yujvriibokpe/Global%20Peace%20Week%202016%20Master%20Schedule%20of%20Events.pdf?dl=0" target="_blank">click here</a>. </strong></p>
                        
                        <p align="center"><a href="https://www.dropbox.com/s/jm0yujvriibokpe/Global%20Peace%20Week%202016%20Master%20Schedule%20of%20Events.pdf?dl=0" target="_blank"><img src="images/ClickHereButton.PNG" alt="Button for Global Peace Week Schedule" width="225" height="119" border="0"></a></p>
                        
                     </div>
                     
                     
                     <hr>
                     
                     <div>
                        
                        <h3>Downloads</h3>
                        
                        <p>Mussum ipsum cacilds, vidis litro abertis.</p>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p class="add_bottom_30">Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te
                           pri facete latine salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne
                           his sint graeco detraxit,
                           posse exerci volutpat has in.
                        </p>
                        
                        <div class="row">
                           
                           <div class="col-md-5 col-sm-5">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Prospectus</a></li>
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Admission form</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                           <div class="col-md-6 col-sm-6">
                              
                              <ul class="list-unstyled">
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Lesson Time table</a></li>
                                 
                                 <li><a href="#0" class="button_download hvr-sweep-to-right">Audio lesson sample</a></li>
                                 
                              </ul>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <hr>
                     
                     <div>
                        
                        <h3>Time table</h3>
                        
                        <p>Mussum ipsum cacilds, vidis litro abertis.</p>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Zril causae ancillae sit ea. Dicam veritus mediocritatem sea ex, nec id agam eius.
                           Te pri facete latine
                           salutandi, scripta mediocrem et sed, cum ne mundi vulputate. Ne his sint graeco detraxit,
                           posse exerci
                           volutpat has in.
                        </p>
                        
                        <table class="table table table-striped cart-list add_bottom_30">
                           
                           <thead>
                              
                              <tr>
                                 
                                 <th> Day</th>
                                 
                                 <th> Lessons</th>
                                 
                                 <th> Workshops</th>
                                 
                                 <th> Group session</th>
                                 
                                 <th> Exams</th>
                                 
                              </tr>
                              
                           </thead>
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <td> Monday</td>
                                 
                                 <td> 09.00am - 11.00am</td>
                                 
                                 <td> 11.00am - 12.00am</td>
                                 
                                 <td> 02.00pm - 04.00pm</td>
                                 
                                 <td> -</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> Tuesday</td>
                                 
                                 <td> 09.00am - 11.00am</td>
                                 
                                 <td> 11.00am - 12.00am</td>
                                 
                                 <td> 02.00pm - 04.00pm</td>
                                 
                                 <td> -</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> Wednesday</td>
                                 
                                 <td> 09.00am - 11.00am</td>
                                 
                                 <td> 11.00am - 12.00am</td>
                                 
                                 <td> -</td>
                                 
                                 <td> 02.00pm - 04.00pm</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> Thursday</td>
                                 
                                 <td> 09.00am - 11.00am</td>
                                 
                                 <td> 11.00am - 12.00am</td>
                                 
                                 <td> 02.00pm - 04.00pm</td>
                                 
                                 <td> -</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td> Friday</td>
                                 
                                 <td> 09.00am - 11.00am</td>
                                 
                                 <td> 11.00am - 12.00am</td>
                                 
                                 <td> -</td>
                                 
                                 <td> 02.00pm - 04.00pm</td>
                                 
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                     </div>
                     
                  </div>
                  
               </div>
               
               <div class="container margin_60">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <div class="box_style_1">
                           
                           <div>
                              
                              <h3>News</h3>
                              
                              <p></p>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p></p>
                              
                              <div class="row">
                                 
                                 <div class="col-md-12">
                                    
                                    <p>The Peace and Justice Institute bi-annually publishes a newsletter with articles written
                                       by
                                       distinguished guests, faculty, and students. The newsletter details the work of the
                                       Institute and
                                       serves as a historical document for the growth and development of the work over time.
                                       The publication
                                       provides information on events from the academic semester and recognizes the contributions
                                       of the many
                                       individuals that bring to the life the mission of PJI.
                                    </p>
                                    
                                    
                                    <p>Additionally, the Peace and Justice Institute is often recognized by the community
                                       in a variety of
                                       media mentions. PJI media mentions include a collection of articles, videos, and more.
                                    </p>
                                    
                                    
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                           <hr>
                           
                           <div>
                              
                              <h3>Newsletter</h3>
                              
                              <p></p>
                              
                           </div>
                           
                           <div class="wrapper_indent">
                              
                              <p></p>
                              
                              <div class="row">
                                 
                                 <div class="col-md-6">
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter14.pdf" target="_blank">Volume
                                             7, No. 1 </a><br> Spring 2016
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter13.pdf" target="_blank">Volume
                                             6, No. 2 </a> <br> Fall 2015
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter12.pdf" target="_blank">Volume
                                             6, No. 1</a> <br> Spring 2015
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter11.pdf" target="_blank">Volume
                                             5, No. 2</a><br> Fall 2014
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter10.pdf" target="_blank">Volume
                                             5, No. 1</a><br> Spring 2014
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter9.pdf" target="_blank">Volume
                                             4, No. 2</a><br> Fall 2013
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter8A.pdf" target="_blank">Volume
                                             4, No. 1</a><br> Spring 2013
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter7.pdf" target="_blank">Volume
                                             3, No. 2</a><a href="/documents/employees/peace-justice-institute/PJINewsletter7.pdf" target="_blank"><br> </a>Fall 2012
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                                 <div class="col-md-6">
                                    
                                    <ul class="list_style_1">
                                       
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/Vol3No1.pdf" target="_blank">Volume 3, No.
                                             1</a><a href="/documents/employees/peace-justice-institute/Volume3-1.pdf" target="_blank"><br> </a>Spring
                                          2012
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/PJINewsletter5.pdf" target="_blank">Volume
                                             2, No. 3</a><a href="/documents/employees/peace-justice-institute/Volume2-3.pdf" target="_blank"></a><a href="/documents/employees/peace-justice-institute/Volume2-3.pdf" target="_blank"><br> </a>Fall 2011
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/July2011.pdf" target="_blank">Volume 2, No.
                                             2</a><a href="/documents/employees/peace-justice-institute/July2011.pdf" target="_blank"></a><a href="/documents/employees/peace-justice-institute/July2011.pdf" target="_blank"><br> </a>Summer
                                          2011
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/Feb2011.pdf" target="_blank">Volume 2, No.
                                             1</a><a href="/documents/employees/peace-justice-institute/Volume2-1.pdf" title="Volume 2, No. 1" target="_blank"><br> </a>Spring 2011
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/Volume1-2.pdf" title="Volume 1, No. 2" target="_blank">Volume 1, No. 2</a><br> Fall 2010
                                          
                                       </li>
                                       
                                       <li>
                                          <a href="/documents/employees/peace-justice-institute/Volume1-1.pdf" title="Volume 1, No. 1" target="_blank">Volume 1, No. 1</a><br> Spring 2009
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
               </div>
               
               
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div>
                           
                           <h3>News</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>The Peace and Justice Institute bi-annually publishes a newsletter with articles written
                                    by
                                    distinguished guests, faculty, and students. The newsletter details the work of the
                                    Institute and
                                    serves as a historical document for the growth and development of the work over time.
                                    The publication
                                    provides information on events from the academic semester and recognizes the contributions
                                    of the many
                                    individuals that bring to the life the mission of PJI.
                                 </p>
                                 
                                 
                                 <p>Additionally, the Peace and Justice Institute is often recognized by the community
                                    in a variety of
                                    media mentions. PJI media mentions include a collection of articles, videos, and more.
                                 </p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr>
                        
                        <div>
                           
                           <h3>Newsletter</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter14.pdf" target="_blank">Volume
                                          7, No. 1 </a><br> Spring 2016
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter13.pdf" target="_blank">Volume
                                          6, No. 2 </a> <br> Fall 2015
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter12.pdf" target="_blank">Volume
                                          6, No. 1</a> <br> Spring 2015
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter11.pdf" target="_blank">Volume
                                          5, No. 2</a><br> Fall 2014
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter10.pdf" target="_blank">Volume
                                          5, No. 1</a><br> Spring 2014
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter9.pdf" target="_blank">Volume
                                          4, No. 2</a><br> Fall 2013
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter8A.pdf" target="_blank">Volume
                                          4, No. 1</a><br> Spring 2013
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter7.pdf" target="_blank">Volume
                                          3, No. 2</a><a href="/documents/employees/peace-justice-institute/PJINewsletter7.pdf" target="_blank"><br> </a>Fall 2012
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <ul class="list_style_1">
                                    
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/Vol3No1.pdf" target="_blank">Volume 3, No.
                                          1</a><a href="/documents/employees/peace-justice-institute/Volume3-1.pdf" target="_blank"><br> </a>Spring
                                       2012
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/PJINewsletter5.pdf" target="_blank">Volume
                                          2, No. 3</a><a href="/documents/employees/peace-justice-institute/Volume2-3.pdf" target="_blank"></a><a href="/documents/employees/peace-justice-institute/Volume2-3.pdf" target="_blank"><br> </a>Fall 2011
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/July2011.pdf" target="_blank">Volume 2, No.
                                          2</a><a href="/documents/employees/peace-justice-institute/July2011.pdf" target="_blank"></a><a href="/documents/employees/peace-justice-institute/July2011.pdf" target="_blank"><br> </a>Summer
                                       2011
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/Feb2011.pdf" target="_blank">Volume 2, No.
                                          1</a><a href="/documents/employees/peace-justice-institute/Volume2-1.pdf" title="Volume 2, No. 1" target="_blank"><br> </a>Spring 2011
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/Volume1-2.pdf" title="Volume 1, No. 2" target="_blank">Volume 1, No. 2</a><br> Fall 2010
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/employees/peace-justice-institute/Volume1-1.pdf" title="Volume 1, No. 1" target="_blank">Volume 1, No. 1</a><br> Spring 2009
                                       
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/programs.pcf">©</a>
      </div>
   </body>
</html>