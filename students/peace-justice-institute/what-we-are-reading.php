<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>What We Are Reading | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/what-we-are-reading.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>What We Are Reading</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>What We Are Reading</h2>
                        
                        <div>
                           
                           
                           <div>
                              
                              <p><strong>The Art and Spirit of Leadership </strong><br>
                                 <em>by Judith Brown</em><br>
                                 <a href="https://www.amazon.com/gp/product/1466910488/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=1466910488&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=f76ad3b51abb503622a44e2272c1c8e9" target="_blank"><img alt="The Art and Spirit of Leadership" border="0" height="200" src="/students/peace-justice-institute/images/wwar-judybrown.jpg" title="The Art and Spirit of Leadership" width="120"></a></p>
                              
                              
                              <p><strong>PEACEBUILDING IN COMMUNITY COLLEGES: A Teaching Resource</strong><br>
                                 <em>by David J Smith </em><br>
                                 <a href="https://www.amazon.com/gp/product/B00GBKZXS2/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B00GBKZXS2&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=f87d7aea707fcae35c8d58dbf120623c" target="_blank"><img alt="PEACEBUILDING IN COMMUNITY COLLEGES: A Teaching Resource" border="0" height="200" src="/students/peace-justice-institute/images/wwar-davidjsmith.jpg" title="PEACEBUILDING IN COMMUNITY COLLEGES: A Teaching Resource" width="120"></a></p>
                              
                              
                              <p><strong>The Peace Book: 108 Simple Ways to Create a More Peaceful World</strong><br>
                                 <em>by Louise Diamond</em><br>
                                 <a href="https://www.amazon.com/gp/product/1573247707/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=1573247707&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=b7f8abc7fde864c65211e138d113b139" target="_blank"><img alt="The Peace Book: 108 Simple Ways to Create a More Peaceful World" border="0" height="200" src="/students/peace-justice-institute/images/wwar-louisediamond.jpg" title="The Peace Book: 108 Simple Ways to Create a More Peaceful World" width="120"></a></p>
                              
                              
                              <p><strong>Twelve Steps to a Compassionate Life</strong><br>
                                 <em>by Karen Armstrong </em><br>
                                 <a href="https://www.amazon.com/gp/product/B003WUYPBA/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B003WUYPBA&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=a98d5b1e18514b9cfd059d4ca1a8806f" target="_blank"><img alt="Twelve Steps to a Compassionate Life" border="0" height="200" src="/students/peace-justice-institute/images/wwar-karenarmstrong.jpg" title="Twelve Steps to a Compassionate Life" width="120"></a></p>
                              
                              
                              <p><strong>The Search for a Nonviolent Future</strong><br>
                                 <em>by by Michael Nagler</em><br>
                                 <a href="https://www.amazon.com/gp/product/B004D4Y3CA/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B004D4Y3CA&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=ea765329088f967eaa82094bbaea9142" target="_blank"><img alt="The Search for a Nonviolent Future: A Promise of Peace for Ourselves, Our Families, and Our World " border="0" height="200" src="/students/peace-justice-institute/images/wwar-michaelnagler.jpg" title="The Search for a Nonviolent Future: A Promise of Peace for Ourselves, Our Families, and Our World " width="120"></a></p>
                              
                              
                              
                              <p><strong>The New Jim Crow: Mass Incarceration in the Age of Colorblindness </strong><br>
                                 <em>by Michelle Alexander </em><br>
                                 <a href="https://www.amazon.com/gp/product/B0067NCQVU/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B0067NCQVU&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=05ebdb82e794f10649bbc76f1416e301" target="_blank"><img alt="The New Jim Crow: Mass Incarceration in the Age of Colorblindness " border="0" height="200" src="/students/peace-justice-institute/images/wwar-michellealexander.jpg" title="The New Jim Crow: Mass Incarceration in the Age of Colorblindness " width="120"></a></p>
                              
                              
                              
                           </div>
                           
                           <div>
                              
                              <p><strong>I'm not leaving</strong><br>
                                 <em>by Carl Wilkens</em><br>
                                 <a href="https://www.amazon.com/gp/product/B0068K6SIE/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B0068K6SIE&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=5509d836cfac51829b873227b24f2e06" target="_blank"><img alt="I'm not leaving" border="0" height="200" src="/students/peace-justice-institute/images/wwar-carlwilkens.jpg" title="I'm not leaving" width="120"></a></p>
                              
                              
                              <p><strong>Healing the Heart of Democracy: The Courage to Create a Politics Worthy of the Human
                                    Spirit</strong><br>
                                 <em>by Parker J. Palmer</em><br>
                                 <a href="https://www.amazon.com/gp/product/B018ZXYXCO/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B018ZXYXCO&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=826093d30cfefec7141c10469dbadaa2" target="_blank"><img alt="Healing the Heart of Democracy: The Courage to Create a Politics Worthy of the Human Spirit" border="0" height="200" src="/students/peace-justice-institute/images/wwar-parkerjpalmer.jpg" title="Healing the Heart of Democracy: The Courage to Create a Politics Worthy of the Human Spirit" width="120"></a></p>
                              
                              
                              <p><strong>And Still Peace Did Not Come: A Memoir of Reconciliation </strong><br>
                                 <em>by Agnes Fallah Kamara-Umunna</em><br>
                                 <a href="https://www.amazon.com/gp/product/B004Q9TYWG/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B004Q9TYWG&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=6698d4fff7a12b96b6c5045ed10b7087" target="_blank"><img alt="And Still Peace Did Not Come: A Memoir of Reconciliation" border="0" height="200" src="/students/peace-justice-institute/images/wwar-agnesfallah.jpg" title="And Still Peace Did Not Come: A Memoir of Reconciliation " width="120"></a></p>
                              
                              
                              
                              <p><strong>The Little Book of Conflict Transformation </strong><br>
                                 <em>by John Paul Lederach</em><br>
                                 <a href="https://www.amazon.com/gp/product/B00RW2UXK6/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B00RW2UXK6&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=3b36ca051745d83ddec06c9f698bc55a" target="_blank"><img alt="The Little Book of Conflict Transformation" border="0" height="200" src="/students/peace-justice-institute/images/wwar-johnpaullederach.jpg" title="The Little Book of Conflict Transformation" width="120"></a></p>
                              
                              
                              
                              <p><strong>The Courage to Teach </strong><br>
                                 <em>by Parker J. Palmer</em><br>
                                 <a href="https://www.amazon.com/gp/product/B003GYEGW0/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=B003GYEGW0&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=2eea37348f00c79b9f55bf2bb7db4d72" target="_blank"><img alt="The Courage to Teach: Exploring the Inner Landscape of a Teacher's Life" border="0" height="200" src="/students/peace-justice-institute/images/wwar-parkerjpalmer1.jpg" title="The Courage to Teach: Exploring the Inner Landscape of a Teacher's Life" width="120"></a></p>
                              
                              
                              
                              
                              
                              <p><strong>Peace Jobs: A Student's Guide to Starting a Career Working for Peace </strong><br>
                                 <em>by David J. Smith</em><br>
                                 <a href="https://www.amazon.com/gp/product/1681233304/ref=as_li_tl?ie=UTF8&amp;camp=1789&amp;creative=9325&amp;creativeASIN=1681233304&amp;linkCode=as2&amp;tag=vcpandj-20&amp;linkId=4334b48cb32318123ccca11323f8a479" target="_blank"><img alt="Peace Jobs: A Studentï¿½s Guide to Starting a Career Working for Peace " border="0" height="200" src="/students/peace-justice-institute/images/wwar-davidjsmith1.jpg" title="Peace Jobs: A Studentï¿½s Guide to Starting a Career Working for Peace " width="120"></a></p>
                              
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/what-we-are-reading.pcf">©</a>
      </div>
   </body>
</html>