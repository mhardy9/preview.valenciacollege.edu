<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Programs  | Valencia College</title>
      <meta name="Description" content="Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful&amp;amp;#10;      environment on campus and within our community - one where conflict leads to growth and transformation rather than&amp;amp;#10;      violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/community-programs/programs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/community-programs/">Community Programs</a></li>
               <li>Programs </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h2>Programs</h2>
                           
                           <p>Practices of peace and nonviolence through our dedicated partnerships with local organizations</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>The Peace and Justice Institute offers a variety of programs to help create an inclusive
                                    and caring
                                    community built on the pillars of respect and nonviolence. PJI continues to partner
                                    with Valencia's
                                    Continuing Education program to afford members of the community, faculty, and staff
                                    opportunities to
                                    further their knowledge and understanding of peace studies and community building.
                                 </p>
                                 
                                 <p>Every fall, PJI hosts Global Peace Week, a celebration of peace education, music,
                                    arts, and crafts. This
                                    week offers those in attendance the opportunity to learn about various peace practices,
                                    engage in the
                                    exploration of self, focus on the culture of collaboration and support a community
                                    of inclusive excellence
                                    where all voices are heard and valued.
                                 </p>
                                 
                                 
                                 <p>Each spring, PJI hosts our week long Conversation on Justice, a program that allows
                                    our community to
                                    discuss contentious issues - ranging from violence, immigration and economic inequality
                                    to food
                                    insecurity, race, and sexual orientation. Through the power of transformative stories,
                                    facilitated-discussions, art projects, award-winning films, and more these critical
                                    conversations are able
                                    to be addressed with the ultimate goal of promoting a culture of peace and understanding.
                                 </p>
                                 
                                 
                                 <p>PJI provides opportunities for the community to empower others and apply the practices
                                    of peace and
                                    nonviolence through our dedicated partnerships with local organizations that aims
                                    to address inequity and
                                    work toward conflict resolution. Through these community partnerships the mission
                                    of the Peace and Justice
                                    Institute remains steadfast and provides the opportunity to extend our practices into
                                    our local and global
                                    communities.
                                 </p>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Professional Continuing Education</h3>
                           
                           <p>Continuing Ed</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>Committed to advancing the culture of peace in Central Florida. Valencia provides
                                    peace and justice
                                    development, education and training, with an emphasis in conflict transformation for
                                    individuals and
                                    organizations. Our learning opportunities not only educate and develop skills, but
                                    also encourage
                                    practical application of what is learned. 
                                 </p>
                                 
                                 <p><strong>As a Peace and Justice Practitioner you join a community of individuals who:</strong><br>
                                    
                                 </p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Place collaborative relationship as central to the work; focusing on the culture of
                                       collaboration
                                       
                                    </li>
                                    
                                    <li> Encourage a reflective practice to support meaning and purpose (mindfulness practice,
                                       emotional
                                       intelligence)
                                       
                                    </li>
                                    
                                    <li> Address conflict as a source and opportunity for growth and transformation</li>
                                    
                                    <li> Use the tools of dialogue, discussion and conversation (introduces the Principles
                                       for How We
                                       Treat Each Other)
                                       
                                    </li>
                                    
                                    <li>Support a community of inclusive excellence in which all voices are heard and valued;
                                       engage in
                                       the exploration of the "other" with an acknowledgement of our inherent interdependence.
                                       
                                    </li>
                                    
                                    <li>Recognize that there can be no peace without justice for all.</li>
                                    
                                 </ul>
                                 
                                 <p></p>
                                 
                                 
                                 <p><strong>As a result of these learning experiences you will:</strong><br>
                                    
                                 </p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Strengthen relationships through skillful conflict engagement and transformation</li>
                                    
                                    <li> Increase productivity within your group</li>
                                    
                                    <li> Build a community of colleagues</li>
                                    
                                    <li> Become a steward of civic engagement and sustainability</li>
                                    
                                    <li>Raise awareness and consciousness by awakening to the work of peace and justice</li>
                                    
                                    <li>Gain the resources needed to be a peacebuilder and leader of social change</li>
                                    
                                 </ul>
                                 
                                 <p></p>
                                 
                                 
                                 
                                 <p><strong>Featured Courses:</strong><br>
                                    
                                 </p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>The Principles for How We Treat Each Other</li>
                                    
                                    <li>Working with Conflict</li>
                                    
                                    <li>Conversations in Inclusiveness: Understanding Inclusive Excellence</li>
                                    
                                    <li>The Danger of a Single Story: Racial Identity Development</li>
                                    
                                    <li>Creating Safe Space for Dialogue</li>
                                    
                                    <li>Mindfulness and the Reflective Practitioner</li>
                                    
                                 </ul>
                                 
                                 <p></p>
                                 
                                 <a href="/STUDENTS/offices-services/peace-justice-institute/community-programs/index.html">Featured Programs: Our
                                    Community Speaks</a>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Global Peace Week</h3>
                           
                           <p>Fall 2016 Global Peace Week:</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>September 19th-23rd </p>
                                 
                                 <p>Each September, the Peace and Justice Institute celebrates International Day of Peace
                                    with Global
                                    Peace Week festivities and events including peace education, music, arts and crafts,
                                    guest speakers,
                                    and free film screenings in partnership with the Global Peace Film Festival.
                                 </p>
                                 <a href="https://www.dropbox.com/s/jm0yujvriibokpe/Global%20Peace%20Week%202016%20Master%20Schedule%20of%20Events.pdf?dl=0" target="_blank"><img align="right" alt="Global Peace Week" border="0" height="200" src="/_resources/img/students/peace-justice-institute/globalpeaceweek.png" title="Global Peace Week" width="200"></a>
                                 
                                 <p>The United Nations sanctioned International Peace Day is September 21st. The mission
                                    of International
                                    Peace Day is to have at least one non-violent, cease-fire day throughout the world.
                                    Visit Peace One
                                    Day for background on how International Peace Day came into being and for more information
                                    on what you
                                    can do to help accomplish a global change.
                                 </p>
                                 
                                 
                                 <p>Faculty, staff and students are invited to participate in the creation, planning and
                                    implementation
                                    of Global Peace Week.
                                 </p>
                                 
                                 <p><strong><a href="https://www.dropbox.com/s/jm0yujvriibokpe/Global%20Peace%20Week%202016%20Master%20Schedule%20of%20Events.pdf?dl=0" target="_blank">For the full schedule of events taking place during Global Peace Week</a></strong></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Conversation on Justice </h3>
                           
                           <p>Spring 2017 Conversation on Justice:</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p> January 30th-February 3rd </p>
                                 
                                 <p>Join the Peace and Justice Institute annually in January for our Conversation on Justice.
                                    <img align="right" alt="Conversation on Justice " height="200" src="/_resources/img/students/peace-justice-institute/conversationonjustice.png" width="200"></p>
                                 
                                 <p>Covering many of today's hot-button issues - ranging from violence, immigration and
                                    economic
                                    inequality to food insecurity, race and sexual orientation - each event, through the
                                    power of
                                    transformative stories, facilitated discussions, award-winning films, art projects
                                    and more, delves
                                    into critical conversations whose goal is to promote a culture of peace and understanding
                                    and mend the
                                    fabric of our humanity. 
                                 </p>
                                 
                                 
                                 <p>The Conversation on Justice is sponsored by the Peace and Justice Institute with funding
                                    support from
                                    Student Development.
                                 </p>
                                 
                                 <p><strong><a href="https://www.dropbox.com/s/8mqk495v86w8q2i/Conversation%20on%20Justice%202017%20Master%20Schedule.pdf?dl=0" target="_blank">For the full schedule of events taking place during the Conversation on Justice</a> </strong></p>
                                 
                                 
                                 <p>&nbsp;</p>
                                 
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Global Outreach</h3>
                           
                           <p>Each year the Peace and Justice Institute, in partnership with Saint Margaret Mary
                              Church in Winter
                              Park
                           </p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p><img align="right" alt="Global Outreach " height="200" src="/_resources/img/students/peace-justice-institute/2015event.png" width="200"></p>
                                 
                                 <p>Konferans Lape Jakmel<br> Jacmel Peace Conference<br> August 20-22, 2015<br>
                                    
                                 </p>
                                 
                                 <p>Each year the Peace and Justice Institute, in partnership with Saint Margaret Mary
                                    Church in Winter
                                    Park, Florida, partner to bring a three day peace conference to Jacmel Haiti. Principles
                                    of respect
                                    and nonviolence, conflict resolution skills, and meditation are highlights of the
                                    three day Konferans
                                    Lape Jakmel, with the goal of supporting a sustainable peace program for the children
                                    and citizens of
                                    Haiti.
                                 </p>
                                 
                                 
                                 <p>In the past up to sixty-five leaders from diverse parishes and churches from the Jacmel
                                    region and
                                    Cite de Soleil's Sakala have come together for this annual event. The Desmond Tutu
                                    Peace Foundation
                                    has also been represented at the conference. As a result of the peace conferences,
                                    there has been the
                                    establishment of the Jacmel Peace Commission with leadership positions and a paid
                                    director in place.
                                    The Commission meets multiple times throughout the year and is developing structures
                                    for its
                                    organization, an online platform, and plans to develop new peace groups in other parishes
                                    around
                                    Jacmel. 
                                 </p>
                                 
                                 
                                 
                                 <center>
                                    
                                    <p><a href="https://vimeo.com/137661474">Peace Conference Jacmel, Haiti 2015</a> from <a href="https://vimeo.com/williejallenjr">Willie J. Allen Jr.</a> on <a href="https://vimeo.com">Vimeo</a></p>
                                    
                                 </center>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Leadership Fellows</h3>
                           
                           <p>The Leadership Fellow position offers students the opportunity to become more highly
                              engaged with the
                              work of the Institute.
                           </p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>The Peace and Justice Leadership Fellow position offers students the opportunity to
                                    become more
                                    highly engaged with the work of the Institute. A prerequisite for the position is
                                    completion of the
                                    Peace and Justice Distinction, participation as a Peace and Justice Ambassador, or
                                    completion of a
                                    Peace Studies Course.
                                 </p>
                                 
                                 
                                 <p>Becoming a Leadership Fellow is determined through an interview process. The position
                                    covers the span
                                    of one academic term. Fellows may serve more than one academic term.
                                 </p>
                                 
                                 
                                 <p>Fellows have the option of earning up to 4 credit hours of Service Learning or a $500.00
                                    stipend
                                    toward books and/or tuition.
                                 </p>
                                 
                                 
                                 <p>Fellows should be committed to the values and the teachings of the Peace and Justice
                                    Institute and
                                    have strong communication, intercultural, computer and organizational skills.
                                 </p>
                                 
                                 <p></p>
                                 
                                 
                                 <p>
                                    
                                 </p>
                                 
                                 <center><strong>If you are interested in being a Peace and Justice Ambassador, please contact the
                                       Institute at
                                       <a href="mailto:peaceandjustice@valenciacollege.edu">peaceandjustice@valenciacollege.edu</a> or <a href="tel:407-582-2291">407-582-2291</a>.
                                       </strong></center>
                                 
                                 <p></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div>
                        <a href="/documents/studets/offices-services/peace-justice-institute/2017-vol8-1-pji-newsletter.pdf"><img src="/_resources/img/students/peace-justice-institute/right-peace-newsletter.png"></a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div align="center">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank" class="button-outline">DONATE</a>
                        
                     </div>
                     
                     <hr>
                     
                     
                     <div class="box_side">
                        
                        <h4>Orlando Speaks</h4>
                        <i class="icon_desktop"></i>
                        
                        
                        <p>Community dialogue to strengthen the relationship between citizens and police:<br><a href="/STUDENTS/offices-services/peace-justice-institute/community-programs/community-conversations.html">learn
                              more</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FPeace-and-Justice-Institute%2F317736788242008&amp;width=245&amp;height=350&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:245px; height:350px;" allowtransparency="true"></iframe>
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/community-programs/programs.pcf">©</a>
      </div>
   </body>
</html>