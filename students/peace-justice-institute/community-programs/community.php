<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Community  | Valencia College</title>
      <meta name="Description" content="Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful&amp;amp;#10;      environment on campus and within our community - one where conflict leads to growth and transformation rather than&amp;amp;#10;      violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/community-programs/community.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/community-programs/">Community Programs</a></li>
               <li>Community </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h2>Community</h2>
                           
                           <p>Practices of peace and nonviolence through our dedicated partnerships with local organizations</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <p>The Peace and Justice Institute provides community members with diverse opportunities
                              to become engaged
                              with the work through events, educational opportunities, and outreach.
                           </p>
                           
                           
                           <p>The deep and committed partnerships between individuals and organizations within the
                              community is
                              foundational to the work of the PJI. These partnerships allow for a connection between
                              the college and the
                              community and help bridge the work of peace and justice for all of our citizens locally.
                           </p>
                           
                           
                           <p>The calendar of events features workshops, film screenings, guest speakers and scholars,
                              among other
                              important events available for community members. Follow this link to access the calendar
                              of events.
                           </p>
                           
                           
                           <p>PJI regards every community member as a rising peace and justice practitioner
                              and provides the tools for all to be leaders of socially inclusive change and innovation
                              in their circle
                              of influence. The commitments of a peace and justice practitioner are a pathway to
                              building The Culture of
                              Peace and an invitation to becoming an agent of change.
                           </p>
                           
                           
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>The commitments of a peace and justice practitioner include the following: </p>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Places relationship as central to the work, focusing on the culture of collaboration</li>
                                    
                                    <li>Encourages a reflective practice to support self-awareness, meaning and purpose, including
                                       mindfulness practice and emotional intelligence
                                       
                                    </li>
                                    
                                    <li>Addresses conflict as a source and opportunity for growth and transformation</li>
                                    
                                    <li>Uses the tools of story, dialogue, and peaceful communication while practicing the
                                       Principles for
                                       How We Treat Each Other
                                       
                                    </li>
                                    
                                    <li>Supports a community of inclusive excellence in which all voices are heard and valued</li>
                                    
                                    <li>Engages in the exploration of the “other” with an acknowledgement of our inherent
                                       interdependence
                                       
                                    </li>
                                    
                                    <li>Recognizes that there can be no peace without justice for all</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div>
                        <a href="/documents/studets/offices-services/peace-justice-institute/2017-vol8-1-pji-newsletter.pdf"><img src="/_resources/img/students/peace-justice-institute/right-peace-newsletter.png"></a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div align="center">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank" class="button-outline">DONATE</a>
                        
                     </div>
                     
                     <hr>
                     
                     
                     <div class="box_side">
                        
                        <h4>Orlando Speaks</h4>
                        <i class="icon_desktop"></i>
                        
                        
                        <p>Community dialogue to strengthen the relationship between citizens and police:<br><a href="/STUDENTS/offices-services/peace-justice-institute/community-programs/community-conversations.html">learn
                              more</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FPeace-and-Justice-Institute%2F317736788242008&amp;width=245&amp;height=350&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:245px; height:350px;" allowtransparency="true"></iframe>
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/community-programs/community.pcf">©</a>
      </div>
   </body>
</html>