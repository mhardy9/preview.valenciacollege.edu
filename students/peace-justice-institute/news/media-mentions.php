<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/news/media-mentions.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/news/">News</a></li>
               <li>Peace and Justice Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Media Mentions</h2>
                        
                        <p>The Peace and Justice Institute is often recognized by the community in a variety
                           of media mentions. PJI media mentions include a collection of articles, videos, and
                           more.
                        </p>
                        
                        
                        <strong>April 2017</strong>
                        
                        <p>Community Leader Jimmie Williams writes in The Orlando Times about the fifth Orlando
                           Speaks that took place on April 12, 2017.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.orlando-times.com/orlando_speaks_564.html" target="_blank">Orlando Times</a>
                              
                           </li>
                           
                        </ul>
                        
                        <p>PJI Director, Rachel Allen was suggested for Scott Maxwell of the Orlando Sentinel's
                           list of 10 people who make Orlando a better place to live
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.orlandosentinel.com/opinion/os-10-people-make-orlando-great-scott-maxwell-20170420-story.html" target="_blank">Orlando Sentinel</a>
                              
                           </li>
                           
                        </ul>
                        
                        <strong>November 2016</strong>
                        
                        <p>Scott Maxwell includes Orlando Speaks in his list of 101 Things to Love about Central
                           Florida for 2016.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.orlandosentinel.com/opinion/os-101-things-love-central-florida-scott-maxwell-20161124-column.html" target="_blank">Orlando Sentinel</a>
                              
                           </li>
                           
                        </ul>
                        
                        <strong>October 2016</strong>
                        
                        <p>PJI facilitated the fourth <a href="/students/peace-justice-institute/community/community-conversations.php">Orlando Speaks</a> in partnership with the City of Orlando and the Orlando Police Department. 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Video: <a href="http://www.wesh.com/article/more-than-100-people-take-part-in-orlando-speaks/6952604" target="_blank">WESH</a>
                              
                           </li>
                           
                           <li>Article:  <a href="http://www.orlandosentinel.com/news/breaking-news/os-orlando-speaks-event-20161019-story.html" target="_blank">Orlando Sentinel</a>
                              
                           </li>
                           
                        </ul>
                        
                        <strong>July 2016</strong>
                        
                        <p>Hillary Clinton holds a roundtable discussion in Orlando following the Pulse Nightclub
                           tragedy. During this discussion, Orlando Mayor Buddy Dyer recommends <a href="/students/peace-justice-institute/community/community-conversations.cfm" target="_blank">Orlando Speaks</a> as a beneficial program for other cities across the nation to follow.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Video: <a href="http://www.msnbc.com/live-online/watch/live-clinton-holds-orlando-roundtable-730262595657" target="_blank">MSNBC</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>August 2015, October 2015, and February 2015</strong>
                        
                        <p><a href="/students/peace-justice-institute/community/community-conversations.cfm">Orlando Speaks</a> is a community workshop designed to encourage dialogue between citizens of the City
                           of Orlando and Police Officers with the goal to enhance our community policing initiative.
                           
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.wesh.com/news/orlando-police-hold-community-meeting/34917832" target="_blank">WESH</a>
                              
                           </li>
                           
                           <li>Article: <a href="http://www.orlandosentinel.com/news/breaking-news/os-orlando-police-race-justice-20150825-story.html" target="_blank">Orlando Sentinel</a>
                              
                           </li>
                           
                        </ul>
                        
                        <strong>May 2014</strong>
                        
                        <p>Members of the Peace and Justice Institute, including Valencia students, faculty,
                           and staff, and local community partners hosting a Bring Back Our Girls rally at Orlando's
                           City Hall to raise awareness of the kidnapped girls in Nigeria.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.wftv.com/news/news/local/orlando-group-rallies-raise-awareness-kidnapped-ni/ngBTf/" target="_blank">WFTV</a>
                              
                           </li>
                           
                           <li>Video: <a href="http://s3.amazonaws.com/TVEyesMediaCenter/UserContent/180409/3599354.5575/WOFL_05-31-2014_17.09.24.mp4" target="_blank">Fox 35</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        	<strong>January 2013</strong>
                        
                        <p>Jackie Chism, teacher at Jackson Heights Middle School and Founder of <strong>Peace Buy Piece</strong> buys back her students violent video games and DVDs. Chism presented during the Institute's
                           2013 Conversation on Justice. 
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article:  <a href="http://www.seminolechronicle.com/vnews/display.v/ART/2014/02/12/52fbe5e4242bd" target="_blank">Seminole Chronicle</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>August 2013</strong>
                        
                        <p> The Orlando Sentinel covers the Institute's Conflict Analysis Workshop on the Trayvon
                           Martin Case 
                        </p>
                        
                        
                        <strong>April 2013</strong>
                        
                        <p> The link below takes you to a blog featuring an exciting development within the Peace
                           and Justice Institute. Written by David Smith, formerly at the United States Institute
                           of Peace, the blog focuses on the work that community colleges are doing to support
                           peacebuilding, conflict resolution and related fields.
                           
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://davidjsmithblog.wordpress.com/2013/04/17/valencia-colleges-peace-and-justice-Institute-promotes-skill-building-and-interdisciplinary-approaches/" target="_blank">David Smith Blog</a>
                              
                           </li>
                           
                        </ul>
                        
                        <strong><strong>February 2013</strong></strong>
                        
                        <p>PJI hosts its first student retreat for their core group of student volunteers, the
                           Peace and Justice Ambassadors.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://thegrove.valenciacollege.edu/peace-ambassador-program-kicks-off-with-retreat/" target="_blank">The Grove</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>January 2013</strong>
                        
                        <p>Emmanuel Ande Ivorgba, Nigerian educator and peace advocate, shares his experience
                           of his time visiting Valencia College during the Institute's Conversation on Peace.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.projecthappiness.org/2013/04/02/lessons-from-valencia/">Project Happiness</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>October 2012</strong>
                        
                        <p> PJI hosts its first Valencia Night at the Islamic Society of Central Florida with
                           200 participants.
                        </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.valenciavoice.com/news/2012/10/16/islamic-leaders-raise-awareness/" target="_blank">Valencia Voice</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>September 2012</strong>
                        
                        <p> Valencia College to advocate peace through film</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://www.valenciavoice.com/2012/09/valencia-college-to-advocate-peace-through-film/" target="_blank">Valencia Voice</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>January 2012</strong>
                        
                        <p> Conversation on Compassion</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://thegrove.valenciacollege.edu/peace-ambassador-program-kicks-off-with-retreat/" target="_blank">The Grove</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        <strong>October 2011</strong>
                        
                        <p>Renowned Scholar Visits Valencia </p>
                        
                        <ul class="list_style_1">
                           
                           <li>Article: <a href="http://thegrove.valenciacollege.edu/renowned-scholar-visits-valencia-to-discuss-peace-studies/" target="_blank">The Grove</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/news/media-mentions.pcf">©</a>
      </div>
   </body>
</html>