<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/news/newsletter.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/news/">News</a></li>
               <li>Peace and Justice Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        
                        
                        
                        
                        
                        <section>     
                           
                           
                           <h2>Newsletter</h2>
                           
                           
                           <h4>Current Newsletter</h4>
                           
                           <ul>
                              
                              <li>
                                 <a href="documents/PJI-Newsletter-Vol-8-No-1.pdf" target="_blank">Volume 8, No. 1</a><br>
                                 Spring 2017
                              </li>
                              
                           </ul>
                           
                           <h4>Past Newsletters</h4>
                           
                           <ul>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter15.pdf" target="_blank">Volume 7, No. 2 </a> <br>
                                 Fall 2016 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter14.pdf" target="_blank">Volume 7, No. 1 </a> <br>
                                 Spring 2016
                                 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter13.pdf" target="_blank">Volume 6, No. 2 </a> <br>
                                 Fall 2015
                                 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter12.pdf" target="_blank">Volume 6, No. 1</a> <br>
                                 Spring 2015
                                 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter11.pdf" target="_blank">Volume 5, No. 2</a><br>
                                 Fall 2014 
                                 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter10.pdf" target="_blank">Volume 5, No. 1</a><br>
                                 Spring 2014
                                 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter9.pdf" target="_blank">Volume 4, No. 2</a><br>
                                 Fall 2013
                                 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter8A.pdf" target="_blank">Volume 4, No. 1</a><br>
                                 Spring 2013 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter7.pdf" target="_blank">Volume 3, No. 2</a><a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter7.pdf" target="_blank"><br>
                                    </a>Fall 2012 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/Vol3No1.pdf" target="_blank">Volume 3, No. 1</a><a href="https://valenciacollege.edu/PJI/news/documents/Volume3-1.pdf" target="_blank"><br>
                                    </a>Spring 2012 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/PJINewsletter5.pdf" target="_blank">Volume 2, No. 3</a><a href="https://valenciacollege.edu/PJI/news/documents/Volume2-3.pdf" target="_blank"></a><a href="https://valenciacollege.edu/PJI/news/documents/Volume2-3.pdf" target="_blank"><br>
                                    </a>Fall 2011 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/July2011.pdf" target="_blank">Volume 2, No. 2</a><a href="https://valenciacollege.edu/PJI/news/documents/July2011.pdf" target="_blank"></a><a href="https://valenciacollege.edu/PJI/news/documents/July2011.pdf" target="_blank"><br>
                                    </a>Summer 2011 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/Feb2011.pdf" target="_blank">Volume 2, No. 1</a><a href="https://valenciacollege.edu/PJI/news/documents/Volume2-1.pdf" target="_blank" title="Volume 2, No. 1"><br>
                                    </a>Spring 2011 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/Volume1-2.pdf" target="_blank" title="Volume 1, No. 2">Volume 1, No. 2</a><br>
                                 Fall 2010 
                              </li>
                              
                              <li>
                                 <a href="https://valenciacollege.edu/PJI/news/documents/Volume1-1.pdf" target="_blank" title="Volume 1, No. 1">Volume 1, No. 1</a><br>
                                 Spring 2009 
                              </li>
                              
                           </ul>            
                           
                        </section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank">DONATE</a>
                        <a href="../documents/Global-Peace-Week-2017-Master-Schedule.pdf" target="_blank">Global Peace Week Schedule</a>
                        
                        
                        
                        
                        
                        
                        
                        <center></center>
                        
                        
                        
                        <div>
                           
                           <ol><em>
                                 <center>Goals of the Institute are to:</center></em></ol>
                           
                           <div> 
                              <ol>1. Create and teach a <a href="../students/curriculum.html">Peace and Justice Studies</a> curriculum <br> 2. Sponsor and collaborate on peace and justice events<br>
                                 3. Foster a connection with Valencia's A.S. programs in peace studies and conflict
                                 transformation<br>
                                 4. Offer community outreach in peace and justice<br>
                                 5. Engage in realizing Valencia's core competencies<br>
                                 
                              </ol>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/news/newsletter.pcf">©</a>
      </div>
   </body>
</html>