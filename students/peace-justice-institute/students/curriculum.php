<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Curriculum | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/students/curriculum.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/students/">Students</a></li>
               <li>Student Curriculum</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Student Curriculum</h2>
                        
                        <p><strong>What is Peace Studies?</strong></p>
                        
                        <p>To find out more about a career in peace studies, read the article <a href="http://www.associationdatabase.com/aws/NCDA/pt/sd/news_article/39568/_PARENT/layout_details_cc/false" target="blank">Starting a Career Building Peace</a> by David J. Smith, President, Forage Center for Peacebuilding and Humanitarian Education,
                           Inc. 
                        </p>
                        
                        
                        
                        <p>Curious how your field of study relates to peace studies? Check out "Strategic Peacebuilding
                           Paths," a diagram created by University of Notre Dame's <a href="http://kroc.nd.edu" target="blank">Kroc Institute for International Peace Studies</a>.
                        </p>
                        
                        
                        
                        <div title="Strategic Peacebuilding Paths">
                           
                           <p>Strategic Peacebuilding Paths </p>
                           
                        </div>
                        
                        
                        <p><strong>Take Courses at Valencia</strong></p>
                        
                        
                        <hr class="styled_2">              
                        
                        <h3>CJE 2062: Peace, Conflict and the Police</h3>
                        
                        
                        <p><strong>Prerequisite: Freshman Composition I</strong><br>
                           In this course, students will learn the meaning of peace and investigate the philosophical
                           and religious theories that underlie peace studies. In addition, students will investigate
                           causes of war and violence from the individual level to international level and evaluate
                           the paradox of the police as instruments of both peace and conflict. Students will
                           investigate the police role in nonviolent movements and learn about occupational and
                           organizational factors that influence police behavior. Finally, students will learn
                           about the history of non-violent movements and the means used to end conflict or injustice
                           peaceably. Finally, students will be expected to devise an alternative framework for
                           the police that emphasize peacekeeping strategies rather than coercive means.
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>EDG 2935: Promoting the Culture of Peace in Classrooms</h3>
                        
                        <p>Promoting the Culture of Peace is a three credit course that can be taken by students
                           in all professions to excel in their careers peacefully, or for Personal Development,
                           or to help improve the society they live in. &nbsp;This course will be customized to meet
                           the career and educational needs of students and will focus on topics such as Peace
                           Education, Conflict Transformation, Communication Skills, Bullying, Cyber-bullying,
                           Non-Violence, Causes of Conflict, Cross-cultural and Inter-faith conflicts, and the
                           current research on the Culture of Peace. The Culture of Peace will help students
                           promote values of compassion, equality, human rights, diversity, and peace. It will
                           empower them with the knowledge, skills, attitudes and values necessary to end violence,
                           injustice, and promote the culture of peace.
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>ENC 1102: Freshman Composition II: Perspectives on War, Philosophies of Peace</h3>
                        
                        
                        <p><strong>Prerequisite: Freshman Composition I</strong><br>
                           Through documentaries, photography, poetry, art, literature, philosophy, and non-fiction
                           works,  this online course explores multiple perspectives of warfare and introduces
                           philosophies of peace to build pathways of healing. Application of skills learned
                           in ENC 1101 is expected, while there is an emphasis on style; use of library; reading
                           and evaluating available sources, along with planning, writing, and documenting a
                           short research paper. This is a Gordon Rule course in which the student is required
                           to demonstrate college-level writing skills through multiple assignments.
                        </p>
                        
                        
                        
                        <hr class="styled_2">                  
                        
                        <h3>ENC 1102: Freshman Composition II: Voices From the Margins: Reading, Discussing and
                           Writing for Peace and Justice
                        </h3>
                        
                        
                        <p><strong>Prerequisite: Freshman Composition I</strong><br>
                           This course will focus on literature written by and about people from marginalized
                           cultures in America. Application of skills learned in ENC 1101. Emphasis on style;
                           use of library; reading and evaluating available sources; planning, writing, and documenting
                           short research paper. Gordon Rule course in which the student is required to demonstrate
                           college-level writing skills through multiple assignments. Minimum grade of C required
                           if ENC 1102 is used to satisfy Gordon Rule and general education requirements.
                        </p>
                        
                        
                        
                        <hr class="styled_2">                                  
                        
                        <h3>EVR 1001: Environmental Science</h3>
                        
                        
                        <p>This non-laboratory course provides understanding of our interdependence with and
                           responsibility for environment, earth and all species. We will Investigate such aspects
                           as pollution, urbanization, population trends and changes in lifestyles. We will address
                           present and projected solutions to current and future problems and predicaments. You
                           are encouraged to inquire beyond these basic areas of study outlined.
                        </p>
                        
                        
                        <hr class="styled_2">                 
                        
                        <h3>GEB 1155: Social Entrepreneurship</h3>
                        
                        
                        <p><strong>Prerequisite: GEB 1011 or GEB 1136 or ECO 2013 or ECO 2023 or MUM 2720C or department
                              approval. </strong><br>
                           This course explores Social entrepreneurship as a rapidly developing and changing
                           business field in which business and nonprofit leaders design, grow, and lead mission-driven
                           enterprises. As the traditional lines blur between nonprofit enterprises, government,
                           and business, students explore opportunities and challenges in this new landscape
                           through local project based learning. 
                        </p>
                        
                        
                        <hr class="styled_2">
                        
                        <h3>HLP 1093: Meditation for Stress Management</h3>
                        
                        
                        <p>This experiential course is an introduction to the art and science of meditation for
                           stress management. Topics include the benefits of meditation, meditation techniques,
                           breath work, meditation and health, and meditation for everyday living. This course
                           will help students find the type of medication that is best for them enabling them
                           to establish a personal meditation practice. This course is suitable for all students,
                           regardless of physical limitations.
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>LIT 2174: Multimedia Literature and the Holocaust</h3>
                        
                        
                        <p><strong>Prerequisite: ENC 1101</strong></p>
                        
                        <p>This course explores literacy characteristics inherent in various media including
                           (but not limited to) Holocaust - related historical text, documentary film, comics
                           (graphic narrative), survivor narratives, pre- and post - Nazi art and contemporary
                           major motion pictures. The examination includes critical analyses of textual, visual,
                           syntactical, mechanical and thematic conventional similarities found in traditional
                           textual 'literature" and in the structure, syntax and language of visual media.
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>PAX 1000: Introduction to Peace Studies</h3>
                        
                        
                        <p>This course studies peace in its philosophical, religious, literary, historical and
                           other cultural contexts. It investigates the causes of violence on the global and
                           personal levels. There is an emphasis on the interdisciplinary study of peace and
                           the peace movement in historical and contemporary views. It also teaches the application
                           of conflict resolution, nonviolence, and other practices necessary to become more
                           powerful and peaceful members of our world
                        </p>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>PAX 1500: Conflict Transformation: Paths to Peace</h3>
                        
                        
                        <p>This course is designed to create the potential for intrapersonal and interpersonal
                           transformation while exploring tools for conflict transformation. Students will explore
                           different approaches to conflict and the many reasons why conflict between individuals,
                           groups and states arises and even turns violent. We will discuss the various actions
                           that people can take to mitigate and transform the destructive forces of both inter
                           and intrapersonal conflict, including, for example: reflective practice, dialogue,
                           mediation and negotiation. We will explore these different conflict interventions
                           by studying the theory and practice of negotiation and mediation skills, analyzing
                           specific conflicts in history and in current events, and becoming proficient in skillful
                           dialogue aimed at bridging personal, social and cultural gaps which often lead to
                           misunderstanding and conflict.
                        </p>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>PSY 2930: Positive Psychology</h3>
                        
                        
                        <p>This course is designed to introduce a strength-based psychology that scientifically
                           studies positive human functioning, specifically, the strengths and virtues that enable
                           individuals and communities to thrive. Emphasis will be placed on studying and applying
                           topics such as; happiness, gratitude, flow, optimism and hope, wisdom and courage,
                           positive affect, resilience, coping, friendship, and positive development across the
                           lifespan.
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>PSY 2930: Psychology of Peace</h3>
                        
                        
                        <p>Using data from developmental, personality, social, learning, and biological psychologies,
                           this course will investigate the causes and consequences of violence and non-violence.
                           Students will understand how conflict is caused and healed on interpersonal and intergroup
                           levels. Analysis from many areas of psychology will focus on personality variables,
                           developmental sequalae, social inequalities, peace-making, peace-building, and social
                           justice.
                        </p>
                        
                        
                        
                        
                        <hr class="styled_2">                  
                        
                        <h3>SLS 2940: Peace and Justice Ambassadors Service Learning (3 credit hours)</h3>
                        
                        
                        <p>This is a planned service-learning experience that focuses on three hallmarks: service,
                           leadership and scholarship. Students in this course will complete 60 hours of Service
                           Learning. Students serve as Peace and Justice Ambassadors in Service to the Peace
                           and Justice Institute at Valencia College.
                        </p>
                        
                        
                        
                        <hr class="styled_2">
                        
                        <h3>WOH 2003: A History of Genocide</h3>
                        
                        
                        <p>The primary focus of this course is to define and discuss genocide in all of its forms,
                           expose the flaws in current pedagogy and reappraise in order to address the complexities
                           of the topic. It will address bureaucratic mechanized genocides as well as the more
                           spontaneous and the pre-industrial types. This course is a mix of theoretical considerations
                           alongside the history of genocide ranging back to antiquity and various case studies.
                        </p>
                        
                     </div>
                     
                  </div>
                  
                  
                  
                  
               </div> 
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/students/curriculum.pcf">©</a>
      </div>
   </body>
</html>