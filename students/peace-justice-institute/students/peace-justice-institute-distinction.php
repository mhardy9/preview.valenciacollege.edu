<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute (PJI) Distinction | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/students/peace-justice-institute-distinction.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/students/">Students</a></li>
               <li>Peace and Justice Institute (PJI) Distinction</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        
                        <h2>Peace and Justice Institute (PJI) Distinction</h2>
                        
                        
                        <p>The Peace and Justice Distinction is one of a number of co-curricular distinctions
                           offered at Valencia College.
                        </p>
                        
                        <p>The Distinction offers an opportunity for a co-curricular focus in peace and justice
                           to advance his or her personal and professional development. Through the Distinction
                           program, the student will gain awareness, garner interest, and seek additional learning
                           opportunities in Peace and Justice.
                        </p>
                        
                        
                        <p>The Peace and Justice Distinction offers an entry into the work of the Peace and Justice
                           Institute. Upon completion of the PJI Distinction, the student becomes eligible to
                           be a Peace and Justice Ambassador and/or apply to become a Leadership Fellow (with
                           a $500 stipend per semester) with the Institute.
                        </p>
                        
                        
                        <p>The Distinction offers a certificate and graduation chord upon completion, honorable
                           mention in the Peace and Justice bi-annual newsletter and recognition at the annual
                           Student Awards Ceremony.
                        </p>
                        
                        		  
                        
                        <p><strong>PJI Certification Requirements:</strong></p>
                        
                        
                        
                        
                        <ol> 
                           
                           <li>
                              <p>Attend the Be the Change skillshop for an overview of the PJI or
                                 meet one on one with a representative from PJI                                   
                                 8 points
                                 
                              </p>
                           </li>
                           
                           <li>
                              <p>Attend 7 PJI events over the course of one year (7 events at 8 points each)      
                                 56 points
                              </p>
                           </li>
                           
                           <li>
                              <p>Volunteer with the Peace and Justice Ambassadors during two additional
                                 events (2 events @ 8 points each)                                                
                                 16 points
                              </p>
                           </li>
                           
                           <li>
                              <p>Attend one off-campus PJI event or service learning experience                   
                                 8 points
                              </p>
                           </li>
                           
                           <li>
                              <p>Create a Capstone Project demonstrating the learnings from PJI                   
                                 12 points
                              </p>
                           </li>
                           
                           <p><strong>Total Points Required                                                            
                                 100 points</strong></p>
                           
                        </ol>
                        
                     </div>
                     
                     <p><strong><a href="http://goo.gl/forms/rNJPe19DAkk2whYF3" target="_blank"> Click here to register in order to become a candidate for the PJI Distinction. </a>  </strong></p>
                     
                     
                     <p>As a result of completing the Peace and Justice Certification you will receive:<br>
                        
                        <ul>
                           
                           <li>A special PJI cord to wear at graduation</li>
                           
                           <li>A certificate of completion for your academic portfolio</li>
                           
                           <li>Honorable mention in the PJI bi-annual newsletter</li>
                           
                           <li>Recognition at the Annual Student Awards Ceremony</li>
                           
                           
                           
                           
                        </ul>
                        		
                     </p>
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/students/peace-justice-institute-distinction.pcf">©</a>
      </div>
   </body>
</html>