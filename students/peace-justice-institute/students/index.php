<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Students | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/students/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Students</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <h2>Students</h2>
                        
                        
                        <p>The Peace and Justice Institute provides students with diverse opportunities to become
                           engaged with the work through curricular, co-curricular and community events.
                        </p>
                        
                        <p>PJI offers students multiple entry points into the world of peace and justice. Signing
                           up for Introduction to Peace Studies and Conflict Transformation: Paths to Peace gives
                           students the foundational courses of Peace Studies. Courses across various disciplines
                           in peace studies round out the curriculum.
                        </p> 
                        
                        
                        <p>In addition to the curriculum of study, students engage with PJI through the Peace
                           and Justice Distinction program, by becoming a Peace and Justice Ambassador, or earning
                           the position of a Leadership Fellow at the Institute. Each of these opportunities
                           exposes students to the foundational teachings of peace and justice and provides them
                           with important experiences with and connections to our campus and community partners.
                        </p>
                        
                        
                        <p>The calendar of events features workshops, film screenings, guest speakers and scholars,
                           among other important events available for students. 
                        </p>
                        
                        <p>PJI regards every individual  as a rising peace and justice practitioner and provides
                           the tools for all to be leaders of socially inclusive change and innovation in their
                           circle of influence. The commitments of a peace and justice practitioner are a pathway
                           to building The Culture of Peace and an invitation to becoming an agent of change.
                        </p>
                        
                        <p>The commitments of a peace and justice practitioner include the following:</p>
                        
                        <ul>
                           
                           <li>Places relationship and community as central to the work, focusing on the culture
                              of collaboration
                           </li>
                           
                           <li>Encourages a reflective practice to support self-awareness, meaning and purpose, including
                              mindfulness practice and emotional intelligence
                           </li>
                           
                           <li>Addresses conflict as a source and opportunity for growth and transformation</li>
                           
                           <li>Uses the tools of story, dialogue, and peaceful communication while practicing the
                              Principles for How We Treat Each Other
                           </li>
                           
                           <li>Supports a community of inclusive excellence in which all voices are heard and valued</li>
                           
                           <li>Engages in the exploration of the “other” with an acknowledgment of our inherent interdependence</li>
                           
                           <li>Recognizes that there can be no peace without justice for all</li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/students/index.pcf">©</a>
      </div>
   </body>
</html>