<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace Through Service | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/students/peace-through-service-program.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/students/">Students</a></li>
               <li>Peace Through Service</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		 
                        <h2>Peace Through Service</h2>
                        		
                        <p>Sowing Peace, Growing Peace, Through Service</p>	<img src="/students/peace-justice-institute/images/sowing-growing-peace-logo.png" class="img-responsive" alt="Sowing Growing Peace Logo" align="right">
                        
                        <p>PJI invites you to become a Peace Leader for the community through our Sowing Peace,
                           Growing Peace, Through Service program. As a student participant, you will learn about
                           the great leaders of peace and explore the nonviolent wisdom of cultures past and
                           present. More importantly, over a period of 16 weeks, you will learn about community
                           needs, both in and outside of the classroom setting, through working with local peace
                           leaders who will help you build authentic relationships with the people who need help.
                           Civic and classroom activities which engage the heart as well as the intellect are
                           at the core of the program. 
                        </p>
                        
                        <p>This program requires attendance and participation in all meetings during the semester.
                           Students will also attend 4 mandatory off campus field trips to local organizations.
                           After completion of all assignments, attending all meetings at the Winter Park campus,
                           and fully participating in the 4 mandatory off campus field trips, students will receive
                           a <strong>$500 stipend</strong>. 
                        </p>
                        
                        <p>Students will be challenged, supported and enriched according to their commitment
                           to the program. This program gives students the opportunity to grow in their knowledge
                           and understanding of how to build a culture of peace through study, dialogue, and
                           active learning experiences. Take up the challenge to grow as a peace leader for your
                           community!
                        </p>
                        		
                        
                        <hr class="styled_2">
                        
                        <h3>Endowed Program Policies</h3>
                        
                        <ol>
                           
                           <li>The Peace and Justice Institute’s Principles for How We Treat Each Other (<a href="/students/peace-justice-institute/principles.php" rel="nofollow noreferrer" target="_blank"> Peace and Justice Institute Principles</a>) are expected to be practiced by all participants in the program.
                           </li>
                           
                           <li>Participants will commit to attend all required meetings at the Winter Park campus
                              in room 246 each Tuesday from 2:30pm to 3:45pm and the 4 mandatory off-site local
                              field trips.
                           </li>
                           
                           <li> From time to time new non-required events will be announced to all participants.
                              If you agree to attend a non-required event but are unable to attend, please notify
                              Paul Chapman (<a href="mailto:pchapman@valenciacollege.edu" rel="noreferrer">pchapman@valenciacollege.edu</a>&nbsp;or call 407-582-6819) at least 24 hours ahead of time. If you have an emergency situation,
                              and can’t meet the 24-hour request, please notify Paul Chapman as soon as possible.
                           </li>
                           
                           <li>Participants must check their BlackBoard/ATLAS email daily in order to stay on top
                              of all program related information.
                           </li>
                           
                           <li> Participants will be expected to adhere to the Valencia Student Code of Conduct and
                              respect all the rules, restrictions and policies of our partner organizations.
                           </li>
                           
                        </ol>
                        	
                        <hr class="styled_2">
                        
                        <h3>Program Outcomes</h3>
                        
                        <ol>
                           
                           <li>Academics: Students will improve their higher order critical thinking skills through
                              analysis assigned reading materials, dialogue with participants, interaction with
                              invited speakers and reflective journaling.
                           </li>
                           
                           <li> Civic Engagement: Students will gain greater understanding of local community organizations
                              and the residents they serve.
                           </li>
                           
                           <li> Personal: Students will produce reflective journals chronicling their personal challenges
                              and growth throughout the semester.
                           </li>
                           
                        </ol>
                        	
                        <hr class="styled_2">
                        
                        <h3>Instructions</h3>
                        
                        <p>To apply for the Sowing Peace, Growing Peace, Through Service program for Fall 2017,
                           please carefully complete the application by <span><strong>August 1, 2017</strong></span>. Only members of the selection committee will see your application.
                        </p>
                        
                        <a href="http://net4.valenciacollege.edu/forms/pji/peace-through-service.cfm" target="_blank">Apply for Peace Through Service</a>
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/students/peace-through-service-program.pcf">©</a>
      </div>
   </body>
</html>