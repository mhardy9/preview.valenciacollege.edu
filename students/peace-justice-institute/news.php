<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>News  | Valencia College</title>
      <meta name="Description" content="Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful&amp;amp;#10;      environment on campus and within our community - one where conflict leads to growth and transformation rather than&amp;amp;#10;      violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/news.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>News </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h2>News</h2>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The Peace and Justice Institute bi-annually publishes a newsletter with articles written
                              by
                              distinguished guests, faculty, and students. The newsletter details the work of the
                              Institute and
                              serves as a historical document for the growth and development of the work over time.
                              The publication
                              provides information on events from the academic semester and recognizes the contributions
                              of the many
                              individuals that bring to the life the mission of PJI.
                           </p>
                           
                           
                           <p>Additionally, the Peace and Justice Institute is often recognized by the community
                              in a variety of
                              media mentions. PJI media mentions include a collection of articles, videos, and more.
                           </p>
                           
                           
                        </div>
                        
                        <div class="row">
                           
                           <div class="col-md-12">
                              
                              <p></p>
                              
                              <h4></h4>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>Newsletter</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2017-vol8-1-pji-newsletter.pdf" target="_blank">Volume
                                          8, No. 1 </a><br> Spring 2017
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2016-vol7-2-pji-newsletter.pdf" target="_blank">Volume
                                          7, No. 2 </a><br> Spring 2016
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2015-vol6-2-pji-newsletter.pdf" target="_blank">Volume
                                          6, No. 2 </a> <br> Fall 2015
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2015-vol6-1-pji-newsletter.pdf" target="_blank">Volume
                                          6, No. 1</a> <br> Spring 2015
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2014-vol5-2-pji-newsletter.pdf" target="_blank">Volume
                                          5, No. 2</a><br> Fall 2014
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2014-vol5-1-pji-newsletter.pdf" target="_blank">Volume
                                          5, No. 1</a><br> Spring 2014
                                       
                                    </li>
                                    
                                    
                                 </ul>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2013-vol4-2-pji-newsletter.pdf" target="_blank">Volume
                                          4, No. 2</a><br> Fall 2013
                                       
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2013-vol4-1-pji-newsletter.pdf" target="_blank">Volume
                                          4, No. 1</a><br> Spring 2013
                                       
                                    </li> 
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2012-vol3-2-pji-newsletter.pdf" target="_blank">Volume
                                          3, No. 2</a><br>Fall 2012 
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2012-vol3-1-pji-newsletter.pdf" target="_blank">Volume 3, No.1</a><br>Spring 2012
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2011-vol2-3-pji-newsletter.pdf" target="_blank">Volume
                                          2, No. 3</a><br>Fall 2011
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2011-vol2-2-pji-newsletter.pdf" target="_blank">Volume 2, No.2</a><br>Summer 2011
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2011-vol2-1-pji-newsletter.pdf" target="_blank">Volume 2, No.1</a><br>Spring 2011
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2010-vol1-2-pji-newsletter.pdf" target="_blank" title="Volume 1, No. 2">Volume 1, No. 2</a><br> Fall 2010 
                                    </li>
                                    
                                    <li>
                                       <a href="/documents/studets/offices-services/peace-justice-institute/2009-vol1-1-pji-newsletter.pdf" target="_blank" title="Volume 1, No. 1">Volume 1, No. 1</a><br> Spring 2009
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div>
                        <a href="/documents/studets/offices-services/peace-justice-institute/2017-vol8-1-pji-newsletter.pdf"><img src="/_resources/img/students/peace-justice-institute/right-peace-newsletter.png"></a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div align="center">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank" class="button-outline">DONATE</a>
                        
                     </div>
                     
                     <hr>
                     
                     
                     <div class="box_side">
                        
                        <h4>Orlando Speaks</h4>
                        <i class="icon_desktop"></i>
                        
                        
                        <p>Community dialogue to strengthen the relationship between citizens and police:<br><a href="/STUDENTS/offices-services/peace-justice-institute/community-programs/community-conversations.html">learn
                              more</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FPeace-and-Justice-Institute%2F317736788242008&amp;width=245&amp;height=350&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=false" scrolling="no" frameborder="0" style="border:none; overflow:hidden; width:245px; height:350px;" allowtransparency="true"></iframe>
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/news.pcf">©</a>
      </div>
   </body>
</html>