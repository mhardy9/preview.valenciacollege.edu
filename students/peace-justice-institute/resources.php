<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Resources  | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Resources </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               	
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Resources</h2>
                        					
                        <p></p>
                        					
                        <h3>Advisory Council</h3>
                        					
                        <p></p>
                        					
                        <p>The Peace and Justice Institute aims to empower people to take action by providing
                           resources to support the culture of peace. As part of these resources, you will find
                           the following
                        </p>
                        					
                        <ul>
                           						
                           <li>A Library Resource Guide which is a collection of books and articles in the field
                              of Peace and Justice as compiled by our Valencia Libraries.
                           </li>
                           						
                           <li>Consultants who have served to lay the foundational philosophies and teachings of
                              PJI. Our consultants have been invaluable in guiding our work and serving as mentors
                              to so many of us.
                           </li>
                           						
                           <li>Community partners who support the initiatives of the PJI, provide resources and education
                              to enhance our efforts, and collaborate to build the culture of peace and justice.
                           </li>
                           						
                           <li>Terminology related to Peace and Justice studies.</li>
                           						
                           <li>What We Are Reading, a list of books that have deeply informed our work.</li>
                           					
                        </ul>
                        				
                     </div>
                     				
                     <p></p>
                     				
                     <h4></h4>
                     				
                     <hr class="styled_2">
                     				
                     <h3>Library Resource Guide</h3>
                     				
                     <p></p>
                     				
                     <p>The Peace and Justice Library Resource Guide assists users in locating information
                        on the key issues surrounding peace and justice. It highlights the Peace and Social
                        Justice Collection at the East Campus, new materials including books and e-books at
                        all campuses, electronic resources, databases, journals, and links to other relevant
                        information. This guide is not intended to be comprehensive, but highlights some of
                        the useful resources as a starting point for inquiry into this interdisciplinary field
                        of study.
                     </p>
                     				
                     <p align="center"><a href="http://libguides.valenciacollege.edu/pji" target="_blank">Click here</a> to access the Peace and Justice Library Resource Guide.
                     </p>
                     				
                     <hr class="styled_2">
                     				
                     <h3>Consultants</h3>
                     				
                     <p></p>
                     				
                     <p>The Peace and Justice Institute at Valencia College promotes peace and justice for
                        all.
                     </p>
                     				
                     <div class="row">
                        					
                        <div class="col-md-6 col-sm-6">
                           						
                           <ul class="list_teachers">
                              							
                              <li>
                                 								
                                 <h5>Alma Abdul Hadi-Jadallah, PhD.</h5>
                                 								
                                 <p>George Mason University, Eastern Mennonite University, Kommon Denominator</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Phyllis Beck Kritek, RN, PhD, FAAN.</h5>
                                 								
                                 <p>Former faculty member, Harvard School of Public Health, Board of Trustees of the Commission
                                    on Graduates of Foreign Nursing Schools
                                 </p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Iron Eagle</h5>
                                 								
                                 <p>Apache Medicine Man</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Angela King</h5>
                                 								
                                 <p>Deputy Director, Life After Hate</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>George Lopez, PhD.</h5>
                                 								
                                 <p>Notre Dame University, The Kroc Institute for International Peace Studies</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Peggy McIntosh, PhD.</h5>
                                 								
                                 <p>Founder and Senior Associate, National SEED Project</p>
                                 							
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <div class="col-md-6 col-sm-6">
                           					
                           <ul class="list_teachers">
                              							
                              <li>
                                 								
                                 <h5>Michael Nagler, PhD.</h5>
                                 								
                                 <p>Founder, The Metta Center for Nonviolence, Professor Emeritus at UC Berkeley</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Lucy M. Roberts, M.A.</h5>
                                 								
                                 <p>Roberts Consulting, primary consultant in the development and foundation of the Peace
                                    and Justice Institute
                                 </p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>David J. Smith</h5>
                                 								
                                 <p>The United States Institute of Peace</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Emily Style</h5>
                                 								
                                 <p>Founding Co-Director, National SEED Project</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Elaine Sullivan, M.A.</h5>
                                 								
                                 <p>The Center for Renewal and Wholeness in Higher Education</p>
                                 							
                              </li>
                              							
                              <li>
                                 								
                                 <h5>Lee Mun Wah</h5>
                                 								
                                 <p>International Diversity Trainer and Filmmaker</p>
                                 							
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        					
                     </div>
                     		
                     			
                     <hr class="styled_2">
                     	
                     			
                     <h3>Partners</h3>
                     						
                     <div class="row">
                        					
                        <div class="col-md-6 col-sm-6">
                           						
                           <ul>
                              							
                              <li>
                                 								<a href="http://www.amnestyorlando.org/" target="_blank">Amnesty International USA</a> Orlando Chapter 519
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://artreachorlando.org/" target="_blank">ArtReach Orlando</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://buildingus.org/" target="_blank">Building US</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.richlandcollege.edu/crwhe/" target="_blank">Center for Renewal and Wholeness in Higher Education</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://cflfreethought.org/" target="_blank">Central Florida Freethought Community</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.cityoforlando.net/" target="_blank">City of Orlando</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.centralfloridahomeless.org/" target="_blank">Coalition for the Homeless of Central Florida</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.creducation.org" target="_blank">The Conflict Resolution Education Connection</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://elcoforangecounty.org/" target="_blank">The Early Learning Coalition of Orange County</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="https://www.fadp.org/" target="_blank">Floridians for Alternatives to the Death Penalty</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.peacefilmfest.org/" target="_blank">The Global Peace Film Festival</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.orlandobuddhism.org/" target="_blank">Guang Ming Buddhist Temple</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="https://www.harborhousefl.com/" target="_blank">Harbor House</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.holocaustedu.org/" target="_blank">Holocaust Memorial Resource &amp; Education Center of Florida</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://howardzinn.org/related-projects/zinn-education-project/" target="_blank">Howard Zinn Education Project</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://interfaithfl.org/" target="_blank">Interfaith Council of Central Florida</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://iscf.org/" target="_blank">The Islamic Society of Central Florida</a>
                                 							
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                        <div class="col-md-6 col-sm-6">
                           						
                           <ul>
                              							
                              <li>
                                 								<a href="http://www.knowledgeforliving.org/" target="_blank">Knowledge for Living</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://kroc.nd.edu/" target="_blank">Kroc Institute of International Peace Studies</a> University of Notre Dame
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://lwvoc.org/" target="_blank">League of Women Voters of Orange County</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.lifeafterhate.org/" target="_blank">Life After Hate</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.mettacenter.org/" target="_blank">Metta Center for Nonviolence</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.ucfglobalperspectives.org/" target="_blank">Office of the Special Assistant to the President for Global Perspectives</a><br>
                                 								The University of Central Florida
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.orlandoyouthalliance.org/" target="_blank">OYA</a><br>
                                 								Orlando Youth Alliance
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.peacejusticestudies.org/" target="_blank">Peace and Justice Studies Association</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="https://www.plannedparenthood.org/" target="_blank">Planned Parenthood</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.seminolestate.edu/" target="_blank">Seminole State College</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="https://www.splcenter.org/" target="_blank">Southern Poverty Law Center Teaching Tolerance</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://valenciacollege.edu/international/studyabroad/default.cfm" target="_blank">Study Abroad and Global Experiences</a><br>
                                 								Valencia College
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://www.usip.org/" target="_blank">U. S. Institute of Peace</a>
                                 							
                              </li>
                              							
                              <li>
                                 								<a href="http://zebrayouth.org/" target="_blank">Zebra Coalition</a>
                                 							
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     		
                     			
                  </div>
                  		
               </div>
               	
               		
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/resources.pcf">©</a>
      </div>
   </body>
</html>