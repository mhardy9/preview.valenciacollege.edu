<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Advisory Council | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/advisory-council.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Advisory Council</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1">
                           					
                           <h2>Advisory Council</h2>
                           					
                           <p></p>
                           				
                        </div>
                        				
                        <div class="wrapper_indent">
                           					
                           <p>The Peace and Justice Institute at Valencia College promotes peace and justice for
                              all.
                           </p>
                           					
                           <div class="row">
                              						
                              <div class="col-md-6 col-sm-6">
                                 							
                                 <ul class="list_teachers">
                                    								
                                    <li>
                                       									
                                       <h5>Rachel Allen</h5>
                                       								
                                    </li>
                                    								
                                    <li>Coordinator, Peace and Justice Institute, Professor of Humanities, Valencia College</li>
                                    								
                                    <li>
                                       									
                                       <h5>Rachel Allen</h5>
                                       									
                                       <p>Coordinator, Peace and Justice Institute, Professor of Humanities, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Patricia Ambinder</h5>
                                       									
                                       <p>Council Chair, Community Organization &amp; Development</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Susan Arkin</h5>
                                       									
                                       <p>Community Organization and Development</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>John Bersia</h5>
                                       									
                                       <p>Director, University of Central Florida Global Perspectives Office</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Allie Braswell</h5>
                                       									
                                       <p>Managing Director, Braswell Management Group, LLC</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Paul Chapman</h5>
                                       									
                                       <p>Professor of Humanities and Peace Studies, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Nicole Elinoff</h5>
                                       									
                                       <p>Sexual Minority Health Coordinator and Human Service Program Consultant I, Area 7
                                          Program Office, Florida Department of Health in Orange County
                                       </p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Anna V. Eskamani</h5>
                                       									
                                       <p>Regional Director of Public Policy and Field Operations, Planned Parenthood of Southwest
                                          and Central Florida
                                       </p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Ann Farrell</h5>
                                       									
                                       <p>Immigration Attorney</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Sue Foreman</h5>
                                       									
                                       <p>Community Organization &amp; Development</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Reverend Bryan Fulwider</h5>
                                       									
                                       <p>President and CEO, Building US</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Marcia Hope Goodwin</h5>
                                       									
                                       <p>Chief Service Officer &amp; Director, Office of Community Affairs and Human Relations,
                                          Office of the Mayor, City of Orlando
                                       </p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Kiki Grossman</h5>
                                       									
                                       <p>Writer and Speaker on Dispute Resolution and Law</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Aminah Hamidullah</h5>
                                       									
                                       <p>Founder and Director, Knowledge for Living, Inc.</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Greg Higgerson</h5>
                                       									
                                       <p>Vice President of Development, Second Harvest Food Bank</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Dr. Stacey Johnson</h5>
                                       									
                                       <p>President, East and Winter Park Campuses, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Liz Jusino</h5>
                                       									
                                       <p>Coordinator of the Career Program Advisors, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Pam Kancher</h5>
                                       									
                                       <p>Executive Director, Holocaust Memorial and Resource Education Center of Florida</p>
                                       								
                                    </li>
                                    							
                                 </ul>
                                 						
                              </div>
                              						
                              <div class="col-md-6 col-sm-6">
                                 							
                                 <ul class="list_teachers">
                                    								
                                    <li>
                                       									
                                       <h5>Rabbi David Kay</h5>
                                       									
                                       <p>Rabbi, Congregation Ohev Shalom</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Lauri Lott</h5>
                                       									
                                       <p>Adjunct Professor of Education, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Debidatta Aurobinda Mahapatra, PhD</h5>
                                       									
                                       <p>Director, Mahatma Gandhi Center for Non-Violence, Human Rights and World Peace, Hindu
                                          University of America
                                       </p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Dr. James McDonald</h5>
                                       									
                                       <p>Dean, Career and Tech Programs, Business and IT, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Cindy Moon</h5>
                                       									
                                       <p>Head of School, Park Maitland School</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Imam Muhammad Musri</h5>
                                       									
                                       <p>President, Islamic Society of Central Florida</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Patricia Newton</h5>
                                       									
                                       <p>Human Rights Investigation Supervisor, Housing and Community Development, City of
                                          Tampa
                                       </p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>LaFontaine E. Oliver</h5>
                                       									
                                       <p>President and General Manager, WMFE 90.7</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Krystal Pherai</h5>
                                       									
                                       <p>Staff Assistant, Peace and Justice Institute, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Marli Porth</h5>
                                       									
                                       <p>Research Fellow, Harvard Kennedy School Corporate Social Responsibility Initiative</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Dr. Yasmeen Qadri</h5>
                                       									
                                       <p>Professor of Education, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Paul Rooney</h5>
                                       									
                                       <p>Assistant Vice President, Safety/Security Risk Management Services, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Dr. M.C. Santana</h5>
                                       									
                                       <p>Director, Women's and Gender Studies Program, University of Central Florida</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Resham Shirsat</h5>
                                       									
                                       <p>Director of Sustainability, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Nina Streich</h5>
                                       									
                                       <p>Executive Director, Global Peace Film Festival</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Andrew Thomas</h5>
                                       									
                                       <p>Senior Project Manager, City of Sanford</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Subhas Tiwari</h5>
                                       									
                                       <p>Professor of Political Science, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Penny Villegas</h5>
                                       									
                                       <p>Professor Emeritus, Peace Studies, Valencia College</p>
                                       								
                                    </li>
                                    								
                                    <li>
                                       									
                                       <h5>Bickley Wilson</h5>
                                       									
                                       <p>Founder of ArtReach Orlando</p>
                                       								
                                    </li>
                                    							
                                 </ul>
                                 						
                              </div>
                              					
                           </div>
                           				
                        </div>
                        			
                     </div>
                     		
                  </div>
                  	
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/advisory-council.pcf">©</a>
      </div>
   </body>
</html>