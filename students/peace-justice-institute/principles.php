<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Principles | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/principles.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Principles</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               			
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <div class="box_style_1">
                           					
                           <h2>Principles</h2>
                           					
                           <h3>HOW WE TREAT EACH OTHER</h3>
                           					
                           <h4>Our Practice of Respect and Community Building</h4>
                           			
                           						
                           <ol>
                              							
                              <li><strong>Create a hospitable and accountable community.</strong>&nbsp;&nbsp;&nbsp; We all arrive in isolation and need the generosity of friendly welcomes.&nbsp;&nbsp; Bring
                                 all of yourself to the work in this community.&nbsp; Welcome others to this place and this
                                 work, and presume that you are welcomed as well.&nbsp; Hospitality is the essence of restoring
                                 community
                              </li>
                              							
                              <li><strong>Listen deeply.</strong>&nbsp; Listen intently to what is said; listen to the feelings beneath the words.&nbsp;&nbsp; Strive
                                 to achieve a balance between listening and reflecting, speaking and acting.
                              </li>
                              							
                              <li><strong>Create an advice free zone.</strong>&nbsp; Replace advice with curiosity as we work together for peace and justice.&nbsp; Each of
                                 us is here to discover our own truths.&nbsp; We are not here to set someone else straight,
                                 to “fix” what we perceive as broken in another member of the group.
                              </li>
                              							
                              <li><strong>Practice asking honest and open questions.</strong>&nbsp; A great question is ambiguous, personal and provokes anxiety.
                              </li>
                              							
                              <li><strong>Give space for unpopular answers.</strong>&nbsp; Answer questions honestly even if the answer seems unpopular.&nbsp; Be present to listen
                                 not debate, correct or interpret.
                              </li>
                              							
                              <li><strong>Respect silence.&nbsp;</strong> Silence is a rare gift in our busy world.&nbsp; After someone has spoken, take time to
                                 reflect without immediately filling the space with words.&nbsp; This applies to the speaker,
                                 as well – be comfortable leaving your words to resound in the silence, without refining
                                 or elaborating on what you have said.
                              </li>
                              							
                              <li><strong>Suspend judgment.</strong>&nbsp; Set aside your judgments.&nbsp; By creating a space between judgments and reactions,
                                 we can listen to the other, and to ourselves, more fully.
                              </li>
                              							
                              <li><strong>Identify assumptions.</strong>&nbsp; Our assumptions are usually invisible to us, yet they undergird our worldview.&nbsp;
                                 By identifying our assumptions, we can then set them aside and open our viewpoints
                                 to greater possibilities.
                              </li>
                              							
                              <li><strong>Speak your truth</strong>.&nbsp; You are invited to say what is in your heart, trusting that your voice will be
                                 heard and your contribution respected.&nbsp; Own your truth by remembering to speak only
                                 for yourself.&nbsp; Using the first person “I” rather than “you” or “everyone” clearly
                                 communicates the personal nature of your expression.
                              </li>
                              							
                              <li><strong>When things get difficult, turn to wonder.</strong>&nbsp; If you find yourself disagreeing with another, becoming judgmental, or shutting
                                 down in defense, try turning to wonder: “I wonder what brought her to this place?”&nbsp;
                                 "I wonder what my reaction teaches me?” “I wonder what he’s feeling right now?
                              </li>
                              							
                              <li><strong>Practice slowing down.</strong>&nbsp; Simply the speed of modern life can cause violent damage to the soul.&nbsp; By intentionally
                                 practicing slowing down we strengthen our ability to extend community building to
                                 others—and to ourselves.
                              </li>
                              							
                              <li><strong>All voices have value.&nbsp;</strong> Hold these moments when a person speaks as precious because these are the moments
                                 when a person is willing to stand for something, trust the group and offer something
                                 he or she sees as valuable.
                              </li>
                              							
                              <li><strong>Maintain confidentiality.&nbsp;</strong> Create a safe space by respecting the confidential nature and content of discussions
                                 held in the group.&nbsp; Allow what is said in the group to remain there.
                              </li>
                              						
                           </ol>
                           						
                           <p><strong><em>Prepared by the Peace and Justice Institute with considerable help from the works
                                    of Peter Block, Parker Palmer, the Dialogue Group and the Center for Renewal and Wholeness
                                    in Higher Education</em></strong></p>
                           						
                           <p>For a one page printable version of the Principles for How We Treat Each Other access
                              these links:<br>
                              							<strong><a href="/students/peace-justice-institute/documents/how-we-treat-each-other-full-page.pdf" target="_blank">English</a> <a href="/students/peace-justice-institute/documents/how-we-treat-each-other-full-page-spanish-version.pdf" target="_blank">Spanish</a> <a href="/students/peace-justice-institute/documents/how-we-treat-each-other-full-page-creole-version.pdf" target="_blank">Creole</a></strong></p>
                           						
                           <p>For a half page printable version of the Principles for How We Treat Each Other access
                              these links:<br> <a href="/students/peace-justice-institute/documents/how-we-treat-each-other-half-sheets.pdf" target="_blank"><strong>
                                    						English</strong></a> <a href="/students/peace-justice-institute/documents/spanish-half-page-principles-for-how-we-treat-each-other.pdf" target="_blank"><strong>Spanish</strong><br></a></p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/principles.pcf">©</a>
      </div>
   </body>
</html>