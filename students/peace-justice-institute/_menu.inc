<ul>

	<li class="submenu"><a class="show-submenu" href="/students/peace-justice-institute/">Peace and Justice Institute <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		<li><a href="/students/peace-justice-institute/principles.php">Principles</a></li>
		<li><a href="/students/peace-justice-institute/advisory-council.php">Advisory Council</a></li>
		<li><a href="/students/peace-justice-institute/office-staff.php">Office Staff</a></li>	
</ul>
	</li>	
	
	
	
	
<li class="submenu"><a class="show-submenu" href="#">Students <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		 <li><a href="/students/peace-justice-institute/students/index.php">Students</a></li>
		<li><a href="/students/peace-justice-institute/students/curriculum.php">Curriculum</a></li>
		<li><a href="/students/peace-justice-institute/students/peace-justice-institute-distinction.php">Peace and Justice Distinction</a></li>
  <li><a href="/students/peace-justice-institute/students/ambassadors.php">Ambassadors
 </a></li>
<li><a href="/students/peace-justice-institute/students/peace-through-service-program.php">Peace Through Service Program</a></li>
<li><a href="/students/peace-justice-institute/students/leadership-fellows.php">Leadership Fellows</a></li>		
</ul>
	</li>	
	
<li class="submenu"><a class="show-submenu" href="/students/peace-justice-institute/faculty-staff/index.php">Faculty &amp; Staff <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		 <li><a href="/students/peace-justice-institute/faculty-staff/faculty-curriculum.php">Faculty Curriculum</a></li>
		<li><a href="/students/peace-justice-institute/faculty-staff/staff-curriculum.php">Staff Curriculum</a></li>
			
</ul>
	</li>	
	
	<li class="submenu"><a class="show-submenu" href="#">Events &amp; News <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		 <li><a href="/students/peace-justice-institute/news/index.php">News</a></li>
		<li><a href="/students/peace-justice-institute/news/newsletter.php">Newsletter</a></li>
		<li><a href="/students/peace-justice-institute/news/media-mentions.php">Media Mentions</a></li>
		<li><a href="/students/peace-justice-institute/events/index.php">Events</a></li>
  <li><a href="/students/peace-justice-institute/events/past-events.php">Past Events</a></li>
</ul>
	</li>	
	<li class="submenu"><a class="show-submenu" href="/students/peace-justice-institute/programs/index.php">Programs <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		 <li><a href="/students/peace-justice-institute/programs/legal-education-action-project.php">>Legal Education Action Project (LEAP)</a></li>
		   <li><a href="/students/peace-justice-institute/programs/orlando-speaks.php">Orlando Speaks</a></li>
		    <li><a href="/students/peace-justice-institute/programs/continuing-ed.php">Continuing Ed</a></li>        
			<li><a href="/students/peace-justice-institute/programs/global-peace-week.php">Global Peace Week</a></li>
			<li><a href="/students/peace-justice-institute/programs/conversation-on-justice.php">Conversation on Justice</a></li>
			<li><a href="/students/peace-justice-institute/programs/global-outreach.php">Global Outreach</a></li>
			<li><a href="/students/peace-justice-institute/programs/past-speakers.php">Past Speakers</a></li>
		
</ul>
	</li>	
	
	<li class="submenu"><a class="show-submenu" href="/students/peace-justice-institute/resources.php">Resources <i class="fas fa-chevron-down" aria-hidden="true"> </i></a>
    <ul>
		<li><a href="/students/peace-justice-institute/terminology.php">Terminology</a></li>
			<li><a href="/students/peace-justice-institute/what-we-are-reading.php">What We Are Reading</a></li>
		
</ul>
	</li>

	
	<li><a href="http://net4.valenciacollege.edu/forms/PJI/contact.cfm" target="_blank">Contact Us</a></li>
</ul>
