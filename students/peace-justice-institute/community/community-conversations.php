<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Community Conversations  | Valencia College</title>
      <meta name="Description" content="List of Private Colleges, Universities, and Technical Schools that have Agreements with Valencia">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/community/community-conversations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li><a href="/students/peace-justice-institute/community/">Community</a></li>
               <li>Community Conversations </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <div class="indent_title_in">
                           
                           <h3>Community Conversations</h3>
                           
                           <p>Orlando Speaks: Community Conversations</p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p align="center"><img src="/_resources/img/students/peace-justice-institute/CityofOrlando_000.jpg" width="115" height="115" alt="City of Orlando logo"> <img src="/_resources/img/students/peace-justice-institute/OPD_000.gif" width="115" height="119" alt="Orlando Police Department logo"></p>
                                 
                                 
                                 <p>In a report issued by the Department of Justice, dialogue is called for as urgently
                                    needed to quell
                                    civil unrest and serve as a preventative measure to potential violence. Orlando Speaks,
                                    designed in
                                    partnership with the City of Orlando and facilitated by the Peace and Justice Institute
                                    of Valencia
                                    College, is an answer to that call. Under the leadership of Mayor Buddy Dyer, and
                                    in partnership with
                                    Valencia College's Peace and Justice Institute, the City of Orlando is taking steps
                                    to bridge divides
                                    and create authentic dialogue between citizens and police. In response to concerns
                                    about excessive use
                                    of force and inequitable policing practices in communities of color and poor communities,
                                    the Mayor,
                                    Chief of Police and City Commissioners of Orlando agreed to bring citizens and officers
                                    together to
                                    have the difficult conversation about how we treat each other and how we can strengthen
                                    trust.
                                 </p>
                                 
                                 
                                 
                                 <p>These custom designed community dialogues, originally launched as Orlando Speaks,
                                    and generally
                                    referred to as <strong>Our</strong> <strong>Community Speaks</strong>, establish a safe space for dialogue where
                                    authentic voices come together to share stories, fears, and concerns, with an eye
                                    toward finding
                                    common ground among two groups that often struggle together - police and citizens.
                                    <strong>Our</strong> <strong>Community Speaks</strong> is a series of interactive workshops that utilizes
                                    innovative communication strategies to foster ongoing citizen engagement and conversation
                                    between
                                    residents, the City and the local Police Department to better serve and celebrate
                                    the community's
                                    diversity. Workshops are custom designed so as to be timely and meet local community
                                    needs.
                                    
                                 </p>
                                 
                                 
                                 <p>Recognizing that unconscious bias can be overcome by cross-cultural communication,
                                    <strong>Our</strong> <strong>Community Speaks</strong> creates safe spaces for authentic dialogue about our
                                    experiences of difference, including but not limited to race. By examining existing
                                    cultural and
                                    systemic structures, the experience increases understanding and positive change within
                                    the community.
                                    Having these conversations is critical to moving a City forward, bringing the community
                                    closer
                                    together, and ensuring that the City remains safe, inclusive and accepting of all.
                                    
                                 </p>
                                 
                                 <p align="center"><img src="/_resources/img/students/peace-justice-institute/OrlandoSpeaks_04_000.JPG" width="300" height="188" alt="Orlando Speaks Image- Citizens talking with police officer"></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        <div class="indent_title_in">
                           
                           <h3>The Goal of Orlando Speaks</h3>
                           
                           <p></p>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p></p>
                           
                           <div class="row">
                              
                              <div class="col-md-12">
                                 
                                 <p>The goal of <strong>Our</strong> <strong>Community Speaks</strong> is to bring voices to the table that don't
                                    often hear one another - and to create a setting where the truth can be heard - through
                                    dialogue not
                                    debate. Understanding that relationships across lines of difference are essential
                                    for the possibility
                                    of social transformation, <strong>Our</strong> <strong>Community Speaks</strong> works toward the following
                                    goals:<br>
                                    
                                    <section>
                                       
                                       <ul class="list_style_1">
                                          
                                          <li> Create a safe space for open, honest communication</li>
                                          
                                          <li>Strengthen interpersonal relationships through the sharing of personal stories and
                                             experiences
                                             
                                          </li>
                                          
                                          <li>Develop trust and sensitivity to support interactions with one another</li>
                                          
                                          <li>Expand citizen engagement</li>
                                          
                                          <li>Increase awareness and understanding of policing practices</li>
                                          
                                          <li>Strengthen police legitimacy</li>
                                          
                                       </ul>
                                       
                                    </section>
                                 </p>
                                 
                                 
                                 <p>Surprisingly, many Police are eager to participate in <strong>Our</strong> <strong>Community Speaks</strong>,
                                    with volunteer attendance often exceeding the need. Citizens from all walks of life
                                    bring their voices
                                    to the table, demonstrating communities ready to take action.
                                 </p>
                                 
                                 <p align="center"><img src="/_resources/img/students/peace-justice-institute/OrlandoSpeaks_01_000.JPG" width="300" height="201" alt="Police Officer and youth at Orlando Speaks"></p>
                                 
                                 <p><strong>Our</strong> <strong>Community Speaks</strong> provides guidelines for talking that invite participants
                                    to speak their truth, suspend judgment and turn to wonder, among other practices.
                                    The program
                                    structures the time in such a way that everyone in the room is an active participant
                                    - not a mere
                                    bystander listening to others speak. "All People. All Voices. All Matter." is the
                                    vision for the work
                                    and for the Peace and Justice Institute (PJI). Cooperation and partnership between
                                    city government,
                                    including the Mayor, the Chief of Police, leadership from the local police department,
                                    community
                                    stakeholders and PJI fosters a meaningful program with positive outcomes.
                                 </p>
                                 
                                 <p>Participant feedback from both officers and citizens has been overwhelmingly positive.
                                    One resident
                                    spoke about the event saying: "I recently had the pleasure of attending "Orlando Speaks"
                                    with my
                                    coworkers and members of our organization, Organize Now. All of us were incredibly
                                    impressed with the
                                    presentation and left feeling inspired and ready to take even more action in our community.
                                    Thank you
                                    for the work that you do in helping to empower our community."
                                 </p>
                                 
                                 <p><strong>Our</strong> <strong>Community Speaks</strong> creates authentic dialogue, strengthens relationships
                                    between police and citizens, and helps communities embrace diversity and foster inclusive
                                    excellence
                                    among us.
                                 </p>
                                 
                                 
                                 <p>To read more feedback from Orlando Speaks, please <a href="https://www.dropbox.com/s/cun7l8tifq67drp/Orlando%20Speaks%20Participant%20Comments.pdf?dl=0" target="_blank">follow this link.</a></p>
                                 
                                 <p align="center"><img src="/_resources/img/students/peace-justice-institute/OrlandoSpeaks_11.JPG" width="300" height="167" alt="Officer and citizens talking at Orlando Speaks">
                                    
                                 </p>
                                 
                                 <p><strong>Our</strong> <strong>Community Speaks</strong> is a fee based program. For more information contact
                                    Rachel Allen, Peace and Justice Institute Coordinator at <a href="mailtoRallen39@valenciacollege.">Rallen39@valenciacollege.edu</a>
                                    a or call 407-582-2709.
                                    
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
                  <aside class="col-md-3">
                     
                     <div>
                        <a href="/documents/employees/peace-justice-institute/PJINewsletter15.pdf"><img src="/_resources/img/students/peace-justice-institute/right-peace-newsletter.png"></a>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h5>Orlando Speaks</h5>
                        <i class="icon_desktop"></i>
                        
                        
                        <p>Community dialogue to strengthen the relationship between citizens and police:<br><a href="/STUDENTS/offices-services/peace-justice-institute/community/community-conversations.html">learn
                              more</a></p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           
                           
                        </p>
                        
                     </div>
                     
                     <hr class="styled">
                     
                     <div class="box_side">
                        
                        <p>
                           <iframe src="//www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fpages%2FPeace-and-Justice-Institute%2F317736788242008&amp;width=245&amp;height=350&amp;colorscheme=light&amp;show_faces=false&amp;border_color&amp;stream=true&amp;header=false" scrolling="no" frameborder="0" allowtransparency="true"></iframe>
                           
                        </p>
                        
                     </div>
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/community/community-conversations.pcf">©</a>
      </div>
   </body>
</html>