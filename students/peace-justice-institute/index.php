<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute | Valencia College</title>
      <meta name="Description" content="The Peace and Justice Institute at Valencia College promotes peace and justice for all. Our aim is to nurture an inclusive, caring and respectful environment on campus and within our community - one where conflict leads to growth and transformation rather than violence or aggression.">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p></p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Peace Justice Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div class="main-title">
                           <td class="main-title">
                              	<img src="/students/peace-justice-institute/images/peace-and-justice-institute-logo.png" alt="Peace and Justice Institute Logo"><br><br>
                              	
                              <h2>Peace and Justice Institute</h2>
                              
                              <p><span>The Peace and Justice Institute at Valencia College promotes peace and justice for
                                    all. Our aim is to nurture an inclusive, caring and respectful environment on campus
                                    and within our community--one where conflict leads to growth and transformation, rather
                                    than violence or aggression.</span></p>
                              
                           </td>
                        </div>
                        
                        <div class="row">
                           <div class="col-md-8 col-sm-8">
                              	
                              <h3>What We Do</h3>
                              	
                              <p></p>
                              	
                              <ul>
                                 		
                                 <li>Teach a Peace and Justice Studies curriculum at Valencia.</li>
                                 		
                                 <li>Work with Associate in Science (A.S.) programs at Valencia, including criminal justice
                                    and nursing, to integrate peace studies into their curriculum, ultimately affecting
                                    the way people in these industries will handle conflict in their work.
                                 </li>
                                 		
                                 <li>Promote peace and justice events and activities through sponsorships and collaborations
                                    with community and student groups.
                                 </li>
                                 		
                                 <li>Facilitate conversations on campus and in the community about peace, conflict resolution,
                                    human rights, race, and privilege.
                                 </li>
                                 		
                                 <li>Host nationally known experts in peace and justice studies as speakers and scholars
                                    in residence.
                                 </li>
                                 		
                                 <li>Provide community service and outreach.</li>
                                 		
                                 <li>Provide students with hands-on experiences such as internships and service-learning
                                    opportunities to give practical meaning to the theories of peace building.
                                 </li>
                                 		
                                 <li>Offer workshops and courses for business, government, nonprofit, and community organizations
                                    in partnership with Valencia's Professional Continuing Education.
                                 </li>
                                 	
                              </ul>
                              	
                              <p></p>
                              
                              
                              
                              <h3>Why It Matters</h3>
                              	
                              <p>Studying peace benefits all of us--it leads to better people and better societies.
                                 It provides a new approach and philosophy toward conflict transformation. It allows
                                 people to make a difference by "being the peace" in their own relationships, their
                                 neighborhood, the college campus, and even the world.
                              </p>
                              	
                              <p>Peace and Justice Studies is also an excellent foundation for students interested
                                 in a broad spectrum of careers, including but not limited to: education, law enforcement,
                                 social services, community organizations, the Peace Corps, the U.S. Department of
                                 State and non-government organizations.
                              </p>
                              
                              
                              
                              <h3>Office Staff</h3>
                              
                              <ul class="list_staff">
                                 <li>
                                    <figure><img src="/students/peace-justice-institute/images/contact-rachel-allen.jpg" alt="Rachel Allen" class="img-circle"></figure>
                                    <h4>Rachel Allen</h4>
                                    <p>Peace and Justice Institute Director</p>
                                 </li>
                                 <li>
                                    <figure><img src="/students/peace-justice-institute/images/no-photo-male-thumb.png" alt="William Jefferson" class="img-circle"></figure>
                                    <h4>William Jefferson</h4>
                                    <p>Community Manager</p>
                                 </li>
                                 <li>
                                    <figure><img src="/students/peace-justice-institute/images/no-photo-female-thumb.png" alt="Ann Persaud" class="img-circle"></figure>
                                    <h4>Ann Persaud</h4>
                                    <p>Staff Assistant II</p>
                                 </li>
                              </ul>
                              					
                           </div>
                           <div class="col-md-4 col-sm-4">
                              <div class="box_style_4">
                                 
                                 <h4>Goals of the Institute</h4>
                                 
                                 <p>All People. All Voices. All Matter.</p>
                                 
                                 <ul class="list_order">
                                    <li><span>1</span>Create and teach a Peace and Justice Studies curriculum
                                    </li>
                                    <li><span>2</span>Sponsor and collaborate on peace and justice co-curricular activities
                                    </li>
                                    <li><span>3</span>Foster a connection with Valencia's A.S. programs in peace studies and conflict transformation
                                    </li>
                                    <li><span>4</span>Offer community outreach in peace and justice
                                    </li>
                                    <li><span>5</span>Engage in realizing Valencia's core competencies
                                    </li>
                                 </ul>
                                 
                              </div>
                           </div>
                        </div>
                        
                        <hr class="more_margin">
                        
                        	
                        <div class="container margin-30">
                           	
                           <section class="grid">
                              <div class="container margin-60">
                                 <div class="main-title">
                                    						
                                    <h2>Peace and Justice Institute YouTube Videos</h2>
                                    						
                                    <p></p>
                                    					
                                 </div>
                                 <div class="row">
                                    						
                                    <ul class="magnific-gallery">
                                       <li>
                                          <figure><img src="/students/peace-justice-institute/images/peace-justice-institute-gallery5-thumb.png" alt="Valencia College PJI Celebrates 10 years video preview"><figcaption>
                                                <div class="caption-content"><a href="https://www.youtube.com/watch?v=dZSC1Qo9sOA" class="video_pop" title="Valencia College PJI Celebrates 10 years"><i class="far fa-film"></i><p>
                                                         										
                                                         									 Valencia College PJI Celebrates 10 years Link
                                                      </p></a></div>
                                             </figcaption>
                                          </figure>
                                       </li>
                                    </ul>
                                    					
                                 </div>
                              </div>
                           </section>
                           		   
                           <hr class="styled_2">
                           	
                        </div>
                        
                        	  
                        		  
                        <div class="container margin-30">
                           
                           <h4 class="stories"><strong>photo gallery</strong></h4>
                           
                        </div>
                        <div class="grid">
                           
                           <ul class="magnific-gallery">
                              <li>
                                 <figure><img src="/students/peace-justice-institute/images/peace-justice-institute-gallery1-thumb.png" alt="Image Gallery Picture 1"><figcaption>
                                       <div class="caption-content"><a href="/students/peace-justice-institute/images/peace-justice-institute-gallery1.png" title="Peace and Justice Institute audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                                										
                                                									 Peace and Justice Institute 
                                                										
                                                									
                                             </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                              <li>
                                 <figure><img src="/students/peace-justice-institute/images/peace-justice-institute-gallery2-thumb.png" alt="Image Gallery Picture 2"><figcaption>
                                       <div class="caption-content"><a href="/students/peace-justice-institute/images/peace-justice-institute-gallery2.png" title="Peace and Justice Institute audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                                										
                                                									 Peace and Justice Institute 
                                                										
                                                									
                                             </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                              <li>
                                 <figure><img src="/students/peace-justice-institute/images/peace-justice-institute-gallery3-thumb.png" alt="Image Gallery Picture 3"><figcaption>
                                       <div class="caption-content"><a href="/students/peace-justice-institute/images/peace-justice-institute-gallery3.png" title="Peace and Justice Institute audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                                										
                                                									 Peace and Justice Institute 
                                                										
                                                									
                                             </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                              <li>
                                 <figure><img src="/students/peace-justice-institute/images/peace-justice-institute-gallery4-thumb.png" alt="Image Gallery Picture 4"><figcaption>
                                       <div class="caption-content"><a href="/students/peace-justice-institute/images/peace-justice-institute-gallery4.png" title="Peace and Justice Institute audience members listening in to the event" data-effect="mfp-move-horizontal"><i class="far fa-picture-o" aria-hidden="true"></i><p>
                                                										
                                                									 Peace and Justice Institute 
                                                										
                                                									
                                             </p></a></div>
                                    </figcaption>
                                 </figure>
                              </li>
                           </ul>
                           
                        </div>
                        		  
                        <hr class="styled_2">
                        
                        <h3>Campus Locations &amp; Contacts</h3>
                        
                        
                        <div class="wrapper_indent">
                           
                           <div class="row">
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>Peace and Justice Institute Director</h4>
                                 
                                 <p><strong>Rachel Allen</strong><br>
                                    			 <a href="tel:407-582-2709">407-582-2709</a></p>
                                 			  
                                 			  
                                 <h4>East Campus</h4>
                                 
                                 <p><strong>Chris Cuevas</strong><br>
                                    			 <a href="tel:407-582-2530">407-582-2530</a></p>
                                 			  
                                 			
                                 <h4>Lake Nona and Osceola Campuses</h4>
                                 				
                                 <p><strong>Jennifer Keefe</strong><br>
                                    					 <a href="tel:321-682-4823">321-682-4823</a></p>	
                                 			  
                                 			  
                              </div>
                              
                              <div class="col-md-6 col-sm-6">
                                 
                                 <h4>West Campus:</h4>
                                 				
                                 <p><strong>Mayra Holzer</strong><br>
                                    					 <a href="tel:407-582-1068">407-582-1068</a></p>
                                 				
                                 				
                                 <h4>Winter Park Campus</h4>
                                 				
                                 <p><strong>Paul Chapman</strong><br>
                                    					 <a href="tel:407-582-6819">407-582-6819</a></p>
                                 
                              </div>
                              	
                           </div>
                           		
                        </div>
                        	   
                        <hr class="styled_2">
                        
                        
                        <h3>Resources</h3>
                        
                        <p>
                           <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank">DONATE</a><br>
                           <a href="/pji/documents/Global-Peace-Week-2017-Master-Schedule.pdf" target="_blank">Global Peace Week Schedule</a><br>
                           <a href="/PJI/news/documents/PJI-Newsletter-Vol-8-No-2.pdf" target="_blank">Peace and Justice Institute Newsletter</a></p> 
                        
                        	
                     </div>
                     	  
                  </div>
                  
               </div>
               
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/index.pcf">©</a>
      </div>
   </body>
</html>