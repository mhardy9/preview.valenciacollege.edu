<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Peace and Justice Institute | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/office-staff.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Peace and Justice Institute</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <section>
                           <h2>Office Staff</h2>
                           
                           
                           
                           
                           <h3>Contact The Peace and Justice Office: </h3>
                           
                           <p><img alt="Rachel Allen, Coordinator" height="200" src="IMG_2304_000.JPG" title="Rachel Allen, Coordinator" width="267"><br>
                              Rachel Allen <br>
                              Professor of Humanities<br>
                              Peace and Justice Institute Coordinator<br>
                              Office: East 1-340 <br>
                              Phone: 407-582-2709<br>
                              Email : <a href="mailto:rallen39@valenciacollege.edu">Rallen39@valenciacollege.edu</a></p>
                           
                           
                           
                           
                           
                           <p><img alt="Krystal Pherai, Staff Assistant" height="250" src="contact-krystal-pherai.png" title="Krystal Pherai, Staff Assistant" width="175"><br>
                              Krystal Pherai <br>
                              Staff Assistant <br>
                              Peace and Justice Ambassadors Co-Coordinator
                              <br>
                              Office: East 1-340 <strong></strong><br>
                              Phone: 407-582-2291<br>
                              Email: <a href="mailto:Kpherai@valenciacollege.edu">Kpherai@valenciacollege.edu<br>
                                 <br>
                                 </a></p>
                           
                           <span>
                              
                              <h2>Campus Representatives</h2>
                              
                              
                              <h3>East Campus: 
                                 
                              </h3>
                              
                              <p><img alt="Chris Cuevas" height="250" src="contact-chris-cuevas.png" title="Chris Cuevas" width="175"><br>Chris Cuevas<br>
                                 <a href="mailto:ccuevas14@valenciacollege.edu">Ccuevas14@valenciacollege.edu</a><br>
                                 Office: 407-582-2530
                              </p>
                              
                              <p>AJ Quaskerbush<br>
                                 Professor of Peace Studies and Political Science<br>
                                 <a href="mailto:Aquackenbush@valenciacollege.edu">Aquackenbush@valenciacollege.edu</a><br>
                                 Office: 407-582-2211
                              </p>
                              
                              
                              <p>Nicole Valentino<br>
                                 <a href="mailto:nvalentino@valenciacollege.edu">Nvalentino@valenciacollege.edu</a><br>
                                 Office: 407-582-2420
                              </p>
                              
                              
                              
                              
                              
                              <h3>Lake Nona and Osceola Campuses: </h3>
                              
                              <p><img alt="Jennifer Keefe" height="250" src="contact-jennifer-keefe.png" title="Jennifer Keefe" width="175"><br>
                                 Jennifer Keefe&nbsp;<br>
                                 <a href="mailto:Jkeefe1@valenciacollege.edu" rel="noreferrer">Jkeefe1@valenciacollege.edu</a>&nbsp;<br>
                                 321-682-4823
                              </p>
                              
                              
                              <h3>West Campus: </h3>
                              
                              <p>Mayra Holzer <br>
                                 <a href="mailto:Mholzer@valenciacollege.edu">Mholzer@valenciacollege.edu</a><br>
                                 Office: 407-582-1068
                              </p>
                              
                              <p>Michele Lima<br>
                                 mlima2@valenciacollege.edu
                                 <br>
                                 Office: 407-582-1414
                              </p>
                              
                              <p><img alt="Subhas Rampersaud" height="250" src="contact-subhas-rampersaud.png" title="Subhas Rampersaud" width="175"><br>Subhas Rampersaud<br>
                                 <a href="mailto:Srampersaud1@valenciacollege.edu">Srampersaud1@valenciacollege.edu<br>
                                    </a>Office: 407-582-1214
                              </p>
                              
                              
                              <h3>Winter Park Campus: </h3>
                              
                              <p>Paul Chapman&nbsp;<br>
                                 <a href="mailto:Pchapman@valenciacollege.edu" rel="noreferrer">Pchapman@valenciacollege.edu</a>&nbsp;<br>
                                 Office: 407-582-6819<br>
                                 
                                 
                                 
                              </p>
                              
                              
                              
                              
                              </span></section>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <a href="https://donate.valencia.org/peace-and-justice-institute" target="_blank">DONATE</a>
                        <a href="documents/Global-Peace-Week-2017-Master-Schedule.pdf" target="_blank">Global Peace Week Schedule</a>
                        
                        
                        
                        
                        
                        
                        
                        <center></center>
                        
                        
                        
                        <div>
                           
                           <ol><em>
                                 <center>Goals of the Institute are to:</center></em></ol>
                           
                           <div> 
                              <ol>1. Create and teach a <a href="students/curriculum.html">Peace and Justice Studies</a> curriculum <br> 2. Sponsor and collaborate on peace and justice events<br>
                                 3. Foster a connection with Valencia's A.S. programs in peace studies and conflict
                                 transformation<br>
                                 4. Offer community outreach in peace and justice<br>
                                 5. Engage in realizing Valencia's core competencies<br>
                                 
                              </ol>
                              
                              
                              
                              
                              
                           </div>
                           
                        </div>
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/office-staff.pcf">©</a>
      </div>
   </body>
</html>