<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Terminology  | Valencia College</title>
      <meta name="Description" content="Peace &amp; Justice Institute">
      <meta name="Keywords" content="peace in justice, all people, all voices, all matter, peace and justice institute, peace, justice, college, school, educational programs, continuing ed, global peace week, conversation on justice, global outreach, speakers, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/peace-justice-institute/terminology.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/peace-justice-institute/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Peace and Justice Institute</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/peace-justice-institute/">Peace Justice Institute</a></li>
               <li>Terminology </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  		
                  <div class="row">
                     			
                     <div class="col-md-12">
                        				
                        <h2>Terminology</h2>
                        					
                        <p>Glossary of peace and justice related terms*</p>
                        					
                        <div>
                           						
                           <h4>Active Listening</h4>
                           						
                           <div>
                              							
                              <div>
                                 								A way of listening that focuses on both the content of statements or responses
                                 in a dialogue and the underlying emotions. It means asking open-ended questions, seeking
                                 clarification, asking for specificity, and confirming your understanding of what the
                                 other party has said.
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Bias</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Intentional and unintentional, conscious and subconscious, attitudes, behaviors and
                                    actions that have a negative and differential impact on segments of the society, or
                                    favor one segment of the society.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Conflict</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>An inevitable aspect of human interaction, conflict is present when two or more individuals
                                    or groups pursue mutually incompatible goals. Conflicts can be waged violently, as
                                    in a war, or nonviolently, as in an election or an adversarial legal process. When
                                    channeled constructively into processes of resolution, conflict can be beneficial.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Conflict Analysis</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The systematic study of conflict in general and of individual or group conflicts in
                                    particular. Conflict analysis provides a structured inquiry into the causes and potential
                                    trajectory of a conflict so that processes of resolution can be better understood.
                                    For specific conflicts, the terms conflict assessment or conflict mapping are sometimes
                                    used to describe the process of identifying the stakeholders, their interests and
                                    positions, and the possibility for conflict management.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Conflict Resolution</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Efforts to address the underlying causes of a conflict by finding common interests
                                    and overarching goals. It includes fostering positive attitudes and generating trust
                                    through reconciliation Institutes, and building or strengthening the institutions
                                    and processes through which the parties interact peacefully.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Conflict Transformation</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Conflict transformation is to envision and respond to the ebb and flow of social conflict
                                    as life-giving opportunities for creating constructive change processes that reduce
                                    violence, increase justice in direct interaction and social structures, and respond
                                    to real-life problems in human relationships.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Culture</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The shared beliefs, traits, attitudes, behavior, products, and artifacts common to
                                    a particular social or ethnic group. The term cross-cultural refers to interactions
                                    across cultures and reflects the fact that different cultures may have different communication
                                    styles and negotiating behavior. The term multicultural refers to the acceptance of
                                    different ethnic cultures within a society. Cultural sensitivity means being aware
                                    of cultural differences and how they affect behavior, and moving beyond cultural biases
                                    and preconceptions to interact effectively.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Democracy</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>A state or community in which all adult members of society partake in a free and fair
                                    electoral process that determines government leadership, have access to power through
                                    their representatives, and enjoy universally recognized freedoms and liberties. Democracy
                                    building or democratization is the exercise of consolidating and strengthening institutions
                                    that help to support democratic government. These institutions may relate to rule
                                    of law Institutes, political party development, constitution building, public administration
                                    development, and civil society education programs.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Dialogue</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>A conversation or exchange of ideas that seeks mutual understanding through the sharing
                                    of perspectives. Dialogue is a process for learning about another group’s beliefs,
                                    feelings, interests, and needs in a non-adversarial, open way, usually with the help
                                    of a third-party facilitator. Facilitated dialogue is a face-to-face process, often
                                    among elites. It takes place at a meeting site, whereas other third-party assisted
                                    processes may occur indirectly or by means of proximity talks.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Diversity</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The range of human differences, including but not limited to race, gender, religion,
                                    ability, sexual orientation, personality, body shape, political beliefs, social class,
                                    etc.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Ethics</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The principles of conduct—right and wrong behavior—governing an individual or a group.</p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Facilitation</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The process or set of skills by which a third party attempts to help the disputants
                                    move toward resolution of their dispute. Facilitation can operate at many levels,
                                    from providing good offices to a more active role as a mediator. It may mean helping
                                    the parties set ground rules and agendas for meetings, helping with communication
                                    between the parties, and analysis of the situation and possible outcomes—in general,
                                    helping the participants keep on track and working toward their mutual goals. It may
                                    also mean helping them set those goals.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Human Rights</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The basic prerogatives and freedoms to which all humans are entitled. Supported by
                                    the United Nation’s Universal Declaration of Human Rights of 1948 and several international
                                    agreements, these rights include the right to life, liberty, education, and equality
                                    before law, and the right of association, belief, free speech, religion, and movement.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Identity</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Identity refers to the way people see themselves—the groups they feel a part of, the
                                    aspects of themselves that they use to describe themselves. Some theorists distinguish
                                    between collective identity, social identity, and personal identity. However, all
                                    are related in one way or another to a description of who one is, and how one fits
                                    into his or her social group and society overall. Identity conflicts are conflicts
                                    that develop when a person or group feels that their sense of self is threatened or
                                    denied legitimacy or respect. Religious, ethnic, and racial conflicts are examples
                                    of identity conflicts. Identity politics tries to exploit those conflicts for political
                                    advantage.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Inclusion</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The Involvement and empowerment of diversity, where the inherent worth and dignity
                                    of all people are recognized.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Inclusive Excellence</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The recognition that a community or institution's social and economic success is dependent
                                    on how well it values, engages, includes, and cares for the rich diversity of its
                                    members in every aspect, layer, level, and role in the institution.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Interfaith Dialogue</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Efforts to promote understanding of and cooperation among different faiths, especially
                                    as a tool to advance peacemaking and peacebuilding.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Justice</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The minimum form of justice is fair and equal treatment before the law—a system of
                                    rule of law based on legal procedures that apply to all members of society. Social
                                    justice refers to a situation characterized by rule of law and fair distribution of
                                    resources and opportunities in society. Some see justice as a prerequisite for a stable
                                    and lasting order, others argue that there can be no justice without order. Access
                                    to justice refers to efforts to make the justice system accessible to those who are
                                    otherwise excluded. Non-state justice, also called customary or traditional justice,
                                    refers to the settlement of disputes outside the formal state justice system, for
                                    example through tribal and community councils. Such mechanisms are widely used in
                                    rural and poor urban areas but sometimes reinforce local inequities and social exclusion,
                                    especially concerning women.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Mediation</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>A mode of negotiation in which a mutually acceptable third party helps the parties
                                    to a conflict find a solution that they cannot find by themselves. It is a three-sided
                                    political process in which the mediator builds and then draws upon relationships with
                                    the other two parties to help them reach a settlement.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Micro-aggressions</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The everyday verbal, nonverbal, and environmental slights, snubs, or insults, whether
                                    intentional or unintentional, that communicate hostile, derogatory, or negative messages
                                    to target persons based solely upon their perceived identity.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Negotiation</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The process of communication and bargaining between parties seeking to arrive at a
                                    mutually acceptable outcome on issues of shared concern. The process typically involves
                                    compromise and concessions and is designed to result in an agreement, although sometimes
                                    a party participates in negotiations for other reasons (to score propaganda points
                                    or to appease domestic political forces, for example).
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Nonviolence</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Nonviolence is a powerful method to harmonize relationships among people (and all
                                    living things) for the establishment of justice and the ultimate well-being of all
                                    parties. It draws its power from awareness of the profound truth to which the wisdom
                                    traditions of all cultures, science, and common experience bear witness: that all
                                    life is one.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Peace</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>The word “peace” evokes complex, sometimes contradictory, interpretations and reactions.
                                    For some, peace means the absence of conflict. For others it means the end of violence
                                    or the formal cessation of hostilities; for still others, the return to resolving
                                    conflict by political means. Some define peace as the attainment of justice and social
                                    stability; for others it is economic well-being and basic freedom. Peacemaking can
                                    be a dynamic process of ending conflict through negotiation or mediation. Peace is
                                    often unstable, as sources of conflict are seldom completely resolved or eliminated.
                                    Since conflict is inherent in the human condition, the striving for peace is particularly
                                    strong in times of violent conflict. That said, a willingness to accommodate perpetrators
                                    of violence without resolving the sources of conflict—sometimes called “peace at any
                                    price”—may lead to greater conflict later.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Peace (and conflict) Studies</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>An interdisciplinary field of study that focuses on conflict analysis, conflict management,
                                    and conflict transformation; nonviolent sanctions; peacebuilding, peacekeeping, and
                                    peace enforcement; social and economic justice; war’s causes and conduct; and international
                                    and domestic security. Peace research is a constituent element of peace studies, drawing
                                    on the work of academicians and nongovernmental organizations alike. Although peace
                                    studies generally refers to college-level work, the term peace education encompasses
                                    all levels of students.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Peacebuilding</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Originally conceived in the context of post-conflict recovery efforts to promote reconciliation
                                    and reconstruction, the term peacebuilding has more recently taken on a broader meaning.
                                    It may include providing humanitarian relief, protecting human rights, ensuring security,
                                    establishing nonviolent modes of resolving conflicts, fostering reconciliation, providing
                                    trauma healing services, repatriating refugees and resettling internally displaced
                                    persons, supporting broad-based education, and aiding in economic reconstruction.
                                    As such, it also includes conflict prevention in the sense of preventing the recurrence
                                    of violence, as well as conflict management and post-conflict recovery. In a larger
                                    sense, peacebuilding involves a transformation toward more manageable, peaceful relationships
                                    and governance structures—the long-term process of addressing root causes and effects,
                                    reconciling differences, normalizing relations, and building institutions that can
                                    manage conflict without resorting to violence.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Respect</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>An attitude- and the behaviors that accompany that attitude- that everyone has the
                                    right to be acknowledged as a valuable individual capable of making positive contributions
                                    to the team.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Serial Testimony</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Coined by Dr. Peggy McIntosh as the "autocratic administration of time in the service
                                    of democratic distribution of time." Serial Testimony is a facilitation method that
                                    empowers people by valuing their knowledge and insight. It offers the opportunity
                                    to testify to the realities of their own lives, making their personal reflections
                                    part of the experience. Serial Testimony creates dialogue that doesn't focus on convincing
                                    others or winning an argument, but rather on sharing perspective that can inform how
                                    participants view themselves and the wider world.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Stereotype</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Placing a person in a "mental file," not based on information derived from knowledge
                                    about or personal experience with the person, but based upon what we believe about
                                    a group to which we assume this person belongs.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Sustainability</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>In general, the ability to maintain something indefinitely. In capacity building,
                                    sustainability means creating capacity that will remain in place and effective even
                                    after the Institute ends or the intervener departs. In development, it means meeting
                                    the needs of the present without compromising the ability of future generations to
                                    meet their own needs. In the context of natural resources, sustainability refers to
                                    harnessing natural resources without depleting them. In the broader context of the
                                    environment, it means satisfying basic human needs while maintaining environmental
                                    quality.T
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           						
                           <h4>Violence</h4>
                           						
                           <div>
                              							
                              <div>
                                 								
                                 <p>Psychological or physical force exerted for the purpose of threatening, injuring,
                                    damaging, or abusing people or property. In international relations, violent conflict
                                    typically refers to a clash of political interests between organized groups characterized
                                    by a sustained and large-scale use of force. Structural violence refers to inequalities
                                    built into the social system, for example, inequalities in income distribution.
                                 </p>
                                 							
                              </div>
                              						
                           </div>
                           					
                        </div>
                        					
                        <p>*References for these sources came from the United States Institute of Peace, Circles
                           of Belonging, The National SEED Project, and the works of John Paul Lederach and Parker
                           J. Palmer.
                        </p>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/peace-justice-institute/terminology.pcf">©</a>
      </div>
   </body>
</html>