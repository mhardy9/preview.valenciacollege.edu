<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Complaints  | Valencia College</title>
      <meta name="Description" content="The VA office at Valencia College strives to provide the best customer service possible. We do this by meeting all of the requirements put forth by the Department of Veterans Affairs. Then we tackle as many of the recommended services the VA suggests and create our own services that we know are important to the success of students using VA educational benefits. If you feel the VA office has fallen short of our requirements/goals or simply want to suggest something thay may better the experience for current and future students please feel free to email us so we may address your concerns.">
      <meta name="Keywords" content="veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/complaints.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li>Complaints </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Complaints</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Valencia College/VA Office</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The VA office at Valencia College strives to provide the best customer service possible.
                              We do this by meeting all of the requirements put forth by the Department of Veterans
                              Affairs. Then we tackle as many of the recommended services the VA suggests and create
                              our own services that we know are important to the success of students using VA educational
                              benefits. If you feel the VA office has fallen short of our requirements/goals or
                              simply want to suggest something thay may better the experience for current and future
                              students please feel free to <a href="mailto:dgibson11@valenciacollege.edu">email us</a> so we may address your concerns.
                           </p>
                           
                           
                           <p>Valencia also has a new <a href="https://valenciacollege.edu/students/disputes/">student electronic academic dispute and administrative complaint resolution web page</a>.
                           </p>
                           
                           
                           <h4>Valencia's VA Office Responsibilities</h4>
                           
                           <p>Keep VA informed of the enrollment status of Veterans and other eligible persons.
                              Use basic forms to keep VA informed, such as:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Enrollment Certification (VA Form 22-1999) to report required enrollment information</li>
                              
                              <li>Notice of Change in Student Status (VA Form 22-1999b) to report changes to enrollment
                                 information
                              </li>
                              
                              <li>Monitor the subjects pursued by a student to certify to VA only those subjects that
                                 apply to the student’s program
                              </li>
                              
                              <li>Monitor student’s grades to ensure s/he is making satisfactory progress; report when
                                 a
                              </li>
                              
                              <li>Student was terminated due to unsatisfactory progress</li>
                              
                              <li>Monitor student’s conduct and report when student is suspended or dismissed for unsatisfactory
                                 conduct
                              </li>
                              
                           </ul>
                           
                           
                           <p>Keep up-to-date on current VA rules and benefits</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Provide e-mail address to VA Education Liaison Representative (ELR)</li>
                              
                              <li>Read and maintain VA bulletins provided by your ELR</li>
                              
                              <li>Attend VA training opportunities</li>
                              
                           </ul>
                           
                           
                           <p>Maintain records of VA students and make all records available for inspection:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Retain file of VA papers submitted &amp; records of academic progress, program pursuit,
                                 etc.
                              </li>
                              
                              <li>Maintain records for at least three years following the student’s last date of attendance</li>
                              
                              <li>Ensure that records are kept in a safe place and that the privacy of VA students is
                                 protected
                              </li>
                              
                           </ul>
                           
                           
                           <p>A school’s file for a VA student should contain:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Copies of all VA paperwork</li>
                              
                              <li>The school’s transcript, grade reports, drop slips, registration slips (for those
                                 courses dropped during drop/add), tuition and fee charges, transcripts from previous
                                 schools with evaluations of same, student’s school application, records of disciplinary
                                 action, program outline, a curriculum guide or graduation evaluation form, and any
                                 other pertinent forms
                              </li>
                              
                           </ul>
                           
                           
                           <p>These are additional activities that schools are encouraged to carry out, but are
                              not required by VA.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Assist VA students in applying for education benefits</li>
                              
                              <li>Maintain copies of appropriate application forms</li>
                              
                              <li>When requested, help Veterans and dependents fill out and send in applications</li>
                              
                              <li>If the student cannot resolve payment problems, assist through VA channels designated
                                 for school officials
                              </li>
                              
                              <li>Disseminate and/or post information on VA education benefits, programs, and contact
                                 points
                              </li>
                              
                              <li>Ensure that VA students are fully aware of their responsibilities to the school</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Department of Veteran Affairs</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The <a href="https://gibill.custhelp.com/app/utils/login_form/redirect/ask">Right Now Web portal</a> can be used to voice complaints to the VA but it can also be used to ask questions
                              about your benefits where you might need a formal in writing response.
                           </p>
                           
                           
                           <p>This system can be used for concerns such as:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>I have not gotten paid yet for my educational benefits</li>
                              
                              <li>My monthly stipend doesnt seem to be the correct amount</li>
                              
                              <li>I dont seem to be recieving my Kicker payment</li>
                              
                              <li>What application should I fill out if I am a (veteran, dependent, disabled)</li>
                              
                           </ul>
                           
                           
                           <p>Basically anything that is VA specific that does not directly involve Valencia College
                              or the Principles of Excellence.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Principles of Excellence</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You may <a href="http://www.benefits.va.gov/gibill/feedback.asp">submit a complaint to the Department of Veterans Affairs</a> if Valencia College is failing to follow the <a href="http://www.benefits.va.gov/gibill/principles_of_excellence.asp">Principles of Excellence</a> agreement. VA will only review the following types of complaints through this complaint
                              system:
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>Recruiting/ Marketing Practices</li>
                              
                              <li>Accreditation</li>
                              
                              <li>Financial Issues (Tution/Fee charges)</li>
                              
                              <li>Student Loans</li>
                              
                              <li>Post Graduation Job Opportunities</li>
                              
                              <li>Change in Degree Plan/Requirements</li>
                              
                              <li>Quality of Education</li>
                              
                              <li>Grade Policy</li>
                              
                              <li>Release of Transcripts</li>
                              
                              <li>Transfer of Credits</li>
                              
                              <li>Refund Issues</li>
                              
                           </ul>
                           
                           
                           <p>We list what can be addressed because Valencia College does not want your concerns
                              to be over looked/not addressed. This portal is very specific in what types of complaints
                              it will address so if you submit concerns that do not address the Principles of Excellence
                              your complaint will be disregarded. Please look at all three complaint systems listed
                              and determine which one best suits your concerns.
                           </p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/complaints.pcf">©</a>
      </div>
   </body>
</html>