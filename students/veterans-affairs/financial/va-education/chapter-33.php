<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>The Post-9/11 GI-Bill, Chapter 33  | Valencia College</title>
      <meta name="Description" content="Information on the The Post-9/11 GI-Bill Chapter 33 benefits, eligibility, and application.">
      <meta name="Keywords" content="gi, bill, 9-11, 9/11, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/chapter-33.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li><a href="/students/veterans-affairs/financial/va-education/">Va Education</a></li>
               <li>The Post-9/11 GI-Bill, Chapter 33 </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>The Post-9/11 GI-Bill — Chapter 33, Title 38 U.S.C</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Eligibility</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Who is Eligible?</h4>
                           
                           <p>You may be eligible if:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>You served at least 90 aggregate days on active duty after September 10, 2001; or</li>
                              
                              <li>You were honorably discharged from active duty for a service-connected disability
                                 after serving 30 continuous days following September 10, 2001.
                              </li>
                              
                           </ul>
                           
                           
                           <p>Note: Children of a member of the Armed Forces who died in the line of duty on or
                              after September 11, 2001, may be eligible for Post-9/11 GI Bill benefits under the
                              Marine Gunnery John David Fry Scholarship Program.
                           </p>
                           
                           
                           <p>The following table lists the percentage of maximum benefits available based on the
                              number of aggregate days of active duty served after September 10, 2001.
                           </p>
                           
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Active Duty Period</th>
                                 
                                 <th scope="col">Percentage of Maximum Benefit</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>At least 36 months</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 30 continuous days and discharged due to service-connected disability</td>
                                 
                                 <td>100%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 30 months, less than 36 months</td>
                                 
                                 <td>90%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 24 months, less than 30 months</td>
                                 
                                 <td>80%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 18 months, less than 24 months</td>
                                 
                                 <td>70%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 12 months, less than 18 months</td>
                                 
                                 <td>60%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 6 months, less than 12 months</td>
                                 
                                 <td>50%</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>At least 90 days, less than 6 months</td>
                                 
                                 <td>40%</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <h4>Period of Eligibility</h4> 
                           
                           <p>You may receive up to 36 months of entitlement.</p>
                           
                           <p>You will be eligible for benefits for 15 years from your last period of active duty
                              of at least 90 consecutive days.
                           </p>
                           
                           
                           <h4>Can I Transfer My Entitlement To My Dependents?</h4>
                           
                           <p>You must be a member of the uniformed services to transfer your unused benefits to
                              your spouse or dependent(s). Generally, you must agree to serve four more years when
                              transferring benefits.
                           </p>
                           
                           
                           <p>See also: <a href="http://www.benefits.va.gov/gibill/post911_gibill.asp">VA.gov Information on Chapter 33</a>.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Entitlement</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You may receive a percentage of the following payments:</p>
                           
                           
                           <ol>
                              
                              <li>A Tuition and Fee payment that is paid to your school on your behalf</li>
                              
                              <li>
                                 A Monthly Housing Allowance that is equal to:
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>the basic allowance for housing (BAH) payable for the zip code of your school to a
                                       military E-5 with dependents for students pursuing resident training
                                    </li>
                                    
                                    <li>one-half the BAH national average for students training solely by distance learning</li>
                                    
                                    <li>The BAH is not payable to individuals on active duty or those enrolled at half time
                                       or less.
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>A Books and Supplies Stipend of $41.67 per credit hour up to 24 credit hours equals
                                 $1000 per academic year (08/01/YY-07/31/YY)
                              </li>
                              
                           </ol>
                           
                           
                           <p>Make sure that your start and end date of classes for the term reflects your intended
                              Monthly Rate!
                           </p>
                           
                           
                           <h4>Current Payment Rates</h4>
                           
                           <p>Effective August 1, 2016.</p>
                           
                           <p>The amounts listed are <strong>estimates</strong> based on enrollment. Actual payment will be based on the Department of Veterans Affairs
                              specific requirements and may be different from the amounts listed.
                           </p>
                           
                           
                           <p>The BAH is <strong>Not Payable</strong> to individuals on <em>active duty</em> or those <em>enrolled at half time or less</em>.
                           </p>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           
                           <h4>Fall &amp; Spring Terms</h4>
                           
                           <h5>Monthly Rates</h5>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Percentage of Benefits Paid</th>
                                 
                                 <th scope="col">Full Time: 12 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 11 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 9-10 Credits</th>
                                 
                                 <th scope="col">More than Half-Time: 8 Credits</th>
                                 
                                 <th scope="col">More than Half-Time: 7 Credits</th>
                                 
                                 <th scope="col">Half-Time or Less: 1-6 Credits</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>100%</td>
                                 
                                 <td>$1,521.00</td> 
                                 
                                 <td>$1,368.90</td> 
                                 
                                 <td>$1,216.80</td> 
                                 
                                 <td>$1,064.70</td> 
                                 
                                 <td>$912.60</td>
                                 
                                 <td rowspan="7">Not Payable</td>
                                 
                              </tr>
                              
                              <tr> 
                                 
                                 <td>90% </td>
                                 
                                 <td>$1,368.90 </td>
                                 
                                 <td>$1,232.01 </td>
                                 
                                 <td>$1,095.12 </td>
                                 
                                 <td>$958.23 </td>
                                 
                                 <td>$821.34</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>80%</td>
                                 
                                 <td>$1,216.80</td>
                                 
                                 <td>$1,095.12</td>
                                 
                                 <td>$973.44</td>
                                 
                                 <td>$851.76</td>
                                 
                                 <td>$730.08</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>70%</td>
                                 
                                 <td>$1,064.70</td>
                                 
                                 <td>$958.23</td>
                                 
                                 <td>$851.76</td>
                                 
                                 <td>$745.29</td>
                                 
                                 <td>$638.82</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>60%</td>
                                 
                                 <td>$912.60</td>
                                 
                                 <td>$821.34</td>
                                 
                                 <td>$730.08</td>
                                 
                                 <td>$638.82</td>
                                 
                                 <td>$547.56</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>50%</td>
                                 
                                 <td>$760.50</td>
                                 
                                 <td>$684.45</td>
                                 
                                 <td>$608.40</td>
                                 
                                 <td>$532.35</td>
                                 
                                 <td>$456.30</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>40%</td>
                                 
                                 <td>$608.40</td>
                                 
                                 <td>$547.56</td>
                                 
                                 <td>$486.72</td>
                                 
                                 <td>$425.88</td>
                                 
                                 <td>$365.04</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <h5>Monthly Rates for Exclusively Online Training (no classroom instruction)</h5>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Percentage of Benefits Paid</th>
                                 
                                 <th scope="col">Full Time: 12 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 11 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 9-10 Credits</th>
                                 
                                 <th scope="col">More than Half-Time: 8 Credits</th>
                                 
                                 <th scope="col">More than Half-Time: 7 Credits</th>
                                 
                                 <th scope="col">Half-Time or Less: 1-6 Credits</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>100%</td>
                                 
                                 <td>$806.00</td>
                                 
                                 <td>$725.40</td>
                                 
                                 <td>$644.80</td>
                                 
                                 <td>$564.20</td>
                                 
                                 <td>$483.60</td>
                                 
                                 <td rowspan="7">Not Payable</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>90%</td>
                                 
                                 <td>$725.40</td>
                                 
                                 <td>$652.86</td>
                                 
                                 <td>$579.32</td>
                                 
                                 <td>$507.78</td>
                                 
                                 <td>$435.24</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>80%</td>
                                 
                                 <td>$644.80</td>
                                 
                                 <td>$580.32</td>
                                 
                                 <td>$515.84</td>
                                 
                                 <td>$451.36</td>
                                 
                                 <td>$386.88</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>70%</td>
                                 
                                 <td>$564.20</td>
                                 
                                 <td>$507.78</td>
                                 
                                 <td>$451.36</td>
                                 
                                 <td>$394.94</td>
                                 
                                 <td>$338.52</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>60%</td>
                                 
                                 <td>$483.60</td>
                                 
                                 <td>$435.24</td>
                                 
                                 <td>$386.88</td>
                                 
                                 <td>$338.52</td>
                                 
                                 <td>$290.16</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>50%</td>
                                 
                                 <td>$403.00</td>
                                 
                                 <td>$362.70</td>
                                 
                                 <td>$322.40</td>
                                 
                                 <td>$282.10</td>
                                 
                                 <td>$241.80</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>40%</td>
                                 
                                 <td>$322.40</td>
                                 
                                 <td>$290.16</td>
                                 
                                 <td>$257.92</td>
                                 
                                 <td>$225.68</td>
                                 
                                 <td>$193.44</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <hr class="styled_2">
                           
                           
                           <h4>Summer Term</h4>
                           
                           <h5>Monthly Rates</h5>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Percentage of Benefits Paid</th>
                                 
                                 <th scope="col">Full Time: 8 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 7 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 6 Credits</th>
                                 
                                 
                                 <th scope="col">More than Half-Time: 5 Credits</th>
                                 
                                 <th scope="col">Half-Time or Less: 1-4 Credits</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>100%</td>
                                 
                                 <td>$1,521.00</td> 
                                 
                                 <td>$1,368.90</td> 
                                 
                                 <td>$1,216.80</td> 
                                 
                                 
                                 <td>$912.60</td>
                                 
                                 <td rowspan="7">Not Payable</td>
                                 
                              </tr>
                              
                              <tr> 
                                 
                                 <td>90% </td>
                                 
                                 <td>$1,368.90 </td>
                                 
                                 <td>$1,232.01 </td>
                                 
                                 <td>$1,095.12 </td>
                                 
                                 
                                 <td>$821.34</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>80%</td>
                                 
                                 <td>$1,216.80</td>
                                 
                                 <td>$1,095.12</td>
                                 
                                 <td>$973.44</td>
                                 
                                 
                                 <td>$730.08</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>70%</td>
                                 
                                 <td>$1,064.70</td>
                                 
                                 <td>$958.23</td>
                                 
                                 <td>$851.76</td>
                                 
                                 
                                 <td>$638.82</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>60%</td>
                                 
                                 <td>$912.60</td>
                                 
                                 <td>$821.34</td>
                                 
                                 <td>$730.08</td>
                                 
                                 
                                 <td>$547.56</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>50%</td>
                                 
                                 <td>$760.50</td>
                                 
                                 <td>$684.45</td>
                                 
                                 <td>$608.40</td>
                                 
                                 
                                 <td>$456.30</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>40%</td>
                                 
                                 <td>$608.40</td>
                                 
                                 <td>$547.56</td>
                                 
                                 <td>$486.72</td>
                                 
                                 
                                 <td>$365.04</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <h5>Monthly Rates for Exclusively Online Training (no classroom instruction)</h5>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Percentage of Benefits Paid</th>
                                 
                                 <th scope="col">Full Time: 8 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 7 Credits</th>
                                 
                                 <th scope="col">3/4ths Time: 6 Credits</th>
                                 
                                 <th scope="col">More than Half-Time: 5 Credits</th>
                                 
                                 <th scope="col">Half-Time or Less: 1-4 Credits</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>100%</td>
                                 
                                 <td>$806.00</td>
                                 
                                 <td>$725.40</td>
                                 
                                 <td>$644.80</td>
                                 
                                 
                                 <td>$483.60</td>
                                 
                                 <td rowspan="7">Not Payable</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>90%</td>
                                 
                                 <td>$725.40</td>
                                 
                                 <td>$652.86</td>
                                 
                                 <td>$579.32</td>
                                 
                                 
                                 <td>$435.24</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>80%</td>
                                 
                                 <td>$644.80</td>
                                 
                                 <td>$580.32</td>
                                 
                                 <td>$515.84</td>
                                 
                                 
                                 <td>$386.88</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>70%</td>
                                 
                                 <td>$564.20</td>
                                 
                                 <td>$507.78</td>
                                 
                                 <td>$451.36</td>
                                 
                                 
                                 <td>$338.52</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>60%</td>
                                 
                                 <td>$483.60</td>
                                 
                                 <td>$435.24</td>
                                 
                                 <td>$386.88</td>
                                 
                                 
                                 <td>$290.16</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>50%</td>
                                 
                                 <td>$403.00</td>
                                 
                                 <td>$362.70</td>
                                 
                                 <td>$322.40</td>
                                 
                                 
                                 <td>$241.80</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>40%</td>
                                 
                                 <td>$322.40</td>
                                 
                                 <td>$290.16</td>
                                 
                                 <td>$257.92</td>
                                 
                                 
                                 <td>$193.44</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Required Documents</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <table class="table table">
                                    
                                    
                                    <tr>
                                       
                                       <th scope="col">Student Category</th>
                                       
                                       <th scope="col">Submit To Valencia</th>
                                       
                                       <th scope="col">Request From Valencia</th>
                                       
                                    </tr>
                                    
                                    
                                    <tr>
                                       
                                       <th scope="row">First Time Using Benefits</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E. (or)</li>
                                             
                                             <li><a href="https://www.ebenefits.va.gov/ebenefits-portal/ebenefits.portal?_nfpb=true&amp;_portlet.async=false&amp;_pageLabel=ebenefits_myeb_vonapp1" target="_blank">22-1990 (Veterans)</a></li>
                                             
                                             <li><a href="https://vabenefits.vba.va.gov/vonapp_ssl/login.asp" target="_blank">22-1990E (Dependents)</a></li>
                                             
                                             <li>DD-214</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transfer Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E. (or)</li>
                                             
                                             <li><a href="https://www.ebenefits.va.gov/ebenefits-portal/ebenefits.portal?_nfpb=true&amp;_portlet.async=false&amp;_pageLabel=ebenefits_myeb_vonapp1" target="_blank">22-1990 (Veterans)</a></li>
                                             
                                             <li><a href="https://vabenefits.vba.va.gov/vonapp_ssl/login.asp" target="_blank">22-1990E (Dependents)</a></li>
                                             
                                             <li>DD-214</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transient Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Transient Form</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </table>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <h4>Document Details</h4>
                                 
                                 <ul class="list_style_1">
                                    
                                    <p><strong>C.O.E</strong> - (Certificate of Eligibility)
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Obtain this from the Department of Veterans Affairs at 1-888-442-4551</li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-1995</strong> - Change of Place of Training Form (Veterans).
                                    </p>
                                    
                                    <p><strong>22-5495</strong> - Change of Place of Training Form (Dependants).
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit 22-1995 or 22-5495 through VONAPP. Print out and provide a copy with confirmation
                                          number to a Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>DD-214</strong> - Certificate of Release or Discharge from Active Duty.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The DD-214 is issued upon a military service member's retirement, separation, or discharge
                                          from active-duty military.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Certification</strong> - Valencia College Veteran Affairs Certification of Enrollment Request
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The Certification needs to be submitted every semester and can be obtained at your
                                          local Valencia Veterans Services Campus or <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">Online</a>.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Transient Form</strong> - Authorization to take course(s) at a secondary institution
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>For Florida Institutions, submit a <a href="https://www.floridashines.org/">Transient Student Admissions Application</a>
                                          
                                       </li>
                                       
                                       <li>For all other states, request a letter from your primary school addressed to Valencia
                                          College Certifying Official. Please refer to example.
                                       </li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           <div class="row" id="transient_letter">
                              
                              <h4>Example of Transient Letter</h4>
                              
                              <p class="pull-right"><em>[Name and Address of Primary School]</em></p>
                              <br>
                              
                              <p><em>[Date]</em></p>
                              
                              <p><em>[Name and Address of Secondary School]</em></p>
                              
                              <p><em>[Student's Name (Claim Number)]</em> is a chapter <em>[e.g., 30]</em> student at <em>[Name of School (facility code)]</em> pursuing a <em>[Name of Program, e.g. B.S. History degree]</em>.
                              </p>
                              
                              <p>The course(s) listed below satisfy <em>[Name of Program]</em> requirements and will transfer at full value to <em>[Name of School]</em>.
                              </p>
                              
                              <p><em>[List course(s) by title and number.]</em></p>
                              
                              <p>Student intends to take the above course(s) at <em>[Secondary School]</em> during <em>[Identify term, e.g. Spring term 2018]</em> as a guest student. Please certify the courses to VA as the secondary school.
                              </p>
                              
                              <p><em>[Signature of the Certifying Official]</em><br>
                                 <em>[Telephone Number]</em></p>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/chapter-33.pcf">©</a>
      </div>
   </body>
</html>