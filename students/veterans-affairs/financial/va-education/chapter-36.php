<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Chapter 36 Services  | Valencia College</title>
      <meta name="Description" content="Information on Chapter 36 counseling services.">
      <meta name="Keywords" content="counseling, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/chapter-36.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li><a href="/students/veterans-affairs/financial/va-education/">Va Education</a></li>
               <li>Chapter 36 Services </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Chapter 36 Services</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Purpose</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The purpose of Chapter 36 services is to provide professional, educational, vocational
                              and career counseling services to Service members, Veterans and dependents so they
                              may do the following:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Address problems that may interfere with achieving an academic/ vocational goal</li>
                              
                              <li>Explore their patterns of abilities, skills and interests</li>
                              
                              <li>Identify a career objective</li>
                              
                              <li>Plan the best use of VA benefit</li>
                              
                              <li>Develop a suitable program of education or training</li>
                              
                              <li>Select an educational or training facility</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Eligibility for Chapter 36 Services</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>38 U.S.C. 3697A governs eligibility for Chapter 36 services. Every Veteran, Servicemember
                              and qualifying dependent may receive Chapter 36 services as many times as requested
                              as long as he/she is still eligible.
                           </p>
                           
                           
                           <h4>Veterans/Servicemembers</h4>
                           
                           <p>Veterans who are within one year from the date of discharge or release from active
                              duty are eligible to receive Chapter 36 services. The discharge must be under conditions
                              other than dishonorable.
                           </p>
                           
                           <p>Also, Veterans participating in the following VA education assistance programs are
                              eligible to receive Chapter 36 services, regardless of discharge date. The one year
                              from discharge requirement does not apply to the situations below:
                           </p>
                           
                           
                           <ol>
                              
                              <li>
                                 <strong>Montgomery GI Bill (Chapter 30)</strong>
                                 
                                 <p>To qualify for the Montgomery GI Bill (MGIB), active-duty Servicemembers enroll and
                                    pay $100 per month for 12 months to be entitled to receive a monthly education benefit
                                    once they have completed a minimum service obligation. The MGIB provides up to 36
                                    months of education benefits. This benefit may be used for degree and certificate
                                    programs, flight training, apprenticeship/on-the-job training and correspondence courses.
                                    Remedial, deficiency and refresher courses may be approved under certain circumstances.
                                    Generally, benefits are payable for 10 years following release from active duty. Visit
                                    www.gibill.va.gov for additional information on entitlement criteria for VA educational
                                    assistance.
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <strong>Veterans Educational Assistance Program (Chapter 32)</strong>
                                 
                                 <p>Veterans Educational Assistance Program (VEAP) is available to those who entered service
                                    for the first time between January 1, 1977, and June 30, 1985. VEAP benefits can be
                                    used for degree, certificate, correspondence, apprenticeship/on-the-job training programs
                                    and vocational flight training programs. In certain circumstances, remedial, deficiency
                                    and refresher training may also be available. Benefit entitlement is up to 36 months.
                                    Generally, benefits are payable for 10 years following release from active duty. Visit
                                    www.gibill.va.gov for additional information on entitlement criteria for VA educational
                                    assistance.
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <strong>Post-9/11 GI Bill (Chapter 33)</strong>
                                 
                                 <p>The Post-9/11 GI Bill provides financial support for education and housing to individuals
                                    with at least 90 days of service on or after September 10, 2001, or individuals discharged
                                    with a service-connected disability. Approved training under the Post-9/11 GI Bill
                                    includes graduate and undergraduate degrees, vocational/technical training, on-the-job
                                    training, flight training, correspondence training, licensing and national testing
                                    programs, and tutorial assistance. The program pays for tuition and fees, in addition
                                    to a monthly housing allowance and a stipend for books and supplies. Benefit entitlement
                                    is up to 36 months. Generally, benefits are payable for 15 years following release
                                    from active duty. In the event that a Veteran transfers Chapter 33 benefits to a dependent,
                                    the dependent could then apply for and receive Chapter 36 services. Visit www.gibill.va.gov
                                    for additional information on entitlement criteria for VA educational assistance.
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <strong>MGIB Selected Reserve (Chapter 1606)</strong>
                                 
                                 <p>A Reservist must be actively drilling and have a six-year obligation in the Selected
                                    Reserve to be eligible for services under Chapter 1606. Approved training under this
                                    benefit includes graduate and undergraduate degrees, vocational/technical training,
                                    on-the-job or apprenticeship training, correspondence training and flight training.
                                    Benefit entitlement is up to 36 months. Entitlement to this benefit is generally contingent
                                    upon continued service in the Selected Reserve. Visit www.gibill.va.gov for additional
                                    information on entitlement criteria for VA educational assistance.
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <strong>Reserve Educational Assistance Program (Chapter 1607)</strong>
                                 
                                 <p>The Reserve Educational Assistance Program (REAP) is an educational program that provides
                                    assistance to members of the Reserve components who are called or ordered to active
                                    service. Approved training under REAP includes graduate and undergraduate degrees,
                                    vocational/technical training, on-the-job or apprenticeship training, correspondence
                                    training and flight training. Benefit entitlement is up to 36 months. Entitlement
                                    to REAP is generally contingent upon continued service in the Reserve Components.
                                    Visit www.gibill.va.gov for additional information on entitlement criteria for VA
                                    educational assistance.
                                 </p>
                                 
                              </li>
                              
                           </ol>
                           
                           
                           <h4>Others</h4>
                           
                           <p>Current beneficiaries of the following VA education assistance programs are eligible
                              to receive Educational, Vocational and Career Counseling services:
                           </p>
                           
                           <ol>
                              
                              <li>
                                 <strong>Benefits for Certain Children of Vietnam Veterans (Chapter 18)</strong>
                                 
                                 <p>VA provides monetary allowances, vocational training and rehabilitation, education
                                    services and health care benefits to certain Korea and Vietnam Veterans' birth children
                                    who are born with spina bifida. See M28R.VII.A.3 for additional information on the
                                    implementation of Chapter 18 services.
                                 </p>
                                 
                              </li>
                              
                              <li>
                                 <strong>Survivors' and Dependents' Educational Assistance Program (Chapter 35)</strong>
                                 
                                 <p>Survivors' and Dependents' Educational Assistance provides education and training
                                    opportunities to eligible dependents of certain Veterans. These benefits may be used
                                    for degree and certificate programs, apprenticeship and on-the-job training. Remedial,
                                    deficiency and refresher courses may be approved under certain circumstances. The
                                    program offers up to 45 months of education benefits. The period of eligibility varies
                                    as it is based on a number of factors.
                                 </p>
                                 
                              </li>
                              
                           </ol>
                           
                           <strong>The request for Chapter 36 services should be annotated in the remarks section of
                              the form 22-1990.</strong>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/chapter-36.pcf">©</a>
      </div>
   </body>
</html>