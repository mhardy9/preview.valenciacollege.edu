<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Montgomery GI Bill, Chapter 30  | Valencia College</title>
      <meta name="Description" content="Information on the Montgomery GI Bill Chapter 30 benefits, eligibility, and application.">
      <meta name="Keywords" content="montgomery, gi, bill, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/chapter-30.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li><a href="/students/veterans-affairs/financial/va-education/">Va Education</a></li>
               <li>Montgomery GI Bill, Chapter 30 </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Montgomery GI Bill Active Duty — Chapter 30, Title 38 U.S.C</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Eligibility</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Who is Eligible?</h4>
                           
                           <p>Chapter 30 benefits generally apply to honorably discharged veterans who began active
                              duty service for the first time after June 30, 1985, who had their pay reduced by
                              $100 a month for 12 months. For information on the four eligibility categories, visit
                              <a href="http://www.benefits.va.gov/gibill/montgomery_bill.asp">the VA.gov information page for Chapter 30</a>.
                           </p>
                           
                           
                           <h4>Entitlement</h4>
                           
                           <p>The MGIB program provides up to 36 months of education benefits. It may extend to
                              the end of term if it expires during the term. If there’s a kicker, the kicker doesn’t
                              extend.
                           </p>
                           
                           <p>The monthly benefit paid to you is based on your enrollment, length of your service,
                              and your category.
                           </p>
                           
                           
                           <h4>Contributions</h4>
                           
                           <p>Veterans may have a kicker, additional contributions, or both. The higher benefit
                              rates are paid automatically when benefits are paid. If veterans don’t receive the
                              benefit they believe they are entitled to receive, they should call The Department
                              of Veterans Affairs about the discrepancy so that VA can resolve the discrepancy with
                              the Department of Defense.
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 Kicker is part of the enlistment contract. It might be referred to as; Army College
                                 Fund, Navy Sea College Fund.
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>For example, $12,000 kicker is $333.33 ($12,000 / 36 months).</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Buy Up a service member may contribute up to $600.00 more into the program, which
                                 will increase the basic full-time Chapter 30 benefit by $150.00 per month. Not refundable
                                 for those who elect Chapter 33.
                              </li>
                              
                              <li>
                                 The $1,200.00 Contribution toward Chapter 30 is non-refundable with one exception.
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Those who elect and exhaust their Chapter 33 entitlement and are receiving a monthly
                                       housing allowance can receive a refund of the $1,200.00 contribution, proportional
                                       to the amount of unused chapter 30 entitlements at the time of election for chapter
                                       33.
                                    </li>
                                    
                                    <li>To get the refund, you must request it in writing to the Department of Veterans Affairs.</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Period of Eligibility</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Generally, benefits are payable for 10 years following your release from active duty.
                              It may be extended for a later period of active duty or disability that prevents completion
                              of program.
                           </p> 
                           
                           
                           <h4>Fall &amp; Spring Terms</h4>
                           
                           <table class="table table">
                              
                              <tbody>
                                 
                                 <tr>
                                    
                                    <th scope="col">Enrollment</th>
                                    
                                    <th scope="col">Credit Hours</th>
                                    
                                    <th scope="col">Monthly Rate (3 year or more obligation*)</th>
                                    
                                    <th scope="col">Monthly Rate (Less than 3 year obligation**)</th>
                                    
                                 </tr>
                                 
                                 
                                 <tr>
                                    
                                    <th scope="row">Full time</th>
                                    
                                    <td>12</td>
                                    
                                    <td>$1,789.00</td>
                                    
                                    <td>$1,454.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th scope="row">3/4 time</th>
                                    
                                    <td>9-11</td>
                                    
                                    <td>$1,341.75</td>
                                    
                                    <td>$1,090.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th scope="row">1/2 time</th>
                                    
                                    <td>6-8</td>
                                    
                                    <td>$894.50</td>
                                    
                                    <td>$727.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th scope="row">Less than 1/2 time more than 1/4 time</th>
                                    
                                    <td>4-5</td>
                                    
                                    <td>Tuition &amp; fees not to exceed $894.50</td>
                                    
                                    <td>Tuition &amp; fees not to exceed $727.00</td>
                                    
                                 </tr>
                                 
                                 <tr>
                                    
                                    <th scope="row">1/4 time or less</th>
                                    
                                    <td>1-3</td>
                                    
                                    <td>Tuition &amp; fees not to exceed $447.25</td>
                                    
                                    <td>Tuition &amp; fees not to exceed $363.50</td>
                                    
                                 </tr>
                                 
                              </tbody>
                              
                           </table>
                           
                           
                           <h4>Summer Term</h4>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Enrollment</th>
                                 
                                 <th scope="col">Credit Hours For 12 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For (TWJ or TWK) 8 Weeks of Classes  </th>
                                 
                                 <th scope="col">Credit Hours For 6 Weeks of Classes  </th>
                                 
                                 <th scope="col">Credit Hours For 4 Weeks of Classes</th>
                                 
                                 <th scope="col">Monthly Rate (3 year or more obligation*)</th>
                                 
                                 <th scope="col">Monthly Rate (Less than 3 year obligation**)</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <th scope="row">Full time</th>
                                 
                                 <td>8</td>
                                 
                                 <td>6</td>
                                 
                                 <td>4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>$1,789.00</td>
                                 
                                 <td>$1,454.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">3/4 time</th>
                                 
                                 <td>7-6</td>
                                 
                                 <td>5-4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>$1,341.75</td>
                                 
                                 <td>$1,090.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">1/2 time</th>
                                 
                                 <td>5-4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>$894.50</td>
                                 
                                 <td>$727.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Less than 1/2 time more than 1/4 time</th>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>1</td>
                                 
                                 <td>Tuition &amp; Fees not to exceed $894.50</td>
                                 
                                 <td>Tuition &amp; Fees not to exceed $727.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">1/4 time or less</th>
                                 
                                 <td>1-2</td>
                                 
                                 <td>1</td>
                                 
                                 <td>1</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>Tuition &amp; Fees not to exceed $447.25</td>
                                 
                                 <td>Tuition &amp; Fees not to exceed $363.50</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>* Entitlement charged at the rate of one month for each $1,789.00 paid.</li>
                              
                              <li>** Entitlement charged at the rate of one month for each $1,454.00 paid.</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Required Documents</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <table class="table table">
                                    
                                    
                                    <tr>
                                       
                                       <th scope="col">Student Category</th>
                                       
                                       <th scope="col">Submit To Valencia</th>
                                       
                                       <th scope="col">Request From Valencia</th>
                                       
                                    </tr>
                                    
                                    
                                    <tr>
                                       
                                       <th scope="row">First Time Using Benefits</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E; (or) 22-1990</li>
                                             
                                             <li>DD-214</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transfer Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E (if for Valencia College) or 22-1995</li>
                                             
                                             <li>DD-214</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transient Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Transient Form</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </table>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <h4>Document Details</h4>
                                 
                                 <ul class="list_style_1">
                                    
                                    <p><strong>C.O.E</strong> - (Certificate of Eligibility)
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Obtain this from the Department of Veterans Affairs at 1-888-442-4551</li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-1990</strong> -The Department of Veterans Affairs Application for Educational Benefits.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit 22-1990 through VONAPP. Print out and provide a copy with confirmation number
                                          to a Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>DD-214</strong> - Certificate of Release or Discharge from Active Duty.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The DD-214 is issued upon a military service member's retirement, separation, or discharge
                                          from active-duty military.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-1995</strong> - Change of Place of Training Form.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit 22-1995 through VONAPP. Print out and provide a copy with confirmation number
                                          to a Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Certification</strong> - Valencia College Veteran Affairs Certification of Enrollment Request
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The Certification needs to be submitted every semester and can be obtained at your
                                          local Valencia Veterans Services Campus or <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">Online</a>.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Transient Form</strong> - Authorization to take course(s) at a secondary institution
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>For Florida Institutions, submit a <a href="https://www.floridashines.org/">Transient Student Admissions Application</a>
                                          
                                       </li>
                                       
                                       <li>For all other states, request a letter from your primary school addressed to Valencia
                                          College Certifying Official. Please refer to example.
                                       </li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           <div class="row" id="transient_letter">
                              
                              <h4>Example of Transient Letter</h4>
                              
                              <p class="pull-right"><em>[Name and Address of Primary School]</em></p>
                              <br>
                              
                              <p><em>[Date]</em></p>
                              
                              <p><em>[Name and Address of Secondary School]</em></p>
                              
                              <p><em>[Student's Name (Claim Number)]</em> is a chapter <em>[e.g., 30]</em> student at <em>[Name of School (facility code)]</em> pursuing a <em>[Name of Program, e.g. B.S. History degree]</em>.
                              </p>
                              
                              <p>The course(s) listed below satisfy <em>[Name of Program]</em> requirements and will transfer at full value to <em>[Name of School]</em>.
                              </p>
                              
                              <p><em>[List course(s) by title and number.]</em></p>
                              
                              <p>Student intends to take the above course(s) at <em>[Secondary School]</em> during <em>[Identify term, e.g. Spring term 2018]</em> as a guest student. Please certify the courses to VA as the secondary school.
                              </p>
                              
                              <p><em>[Signature of the Certifying Official]</em><br>
                                 <em>[Telephone Number]</em></p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/chapter-30.pcf">©</a>
      </div>
   </body>
</html>