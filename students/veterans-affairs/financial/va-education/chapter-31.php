<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Vocational Rehabilitation and Employment Program, Chapter 31  | Valencia College</title>
      <meta name="Description" content="Information on the Vocational Rehabilitation and Employment Program Chapter 31 benefits, eligibility, and application.">
      <meta name="Keywords" content="vocational, rehabilitation, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/chapter-31.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li><a href="/students/veterans-affairs/financial/va-education/">Va Education</a></li>
               <li>Vocational Rehabilitation and Employment Program, Chapter 31 </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Vocational Rehabilitation and Employment Program — Chapter 31, Title 38 U.S.C</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Eligibility</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Who is Eligible?</h4>
                           
                           <p>Active Duty Service Members are eligible if they:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Expect to receive an honorable discharge upon separation from active duty</li>
                              
                              <li>Obtain a memorandum rating of 20% or more from the Department of Veterans Affairs
                                 (VA)
                              </li>
                              
                              <li>Apply for Vocational Rehabilitation and Employment (VR&amp;E) services</li>
                              
                           </ul>
                           
                           
                           <p>Veterans are eligible if they:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Have received, or will receive, a discharge that is other than dishonorable</li>
                              
                              <li>Have a service-connected disability rating of at least 10%, or a memorandum rating
                                 of 20% or more from the Department of Veterans Affairs (VA)
                              </li>
                              
                              <li>Apply for Vocational Rehabilitation and Employment (VR&amp;E) services</li>
                              
                           </ul>
                           
                           
                           <h4>How is Eligibility Determined?</h4>
                           
                           <p>A VA Counselor decides if a veteran has an employment handicap based upon the results
                              of the comprehensive evaluation. Entitlement to services is established if the veteran
                              has a 20% service-connected disability and an employment handicap. If the disability
                              is 10% service-connected, then a serious employment handicap must be found to establish
                              entitlement to vocational rehabilitation services (see definitions for more details).
                           </p>
                           
                           <p>After an entitlement decision is made, the veteran and the counselor will work together
                              to develop a rehabilitation plan.
                           </p>
                           
                           
                           <h4>Period of Eligibility</h4>
                           
                           <p>The basic period of eligibility in which VR&amp;E services may be used is 12 years from
                              the latter of the following:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Date of separation from active military service, or</li>
                              
                              <li>Date the veteran was first notified by Department of VA of a service-connected disability
                                 rating.
                              </li>
                              
                           </ul>
                           
                           <p>The basic period of eligibility may be extended if a Vocational Rehabilitation Counselor
                              (VRC) determines that a Veteran has a serious employment handicap.
                           </p>
                           
                           
                           <h4>Requirements to Maintain Eligibility</h4>
                           
                           <p>A Comprehensive Evaluation is completed with a Vocational Rehabilitation Counselor
                              that includes:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>A full assessment of the veteran's interests, aptitudes, and abilities to determine
                                 whether the veteran is "entitled" to VR&amp;E services
                              </li>
                              
                              <li>An assessment of whether service-connected disabilities impair the veteran's ability
                                 to find and/or hold a job using the occupational skills already attained
                              </li>
                              
                              <li>Vocational exploration and goal development</li>
                              
                           </ul>
                           
                           
                           <p>See also: <a href="http://www.benefits.va.gov/vocrehab/">VA.gov Information on Chapter 31</a>.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Entitlement</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Comprehensive rehabilitation evaluation to determine abilities, skills, and interests
                                 for employment
                              </li>
                              
                              <li>Vocational counseling and rehabilitation planning for employment services</li>
                              
                              <li>Employment services such as job-training, job-seeking skills, resume development,
                                 and other work readiness assistance
                              </li>
                              
                              <li>Assistance finding and keeping a job, including the use of special employer incentives
                                 and job accommodations
                              </li>
                              
                              <li>On the Job Training (OJT), apprenticeships, and non-paid work experiences</li>
                              
                              <li>Post-secondary training at a college, vocational, technical or business school</li>
                              
                              <li>Supportive rehabilitation services including case management, counseling, and medical
                                 referrals
                              </li>
                              
                              <li>Independent living services for Veterans unable to work due to the severity of their
                                 disabilities
                              </li>
                              
                           </ul>
                           
                           
                           <p>Make sure that your start and end date of classes for the term reflects your intended
                              Monthly Rate!
                           </p>
                           
                           
                           <h4>Current Payment Rates</h4>
                           
                           <p>Effective October 1, 2015.</p>
                           
                           <p>An individual eligible for Chapter 33 who is receiving benefits under Chapter 31 may
                              elect to receive the applicable Chapter 33 Monthly Housing Allowance in lieu of the
                              monthly subsistence allowance. Students should contact their Vocational Rehabilitation
                              counselors for additional information.
                           </p>
                           
                           
                           <h4>Fall &amp; Spring Terms</h4>
                           
                           <table class="table table">
                              
                              
                              <tr>
                                 
                                 <th scope="col">Number of Dependents</th>
                                 
                                 <th scope="col">Full Time</th>
                                 
                                 <th scope="col">One-Half Time</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <th scope="row">No Dependents</th>
                                 
                                 <td>$605.44</td>
                                 
                                 <td>$304.39</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">One Dependent</th>
                                 
                                 <td>$751.00</td>
                                 
                                 <td>$377.14</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Two Dependents</th>
                                 
                                 <td>$855.00</td>
                                 
                                 <td>$443.31</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="row">Each Additional Dependent</th>
                                 
                                 <td>$64.50</td>
                                 
                                 <td>$33.10</td>
                                 
                              </tr>
                              
                              
                           </table>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Required Documents</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <table class="table table">
                                    
                                    
                                    <tr>
                                       
                                       <th scope="col">Student Category</th>
                                       
                                       <th scope="col">Submit To Valencia</th>
                                       
                                       <th scope="col">Request From Valencia</th>
                                       
                                    </tr>
                                    
                                    
                                    <tr>
                                       
                                       <th scope="row">First Time Using Benefits</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>VA Form 1905</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transfer Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>VA Form 1905</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transient Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>VA Form 1905</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </table>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <h4>Document Details</h4>
                                 
                                 <ul class="list_style_1">
                                    
                                    <p><strong>C.O.E</strong> - (Certificate of Eligibility)
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Obtain this from the Department of Veterans Affairs at 1-888-442-4551</li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-1905</strong> - Authorization and Certification of Entrance or Reentrance into Rehabilitation and
                                       Certification of Status.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Issued by the Vocational Rehabilitation Counselor assigned</li>
                                       
                                    </ul>
                                    
                                    <p><strong>DD-214</strong> - Certificate of Release or Discharge from Active Duty.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The DD-214 is issued upon a military service member's retirement, separation, or discharge
                                          from active-duty military.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Certification</strong> - Valencia College Veteran Affairs Certification of Enrollment Request
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The Certification needs to be submitted every semester and can be obtained at your
                                          local Valencia Veterans Services Campus or <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">Online</a>.
                                       </li>
                                       
                                    </ul>
                                    
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/chapter-31.pcf">©</a>
      </div>
   </body>
</html>