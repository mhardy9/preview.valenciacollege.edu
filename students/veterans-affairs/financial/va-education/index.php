<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Veterans Affairs Education Benefits  | Valencia College</title>
      <meta name="Description" content="Information on the chapters of benefits available to veterans, eligibility, application processes.">
      <meta name="Keywords" content="benefits, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li>Va Education</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Veterans Affairs Educational Benefits</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>Select a program for more information.</p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 Chapter 30: <a href="chapter-30.html">Montgomery GI Bill Active Duty (MGIB-AD) </a><br>
                                 
                              </li>
                              
                              
                              <li>
                                 Chapter 31: <a href="chapter-31.html">Vocational Rehabilitation and Employment</a><br>
                                 
                              </li>
                              
                              
                              <li>
                                 Chapter 33: <a href="chapter-33.html">The Post 9/11 GI Bill </a><br>
                                 
                              </li>
                              
                              
                              <li>
                                 Chapter 35: <a href="chapter-35.html">Survivors &amp; Dependents Assistance (DEA) </a><br>
                                 
                              </li>
                              
                              
                              <li>
                                 Chapter 1606: <a href="chapter-1606.html">Montgomery GI Bill Selected Reserve (MGIB-SR) </a><br>
                                 
                              </li>
                              
                              
                              <li>
                                 Chapter 1607: <a href="chapter-1607.html">Reserve Educational Assistance (REAP) </a><br>
                                 
                              </li>
                              
                           </ul>
                           
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/index.pcf">©</a>
      </div>
   </body>
</html>