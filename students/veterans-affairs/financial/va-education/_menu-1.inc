<ul>
	<li><a href="/students/offices-services/veteran-affairs/index.php">Veteran Affairs</a></li>
	<li class="submenu"> <a href="/students/offices-services/veteran-affairs/enrollment/" class="show-submenu"> Enrollment/Info <i class="fas fa-chevron-down" aria-hidden="true"> </i> </a> 
		<ul>
			<li><a href="/students/offices-services/veteran-affairs/enrollment/understanding.php">Statement of Understanding</a></li>
			<li><a href="/students/offices-services/veteran-affairs/enrollment/certification.php">Certification</a></li>
			<li><a href="/students/offices-services/veteran-affairs/enrollment/student-veterans.php">Student Veterans of America</a></li>
			<li><a href="/students/offices-services/veteran-affairs/enrollment/veteran-only-courses.php">Veterans-Only Courses</a></li>
			<li><a href="/students/offices-services/veteran-affairs/enrollment/links.php">Links</a></li>
		</ul>
	</li>
	<li class="submenu"> <a href="javascript:void(0);" class="show-submenu"> Financial <i class="fas fa-chevron-down" aria-hidden="true"> </i> </a> 
		<ul>
			<li><a href="/students/offices-services/veteran-affairs/financial/tuition-waiver.php">C.W. Bill Young Tuition Waiver</a></li>
			<li><a href="/students/offices-services/veteran-affairs/financial/dod-assistance.php">DOD Tuition Assistance</a></li>
			<li><a href="/students/offices-services/veteran-affairs/financial/combat-decorations.php">Combat Decorations (Tuition)</a></li>
			<li><a href="/students/offices-services/veteran-affairs/financial/va-education/">VA Education Benefits</a></li>
			<li><a href="/students/offices-services/veteran-affairs/financial/debt.php">VA Debt</a></li>
		</ul>
	</li>
	<li><a href="/students/offices-services/veteran-affairs/faqs.php">FAQs</a></li>
	<li><a href="/students/offices-services/veteran-affairs/complaints.php">Complaints</a></li>
</ul>