<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Dependents Educational Assistance Chapter 35  | Valencia College</title>
      <meta name="Description" content="Information on the Dependents Educational Assistance Chapter 35 benefits, eligibility, and application.">
      <meta name="Keywords" content="dependents, educational, assistance, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/chapter-35.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li><a href="/students/veterans-affairs/financial/va-education/">Va Education</a></li>
               <li>Dependents Educational Assistance Chapter 35 </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Dependents Educational Assistance — Chapter 35, Title 38 U.S.C.</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Eligibility</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Who is Eligible?</h4>
                           
                           <p>Dependents (spouse or child) of a veteran who died or is permanently and totally disabled
                              as the result of a service-connected disability; or who is listed as a Prisoner of
                              War or Missing in Action, may be eligible for these benefits. For further information,
                              visit the <a href="http://gibill.va.gov/benefits/other_programs/dea.html">VA.gov website</a>.
                           </p>
                           
                           
                           <h4>Entitlement</h4>
                           
                           <p>The program offers up to 45 months of eudcational benefits.</p>
                           
                           <p>No extensions except for children in special restorative training.</p>
                           
                           
                           <h4>Period of Eligibility</h4>
                           
                           <p>Eligibility rules are complex. Claimant should carefully read eligibility letter.</p>
                           
                           <p>Spouse:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>10 years, if veteran rated permanently and totally disabled.</li>
                              
                              <li>20 years, if veteran rated permanently and totally disabled within 3 years of discharge
                                 or if death of veteran resulted while on active duty.
                              </li>
                              
                           </ul>
                           
                           
                           <p>Child:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>8 years, if veteran rated permanently and totally disabled.</li>
                              
                              <li>
                                 Child must be between the ages of 18 and 26.<br>
                                 In certain instances, it is possible to begin before age 18 and can also continue
                                 after age 26.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Payment Rates</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Make sure that your start and end date of classes for the term reflects your intended
                              Monthly Rate!
                           </p>
                           
                           <p>Rates effective October 1, 2015:</p>
                           
                           
                           <h4>Fall &amp; Spring Terms</h4>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Enrollment</th>
                                 
                                 <th scope="col">Credit Hours</th>
                                 
                                 <th scope="col">Monthly Rate</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>Full time</td>
                                 
                                 <td>12</td>
                                 
                                 <td>$1,021.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/4 time</td>
                                 
                                 <td>9-11</td>
                                 
                                 <td>$766.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/2 time</td>
                                 
                                 <td>6-8</td>
                                 
                                 <td>$511.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Less than 1/2 time and more than 1/4 time</td>
                                 
                                 <td>4-5</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $511.00 **</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/4 time or less</td>
                                 
                                 <td>1-3</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $255.25 **</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <h4>Summer Term</h4>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Enrollment</th>
                                 
                                 <th scope="col">Credit Hours For 12 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For 8 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For 6 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For 4 Weeks of Classes</th>
                                 
                                 <th scope="col">Monthly Rate**</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>Full time</td>
                                 
                                 <td>8</td>
                                 
                                 <td>6</td>
                                 
                                 <td>4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>$1,021.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/4 time </td>
                                 
                                 <td>7-6</td>
                                 
                                 <td>5-4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>$766.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/2 time </td>
                                 
                                 <td>5-4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>$511.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Less than 1/2 time, more than 1/4 time </td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>1</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $511.00</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/4 time or less </td>
                                 
                                 <td>1-2</td>
                                 
                                 <td>1</td>
                                 
                                 <td>1</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $255.25</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <p>**Entitlement charged at the rate of one month for each $1,021.00 paid.</p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Scholarships for Chapter 35 Dependants and Spouses</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ol>
                              
                              <li>
                                 <a href="https://scholarships.foldsofhonor.org/">Folds of Honor</a>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>For children and spouses of disabled veterans with a service connected disability
                                       of AT LEAST 10%
                                    </li>
                                    
                                    <li>Applications accepted Jan 15 2015 – Mar 15 2015</li>  
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.floridastudentfinancialaid.org/SSFAD/factsheets/CDDV.pdf">Scholarships for Children and Spouses of Deceased or Disabled Veterans</a>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Scholarship for spouses and children whose vet has retired in the state of Florida
                                       with a 100% service disconnect, or deceased due to service disconnect.
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.benefits.va.gov/gibill/docs/factsheets/fry_scholarship.pdf">Marine Gunnery Sergeant John David Fry Scholarship</a>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>For children of service members who died in active duty on or after September 11th,
                                       2011.
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ol>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Required Documents</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <table class="table table">
                                    
                                    
                                    <tr>
                                       
                                       <th scope="col">Student Category</th>
                                       
                                       <th scope="col">Submit To Valencia</th>
                                       
                                       <th scope="col">Request From Valencia</th>
                                       
                                    </tr>
                                    
                                    
                                    <tr>
                                       
                                       <th scope="row">First Time Using Benefits</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E. (or)</li>
                                             
                                             <li><a href="https://www.ebenefits.va.gov/ebenefits-portal/ebenefits.portal?_nfpb=true&amp;_portlet.async=false&amp;_pageLabel=ebenefits_myeb_vonapp1">22-5490</a></li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transfer Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E. (or)</li>
                                             
                                             <li><a href="https://www.ebenefits.va.gov/ebenefits-portal/ebenefits.portal?_nfpb=true&amp;_portlet.async=false&amp;_pageLabel=ebenefits_myeb_vonapp1">22-5495</a></li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transient Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Transient Form</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </table>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <h4>Document Details</h4>
                                 
                                 <ul class="list_style_1">
                                    
                                    <p><strong>C.O.E</strong> - (Certificate of Eligibility)
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Obtain this from the Department of Veterans Affairs at 1-888-442-4551</li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-5490</strong> - The Department of Veterans Affairs Application for Educational Benefits.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit through VONAPP. Print out and provide a copy with confirmation number to a
                                          Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-5495</strong> - Change of Place of Training Form.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit through VONAPP. Print out and provide a copy with confirmation number to a
                                          Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Certification</strong> - Valencia College Veteran Affairs Certification of Enrollment Request
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The Certification needs to be submitted every semester and can be obtained at your
                                          local Valencia Veterans Services Campus or <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">Online</a>.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Transient Form</strong> - Authorization to take course(s) at a secondary institution
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>For Florida Institutions, submit a <a href="https://www.floridashines.org/">Transient Student Admissions Application</a>
                                          
                                       </li>
                                       
                                       <li>For all other states, request a letter from your primary school addressed to Valencia
                                          College Certifying Official. Please refer to example.
                                       </li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           <div class="row" id="transient_letter">
                              
                              <h4>Example of Transient Letter</h4>
                              
                              <p class="pull-right"><em>[Name and Address of Primary School]</em></p>
                              <br>
                              
                              <p><em>[Date]</em></p>
                              
                              <p><em>[Name and Address of Secondary School]</em></p>
                              
                              <p><em>[Student's Name (Claim Number)]</em> is a chapter <em>[e.g., 30]</em> student at <em>[Name of School (facility code)]</em> pursuing a <em>[Name of Program, e.g. B.S. History degree]</em>.
                              </p>
                              
                              <p>The course(s) listed below satisfy <em>[Name of Program]</em> requirements and will transfer at full value to <em>[Name of School]</em>.
                              </p>
                              
                              <p><em>[List course(s) by title and number.]</em></p>
                              
                              <p>Student intends to take the above course(s) at <em>[Secondary School]</em> during <em>[Identify term, e.g. Spring term 2018]</em> as a guest student. Please certify the courses to VA as the secondary school.
                              </p>
                              
                              <p><em>[Signature of the Certifying Official]</em><br>
                                 <em>[Telephone Number]</em></p>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/chapter-35.pcf">©</a>
      </div>
   </body>
</html>