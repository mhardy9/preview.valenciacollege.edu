<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Reserve Educational Assistance Program (Chapter 1607)  | Valencia College</title>
      <meta name="Description" content="Information on the Reserve Educational Assistance Program benefits and application process.">
      <meta name="Keywords" content="reserve, assistance, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/va-education/chapter-1607.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/financial/va-education/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li><a href="/students/veterans-affairs/financial/va-education/">Va Education</a></li>
               <li>Reserve Educational Assistance Program (Chapter 1607) </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Reserve Educational Assistance Program — Chapter 1607, Title 10 U.S.C</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Eligibility</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Who Is Eligible?</h4>
                           
                           <p>For active members of the Selected Reserve called to active duty for 90 days or more
                              and members of the Individual Ready Reserve called to active duty in support of a
                              contingency operation or a national emergency declared by the President or Congress.
                              Further information is available from <a href="http://www.gibill.va.gov/benefits/other_programs/reap.html" target="_blank">the VA.gov website</a>.
                           </p>
                           
                           
                           <h4>Entitlement</h4>
                           
                           <p>You may be entitled to receive up to 36 months of education benefits, no extensions.</p>
                           
                           <p>If you are eligible for two or more educational benefits, the new maximum would be
                              48 months.
                           </p>
                           
                           
                           <h4>$600 Buy-Up Program</h4>
                           
                           <p>Some reservists may contribute up to an additional $600 to the GI Bill to receive
                              increased monthly benefits. For an additional $600 contribution, you may receive up
                              to $5400 in additional GI Bill benefits.
                           </p>
                           
                           
                           <h4>Period of Eligibility</h4>
                           
                           <p>10 years from the separation date if separated after completing service contract and
                              discharge isn't dishonorable.
                           </p>
                           
                           <p>10 years from Chapter 1607 eligibility date if separated for disability.</p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Entitlement</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Make sure that your start and end date of classes for the term reflects your intended
                              Monthly Rate!
                           </p>
                           
                           
                           <h4>Current Payment Rates</h4>
                           
                           <p>Effective October 1, 2015.</p>
                           
                           
                           <h4>Fall &amp; Spring Terms</h4>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Enrollment</th>
                                 
                                 <th scope="col">Credit Hours</th>
                                 
                                 <th scope="col">Monthly Rate** for service longer than 90 days but less than one year</th>
                                 
                                 <th scope="col">Monthly Rate** for at least one year of service</th>
                                 
                                 <th scope="col">Monthly Rate** for at least two years of service</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>Full time</td>
                                 
                                 <td>12</td>
                                 
                                 <td>$715.60</td>
                                 
                                 <td>$1,073.40</td>
                                 
                                 <td>$1,431.20</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/4 time</td>
                                 
                                 <td>9-11</td>
                                 
                                 <td>$536.70</td>
                                 
                                 <td>805.05</td>
                                 
                                 <td>$1,073.40</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/2 time</td>
                                 
                                 <td>6-8</td>
                                 
                                 <td>$357.80</td>
                                 
                                 <td>$536.70</td>
                                 
                                 <td>$715.60</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Less than 1/2 time more than 1/4 time</td>
                                 
                                 <td>4-5</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $357.80 </td>
                                 
                                 <td>Tuition &amp; fees not to exceed $536.70 </td>
                                 
                                 <td>Tuition &amp; fees not to exceed $715.60</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/4 time or less</td>
                                 
                                 <td>1-3</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $178.90</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $268.35</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $357.80</td>
                                 
                              </tr>
                              
                           </table>
                           
                           
                           <h4>Summer Term</h4>
                           
                           <table class="table table">
                              
                              <tr>
                                 
                                 <th scope="col">Enrollment</th>
                                 
                                 <th scope="col">Credit Hours For 12 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For 8 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For 6 Weeks of Classes</th>
                                 
                                 <th scope="col">Credit Hours For 4 Weeks of Classes</th>
                                 
                                 <th scope="col">Monthly Rate** for service longer than 90 days but less than one year</th>
                                 
                                 <th scope="col">Monthly Rate** for at least one year of service</th>
                                 
                                 <th scope="col">Monthly Rate** for at least two years of service</th>
                                 
                              </tr>
                              
                              
                              <tr>
                                 
                                 <td>Full time</td>
                                 
                                 <td>8</td>
                                 
                                 <td>6</td>
                                 
                                 <td>4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>$715.60</td>
                                 
                                 <td>$1,073.40</td>
                                 
                                 <td>$1,431.20</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>3/4 time</td>
                                 
                                 <td>7-6</td>
                                 
                                 <td>5-4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>$536.70</td>
                                 
                                 <td>$805.05</td>
                                 
                                 <td>$1,073.40</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/2 time</td>
                                 
                                 <td>5-4</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>$357.80</td>
                                 
                                 <td>$536.70</td>
                                 
                                 <td>$715.60</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>Less than 1/2 time more than 1/4 time</td>
                                 
                                 <td>3</td>
                                 
                                 <td>2</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>1</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $357.80</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $536.70</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $715.60</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <td>1/4 time or less</td>
                                 
                                 <td>1-2</td>
                                 
                                 <td>1</td>
                                 
                                 <td>1</td>
                                 
                                 <td>n/a</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $178.90</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $268.35</td>
                                 
                                 <td>Tuition &amp; fees not to exceed $357.80</td>
                                 
                              </tr>
                              
                              
                           </table>
                           
                           
                           <p>**Entitlement is charged based on the rate of one month for a benefit amount equal
                              to the full-time institutional rate. If you participated in the "$600.00 buy-up,"
                              consult the <a href="http://www.gibill.va.gov/resources/benefits_resources/rates/600_buyup.html">VA.gov website</a> for rates.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Required Documents</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <table class="table table">
                                    
                                    
                                    <tr>
                                       
                                       <th scope="col">Student Category</th>
                                       
                                       <th scope="col">Submit To Valencia</th>
                                       
                                       <th scope="col">Request From Valencia</th>
                                       
                                    </tr>
                                    
                                    
                                    <tr>
                                       
                                       <th scope="row">First Time Using Benefits</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>C.O.E; (or) 22-1990</li>
                                             
                                             <li>DD-214</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transfer Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>22-1995</li>
                                             
                                             <li>DD-214</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                             <li>Statement of Understanding</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                    <tr>
                                       
                                       <th scope="row">Transient Students</th>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Transient Form</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                       <td>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Certification</li>
                                             
                                          </ul>
                                          
                                       </td>
                                       
                                    </tr>
                                    
                                 </table>
                                 
                              </div>
                              
                              <div class="col-md-6">
                                 
                                 <h4>Document Details</h4>
                                 
                                 <ul class="list_style_1">
                                    
                                    <p><strong>C.O.E</strong> - (Certificate of Eligibility)
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Obtain this from the Department of Veterans Affairs at 1-888-442-4551</li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-1990</strong> -The Department of Veterans Affairs Application for Educational Benefits.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit 22-1990 through VONAPP. Print out and provide a copy with confirmation number
                                          to a Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>DD-214</strong> - Certificate of Release or Discharge from Active Duty.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The DD-214 is issued upon a military service member's retirement, separation, or discharge
                                          from active-duty military.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>22-1995</strong> - Change of Place of Training Form.
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>Submit 22-1995 through VONAPP. Print out and provide a copy with confirmation number
                                          to a Valencia College VA Representative.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Certification</strong> - Valencia College Veteran Affairs Certification of Enrollment Request
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>The Certification needs to be submitted every semester and can be obtained at your
                                          local Valencia Veterans Services Campus or <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">Online</a>.
                                       </li>
                                       
                                    </ul>
                                    
                                    <p><strong>Transient Form</strong> - Authorization to take course(s) at a secondary institution
                                    </p>
                                    
                                    <ul class="list_style_1">
                                       
                                       <li>For Florida Institutions, submit a <a href="https://www.floridashines.org/">Transient Student Admissions Application</a>
                                          
                                       </li>
                                       
                                       <li>For all other states, request a letter from your primary school addressed to Valencia
                                          College Certifying Official. Please refer to example.
                                       </li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <hr class="styled_2">
                           
                           
                           
                           <div class="row" id="transient_letter">
                              
                              <h4>Example of Transient Letter</h4>
                              
                              <p class="pull-right"><em>[Name and Address of Primary School]</em></p>
                              <br>
                              
                              <p><em>[Date]</em></p>
                              
                              <p><em>[Name and Address of Secondary School]</em></p>
                              
                              <p><em>[Student's Name (Claim Number)]</em> is a chapter <em>[e.g., 30]</em> student at <em>[Name of School (facility code)]</em> pursuing a <em>[Name of Program, e.g. B.S. History degree]</em>.
                              </p>
                              
                              <p>The course(s) listed below satisfy <em>[Name of Program]</em> requirements and will transfer at full value to <em>[Name of School]</em>.
                              </p>
                              
                              <p><em>[List course(s) by title and number.]</em></p>
                              
                              <p>Student intends to take the above course(s) at <em>[Secondary School]</em> during <em>[Identify term, e.g. Spring term 2018]</em> as a guest student. Please certify the courses to VA as the secondary school.
                              </p>
                              
                              <p><em>[Signature of the Certifying Official]</em><br>
                                 <em>[Telephone Number]</em></p>
                              
                           </div>
                           
                        </div>
                        
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/va-education/chapter-1607.pcf">©</a>
      </div>
   </body>
</html>