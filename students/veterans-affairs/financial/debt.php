<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>VA Debt  | Valencia College</title>
      <meta name="Description" content="fixme">
      <meta name="Keywords" content="debt, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/debt.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li>VA Debt </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>VA Debt</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Instructions to Obtain a Financial Status Report — VA Form 5655</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>In order to obtain a Financial Status Report VA Form 5655 over the Internet, do the
                              following:
                           </p>
                           
                           
                           <ol>
                              
                              <li>Go to <a href="https://www.va.gov" target="_blank">the VA.gov website</a>.
                              </li>
                              
                              <li>On the bottom of the page, under "Quick List" section, click on "VA Forms".</li>
                              
                              <li>Enter 5655 in the Form Number block.</li>
                              
                           </ol>
                           
                           
                           <p>You will be given two options:</p>
                           
                           <ol>
                              
                              <li>
                                 5655blank — Financial Status Report (blank copy)<br>
                                 This form may be printed and filled out manually.
                                 
                              </li>
                              
                              <li>
                                 5655 — Financial Status Report<br>
                                 This form may be filled out online and emailed to <a href="mailto:dmc.ops@va.gov">dmc.ops@va.gov</a> (see instructions below).
                                 
                              </li>
                              
                           </ol>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Instructions to Request a Lower Withholding from <em>Current</em> VA Benefits
                           </h3>          
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>To request a lower <em>Withholding</em>: Complete and sign the Financial Status Report - VA Form 5655.  Specify "Lower Withholding"
                              in Block "3" of Financial Status Report and enter amount of the monthly withholding
                              amount you are requesting in block "24B".
                           </p>
                           
                           <p>If you are requesting a <em>Partial Refund</em> of a benefit that was already taken and applied to the debt, please state "Lower
                              Withholding/Refund" in Block "3" of the Financial Status Report.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Instructions to Request a Waiver</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>To request a Waiver: Complete and sign the Financial Status Report - VA Form 5655.
                              Specify "Waiver" in Block "3" of Financial Status Report.   In addition to completing
                              the Financial Status Report, please write a letter on a separate sheet of paper stating
                              that you are "Requesting a Waiver".
                           </p>
                           
                           <p>NOTE: The word "waiver" must be included in your letter.  Explain why you are requesting
                              the waiver and why you don’t feel you should be held responsible for this debt.  You
                              may include supporting documentation you feel will help your case. 
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Remember to put your file number and name on the top of each page of documentation.</li>
                              
                              <li>Please Note:  You have 180 days from the date of the first notification letter to
                                 request a waiver.
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Instructions to Establish a Monthly Payment Arrangement Where Payments Would Be Submitted
                              to VA to Credit the Debt
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>To request a lower Payment Plan: Complete and sign the Financial Status Report -VA
                              Form 5655.  Specify "Payment Plan" in Block "3" of Financial Status Report and enter
                              the monthly payment amount you are requesting in block "24B".
                           </p>
                           
                           <p>You will receive notification from Debt Management Center either agreeing to the requested
                              amount or advising you of the amount we would be able to accept.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Instructions to Submit the Financial Status Report — VA Form 5655, or Any Additional
                              Information to Debt Management Center
                           </h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You can submit the Financial Status Report and any additional documents the following
                              ways: 
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Via Fax: 612-970-5798</li>
                              
                              <li>Via Mail:
                                 
                                 <address>
                                    VA Debt Management<br>
                                    P. O. Box 11930<br>
                                    St. Paul, MN 55111-0930 
                                    
                                 </address>
                                 
                              </li>
                              
                              <li>
                                 Via Email: <a href="mailto:dmc.ops@va.gov">dmc.ops@va.gov</a><br>
                                 Note: This and email box in the correspondence unit.  They will process your request
                                 in the order it is received. If you send via Email, please indicate the purpose of
                                 the Email on the "Subject" line i.e.: <em>REPAY</em>, <em>WAIVER</em>, <em>PAYMENT PLAN</em> or <em>REPAY and WAIVER</em>.
                                 
                              </li>
                              
                           </ul>
                           
                           <p>Make sure to keep a copy of all documents you send to the Debt Management Center.</p>
                           
                        </div>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/debt.pcf">©</a>
      </div>
   </body>
</html>