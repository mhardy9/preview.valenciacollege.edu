<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>C.W. Bill Young Tuition  | Valencia College</title>
      <meta name="Description" content="The C.W. Bill Young tuition waiver waives out of state tuition charges. If you cannot prove Florida Residency for tuition purposes, this is another option for you.">
      <meta name="Keywords" content="bill, young, tuition, waiver, florida, gi, bill, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/tuition-waiver.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li>C.W. Bill Young Tuition </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>C.W. Bill Young Tuition Waiver</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>The C.W. Bill Young tuition waiver waives out of state tuition charges. If you cannot
                              prove <a href="https://valenciacollege.edu/admissions-records/florida-residency/">Florida Residency for tuition purposes</a>, this is another option for you.
                           </p>
                           
                           
                           <p>This waiver is granted to two different groups of students:</p>
                           
                           <ol>
                              
                              <li>Students who use a VA educational benefit. (This includes dependents.)
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>This group is awarded the waiver automatically if the <a href="/students/veteran-affairs/">VA office</a> has received <a href="/students/veteran-affairs/financial/va-education/">proof of benefit</a> and a <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">certification request</a> by the first day of the term.
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>Veterans who are not using a VA educational benefit but can provide the VA office
                                 a copy of their DD-214 member 4 showing an HONORABLE discharge.
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>This group is awarded the waiver manually by <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=dgibson11">Valencia's VA School Certifying Official</a> after reviewing the DD-214. This process can take several days to process, but must
                                       be completed by the first day of the term.
                                    </li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ol>
                           
                           <p>If you have any further questions regarding this waiver please contact the VA office
                              by phone at (407) 582-VETS or <a href="mailto:Veterans@valenciacollege.edu">email</a>.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/tuition-waiver.pcf">©</a>
      </div>
   </body>
</html>