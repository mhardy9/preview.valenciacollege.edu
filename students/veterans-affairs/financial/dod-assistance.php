<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>DOD Assistance  | Valencia College</title>
      <meta name="Description" content="Provides an overview of tuition assistance programs available by branch of service, as well as requirements and how to apply for assistance.">
      <meta name="Keywords" content="tuition, assistance, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/dod-assistance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li>DOD Assistance </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>DOD Tuition Assistance</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="wrapper_indent">
                           
                           <p><a href="mailto:jnagy7@valenciacollege.edu">Joy Nagy is the Valencia College Business Office Point of Contact.</a></p>
                           
                           <p>Select a branch of service for more information.</p>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="#air-force">Air Force Tuition Assistance</a></li>
                              
                              <li><a href="#army">Army Tuition Assistance</a></li>
                              
                              <li><a href="#navy">Navy Tuition Assistance</a></li>
                              
                              <li><a href="#marines">Marine Corps Tuition Assistance</a></li>
                              
                              <li><a href="#coast-guard">Coast Guard Tuition Assistance</a></li>
                              
                              <li><a href="#military-spouse">MyCAA Military Spouse Career Advancement Accounts</a></li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="air-force">
                           
                           <h3>Air Force Tuition Assistance</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>You must apply for Tuition Assistance online using the Air Force Virtual Education
                              Center. This must be done through the <a href="https://www.my.af.mil/">Air Force Portal</a>. There are six steps to completing the <a href="http://www.military.com/education/money-for-school/the-air-forces-virtual-education-center-online-ta-request.html">AFVEC online TA process</a>.
                           </p>
                           
                           <p>You will be unable to apply online for TA if the following applies to you:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Missing grades over 60 days from course end date.</li>
                              
                              <li>Suspense dates that have expired.</li>
                              
                              <li>Missing personal data in the education record including: Phone, DOS, DOB, Unit, Office
                                 Symbol, Mailing Address, Email Address, base, and Education Level.
                              </li>
                              
                              <li>Requesting TA for courses that start more than 30 days into the future.</li>
                              
                              <li>Requesting TA for courses that have already started.</li>
                              
                              <li>Requesting TA for lower level courses which are less than highest ed level awarded.</li>
                              
                              <li>No degree plan in records.</li>
                              
                           </ul>
                           
                           <p>TA is not authorized for courses leading to a lateral or lower level degree than you
                              already possess (i.e. Second Associate's or Bachelor's degree).
                           </p>
                           
                           <p>After you have completed your tuition assistance form and registered for class, you
                              may still drop/change courses without penalty as long as you notify both the base
                              education center and the school. If you drop a course after the drop/ add period,
                              you must still notify the base ed center and the school but you are liable for the
                              cost of tuition, unless you qualify for waiver of tuition assistance reimbursement.
                           </p>
                           
                           <p>If you receive a grade of incomplete from a school, you have as much time as the school
                              allows you to clear the incomplete or 12 months from the end of the term, whichever
                              comes first, to clear the incomplete. If you fail to provide a grade that clears the
                              incomplete by that time, we are obligated to recover the tuition assistance.
                           </p>
                           
                           <p>No Tuition Assistance for post-masters degree course work or degree.</p>
                           
                           
                           <h4>Online TA Request</h4>
                           
                           <p>Before you can access the online TA request process, you must create a user name and
                              password on the AFVEC. This can be done through the <a href="https://www.my.af.mil/">Air Force Portal</a>.
                           </p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 
                                 <h5>Step One - Select Reason for Request</h5>
                                 
                                 <p>Enrollment Reason options are available for the Air Force to better determine why
                                    you choose to utilize TA for off-duty education. Please select the reason that best
                                    identifies why you are using TA. Definitions of each of the four options are available
                                    to help you make a selection.
                                 </p>
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 <h5>Step Two - Select the School Name (Civilian Institution)</h5>
                                 
                                 <p>Select the school or civilian institution that you will be attending from the list
                                    provided. (Valencia College)
                                 </p>
                                 
                              </li>
                              
                              
                              
                              <li>
                                 
                                 <h5>Step Three - Enter Term Dates</h5>
                                 
                                 <p>Enter the beginning and end dates for the term you will be taking. Please note that
                                    term dates are inclusive dates of a specific term and do not necessarily represent
                                    the exact start date for your specific course (i.e., term starts on Monday, Jan 10,
                                    but your course does not start until Wednesday, Jan 12). The dates must be exact or
                                    the TA will be disapproved.
                                 </p>
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 <h5>Step Four - Enter the Course Information</h5>
                                 
                                 <p>Enter the course information by manually entering the course information.</p>
                                 
                                 <p>Continue to add courses to the form until you have added all courses that you will
                                    be taking for that SCHOOL and TERM.
                                 </p>
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 <h5>Step Five - Enter the Registration Fees</h5>
                                 
                                 <p>Select the registration fees from the drop down list. These fees are only paid by
                                    the Air Force if payment of these fees is MANDATORY as a condition of enrollment.
                                    Enter each fee type and cost separately.
                                 </p>
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 <h5>Step Six - Verify TA Information and Submit Request</h5>
                                 
                                 <p>Verify that all of the TA information on the form is correct. Be careful to note school,
                                    term and course information to ensure that the information provided is accurate. Use
                                    the back buttons to correct any errors.
                                 </p>
                                 
                                 <p>If AFVEC finds that TA cannot cover some or all of the tuition, you will be notified
                                    and given the option to elect the "VA Top-up" GI Bill option. Follow the <a href="http://www.military.com/education/money-for-school/gi-bill/tuition-top-up-program.html">Top-Up</a> link to verify your eligibility. You may use this option to supplement any cost not
                                    covered by TA if you are GI Bill qualified. Lastly, make sure your email address is
                                    updated! This is the address where all communications between you and the education
                                    office will occur. You must use your military account unless you do not have one.
                                 </p>
                                 
                                 <p>Be sure to read each of the conditions and certifications. You must agree to all conditions
                                    and certifications by checking them off prior to submitting your application for approval.
                                    Once you have agreed to all conditions and certifications, enter your full name and
                                    "MY AFVEC" password to submit.
                                 </p>
                                 
                                 <p>Your application will be submitted to your local education center for final approval/disapproval.
                                    Do not factor this TA into defraying tuition costs until you receive final approval
                                    from the education center.
                                 </p>
                                 
                              </li>
                              
                              
                              <li>
                                 
                                 <h5>After your request is approved</h5>
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>You will receive notification of approved TA form.</li>
                                    
                                    <li>The approved TA form will have both the approval official's and your digitally signed
                                       signatures.
                                    </li>
                                    
                                    <li>You MUST send a copy of the approved TA form to the Business Office</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="army">
                           
                           <h3>Army Tuition Assistance</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>The program is open to nearly all soldiers (officers, warrant officers, enlisted)
                              including Army Reserve, and Army National Guard on active duty.
                           </p>
                           
                           <p>There are some restrictions to who may use Army Tuition Assistance and what courses
                              it can be used for. Download AR 621-5 (Army Continuing Education System Regulations)
                              to get further details.
                           </p>
                           
                           <p>The Army Continuing Education System has launched a new automated tuition assistance
                              project. The TA automation will allow active duty soldiers to request TA and enroll
                              in courses online. This will speed up the enrollment process and minimize wait times
                              at Army Education Centers. Active Duty soldiers are now able to request TA online
                              through GoArmyEd.
                           </p>
                           
                           <p>Soldiers must request TA through <a href="http://www.goarmyed.com/">www.GoArmyEd.com</a>, prior to the course start date or before the school’s late registration period.
                           </p>
                           
                           <p>TA is requested on a course-by-course basis.</p>
                           
                           <p>GoArmyEd will notify the Soldier whether the TA is approved or not. If the TA request
                              is not approved, GoArmyEd will advise the Soldier of the reason and next steps.
                           </p>
                           
                           <p><strong>Once a TA is approved, please print the form and submit it to the Business Office.</strong></p>
                           
                           <p>All drops/withdrawals must take place through GoArmyEd. Soldiers who receive an "F"
                              grade will be required to repay TA. Soldiers who are unable to complete a course due
                              to military duties must ensure that they withdraw from the course through GoArmyEd
                              to ensure that they will not be charged.
                           </p>
                           
                           <p>Further details regarding TA procedures are outlined in GoArmyEd Training.</p>
                           
                           <p>For general information about the Army's automated TA process, visit <a href="http://www.goarmyed.com/">www.GoArmyEd.com</a>.
                           </p>
                           
                           <p><a href="https://www.goarmyed.com/public/facility_pages/NG_Florida_Education_Services_Office/default.asp">Florida National Guard Points of Contact</a></p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="navy">
                           
                           <h3>Navy Tuition Assistance</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>TA is available to both Naval Officer and Enlisted active duty personnel and Naval
                              Reservists on continuous active duty. It is also available to enlisted Naval Reservists
                              ordered to active duty 120 days and to Naval Reservist Officers ordered to active
                              duty for 2 years or more. To qualify, service members must:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>be on active duty for the whole length of the course.</li>
                              
                              <li>receive counseling from a <a href="https://www.navycollege.navy.mil/ncp/nc_office.aspx">Navy College Office</a>
                                 
                              </li>
                              
                              <li>provide all grades from previously funded TA courses and reimburse all W and F grades.
                                 (Withdrawals for involuntary reasons may be granted with command verification.)
                              </li>
                              
                              <li>Agree, if an officer, to remain on active duty for at least two years upon completion
                                 of courses funded by TA. This obligation runs concurrently with remaining obligated
                                 service time. Those who fail to serve the obligation must repay the TA funds expended
                                 on their behalf during the last two years of active duty on a pro rated basis.
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Application Process</h4>
                           
                           <p>Contact your Navy College Office to receive educational counseling in person, by phone
                              or email. With your Navy College advisor, determine which courses will be requested
                              for TA funding.
                           </p>
                           
                           <p>Complete a TA Application form NETPDTC 1560/3 listing course(s) and fee(s). Check
                              with Valencia College to make sure the amounts for tuition and fees you list are correct.
                              Allowable fees that can be funded under Tuition Assistance are:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Fees directly required for course enrollment may be combined with tuition. Navy will
                                 pay fees that are published, mandatory, and charged for course enrollment.
                              </li>
                              
                              <li>Mandatory non-reimbursable fees meeting the criteria listed above may be funded with
                                 TA. However, if the course is canceled allowing the tuition to be refunded, the student
                                 is responsible for paying the non-reimbursable fee.
                              </li>
                              
                              <li>Sailors requesting payment of fees with tuition are responsible for providing accurate
                                 fee information to their Navy College Office when applying for TA
                              </li>
                              
                           </ul>
                           
                           <p>Carefully read the second page of the TA Application, the Tuition Assistance Application
                              Agreement, and complete the requested information at the bottom of the page. Your
                              signature on this form indicates youunderstand the current rules relating to Tuition
                              Assistance funding. Receive command approval signature to enroll in the course(s).
                           </p>
                           
                           <p>Your TA Application MUST be returned to your Navy College Office for processing.</p>
                           
                           <p>For further program details and Navy Tuition Assistance program updates, visit the
                              Navy College website Tuition Assistance page.
                           </p>
                           
                           <p>Once you have an approved TA, please print it and submit it to the Business Office.</p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="marines">
                           
                           <h3>Marine Corps Tuition Assistance</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           <h4>Application Process</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>First-time students must complete a TA Orientation Class PRIOR to using TA.</li>
                              
                              <li>Marines at remote sites (non-Marine Corps installations) may access the course by
                                 clicking on the TA Orientation link below. All others: Please proceed to your local
                                 base Lifelong Learning or Education Office.
                              </li>
                              
                              <li>Marines must apply for and receive written authorization for TA through the appropriate
                                 education office, PRIOR to enrollment. Use <a href="https://www.navycollege.navy.mil/docs/TAAppPaperVers_110207.doc">form NETPDTC 1560</a>.
                              </li>
                              
                              <li>TA for remote Marines and those assigned to other service sites is processed at Camp
                                 Lejeune for those east of the Mississippi and Camp Pendleton for those west of the
                                 Mississippi. I-I duty Marines apply through MARFORRES, New Orleans. Recruiters apply
                                 via their recruiting district or region headquarters.
                              </li>
                              
                           </ul>
                           
                           
                           <p>Download the <a href="http://www.usmc-mccs.org/education/downloads/College101Notes.pdf">TA Orientation (PDF format)</a>. When you have completed the orientation please fill out and print the <a href="http://www.usmc-mccs.org/education/certform.swf">certification form</a> as well.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Once a Marine is enrolled with TA he or she must submit a degree plan before exceeding
                                 12 semester hours.
                              </li>
                              
                              <li>TA will fund up to 100% of institution charges for tuition, instructional fees, laboratory
                                 fees, computer fees and mandatory enrollment fees combined for postsecondary education,
                                 from vocational certification through graduate study.
                              </li>
                              
                              <li>TA is not authorized for books.</li>
                              
                              <li>TA is not authorized for courses leading to a lateral or lower level degree than you
                                 already possess (i.e. second Associate's or Bachelor's degree).
                              </li>
                              
                              <li>Officers using TA agree to remain on active duty for two (2) years following the completion
                                 of the TA funded course.
                              </li>
                              
                           </ul>
                           
                           
                           <p>Once you have an approved TA, please print it and submit it to the Business Office.</p>
                           
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="coast-guard">
                           
                           <h3>Coast Guard Tuition Assistance</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>For FY2014 the Coast Guard revised the eligiblity and payment rates for the tuition
                              assistance program. (<a href="http://www.military.com/education/money-for-school/coast-guard-tuition-assistance.html#ALCOAST">See ALCOAST 502/13 Below for details</a>)
                           </p>
                           
                           
                           <h4>Application Process</h4>
                           
                           <p>The management of TA is centralized at the USCG Institute through a consolidated tuition
                              assistance processing system managed by the U.S. Naval Education and Training Professional
                              Development and Technology Center (NETPDTC) in Pensacola, Florida.
                           </p>
                           
                           <p>The Coast Guard Institute approves the <a href="http://www.uscg.mil/hq/cgi/downloads/forms/CG_Form_4147.pdf">TA Application (CG-4147)</a>, inputs data into the Navy's computer database, and issues the TA Authorization form
                              (CGI-1560).
                           </p>
                           
                           <p>Once you have an approved TA, please print it and submit it to the Business Office.</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Tuition assistance is not authorized for use to meet unit specific operational training
                                 requirements.
                              </li>
                              
                              <li>Eligibility and benefits are standardized service wide for Coast Guard active duty
                                 and reserve on active duty for more than 180 days.
                              </li>
                              
                              <li>TA will be authorized "up-front" for traditional college coursework for courses less
                                 than 18 weeks in length. Courses may be resident or remote.
                              </li>
                              
                              <li>There is no limitation on the use of TA when a member is receiving "financial aide"
                                 such as a student loan, Sallie Mae, Stafford loan, etc.
                              </li>
                              
                              <li>All courses must be taken from a nationally or regionally accredited institution,
                                 resulting in college credit or accredited clock or contact hours.
                              </li>
                              
                              <li>TA is not authorized for reimbursement for books.</li>
                              
                              <li>Developmental courses may be authorized if required by the institution prior to taking
                                 a freshman level course. Many colleges require a developmental course in Math, English
                                 and reading if the applicant has been out of school for several years. Applicants
                                 should note that these courses (usually numbered starting with zero "0" as the first
                                 digit) are not transferable.
                              </li>
                              
                           </ul>
                           
                           <p>TA does <strong>not</strong> cover the following expenses:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Application, entrance or enrollment fees</li>
                              
                              <li>Record-maintenance fees</li>
                              
                              <li>Student activity fees/ Student ID</li>
                              
                              <li>Course registration fees</li>
                              
                              <li>Textbooks, manuals</li>
                              
                              <li>Non-consumable materials</li>
                              
                              <li>Assembled items available commercially such as computers, televisions, robots</li>
                              
                              <li>Fees for flight time, flying lessons, or noncredit aviation classes</li>
                              
                              <li>Parking fee</li>
                              
                              <li>Cost of tools, protective or other equipment that becomes the property of the student</li>
                              
                              <li>Certification courses and tests, or licenses.</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in" id="military-spouse">
                           
                           <h3>MyCAA Military Spouse Career Advancement Accounts</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Unofficially referred to as Spouse Tuition Assistance, the Department of Defense recently
                              expanded Military Spouse Career Advancement Accounts (MyCAA) program can provide up
                              to $4,000 of Financial Assistance for military spouses.
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>Be available to spouses of active duty service members in pay grades E1-E5, W1-W2,
                                 and O1-O2. including the spouses of activated Guard and Reserve members within those
                                 ranks. Spouses of Guard and Reserve members must be able to start and complete their
                                 courses while their sponsor is on Title 10 orders.
                              </li>
                              
                              <li>Offer a maximum financial benefit of $4,000 with a fiscal year cap of $2,000. Waivers
                                 will be available for spouses pursuing licensure or certification up to the total
                                 maximum assistance of $4,000
                              </li>
                              
                              <li>Require military spouses to finish their program of study within three years from
                                 the start date of the first course
                              </li>
                              
                              <li>Be limited to associate degrees, certification and licensures</li>
                              
                              <li>MyCAA does not pay for education and training programs that include computers (CPUs
                                 or laptops); application, graduation or membership fees; student activity cards; child
                                 care; parking; transportation; or medical services.
                              </li>
                              
                              <li>If a spouse enrolls in a course without an approved MyCAA Financial Assistance (FA)
                                 document, the spouse will be responsible for paying course costs.
                              </li>
                              
                              <li>MyCAA does not provide reimbursements of any kind to spouses for any reason.</li>
                              
                              <li>MyCAA FA payments are made directly to schools using MyCAA’s electronic invoicing
                                 system. As A Second Language (ESL) Classes and GED Classes and Testing Programs.
                              </li>
                              
                              <li>Unfortunately, Coast Guard spouses are not covered by the MyCAA Spouse Employment
                                 congressional mandate: PL 110-417 Sec 582. Coast Guard is a part of the Department
                                 of Homeland Security (DHS), not the Department of Defense.
                              </li>
                              
                           </ul>
                           
                           <h4>The MyCAA Application Process</h4>
                           
                           <p>Eligible spouses can establish a MyCAA Account by <a href="https://aiportal.acc.af.mil/mycaa">visiting the MyCAA website</a>. Once spouse Profile information is provided, MyCAA will verify spouse DEERS benefit
                              eligibility. Eligible spouses will be allowed to create their Career and Training
                              Plan and request FA when they are within 30 days of course start dates. Additionally,
                              spouses are responsible for applying to their selected school or program and enrolling
                              in each course included in their approved MyCAA Career and Training Plan.
                           </p>
                           
                           <p>Once you have an approved TA, please print it and submit it to the Business Office.</p>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/dod-assistance.pcf">©</a>
      </div>
   </body>
</html>