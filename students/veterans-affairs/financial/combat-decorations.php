<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Combat Decorations (Tuition)  | Valencia College</title>
      <meta name="Description" content="If the student is currently a Florida resident as well as a Florida resident when awarded a superior combat decoration they can give their DD-214 to the school business office to have the tuition and fees (not to include Special fees or Lab fees) taken care of.">
      <meta name="Keywords" content="combat, decorations, tuition, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/financial/combat-decorations.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/financial/">Financial</a></li>
               <li>Combat Decorations (Tuition) </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Combat Decorations (Tuition)</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Combat Decorations Superior in Precedence</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>If the student is currently a Florida resident as well as a Florida resident when
                              awarded a superior combat decoration they can give their DD-214 to the school business
                              office to have the tuition and fees (not to include Special fees or Lab fees) taken
                              care of. This waiver is applicable for 110% of the number of required credit hours
                              of the degree or certificate program for which the student is enrolled.
                           </p>
                           
                           <h4>List</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Purple Heart</li>
                              
                              <li>Bronze Star (must be "V" designation or device)</li>
                              
                              <li>Distinguished Flying Cross</li>
                              
                              <li>Legion of Merit (must be "V" designation or device)</li>
                              
                              <li>Silver Star</li>
                              
                              <li>Air Force Cross</li>
                              
                              <li>Navy Cross</li>
                              
                              <li>Distinguished Service Cross</li>
                              
                              <li>Medal of Honor</li>
                              
                           </ul>
                           
                        </div>
                        
                     </div>
                     
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/financial/combat-decorations.pcf">©</a>
      </div>
   </body>
</html>