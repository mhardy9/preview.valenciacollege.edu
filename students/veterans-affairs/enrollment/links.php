<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Links  | Valencia College</title>
      <meta name="Description" content="Useful links to websites with resources for military veterans and their families.">
      <meta name="Keywords" content="links, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/enrollment/links.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/enrollment/">Enrollment</a></li>
               <li>Links </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Links</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Valencia Links</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="https://valenciacollege.edu/advising/">Advising &amp; Counseling </a><br>
                                 Seek academic advising and counseling services.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/studentservices/answer.cfm">Answer Center </a><br>
                                 Get answers to many of your questions during the application and enrollment process
                                 to Valencia College.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/student-services/">Educational &amp; Wellness Resources </a><br>
                                 Enhance your knowledge in the areas of wellness and academics
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/finaid/">Financial Aid </a><br>
                                 Apply for Financial Aid!
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/lifemap/CareerCenters.cfm">Career Center </a><br>
                                 Need assistance in making career decisions?
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/helpas/">Career Program Services </a><br>
                                 Preparing for an Associate in Science Degree or Certificate Program? Speak with a
                                 Career Program Advisor.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/internship/">Internship &amp; Workforce Services Office </a><br>
                                 Pursue opportunities in a specific field such as employment or internship.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/osd/">Office for Students with Disabilities </a><br>
                                 Provides individual assistance to students with disabilities.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/studentservices/skillshops.cfm">Skillshops </a><br>
                                 Take advantage of the free workshops that provides real-life solutions to common student
                                 issues.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/studentdev/">Student Development </a><br>
                                 Become a Student Leader.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/Labs/">Student Learning Laboratories </a><br>
                                 Designed to assist in reading, writing, and mathematics skills.
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/learning-support/browse-by-campus.cfm">Tutoring Services </a><br>
                                 Tutoring services for academic courses at No Charge!
                                 
                              </li>
                              
                              
                              <li>
                                 <a href="https://valenciacollege.edu/studentdev/clubs/">Valencia Student Veterans of America </a><br>
                                 Be a part of a coalition of student veterans at Valencia College.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>External Links</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Note: many websites with ".com" addresses are for-profit; use with caution!</p>
                           
                           
                           <h4>Transition</h4>
                           
                           <p>Health, education, career information for military veterans and their families.</p>
                           
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://maketheconnection.net/">Make the Connection</a> - create a tailored profile, share experiences &amp; support
                              </li>
                              
                              <li>
                                 <a href="http://www.bbbmarketplacetrust.org/category/291048/military-and-veterans">BBB Institute for Marketplace Trust</a> - comprised of 112 Better Business Bureaus includes financial literacy &amp; searchable
                                 database for consumer protection
                              </li>
                              
                              <li>
                                 <a href="http://spreadtheword.veteranscrisisline.net/">Veterans Crisis Line</a> - assistance for veterans, family or friends of veterans [regardless to whether registered]
                                 via text, call, or live chat online.
                              </li>
                              
                              <li>
                                 <a href="http://projectsanctuary.us/">Project Sanctuary</a> - provide means for military families to decompress and distress after deployment/
                                 exit from service.
                              </li>
                              
                              <li>
                                 <a href="http://militaryhandbooks.com/view-military-handbooks/us-military-handbook/">Military Handbooks</a> - download or purchase guide book; site has useful links/ resources per military
                                 branch including link to children’s scholarship resources. [provided through mail
                                 upon completion of service]
                              </li>
                              
                              <li>
                                 <a href="http://www.military.com/">Military.com</a> - general knowledge, news, entering military, active duty and spouses
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Benefits</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="https://www.ebenefits.va.gov/ebenefits/apply#education">eBenefits</a> - one stop site for health, education, pension and home loan benefits
                              </li>
                              
                              <li>
                                 <a href="http://www.vetcenter.va.gov/">Vet Center</a> - provides combat veterans readjustment and trauma counseling, and conducts workshops
                                 for the community.
                              </li>
                              
                           </ul>
                           
                           
                           <p>Florida Resources - Community Assistance</p>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="https://www.uwcf.org/servlet/eAndar.article/836/HomePage">United Way</a> - United Way agency links to additional locations (agencies and programs)
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Education/College Prep</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://collegecost.ed.gov/">College Affordability and Transparency Center</a> – search US Dept. of Education site for:
                                 
                                 <ul class="list_style_1">
                                    
                                    <li>Scoreboard</li>
                                    
                                    <li>Net price calculator</li>
                                    
                                    <li>Navigator</li>
                                    
                                    <li>Affordability</li>
                                    
                                    <li>90/10 (schools not meeting 90% max.</li>
                                    
                                 </ul>
                                 
                              </li>
                              
                              <li>
                                 <a href="http://www.nelnetsolutions.com/dantes/">OASC &amp; CPST</a> - ASVAB, basic skills, CLEP &amp; DSST (Dantes)
                              </li>
                              
                              <li>
                                 <a href="http://literacydirectory.org">National Literacy Directory</a> - GED, adult basic ed., civics, computer, health
                              </li>
                              
                              <li>
                                 <a href="http://collegeforadults.org/studentcenter/brushup.html">College For Adults: Academic Brush-Up</a> - Planning, applying, academic reviews per subject [ <a href="http://www.free-ed.net/free-ed/">http://www.free-ed.net/free-ed/</a> ]
                              </li>
                              
                           </ul>
                           
                           
                           <h4>Career</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>
                                 <a href="http://www.militaryhire.com/">MilitaryHire.com</a> - post resume for employers, and search for jobs
                              </li>
                              
                              <li>
                                 <a href="http://www.onetonline.org/">O*NET Online</a> - search occupations, and industries
                              </li>
                              
                           </ul>
                           
                           
                           <p>Link MOS to civilian occupations; search MOS equivalent careers, and industries</p>
                           
                           <ul class="list_style_1">
                              
                              <li><a href="http://myskillsmyfuture.org">mySkills myFuture</a></li>
                              
                              <li><a href="http://mynextmove.org">My Next Move</a></li>
                              
                              <li><a href="http://www.taonline.com/">Transition Assistance Online</a></li>
                              
                           </ul>
                           
                           
                           <h4>Employment</h4>
                           
                           <ul class="list_style_1">
                              
                              
                              
                              <li>
                                 <a href="http://www.dol.gov/vets/">Department of Labor VETS</a> - employment assistance information; includes links for female veterans!
                              </li>
                              
                              <li>
                                 <a href="https://www.usajobs.gov/Veterans">USAJOBS.gov</a> - tutorials, resume builder and world-wide federal job search
                              </li>
                              
                              <li>
                                 <a href="http://www.va.gov/jobs/">VA.gov Jobs</a> - search Veterans Administration for jobs and internships
                              </li>
                              
                              <li><a href="http://vetjobs.com/">VetJobs.com</a></li>
                              
                              <li>
                                 <a href="http://www.military.com/veteran-jobs">Military.com Veteran Employment Center</a> - transition and employment tools/search (through Monster.com)
                              </li>
                              
                              <li>
                                 <a href="https://veteran.employflorida.com/vosnet/Default.aspx">Employ Florida VETS</a> - This is a cross walk program from military MOS to Civilian Jobs.
                              </li>
                              
                           </ul>
                           
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/enrollment/links.pcf">©</a>
      </div>
   </body>
</html>