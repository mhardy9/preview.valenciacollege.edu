<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Statement of Understanding  | Valencia College</title>
      <meta name="Description" content="Statement demonstrating that the student has been informed of the expectations and requirements they will be held to as a recipient of VA benefits.">
      <meta name="Keywords" content="statement, understanding, enrollment, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/enrollment/understanding.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/enrollment/">Enrollment</a></li>
               <li>Statement of Understanding </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     
                     				
                     <div class="col-md-12">
                        					
                        <h2>Statement of Understanding</h2>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>The Following Has Been Explained To Me:</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <p>Valencia College’s Veteran Affairs website( Benefit information, Certification process,
                              Things to know and Student Veterans of America)
                           </p>
                           
                           						
                           <p>My benefit  ( Ch. 30, Ch. 31, Ch. 33, Ch. 35, Ch. 1606, Ch. 1607)</p>
                           
                           						
                           <p>Residency status :</p>
                           
                           						
                           <p>If not a FL. Resident you can get a waiver if; Students who use a VA educational benefit
                              (This includes dependents)  or Veterans not using a VA educational benefit but can
                              provide the VA office a copy of their DD-214 member 4 showing an HONORABLE discharge.
                           </p>
                           
                           						
                           <p>That V.A. will only pay for In-State Tuition and not pay for late registration fee.</p>
                           
                           						
                           <p>Submit Certificate of Eligibility or a printout showing the Confirmation number of
                              form; (22-1990/22-1990E/22-1995/22-5490/22-5495) and a Copy of DD 214 If applicable
                           </p>
                           
                           						
                           <p>I AM RESPONSIBLE FOR ALL FEES ON MY ACCOUNT NOT PAID FOR BY THE V.A. before the deferment
                              deadline.
                           </p>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>I Understand The Following Certification Requirements:</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <p>Veterans/Dependents must certify ONLINE <strong>EVERY</strong> semester they choose to use their benefits. Must be enrolled in classes and have
                              schedule finalized beforehand.
                           </p>
                           
                           						
                           <p>The V.A. will only certify classes pertaining to your Primary/Secondary degree that
                              Valencia has listed in their system.
                           </p>
                           
                           						
                           <p>V.A. will not pay for Hybrid/online developmental courses.</p>
                           
                           						
                           <p>An automatic V.A. deferment will be applied once I have submitted a certification
                              request (Note: This must be done before each term Fee Payment deadline).
                           </p>
                           
                           						
                           <p>In order to keep full time status (12 CR HRS in Fall/Spring, 8 CR HRS in Summer, I
                              must maintain full time credit requirement throughout each term.
                           </p>
                           
                           						
                           <p>Maintain cumulative GPA above 2.0 and per term a completion ratio above 50% to prevent
                              suspension of benefits.
                           </p>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>I Recognize And Understand That It's My Responsibility As A:</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <p>CH 31- I must submit a 22-1905 each term and submit a certification request.</p>
                           
                           						
                           <p>CH 30, 1606, and 1607- I must verify enrollment at the end of every month. VTA Wave
                              In order to receive monthly stipend. (12 CR HRS in Fall/Spring. 8 CR HRS in Summer).
                           </p>
                           
                           						
                           <p>CH 33 – In order to receive my monthly stipend (12 CR HRS in Fall/Spring. 8 CR HRS
                              in Summer), I must be enrolled in at least ONE ONSITE credit hour through the duration
                              of all classes taken that semester. ½ BAH for Veteran solely taking Web based classes.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/enrollment/understanding.pcf">©</a>
      </div>
   </body>
</html>