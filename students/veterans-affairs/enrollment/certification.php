<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Certification  | Valencia College</title>
      <meta name="Description" content="Learn about how to submit a certification request to the VA, and the steps of the process.">
      <meta name="Keywords" content="certification, transient, enrollment, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/enrollment/certification.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/enrollment/">Enrollment</a></li>
               <li>Certification </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Certification</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>VA Certification Process</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <div><a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm" class="button-outline">Certification Request Form</a></div>
                           
                           
                           <p>FIXME: Kaltura video embed</p>
                           
                           
                           <hr class="styled_2">
                           
                           <h4>Remedial Courses Note</h4>
                           
                           <p>Beginning with the 2011 summer term, Veterans Affairs will no longer pay for remedial
                              or preparatory courses that are offered online or in a hybrid format (partially online).
                              These courses can no longer be included in your certified hours. Please make any necessary
                              adjustments to your schedule if need be prior to submission of your <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">VA certification request</a>.
                           </p>
                           
                           <hr class="styled_2">
                           
                           
                           <h4>What To Expect When You Submit a VA Certification Request</h4>
                           
                           <p>When you submit an on-line certification request you should expect an automated reply
                              with your submitted form to include answers sent to your Valencia atlas email account.
                              This email typically arrives within 5 minutes after submission.
                           </p>
                           
                           
                           <p>Within 1 to 2 business days you should get a second email in your atlas email account
                              informing you that we have received your certification and to please give us two weeks
                              to process your request. When you get this email you can now rest assured you have
                              a VA deferment/protection on your account and your courses should not get deleted
                              unless you have an outstanding balance from a previous term.
                           </p>
                           
                           
                           <p>When our office goes to certify your enrollment you will get an email stating we have
                              completed our part and now you have to wait on the VA to finish their part, or you
                              will get an email explaining why we can't move forward with your request at that time.
                              If the VA at Valencia does experience problems, please reply back to the email as
                              soon as you can to prevent any delays.
                           </p>
                           
                           
                           <p>The last email you should get comes from VA-Once, which is the system schools use
                              to certify your enrollment. This email will explain what was submitted on your behalf
                              and is typically received within 5 minutes of submission.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Certification for Transient Students</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Consult the <a href="http://www.floridashines.org/">Florida Virtual Campus</a> website for more information about taking courses as a Transient student.
                           </p>
                           
                           
                           <h4>If Valencia Is Your Home Institution</h4>
                           
                           <p>If Valencia is your home institution and you are planning on taking a course at another
                              school you will need to get a letter from Valencia's VA School Certifying Official,
                              <a href="mailto:dgibson11@valenciacollege.edu">Donald Gibson</a>. Please email him a copy of your Florida Virtual Campus approval email if you are
                              going to another Florida public school. If you are going to a Private School or a
                              school outside the state of Florida please fill out a Valencia Transient Form and
                              submit it via email to Donald Gibson. The VA office will then provide a Transient/
                              Parent School letter to both you and your secondary School's VA Certifying Official
                              via email.
                           </p>
                           
                           
                           <h4>If Valencia Is Your Secondary Institution</h4>
                           
                           <p>If Valencia is your secondary institution, please provide the VA office with a Parent
                              School Letter/Transient Form that is signed off by your home school's VA School Certifying
                              Official. They will typically ask for the Florida Virtual Campus approval or need
                              to internally approve you to take the course(s) before supplying this to you. It is
                              your responsibility to make sure that approval is turned into your VA office with
                              your certification request either in person or <a href="mailto:dgibson11@valenciacollege.edu">electronically</a>. The Valencia VA Office will notify you by Atlas email if we are missing this documentation
                              and remind you that it is needed to process your certification request.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/enrollment/certification.pcf">©</a>
      </div>
   </body>
</html>