<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Veteran-Only Courses  | Valencia College</title>
      <meta name="Description" content="Veterans Returning to college can sometimes find it to be a daunting experience, which is why fellow veteran Professor John Creighton (Captain USA) has teamed up to create four veteran specific courses.">
      <meta name="Keywords" content="veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/enrollment/veteran-only-courses.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/enrollment/">Enrollment</a></li>
               <li>Veteran-Only Courses </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     
                     				
                     <div class="col-md-12">
                        					
                        <h2>Veteran-Only Courses</h2>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="wrapper_indent">
                           						
                           <p>Veterans Returning to college can sometimes find it to be a daunting experience, which
                              is why fellow veteran Professor <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=jcreighton">John Creighton</a> (Captain USA) has teamed up to create four veteran specific courses.
                           </p>
                           						
                           <ul class="list_style_1">
                              							
                              <li>ENC1101 - Freshman Comp M/W 10:00 - 11:15am taught by <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=jvrhovac">James Vrhovac</a>
                                 							
                              </li>
                              							
                              <li>MAT1033 - Intermediate Algebra M/W 11:30 - 12:45pm taught by <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=lhoward13">Lynn Howard</a>
                                 							
                              </li>
                              							
                              <li>SLS1122 - New Student Experience M/W 2:30 - 3:45pm taught by <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=esouza">Ed Souza</a>
                                 							
                              </li>
                              							
                              <li>SPC1608 - Speech T/R 2:30 - 3:45pm taught by John Creighton</li>
                              						
                           </ul>
                           
                           						
                           <p>To register for these courses:</p>
                           						
                           <ul class="list_style_1">
                              							
                              <li>For either ENC1101 or SPC1608, contact <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=ssorrough">Sharon Sorrough</a> at 407-582-1313
                              </li>
                              							
                              <li>For MAC1033, contact <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=adoctor1">Adele Doctor</a> or <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=cbelinmortera">Christine Belin-Mortera</a> at 407-582-1625
                              </li>
                              							
                              <li>For SLS1122, contact <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=aards1">Anthony Ards</a> at 407-582-1516
                              </li>
                              						
                           </ul>
                           					
                        </div>
                        				
                     </div>
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/enrollment/veteran-only-courses.pcf">©</a>
      </div>
   </body>
</html>