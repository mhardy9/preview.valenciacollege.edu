<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Student Veterans of America  | Valencia College</title>
      <meta name="Description" content="Information about the Student Veteran Club chapters on Valencia College's campuses.">
      <meta name="Keywords" content="student, veterans, enrollment, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/enrollment/student-veterans.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li><a href="/students/veterans-affairs/enrollment/">Enrollment</a></li>
               <li>Student Veterans of America </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     
                     				
                     <div class="col-md-12">
                        					
                        <h2>Student Veterans of America</h2>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>West Campus Chapter</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <div class="row">
                              							
                              <div class="col-md-6">
                                 								
                                 <div><a href="https://docs.google.com/forms/d/1s8MuiIUeF8FnGvx87UFOlrgeJ4kg2ksfwWf5grHevIg/viewform" class="button-outline">West Student Veterans Club Sign-Up</a></div>
                                 								
                                 <p>Follow us on <a href="https://www.facebook.com/svawest">Facebook</a>!
                                 </p>
                                 
                                 								
                                 <ul class="list-unstyled">
                                    									
                                    <li><a href="https://docs.google.com/forms/d/1F0P_ho5ZO9Y5hD1mNRqLs-4uax2cLw5IwRL3cENi_RM/viewform?c=0&amp;w=1&amp;usp=mail_form_link">Request for Point Balance</a></li>
                                    									
                                    <li><a href="https://docs.google.com/forms/d/1btboZUNDdzkTVhr1aHdXMZ9A1hlLOp6NbdEbdOVat5A/viewform?c=0&amp;w=1&amp;usp=mail_form_link">Request to Redeem Points</a></li>
                                    								
                                 </ul>
                                 							
                              </div>
                              
                              							
                              <div class="col-md-6">
                                 								
                                 <ul class="list-unstyled">
                                    									
                                    <li><strong>ADVISOR:</strong></li>
                                    									
                                    <li>John Creighton (Lead Advisor)</li>
                                    									
                                    <li>&nbsp;</li>
                                    									
                                    <li><strong>OFFICERS:</strong></li>
                                    									
                                    <li>
                                       										<a href="mailto:telkhouly@mail.valenciacollege.edu">Tamer Elkhouly</a> (President)
                                    </li>
                                    									
                                    <li>
                                       										<a href="mailto:jbraddy2@mail.valenciacollege.edu">James Braddy</a> (Vice President)
                                    </li>
                                    									
                                    <li>
                                       										<a href="mailto:krodriguez187@mail.valenciacollege.edu">Kasandra Rodriguez</a> (Secretary)
                                    </li>
                                    									
                                    <li>
                                       										<a href="mailto:lharris50@mail.valenciacollege.edu">Lanecia Harris</a> (Secretary)
                                    </li>
                                    									
                                    <li>
                                       										<a href="mailto:msuydam@mail.valenciacollege.edu">Morgan Suydam</a> (Communications)
                                    </li>
                                    								
                                 </ul>
                                 							
                              </div>
                              						
                           </div>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>East Campus Chapter</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <div class="row">
                              							
                              <div class="col-md-6">
                                 								
                                 <div><a href="https://docs.google.com/forms/d/1NVghXPc9nYkkyt9VkLlsmAvoHCL_xL1MmTEsD-DhAoU/viewform?c=0&amp;w=1&amp;usp=mail_form_link" class="button-outline">East Student Veterans Club Sign-Up</a></div>
                                 								
                                 <p>Follow us on <a href="https://www.facebook.com/SVAValenciaEast?fref=ts">Facebook</a>!
                                 </p>
                                 
                                 								
                                 <p>If you are interested in joining, please reach out to a co-advisor.</p>
                                 								
                                 <p><strong>Stay tuned for the Fall Meeting Dates!</strong></p>
                                 							
                              </div>
                              
                              							
                              <div class="col-md-6">
                                 								
                                 <ul class="list-unstyled">
                                    									
                                    <li><strong>ADVISORS:</strong></li>
                                    									
                                    <li>
                                       										<a href="mailto:khenry16@valenciacollege.edu">Kim Henry</a> (Co Advisor)
                                    </li>
                                    									
                                    <li>
                                       										<a href="mailto:ldelagua@valenciacollege.edu">Fernando Del Agua</a> (Co Advisor)
                                    </li>
                                    									
                                    <li>&nbsp;</li>
                                    									
                                    <li><strong>OFFICERS:</strong></li>
                                    									
                                    <li>(President)</li>
                                    									
                                    <li>(Vice President)</li>
                                    								
                                 </ul>
                                 							
                              </div>
                              						
                           </div>
                           
                           						
                           <h4>Veteran Space</h4>
                           						
                           <p>This is a location on East Campus that can be used to study, hangout, eat lunch, hold
                              SVA meetings, etc. The location for this Veteran Space is Building 2, Room 116. If
                              you use this facility, please remember to sign in each time so we can get credit.
                              This room is being monitored to determine if we have enough Veteran traffic to justify
                              it. There are two computers there for your use as well.
                           </p>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>Osceola Campus Chapter</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <div class="row">
                              							
                              <div class="col-md-6">
                                 								
                                 <p>These officers are in the beginning stages of formally creating an Osceola Campus
                                    SVA chapter. If you are interested in assisting, please reach out to the president
                                    to find out how to assist and become an active member.
                                 </p>
                                 							
                              </div>
                              
                              							
                              <div class="col-md-6">
                                 								
                                 <ul class="list-unstyled">
                                    									
                                    <li><strong>ADVISORS:</strong></li>
                                    									
                                    <li>
                                       										<a href="mailto:jcruz95@valenciacollege.edu">Jasmin Cruz</a> (Lead Advisor)
                                    </li>
                                    									
                                    <li>&nbsp;</li>
                                    									
                                    <li><strong>OFFICERS:</strong></li>
                                    									
                                    <li>
                                       										<a href="mailto:mhernandez125@mail.valenciacollege.edu">Michael Hernandez</a> (President)
                                    </li>
                                    								
                                 </ul>
                                 							
                              </div>
                              						
                           </div>
                           					
                        </div>
                        				
                     </div>
                     
                     
                     
                     
                     
                     
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/enrollment/student-veterans.pcf">©</a>
      </div>
   </body>
</html>