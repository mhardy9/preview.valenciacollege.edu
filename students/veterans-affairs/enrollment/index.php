<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Enrollment  | Valencia College</title>
      <meta name="Description" content="Steps to enroll as a student at Valencia College and apply for available VA benefits, as well as important information about the academic standards and code of conduct which students receiving veteran's benefits will be held to.">
      <meta name="Keywords" content="enrollment, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/enrollment/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/veteran-affairs/enrollment/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li>Enrollment</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        
                        <h2>Enrollment</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Steps to Enroll</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           
                           
                           <ol>
                              
                              <li>Apply for <a href="https://valenciacollege.edu/admissions-records/admission-details/">Admission</a> to Valencia College.
                              </li>
                              
                              <li>Apply to the Department of Veterans Affairs for your <a href="http://vabenefits.vba.va.gov/vonapp/main.asp">Educational Benefits</a>.
                              </li>
                              
                              <li>Apply for <a href="http://www.fafsa.ed.gov/">Financial Aid</a> (School Code 006750).
                              </li>
                              
                              <li>Create your <a href="https://atlas.valenciacollege.edu/">Atlas Account</a>.
                              </li>
                              
                              <li>Apply for <a href="https://valenciacollege.edu/admissions-records/florida-residency/">Florida Residency for Tuition Purposes</a>.
                              </li>
                              
                              <li>Take your <a href="https://valenciacollege.edu/testing-center/">Entry Testing</a> (required by all students - may include a placement test based on FL 1720 law) .
                              </li>
                              
                              <li>Check out Valencia <a href="https://valencia.org/scholarships/">scholarships</a>.
                              </li>
                              
                              <li>Turn in Official Transcripts (including <a href="https://jst.doded.mil/smart/dodMandatoryBannerForm/submit.do">Military</a>) to the Answer Center in person or mail directly to:
                                 
                                 <div class="wrapper_indent">
                                    
                                    <address class="nomargin">
                                       Valencia College<br>
                                       Admissions &amp; Records<br>
                                       P.O. Box 3028<br>
                                       Orlando FL 32802-3028
                                       
                                    </address>
                                    
                                 </div>
                                 
                              </li>
                              
                              
                              <li>Sign up for a <a href="https://valenciacollege.edu/orientation/">Student Orientation session</a> via your Atlas account.
                              </li>
                              
                              <li>Register for degree seeking classes.</li>
                              
                              <li>Collect the <a href="/students/veteran-affairs/financial/va-education/">Required Documents</a> according to your chapter of benefits and student Category (First Time using benefits,
                                 Transfer, Transient)
                              </li>
                              
                              <li>Visit a Valencia Veteran Services Office to:
                                 
                                 <ol>
                                    
                                    <li><a href="/students/veteran-affairs/enrollment/certification.html">Certify Enrollment</a></li>
                                    
                                    <li>Request a VA Tuition Deferment</li>
                                    
                                 </ol>
                                 
                              </li>
                              
                              <li>Get your student ID at <a href="https://valenciacollege.edu/studentdev/locationsInformationServices/centers.cfm">Student Development</a> or Security office depending on your campus.
                              </li>
                              
                              <li>Get your parking decal at <a href="https://valenciacollege.edu/security/">Security</a>.
                              </li>
                              
                              <li>Get your <a href="https://valenciacollege.edu/locations-store/">books</a> and go to class.
                              </li>
                              
                           </ol>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Things to Know</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>Satisfactory Academic Standards</h4>
                           
                           <p>All students receiving veteran’s benefits must maintain the following academic standards:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Cumulative GPA of 2.0 and</li>
                              
                              <li>Complete at least 50% of all coursework taken in a term</li>
                              
                           </ul>
                           
                           <p>If you fail to meet these requirements two semesters back to back your benefits will
                              be terminated. To reinstate your benefits, you must bring your GPA and completion
                              ratio to the required academic standards. If you need to appeal this decision you
                              may turn in your written appeal to Valencia College Veterans Affairs office. Once
                              it is reviewed by Valencia’s Certifying Official they will contact you via email to
                              inform you of the decision.
                           </p>
                           
                           
                           <p>Please keep in mind Valencia and Financial Aid have different and separate standards
                              you may also need to comply with.
                           </p>
                           
                           
                           <h4>Code of Conduct</h4>
                           
                           <p>The Code is a definition of behavioral standards that a student has agreed to follow
                              and is accountable to follow while attending Valencia College. These standards are
                              defined in the student handbook under <a href="https://valenciacollege.edu/generalcounsel/policy/documents/volume8/8-03-Student-Code-of-Conduct.pdf">POLICY: 6Hx28:8-03</a>.
                           </p>
                           
                           
                           <h4>Deferment</h4>
                           
                           <p>All students who are using a VA educational benefit by submitting a VA certification
                              request will automatically receive a deferment/ protection so that the courses do
                              not get dropped unless you have a previous outstanding balance. There is a deferment
                              deadline which is found under important dates in the catalog. Please ensure your balance
                              is at $0.00 by this deadline.
                           </p>
                           
                           
                           <h4>Transferring Hours</h4>
                           
                           <p>Have all official transcripts sent to the Admissions Office immediately to be evaluated.
                              The Valencia College Veterans Affairs office can only certify a student’s enrollment
                              for two terms without military transcripts and official college transcripts being
                              received.
                           </p>
                           
                           
                           <h4>Tutorial Assistance</h4>
                           
                           <p>If you are having academic difficulty in a course, Valencia College provides free
                              tutorial assistance. The Department of Veterans Affairs can also provide tutorial
                              assistance for up to $100.00 per month as a reimbursement. If you have questions about
                              this service please contact the Valencia College Veterans Affairs Office.
                           </p>
                           
                           
                           <h4>Withdrawal</h4>
                           
                           <p>When a student withdraws from course(s) the VA automatically forgives up to six credits
                              of non-punitive grades. This is a one-time exception of up to six credits. If you
                              withdraw from multiple courses totaling 6 credits they all must be done on the same
                              day. When forgiven this means that no moneys will have to be paid back to the VA until
                              date of withdrawal. After you use your one-time exception you will receive a debt
                              letter from the VA. The instructions for contacting their office will be in the letter
                              from Debt Management.
                           </p>
                           
                           
                           <h4>Drop vs Withdraw</h4>
                           
                           <p>When you drop a course this means you have no financial obligations to pay for the
                              course and it will not show up on your transcripts. The window to drop a course is
                              a very small time frame (normally the first week of the term). A dropped course does
                              not get counted into attempted hours for VA or Financial Aid benefits.
                           </p>
                           
                           <p>A withdraw is when you remove a course after the drop window has ended. When this
                              happens money is still owed for the course and you will have a "W" on your transcripts
                              showing you attempted the course. This does get calculated into attempted hours for
                              VA and Financial Aid benefits.
                           </p>
                           
                           
                           <h4>Failing Grades</h4>
                           
                           <p>There are two types of failing grades:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>The student earns a "F" by completing the course and taking the Final Exam. The VA
                                 will not ask for money back and will pay for it to be retaken.
                              </li>
                              
                              <li>The student receives a "F" that is not earned due to not taking the Final Exam. If
                                 the grade was not earned, then it is treated like a "W" withdraw and money will be
                                 asked back by the VA from the last date of attendance. The VA will pay for the class
                                 to be retaken for these "F" as well.
                              </li>
                              
                           </ul>
                           
                           <p>If you have questions regarding this process please contact Valencia College Veterans
                              Affairs Office.
                           </p>
                           
                           
                           <h4>Incomplete Grades</h4>
                           
                           <p>If you receive an incomplete (I) grade, you have one full semester to earn a grade
                              without penalty. If the grade is not earned within the one semester, it will be converted
                              to a "F". This can only be approved or denied by the professor of the specific course
                              you are concerned with.
                           </p>
                           
                           
                           <h4>Third Attempt</h4>
                           
                           <p>When you register and attempt a course for the third time you will automatically get
                              charged the full cost of attendence. At this time only one VA educational benefit
                              will pay the full cost of this which is Vocational Rehabilitaion. Chapter 33 (Post
                              9/11) will only pay the in state portion and the outstanding balance responsibility
                              falls onto you the student. Valencia does have a waiver to attempt to reduce the cost
                              back down to the instate rate for those who can not afford to pay the full cost. This
                              form gets submitted to the answer center.
                           </p>
                           
                           
                           <h4>1720 State Law</h4>
                           
                           <p>The 1720 state law says that any student who started in a Florida public high school
                              in 2003 or later and then also graduated from a Florida public high school in 2007
                              or after do not need to take the placement exam (PERT).
                           </p>
                           
                           <p>The 1720 state law says that any student on active duty when they apply to Valencia
                              College does not need to take the placement exam (PERT).
                           </p>
                           
                           <p>If you are eligible for this exemption it will automatically get placed on your account.
                              What this means for you as a student using VA benefits is that the VA <strong>will NOT</strong> pay for remedial courses even if you choose to still take the placement exam and
                              it is determined that you should register for remedial courses. If you want to register
                              for remedial courses please note the financial responsibility will fall on you the
                              student.
                           </p>
                           
                           
                           <h4>Remedial Courses</h4>
                           
                           <p>If you do not qualify for the 1720 state law mentioned above please remember that
                              if remedial courses are required for you to graduate that the VA will only pay for
                              on-site courses. This means they cannot be hybrid which is a combination of on-site
                              and online, nor can they be strictly online. If you register for either a hybrid or
                              online course they will automatically be removed from your certification (22-1999)
                              that is submitted to the Department of Veterans Affairs.
                           </p>
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/enrollment/index.pcf">©</a>
      </div>
   </body>
</html>