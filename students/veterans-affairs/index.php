<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Office of Veteran Affairs  | Valencia College</title>
      <meta name="Description" content="Valencia College Veteran Services provides essential resources and support for our Veterans, Active-Duty, National Guard and Reserve military personnel, their spouses and dependents. We vow to assist those whose lives are connected to the military, as they transition and learn to navigate through the college experience.">
      <meta name="Keywords" content="veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Veteran Affairs</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  		
                  <div class="row">
                     
                     
                     <div class="col-md-12">
                        		  
                        
                        <h2>Office of Veteran Affairs</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="wrapper_indent">
                           
                           <p>Valencia College Veteran Services provides essential resources and support for our
                              Veterans, Active-Duty, National Guard and Reserve military personnel, their spouses
                              and dependents. We vow to assist those whose lives are connected to the military,
                              as they transition and learn to navigate through the college experience.
                           </p>
                           
                           
                           <p>The laws dealing with the different veteran benefit programs are complex and change
                              quite often. Veteran Services can reduce the chance of hardship by using our resources;
                              contact a Valencia College Veterans Services Representative for more details.
                           </p>
                           
                           
                           <p>We have created a Facebook page where the Veterans Office will post school activities,
                              scholarships, details about processing time, reminders like early registration, etc.
                              If you are interested in getting this information please feel free to follow our page.
                           </p>
                           
                           
                           <p>FIXME: insert Kaltura video here </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Contact Us</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>For general assistance, call 407-582-8387 (VETS) or <a href="mailto:veterans@valenciacollege.edu">e-mail veterans@valenciacollege.edu</a>.
                           </p>
                           
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <h4>Veteran Services, West Campus</h4>
                                 
                                 <p>Location: SSB-203</p>
                                 
                                 <p>Phone: 407-582-8387</p>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=dgibson11">Donald Gibson</a> - Main Certifying Official
                                    </li>
                                    
                                    <li>Jennifer Papoula - A. A. Academic Counselor</li>
                                    
                                    <li>VA Representatives</li>
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=jsininger">Jason Sininger</a> - Coordinator of Conduct/Academic Success
                                    </li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              
                              <div class="col-md-6">
                                 
                                 <h4>Veteran Services, East Campus</h4>
                                 
                                 <p>Location: 5-210</p>
                                 
                                 <p>Phone: 407-582-2049</p>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=ksuarez">Kathy Suarez</a> -  Assistant Director of Financial Aid
                                    </li>
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=khenry16">Kim Henry</a> - Certifying Official
                                    </li>
                                    
                                    <li>Susan Russo - A. A. Academic Advisor</li>
                                    
                                    <li>VA Representatives</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <h4>Veteran Services, Osceola Campus</h4>
                                 
                                 <p>Location: 2-140</p>
                                 
                                 <p>Phone: 321-682-4392</p>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=icordero2">Ilia Cordero</a> -  Assistant Director of Financial Aid
                                    </li>
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=inunes">Ishanna Nunes</a> - Point Of Contact
                                    </li>
                                    
                                    <li>Wayne Bart - A. A. Academic Advisor</li>
                                    
                                    <li>VA Representatives</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                              
                              <div class="col-md-6">
                                 
                                 <h4>Veteran Services, Winter Park Campus</h4>
                                 
                                 <p>Location: Main Building, 208</p>
                                 
                                 <p>Phone: 407-582-6892</p>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>
                                       <a href="https://valenciacollege.edu/contact/Search_Detail2.cfm?ID=abrocious">Ashley Brocious</a> - Point of Contact
                                    </li>
                                    
                                    <li>Linda Firmani - A. A. Academic Advisor</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                           <div class="row">
                              
                              <div class="col-md-6">
                                 
                                 <h4>Veteran Services, Lake Nona Campus</h4>
                                 
                                 <ul class="list-unstyled">
                                    
                                    <li>Melinda Smith - A. A. Academic Advisor</li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/index.pcf">©</a>
      </div>
   </body>
</html>