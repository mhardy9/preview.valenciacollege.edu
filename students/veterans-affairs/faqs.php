<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="Frequently asked questions about becoming a Valencia College student, applying for Veterans Affairs Educational Benefits, classes which qualify for benefits, etc.">
      <meta name="Keywords" content="frequently, asked, questions, faq, veteran, affairs, va, military, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/veterans-affairs/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/veterans-affairs/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/veterans-affairs/">Veteran Affairs</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-60" role="main">
                  			
                  <div class="row">
                     
                     				
                     <div class="col-md-12">
                        					
                        <h2>Frequently Asked Questions</h2>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>Application Process</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <h4>How do I become a Valencia student?</h4>
                           						
                           <p>There are six steps to becoming a Valencia student:</p>
                           						
                           <ul class="list_style_1">
                              							
                              <li>Apply for admission and financial aid.</li>
                              							
                              <li>Create your Atlas account.</li>
                              							
                              <li>Take entry testing and attend new student orientation.</li>
                              							
                              <li>Register for your courses and pay tuition.</li>
                              							
                              <li>Get your student I.D. and parking decal.</li>
                              							
                              <li>Buy your books and go to class.</li>
                              						
                           </ul>
                           						
                           <p>For more information, visit the <a href="https://valenciacollege.edu/futureStudents/admissions/">Valencia College Admissions</a> page.
                           </p>
                           
                           						
                           <h4>How do I apply for my Veterans Affairs Educational Benefits?</h4>
                           						
                           <p>Your chapter will determine which forms you will need to complete through <a href="https://www.ebenefits.va.gov/ebenefits-portal/ebenefits.portal?_nfpb=true&amp;_portlet.async=false&amp;_pageLabel=ebenefits_myeb_vonapp1">VONAPP</a>.
                           </p>
                           						
                           <p>Complete VA Form 22-1990 form to apply for educational assistance under the following
                              benefit programs:
                           </p>
                           						
                           <ul class="list_style_1">
                              
                              							
                              <li>Chapter 33, Post-9/11 GI Bill or</li>
                              							
                              <li>Chapter 30, Montgomery GI Bill (MGIB) or</li>
                              							
                              <li>Chapter 1606, Montgomery GI Bill - Selected Reserve (MGIB-SR) or</li>
                              							
                              <li>Chapter 1607, Reserve Educational Assistance Program (REAP) or</li>
                              							
                              <li>Chapter 32, Post-Vietnam Era Veterans’ Educational Assistance Program (VEAP)</li>
                              							
                              <li>Chapter 33 TOE, Complete VA Form 22-1990E to apply for educational assistance under
                                 the Transfer of Entitlement (TOE) program.
                              </li>
                              							
                              <li>Chapter 35, Complete VA Form 22-5490 to apply for Dependent Educational Assistance
                                 (DEA).
                              </li>
                              							
                              <li>Chapter 31, Complete VA Form 28-1900 to apply for educational assistance under Vocational
                                 Rehabilitation (VET Success).
                              </li>
                              						
                           </ul>
                           
                           						
                           <h4>How do I transfer my benefits to Valencia College?</h4>
                           						
                           <ul class="list_style_1">
                              							
                              <li>If you are a Veteran or Service Member, complete VA Form 22-1995 through VONAPP</li>
                              							
                              <li>If you are a Dependent of a Veteran or Service Member, complete VA Form 22-5495, through
                                 VONAPP
                              </li>
                              						
                           </ul>
                           
                           
                           						
                           <h4>Can I apply for Financial Aid?</h4>
                           						
                           <p>Yes. You can use VA Education Benefits and apply for Federal Student Aid. To apply,
                              fill out the <a href="http://www.fafsa.ed.gov/">Free Application for Federal Student Aid</a>.
                           </p>
                           
                           						
                           <h4>How do I change my address with the VA?</h4>
                           						
                           <p>You must notify all of the following offices of your change in address:</p>
                           
                           						
                           <ul class="list_style_1">
                              							
                              <li>Valencia College — you can update your address in your Atlas Account. <a href="http://valenciacollege.edu/support/howto/">Instructions are available</a>.
                              </li>
                              							
                              <li>The Department of Veterans Affairs: Office in Muskogee — Call 1-888-442-4551 or e-mail
                                 <a href="mailto:atlrpo@vba.va.gov">atlrpo@vba.va.gov</a>
                                 							
                              </li>
                              						
                           </ul>
                           
                           						
                           <h4>What about my military school credits?</h4>
                           						
                           <p>Veterans should submit all copies of transcripts from military experience for credit
                              evaluation along with any other official college transcripts.
                           </p>
                           
                           						
                           <h4>How may I contact the U.S. Department of Veterans Affairs?</h4>
                           						
                           <ul class="list_style_1">
                              							
                              <li>Education Benefits: 1-888-442-4551</li>
                              							
                              <li>VA Regional Office: 1-800-827-1000, Fax 404-929-3009</li>
                              							
                              <li>VA Debt Management Center: 1-800-827-0648</li>
                              							
                              <li><a href="mailto:atlrpo@vba.va.gov">E-mail</a></li>
                              							
                              <li><a href="http://www.gibill.va.gov">VA Web Site</a></li>
                              						
                           </ul>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>Payments</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <h4>Do I have enough credit hours to qualify for your monthly payments during the summer
                              term?
                           </h4>
                           						
                           <p>Chapter 33 students must be enrolled more than 1/2 time to receive their BAH. Please
                              refer to chart below.
                           </p>
                           
                           
                           						
                           <table class="table table">
                              							
                              <thead>
                                 								
                                 <tr>
                                    									
                                    <th>Enrollment (Credit Hours)</th>
                                    									
                                    <th>(1)<br>12 Weeks of Class
                                    </th>
                                    									
                                    <th>(TWJ or TWK)<br>8 Weeks of Class
                                    </th>
                                    									
                                    <th>(H1 or A/H2 or B)<br>6 Weeks of Class
                                    </th>
                                    									
                                    <th>(TR1 or TR2 or TR3)<br>4 Weeks of Class
                                    </th>
                                    								
                                 </tr>
                                 							
                              </thead>
                              							
                              <tbody>
                                 								
                                 <tr>
                                    									
                                    <td>Full Time</td>
                                    									
                                    <td>8</td>
                                    									
                                    <td>6</td>
                                    									
                                    <td>4</td>
                                    									
                                    <td>3</td>
                                    								
                                 </tr>
                                 								
                                 <tr>
                                    									
                                    <td>3/4 Time</td>
                                    									
                                    <td>7-6</td>
                                    									
                                    <td>5-4</td>
                                    									
                                    <td>3</td>
                                    									
                                    <td>2</td>
                                    								
                                 </tr>
                                 								
                                 <tr>
                                    									
                                    <td>1/2 Time</td>
                                    									
                                    <td>5-4</td>
                                    									
                                    <td>3</td>
                                    									
                                    <td>2</td>
                                    									
                                    <td>n/a</td>
                                    								
                                 </tr>
                                 								
                                 <tr>
                                    									
                                    <td>Between 1/4th Time and 1/2 Time</td>
                                    									
                                    <td>3</td>
                                    									
                                    <td>2</td>
                                    									
                                    <td>n/a</td>
                                    									
                                    <td>1</td>
                                    								
                                 </tr>
                                 								
                                 <tr>
                                    									
                                    <td>1/4 Time or Less</td>
                                    									
                                    <td>1-2</td>
                                    									
                                    <td>1</td>
                                    									
                                    <td>1</td>
                                    									
                                    <td>n/a</td>
                                    								
                                 </tr>
                                 							
                              </tbody>
                              						
                           </table>
                           
                           						
                           <h4>Do I qualify for a Tuition Deferment?</h4>
                           						
                           <p>When you request certification of your enrollment with the Valencia Veteran Services
                              Office you will automatically be put in for a Financial Aid protection. This protection
                              can not be seen by the business office. You will know it is there when you receive
                              your confirmation email stating we have received your certification request. Once
                              this email has been received you may disregard any emails from the business office
                              reminding you of a balance. It is your responsibility to ensure your balance is at
                              $0.00 by the VA deferment deadline. Please note this protection is only trumped by
                              one fact which is if you have a balance from a previous term we can not award the
                              protection and you will be dropped from classes.
                           </p>
                           
                           						
                           <h4>Why has my Tuition NOT been deferred?</h4>
                           						
                           <p>If you have not received the email stating your certification request has been received
                              then something may have happened to prevent successful arrival of your form and you
                              may need to re-submit the form and check on its arrival via phone or email by contacting
                              Veteran Services at Valencia College.
                           </p>
                           						
                           <p>If you have not yet completed the certification process, please see a VA Representative
                              at Valencia College.
                           </p>
                           
                           						
                           <h4>Should I wait until my tuition and fees are posted to certify my hours?</h4>
                           						
                           <p>No. You can submit your paperwork as soon as your schedule is finalized. When tuition
                              and fees are posted, we will process all certifications in the order that they were
                              received.
                           </p>
                           
                           						
                           <h4>How much money will I get?</h4>
                           						
                           <p>To calculate your monthly entitlement please refer to the Benefits Summary for your
                              Chapter of Benefits on the <a href="financial/va-education/">Educational Benefits page</a>.
                           </p> 
                           
                           						
                           <h4>How do I get my money?</h4>
                           						
                           <p>After you have been certified by the Valencia College Certifying Official and you
                              have received a Certificate of Eligibility for your benefits the Department of Veterans
                              Affairs will:
                           </p>
                           
                           						
                           <ul class="list_style_1">
                              							
                              <li>Send you a check or deposit directly into your bank account after each month of school
                                 completed.
                              </li>
                              							
                              <li>If you are a Chapter 30, 1606, 1607 or VRAP recipient you must verify your enrollment
                                 with the Department of Veterans Affairs the first of every month you are enrolled
                                 in order for your payment to be released.
                              </li>
                              							
                              <li>Contact the Department of Veterans Affairs via telephone 1-877-823-2378 or by web
                                 through <a href="https://www.gibill.va.gov/wave/index.do">WAVE</a>.
                              </li>
                              						
                           </ul>
                           
                           						
                           <h4>How long does it take to get paid?</h4>
                           						
                           <p>Once Valencia College submits your enrollment certification, it must be processed
                              by the Department of Veterans Affairs. VA is generally overwhelmed with certifications
                              at the start of each semester; therefore it could take up to 6 weeks to process an
                              enrollment during those times. Your patience is appreciated. The Department of Veterans
                              Affairs will process all of the enrollments in order of date received.
                           </p>
                           
                           						
                           <h4>Why have I not been paid?</h4>
                           						
                           <p>There are many reasons why you may not have received your benefits:</p>
                           
                           						
                           <ul class="list_style_1">
                              							
                              <li>You may not have applied for your benefits with Veterans Affairs. If you have not
                                 applied yet, you can <a href="http://vabenefits.vba.va.gov/vonapp/main.asp">apply at the VA website</a>.
                              </li>
                              							
                              <li>You may not have certified with Valencia’s Veteran Services Office. If this is the
                                 case, you will need to visit our <a href="enrollment/certification.html">Certification page</a> and submit the proper paperwork.
                              </li>
                              							
                              <li>You may have recently certified with Valencia’s Veteran Services Office. If your paperwork
                                 was submitted recently, please allow two weeks for the Valencia’s VSO to process your
                                 certification. After we submit your certification to Veterans Affairs (VA), it may
                                 take the Department of Veterans Affairs 6-8 weeks to process your paperwork.
                              </li>
                              							
                              <li>Have you recently changed your direct deposit information or mailing address? If you
                                 have, it will take some time for Veterans Affairs to process the change. If you receive
                                 your benefits by check, the check may have been sent to your old address. If you receive
                                 your benefits by direct deposit, the Department of Veterans Affairs may have tried
                                 to make a deposit in your previous account.
                              </li>
                              						
                           </ul>
                           
                           						
                           <h4>How do I change my direct deposit information?</h4>
                           						
                           <p>To update, change, or add direct deposit information call: 1-877-838-2778 or log in
                              to <a href="https://www.gibill.va.gov/wave/">WAVE</a>. Note: it can take up to 30 days for your direct deposit changes to become effective.
                           </p>
                           
                           						
                           <h4>How do I receive benefits via direct deposit?</h4>
                           						
                           <p>All Chapters of benefits may use direct deposit. Direct deposit authorization can
                              be initiated at time of application, by calling 1-877-838-2778 or logging in to <a href="https://www.gibill.va.gov/wave/">WAVE</a>.
                           </p>
                           
                           						
                           <h4>Who handles tuition assistance?</h4>
                           						
                           <p>Tuition assistance is through your unit and the Valencia College Business Office.</p>
                           
                           						
                           <h4>Does the VA pay for my tuition?</h4>
                           						
                           <p>For VA Chapters 30, 1606, 1607, 35, and VRAP the answer is no. It is the student's
                              responsibility to pay their tuition to the Business Office by the tuition deadline
                              stated either in the catalog or VA deferment. The VA pays <strong>you</strong> directly on a monthly basis.
                           </p>
                           						
                           <p>For Chapter 33, Post 9/11, the answer is yes, but only for Florida Resident tuition
                              and fee amounts. Effective 8-1-11, they will no longer pay for Out-of-State tuition
                              and fees.
                           </p>
                           						
                           <p>For Chapter 31, VA Vocational Rehabilitation, the answer is yes.</p>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>Registering for Courses</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <h4>Do all classes count for VA benefits?</h4>
                           						
                           <p>No, refer to the Enrollment Checklist for your Chapter of Benefits on the <a href="financial/va-education/">Educational Benefits page</a>.
                           </p>
                           						
                           <ul class="list_style_1">
                              							
                              <li>The VA will only pay for courses that are required for your degree program here at
                                 Valencia College. If you are registered for courses that are considered prerequisites
                                 for another college, the VA will pay for them if you submit a letter from that college
                                 listing what the required courses are.
                              </li>
                              							
                              <li>The VA will not pay for remedial (developmental courses) offered online or hybrid.</li>
                              							
                              <li>Audit courses will not be paid for by VA.</li>
                              						
                           </ul>
                           						
                           <p>Please speak with a Valencia VA representative for more details.</p>
                           
                           						
                           <h4>What do I need to do if I am taking courses at another school?</h4>
                           						
                           <p>You will need to make sure that the classes you are taking at the secondary school
                              will apply to the degree you’re pursuing at Valencia College. 
                           </p>
                           						
                           <p>If attending a Florida School:</p>
                           						
                           <ul class="list_style_1">
                              							
                              <li>Complete the online <a href="https://www.floridashines.org/apply">Transient Student Form</a>.
                              </li>
                              							
                              <li>Submit an enrollment certification request to the VA office at Valencia College.</li>
                              							
                              <li>Contact the Certifying Official at the secondary school where you are taking your
                                 transient classes in order for them to submit that enrollment to the Department of
                                 Veterans Affairs. Bring transient paperwork to the secondary schools certifying official.
                              </li>
                              							
                              <li>At the end of the semester, you will need to submit your official transcripts from
                                 the secondary school to Valencia to be able to receive veteran benefits the following
                                 semester.
                              </li>
                              						
                           </ul>
                           
                           						
                           <p>If attending an Out-of-State School:</p>
                           
                           						
                           <ul class="list_style_1">
                              							
                              <li>You will need to contact a VA Representative at Valencia College for more details.</li>
                              							
                              <li>Contact the Certifying Official at the secondary school where you are taking your
                                 transient classes in order for them to submit that enrollment to the Department of
                                 Veterans Affairs. Bring transient paperwork to the secondary schools certifying official.
                                 &amp;gt;
                                 							
                              </li>
                              							
                              <li>At the end of the semester, you will need to submit your official transcripts from
                                 the secondary school to Valencia to be able to receive veteran benefits the following
                                 semester.
                              </li>
                              						
                           </ul>
                           
                           						
                           <h4>Will the VA pay benefits for remedial (developmental) or deficiency courses?</h4>
                           						
                           <p>Yes, the VA will pay for all mandated remedial or deficiency courses in a classroom/on-site
                              situation only. However, the VA will NOT pay benefits for remedial or deficiency courses
                              in the online and/or hybrid delivery method.
                           </p>
                           
                           						
                           <h4>Can I drop a class?</h4>
                           						
                           <p>Yes. Before or during the drop/add period of registration, you may add or drop classes.</p>
                           
                           						
                           <h4>Can I withdraw from a class?</h4>
                           						
                           <p>Yes. If you withdraw from a class you do not need to contact Valencia College’s Veteran
                              Services Office unless you want to know how it will affect your VA payments.
                           </p>
                           
                           						
                           <h4>How does the VA pay benefits for parts of term?</h4>
                           						
                           <p>The VA pays benefits based on the term dates. Payment begins on the first day of your
                              class and ends on the official last day of your classes. Example: If you are enrolled
                              in both full term and part of term classes your monthly allowance may be affected.
                           </p>
                           						
                           <p>Check with the VA office on your campus if you have any questions regarding your monthly
                              allowance.
                           </p>
                           
                           						
                           <h4>Does the VA pay benefits for breaks between terms?</h4>
                           						
                           <p>Effective August 1, 2011 the VA will not pay benefits for breaks between semesters.
                              Example: Christmas Break.
                           </p>
                           					
                        </div>
                        					
                        <hr class="styled_2">
                        
                        					
                        <div class="indent_title_in">
                           						
                           <h3>Certifying Enrollment</h3>
                           					
                        </div>
                        					
                        <div class="wrapper_indent">
                           						
                           <h4>How often do I need to submit a Certification Request with Valencia Veteran Services
                              Office?
                           </h4>
                           						
                           <p>You need to <a href="http://net4.valenciacollege.edu/forms/veterans-affairs/certification/request-form.cfm">submit a Certification Request</a> every semester you want to receive your VA Benefits.
                           </p>
                           
                           						
                           <h4>When should I turn in my certification?</h4>
                           						
                           <p>Once you have finalized your enrollment schedule.</p>
                           
                           						
                           <h4>What VA Chapter of benefits do I qualify for?</h4>
                           						
                           <p>Please refer to the Benefits Summary for a brief description on eligibility requirements
                              for each Chapter of Benefits on the <a href="financial/va-education/">Educational Benefits page</a>.
                           </p>
                           
                           						
                           <h4>What do I need to bring with me when I come in?</h4>
                           						
                           <p>Please refer to the Required Documents for a brief description on required VA documents
                              for each Chapter of Benefits on the <a href="financial/va-education/">Educational Benefits page</a>.
                           </p>
                           
                           						
                           <h4>How do I know how many months of entitlement I have left?</h4>
                           						
                           <p>The VA Office at Valencia does not have this information. You will need to contact
                              the Department of Veterans Affairs Regional Office at 1-888-442-4551.
                           </p>
                           					
                        </div>
                        				
                     </div>
                     
                     			
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/veterans-affairs/faqs.pcf">©</a>
      </div>
   </body>
</html>