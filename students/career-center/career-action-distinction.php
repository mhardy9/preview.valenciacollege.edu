<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Action Distinction  | Valencia College</title>
      <meta name="Description" content="Like many decisions you've made before, career selection is a defining moment. This distinction offers a variety of activities and resources that support career exploration and employment readiness &amp;mdash; getting and keeping a job. Like many decisions you've made before, career selection is a defining moment. This distinction offers a variety of activities and resources that support career exploration and employment readiness &amp;mdash; getting and keeping a job.">
      <meta name="Keywords" content="action, disctinction, career, center, college, school, educational action, disctinction, career, center, college, school, educational">
      <meta name="Author" content="Valencia College Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/career-action-distinction.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Career Action Distinction </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>Career Action Distinction</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Purpose</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Like many decisions you've made before, career selection is a defining moment. This
                           distinction offers a
                           variety of activities and resources that support career exploration and employment
                           readiness —
                           getting and keeping a job.
                        </p>
                        
                        
                        <p>Through this experience, you will:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>Increase self-awareness</li>
                           
                           <li>Explore and evaluate career and transfer school options</li>
                           
                           <li>Gain an awareness of what it means to be a "professional" in the workplace</li>
                           
                           <li>Learn how to market yourself to employers, and make yourself more competitive in your
                              job search
                           </li>
                           
                        </ul>
                        
                        &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                        
                        <p>You'll receive:</p>
                        
                        <ul class="list_style_1">
                           
                           <li>A cord at graduation</li>
                           
                           <li>A recognition of completion</li>
                           
                           <li>A co-curricular verification letter of activities completed</li>
                           
                           <li>An enhancement to your resume and transfer college application</li>
                           =======
                           
                           
                           
                           
                           
                           
                           <meta charset="utf-8">
                           
                           <meta http-equiv="X-UA-Compatible" content="IE=edge">
                           
                           <meta name="viewport" content="width=device-width, initial-scale=1">
                           
                           
                           <meta name="keywords" content="action, disctinction, career, center, college, school, educational">
                           
                           <meta name="description" content="Like many decisions you've made before, career selection is a defining moment. This distinction offers a variety of activities and resources that support career exploration and employment readiness — getting and keeping a job.">
                           
                           <meta name="author" content="Valencia College">
                           
                           <title>Career Action Distinction | Career Center | Valencia College</title>
                           
                           
                           
                           <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">
                           
                           <link rel="apple-touch-icon" type="image/x-icon" href="/img/apple-touch-icon-57x57-precomposed.png">
                           
                           <link rel="apple-touch-icon" type="image/x-icon" sizes="72x72" href="/img/apple-touch-icon-72x72-precomposed.png">
                           
                           <link rel="apple-touch-icon" type="image/x-icon" sizes="114x114" href="/img/apple-touch-icon-114x114-precomposed.png">
                           
                           <link rel="apple-touch-icon" type="image/x-icon" sizes="144x144" href="/img/apple-touch-icon-144x144-precomposed.png">
                           
                           
                           
                           <link href="/_resources/css/base.css" rel="stylesheet">
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div id="preloader">
                              
                              <div class="pulse"></div>
                              
                           </div>
                           <a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
                           
                           
                           <div class="header-alert" id="valencia-alert"></div>
                           
                           
                           <div class="header-chrome desktop-only">
                              
                              <div class="container">
                                 
                                 <nav>
                                    
                                    <div class="row">
                                       
                                       <div class="chromeNav col-lg-9 col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Main Navigation">
                                          
                                          <ul>
                                             
                                             <li class="submenu">
                                                <a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                                
                                                <ul>
                                                   
                                                   <li><a href="/academics/current-students/index.html">Current Students</a></li>
                                                   
                                                   <li><a href="https://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                                   
                                                   <li><a href="https://international.valenciacollege.edu">International Students</a></li>
                                                   
                                                   <li><a href="/academics/military-veterans/index.html">Military &amp;
                                                         Veterans</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li><a href="/academics/continuing-education/index.html">Continuing Education</a></li>
                                             
                                             <li><a href="/EMPLOYEES/faculty-staff.html">Faculty &amp; Staff</a></li>
                                             
                                             <li><a href="/FOUNDATION/alumni/index.html">Alumni &amp; Foundation</a></li>
                                             &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; fea88b4764015604ad30f5e4d5de1ce47c588a31
                                             
                                          </ul>
                                          
                                          
                                          <p><strong>Plus</strong>, more benefits to gain a competitive edge:
                                          </p>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Connect your college experience to your career</li>
                                             
                                             <li>Practice job search and networking skills</li>
                                             
                                             <li>Learn what it means to be a professional</li>
                                             
                                             <li>Maximize your research skills as you prepare for transfer or the workforce</li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <hr class="styled_2">
                                       
                                       
                                       <div class="indent_title_in">
                                          
                                          <h3>Enrollment</h3>
                                          
                                       </div>
                                       
                                       <div class="wrapper_indent">
                                          
                                          <p>Enrollment is open to any degree-seeking student at Valencia during the start of both
                                             Fall and Spring
                                             semesters. <strong>We suggest that you have completed 12-15 credit hours before beginning.</strong></p>
                                          
                                          
                                          <p>It's easy to enroll! <strong>Using your Atlas e-mail,</strong> send your name and your VID number to your
                                             campus's Career Center.
                                          </p>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>East/Winter Park Campuses: <a href="mailto:careercenter-east@valenciacollege.edu">careercenter-east@valenciacollege.edu</a>
                                                
                                             </li>
                                             
                                             <li>Osceola Campus: <a href="mailto:osceolacareercenter@valenciacollege.edu">osceolacareercenter@valenciacollege.edu</a>
                                                
                                             </li>
                                             
                                             <li>West Campus: <a href="mailto:careercenter-west@valenciacollege.edu">careercenter-west@valenciacollege.edu</a>
                                                
                                             </li>
                                             
                                          </ul>
                                          
                                          
                                          <p>You'll be accumulating 100 points in the following dimensions by participating in
                                             Skillshops, activities
                                             and campus/community events. To begin, attend an Orientation — we'll help you get
                                             on track.
                                          </p>
                                          
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>Who Am I? Self-Awareness</li>
                                             
                                             <li>Where Am I Going? Exploration/Research</li>
                                             
                                             <li>How Do I Get There? Becoming a Professional</li>
                                             
                                          </ul>
                                          
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                    
                                    <aside class="col-md-3">
                                       
                                       <div class="box_side">
                                          
                                          <h3 class="add_bottom_30">Resource Links</h3>
                                          
                                          
                                          <p>
                                             <a href="http://preview.valenciacollege.edu/future-students/career-coach/?_ga=1.48107232.67023854.1466708865"><img src="/_resources/img/students/career-center/sidebar-career-coach-logo.png" alt="Career Coach" class="img-responsive"></a> <br>Visit Career Coach to explore careers, salaries, job demand, and related
                                             degrees.
                                             
                                          </p>
                                          
                                          
                                          <ul class="list_style_1">
                                             
                                             <li><a href="https://valenciacollege.edu/advising-counseling/">Advising &amp; Counseling</a></li>
                                             
                                             <li><a href="https://valenciacollege.edu/programs/">Degree &amp; Career Programs</a></li>
                                             
                                             <li><a href="https://valenciacollege.edu/internship/">Internship &amp; Workforce Services</a></li>
                                             
                                             <li><a href="/about/life-map/">LifeMap</a></li>
                                             
                                             <li><a href="https://valenciacollege.edu/student-services/">Student Services</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       
                                    </aside>
                                    
                                 </nav>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                        </ul>
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     <main role="main">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </main>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/career-action-distinction.pcf">©</a>
      </div>
   </body>
</html>