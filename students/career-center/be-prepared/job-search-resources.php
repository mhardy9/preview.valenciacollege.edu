<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/be-prepared/job-search-resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li><a href="/students/career-center/be-prepared/">Be Prepared</a></li>
               <li>Career Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Job Search Resources</h2>
                        
                        <p>Preparing for a job fair or interviewing for an internship or job requires that you
                           develop a set of skills beyond your academic studies.  
                        </p>
                        
                        <p>This series of short videos is designed to help you get started in developing those
                           skills.  
                        </p>
                        
                        <p>We encourage you to follow up with a career advisor at your campus career center to
                           help you refine the strategies that you need to successfully navigate your job search.
                           
                        </p>
                        
                        
                        
                        
                        <h3>Research the Company or Position Beforehand</h3>
                        
                        <ul>
                           
                           <li><a href="https://www.candidcareer.com/video-informational+interviewing,bddaa88aa1b24bace450,ValenciaCollege" target="_blank">How to Do an Informational Interview</a></li>
                           
                        </ul>
                        
                        
                        
                        <h3>Job Searching 101<br>
                           
                        </h3>
                        
                        <p>Learn employment search strategies and tips on interacting with employers at Job Fairs.</p>
                        
                        <ul>
                           
                           <li><a href="https://www.candidcareer.com/video-career+fairs,2ef35050ca72743ae6ca,ValenciaCollege" target="_blank">How to Work a Career Fair</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-career+fair+advice,4208b2677a084f5d7456,ValenciaCollege" target="_blank">Career Fair Advice</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-job+searching+101,70b6c547ce09aa59abd5,ValenciaCollege" target="_blank">Job Searching 101</a></li>
                           
                        </ul>
                        
                        
                        
                        <h3>Interviewing</h3>
                        
                        <ul>
                           
                           <li><a href="https://www.candidcareer.com/video-reverse+interview,4728b38627162ba29782,ValenciaCollege" target="_blank">The Reverse Interview</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-interview+mistakes,47d2bd3026ce457dcfd8,ValenciaCollege" target="_blank">Biggest Interview Mistakes</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-internet+entrepreneur,0348a275284efacdb6db,ValenciaCollege" target="_blank">Salary Negotiating</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-elevator+pitch,ed5fdd900a274930252f,ValenciaCollege" target="_blank">The Elevator Pitch</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        <h3>Creating Resumes &amp; Cover letters </h3>
                        
                        <ul>
                           
                           <li><a href="https://www.candidcareer.com/video-resume+writing,4fa549afe68373d9087e,ValenciaCollege" target="_blank">Resume Writing Do's &amp; Don'ts</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-cover+letters,9f6d7e30857d00d0312d,ValenciaCollege" target="_blank">Preparing a Cover Letter</a></li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        <h3>Networking</h3>
                        
                        <ul>
                           
                           <li><a href="https://www.candidcareer.com/video-etiquette++introductions+++meetings,dd46adf110cf871e3ed3,ValenciaCollege" target="_blank">Business Etiquette Part 3</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-linkedin+use,2adba41e5eaf26d1960a,ValenciaCollege" target="_blank">LinkedIn: Using LinkedIn Effectively</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-linkedin+profile,6ea2e20ca9c166ee52f9,ValenciaCollege" target="_blank">LinkedIn: Creating Your Profile</a></li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/be-prepared/job-search-resources.pcf">©</a>
      </div>
   </body>
</html>