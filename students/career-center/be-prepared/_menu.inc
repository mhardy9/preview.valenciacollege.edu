<ul>
<li><a href="../index.php">Career Center</a></li>
<li class="submenu megamenu">
<a class="show-submenu-mega" href="javascript:void(0);">Menu <i aria-hidden="true" class="far fa-angle-down"></i></a><div class="menu-wrapper c3"><div class="col-md-4"><ul>
<li><a href="../index.php">Career Center Home</a></li>
<li><a href="../services/index.php">Services &amp; Programs</a></li>
<a href="../explore/index.php"><h3>Explore Your Options</h3></a><li><a href="../explore/majors-to-careers.php">Connect Majors to Careers</a></li>
<li><a href="../explore/featured-career-videos.php">Featured Career Videos</a></li>
<a href="../develop-plan/index.php"><h3>Develop a Plan</h3></a><li><a href="../develop-plan/catalogs-manuals.php">College Catalogs and Transfer Manuals</a></li>
<a href="index.php"><h3>Be Prepared</h3></a><li><a href="job-search-resources.php">Job Search Resources</a></li>
<li><a href="http://net4.valenciacollege.edu/forms/careercenter/AskAdvisor.cfm" target="_blank">Ask A Career Advisor</a></li>
<li><a href="../skillshops.php">Skillshops</a></li>
<li><a href="../aboutus.php">About Us</a></li>
</ul></div></div>
</li>
</ul>
