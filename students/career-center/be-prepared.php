<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Be Prepared  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="prepared, job, search, resources, career, center, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/be-prepared.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Be Prepared </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>Be Prepared</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Interview Preparation</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Do you want to be better prepared for your job or internship interview? <a href="http://www.perfectinterview.com/valencia">The Perfect Interview</a> is an online resource that
                           allows you to practice and record your interview. An Atlas e-mail address is required
                           to set up your free
                           account.
                        </p>
                        
                        
                        <p>The <a href="/documents/students/career-center/job-search-guide.pdf">Job Search
                              Reference Guide</a> provides sample resumes, cover letters, and an interview guide.
                        </p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Job Search Tip Videos</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Preparing for a job fair or interviewing for an internship or job requires that you
                           develop a set of
                           skills beyond your academic studies.
                        </p>
                        
                        
                        <p>This series of short videos is designed to help you get started in developing those
                           skills.
                        </p>
                        
                        
                        <p>We encourage you to follow up with a career advisor at your campus career center to
                           help you refine the
                           strategies that you need to successfully navigate your job search.
                        </p>
                        
                        <hr class="styled_2">
                        
                        
                        <h4>Research the Company or Position Beforehand</h4>
                        
                        <ul class="list_style_1">
                           
                           <li>
                              <a href="https://www.candidcareer.com/video-informational+interviewing,bddaa88aa1b24bace450,ValenciaCollege">How
                                 to Do an Informational Interview</a>
                              
                           </li>
                           
                        </ul>
                        
                        <hr class="styled_2">
                        
                        &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                        &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                        
                        <h4>Job Searching 101</h4>
                        
                        <ul class="list_style_1">
                           
                           <li><a href="https://www.candidcareer.com/video-career+fairs,2ef35050ca72743ae6ca,ValenciaCollege">How to
                                 Work a Career Fair</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-career+fair+advice,4208b2677a084f5d7456,ValenciaCollege">Career
                                 Fair Advice</a></li>
                           
                           <li><a href="https://www.candidcareer.com/video-job+searching+101,70b6c547ce09aa59abd5,ValenciaCollege">Job
                                 Searching 101</a></li>
                           =======
                           =======
                           &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; 1c7cf389237c1dd4755e37315e933d29961ba73f
                           
                           <div class="header-alert" id="valencia-alert"></div>
                           
                           
                           <div class="header-chrome desktop-only">
                              
                              <div class="container">
                                 
                                 <nav>
                                    
                                    <div class="row">
                                       
                                       <div class="chromeNav col-lg-9 col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Main Navigation">
                                          
                                          <ul>
                                             
                                             <li class="submenu">
                                                <a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                                
                                                <ul>
                                                   
                                                   <li><a href="/academics/current-students/index.html">Current Students</a></li>
                                                   
                                                   <li><a href="https://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                                   
                                                   <li><a href="https://international.valenciacollege.edu">International Students</a></li>
                                                   
                                                   <li><a href="/academics/military-veterans/index.html">Military &amp;
                                                         Veterans</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li><a href="/academics/continuing-education/index.html">Continuing Education</a></li>
                                             
                                             <li><a href="/EMPLOYEES/faculty-staff.html">Faculty &amp; Staff</a></li>
                                             
                                             <li><a href="/FOUNDATION/alumni/index.html">Alumni &amp; Foundation</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       <div class="chromeNav col-md-3 col-sm-3 col-xs-3 pull-right">
                                          
                                          <ul>
                                             
                                             <li>
                                                <a href="#" data-toggle="modal" data-target="#login">Login <i class="far fa-angle-down" aria-hidden="true"></i></a>
                                                
                                                <ul>
                                                   
                                                   <li class="fa-2x"><a href="https://atlas.valenciacollege.edu/"><i class="far fa-user-circle-o" aria-hidden="true">&nbsp;</i> Atlas</a></li>
                                                   
                                                   <li>
                                                      <a href="https://valenciaalumniconnect.com/"><i class="far fa-users" aria-hidden="true">&nbsp;</i> Alumni Connect</a>
                                                      
                                                   </li>
                                                   
                                                   <li>
                                                      <a href="https://login.microsoftonline.com/PostToIDP.srf?msg=AuthnReq&amp;realm=mail.valenciacollege.edu&amp;wa=wsignin1.0&amp;wtrealm=urn:federation:MicrosoftOnline&amp;wctx=bk%3D1367916313%26LoginOptions%3D3"><i class="valencia-font icon-o365" aria-hidden="true">&nbsp;</i> Office 365</a>
                                                      
                                                   </li>
                                                   
                                                   
                                                   <li><a href="http://learn.valenciacollege.edu"><i class="far fa-graduation-cap" aria-hidden="true">&nbsp;</i>Valencia Online</a></li>
                                                   
                                                   <li><a href="https://outlook.office.com/owa/?realm=mail.valenciacollege.edu"><i class="far fa-envelope" aria-hidden="true">&nbsp;</i> Webmail</a></li>
                                                   
                                                </ul>
                                                
                                             </li>
                                             
                                             <li>
                                                <a href="https://search.valenciacollege.edu" id="search_bt"><i class="far fa-search" aria-hidden="true"></i><span>Search</span></a>
                                                
                                             </li>
                                             &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; fea88b4764015604ad30f5e4d5de1ce47c588a31
                                             
                                          </ul>
                                          
                                          <hr class="styled_2">
                                          
                                          
                                          <h4>Interviewing</h4>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li><a href="https://www.candidcareer.com/video-reverse+interview,4728b38627162ba29782,ValenciaCollege">The
                                                   Reverse Interview</a></li>
                                             
                                             <li><a href="https://www.candidcareer.com/video-interview+mistakes,47d2bd3026ce457dcfd8,ValenciaCollege">Biggest
                                                   Interview Mistakes</a></li>
                                             
                                             <li>
                                                <a href="https://www.candidcareer.com/video-internet+entrepreneur,0348a275284efacdb6db,ValenciaCollege">Salary
                                                   Negotiating</a>
                                                
                                             </li>
                                             
                                             <li><a href="https://www.candidcareer.com/video-elevator+pitch,ed5fdd900a274930252f,ValenciaCollege">The
                                                   Elevator Pitch</a></li>
                                             
                                          </ul>
                                          
                                          <hr class="styled_2">
                                          
                                          
                                          <h4>Creating Resumes &amp; Cover Letters</h4>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li><a href="https://www.candidcareer.com/video-resume+writing,4fa549afe68373d9087e,ValenciaCollege">Resume
                                                   Writing Do's &amp; Don'ts</a></li>
                                             
                                             <li><a href="https://www.candidcareer.com/video-cover+letters,9f6d7e30857d00d0312d,ValenciaCollege">Preparing
                                                   a Cover Letter</a></li>
                                             
                                          </ul>
                                          
                                          <hr class="styled_2">
                                          
                                          
                                          <h4>Networking</h4>
                                          
                                          <ul class="list_style_1">
                                             
                                             <li>
                                                <a href="https://www.candidcareer.com/video-etiquette++introductions+++meetings,dd46adf110cf871e3ed3,ValenciaCollege">Business
                                                   Etiquette Part 3</a>
                                                
                                             </li>
                                             
                                             <li><a href="https://www.candidcareer.com/video-linkedin+use,2adba41e5eaf26d1960a,ValenciaCollege">LinkedIn:
                                                   Using LinkedIn Effectively</a></li>
                                             
                                             <li><a href="https://www.candidcareer.com/video-linkedin+profile,6ea2e20ca9c166ee52f9,ValenciaCollege">LinkedIn:
                                                   Creating Your Profile</a></li>
                                             
                                          </ul>
                                          
                                          
                                       </div>
                                       
                                       
                                    </div>
                                    
                                    
                                    <aside class="col-md-3">
                                       
                                       <div class="box_side">
                                          
                                          <h3 class="add_bottom_30">Resource Links</h3>
                                          
                                          
                                          <p>
                                             <a href="http://preview.valenciacollege.edu/future-students/career-coach/?_ga=1.48107232.67023854.1466708865"><img src="/_resources/img/students/career-center/sidebar-career-coach-logo.png" alt="Career Coach" class="img-responsive"></a> <br>Visit Career Coach to explore careers, salaries, job demand, and related
                                             degrees.
                                             
                                          </p>
                                          
                                          
                                          <ul class="list_style_1">
                                             
                                             <li><a href="https://valenciacollege.edu/advising-counseling/">Advising &amp; Counseling</a></li>
                                             
                                             <li><a href="https://valenciacollege.edu/programs/">Degree &amp; Career Programs</a></li>
                                             
                                             <li><a href="https://valenciacollege.edu/internship/">Internship &amp; Workforce Services</a></li>
                                             
                                             <li><a href="/about/life-map/">LifeMap</a></li>
                                             
                                             <li><a href="https://valenciacollege.edu/student-services/">Student Services</a></li>
                                             
                                          </ul>
                                          
                                       </div>
                                       
                                       
                                    </aside>
                                    
                                 </nav>
                                 
                              </div>
                              
                              
                              
                           </div>
                           
                           
                        </ul>
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     <main role="main">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </main>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/be-prepared.pcf">©</a>
      </div>
   </body>
</html>