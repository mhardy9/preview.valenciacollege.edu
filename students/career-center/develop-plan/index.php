<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/develop-plan/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Develop Plan</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Develop a Plan for Action</h2>
                        
                        <p>Based on your assessments and new knowledge of career options, you can begin to develop
                           a career plan. Understanding your decision-making process will assist you. You can
                           then select an area of study that will lead to your initial career decision.
                        </p>
                        
                        <p>Researching your major includes:</p>
                        
                        <ul>
                           
                           <li> Understanding career options related to your major: <a href="https://whatcanidowiththismajor.com/major" target="_blank">What Can I Do With This Major?</a>
                              
                           </li>
                           
                           <li> Identifying colleges that offer your major and the courses required for transfer
                              into that major
                           </li>
                           
                           <li> Talking with faculty with experience in your career or major</li>
                           
                           <li> Conducting a career interview with people who are working in your career field. Find
                              out their career preparation and path
                           </li>
                           
                           <li> Exploring options for Internships, part-time or volunteer work  to gain experience
                              in your career and major. <a href="../../../../internship/index.html">Valencia's Internships</a>
                              
                           </li>
                           
                           <li> Learning about professional associations that can enhance your career preparation.</li>
                           
                           <li> Develop an educational plan using "My Lifemap" on Atlas, which is available <a href="https://atlas.valenciacollege.edu">online</a> or in the <a href="../../student-services/atlas-access-labs.html">Atlas Access Labs</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/develop-plan/index.pcf">©</a>
      </div>
   </body>
</html>