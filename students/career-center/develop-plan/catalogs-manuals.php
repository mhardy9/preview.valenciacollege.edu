<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/develop-plan/catalogs-manuals.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li><a href="/students/career-center/develop-plan/">Develop Plan</a></li>
               <li>Career Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>College Catalogs &amp; Transfer Manuals</h2>
                        
                        <p><strong>Catalogs</strong> are available in the Career Center office through <em><u>College Source</u></em> for Florida's 11 public universities, private colleges and universities, and community
                           colleges. All catalogs provide information on location, tuition, program prerequisites,
                           maps of the institution and various services available. Internet search is available.
                        </p>
                        
                        <p><strong>Transfer Manuals </strong>are available on-line for 7 of the 11 state universities in Florida. The UCF, FSU,
                           and FIU transfer manuals are available in the Career Center office. Transfer manuals
                           are developed for students who plan to transfer from one of Florida's 28 community
                           colleges to a state university, or to a designated private college/university. 
                        </p>
                        
                        <p><strong><u>The transfer manual provides information on prerequisites required for the major and
                                 other pertinent transfer information.</u></strong> For more information on Associate in Arts Transfer Guarantees and the Florida Articulation
                           Agreement, see the "Transfer Plans" section of your <a href="../../../catalog/index.html" target="_blank">Valencia College Catalog</a>.
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="th">State University</div>
                                 
                                 <div data-old-tag="th">Location</div>
                                 
                                 <div data-old-tag="th">Link</div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"> Florida A &amp; M University</div>
                                 
                                 <div data-old-tag="td">Tallahassee</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.famu.edu/index.cfm?a=catalog" target="_blank">Catalog</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Florida Atlantic University</div>
                                 
                                 <div data-old-tag="td">Boca Raton</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.fau.edu/registrar/tsm.php" target="_blank">Transfer Student Guidebook</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Florida Gulf Coast University</div>
                                 
                                 <div data-old-tag="td">Ft. Myers</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.fgcu.edu/Admissions/Prospective/transferadmincriteria.html" target="_blank">Transfer Admission Requirements</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Florida International University</div>
                                 
                                 <div data-old-tag="td">Miami</div>
                                 
                                 <div data-old-tag="td"><a href="http://admissions.fiu.edu/how/transfer.php" target="_blank"> Transfer Student Info</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">Florida State University</div>
                                 
                                 <div data-old-tag="td">Tallahassee</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.academic-guide.fsu.edu/" target="_blank">Academic Guide</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">University of Central Florida</div>
                                 
                                 <div data-old-tag="td">Orlando</div>
                                 
                                 <div data-old-tag="td"><a href="http://transfer.sdes.ucf.edu/" target="_blank">Transfer Students</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">University of Florida</div>
                                 
                                 <div data-old-tag="td">Gainesville</div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://www.admissions.ufl.edu/prospectivetransfer.html" target="_blank">Transfer Info</a><br>
                                    <a href="https://www.isis.ufl.edu/cgi-bin/nirvana?MDASTRAN=RSI-ETMEN" target="_blank">Choose your UF Major</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">University of North Florida</div>
                                 
                                 <div data-old-tag="td">Jacksonville</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.unf.edu/admissions/transfer/" target="_blank">Transfer Admissions</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">University of South Florida</div>
                                 
                                 <div data-old-tag="td">Tampa</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.ugs.usf.edu/catalogs.htm" target="_blank">Catalog</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">University of West Florida</div>
                                 
                                 <div data-old-tag="td">Pensacola</div>
                                 
                                 <div data-old-tag="td"><a href="http://uwf.edu/catalog/" target="_blank">Catalog</a></div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">New College of Florida</div>
                                 
                                 <div data-old-tag="td">Sarasota</div>
                                 
                                 <div data-old-tag="td"><a href="http://www.ncf.edu/transfer-students" target="_blank">Transfer Info</a></div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <p>Transfer Manuals are Available in the Career Center Office </p>
                        
                        <p>Other Helpful Links:</p>
                        
                        <p><a href="http://www.bls.gov/oco/" target="_blank">Bureau of Labor Statistics</a> (Occupational Outlook Handbook) 
                        </p>
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/develop-plan/catalogs-manuals.pcf">©</a>
      </div>
   </body>
</html>