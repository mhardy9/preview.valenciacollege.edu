<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Explore Your Options  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="explore, options, career, center, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/explore.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Explore Your Options </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>Explore Your Options</h2>
                     
                     <hr class="styled_2">
                     
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Learn About Yourself</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>A satisfying career decision, whether it be an initial career choice or the decision
                           to change careers,
                           requires current information about your interests, personality preferences, skills,
                           and values. You can
                           identify career interests through a variety of electronic and non-electronic assessments
                           that relate to
                           your strengths, likes and dislikes, skills, and what is important to you.
                        </p>
                        
                        
                        <p><a href="index.html">Visit your career center</a> for more details on available resources for further
                           exploration. Available assessments include:
                        </p>
                        
                        <ul class="list_style_1">
                           
                           
                           
                           <li>Work Values Inventory</li>
                           
                           <li>Motivated Skills Sort</li>
                           
                           <li>Myers-Briggs Type (MBTI) Indicator</li>
                           
                           <li>STRONG Interest Inventory</li>
                           
                        </ul>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>MyCareerShines</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p><a href="http://www.mycareershines.org">MyCareerShines</a> is Florida's new career and education planning
                           system available to students in Florida for <strong>FREE</strong>. The system allows students to explore
                           careers, assess interests and skills, and find out more about linking education to
                           careers.
                        </p>
                        
                        
                        <p>Start planning your path to a career you love.</p>
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                     &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                     
                     <div class="indent_title_in">
                        
                        <h3>Learn About Career Fields</h3>
                        =======
                        =======
                        &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; 1c7cf389237c1dd4755e37315e933d29961ba73f
                        
                        <div class="header-alert" id="valencia-alert"></div>
                        
                        
                        <div class="header-chrome desktop-only">
                           
                           <div class="container">
                              
                              <nav>
                                 
                                 <div class="row">
                                    
                                    <div class="chromeNav col-lg-9 col-md-9 col-sm-9 col-xs-9" role="navigation" aria-label="Main Navigation">
                                       
                                       <ul>
                                          
                                          <li class="submenu">
                                             <a href="javascript:void(0);" class="show-submenu">Students <i class="fas fa-chevron-down" aria-hidden="true"></i></a>
                                             
                                             <ul>
                                                
                                                <li><a href="/academics/current-students/index.html">Current Students</a></li>
                                                
                                                <li><a href="https://preview.valenciacollege.edu/future-students/">Future Students</a></li>
                                                
                                                <li><a href="https://international.valenciacollege.edu">International Students</a></li>
                                                
                                                <li><a href="/academics/military-veterans/index.html">Military &amp;
                                                      Veterans</a></li>
                                                
                                             </ul>
                                             
                                          </li>
                                          
                                          <li><a href="/academics/continuing-education/index.html">Continuing Education</a></li>
                                          
                                          <li><a href="/EMPLOYEES/faculty-staff.html">Faculty &amp; Staff</a></li>
                                          
                                          <li><a href="/FOUNDATION/alumni/index.html">Alumni &amp; Foundation</a></li>
                                          
                                       </ul>
                                       &amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt;&amp;gt; fea88b4764015604ad30f5e4d5de1ce47c588a31
                                       
                                    </div>
                                    
                                    <div class="wrapper_indent">
                                       
                                       <p>Explore the world of work through a wealth of online and hard-copy resources:</p>
                                       
                                       <ul class="list_style_1">
                                          
                                          <li><a href="http://www.bls.gov/oco/">Occupational Outlook Handbook</a></li>
                                          
                                          <li><a href="/documents/students/career-center/information-interview-guide.pdf">Informational
                                                Interview Guide</a></li>
                                          
                                          <li>Encyclopedia of Careers</li>
                                          
                                          <li>Vocational biographies</li>
                                          
                                          <li>Career books, videos, and CD-ROMs</li>
                                          
                                          <li>Career Web sites</li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    <hr class="styled_2">
                                    
                                    
                                    <div class="indent_title_in">
                                       
                                       <h3>Connect Majors to Careers</h3>
                                       
                                    </div>
                                    
                                    <div class="wrapper_indent">
                                       
                                       
                                       <p>Finally, a convenient website where you can explore information and websites from
                                          multiple majors to help
                                          you learn about a wide range of career opportunities.
                                       </p>
                                       
                                       <p>For a complete list of Valencia College degree programs:</p>
                                       
                                       <ul class="list_style_1">
                                          
                                          <li><a href="https://valenciacollege.edu/aadegrees/premajors_list.cfm">Associate in Arts Transfer Plan
                                                &amp; Pre-Majors</a></li>
                                          
                                          <li><a href="https://valenciacollege.edu/asdegrees/">Associate in Science degree and certificate
                                                programs</a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <aside class="col-md-3">
                                    
                                    <div class="box_side">
                                       
                                       <h3 class="add_bottom_30">Resource Links</h3>
                                       
                                       
                                       <p>
                                          <a href="http://preview.valenciacollege.edu/future-students/career-coach/?_ga=1.48107232.67023854.1466708865"><img src="/_resources/img/students/career-center/sidebar-career-coach-logo.png" alt="Career Coach" class="img-responsive"></a> <br>Visit Career Coach to explore careers, salaries, job demand, and related
                                          degrees.
                                          
                                       </p>
                                       
                                       
                                       <ul class="list_style_1">
                                          
                                          <li><a href="https://valenciacollege.edu/advising-counseling/">Advising &amp; Counseling</a></li>
                                          
                                          <li><a href="https://valenciacollege.edu/programs/">Degree &amp; Career Programs</a></li>
                                          
                                          <li><a href="https://valenciacollege.edu/internship/">Internship &amp; Workforce Services</a></li>
                                          
                                          <li><a href="/about/life-map/">LifeMap</a></li>
                                          
                                          <li><a href="https://valenciacollege.edu/student-services/">Student Services</a></li>
                                          
                                       </ul>
                                       
                                    </div>
                                    
                                    
                                 </aside>
                                 
                              </nav>
                              
                           </div>
                           
                           
                           
                        </div>
                        
                        
                     </div>
                     
                     
                     
                     
                     
                     
                     <main role="main">
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                        
                     </main>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/explore.pcf">©</a>
      </div>
   </body>
</html>