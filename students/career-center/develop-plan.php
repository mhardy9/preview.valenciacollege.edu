<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Develop a Plan  | Valencia College</title>
      <meta name="Description" content="Based on your assessments and new knowledge of career options, you can begin to develop a career plan. Understanding your decision-making process will assist you.">
      <meta name="Keywords" content="develop, plan, career, center, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/develop-plan.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Develop a Plan </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>Develop a Plan</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Action Planning</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p>Based on your assessments and new knowledge of career options, you can begin to develop
                           a career plan.
                           Understanding your decision-making process will assist you. You can then select an
                           area of study that will
                           lead to your initial career decision.
                        </p>
                        
                        
                        <p>Researching your major includes:</p>
                        
                        <ul class="list_style_1">
                           &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                           
                           
                           <li>Understanding career options related to your major: <a href="http://whatcanidowiththismajor.com/">What
                                 Can I Do With This Major?</a>
                              
                           </li>
                           
                           <li>Identifying colleges that offer your major and the courses required for transfer into
                              that major
                           </li>
                           
                           <li>Talking with faculty with experience in your career or major</li>
                           
                           <li>Conducting a career interview with people who are working in your career field. Find
                              out their career
                              preparation and path
                              =======
                              
                           </li>
                           
                           <li><a href="https://valenciacollege.edu/advising-counseling/">Advising &amp; Counseling</a></li>
                           
                           <li><a href="https://valenciacollege.edu/programs/">Degree &amp; Career Programs</a></li>
                           
                           <li><a href="https://valenciacollege.edu/internship/">Internship &amp; Workforce Services</a></li>
                           
                           <li><a href="/about/life-map/">LifeMap</a></li>
                           
                           <li><a href="https://valenciacollege.edu/student-services/">Student Services</a></li>
                           
                        </ul>
                        
                     </div>
                     
                     
                     
                     
                  </div>
                  
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/develop-plan.pcf">©</a>
      </div>
   </body>
</html>