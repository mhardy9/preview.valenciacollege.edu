<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>College Catalogs &amp; Transfer Manuals  | Valencia College</title>
      <meta name="Description" content="Catalogs provide information on location, tuition, program prerequisites, maps of the institution and various services available. The transfer manual provides information on prerequisites required for the major and other pertinent transfer information.">
      <meta name="Keywords" content="catalogs, transfer, manuals, career, center, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/catalogs-manuals.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>College Catalogs &amp; Transfer Manuals </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <h2>College Catalogs &amp; Transfer Manuals</h2>
                     
                     <hr class="styled_2">
                     
                     
                     <div class="indent_title_in">
                        
                        <h3>Career Center Resources</h3>
                        
                     </div>
                     
                     <div class="wrapper_indent">
                        
                        <p><strong>Catalogs</strong> are available in the Career Center office through College Source for Florida's
                           11 public universities, private colleges and universities, and community colleges.
                           All catalogs provide
                           information on location, tuition, program prerequisites, maps of the institution and
                           various services
                           available. Internet search is available.
                        </p>
                        
                        
                        <p><strong>Transfer Manuals</strong> are available on-line for 7 of the 11 state universities in Florida.
                           The UCF, FSU, and FIU transfer manuals are available in the Career Center office.
                           Transfer manuals are
                           developed for students who plan to transfer from one of Florida's 28 community colleges
                           to a state
                           university, or to a designated private college/university.
                        </p>
                        
                        
                        <p>The transfer manual provides information on prerequisites required for the major and
                           other pertinent
                           transfer information. For more information on Associate in Arts Transfer Guarantees
                           and the Florida
                           Articulation Agreement, see the "Transfer Plans" section of your <a href="https://valenciacollege.edu/catalog/">Valencia College Catalog</a>.
                        </p>
                        
                        
                     </div>
                     
                     <hr class="styled_2">
                     
                     &amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt;&amp;lt; HEAD
                     
                     <div class="indent_title_in">
                        
                        <h3>Online Resources</h3>
                        =======
                        
                        
                        
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/catalogs-manuals.pcf">©</a>
      </div>
   </body>
</html>