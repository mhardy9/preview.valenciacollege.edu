<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/explore/featured-career-videos.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li><a href="/students/career-center/explore/">Explore</a></li>
               <li>Career Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Featured Career Videos</h2>
                        
                        
                        <p>The Career Centers contribute  to academic and career success by providing services
                           and resources that allow  students to explore career options, set career &amp; personal
                           goals,  and develop  employability skills.
                        </p> Discover career fields and learn what it takes to get there.<br><a href="http://www.candidcareer.com/ValenciaCollege" target="_blank">www.candidcareer.com/ValenciaCollege</a> 
                        
                        
                        
                        <div>
                           
                           <h3><strong>Lighting Designer, Self-Employed</strong></h3>
                           
                           
                           
                           
                           
                           <h3><strong>Senior Vice President of Human Performance, Comcast</strong></h3>
                           
                           
                           
                           <h3><strong>Senior Financial Sales Director, Bloomberg LP, Job Description</strong></h3>
                           
                           
                           
                           <h3><strong>Assoc. Producer, Imagineer, Theme Park &amp; Resort Engineering</strong></h3>
                           
                           
                           
                           <h3><strong>Registered Nurse, North Mississippi Medical Center</strong></h3>
                           
                           
                           
                           
                           
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <h2>Explore Careers</h2>
                        
                        
                        <iframe allowfullscreen="" frameborder="0" height="200" scrolling="no" src="https://www.candidcareer.com/embed.php?vkey=7437a1309dc64e4db32a&amp;shared=ValenciaCollegeAdmin&amp;uid=4148" title="Share Video" width="245"></iframe>
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/explore/featured-career-videos.pcf">©</a>
      </div>
   </body>
</html>