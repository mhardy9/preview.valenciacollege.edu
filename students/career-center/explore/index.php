<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/explore/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Explore</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        
                        <h2>Explore Your Options</h2>
                        
                        <h3>Learn About Yourself</h3>
                        
                        <p>A satisfying career decision, whether it be an initial career choice or the decision
                           to change careers, requires current information about your interests, personality
                           preferences, skills, and values. You can identify career interests through a variety
                           of electronic and non-electronic assessments that relate to your 
                           strengths,
                           likes and dislikes, skills, and what is important to you.
                        </p>
                        
                        <p> <a href="../aboutus.html">Visit your  career center</a> for more details on available resources for further exploration.<br>
                           
                        </p>
                        
                        <p>Available assessments include:</p>
                        
                        <ul>
                           
                           <li><a href="http://www.flvs.net/myFLVS/counselor-office/Pages/Career%20Advice/Career%20Selection.aspx" target="_blank"> Florida Virtual Campus Career Exploration</a></li>
                           
                           <li> Work Values Inventory </li>
                           
                           <li>Motivated Skills Sort</li>
                           
                           <li> Myers-Briggs Type (MBTI) Indicator</li>
                           
                           <li> STRONG Interest Inventory</li>
                           
                        </ul>
                        
                        
                        
                        <h3>Learn About Career Fields</h3>
                        
                        <p>Explore the world of work through a wealth of online and hard-copy 
                           resources:
                        </p>
                        
                        <ul>
                           
                           <li> <a href="http://www.bls.gov/oco/" target="_blank">Occupational Outlook Handbook</a>
                              
                           </li>
                           
                           <li>
                              <a href="documents/InformationInterview-revised2014MASTERPDF.pdf" target="_blank">Informational Interview Guide</a> 
                           </li>
                           
                           <li> Encyclopedia of Careers</li>
                           
                           <li> Vocational biographies</li>
                           
                           <li> Career books, videos, and CD-ROMs</li>
                           
                           <li> Career Web sites</li>
                           
                        </ul>
                        
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/explore/index.pcf">©</a>
      </div>
   </body>
</html>