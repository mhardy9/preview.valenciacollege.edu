<ul>
<li><a href="/students/career-center/index.php">Career Center</a></li>
<li><a href="/students/career-center/aboutus.php">About Us</a></li>
<li><a href="/students/career-center/services/index.php">Services &amp; Programs</a></li>



   <li class="megamenu submenu"><a class="show-submenu-mega" href="javascript:void(0);">Explore | Develop | Prepare<span class="fas fa-chevron-down" aria-hidden="true"></span></a>
              <div class="menu-wrapper">
                <div class="col-md-12">
                  <h3>Explore Your Options</h3>
                  <ul>
                    <li><a href="/students/career-center/explore/index.php">Explore Your Options</a></li>
                    <li><a href="/students/career-center/explore/majors-to-careers.php">Connect Majors to Careers</a></li>
                    <li><a href="/students/career-center/explore/featured-career-videos.php">Featured Career Videos</a></li></ul>
                  <h3>Develop a Plan for Action</h3>
                  <ul>
                 <li><a href="/students/career-center/develop-plan/index.php">Develop a Plan</a></li>
                  <li><a href="/students/career-center/develop-plan/catalogs-manuals.php">College Catalogs and Transfer Manuals</a></li>
                  </ul>
                 <h3>Learning Technology Services (LTS) </h3>
                  <ul>
               <li><a href="/students/career-center/be-prepared/index.php">Be Prepared</a></li>
            <li><a href="/students/career-center/be-prepared/job-search-resources.php">Job Search Resources</a></li>
            </ul>

                </div>
               
              </div>
              <!-- End menu-wrapper --></li>
              

<li><a href="http://net4.valenciacollege.edu/forms/careercenter/AskAdvisor.PHP" target="_blank">Ask A Career Advisor</a></li>
<li><a href="/students/career-center/skillshops.php">Skillshops</a></li>
</ul>