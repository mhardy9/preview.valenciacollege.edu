<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/skillshops.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Career Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>Upcoming Career Development Skillshops</h2>
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/college_etiquette_mind_your_manners" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>College Etiquette: Mind Your Manners at Osceola Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>October 26</strong>
                                    
                                    at 1:00 PM</span> 
                                 
                                 <p>This Skillshop is designed to help students understand proper behavior in 4 different
                                    key areas: classrooms, email, social media and job search. Although technology is...
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/preparing_for_professional_graduate_education_how_do_i_do_it_all" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>Preparing for Professional Graduate Education: How Do I Do It All?  at Osceola Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>November 15</strong>
                                    
                                    at 1:00 PM</span> 
                                 
                                 <p>Students will learn what is required to be a successful candidate for graduate medical
                                    education such as: medical, pharmacy, physical therapy, etc.  Students will discuss
                                    how...
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <div>
                           
                           <a href="http://events.valenciacollege.edu/event/whats_your_code_matching_your_interests_skills_to_careers" target="_blank">
                              
                              
                              
                              
                              <div>
                                 
                                 <span>What's Your Code? Matching your interests and skills to careers at Lake Nona Campus</span> <br>
                                 
                                 
                                 <span>
                                    <strong>November 29</strong>
                                    
                                    at 2:00 PM</span> 
                                 
                                 <p>Holland codes are a simple way to match individuals to the "right" careers based on
                                    matching an individual's interests, skills and qualitites to a corresponding career
                                    type....
                                 </p>
                                 
                                 
                                 
                              </div>
                              </a>
                           
                        </div>
                        
                        
                        
                        
                        <p><a href="#top">TOP</a></p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/skillshops.pcf">©</a>
      </div>
   </body>
</html>