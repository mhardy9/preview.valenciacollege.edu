<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/cac.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Career Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>Career Action Distinction</h2>
                        
                        
                        <p>Like many decisions you've made before, career selection is a defining moment. This
                           distinction offers a variety of activities and resources that support career exploration
                           and employment readiness — getting and keeping a job.
                        </p>
                        
                        
                        <p>Through this experience, you will:</p>
                        
                        <ul>
                           
                           <li>Increase self-awareness</li>
                           
                           <li>Explore and evaluate career and transfer school options</li>
                           
                           <li>Gain an awareness of what it means to be a "professional" in the workplace</li>
                           
                           <li>Learn how to market yourself to employers, and make yourself more competitive in your
                              job search
                           </li>
                           
                        </ul>
                        
                        
                        <p>You'll receive:</p>
                        
                        <ul>
                           
                           <li>A cord at graduation</li>
                           
                           <li>A recognition of completion</li>
                           
                           <li>A co-curricular verification letter of activities completed</li>
                           
                           <li>An enhancement to your resume and transfer college application</li>
                           
                        </ul>
                        
                        
                        <p><strong>Plus</strong>, more benefits to gain a competitive edge:
                        </p>
                        
                        
                        <ul>
                           
                           <li>Connect your college experience to your career</li>
                           
                           <li>Practice job search and networking skills</li>
                           
                           <li>Learn what it means to be a professional</li>
                           
                           <li>Maximize your research skills as you prepare for transfer or the workforce</li>
                           
                        </ul>
                        
                        
                        <p>Enrollment is open to any degree-seeking student at Valencia during the start of both
                           Fall and Spring semesters. <strong>We suggest that you have completed 12-15 credit hours before beginning.</strong></p>
                        
                        
                        <p>You'll be accumulating 100 points in the following dimensions by participating in
                           Skillshops, activities and campus/community events. To begin, attend an Orientation
                           — we'll help you get on track.
                        </p>
                        
                        
                        <ul>
                           
                           <li>Who Am I? Self-Awareness</li>
                           
                           <li>Where Am I Going? Exploration/Research</li>
                           
                           <li>How Do I Get There? Becoming a Professional</li>
                           
                        </ul>
                        
                        
                        <p>It's easy to enroll! <strong>Using your Atlas e-mail,</strong> send your name and your VID number to your campus's Career Center.
                        </p>
                        
                        
                        <ul>
                           
                           <li>East/Winter Park Campuses: <a href="mailto:careercenter-east@valenciacollege.edu">careercenter-east@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>Osceola Campus: <a href="mailto:osceolacareercenter@valenciacollege.edu">osceolacareercenter@valenciacollege.edu</a>
                              
                           </li>
                           
                           <li>West Campus: <a href="mailto:careercenter-west@valenciacollege.edu">careercenter-west@valenciacollege.edu</a>
                              
                           </li>
                           
                        </ul>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/cac.pcf">©</a>
      </div>
   </body>
</html>