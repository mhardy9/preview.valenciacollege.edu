<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Career Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/career-center/about-us.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/career-center/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Career Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/career-center/">Career Center</a></li>
               <li>Career Center</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <a name="content" id="content"></a>
                        <a href="index.html"></a>
                        
                        
                        
                        
                        <h2>About Us</h2>
                        
                        <h3>Purpose</h3>
                        
                        
                        <p>The Career Centers contribute  to academic and career success by providing services
                           and resources that allow  students to explore career options, set career &amp; personal
                           goals, and develop  employability skills.          
                        </p>
                        
                        <h3>Meet the Career Center Staff</h3>
                        
                        <h3>East  Campus</h3>
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 5, Rm 230</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-2259</div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        Cindy Oakley-Paulik, Coordinator of Career Development <br>
                        B.A., University of Florida<br>
                        M.A., University of Florida
                        
                        <p>Catherine S. Espenscheid, Career Counselor<br>
                           B.S., University of Florida<br>
                           M.Ed., University of Florida
                        </p>
                        
                        <p>Cassandra Haley, Career Advisor<br>
                           B.A., Inter American University of Puerto Rico
                        </p>
                        
                        <p>Kimberly Williams, Career Advisor <br>
                           B.S., University of Central Florida <br>
                           M.S., Concordia University-Wisconsin 
                        </p>
                        
                        
                        <h3>Osceola  Campus</h3>
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 2, Rm 125</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-4391</div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        <p>Kristin Kaye, Career Advisor<br>
                           B.A., University of Central Florida              <br>
                           M.L.I.S., Wayne State University
                        </p>
                        
                        <p>Margaux Pratt , Career Advisor</p>
                        
                        <p>B.A., University of Central Florida </p>
                        
                        <p>M.A., University of Central Florida</p>
                        
                        <p>Sara Hernandez, Career Advisor<br>
                           B.S., Pontifical Javeriana University
                        </p>
                        
                        
                        <h3>West  Campus</h3>
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>SSB, Rm 206</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-1464</div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        <p>Christine Moran, Coordinator of Career Development <br>
                           B.A. Goucher College
                           
                        </p>
                        
                        <p>Celeste Henry, Career Counselor<br>
                           B.S.,  University of Wisconsin <br>
                           M.S.,  University of Wisconsin<br>
                           Ed.D., Nova Southeastern University
                        </p>
                        
                        <p>Graham Quirk, Career Advisor<br>
                           B.S., Canisius College<br>
                           M.S., Canisius College
                        </p>
                        
                        <p>Ed.D., University of Central Florida </p>
                        
                        <p>Tamara Engelhaupt, Career Advisor</p>
                        
                        <p>B.S.W., University of North Carolina at Charlotte</p>
                        
                        <p>M.Ed.,&nbsp; University of North Carolina Wilmington</p>
                        
                        <p>Katelyn Mason, Staff Assistant</p>
                        
                        <p>B.S.E., Bridgewater State University </p>
                        
                        
                        <h3>Winter  Park Campus</h3>
                        
                        
                        
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 217</div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>407-582-6882</div>
                                    
                                 </div>
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        <p>Julie Corderman, Director, Student Services <br>
                           B.A., Emory University <br>
                           M.S., University of Central Florida <br>
                           Ed.D., Educational Leadership, University of Central Florida
                           
                        </p>
                        
                        <p>Jalisa Morant , Career Advisor</p>
                        
                        <p>B.S., University of Central Florida </p>
                        
                        <p dir="auto">Sean Dement, Career Advisor<br>
                           B.S. University of Central Florida
                        </p>
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank"><img alt="Career Coach" height="106" src="career-coach-logo-rgb.png" width="245"></a></p>
                        
                        
                        <p><a href="http://preview.valenciacollege.edu/future-students/career-coach/" target="_blank" title="Career Coach">Visit Career Coach</a>&nbsp;to explore  careers, salaries, job demand and related degrees.
                        </p>
                        
                        
                        
                        
                        <h2>
                           <center>Explore Careers</center>
                        </h2>
                        
                        <div>
                           <center><iframe frameborder="0" marginheight="0" marginwidth="0" scrolling="no" src="https://www.candidcareer.com/widget/widget4.php?uid=4147"></iframe></center>
                        </div>
                        
                        
                        
                        
                        
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/career-center/about-us.pcf">©</a>
      </div>
   </body>
</html>