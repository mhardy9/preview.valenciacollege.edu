<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Rules | Testing Center | Valencia College</title>
      <meta name="Description" content="Rules | Testing Center">
      <meta name="Keywords" content="college, school, educational, testing, center, rules">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/testing/rules.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/testing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/testing/">Testing</a></li>
               <li>Rules</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Rules</h2>
                        
                        <p>Failure to comply with these regulations gives the test administrator the right to
                           collect test materials and return them to the instructor.
                        </p>
                        
                        
                        <h3>Before Entering the Testing Center</h3>
                        
                        <ul>
                           
                           <li>Students must have their Valencia ID card for testing purposes.</li>
                           
                           <li>No tests are distributed the last hour before closing <strong>under any circumstances.</strong>
                              
                           </li>
                           
                           <li>Finish food and drinks before entering the testing room. </li>
                           
                        </ul>
                        
                        <h3>Technology Policy</h3>
                        
                        <ul>
                           
                           <li>No electronic devices allowed. Cell phones and all other electronic devices must be
                              turned off.
                           </li>
                           
                           <li>If you are taking an online test, you will be required to use the Testing Center computers
                              and will not be allowed to use your own laptop computer.
                           </li>
                           
                        </ul>
                        
                        <h3>During the Test</h3>
                        
                        <ul>
                           
                           <li>Read instructions carefully before you begin your test. If you are not sure of directions,
                              check with a testing staff at the counter.
                           </li>
                           
                           <li>You are responsible for monitoring your own time for an exam.</li>
                           
                           <li>Talking to other students in the Testing Center is cause for an accusation of cheating.</li>
                           
                           <li>All students are expected to comply with the Valencia Academic Honesty policy. </li>
                           
                        </ul>
                        
                        <h3>Notes and Scratch Paper</h3>
                        
                        <ul>
                           
                           <li>Any resources to be used during the exam must be arranged <u>by your instructor</u> with the Testing Center Staff. 
                           </li>
                           
                           <li> Any test(s) which permits the use of  notes, the notes must be hard copies - you
                              may not use notes stored on an electronic device.
                           </li>
                           
                           <li>All scratch paper must be turned in with the exam.</li>
                           
                           <li>You may not write down any information from the test and take it out of the Testing
                              Center.
                           </li>
                           
                        </ul>
                        
                        <h3>Leaving the Testing Center</h3>
                        
                        <ul>
                           
                           <li>Once you receive your test, you will not be allowed to leave the Testing Center.</li>
                           
                           <li>Once a test is handed in, it cannot be returned to you for any reason. </li>
                           
                           <li>If you leave the room without permission of the test proctor, your exam will be considered
                              complete and must be turned in.
                           </li>
                           
                           <li>
                              <strong>Restroom breaks are not permitted during a test</strong>. Please make a restroom stop before you come to the Testing Center.
                           </li>
                           
                           <li>Tests will be collected five minutes before closing time.</li>
                           
                        </ul>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <strong>Each Testing Center has its own policy regarding acceptable identification for test-taking.
                           Please check the center you plan to test with before arriving for your exam.</strong>
                        
                        
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/testing/rules.pcf">©</a>
      </div>
   </body>
</html>