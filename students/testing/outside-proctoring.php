<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Outside Proctoring | Testing Center | Valencia College</title>
      <meta name="Description" content="Outside Proctoring | Testing Center">
      <meta name="Keywords" content="college, school, educational, testing, center, outside, proctoring">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/testing/outside-proctoring.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/testing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/testing/">Testing</a></li>
               <li>Outside Proctoring</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Outside Proctoring</h2>
                        
                        
                        <p>The <a href="../../../east/academic-success/testing/index.html">East</a> and <a href="../../../osceola/testing/index.html">Osceola</a> campus Testing Centers will proctors exams from other institutions. Please contact
                           those centers directly to make arrangements for proctoring. 
                        </p>
                        
                        <p>For Proctor Students from other institutions, you must have a valid photo ID. Please
                           be sure to bring your proctor test payment receipt with you in order to test unless
                           your institution pays the fees for you; example WGU students. All of the above rules
                           and hours apply to Proctor Students. 
                        </p>
                        
                        <p>West Campus proctors outside exams through their <a href="http://preview.valenciacollege.edu/continuing-education/">Continuing Education Center</a>. Please contact Jane O'Rork @ 407-582-6777 or email at testingcenter@valenciacollege.edu.
                           
                        </p>
                        
                        <p>To Have an Exam Proctored</p>
                        
                        <ol>
                           
                           <li>Fill out an <a href="http://valenciacollege.edu/testing-center/documents/IS-Student-Information-Sheet.docx">information form</a> and send the information to either the <a href="../../../east/academic-success/testing/default.html">East</a> or <a href="../../../osceola/testing/index.html">Osceola</a> Testing Center. Without an information sheet, the Testing Center has no way of contacting
                              the student.
                           </li>
                           
                           <li>Have the other institution send the exam to the Testing Center at the address on the
                              <a href="documents/Proctor-Information-Valencia-College-East-Campus-Testing-Center.pdf">proctor information sheet</a>.
                           </li>
                           
                           <li>Pay proctoring fees by simply picking up a fee slip at the Testing Center and then
                              taking it to the <a href="../../../business-office/index.html">Business Office</a>.
                           </li>
                           
                           <li>Bring the receipt from the Business Office to the Testing Center and take the test.</li>
                           
                        </ol>
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <strong>Each Testing Center has its own policy regarding acceptable identification for test-taking.
                           Please check the center you plan to test with before arriving for your exam.</strong>
                        
                        
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/testing/outside-proctoring.pcf">©</a>
      </div>
   </body>
</html>