<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Frequently Asked Questions  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="faq, frequently, asked, questions, testing, center, west, campus, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/testing/west/faqs.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/testing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/testing/">Testing</a></li>
               <li><a href="/students/testing/west/">West</a></li>
               <li>Frequently Asked Questions </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>Frequently Asked Questions (FAQs)</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Student FAQs</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>What do I bring with me to the Testing Center?</h4>
                           
                           <p>A current Valencia student ID card, UCF student ID card, or a state driver's license
                              is required to sit
                              an exam. <strong>No exceptions!</strong></p>
                           
                           
                           <p>A Valencia ID can be obtained from any <a href="https://valenciacollege.edu/security/">Campus
                                 Security</a> office.
                           </p>
                           
                           
                           <h4>What do I need to know before I come for testing?</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Your professor's last name. Tests are filed by the professor's name.</li>
                              
                              <li>Supplies your professor allows for your test (textbooks, calculators, notes, etc.).
                                 Check with your
                                 instructor for details.
                                 
                              </li>
                              
                              <li>The deadline date for your test. Most tests are not available after the deadline unless
                                 you make
                                 special arrangements with your professor in advance.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <h4>What are the Testing Center's policies?</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>No electronic devices allowed. Cell phones and all other non-approved electronic devices
                                 must be
                                 turned off. If you leave the room to answer a phone call or text, your test is considered
                                 complete and
                                 must be turned in.
                                 
                              </li>
                              
                              <li>
                                 All materials not used during testing must be placed in the RED VALENCIA TESTING CENTER
                                 bag provided
                                 during check. <img class="image-responsive center-block" src="/_resources/img/students/testing/west/red-testing-bag.jpg" alt="The Red Valencia Testing Bag">
                                 
                              </li>
                              
                              <li>No restroom breaks are allowed after you receive your exam until your test is completed!</li>
                              
                              <li>You may not start an exam, then turn it in and return later to complete it. Give yourself
                                 plenty of
                                 time to complete a test in one sitting. You must take a test after you have seen it
                                 - incomplete tests
                                 are returned to your professor.
                                 
                              </li>
                              
                              <li>All students are expected to comply with the Valencia College academic honesty policy
                                 in the student
                                 handbook and posted in the Testing Centers.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <p>Policies are subject to change, and may vary from campus to campus.</p>
                           
                           <p>Consult the <a href="rules.html">full list of West Campus Testing Center rules</a> for more information.
                              
                           </p>
                           
                           
                           <h4>What do I need to do if I want a non-Valencia or non-UCF test proctored?</h4>
                           
                           <p>PLEASE NOTE: Individual proctoring of non-Valencia tests is not arranged through the
                              Testing Center. It
                              is arranged through <a href="http://preview.valenciacollege.edu/continuing-education/programs/workforce-testing/">Continuing
                                 Education</a>.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Faculty FAQs</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <h4>How do I submit a test for a student?</h4>
                           
                           <ul class="list_style_1">
                              
                              <li>Tests can be uploaded through Atlas. Click the "Testing Center Referral Form" link
                                 under "Faculty
                                 Tools" on the Faculty tab.
                                 
                              </li>
                              
                              <li>Or you can visit us in 11-142 for a paper form.</li>
                              
                           </ul>
                           
                           
                           <p>Any Valencia faculty member can use the Testing Center to provide proctoring for makeup
                              tests for
                              individual students, special accommodations tests for students with disabilities,
                              and tests for hybrid or
                              online courses. Some of the types of tests proctored are:
                           </p>
                           
                           <ul class="list_style_1">
                              
                              <li>traditional paper tests</li>
                              
                              <li>computer graded multiple choice tests (ScanTron)</li>
                              
                              <li>online exams through Blackboard</li>
                              
                              <li>other online exams (e.g. Excel)</li>
                              
                           </ul>
                           
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 16 campusID = 3 department_fields = "none" campus_fields = "none"
                        office_fields = "location,hours,phone,website"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Bring Your ID!</h3>
                        
                        
                        <p>A current Valencia ID, UCF ID, or state driver's license is required.</p>
                        <img class="img-responsive center-block" src="/_resources/img/students/testing/west/student-id.png" alt="Valencia College Student Photo ID"> <br> <img class="img-responsive center-block" src="/_resources/img/students/testing/west/ucf-id.gif" alt="UCF Student Photo ID">
                        
                     </div>
                     
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/testing/west/faqs.pcf">©</a>
      </div>
   </body>
</html>