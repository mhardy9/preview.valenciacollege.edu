<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>West Campus Testing Center  | Valencia College</title>
      <meta name="Description" content="FIXME">
      <meta name="Keywords" content="testing, center, west, campus, learning, support, college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/testing/west/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/offices-services/testing/west/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1></h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/testing/">Testing</a></li>
               <li>West</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin_60">
            <div class="row">
               	
               		
               <div class="row">
                  
                  <div class="col-md-9">
                     
                     <div class="box_style_1">
                        
                        <h2>West Campus Testing Center</h2>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>About the Testing Center</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Valencia's West Campus Testing Center is available to students for:</p>
                           
                           <ul class="list_style_1">
                              
                              <li>Make-up exams arranged with a professor</li>
                              
                              <li>Online</li>
                              
                              <li>Special accommodations testing arranged with a professor</li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Important Reminders for Students</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <p>Before arriving at the testing center, please keep in mind:</p>
                           
                           
                           
                           <ul class="list_style_1">
                              
                              <li>You must have a current Valencia photo ID, a current UCF ID, or a state driver's license
                                 to take an
                                 exam in the Testing Center.
                                 
                              </li>
                              
                              <li>Please know your instructor's last name, and which exam(s) are being taken.</li>
                              
                              <li>The last exam is given <strong>one hour prior to closing</strong>. Students who begin a test no later
                                 than one hour prior to closing may remain in the Testing Center only until closing
                                 time to complete the
                                 test or until the instructor’s specified time limit has been reached, whichever comes
                                 first.
                                 
                              </li>
                              
                              <li>
                                 <strong>ALL STUDENTS MUST BE DONE</strong> with their test(s) at the time the Testing Center closes!
                                 <strong>NO EXCEPTIONS!</strong> Refusal to follow this rule will result in a staff member of the Testing
                                 Center contacting the professor and reporting the student’s behavior!
                                 
                              </li>
                              
                              <li>PERT, CPT, Compass, CJBAT, and TEAS tests are proctored in the <a href="https://valenciacollege.edu/assessments/">Assessment Center</a>, located in SSB-171.
                                 
                              </li>
                              
                           </ul>
                           
                           
                           <p><strong>PLEASE NOTE</strong>: Individual proctoring of non-Valencia tests is not arranged through the
                              Testing Center. It is arranged through <a href="https://preview.valenciacollege.edu/continuing-education/">Continuing
                                 Education</a>.
                           </p>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>For Faculty</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              
                              
                              <li>Tests can be uploaded through Atlas. Click the "Testing Center Referral Form" link
                                 under "Faculty
                                 Tools" on the Faculty tab.
                                 
                              </li>
                              
                              <li>During our peak testing periods, we may be unable to answer the phones, so please
                                 contact the Testing
                                 Center staff at <a href="mailto:wec-testingcenter@valenciacollege.edu">wec-testingcenter@valenciacollege.edu</a>.
                                 
                              </li>
                              
                              <li>Referral forms may be picked up and filled out in the Testing Center when exams are
                                 submitted in
                                 person.
                                 
                              </li>
                              
                           </ul>
                           
                        </div>
                        
                        <hr class="styled_2">
                        
                        
                        <div class="indent_title_in">
                           
                           <h3>Closures/Holidays for 2017</h3>
                           
                        </div>
                        
                        <div class="wrapper_indent">
                           
                           <ul class="list_style_1">
                              
                              <li>Monday, January 16 (Martin Luther King, Jr. Day)</li>
                              
                              <li>Friday, February 10 (Learning Day)</li>
                              
                              <li>Monday, March 13 through Sunday, March 19 (Spring Break)</li>
                              
                              <li>Monday, May 29 (Memorial Day)</li>
                              
                              <li>Tuesday, July 4 (Independence Day)</li>
                              
                              <li>Monday, September 4 (Labor Day)</li>
                              
                              <li>Thursday, October 12 (College Night)</li>
                              
                              <li>Wednesday, November 22 through Sunday, November 26 (Thanksgiving)</li>
                              
                              <li>Thursday, December 21 through Monday, January 1 (Winter Break)</li>
                              
                           </ul>
                           
                        </div>
                        
                        
                        
                     </div>
                     
                  </div>
                  
                  
                  <aside class="col-md-3">
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Location</h3>
                        
                        departmentID = 16 campusID = 3 department_fields = "none" campus_fields = "none"
                        office_fields = "location,hours,phone,website"
                        
                        
                     </div>
                     
                     <hr class="styled">
                     
                     
                     <div class="box_side">
                        
                        <h3 class="add_bottom_30">Bring Your ID!</h3>
                        
                        
                        <p>A current Valencia ID, UCF ID, or state driver's license is required.</p>
                        <img class="img-responsive center-block" src="/_resources/img/students/testing/west/student-id.png" alt="Valencia College Student Photo ID"> <br> <img class="img-responsive center-block" src="/_resources/img/students/testing/west/ucf-id.gif" alt="UCF Student Photo ID">
                        
                     </div>
                     
                     
                     
                  </aside>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/testing/west/index.pcf">©</a>
      </div>
   </body>
</html>