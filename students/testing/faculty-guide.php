<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Faculty Guide | Testing Center | Valencia College</title>
      <meta name="Description" content="Faculty Guide | Testing Center">
      <meta name="Keywords" content="college, school, educational, testing, center, faculty, guide">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/testing/faculty-guide.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/testing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/testing/">Testing</a></li>
               <li>Faculty Guide</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <h2>Faculty Guide</h2>
                        
                        
                        <p>Any Valencia instructor can use the Testing Center to provide proctoing for their
                           exams. 
                        </p>
                        
                        <p>The easiest way to submit a test is through the online Testing Center Exam Referral
                           Form in 
                           <a href="https://atlas.valenciacollege.edu">Atlas</a>. By using the online form, an instructor may submit referrals to multiple Testing
                           Centers. 
                        </p>
                        
                        <p>Referral Forms may also be picked up and filled out in the Testing Center and submitted
                           in person.
                        </p>
                        
                        <h3>Things to Know</h3>
                        
                        <ul>
                           
                           <li>Please allow 48 hours for the Testing Center to process your referral during the semester
                              and 7 days prior to finals week.
                           </li>
                           
                           <li>The Testing Center strictly follows the deadline listed on the Referral Form. The
                              Testing Center must be notified by the instructor to extend the deadline.
                           </li>
                           
                           <li>Please be specific on the materials allowed for the exam. We will not allow students
                              to use materials unless it is specifically stated on the Referral Form.
                           </li>
                           
                           <li>The Testing Center can hold tests for pick up or return them by inter-office mail.</li>
                           
                           <li>Some Testing Centers provide additional services for instructors. Please contact each
                              campus locations for questions about additional services.
                           </li>
                           
                        </ul>
                        
                        <h3>Tell Your Students</h3>
                        
                        <ul>
                           
                           <li>Each Testing Center has its own policy regarding acceptable identification for testing-taking.
                              Please check with the Testing Center you plan to test with before arriving for your
                              exam. 
                           </li>
                           
                           <li>Testing Center hours vary between campuses and are subject to change. Hours for each
                              campus location are available on the Testing Center websites listed <a href="mailto:http://valenciacollege.edu/testing-center/">here</a>. 
                           </li>
                           
                        </ul>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <strong>Each Testing Center has its own policy regarding acceptable identification for test-taking.
                           Please check the center you plan to test with before arriving for your exam.</strong>
                        
                        
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/testing/faculty-guide.pcf">©</a>
      </div>
   </body>
</html>