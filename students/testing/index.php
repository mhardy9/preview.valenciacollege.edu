<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Testing Center | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/testing/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/testing/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Testing Center</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Testing</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        <a name="content" id="content"></a>
                        
                        
                        
                        
                        
                        
                        <p>Testing Centers are available on each campus for you to take exams. Students are welcome
                           to visit any location they wish, <u>IF</u> their professor has allowed them the option to test at any campus. 
                        </p>
                        
                        <p>For information about New Student Intake Reviews, PERT, TEAS, CJBAT or CLEP, please
                           visit the <a href="../../../assessments/index.html">Assessment Center</a>.
                        </p>
                        
                        
                        <h3>Before arriving at the Testing Center you will need to...</h3>
                        
                        
                        <ul>
                           
                           <li>
                              <strong>bring your  student ID</strong>- Each testing center has their own policy regarding acceptable identification to
                              test taking. Please check the center you plan to test with before arriving for your
                              exam. 
                           </li>
                           
                           <li>know the <strong>deadline for your exam</strong>
                              
                           </li>
                           
                           <li>know your <strong>course name</strong>
                              
                           </li>
                           
                           <li>know your<strong> instructor's last name</strong>
                              
                           </li>
                           
                           <li>know which <strong>campus locations</strong> your exam is available
                           </li>
                           
                        </ul>
                        <br>
                        
                        <h3>Locations, Contact &amp; Hours</h3>
                        
                        <p><strong>Last exam is given one hour before closing.</strong></p>
                        
                        <div>
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           
                           <div>
                              
                              
                              <div>
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../../../east/academic-success/testing/index.html">
                                             East Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4 , Rm 124</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-2428</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:eac-testingcenter@valenciacollege.edu">eac-testingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday: 7am - 9pm<br>Friday: 7am - 6pm<br>Saturday: 8am - 3pm<br>Sunday: Closed <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../lakenona/testingcenter.html">
                                             Lake Nona Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 206 </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-7104 </div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:lnc_testingcenter@valenciacollege.edu">lnc_testingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>We STOP proctoring tests one hour prior to closing time<br>Monday - Thursday: 8:00am - 9:00pm<br>Friday: 9:00am - 5:00pm<br>Saturday: 8:00am - 1:00pm<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../../../osceola/testing/index.html">
                                             Osceola Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 4, Rm 248</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-4149 </div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:osc-testingcenter@valenciacollege.edu">osc-testingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Monday - Thursday 8:00am - 9:00pm<br>Tests will not be given out after 8 pm<br>Friday 8:00am - 5:00pm<br>Tests will not be given out after 4:00pm<br>Saturday 8am-12pm<br>Tests will not be given out after 11 am<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../../../poinciana/index.html">
                                             Poinciana Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Room 325</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-6052</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:testingcenterpnc@valenciacollege.edu">testingcenterpnc@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>Mon-Thurs:  8am-7pm <br>
                                       <em>(All tests and assessments cease  administration at 6pm)</em><br>
                                       Friday: 8am-5pm <br>
                                       <em>(All tests and assessments cease  administration at 4pm)</em><br>
                                       Saturday:  Closed<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../../../learning-support/testing/index.html">
                                             West Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 11, Rm 142 </div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-1323 </div>
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div>Monday-Thursday 8 am-9 pm<br>Friday: 8 am-12 pm<br>Saturday: 9 am-2 pm<br>Sunday: Closed<br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                                 <div>
                                    
                                    <div><strong>
                                          
                                          <a href="../../../wp/testing.html">
                                             Winter Park Campus 
                                             </a>
                                          
                                          </strong></div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>Bldg 1, Rm 104</div>
                                    
                                 </div>
                                 
                                 <div>
                                    
                                    <div>407-582-6086</div>
                                    
                                 </div>
                                 
                                 <div> 
                                    
                                    <div>
                                       <a href="mailto:wpc-testingcenter@valenciacollege.edu">wpc-testingcenter@valenciacollege.edu</a>
                                       
                                    </div>
                                    
                                    
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    <div>
                                       <br>Monday - Thursday: 8 am - 6 pm
                                       <br> Course Exams will NOT be given out after 5pm.
                                       <br> Friday: 8 am - 5 pm
                                       <br> Course Exams will NOT be given out after 4 pm.
                                       <br>Saturday and Sunday: Closed
                                       <br>
                                       <br>The Testing Center will STOP giving tests one hour prior to closing.
                                       <br>
                                       <br>
                                       
                                    </div>
                                    
                                 </div>
                                 
                                 
                                 <div>
                                    
                                    
                                 </div>
                                 
                                 
                                 
                              </div>
                              
                           </div>
                           
                           
                        </div>
                        
                        
                        
                        
                        
                     </div>
                     
                     <aside class="col-md-3">
                        
                        
                        <strong>Each Testing Center has its own policy regarding acceptable identification for test-taking.
                           Please check the center you plan to test with before arriving for your exam.</strong>
                        
                        
                        
                        
                        <h3>College Closed Dates</h3>
                        
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/thanksgivingbreak" target="_blank"><strong>Thanksgiving Holiday</strong></a><br>
                           November 22, 2017 
                           - November 26, 2017 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/winterbreak" target="_blank"><strong>Winter Break</strong></a><br>
                           December 21, 2017 
                           - January 1, 2018 
                           
                        </div>
                        
                        
                        <div>
                           <a href="http://events.valenciacollege.edu/event/springbreak" target="_blank"><strong>Spring Break</strong></a><br>
                           March 12, 2018 
                           - March 18, 2018 
                           
                        </div>
                        
                        
                     </aside>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/testing/index.pcf">©</a>
      </div>
   </body>
</html>