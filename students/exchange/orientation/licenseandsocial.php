<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/orientation/licenseandsocial.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/orientation/">Orientation</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Driver's License &amp; Social Security Card </h2>
                        
                        <h3>Driver's License</h3>
                        
                        <p>You will need a Florida Driver’s License issued by the Department of Motor Vehicles
                           (DMV) to drive a car. To obtain a license, you must pass a written test and a driving
                           test. You will need the following in order to apply for a license: DS-2019, visa,
                           passport, I-94 card, and Social Security number (if issued). Other services provided
                           by the Department of Motor Vehicle (DMV) include: issuance of Florida ID card, license
                           renewals, license plate renewals, getting a learner’s permit, and finding a Florida
                           DMV-approved traffic school. 
                        </p>
                        
                        <p>NOTE: Florida law defines a bicycle as a vehicle so bicyclists are subject to many
                           of the same responsibilities as operators of motor vehicles and subject to the same
                           citations and fines for violating traffic law. For more information, visit: <a href="http://dmvflorida.com/">Division of Motor Vehicles. </a></p>
                        
                        <h3>Valencia Parking Permit</h3>
                        
                        <p>If you will have a car, be sure to request a Valencia parking permit. You do this
                           through your Atlas account. Click on Parking Permit Application and then Request a
                           Permit. Follow the instructions on the page. 
                        </p>
                        
                        <h3>Social Security Card </h3>
                        
                        <p>Exchange Visitors who are receiving program sponsor funds, as indicated on the DS-2019,
                           are eligible to apply for a Social Security card. You MUST wait at least ten days
                           after entering the U.S. before visiting the local Social Security Administration (SSA)
                           office or the Social Security card will be delayed. The SSA will not issue a social
                           security number to an individual who will not reside in the U.S. at least 14 days
                           beyond the date that the application for a Social Security number is submitted to
                           the SSA office.  You must apply in person at a local Social Security office and you
                           will need to bring your passport, I-94 Card, and DS-2019.  In order to start working,
                           you must present your Social Security Card to your Responsible Officer and your employer.
                           Typically, there is a two- to four-week wait for Social Security cards. For more information,
                           visit the <a href="http://www.ssa.gov">Social Security Administration</a>. 
                        </p>
                        
                        <p> <span>Exception to above</span>: Short-term scholars who will earn income from Valencia and will reside in the U.S.
                           for at least 15 days but not more than 29 days should not wait 10 days. They must
                           visit an SSA office immediately after arriving in the U.S. and following validation
                           in SEVIS.<br>
                           <br>
                           
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/orientation/licenseandsocial.pcf">©</a>
      </div>
   </body>
</html>