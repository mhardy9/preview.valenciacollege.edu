<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/orientation/employment.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/orientation/">Orientation</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div id="google_translate_element"></div>    
                        
                        
                        
                        <h2>Academic Training &amp; Employment</h2>
                        
                        <h3>Academic Training </h3>
                        
                        <p>Academic Training is a work program where students may or may not get paid (depending
                           on the employer options). The work must directly contribute to the student's program
                           of study. In order to be eligible for academic training, you must meet the following
                           criteria:
                        </p>
                        
                        <ul>
                           
                           <li>                 Be in good academic standing.</li>
                           
                           <li>Continue to maintain a full course of study throughout your academic training.</li>
                           
                           <li>May be paid or non-paid.</li>
                           
                           <li>Must be approved in advance by the RO/ARO.</li>
                           
                           <li>Can occur during your studies or no later than 30 days after completion of studies
                              CFR 22, 62.73.
                           </li>
                           
                           <li>Must be directly related to your major field of study as listed on the DS-2019.</li>
                           
                           <li>Cannot exceed a period of 18 months.</li>
                           
                           <li>You must consult with your RO/ARO and get approval for any employment.</li>
                           
                           <li>You must present your RO/ARO with an original job offer letter and ob-tain a Social
                              Security Card.
                           </li>
                           
                           <li>You will need to complete a W-4 Form and I-9 Form with your employer.</li>
                           
                           <li>Should you lose your academic training with your employer, you must either add more
                              course(s) or get another academic training. 
                           </li>
                           
                        </ul>             
                        
                        <p><strong>Degree-Seeking Students:</strong> Participants can apply to Valencia's <a href="../../../internship/index.html">Internship Program</a>. They must  pay the  cost of the 3-credit intern course per term associated with
                           this program.
                        </p>
                        
                        <p><strong>Exchange Students: </strong>Academic training programs are not available to students studying at Valencia for
                           less than one year. 
                        </p>
                        
                        <p><strong>Nondegree-Seeking Students: </strong>See the <a href="../collegeprogram/index.html"> International College Program at Walt Disney World</a> for more information.
                        </p>
                        
                        <h3>Employment</h3>
                        
                        <p> Students may engage in part-time employment under certain conditions, including good
                           academic standing at their host institution. On-campus employment is allowed if it
                           is pursuant to the terms of a scholarship, and if it occurs on the premises of the
                           school. The Exchange Visitor must be in good academic standing and continue to engage
                           in a full course of study. Employment must be approved in advance by the RO/ARO (CFR
                           22, 62.20) and not be more than 20 hours per week. For Professors, an occasional lecture
                           or consultation is allowed if it is directly related to the program objective and
                           is approved in advance by the RO/ARO.
                        </p>
                        
                        <p>When in school or at work, be sure you understand U.S. laws and institutional/organizational
                           policies as they relate to the following:
                        </p>
                        
                        <ul>
                           
                           <li> Work/School attendance</li>
                           
                           <li>Smoking</li>
                           
                           <li>Use of intoxicants</li>
                           
                           <li>Possession of illegal substances</li>
                           
                           <li>Discrimination</li>
                           
                           <li>Public displays of affection</li>
                           
                           <li>Sexual harassment</li>
                           
                           <li>Gift-giving or receiving</li>
                           
                        </ul>             
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/orientation/employment.pcf">©</a>
      </div>
   </body>
</html>