<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/orientation/visastatus.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/orientation/">Orientation</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Maintaining Your Visa Status </h2>
                        
                        <h3>Maintaining Status</h3>
                        
                        <p>The U. S. Department of State considers an Exchange Visitor to be in “valid program
                           status” only when he or she engages in approved J-1 activities, regardless of the
                           end date on the DS-2019 form.&nbsp; This means that you must actively and successfully
                           participate in all of the required academic and training activities of your program.&nbsp;
                           If you fail participate in some or all of these activities, you will no longer be
                           in valid program status, which means that Valencia may have to terminate your program.&nbsp;
                           Responsibilities include:
                        </p>
                        
                        <ul>
                           
                           <li> Obtain validation of your SEVIS record after checking in with your RO/ARO upon arrival.<br>
                              <br>
                              
                           </li>
                           
                           <li> Retain required documentation at all times which include a valid DS-2019, I-94 card,
                              and valid passport during the entire length of the program.<br>
                              <br>
                              
                           </li>
                           
                           <li> Engage only in appropriate activities permitted, specifically in Section 4 of the
                              DS-2019.<br>
                              <br>
                              
                           </li>
                           
                           <li> Report address changes to your assigned RO/ARO within 10 days of the move date.<br>
                              <br>
                              
                           </li>
                           
                           <li> Maintain the required sickness and injury insurance coverage for the entire program
                              period (including program extensions). You must provide proof of coverage for yourself
                              and any dependents in J-2 status.<br>
                              <br>
                              
                           </li>
                           
                           <li> Comply with employment guidelines and refrain from any unauthorized employment. All
                              employment activity that is not included in Part 4 on the DS-2019 must be approved
                              in writing by the RO/ARO before the activity begins. Students may only work at the
                              <br>
                              designated internship site and be “in good standing” with their employer.<br>
                              <br>
                              
                           </li>
                           
                           <li> Comply with all academic program guidelines and acceptable standards of student conduct.
                              This includes attendance requirements for each course.<br>
                              <br>
                              
                           </li>
                           
                           <li> Report any program changes to the RO/ARO in advance.<br>
                              <br>
                              
                           </li>
                           
                           <li> Obtain a travel signature on the DS-2019 from the RO/ARO prior to departing the United
                              States anytime during your program duration. Please note that students may not be
                              allowed to re-enter the U.S. without travel authorization.<br>
                              <br>
                              
                           </li>
                           
                           <li> File timely and appropriate school transfer or program extension paperwork with the
                              appropriate staff member.<br>
                              <br>
                              
                           </li>
                           
                           <li>Report your departure date and reason to the RO/ARO in advance. You must depart the
                              United States within 30 days of completing or ceasing program activities. Overstaying
                              the 30 days is a serious immigration violation that may negatively affect your ability
                              to obtain a new visa or re-enter the U.S. in the future.
                           </li>
                           
                        </ul>             
                        
                        <h3>Travel and Re-Entry</h3>
                        
                        <p>You can travel outside the U.S. and then request re-entry to continue your J program.
                           You will need a valid passport, a DS-2019 travel signature by an RO/ARO, and proof
                           of current injury and sickness insurance.  If the J visa stamp is expired, the Exchange
                           Visitor must obtain a new visa stamp before entry to the U.S. will be granted. For
                           travel to a country other than the visitor’s home country, Exc hange Visitors should
                           contact the consulate or embassy of that country for entry requirements and procedures.
                           Travel must be completed before the end date on the DS-2019.
                        </p>
                        
                        <h3>Program Termination</h3>
                        
                        <p>An Exchange Visitor can be terminated from the program for any of the following reasons:
                           fails to report to the College, is unable to attend classes, violates any of the program
                           or college's regulations, fails to maintain health insurance coverage, or engages
                           in authorized employment. The Exchange Visitor must depart the U.S. immediately upon
                           termination. Individuals who are terminated are ineligible for an extension of stay
                           or in-country change of status and may be deported.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/orientation/visastatus.pcf">©</a>
      </div>
   </body>
</html>