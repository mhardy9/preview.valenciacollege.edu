<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/orientation/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li>Orientation</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div id="google_translate_element"></div>
                        
                        <h2>J Exchange Visitor Program Orientation </h2>
                        
                        <p>This orientation information outlines the responsibilities for a J-1 visa holder.
                           This is very important information that Exchange Visitors must know in order to maintain
                           their visa status. Click on the links to the left for more details. 
                        </p>
                        
                        
                        <h3><strong>Valencia College Contacts </strong></h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Student Exchange Programs</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p>Ms. Bliss Thompson<br>
                                       International Counselor<br>
                                       West Campus<br> 
                                       1800 South Kirkman Rd.<br>
                                       Student Services Building, office 146<br>
                                       407-582-5830<br>
                                       <a href="mailto:bthompson@valenciacollege.edu">bthompson@valenciacollege.edu</a><br>
                                       <br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong> Professor &amp; Scholar Exchange Programs</strong></p>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">Ms. Jennifer Robertson <br> 
                                    Director, Study Abroad &amp; Global Experiences <br> 
                                    Downtown Center
                                    <br>
                                    190 South Orange Ave., 2nd Floor <br>
                                    407-582-3188<br>
                                    <a href="mailto:jrobertson@valenciacollege.edu">jrobertson@valenciacollege.edu</a>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>International College Program at Walt Disney World Resort</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <p>Ms. Lisa Eli<br>
                                       Director, Continuing/International Education<br>
                                       West Campus<br>
                                       1800 S Kirkman Road, Building 10<br>
                                       407-582-6649<br>
                                       <a href="mailto:leli@valenciacollege.edu">leli@valenciacollege.edu</a><br>
                                       <br>
                                       
                                    </p>
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>             
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/orientation/index.pcf">©</a>
      </div>
   </body>
</html>