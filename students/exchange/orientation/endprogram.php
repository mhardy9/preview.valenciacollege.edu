<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/orientation/endprogram.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/orientation/">Orientation</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Ending or Extending Your Program </h2>
                        
                        <h3>Ending Your Program </h3>
                        
                        <p>Exchange Visitors who end their program more than 30 days prior to the end date on
                           the DS-2019 and intend to depart the U.S. and not return must notify the sponsoring
                           department and their RO/ARO.
                        </p>
                        
                        <h3>Program Extensions </h3>
                        
                        <p>Program extensions are obtained through the sponsoring department and must be submitted
                           at least 30 days before the program end date. The counting time in J status BEGINS
                           from either the begin date on the DS-2019 OR the arrival date on the I-94 card. The
                           counting time in J status ENDS when you reach the end date on the DS-2019 OR you reach
                           the maximum limit of your category OR you complete your program objective.
                        </p>
                        
                        <ul>
                           
                           <li>A STUDENT is limited to 24 months in a degree program or 36 months in a nondegree
                              program. If the student is doing academic training, this can be done after the participant’s
                              studies, but not to extend the total program duration. <br>
                              <br>
                              
                           </li>
                           
                           <li>A PROFESSOR can be sponsored for a maximum period of five years. For example, if the
                              initial program is scheduled for one year, program extensions may be requested for
                              a total of five years.<br>
                              <br>
                              
                           </li>
                           
                           <li>A SHORT-TERM SCHOLAR is limited to six months in duration. Extensions are not granted
                              beyond that time.<br>
                              <br>
                              
                           </li>
                           
                           <li>A SPECIALIST can be sponsored for a maximum period of one year. Extensions are not
                              granted beyond that time.<br>
                              
                           </li>
                           
                        </ul>            
                        
                        <h3>Program Transfers </h3>
                        
                        <p>An EV who desires to transfer his or her program must consult with their sponsoring
                           department and RO/ARO, complete the Transfer Out Form, and submit it no later than
                           30 days before the desired transfer date. Requirements for program transfer include:
                        </p>
                        
                        <ul>
                           
                           <li> Program sponsor is approved by Department of State (DoS) to sponsor the same J Exchange
                              Visitor category;<br>
                              Program objective/activity remains unchanged;<br>
                              Program period will not exceed maximum DoS established period;<br>
                              Exchange Visitor checks in with the new sponsor immediately following transfer date.
                           </li>
                           
                        </ul>            
                        
                        <h3><strong>Two-Year Residency Requirement 212(e)</strong></h3>
                        
                        <p><strong>             </strong>Some Exchange Visitors are subject to the requirement known as 212(e). The U.S. embassy
                           or consulate determines if an Exchange Visitor is subject to the 212(e) rule.  The
                           U. S. Consulate officer will make a preliminary endorsement in the lower left corner
                           of your DS-2019 form if you are subject to 212(e).&nbsp; The U. S. Department of State
                           reserves the right to make the final determination regarding 212(e). 
                        </p>
                        
                        <p>Those who are subject to the 212(e) rule must return to their home country, or last
                           country of residence, and reside in that country for two years before they become
                           eligible for H, L, or permanent resident status in the United States, and they are
                           are not permitted to change their nonimmigrant status within the U.S. from J to any
                           other nonimmigrant category except A (diplomatic) and G (international organization)
                           statuses. Marriage to a U. S. citizen or legal permanent resident or birth of a child
                           in the U. S. does not remove this requirement.&nbsp;<br>
                           <br>
                           Exchange Visitors may be subject to this requirement if their skill category appears
                           on the applicable country’s “list of skills” that are in critical demand (listings
                           of skill categories for applicable countries are located in the Federal Register)
                           and/or the Exchange Visitor's program is supported financially by a government or
                           international agency.  <br>
                           <br>
                           A waiver of the two-year home-country physical-presence requirement is possible. Waivers
                           are permitted by the DoS and granted by U.S. Citizenship and Immigration Services
                           (USCIS).<br>
                           <br>
                           <br>
                           <br>
                           
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/orientation/endprogram.pcf">©</a>
      </div>
   </body>
</html>