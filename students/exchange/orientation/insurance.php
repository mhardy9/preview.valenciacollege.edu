<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/orientation/insurance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/orientation/">Orientation</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Insurance &amp; Healthcare</h2>
                        
                        <h3>
                           <strong>Medical Insurance</strong> 
                        </h3>
                        
                        <p>The U. S. Department of State requires all Exchange Visitors to have medical insurance,
                           provided by an approved insurance company, which covers them for sickness and accidents
                           throughout their program duration.&nbsp; Valencia requires all international students to
                           purchase medical insurance through the college to ensure that the U.S. Department
                           of State insurance coverage requirements are met. Even with health insurance, you
                           must pay a portion of every medical bill called a "co-pay" or a “deductible.” Certain
                           medical conditions and procedures are not covered at all, including treatment for
                           illnesses or conditions you had before coming to the U. S.&nbsp; These are known as “pre-existing
                           conditions.”&nbsp; It is very important to read your insurance policy carefully before
                           you need to use it so that you understand what costs your health insurance will cover.&nbsp;
                           
                        </p>
                        
                        <h3>Healthcare</h3>
                        
                        <p>Health care in the U. S. is complicated, even for U. S. residents.&nbsp; The American health
                           care system is different from many other countries, where the government may pay all
                           or part of an individual's health care costs.&nbsp; Medical care is very expensive in the
                           U.S., and people are expected to pay for it themselves.&nbsp; Even a few days in the hospital
                           can cost thousands of dollars.&nbsp; There will be separate bills from the hospital, from
                           every doctor who examines the patient, for every laboratory procedure, and for all
                           medicines.&nbsp; Americans purchase health insurance to protect them from these high health
                           care costs, and all Exchange Visitors are required to have it.&nbsp; Do not view health
                           insurance as an additional, unnecessary expense!&nbsp; Without it you would have to pay
                           thousands of dollars in medical bills if you had a serious accident or illness that
                           required hospitalization.
                        </p>
                        
                        <p>In the U. S. it is customary to ask your doctor questions about your diagnosis and
                           treatment.&nbsp; You may want to have a friend accompany you to a medical appointment in
                           order to help listen and ask questions, assist with translation (if necessary), and
                           provide overall support. Many medical offices and pharmacies in the Orlando area have
                           Spanish speakers on staff and sometimes they can assist speakers of other languages.&nbsp;
                           So do not be afraid to ask if someone there can talk with you in your native language
                           so that you can ask questions and make sure you understand everything they tell you.
                        </p>
                        
                        
                        <h3>Where to Get Medical Care </h3>
                        
                        <p>For most illnesses and injuries, people go to a walk-in clinic, where an appointment
                           is not necessary.&nbsp; These clinics are staffed by fully qualified doctors and nurses
                           who  treat problems such as sore throats; coughs; cold or flu symptoms; eye, ear,
                           or skin infections; nausea and vomiting; sprains and fractures; urinary problems;
                           and body aches and pains.&nbsp; X-rays and other diagnostic procedures are often done on
                           the premises.&nbsp; Walk-in clinics are also known as “urgent care centers,” but they should
                           not be confused with a hospital emergency room.&nbsp; 
                        </p>
                        
                        <p>A hospital emergency room is the most expensive place to obtain medical care.&nbsp; Your
                           insurance is likely to pay for it only if you have a serious, life-threatening medical
                           emergency such as a  traumatic injury, breathing problems, high fever, serious burns,
                           or if you have an urgent need during hours when walk-in clinics are closed.&nbsp; The waiting
                           time for nonemergency care in a hospital emergency room can be very long, sometimes
                           up to eight hours.&nbsp;<strong>In the event of an emergency, dial 911. </strong> 
                        </p>
                        
                        <p>Use an ambulance to get to a hospital only if emergency medical procedures may be
                           needed on the way.&nbsp; For serious but non-life-threatening health problems, use a less
                           expensive form of transportation such as a taxi.&nbsp; Ambulance transportation costs hundreds
                           of dollars.&nbsp; Your insurance will only cover part of the ambulance fee, and even then
                           only if the ambulance was needed for a true medical emergency.
                        </p>
                        
                        <h3>Medical Costs </h3>
                        
                        <p>In order to keep the total cost of  insurance as low as possible, your insurance is
                           intended to protect you against big illnesses or injuries that would cost you thousands
                           of dollars for hospital care.&nbsp; Therefore, for each injury or sickness, you are responsible
                           for the first $100 of fees to each medical provider.&nbsp; For example, let’s say you have
                           a cough and you go to a clinic that charges $226 for a basic visit.&nbsp; You may be expected
                           to pay the entire amount and then submit a claim to the insurance company.&nbsp; In this
                           case, insurance should reimburse you $126 of the $226.&nbsp; If you return a few days later
                           for a follow-up visit about your cough, this time the insurance should reimburse you
                           the whole $226.&nbsp; On the other hand, if you return to the same clinic the next week
                           for a stomach ache, this is a new illness and you will  be reimbursed only $126 of
                           the $226 fee.&nbsp; 
                        </p>
                        
                        <p>In the above example, if you went to a clinic that charged $69, you would pay that
                           amount to the clinic.&nbsp; Insurance would not reimburse you because you are responsible
                           for the first $100 for each injury or sickness.&nbsp; However, you should still file a
                           claim to the insurance company for this visit, because if you had to return to the
                           clinic for a follow-up appointment, the insurance company would know that you had
                           already paid $69 of your $100 deductible for that illness.
                        </p>
                        
                        <p>Be prepared to pay the entire bill with cash or a credit card any time that you receive
                           medical care or buy prescription medicine.&nbsp; Bring your<strong> health insurance ID card</strong> with you, because sometimes the medical provider will call the insurance company
                           to verify that you have insurance.&nbsp; In that case, the medical provider may bill the
                           insurance company for their portion of the cost and charge you just for your part.&nbsp;
                           Get a detailed statement of your bill before you leave the medical office; you will
                           need this to submit your claim to the insurance company.
                        </p>
                        
                        <p>Please note that only “medically necessary” expenses are covered.&nbsp; Routine physical
                           exams and dental care are not covered,  but there is coverage for emergency dental
                           treatments.&nbsp; Eye examinations for the purpose of prescribing eyeglasses or contact
                           lenses are not covered, unless they are needed due to an accidental injury that occurred
                           while covered by insurance.&nbsp; 
                        </p>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/orientation/insurance.pcf">©</a>
      </div>
   </body>
</html>