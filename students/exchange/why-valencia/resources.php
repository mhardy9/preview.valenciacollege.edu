<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/why-valencia/resources.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/why-valencia/">Whyvalencia</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Community Resources </h2>
                        
                        <p>The following is a list of resources that you will find helpful during your stay in
                           Orlando. 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Transportation</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li>
                                          <a href="http://www.orlandoairports.net/">Orlando International Airport</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://hsmv.state.fl.us/offices/orange.html">Division of Motor Vehicles</a> (for driver's license) 
                                       </li>
                                       
                                       <li><a href="http://www.golynx.com">Lynx Bus System</a></li>
                                       
                                       <li>
                                          <a href="http://www.mearstransportation.com/services/taxis.htm">Mears Transportation Services</a> (taxis) <br>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Insurance &amp; Medical Care </strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <ul>
                                       
                                       <li><a href="http://www.insuranceforstudents.com/">Insurance for Students</a></li>
                                       
                                       <li><a href="http://www.centracare.org/">Centra Care Walk-In Clinics</a></li>
                                       
                                       <li><a href="http://www.floridahospital.com/">Florida Hospital</a></li>
                                       
                                       <li>
                                          <a href="http://www.orlandohealth.com/OrlandoHealth/index.aspx">Orlando Health</a> (hospital system) <br>
                                          
                                       </li>
                                       
                                    </ul>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Public Services </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>
                                          <a href="http://www.ssa.gov/cardcenters/cardcenterinfo.html#orlando%23orlando">Social Security Administration</a> 
                                       </li>
                                       
                                       <li><a href="http://www.ocls.info/">Orange County Library System</a></li>
                                       
                                       <li>
                                          <a href="http://www.osceolalibrary.org/">Osceola Library</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.wppl.org/">Winter Park Library</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.usps.com/">U.S. Postal System</a>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Things to Do </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li><a href="http://www.visitorlando.com/events/">Orlando Calendar of Events</a></li>
                                       
                                       <li> <a href="http://www.wftv.com/icflorida/index.html">icFlorida.com </a>
                                          
                                       </li>
                                       
                                       <li><a href="http://www.visitflorida.com/events">VisitFlorida.com</a></li>
                                       
                                       <li><a href="http://www.visitorlando.com/shopping/">Shopping Malls &amp; Outlet Stores</a></li>
                                       
                                       <li>
                                          <a href="http://www.pointeorlando.com/">Point Orlando</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.orlandovenues.net/other_info_files/amway_arena.php">Amway Arena</a> <br>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Arts &amp; Entertainment </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li><a href="http://www.orlandosentinel.com/entertainment/">Arts &amp; Entertainment </a></li>
                                       
                                       <li><a href="http://www.bachfestivalflorida.org/">Bach Festival Society </a></li>
                                       
                                       <li><a href="http://orlando.about.com/od/orlandocalendar/Orlando_Events_Festivals_Shows.htm">Events, Festivals &amp; Shows </a></li>
                                       
                                       <li>
                                          <a href="http://www.blueman.com/">Blue Man Group</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.enzian.org/festivals/florida_film_festival/">Enzian Theater &amp; Florida Film Festival</a> 
                                       </li>
                                       
                                       <li><a href="http://www.cirquedusoleil.com/en/shows/lanouba/default.aspx">La Nouba by Cirque du Soleil </a></li>
                                       
                                       <li>
                                          <a href="http://orlandoshakes.org/">Orlando Shakespeare Theater</a> 
                                       </li>
                                       
                                       <li><a href="http://www.orlandoballet.org/">The Orlando Ballet </a></li>
                                       
                                       <li>
                                          <a href="http://orlandophil.org/">The Orlando Philharmonic</a> <br>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Theme Parks &amp; Attractions </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li><a href="http://buschgardens.com/bgt/">Busch Gardens</a></li>
                                       
                                       <li>
                                          <a href="http://www.gatorland.com/">Gatorland</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://disneyworld.disney.go.com/parks/epcot/special-events/epcot-international-flower-and-garden-festival/">International Flower and Garden Festival</a> 
                                       </li>
                                       
                                       <li><a href="http://www.nasa.gov/centers/kennedy/home/index.html">Kennedy Space Center </a></li>
                                       
                                       <li>
                                          <a href="http://myoldtownusa.com/home/">Old Town U.S.A.</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.cityoforlando.net/fpr/net/Index.aspx">Parks and Recreation</a> 
                                       </li>
                                       
                                       <li><a href="http://www.seaworld.com/orlando/">Sea World</a></li>
                                       
                                       <li><a href="http://www.longandscottfarms.com/">Scott's Corn Maze </a></li>
                                       
                                       <li>
                                          <a href="http://www.holylandexperience.com/">The Holy Land</a> 
                                       </li>
                                       
                                       <li><a href="http://www.universalorlando.com/OverviewPages/ThemeParks/themeparks.aspx">Universal Studios</a></li>
                                       
                                       <li><a href="http://disneyworld.disney.go.com/">Walt Disney World</a></li>
                                       
                                       <li><a href="http://www.weekiwachee.com/">Weeki Wachi Springs </a></li>
                                       
                                       <li>
                                          <a href="http://www.wetnwildorlando.com/">Wet n' Wild</a> <br>
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Sports and Recreation</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li><a href="http://www.floridaecosafaris.com/">Florida EcoSafaris</a></li>
                                       
                                       <li><a href="http://www.floridastateparks.org/">Florida State Parks </a></li>
                                       
                                       <li><a href="http://www.orlandocitysoccer.com/schedule/gameschedule/">Orlando City Soccer</a></li>
                                       
                                       <li><a href="http://www.nba.com/magic/index_main.html">Orlando Magic </a></li>
                                       
                                       <li><a href="http://ucfathletics.cstv.com/">University of Central Florida Athletics</a></li>
                                       
                                       <li><a href="http://www.scenicboattours.com/home.htm">Winter Park Scenic Boat Tour </a></li>
                                       
                                       <li>
                                          <a href="http://www.leugardens.org/">Leu Botanical Gardens </a><br> 
                                          
                                       </li>
                                       
                                    </ul>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Museums</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <ul>
                                       
                                       <li>
                                          <a href="http://polasek.org/">Albin Polasek Museum and Sculpture Gardens</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.morsemuseum.org/">Charles Hosmer Morse Museum of American Art</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.rollins.edu/cfam/">Cornell Fine Arts Museum</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.holocaustedu.org/">Holocaust Memorial Resource and Education Center</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.maitartctr.org/">Maitland Art Center</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.thehistorycenter.org/">Orange County Regional History Center</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.omart.org/">Orlando Museum of Art</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.osc.org/">Orlando Science Center</a> 
                                       </li>
                                       
                                       <li>
                                          <a href="http://www.zoranealehurstonmuseum.com/">Zora Neal Hurston National Museum of the Arts <br>
                                             </a>                  
                                       </li>
                                       
                                    </ul>
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/why-valencia/resources.pcf">©</a>
      </div>
   </body>
</html>