<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/why-valencia/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li>Whyvalencia</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Why Valencia</h2>
                        
                        <h3><strong>The Valencia Difference</strong></h3>
                        
                        <p>Valencia College is a premier learning college that transforms lives, strengthens
                           community, and inspires individuals to excellence.&nbsp; Located in Orlando, Florida, Valencia
                           is considered one of the country’s top colleges.&nbsp; There are many factors which contribute
                           to our success:
                        </p>
                        
                        <ul>
                           
                           <li>Most full-time faculty have a master’s or doctorate degree and all are experts within
                              their field. 
                           </li>
                           
                           <li>With smaller campuses and classes, students get more support during their studies.</li>
                           
                           <li>Valencia is a truly diverse community with more than 60 nations represented within
                              its student body.
                           </li>
                           
                           <li>Valencia’s retention rate of 86 percent is proof that the support we provide is helping
                              our students succeed.
                           </li>
                           
                           <li>Students get the same quality education available at a state university but at about
                              half the cost. 
                           </li>
                           
                        </ul>
                        
                        <h3><strong>Valencia Facts</strong></h3>
                        
                        <ul>
                           
                           <li>Valencia ranks first among the nation’s two-year colleges in the number of associate
                              degrees earned and fourth among all institutions.
                           </li>
                           
                           <li>95 percent of Valencia’s two-year graduates are placed in jobs after graduation earning
                              an average annual salary of $44,680.
                           </li>
                           
                           <li>Twice as many high school graduates in Orange and Osceola Counties start college at
                              Valencia than at all Florida state universities combined.
                           </li>
                           
                        </ul>
                        
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <a href="http://preview.valenciacollege.edu/future-students/why-valencia/student-life/">Student Life</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://preview.valenciacollege.edu/future-students/visit-valencia/">Visit Valencia</a> 
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <a href="http://preview.valenciacollege.edu/future-students/why-valencia/">Meet our Faculty</a> 
                                 </div>
                                 
                                 <div data-old-tag="td"><a href="http://preview.valenciacollege.edu/future-students/why-valencia/consumer-information/">Consumer Information </a></div>  
                              </div>
                              
                           </div>
                           
                        </div>          
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/why-valencia/index.pcf">©</a>
      </div>
   </body>
</html>