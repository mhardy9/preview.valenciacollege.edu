<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Exchange Visitor Program | Valencia College</title>
      <meta name="Description" content="Valencia College is an official Program Sponsor designated by the Department of State to administer J-1 visas to participate in the Exchange Visitor Program.">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li>Exchange</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div id="google_translate_element"></div>
                  
                  <h2>J  Exchange Visitor Program Purpose </h2>
                  
                  
                  <p><img alt="Meet Faculty" border="1" height="159" src="MeetFaculty.jpg" width="436">Valencia College is an official Program Sponsor designated by the Department of State
                     to administer J-1 visas to participate in the Exchange Visitor Program. This is an
                     academic program designed to foster mutual educational and cultural  exchanges of
                     ideas between Americans and foreign nationals, stimulate international collaborative
                     teaching and research efforts, promote  mutual enrichment, and create linkages between
                     research and educational institutions in the U.S. and those of other countries. Designations
                     and program durations are as follows: 
                  </p>
                  
                  <ul>
                     
                     <li>Student (up to 2 years)<br>
                        
                     </li>
                     
                     <li>Professor (3 weeks to 5 years)<br> 
                        
                     </li>
                     
                     <li>Short-Term Scholar (1 day to 6 months) <br>
                        
                     </li>
                     
                     <li>Specialist (3 weeks to 1 year) </li>
                     
                  </ul>                        
                  
                  <p>Applicants will qualify for the Exchange Visitor visa as long as they can demonstrate
                     the following:
                  </p>
                  
                  <ul>
                     
                     <li>They plan to remain in the U.S. for a temporary, specific, and limited period.</li>
                     
                     <li>They plan to carry out a specific activity, such as study or research.</li>
                     
                     <li>There is evidence of funds to cover expenses in the United States.</li>
                     
                     <li>They can show evidence of compelling social and economic ties abroad; and other binding
                        ties which will ensure their return abroad at the end of the visit.  
                     </li>
                     
                  </ul>                        
                  
                  
                  
               </div>
               
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/index.pcf">©</a>
      </div>
   </body>
</html>