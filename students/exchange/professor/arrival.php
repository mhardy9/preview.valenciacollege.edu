<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/professor/arrival.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/professor/">Professor</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Professors &amp; Scholars - Arrival Information </h2>
                        
                        <h3>First Day Arrival to Valencia College </h3>
                        
                        <p><img alt="Exchange Faculty" border="1" height="175" hspace="3" src="Faculty6.jpg" width="275">Exchange Visitors cannot arrive more than 30 days prior to their program start date,
                           but you should plan to arrive with a couple of days to get settled. On the first day
                           of your campus arrival, you will need to report to your Host Department contact if
                           you have not been in contact with him or her already.<br>
                           <br>
                           You will then need to report to Ms. Bliss Thompson, International Counselor and Responsible
                           Officer, at Valencia's West Campus, 1800 South Kirkman Rd., Student Services Building,
                           office 146. It is best to contact her in advance to request an appointment. The hours
                           are Monday-Thursday 9:00 a.m. to 5:00 p.m. and Friday 9:00 a.m. to 4:00 p.m. Phone:
                           407-582-5830 / Email: <a href="mailto:bthompson@valenciacollege.edu">bthompson@valenciacollege.edu</a>. Please bring the following documents when you check-in: 
                        </p>
                        
                        <ul>
                           
                           <li>Passport with J-1 visa </li>
                           
                           <li>I-94 card </li>
                           
                           <li>DS-2019 </li>
                           
                           <li>The documents above for all J-2 dependents </li>
                           
                           <li>Proof of insurance payment (if already purchased) </li>
                           
                        </ul>
                        
                        <h3>J Exchange Visitor Orientation </h3>
                        
                        <p>All J Exchange Visitors are required to attend a <a href="../jevorientation/index.html">J Exchange Visitor Orientation</a> session that covers specific information to this program.
                        </p>
                        
                        <h3>Emergency Messaging System</h3>
                        
                        <p>Upon arrival, be sure to sign up for Valencia's Emergency Messaging System. You do
                           this through your Atlas account. Find VALENCIA ALERT on your Atlas home page and click
                           the link to sign up for alerts. You can receive instant notification via email or
                           text message to your cell phone. 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/professor/arrival.pcf">©</a>
      </div>
   </body>
</html>