<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/professor/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/professor/">Professor</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Professors &amp; Scholars - How to Apply </h2>
                        
                        <h3>Application Instructions </h3>
                        
                        <p>The purpose of the Exchange Visitor Program for professors, short-term scholars, and
                           specialists is to share knowledge, information, ideas, and best practices as it relates
                           to methodology, curriculum design, and technology; and  to gain a greater understanding
                           and appreciation for each other's country and culture. <strong>In order to participate, you must be invited by a Valencia faculty or staff member</strong>. This person must fill out the following form for approval:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/JEVProgram_HostingDepartmentSign-OffForm_2012-05-15.pdf">JEV Hosting Department Sign-Off Form</a> 
                           </li>
                           
                        </ul>             
                        <p>All visiting professors, short-term scholars, and specialists must be hosted by a
                           Valencia department. The first step in this process is for the primary contact in
                           the host department to complete Valencia's internal application for exchange programs.
                           The program participation outcome will be some type of contribution to International
                           Education at Valencia.&nbsp; This could be in the form of modifying existing learning outcomes
                           in curricula, infusing global concepts into the curriculum, creating learning objects
                           to add to the curriculum, creating new courses related to international education,
                           or sharing international exchange experiences with peers college-wide. &nbsp;The following
                           packet includes a checklist on the steps to apply for and obtain your J visa:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="documents/JEVApplicationProf-Spec-Scholar_06-01-12.pdf">J Exchange Visitor Application Packet for Profesors, Short-Term Scholars, and Specialists</a> (updated 06/01/12)
                           </li>
                           
                           <li>NEW <a href="documents/JEVFormDS7002.pdf">DS7002 Dated 01-2013</a>. This new form must be used to replace the old form. 
                           </li>
                           
                           <li>
                              <a href="documents/EmbassyInterviewChecklist.pdf">Embassy Interview Checklist</a> 
                           </li>
                           
                        </ul>             
                        
                        <h3>Program Eligibility </h3>
                        
                        <p>Have a host department at Valencia. </p>
                        
                        <p>Be employed with an overseas organization or educational institution. </p>
                        
                        <p>Have a valid passport for the program duration.</p>
                        
                        <p>Submit proof of your English language proficiency through the submission of a written
                           sample, completion of an oral interview, or have a TOEFL score of 64 or higher. 
                        </p>
                        
                        <p>Show proof of financial support for the program duration for all expenses.</p>
                        
                        <p>Purchase the College’s health insurance plan.&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </p>             
                        <h3>Program Fees &amp; Cost of Living </h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <div><strong> 1 Week </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong> 1 Month </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>1 Semester </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>1 Year </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Injury and Sickness Insurance </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$100 </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$100 </div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$453</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,157</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Living Expenses</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$324</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,295</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$5,180</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$15,541</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Transportation (car rental) </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$150</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$600</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$3,000</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$6,000</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Social and Cultural Activities </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$50</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$200</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$800</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,600</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Total Financial Requirement: </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>$624</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>$2,195</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>$9,433</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>$24,298</strong></div>
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Family &amp; Dependents </h3>
                        
                        <p>Spouses and children under the age of 21 of J-1 visa holders are allowed to come to
                           the United States for the program duration on a J-2 visa. Dependents will be required
                           to have a separate DS-2019 and carry the College's health insurance. Dependents are
                           eligible to work once they arrive by filing an I-765 with the USCIS, but the income
                           received cannot be counted towards the financial support of the J-1 visa holder. Any
                           termination of the J-1 visa holder's status will invalidate the J-2 status for dependents.
                           Dependents may not remain in the U.S. if the J-1 visa holder remains abroad for more
                           than 30 days. J-2 visa holders cannot remain in the U.S. after reaching the age of
                           21. 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/professor/apply.pcf">©</a>
      </div>
   </body>
</html>