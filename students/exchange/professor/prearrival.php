<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/professor/prearrival.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/professor/">Professor</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Professors &amp; Scholars - Pre-Arrival Information </h2>
                        
                        <h3>Visa Application &amp; Interview</h3>
                        
                        <p>  After submitting a copy of the J Exchange Visitor Application Packet, you will be
                           issued Form DS-2019. You will use this form to take to your local U.S. embassy to
                           obtain the visa. Use this <a href="../students/documents/EmbassyInterviewChecklist.doc">Embassy Interview Checklist</a> to prepare for your interview. 
                        </p>
                        
                        <h3>Housing</h3>
                        <br>
                        <br>
                        When looking for housing, your first consideration must be the campus that you will
                        be based at with your Program Sponsor at Valencia. Please see the following link for
                        campus locations.&nbsp;East Campus and West Campus are the two largest campuses, and Winter
                        Park and Osceola are smaller campuses: <a href="../../../map/index.html">http://valenciacollege.edu/aboutus/locations</a>.
                        
                        <p>In some cases, your Program Sponsor  may offer you accommodations. Otherwise, you
                           will need to rent an apartment, look for an extended stay hotel, or stay in a homestay
                           family program. If you will be renting, you normally have to pay the first month’s
                           rent plus 1-2 month’s rent as a deposit.&nbsp; The monthly rent for a studio apartment
                           ranges from $500 to $700 per month plus utilities (electricity, water, cable TV, Internet).&nbsp;In
                           general, the amount you spend for housing should be limited to one-fourth or one-third
                           of the total amount you have planned to spend on living expenses. If the cost is one-half
                           of your budget or more, you may be spending too much.&nbsp; Here are some links to help
                           you get started with your search:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.forrent.com">http://www.forrent.com</a> 
                           </li>
                           
                           <li>
                              <a href="http://collegerentals.com/Valencia-Community-College-apartments/">http://collegerentals.com/Valencia-Community-College-apartments/</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.ihpshomestays.com">http://www.ihpshomestays.com</a> 
                           </li>
                           
                        </ul>
                        Please contact the SAGE office at <a href="mailto:studyabroad@valenciacollege.edu">studyabroad@valenciacollege.edu</a> if you need additional housing assistance.&nbsp;
                        
                        <h3>What to Pack</h3>
                        
                        <p> The biggest thing you need to consider when preparing for your trip is the weather
                           for the time of year you will be in Orlando. Use this <a href="../students/documents/04RecommendedPackingList.pdf">Recommended Packing Checklist</a> for help. 
                        </p>
                        
                        <h3>Orlando International Airport</h3>
                        
                        <p> Students will arrive to the <a href="http://www.orlandoairports.net/index.htm">Orlando International Airport</a>.&nbsp; Going through customs can add an additional hour on to your arrival time, so be
                           prepared.&nbsp; After you go through customs, you will be required to pass through another
                           security and baggage check point.&nbsp; Here are some useful links to help you:
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://www.orlandoairports.net/arrive/index.htm">Airport Arrivals Guide</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.orlandoairports.net/gt.htm">Airport Parking</a> 
                           </li>
                           
                           <li>
                              <a href="http://www.orlandoairports.net/transport/directions.htm">Driving Distances and Maps</a> 
                           </li>
                           
                        </ul>
                        
                        <h3>NSEERS</h3>
                        
                        <p>When you arrive in the U.S., you may be selected to register for the National Security
                           Entry-Exit Registration System (NSEERS). If you are subject to NSEERS, you must report
                           to an immigration officer at your port of exit when you leave the U.S. You are STRONGLY
                           urged to make an appointment with your RO/ARO to discuss procedures before leaving
                           the U.S. Failure to register your departure with NSEERS could result in you being
                           denied re-entry to the U.S.
                        </p>
                        
                        <h3>Transportation</h3>
                        
                        <p> Due to College policy, Valencia cannot provide airport pick up. You can take a <a href="http://www.orlandoairports.net/transport/local_transport.htm">Taxi Cab</a> to your accommodations if you do not have someone meeting you.&nbsp; Taxis use metered
                           rates here in Orlando. Most students depend upon the <a href="http://www.golynx.com/?fuse=cstm&amp;app=route">Lynx Bus System</a> for local transportation, but we recommend the <a href="http://www.orlandoairports.net/transport/rental_cars.htm">rental car companies</a> in Orlando. Check out the latest <a href="http://www.traffic.com/Orlando-Traffic/Orlando-Traffic-Reports.html">Orlando Travel Reports</a> to see current delays. 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/professor/prearrival.pcf">©</a>
      </div>
   </body>
</html>