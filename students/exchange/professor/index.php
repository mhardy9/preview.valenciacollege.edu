<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/professor/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li>Professor</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Professors &amp; Scholars - J Exchange Visitor Program</h2>
                        
                        <p>Thank you for your interest in the J Exchange Visitor program. Interested applicants
                           can come on a Professor, Short-Term Scholar, or Specialist designation. Click on the
                           links to the left for more details. 
                        </p>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <div><strong> Designation</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong> Description </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong> Duration</strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Professor</strong></div>
                                 
                                 <div data-old-tag="td">An individual primarily engaged in teaching, lecturing, observing, or consulting.
                                    A professor may also conduct research, but  cannot be a candidate for a tenure-accruing
                                    position.&nbsp;He or she is subject to the 24-month bar rule.<br>
                                    <br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>
                                       
                                       <p>3 weeks up to <br>
                                          5 years 
                                       </p>
                                       
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Short-Term Scholar</strong></div>
                                 
                                 <div data-old-tag="td">An individual engaged in lecturing, observing, consulting, training, or demonstrating
                                    special skills. There is no program extension beyond 6 months. <br>
                                    <br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>1 day up to <br>
                                       6 months 
                                    </div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td">
                                    <p><strong>Specialist</strong></p>
                                 </div>
                                 
                                 <div data-old-tag="td">An individual who is an expert in a field of specialized knowledge or skill.&nbsp; A specialist
                                    can observe, consult, or demonstrate their skill or knowledge. He or she cannot accept
                                    a permanent position with the college. <br>
                                    <br>
                                    
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>3 weeks up to <br>
                                       1 year 
                                    </div>
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>
                        
                        <h3>Academic Coursework </h3>
                        
                        <p>There is no regulatory prohibition on a Professor, Specialist, or Short-term Scholar
                           enrolling in academic classes as long as the classes are incidental to his or her
                           primary activity, the participant continues to pursue the program objectives, the
                           program continues to fulfill the objectives of the designated category, and the classes
                           do not become the primary objective. Consultation with&nbsp; your department&nbsp; contact and
                           RO/ARO should be undertaken before pursuing a degree program as this is considered
                           a change in status.
                        </p>
                        
                        <h3>24-Month Bar</h3>
                        
                        <p>An Exchange Visitor who participates in a J program as a Professor or Research Scholar
                           (excluding Short-Term Scholar) is barred from returning to the U.S. in the same category
                           for two years (24 months) immediately following the completion of their initial program
                           (including J-2 dependents). 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/professor/index.pcf">©</a>
      </div>
   </body>
</html>