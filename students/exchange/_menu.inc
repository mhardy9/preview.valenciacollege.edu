<ul>
	<li><a href="/students/exchange/">Exchange Program</a></li>
	<li class="submenu"><a class="show-submenu" href="/students/exchange/why-valencia/">Why Valencia <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/students/exchange/why-valencia/aboutOrlando.php">About Orlando</a></li>
			<li><a href="/students/exchange/why-valencia/resources.php">Community Resources</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/exchange/students/">Students <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/students/exchange/students/description.php">Program Description</a></li>
			<li><a href="/students/exchange/students/apply.php">How to Apply</a></li>
			<li><a href="/students/exchange/students/prearrival.php">Pre-Arrival</a></li>
			<li><a href="/students/exchange/students/arrival.php">Arrival</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/exchange/college-program/">International College Program <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/students/exchange/college-program/description.php">Program Description</a></li>
			<li><a href="/students/exchange/college-program/apply.php">How to Apply</a></li>
			<li><a href="/students/exchange/college-program/prearrival.php">Pre-Arrival</a></li>
			<li><a href="/students/exchange/college-program/arrival.php">Arrival</a></li>
			<li><a href="/students/exchange/college-program/student-life">Student Life</a></li>
			<li><a href="/students/exchange/college-program/student-life/insurance.php">Insurance &amp; Healthcare</a></li>
			<li><a href="/students/exchange/college-program/student-life/visastatus.php">Maintaining Visa Status</a></li>
			<li><a href="/students/exchange/college-program/student-life/culture.php">U.S. Customs &amp; Culture</a></li>
		</ul>
	</li>
	<li class="submenu"><a class="show-submenu" href="/students/exchange/professor/">Professors <span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/international/exchange/professor/apply.cfm">How to Apply</a></li>
			<li><a href="/international/exchange/professor/prearrival.cfm">Pre-Arrival</a></li>
			<li><a href="/international/exchange/professor/arrival.cfm">Arrival</a></li>
		</ul>

	</li>
	<li class="submenu"><a class="show-submenu" href="/students/exchange/orientation/">JEV Orientation<span class="caret" aria-hidden="true"></span></a>
		<ul>
			<li><a href="/students/exchange/orientation/purpose.php">Program Purpose</a></li>
			<li><a href="/students/exchange/orientation/insurance.php">Insurance &amp; Healthcare</a></li>
			<li><a href="/students/exchange/orientation/licenseandsocial.php">Driver's License &amp; Social Security Card</a></li>
			<li><a href="/students/exchange/orientation/visastatus.php">Maintaining Your Visa Status</a></li>
			<li><a href="/students/exchange/orientation/employment.php">Academic Training &amp; Employment</a></li>
			<li><a href="/students/exchange/orientation/endprogram.php">Ending / Extending Your Program</a></li>
			<li><a href="/students/exchange/orientation/governmentcontacts.php">Government Agency Contacts</a></li>
		</ul>
	</li>
	<li><a class="Group" href="http://net4.valenciacollege.edu/forms/students/exchange/contact.cfm" target="_blank">Contact Us</a></li>
</ul>