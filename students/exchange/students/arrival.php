<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/students/arrival.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Students - Arrival Information</h2>
                        
                        <h3>First Day Arrival to Valencia College </h3>
                        
                        <p>Students cannot arrive more than 30 days prior to their program start date, but you
                           should plan to arrive at least two weeks before classes begin. On the first day of
                           your campus arrival, please report to Ms. Bliss Thompson, International Counselor
                           and Responsible Officer, at Valencia's West Campus, 1800 South Kirkman Rd., Student
                           Services Building, office 146. It is best to contact her in advance to request an
                           appointment. The hours are Monday through Thursday from 9 a.m. to 5 p.m. and Friday
                           from 9 a.m. to 4 p.m. Phone: 407-582-5830 / Email: 
                           
                           
                           <a href="mailto:bthompson@valenciacollege.edu">bthompson@valenciacollege.edu</a>. Please bring the following documents when you check in: 
                        </p>
                        
                        <ul>
                           
                           <li>Passport with J-1 visa</li>
                           
                           <li>I-94 card</li>
                           
                           <li>DS-2019 </li>
                           
                           <li>The documents above for all J-2 dependents</li>
                           
                           <li>Proof of insurance payment (if already purchased) </li>
                           
                        </ul>             
                        
                        <p><strong>Exchange Students:</strong>  Please contact the Study Abroad &amp; Global Experiences Office at 407-582-3188 to be
                           matched up with a Valencia "buddy" who will help acclimate you to campus life and
                           getting around Orlando. 
                        </p>
                        
                        <h3>New Student Orientation</h3>
                        
                        <p><img alt="Exchange Students Classroom" border="1" height="130" src="fs_dualEnroll.jpg" width="204">All new students are required to participate in an orientation session before registering
                           for their first term. This session includes information on education planning, an
                           introduction to college resources, and advising for the first term. You must sign
                           up in advance for the <a href="../../../admissions/orientation/index.html">New Student Orientation at Valencia</a>. 
                        </p>
                        
                        <h3>J Exchange Visitor Orientation </h3>
                        
                        <p>All J Exchange Visitors are required to attend a <a href="../jevorientation/default.html">J Exchange Visitor Orientation</a> session that covers  information specific to this program:
                        </p>
                        
                        <h3>Emergency Messaging System</h3>
                        
                        <p>Upon arrival, be sure to sign up for Valencia's Emergency Messaging System. You do
                           this through your Atlas account. Find VALENCIA ALERT on your Atlas home page and click
                           the link to sign up for alerts. You can receive instant notification via email or
                           text message to your cell phone. 
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/students/arrival.pcf">©</a>
      </div>
   </body>
</html>