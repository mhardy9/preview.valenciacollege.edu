<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/students/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Students - How to Apply </h2>
                        
                        <h3>              Application Checklist &amp; Packet </h3>
                        
                        <p>The following packet includes a checklist on the steps to enroll and the forms needed
                           to issue your J visa: <a href="documents/JEVApplicationStudent_2011-2012_03-19-122.pdf">J Exchange Visitor Application Packet</a> (updated 03/19/2012).
                        </p>
                        
                        <p>As part of the checklist process, you will need to <a href="../../../admissions-records/index.html">Apply for Admission</a> to Valencia as a J visa nondegree-seeking student. All applicants will need to pay
                           the nonrefundable $35 application fee. You should  select "transient" on the application.
                           
                        </p>
                        
                        <h3>Program Eligibility </h3>
                        
                        <ul>
                           
                           <li>Be 18 years of age or older.<br>
                              <br>
                              
                           </li>
                           
                           <li>Have a valid passport for the program duration.<br>
                              <br>
                              
                           </li>
                           
                           <li>Degree-seeking students: Have a minimum TOEFL score of 45 on the Internet version.
                              Nondegree-seeking students in an Exchange Program do not have to take the TOEFL, but
                              a minimum score of 64 is recommended. A written letter signed by your institution
                              may be requested.<br>
                              <br>
                              
                           </li>
                           
                           <li>Have completed one year of study at your home institution.<br>
                              <br>
                              
                           </li>
                           
                           <li>Show proof of financial support  for the program duration for all expenses:<br>
                              <br>
                              
                              <ul>
                                 
                                 <li>In order to use personal/family funds for more than 50 percent of your program costs,
                                    Valencia must have a Memorandum of Understanding on file with your home institution.
                                    Otherwise, you must be funded more than 50 percent by a government agency, institution,
                                    or some other international organization. <br>
                                    <br>
                                    
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Purchase Valencia's health insurance plan.�������������            </li>
                           
                        </ul>            
                        
                        <h3>Dates &amp; Deadlines </h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Fall 2012:&nbsp;</strong></div>
                                 
                                 <div data-old-tag="td">
                                    
                                    <p><strong>Course Registration Opens:</strong>&nbsp; May 25, 2012<br>
                                       <strong>Application Deadline:</strong>&nbsp; June 15, 2012<br>
                                       <strong>Earliest Possible Report Date</strong>: July 27, 2012<br>
                                       <strong>Term Begins:</strong>&nbsp; August 27, 2012<br>
                                       <strong>Term Ends:</strong>&nbsp; December 16, 2012<br>
                                       
                                    </p>
                                    
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Spring 2013:</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Course Registration Opens:&nbsp;</strong> November 2, 2012<br>
                                    <strong>Application Deadline:</strong>&nbsp; October 19, 2012<br>
                                    <strong>Earliest Possible Report Date:</strong> November 9, 2012&nbsp;<br>
                                    <strong>Term Begins:</strong>&nbsp; January 7, 2013<br>
                                    <strong>Term Ends:</strong> April 28, 2013
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Summer 2013:</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <strong>Course Registration Opens:&nbsp;</strong> February 22, 2013<br>
                                    <strong>Application Deadline:</strong>&nbsp; February 15, 2013<br>
                                    <strong>Earliest Possible Report Date:</strong>&nbsp; April 15, 2013<br>
                                    <strong>Term Begins:</strong>&nbsp; May 6, 2013<br>
                                    <strong>Term Ends:</strong>&nbsp; July 30, 2013
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <h3>Program Fees &amp; Cost of Living </h3>
                        
                        <div data-old-tag="table">
                           
                           <div data-old-tag="tbody">
                              
                              <div data-old-tag="tr">
                                 
                                 
                                 <div data-old-tag="td">
                                    <div><strong>1 Semester<br>
                                          15 Credits </strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>1 Year <br>
                                          30 Credits </strong></div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Application Fee (nonrefundable)</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$35</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$35</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Tuition and Fees: 1 credit hour = $99.06; full-time enrollment is 12 to 18 credits
                                       </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,486</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$2,972</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Books</strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$589</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,178</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Injury and Sickness Insurance </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$453</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,157</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Living Expenses (lodging, utilities, food, bus transportation, etc.) </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$5,180</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$15,541</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Social and Cultural Activities </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div>$800</div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div>$1,600</div>
                                 </div>
                                 
                              </div>
                              
                              <div data-old-tag="tr">
                                 
                                 <div data-old-tag="td"><strong>Total Financial Requirement: </strong></div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>$8,543</strong></div>
                                 </div>
                                 
                                 <div data-old-tag="td">
                                    <div><strong>$22,483</strong></div>
                                 </div>  
                              </div>
                              
                           </div>
                           
                        </div>            
                        
                        <h3>Family &amp; Dependents </h3>
                        
                        <p>Spouses and children under the age of 21 of J-1 visa holders are allowed to come to
                           the United States for the program duration on a J-2 visa. Dependents will be required
                           to have a separate DS-2019 and carry the college's health insurance. Dependents are
                           eligible to work once they arrive by filing an I-765 with the USCIS, but the income
                           received cannot be counted toward the financial support of the J-1 visa holder. Any
                           termination of the J-1 visa holder's status will invalidate the J-2 status for dependents.
                           Dependents may not remain in the U.S. if the J-1 visa holder remains abroad for more
                           than 30 days. J-2 visa holders cannot remain in the U.S. after reaching the age of
                           21. 
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/students/apply.pcf">©</a>
      </div>
   </body>
</html>