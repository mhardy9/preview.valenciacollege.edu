<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/students/description.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/students/">Students</a></li>
               <li>ENTER TITLE HERE</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <a name="content" id="content"></a>
                        <a href="../index.html"></a>
                        
                        
                        
                        <style>
.google-translate{ background-color: #f0f0f0; 
border: solid 1px #aaa; 
padding: 5px; 
width: 320px; 
height: 40px; 
margin: 5px 0 0 0; 
overflow: hidden; }
</style>
                        <div id="google_translate_element"></div><script type="text/javascript">
function googleTranslateElementInit() {
  new google.translate.TranslateElement({pageLanguage: 'en', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
}
</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
                        
                        
                        
                        <h2>Students - Program Description</h2>
                        
                        <h3><strong>Full-time Degree-Seeking Students</strong></h3>
                        
                        <p>Study in a full-time Associate of Arts or Associate of Science degree program in Arts
                           &amp; Entertainment, Business, Criminal Justice, Engineering, Hospitality, or Information
                           Technology. Please note that international students cannot enroll in any of the Healthcare
                           programs since space is limited. Students must enroll for 12 to 19 credits per semester.
                           Most courses are 3 to 4 credits each. Please note that space is limited and courses
                           are not guaranteed. Seats are assigned on a first-come, first-serv basis so plan ahead
                           and apply early. The minimum program duration is three weeks up to two years. 
                        </p>
                        
                        <ul>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/future-students/programs/">Degree and Career Programs</a> 
                           </li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/financial-aid/">Cost of Attendance </a></li>
                           
                           <li><a href="../../../calendar/index.html">College Dates and Deadlines</a></li>
                           
                           <li><a href="http://net5.valenciacollege.edu/schedule/">Class Schedules</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/why-valencia/location/">Housing and Campus Locations</a></li>
                           
                           <li>
                              <a href="../../../studentdev/CampusInformationServices/index.html">Campus Information Services</a> 
                           </li>
                           
                           <li>
                              <a href="http://preview.valenciacollege.edu/future-students/why-valencia/student-support-services/">Student Support Services</a> 
                           </li>
                           
                           <li>
                              <a href="../../../studentdev/clubs2.html">Student Clubs and Organizations</a> 
                           </li>
                           
                           <li>
                              <a href="../../../studentdev/intramural.html">Intramural Sports</a>  
                           </li>
                           
                           <li><a href="../../../internship/index.html">Internships</a></li>
                           
                           <li><a href="http://preview.valenciacollege.edu/future-students/directconnect-to-ucf/">DirectConnect to the University of Central Florida </a></li>
                           
                        </ul>
                        
                        
                        <h3><strong>Nondegree-Seeking Exchange Students</strong></h3>
                        
                        <p>Students can come for one semester of study in any of the degree programs mentioned
                           above. Valencia must have a signed agreement with your institution. You will need
                           to submit a signed approval form from your home institution for all course selections.
                           Be sure to consider course pre-requisites when you make your selections. Students
                           must enroll for 18 credits per semester unless their institution requires otherwise.
                           The following is a list of our current Exchange Program Partners:
                        </p>
                        
                        <ul>
                           
                           <li><a href="http://www.rhs.dk/sites/en/Pages/Welcome-to-Roskilde-Business-College.aspx">Roskilde Business College, Denmark</a></li>
                           
                        </ul>             
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/students/description.pcf">©</a>
      </div>
   </body>
</html>