<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/prearrival.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li>Pre-Arrival Information</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div id="google_translate_element"></div>
                        
                        
                        
                        
                        <h2>Valencia's International College Program with Academic Training <br>
                           at the <em>Walt Disney World© </em>Resort
                        </h2>
                        
                        <h2>Pre-Arrival Information</h2>
                        
                        <p><a href="#visa_application_interview">Visa Application &amp; Interview </a><br>
                           <a href="#housing">Housing</a><br>
                           <a href="#what_to_pack">What to Pack</a><br>
                           <a href="#travel_and_entry">Travel and Entry into the United States</a> <br>
                           <a href="#airport">Orlando International Airport</a><br>
                           <a href="#transportation">Transportation</a><br>
                           <a href="/students/exchange/documents/EmbassyInterviewChecklist_VCC.pdf">Embassy Interview Checklist_VCC</a></p>
                        
                        <h3>Visa Application &amp; Interview</h3>
                        <a name="visa_application_interview" id="visa_application_interview"></a> 
                        
                        <p>Please review the steps below for details on applying for a visa. Once you are done
                           reviewing the information below, please refer to the  <a href="/students/exchange/documents/EmbassyInterviewChecklist_VCC.pdf">Embassy Interview Checklist</a> to help prepare for your interview.
                        </p>
                        
                        <p>Students interested in Valencia's International College Program will need to apply
                           for the nonimmigrant <a href="http://j1visa.state.gov/programs/college-and-university-student">Exchange Visitor (J-1) visa</a> (College/University Student category). Even if you already have a tourist visa, you
                           will still need a J-1 visa to participate in this program.&nbsp; 
                        </p>
                        
                        <ol>
                           
                           <li>
                              Read the <a href="/students/exchange/documents/DOSJEVWelcomeBrochure.pdf">U. S. Department of State Exchange Visitor Program Welcome Brochure</a> to learn more about the Exchange Visitor program and the J-1 visa rules that will
                              apply to you during your stay in the United States.&nbsp;&nbsp; 
                           </li>
                           
                           <li>
                              Review the Form DS-2019 (this form is also known as the “Certificate of Eligibility
                              for Exchange Visitor Status.")&nbsp;
                              
                              <ul>
                                 
                                 <li> Check your name, date of birth, and all other personal information for accuracy.&nbsp;
                                    If there are any errors, let us know immediately. 
                                 </li>
                                 
                                 <li>Sign at the bottom where it says your name and date.</li>
                                 
                                 <li>Make a copy of this for your records to leave at home with a family member along with
                                    a copy of your passport.
                                 </li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>
                              Pay the SEVIS fee.&nbsp; <a href="http://www.ice.gov/sevis/index.htm">SEVIS</a> (Student and Exchange Visitor Information System) is a computer system that collects
                              and maintains information on the current status of nonimmigrant exchange visitors
                              during their stay in the United States.&nbsp; You must have your DS-2019 before you can
                              pay the SEVIS fee. You may schedule your visa interview before you pay the SEVIS fee.
                              However, the SEVIS fee payment must be processed at least three business days before
                              your visa interview takes place. 
                           </li>
                           
                        </ol>
                        
                        <ul>
                           
                           <li>The easiest way to pay the SEVIS fee is <a href="https://www.fmjfee.com/i901fee/">online with a credit card</a>.&nbsp; You must have your DS-2019 form with you in order to do this.&nbsp; 
                           </li>
                           
                           <li>Be sure to enter your personal information <em>exactly </em>as it appears on your DS-2019.&nbsp; 
                           </li>
                           
                           <li>The Valencia College Program Number is located on your DS-2019. </li>
                           
                           <li>Your SEVIS ID number is printed at the top right of the DS-2019 above the bar code.&nbsp;
                              It starts with the letter “N”.&nbsp; 
                           </li>
                           
                           <li>When paying the fee online, be sure to have your printer ready before you start the
                              payment process.&nbsp; Do not exit the receipt page until you have successfully printed
                              the receipt, because you will need it to present at the U. S. Consulate to prove that
                              you paid the SEVIS fee.&nbsp; You will not be able to return to the receipt page after
                              you exit it.
                           </li>
                           
                        </ul>
                        
                        <li>
                           Complete the Form DS-160 (which is your visa application). It is best to make your
                           visa application at the <a href="http://www.usembassy.gov/">American embassy or consulate</a> that has jurisdiction over your place of permanent residence. Although you are allowed
                           to apply at any U.S. consular office, it may be more difficult to qualify for the
                           visa outside your country of permanent residence. Each consular office has its own
                           specific process, so make sure to check its web site carefully and follow all the
                           rules. 
                        </li>
                        
                        
                        <p>Instructions for Completing the Form DS-160</p>
                        
                        <table class="table table">
                           
                           <tbody>
                              
                              <tr>
                                 
                                 <th scope="col">Purpose of Your Trip:</th>
                                 
                                 <td>Student/Exchange Visa (F, J, M, Q) </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="col">Intended Date of Arrival:</th>
                                 
                                 <td>Refer to DS-2019</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="col">Intend Length of Stay in the U. S.:</th>
                                 				  
                                 <td>6 months</td>
                                 				  
                              </tr>
                              
                              <tr>
                                 				  
                                 <th scope="col">Address Where You Will Stay in the U. S. : </th>
                                 				  
                                 <td>8050 Gables Commons Drive<br>Orlando, Florida 32821<br>Your exact apartment number is not known yet, but your mail will reach you at this
                                    address.) 
                                 </td>
                                 				
                              </tr>
                              
                              <tr>
                                 				  
                                 <th scope="col">Person/Entity Paying for Your Trip:</th>
                                 
                                 <td>Self</td>
                                 				  
                              </tr>
                              
                              <tr>
                                 				  
                                 <th scope="col">Contact Person or Organization in the United States</th>
                                 
                                 <td>Talia Popovski </td>
                                 				  
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="col">Organization Name: </th>
                                 
                                 <td>Valencia College</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="col">Relationship to You:</th>
                                 
                                 <td>School Official</td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="col">Address and Phone Number of Point of Contact: </th>
                                 
                                 <td>1800 S Kirkman Road, Building 10, Orlando, FL 32811;<br>Business Phone:&nbsp; 407-582-6607 
                                 </td>
                                 
                              </tr>
                              
                              <tr>
                                 
                                 <th scope="col">SEVIS Information Page:</th>
                                 
                                 <td>Refer to the DS-2019 for SEVIS ID # and institution information</td>
                                 				  
                              </tr>
                              
                           </tbody>
                           
                        </table>
                        
                        <p>Important:&nbsp; If the U. S. consular official denies your J-1 visa, inform the coordinator
                           at your university immediately.&nbsp; He or she will communicate this information to Valencia.&nbsp;
                           If you decide to withdraw from the program prior to arrival, notify your university’scoordinator,
                           who will inform Valencia.&nbsp; In either case, any fees paid to the U.S. government are
                           nonrefundable. <br>
                           
                        </p>
                        
                        <h3>Housing<a name="housing" id="housing"></a>
                           
                        </h3>
                        
                        <p>Upon your arrival at Disney’s Vista Way Welcome Center, you will be assigned to an
                           apartment in one of Disney’s student housing complexes.<img alt="Apartment Interior" src="/students/exchange/images/ApartmentInterior3_000.jpg" class="img-responsive">&nbsp; Full information about Disney housing facilities, furnishings and policies is available
                           at <a href="https://www.disneyinternationalprograms.com/j1/living_intro.html">https://www.disneyinternationalprograms.com/j1/living_intro.html</a>.&nbsp;&nbsp; &nbsp;
                        </p>
                        
                        <h3>What to Pack<a name="what_to_pack" id="what_to_pack"></a> 
                        </h3>
                        
                        <p>Disney will provide you with a list of things to bring.&nbsp; Here are additional suggestions:</p>
                        
                        <ul>
                           
                           <li> Medical records:&nbsp; If you have any chronic medical problems or have had serious illnesses
                              or surgeries in the past, bring a letter from your doctor that explains your diagnoses
                              and treatments, in case a doctor in the U. S. needs to know about them.&nbsp; Make sure
                              the letter is translated into English, if necessary.&nbsp; It is also helpful to have a
                              written list of any medications that you take regularly.<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              Medications:&nbsp; All of your prescription medications should be in their original containers.&nbsp;
                              If you take regular medication, it is best to bring enough to last you throughout
                              your program, if possible.&nbsp; If you need to get a prescription from home refilled while
                              in the U. S., the pharmacy may require you to present the original prescription from
                              your doctor.&nbsp; This should be translated into English.&nbsp; Note that some medications
                              that are sold without restriction in other countries require a doctor’s prescription
                              in the United States.&nbsp; Therefore, it makes sense to bring a supply of your favorite
                              medications for pain, cough, allergies, etc.&nbsp; However, be aware that certain drugs
                              with a high potential for abuse may not be brought into the U. S. and there are severe
                              penalties for trying to do so.&nbsp; You can find the latest information about this at
                              <a href="http://www.cbp.gov/xp/cgov/travel/vacation/kbyg/prohibited_restricted.xml#Medication">http://www.cbp.gov/xp/cgov/travel/vacation/kbyg/prohibited_restricted.xml#Medication</a> . <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              Eye glasses and contact lenses:&nbsp; Bring a copy of these prescriptions.&nbsp; It is helpful
                              to have a spare pair of prescription glasses.&nbsp; Even if you only wear contact lenses,
                              you may need glasses if you develop an eye problem that prevents you from wearing
                              contacts for a while. <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              Laptop computer:&nbsp; Even though the Disney housing complexes have computer labs, they
                              are often crowded and they close early in the evening.&nbsp;  We highly recommend that
                              you bring your own laptop to use in your apartment or you might want to check prices
                              on the Internet to see if it would be less expensive to buy one after you arrive in
                              the U. S. instead of in your home country.<br>
                              <br>
                              
                              
                           </li>
                           
                           <li>
                              Things that represent your home country:&nbsp; Bring music, photographs, cultural dress,
                              maps, posters, snacks and other items that will help you remain connected to your
                              home culture and help you to teach your new friends about it.&nbsp; <br>
                              <br>
                              
                           </li>
                           
                           <li>
                              Cooking items:&nbsp; Even if you have not cooked much before, you will need to start in
                              Orlando.&nbsp; Restaurant food is expensive and often not very healthy.&nbsp; Bring your favorite
                              recipes.&nbsp; If your favorite foods call for special spices or ingredients, consider
                              bringing them from home as they may be difficult or impossible to get in the U. S.&nbsp;
                              Food is generally allowed by U.S. Customs as long as it is dry.&nbsp; <em>Do not bring meat, fresh fruits or vegetables as they are prohibited and will be confiscated
                                 by U. S. Customs when you enter the U. S.</em>
                              
                           </li>
                           
                        </ul>
                        Appropriate clothing for Disney classes:
                        
                        <ul>
                           
                           <li>Go to <a href="https://www.disneyinternationalprograms.com/j1/look_intro.html">https://www.disneyinternationalprograms.com/j1/look_intro.html</a> to see all of the requirements for “The Disney Look.”&nbsp; You are expected to be fully
                              compliant with the appearance requirements when you arrive in Orlando.&nbsp; To know what
                              you can and can’t wear during your Disney classes, click on “All Female Cast Members”
                              or “All Male Cast Members” and scroll down to the sections called “Non-Costumed Females”
                              or “Non-Costumed Males.”<br>
                              <br>
                              
                           </li>
                           
                           <li>
                              Warm clothing:&nbsp; Many people don’t realize that Florida can be cold in the winter,
                              especially for people who live in countries near the equator and are not used to cooler
                              temperatures.&nbsp; You can find monthly average temperature charts online.&nbsp; If you will
                              be arriving in January, you will want to bring or plan to buy a medium-weight jacket
                              or coat.&nbsp; Also, Florida buildings are always air conditioned and often feel quite
                              cold, especially to people who are not used to air conditioning.&nbsp; So you will want
                              to have some sweaters or a light jacket to wear inside at any time of the year.&nbsp;&nbsp;
                              
                           </li>
                           
                        </ul>
                        
                        <h3>Travel and Entry into the United States<a name="travel_and_entry" id="travel_and_entry"></a>
                           
                        </h3>
                        
                        <p>Entry Point/Arrival in Orlando<br>
                           <br>
                           If you enter the U. S. at any city other than Orlando, you are responsible for getting
                           yourself to Orlando on your program start date.&nbsp; Please refer to Box 3 on your DS-2019
                           form for your program start and end dates.&nbsp; You are responsible for purchasing a round-trip
                           airline ticket to the United States.&nbsp; You may wish to leave your return date open
                           ended in case your program should end early for any reason, or if you should decide
                           to stay longer to take advantage of the 30-day grace period for travel in the U. S.
                        </p>
                        
                        
                        <p>You are allowed to arrive in the U. S. up to 30 days before the program start date.&nbsp;
                           However, you cannot check into housing complex at the <em>Walt Disney World </em>Resort until your program start date, so you will need to arrange for other lodging
                           if you arrive before then.&nbsp; If possible, we recommend that you enter the U. S. at
                           least one day before your program start date. This will allow for any flight delays
                           and give you time to rest from your travels before the busy orientation period begins.
                           Also, by arriving at least a day early, you can check in to Disney’s housing promptly
                           on the morning of your program start date. You will then have the rest of the day
                           to shop for necessities and settle in to your apartment. After that, the next several
                           days and evenings will be full with mandatory meetings.
                        </p>
                        
                        
                        <p>Arriving after the program start date listed on your DS-2019 could present problems
                           at the U. S. border.&nbsp; A late arrival also would mean missing some of your required
                           orientation sessions.&nbsp; If for any reason you need to arrive in the U. S. after your
                           program start date, please tell your school’s Valencia Disney coordinator, who will
                           contact us.
                        </p>
                        
                        
                        <p>I-94 Card</p>
                        
                        <p>During your flight to the U. S., the flight attendant will ask you to complete an
                           arrival card called Form I-94.&nbsp; When filling out the I-94 and all official documents,
                           always use your official name and other information exactly as they appear on your
                           passport. 
                        </p>
                        
                        <p>Immigration Inspection upon Arrival</p>
                        
                        <p>Upon arrival in the U. S., your documents will be inspected by the U. S. Department
                           of Homeland Security.&nbsp; Have your passport, original DS-2019 (not a photocopy) and
                           Form I-94 ready.&nbsp; The small white I-94 card will be stapled into your passport.&nbsp; Examine
                           your documents immediately after they are returned to you to make sure that the officials
                           did not make any errors (for example, your visa classification – it should say J-1/DS.)&nbsp;
                           If you find an error, tell an official and get it corrected before you leave the airport.
                           
                        </p>
                        
                        <p>Your DS-2019 and I-94 are important documents!&nbsp; Keep them in a safe place with your
                           passport at all times.&nbsp; During your first day of orientation, Valencia staff will
                           make copies of your documents for our records. 
                        </p>
                        
                        <h3>Orlando International Airport<a name="airport" id="airport"></a>
                           
                        </h3>
                        
                        <h3>&nbsp; </h3>Students will arrive to the <a href="http://www.orlandoairports.net/index.htm">Orlando International Airport</a>.&nbsp; Going through customs can add an additional hour on to your arrival time, so be
                        prepared.&nbsp; After you go through customs, you will be required to pass through another
                        security and baggage check point.&nbsp; Here are some useful links to help you:
                        
                        <ul>
                           
                           <li><a href="http://www.orlandoairports.net/arrive/index.htm">Airport Arrivals Guide</a></li>
                           
                           <li><a href="http://www.orlandoairports.net/gt.htm">Airport Parking</a></li>
                           
                           <li><a href="http://www.orlandoairports.net/transport/directions.htm">Driving Distances and Maps</a></li>
                           
                        </ul>
                        
                        <h3></h3>
                        
                        <h3>Transportation<a name="transportation" id="transportation"></a>
                           
                        </h3>
                        
                        <h3>&nbsp; </h3>Due to college policy, Valencia cannot provide airport pick up. You can take a <a href="http://www.orlandoairports.net/transport/local_transport.htm">Taxi Cab</a> to your accommodations if you do not have someone meeting you.&nbsp; Taxis use metered
                        rates  in Orlando. There are many <a href="http://www.orlandoairports.net/transport/rental_cars.htm">rental car companies</a> in Orlando, but you must be at least 21 years old to rent a car&nbsp;with most companies.
                        Check  the latest <a href="http://www.traffic.com/Orlando-Traffic/Orlando-Traffic-Reports.html">Orlando Travel Reports</a> to see current delays.
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/prearrival.pcf">©</a>
      </div>
   </body>
</html>