<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/apply.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li>How to Apply</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        		
                        <div id="google_translate_element"></div>      
                        
                        
                        
                        <h2> Valencia's International College Program with Academic Training at the <em>Walt Disney World© </em>  Resort
                        </h2>
                        
                        <h2>How to Apply </h2>
                        
                        <h3>Application Checklist &amp; Packet</h3>
                        
                        <p>Students interested in the program should contact their school representative in order
                           to learn more.&nbsp; Approved students will be required to<img alt="Student in Valencia College Business Management Course" border="1" height="186" hspace="11" src="DisneyManagementClass197.jpg" vspace="11" width="280"> complete a program application.&nbsp; After the application is approved, students will
                           be orally evaluated in order to satisfy the English language proficiency requirement.&nbsp;
                           Students who have met the English requirement will need to attend a Disney International
                           Programs recruitment presentation and participate in an interview with a Disney recruiter.&nbsp;
                           Students who are selected to participate will then pay the application fee and submit
                           the remaining documents listed in the application.&nbsp; Valencia then issues the DS-2019
                           and sends the student the <em>J Exchange Visitor Pre-Arrival Packet.</em>&nbsp; After receiving notification that the student’s visa has been issued, the balance
                           of the program fees must be submitted to their school representative.&nbsp;&nbsp;&nbsp; 
                        </p>
                        
                        <p> <a href="/students/exchange/documents/ValIntlCollProg_StudentApplication_080911.pdf">JEV International College Program Application </a><br>
                           
                        </p>
                        
                        <h3>Program Eligibility </h3>
                        
                        <p>In order for students to be enrolled in this program, they must satisfy the following
                           requirements:
                        </p>
                        
                        <ul>
                           
                           <li>The student’s primary purpose in the United States must be to study rather than work.</li>
                           
                           <li>The student must be in good academic standing and currently enrolled at their home
                              school.
                           </li>
                           
                           <li>The Academic Training Practicum must be related to their major field of study.</li>
                           
                           <li>The student must meet the folllowing minimum English language requirements: </li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>Speakers at this level are able to converse with ease and confidence when dealing
                              with most routine tasks and social situations.&nbsp; They are able to successfully handle
                              many uncomplicated tasks and social situations requiring an exchange of basic information
                              related to work, school, recreation, particular interests and areas of competence,
                              though hesitation and errors may be evident.&nbsp; 
                           </p>
                           
                        </blockquote>            
                        
                        <ul>
                           
                           <li>The student must have completed their first year of study. </li>
                           
                           <li>There must be an agreement between Valencia College and the student's home school.
                              
                           </li>
                           
                        </ul>            
                        
                        <h3>Partner Schools </h3>
                        
                        <p>Brazil <br>
                           <a href="http://www.castelli.edu.br/site/">Castelli Escola Superior de Hotelaria</a> <br>
                           <a href="http://translate.google.com/translate?hl=en&amp;sl=pt&amp;u=http%3A//www.estacio.br/&amp;prev=/search%3Fq=estacio+de+sa&amp;hl=en&amp;rls=com.microsoft%3Aen-us%3AIE-SearchBox&amp;biw=1280&amp;bih=822&amp;prmd=imvns&amp;sa=X&amp;ei=NknjT8qDJ4_g8ATj_vCGCA&amp;sqi=2&amp;ved=0CHcQ7gEwAA">Universidade Estacio da Sa</a> 
                        </p>
                        
                        <p>Colombia<br>
                           <a href="http://www.uninorte.edu.co/" target="_blank">Fundación Universidad del Norte </a></p>
                        
                        <p>Denmark<br>
                           <a href="http://www.rhs.dk/Pages/Velkommen-til-RHS.aspx" target="_blank">Roskilde Business College</a> 
                        </p>
                        
                        <p>Japan<br>
                           <a href="http://www.akita-u.ac.jp/english/">Akita University</a><br>
                           <a href="http://www.baiko.ac.jp/university/index.html" target="_blank">Baiko Gakuin University</a> <br>
                           <a href="http://www.shimonoseki-cu.ac.jp/index.html">Shimonoseki City University</a><br>
                           <a href="http://www.kitakyu-u.ac.jp/lang_en/index.html" target="_blank">University of Kitakyushu</a> <br>
                           <a href="http://www.tokushima-u.ac.jp/english/" target="_blank">University of Tokushima </a></p>
                        
                        <p>Netherlands<br>
                           <a href="http://www.idcollege.nl/" target="_blank">ID College</a><br>
                           <a href="http://www.leijgraaf.nl/ROC/goto347.aspx" target="_blank">ROC de Leijgraaf </a></p>
                        
                        <p>Paraguay<br>
                           <a href="http://www.unida.edu.py/">Universidad de la Integración de las Américas </a></p>
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/apply.pcf">©</a>
      </div>
   </body>
</html>