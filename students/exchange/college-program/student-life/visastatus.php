<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/student-life/visastatus.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li><a href="/students/exchange/college-program/student-life/">Student Life</a></li>
               <li>Maintaining Visa Status </li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <div id="google_translate_element"></div>
                        
                        <h2>Valencia's International College Program with Academic Training at the <em>Walt Disney World© </em>  Resort
                           
                        </h2>
                        
                        <h3>Maintaining Visa Status </h3>
                        
                        <h3>Home-Country Physical Presence Requirement</h3>
                        
                        <ul>
                           
                           <li>The U.S. embassy or consulate determines if an exchange visitor is subject to the
                              212(e) rule. 
                           </li>
                           
                           <li>Exchange visitors subject to the 212(e) rule must return to their home country, or
                              last country of residence,&nbsp; and reside in that country for two years before they become
                              eligible for H, L, or permanent resident status. 
                           </li>
                           
                           <li>If subject to the 212(e) rule, exchange visitors are not permitted to change their
                              nonimmigrant status within the U.S. from J to any other nonimmigrant category except
                              A (diplomatic) or G (international organization) status.
                           </li>
                           
                           <li>A waiver of the two-year home-country physical presence requirement is possible. Waivers
                              are permitted by the DoS and granted by U.S. Citizenship and Immigration Services
                              (USCIS).
                           </li>
                           
                        </ul>
                        
                        <h3>Responsibilities of Visa Holder</h3>
                        
                        <p>As a J-1 nondegree-seeking student, your responsibilities include but may not be limited
                           to the following items listed below:
                        </p>
                        
                        <ul>
                           
                           <li>Obtain validation of your SEVIS record after checking in with your ARO upon arrival.</li>
                           
                           <li>Retain required documentation at all times which include a valid DS-2019, I-94 card,
                              and valid passport during the entire length of the program.
                           </li>
                           
                           <li>Engage only in appropriate activities permitted, specifically in Section 4 of the
                              DS-2019.
                           </li>
                           
                           <li>Report address changes to your assigned ARO within 10 days of the move date.</li>
                           
                           <li>Maintain the required sickness and injury insurance coverage for the entire program
                              period (including program extensions). 
                           </li>
                           
                           <li>Comply with all academic program guidelines and acceptable standards of student conduct.&nbsp;
                              This includes attendance requirements for each course.&nbsp; 
                           </li>
                           
                           <li>Comply with employment guidelines and refrain from any unauthorized employment.&nbsp; All
                              employment activity that is not included in Part 4 on the DS-2019 must be approved
                              in writing by the ARO before the activity begins.&nbsp; You may only work at the designated
                              academic training and practicum site and be “in good standing” with your employer.
                              
                           </li>
                           
                        </ul>
                        
                        <p>  All students registered in this program are subject to the policies and procedures
                           established by Valencia College and outlined in Valencia's <em><a href="/about/general-counsel/policy/" target="_top">Policies and Procedures Manual</a></em>. 
                        </p>
                        
                        <h3><strong>Program Amendments</strong></h3>
                        
                        <ul>
                           
                           <li>Changes to the J category are not permitted unless you can demonstrate the change
                              is a continuation of the original J objective.
                           </li>
                           
                           <li>Exchange Visitor may NOT change their program objective.</li>
                           
                           <li>Changes in program activities are not permitted without prior ARO approval.&nbsp; The exchange
                              visitor must inform their ARO of any of the following changes:&nbsp; funding, worksite
                              location, supervisor’s name, and department changes.
                           </li>
                           
                        </ul>
                        
                        <h3>Checking In </h3>
                        
                        <ul>
                           
                           <li>Validation of the SEVIS record will only occur after the check-in with your ARO is
                              completed. 
                           </li>
                           
                           <li>Documents required for check-in include:&nbsp; J-1 Visa, Passport, I-94 Card, DS-2019,
                              and proof of insurance coverage.
                           </li>
                           
                        </ul>
                        
                        <h3>School / Work Policies</h3>
                        
                        <p>When at school or work, be sure you understand American policies as they relate to
                           the following:
                        </p>
                        
                        <ul>
                           
                           <li>Work/School attendance</li>
                           
                           <li>Smoking </li>
                           
                           <li>Use of intoxicants</li>
                           
                           <li>Possession of illegal substances</li>
                           
                           <li>Discrimination</li>
                           
                           <li>Public displays of affection</li>
                           
                           <li>Sexual harassment</li>
                           
                           <li>Gift giving </li>
                           
                        </ul>
                        
                        <h3>Reasons for Termination</h3>
                        
                        <p>A sponsor shall terminate an exchange visitor's participation in its program when
                           the exchange visitor:
                        </p>
                        
                        <ul>
                           
                           <li>Fails to continue in the program </li>
                           
                           <li>Is unable to continue </li>
                           
                           <li>Violates the Exchange Visitor Program regulations and/or the college’s rules and policies</li>
                           
                           <li>Fails to maintain the insurance coverage</li>
                           
                           <li>Engages in unauthorized employment</li>
                           
                        </ul>
                        
                        <h3>Termination Reasons for the <em>Walt Disney World</em> Resort 
                        </h3>
                        
                        <ul>
                           
                           <li>Alcohol/Drugs</li>
                           
                           <li>Hazing/Initiation related activities</li>
                           
                           <li>Firearms/Explosives</li>
                           
                           <li>Violation of visitor policy</li>
                           
                           <li>Job termination </li>
                           
                           <li>Offensive computer usage</li>
                           
                        </ul>
                        
                        <p><strong><em>If any of these violations occurs, the ARO shall terminate the exchange visitor's
                                 participation in the program.</em></strong></p>
                        
                        <h3>Travel and Re-Entry </h3>
                        
                        <ul>
                           
                           <li>Obtain a travel signature on the DS-2019 from the ARO prior to departing the United
                              States anytime during your program duration.&nbsp; Please note that you may not be allowed
                              to re-enter the U.S. without travel authorization.
                           </li>
                           
                           <li>You can travel outside the U.S. and then request re-entry to continue your J program.&nbsp;
                              You will need a valid passport, valid visa, a DS-2019 travel signature by an ARO,
                              and proof of current sickness and injury insurance.
                           </li>
                           
                           <li>If the J visa is expired, the exchange visitor must obtain a new visa before re-entry
                              to the U.S. will be granted. 
                           </li>
                           
                           <li>For travel to a country other than the visitor’s home country, exchange visitors should
                              contact the consulate or embassy of that country for entry requirements and procedures.
                           </li>
                           
                           <li>Travel must be completed before the end date on the DS-2019. </li>
                           
                        </ul>
                        
                        
                        <h3>Ending Your J Exchange Visitor Program </h3>
                        
                        <ul>
                           
                           <li>Report your departure date and reason to the ARO in advance.&nbsp; You must depart the
                              United States within 30 days of completing or ceasing program activities.&nbsp; Overstaying
                              the 30 days is a serious immigration violation that may negatively affect your ability
                              to obtain a new visa or re-enter the U.S. in the future.
                           </li>
                           
                           <li>Exchange visitors who end their program more than 30 days prior to the end date on
                              the DS-2019, who intend to depart the U.S. and not return, must notify the sponsoring
                              department/center and their ARO. 
                           </li>
                           
                           <li>The counting time in J status <strong>BEGINS</strong> from either the begin date on the DS-2019 OR the arrival date on the I-94 card.&nbsp;
                              The counting time in J status <strong>ENDS</strong> when you reach the end date on the DS-2019 OR you reach the maximum limit of your
                              category OR you complete your program objective:
                           </li>
                           
                        </ul>
                        
                        
                        <h3>Government Agency Contacts </h3>
                        
                        <ul>
                           
                           <li>Department of State (DoS): <a href="http://www.state.gov/">http://www.state.gov/</a> 
                           </li>
                           
                           <li>U.S. DoS Office of Exchange Coordination and Designation: (202) 203-5029</li>
                           
                           <li>U.S. Citizenship and Immigration Services (USCIS): <a href="http://www.uscis.gov/portal/site/uscis">http://www.uscis.gov/portal/site/uscis</a> 
                           </li>
                           
                        </ul>
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/student-life/visastatus.pcf">©</a>
      </div>
   </body>
</html>