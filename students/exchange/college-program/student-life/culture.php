<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/student-life/culture.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li><a href="/students/exchange/college-program/student-life/">Student Life</a></li>
               <li>US Customs &amp; Culture</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <div id="google_translate_element"></div>     
                        
                        
                        
                        <h2>Valencia's International College Program with Academic Training<br>
                           at the <em>Walt Disney World© </em>  Resort
                           
                        </h2>
                        
                        <h3>U.S. Customs &amp; Culture</h3>
                        
                        <h4>Culture Shock</h4>
                        
                        <p><em>Culture shock</em> is the term used to describe the anxiety and feelings of surprise, disorientation,
                           or confusion felt when people are required to operate in a culture different from
                           the one in which they live, or in a social environment different from the one in which
                           they grew up. During the orientation, we will review the common elements of culture
                           shock and what students can do adjust and thrive in a new environment.
                        </p>
                        
                        <h4>American Culture</h4>
                        
                        <h5>Time</h5>
                        
                        <p>It is very important in the U.S. to be on time for appointments, classes, work, etc.&nbsp;
                           Arriving late is considered rude and may result in penalties for classes and work.&nbsp;
                           
                        </p>
                        
                        <h5>Conversation Etiquette</h5>
                        
                        <p>It is considered rude in the U.S. to interrupt someone when they are talking.&nbsp; Wait
                           until they are finished to make your comment or ask a question.
                        </p>
                        
                        <p>"Safe" topics to discuss:</p>
                        
                        <ul>
                           
                           <li>The weather</li>
                           
                           <li>Classes and jobs</li>
                           
                           <li>Travel experiences</li>
                           
                           <li>Sports</li>
                           
                           <li>Music and movies</li>
                           
                           <li>Fashion, shopping, and clothes</li>
                           
                        </ul>
                        
                        
                        <p>Topics to avoid:</p>
                        
                        <ul>
                           
                           <li>Money (how much one earns or how much their house, etc., cost).</li>
                           
                           <li>Family problems</li>
                           
                           <li>Religion</li>
                           
                           <li>Politics</li>
                           
                        </ul>
                        
                        
                        <p>"Personal Space"</p>
                        
                        <p>In the U.S., people value their space and do not like it when someone stands too close
                           to them when talking. A typical form of American greeting is a firm handshake (extend
                           the right hand) when meeting someone for the first time. When you greet friends, it
                           is common to hug them.
                        </p>
                        
                        
                        <p>Public Displays of Affection </p>
                        
                        <p>Public displays of affection are frowned upon (passionate kissing or any other sexual
                           behavior in public).
                        </p>
                        
                        <p>Making inappropriate comments or touching someone in a personal manner can be considered
                           sexual harassment and is not acceptable.&nbsp; What may seem like an innocent comment or
                           gesture on your part can be interpreted differently by someone else.
                        </p>
                        
                        
                        <h4>American Culture (Classroom Behavior)</h4>
                        
                        <p>Education in the U.S. is "learning centered", meaning that students are encouraged
                           to listen, take notes, read, think critically, express different perspectives in class,
                           participate in discussions, and demonstrate understanding of a topic as opposed to
                           memorizing what they hear or reading and taking a test on that topic.
                        </p>
                        
                        <p>Interaction and teamwork are common in the classroom, and questions and discussion
                           are encouraged.&nbsp; Many instructors will tell you that there is no such thing as a “stupid
                           question” and if you are wondering about something, chances are that other students
                           are wondering the same thing but are afraid to ask.&nbsp; Instructors are always available
                           after class or through email if you want to ask a question privately.
                        </p>
                        
                        <p>Use of Cell Phones</p>
                        
                        <p>It is considered impolite to talk, text message, or do anything which conveys the
                           impression that you are not paying attention to the instructor or to other students
                           when they are talking or making a presentation.&nbsp; Cell phones have become a major problem
                           in classrooms, and many teachers will deduct points from your grade if you use your
                           cell phone in any way during class or even have it visible.&nbsp; Cell phones should always
                           be turned off and put out of sight in class.
                        </p>
                        
                        <p>Cheating</p>
                        
                        <p>Cheating is not tolerated in the U.S. classroom and has repercussions.&nbsp; Many times
                           you will fail the class if you are caught cheating.&nbsp;&nbsp; Even though students are often
                           encouraged to work together in groups or teams, tests and quizzes should be your own
                           work and copying or discussing answers with another student is considered cheating.
                        </p>
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/student-life/culture.pcf">©</a>
      </div>
   </body>
</html>