<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/student-life/insurance.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li><a href="/students/exchange/college-program/student-life/">Student Life</a></li>
               <li>Insurance &amp; Healthcare</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        <div id="google_translate_element"></div>
                        
                        
                        
                        
                        <h2>Valencia's International College Program with Academic Training at the <em>Walt Disney World© </em>  Resort
                        </h2>
                        
                        <h3>Insurance &amp; Healthcare </h3>
                        
                        <h4>Sickness and Injury Insurance</h4>
                        
                        <p>Your insurance is through <em>Insurance for Students</em> (provider is <em>United Healthcare</em>), and during your orientation  you will be given  your insurance card which provides
                           insurance coverage through the Care Plus plan for the six months of your JEV program.&nbsp;
                           You must present this card when receiving any covered medical service.&nbsp; You may be
                           required to pay a deductible amount.&nbsp; 
                        </p>
                        
                        <p>Florida Hospital Centra Care (near Downtown Disney)&nbsp;&nbsp;&nbsp;&nbsp; (They will provide transportation
                           if needed.)<br>12500 S. Apopka Vineland Rd.<br>Orlando, FL 32836<br>
                           Phone:&nbsp; 407-934-2273
                           <br>
                           
                        </p>
                        
                        <p>Your contact for insurance questions is India Turkell at 800-356-1235 or <a href="mailto:india@insuranceforstudents.com">india@insuranceforstudents.com</a>.
                        </p>
                        
                        
                        <h4>Property Insurance</h4>
                        
                        <p>Valencia Community College, the Walt Disney World Company, or Price Management cannot
                           be held responsible if anything happens to your personal property during this program.&nbsp;
                           This includes:
                        </p>
                        
                        <ul>
                           
                           <li>Theft from your apartment, whether it was locked or unlocked</li>
                           
                           <li>Water damage from any source (Florida’s hurricane season is June through November)</li>
                           
                           <li>Fire or smoke damage</li>
                           
                           <li>Any other loss or damage</li>
                           
                        </ul>
                        
                        <p><strong>ALWAYS lock up your valuables – passport, DS-2019, cash, computer, Ipod, camera, jewelry,
                              etc., in your locker when you are not using them.&nbsp; Do not trust that everyone who
                              enters your apartment is a “friend” who shares the same values as you.&nbsp; Even honest
                              people sometimes cannot resist temptation.&nbsp; Do not provide temptation to others by
                              leaving your valuable property unlocked.</strong></p>
                        
                        <p>Valencia strongly recommends that you purchase personal property insurance from any
                           insurance company of your choice.&nbsp; It is your responsibility to arrange for this insurance;
                           renters’ insurance is available for as low as $15 a month.
                        </p>
                        
                        <h4>Healthcare</h4>
                        
                        <p>Health care in the U. S. is complicated, even for U. S. residents.&nbsp; The American health
                           care system is different from many other countries, where the government may pay all
                           or part of an individual's health care costs.&nbsp; Medical care is very expensive in the
                           U.S., and people are expected to pay for it themselves.&nbsp; Even a few days in the hospital
                           can cost thousands of dollars.&nbsp; There will be separate bills from the hospital, from
                           every doctor who examines the patient, for every laboratory procedure, and for all
                           medicines.&nbsp; Americans purchase health insurance to protect them from these high health
                           care costs, and all Exchange Visitors are required to have it.&nbsp; Do not view health
                           insurance as an additional, unnecessary expense!&nbsp; Without it you would have to pay
                           thousands of dollars in medical bills if you had a serious accident or illness that
                           required hospitalization.
                        </p>
                        
                        <p>In the U. S. it is customary to ask your doctor questions about your diagnosis and
                           treatment.&nbsp; You may want to have a friend accompany you to a medical appointment in
                           order to help listen and ask questions, assist with translation (if necessary), and
                           provide overall support. Many medical offices and pharmacies in the Orlando area have
                           Spanish speakers on staff and sometimes they can assist speakers of other languages.&nbsp;
                           So do not be afraid to ask if someone there can talk with you in your native language
                           so that you can ask questions and make sure you understand everything they tell you.
                        </p>
                        
                        
                        
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/student-life/insurance.pcf">©</a>
      </div>
   </body>
</html>