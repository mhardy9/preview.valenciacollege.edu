<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/student-life/index.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li>Student Life</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               		
               		
               <div class="container margin-30" role="main">
                  			
                  <div class="row">
                     				
                     <div class="col-md-12">
                        <div id="google_translate_element"></div>
                        
                        
                        					
                        <h2>Valencia's International College Program with Academic Training<br>
                           						at the <em>Walt Disney World© </em>  Resort
                           					
                        </h2>
                        					
                        <h2>Student Life</h2>
                        					
                        <p><a href="#Contacts">College Contacts</a><br>
                           						<a href="#resources">Community Resources &amp; Local Attractions</a><br>
                           						<a href="#driverslicense">Driver's License &amp; Social Security Card</a><br>
                           						<a href="#safety">Safety &amp; Emergency Procedures</a><br>
                           						<a href="#valenciabenefits">Valencia Benefits &amp; Resources</a></p>
                        					
                        <h3>College Contacts/Responsible Officers<a name="Contacts" id="Contacts"></a></h3>
                        					
                        <div class="row">
                           						
                           <div class="col-md-4 box_style_2">
                              							
                              <p><strong>Karla Martell</strong> <br>
                                 								Alternate Resposible Officer<br>
                                 								Program Coordinator, Continuing Education<br>
                                 								Valencia College<br>
                                 								1800 S Kirkman Road, Building 10<br> 
                                 								Orlando, FL 32811<br>
                                 								<i class="far fa-phone fa-fw"></i>Telephone: 407-582-6717<br>
                                 								<i class="far fa-fax fa-fw"></i>Fax Number: 407-582-6610<br>
                                 								Email: <a href="mailto:solin@valenciacollege.edu">kmartell3@valenciacollege.edu</a>
                                 							
                              </p>
                              						
                           </div>
                           						
                           <div class="col-md-4 box_style_2">
                              							
                              <p><strong>Tatiana Tyler </strong><br>
                                 								Alternate Responsible Officer<br>
                                 								Academic Advisor<br>
                                 								Valencia College<br>
                                 								1800 S Kirkman Road, Building 10<br> 
                                 								Orlando, FL 32811<br>
                                 								
                                 								<i class="far fa-phone fa-fw"></i>Telephone: 407-582-6601<br>
                                 								<i class="far fa-fax fa-fw"></i>Fax Number: 407-582-6610<br>
                                 								Email:&nbsp;&nbsp;<a href="mailto:ttyler4@valenciacollege.edu">ttyler4@valenciacollege.edu</a>
                                 							
                              </p>
                              						
                           </div>
                           
                           						
                           <div class="col-md-4 box_style_2">
                              							
                              <p><strong>Talia Popovski</strong> <br>
                                 								Director, International Student Recruitment and Global Engagement <br>
                                 								Valencia College<br>
                                 								1800 S Kirkman Road, Building 10<br> 
                                 								Orlando, FL 32811<br>
                                 								
                                 								<i class="far fa-phone fa-fw"></i>Telephone: 407-582-5031<br>
                                 								<i class="far fa-fax fa-fw"></i>Fax Number: 407-582-1419<br>
                                 								Email:&nbsp;<a href="mailto:tpopovski1@valenciacollege.edu">tpopovski1@valenciacollege.edu</a>
                                 							
                              </p>
                              						
                           </div>
                           					
                        </div>
                        					
                        <h3>Community Resources &amp; Local Attractions<a name="resources" id="resources"></a></h3>
                        					<img alt="Students on staircase at Walt Disney World" src="/students/exchange/images/Staircase.jpg" class="img-responsive">
                        					
                        <p>While you are living in Orlando, Florida, we want to make sure that you have easy
                           access to the resources you need for everyday life, as well as information on things
                           to do and local attractions. The <a href="/students/exchange/why-valencia/resources.php" target="_blank">community resources</a> page has information on transportation, insurance, medical care, libraries and other
                           public services, as well as links to event calendars, shopping malls and outlets,
                           theme parks, festivals,  performing arts, museums, and galleries.&nbsp; 
                        </p>
                        
                        					
                        <h3>Driver's License &amp; Social Security Card<a name="driverslicense" id="driverslicense"></a>
                           					
                        </h3>
                        					
                        <p>Driver's License: If you want to drive a car, you will need a Florida Driver's License
                           issued by the Department of Motor Vehicles (DMV).&nbsp; To obtain a license, you must:
                           
                        </p>
                        					
                        <ul>
                           						
                           <li>Pass a written test and a driving test </li>
                           						
                           <li>Bring your DS-2019, visa, passport, I-94 card, and social security number (if issued).&nbsp;
                              Check when you make your appointment to see if other documents are required.
                           </li>
                           					
                        </ul>
                        					
                        <p><strong>NOTE: </strong>Florida law defines a bicycle as a vehicle so bicyclists are subject to many of the
                           same responsibilities as operators of motor vehicles and subject to the same citations
                           and fines for violating traffic law.&nbsp; For more information, visit:&nbsp; <a href="http://www.dmvflorida.com/">DMVFlorida.com</a>.&nbsp; 
                        </p>
                        
                        					
                        <p>Social Security Card: After you have been in the U.S. for ten days, a Disney representative
                           will take you to the local Social Security Administration so you can apply for your
                           Social Security card, which is necessary for employment in the U.S. 
                        </p>
                        					
                        <h3>Safety &amp; Emergency Procedures<a name="safety" id="safety"></a></h3>
                        					
                        <p><strong>Personal Safey</strong></p>
                        					
                        <p><strong><u></u></strong>Valencia has a Security department as well as a website designed to create an environment
                           that is safe for its students.&nbsp; For detailed information on safety and security go
                           to: <a href="/students/security/">Valencia Security</a><u>.&nbsp; </u>Below are guidelines for staying safe.
                        </p>
                        					
                        <p>Everyday Living </p>
                        					
                        <ul>
                           						
                           <li>Keep emergency phone numbers accessible. </li>
                           						
                           <li>Lock all doors and windows every time you leave your apartment.</li>
                           						
                           <li>Keep apartment and car keys on separate rings.</li>
                           						
                           <li>Do not lend your keys to people you do not know well.</li>
                           						
                           <li>Always ask service people to identify themselves before allowing them to enter your
                              apartment.
                           </li>
                           						
                           <li>Get to know your neighbors so you can help each other.</li>
                           						
                           <li>Do not keep large sums of money, jewelry, or valuable items in plain view in your
                              apartment, room, or vehicle.
                           </li>
                           						
                           <li>Leave spare keys with trusted neighboors, not under a doormat or in a flower planter.<br>
                              						
                           </li>
                           					
                        </ul>
                        					
                        <p><u>Elevators</u></p>
                        					
                        <p>Try to avoid entering elevators occupied by strangers. If you are waiting for an elevator
                           with a stranger, stand away from the door to avoid being pushed inside. Get off on
                           the next floor if you feel uneasy. Hit the alarm button if you are accosted on the
                           elevator.
                        </p>
                        					
                        <p><strong><u><br>
                                 						</u></strong><u>When Walking </u></p>
                        					
                        <p><strong><u></u></strong>Avoid dark, vacant, or deserted areas; use well-lit and traveled areas. Avoid walking
                           alone with a music player and earphones--you can't hear people approaching from behind.
                        </p>
                        					
                        <ul>
                           						
                           <li>Avoid walking or jogging alone, especially at night. Ask a friend to jog with you.
                              
                           </li>
                           						
                           <li>Dress in clothes and shoes that will not hamper movement.</li>
                           						
                           <li>Be alert and aware of your surroundings at all times.</li>
                           						
                           <li>Carry a noise-making device with you at all times and use it if you suspect you are
                              in danger, then move to a lighted area or building and raise a commotion. Call 911
                              or activate a call box in the event of an emergency.
                           </li>
                           					
                        </ul>
                        					
                        <p><u>If You Sense You Are in Trouble</u></p>
                        					
                        <ul>
                           						
                           <li>Move away from the potential threat if possible.</li>
                           						
                           <li>Join any group of people nearby; cross the street and increase your pace.</li>
                           						
                           <li>If a threatening situation is imminent, and people are close by, help, yell, scream,
                              or make a commotion in any way you can to get their attention. 
                           </li>
                           						
                           <li>If you are facing an armed criminal, you may minimize the risk of injury if you comply
                              with his/her demands. However, if your life is in immediate danger, use any defense
                              you can to get away.
                           </li>
                           						
                           <li>Dial 911 immediately and give a description of the suspect.</li>
                           					
                        </ul>
                        					
                        <p>When Driving </p>
                        					
                        <ul>
                           						
                           <li>Make sure no one is hiding in your vehicle. </li>
                           						
                           <li>Drive on well-traveled streets and keep your car in gear while stopped.</li>
                           						
                           <li>Keep doors locked, windows shut, and keep valuables out of sight, either covered or
                              in the trunk.
                           </li>
                           						
                           <li>Open the hood and stay inside if your car breaks down. If someone stops to help, do
                              not open your window or door, but have them call for assistance.
                           </li>
                           						
                           <li>Ask someone for specific directions before you leave if you do not know the location
                              of your destination.
                           </li>
                           						
                           <li>Do not pull over until you find a well-lit public area where you can stop and ask
                              directions if you get lost.
                           </li>
                           						
                           <li>Drive to a well-lit public area and call the police if you suspect you are being followed.</li>
                           					
                        </ul>
                        					
                        <p><u>Because carjacking has become a growing phenomenon, beware of people who:</u></p>
                        					
                        <ul>
                           						
                           <li>yell, honk, and point at your car as if something is wrong. If your car breaks down,
                              stay inside and lock the doors. If anyone approaches to help, crack the window and
                              ask them to call the police. Ask nonuniformed and uniformed people to show identification;
                           </li>
                           						
                           <li>motion and ask you to stop and lend assistance. If you want to assist someone whose
                              car has broken down, go to the nearest phone and call the police;
                           </li>
                           						
                           <li>flash headlights at you;</li>
                           						
                           <li>bump your vehicle from behind. If you think you were bumped intentionally, signal
                              the other person to follow you to the nearest police station.
                           </li>
                           					
                        </ul>
                        					
                        <p>Most importantly, if a person with a weapon confronts you and wants your vehicle,
                           give it up. No car is worth being injured or losing your life over.
                        </p>
                        					
                        <p><u>Personal Information</u></p>
                        					
                        <p>Not only can thieves steal your physical possessions, they can also use your personal
                           information to steal from you. Many times you unknowingly provide them with this information.
                           Here are some items you should <strong>never</strong> provide to strangers over the phone: 
                        </p>
                        					
                        <ul>
                           						
                           <li>Your credit card number or expiration date, unless you know the company is reputable.
                              Thieves can use these numbers to make purchases of their own.
                           </li>
                           						
                           <li>Your checking account number can be used to withdraw money from your account.</li>
                           						
                           <li>Your telephone calling card number can be used to charge unwanted calls to your account.</li>
                           						
                           <li>Your Social Security number can be used by someone else to find out information about
                              you or to establish a false identity. Only give it out if it is legally required.
                           </li>
                           						
                           <li>Your driver's license number may allow someone to obtain information about you, such
                              as your physical description.
                           </li>
                           						
                           <li>Health information, such as your insurance, medical, mental history, and doctor can
                              be used to fraudulently obtain health care and health services.
                           </li>
                           						
                           <li>Information about your apartment security system, such as whether or not you have
                              one.
                           </li>
                           						
                           <li>Information about those who live with you or whether you live alone.</li>
                           						
                           <li>Financial information, such as annual income. </li>
                           					
                        </ul>
                        
                        
                        					
                        <p><strong>Emergency Procedures</strong></p>
                        					
                        <ul>
                           						
                           <li>In the event of an emergency, dial 9-1-1.&nbsp; Be sure to have your address location.&nbsp;
                              This number is used to dispatch fire, police, and ambulance and should only be called
                              in the event of an emergency.
                           </li>
                           						
                           <li>Hospitals in Orlando are Orlando Healthcare and Florida Hospital.&nbsp; Florida Hospital
                              has walk-in Centra Care Clinics in many locations.&nbsp; For more information visit:&nbsp; <a href="http://www.centracare.org/">www.centracare.org</a>.
                           </li>
                           					
                        </ul>
                        
                        					
                        <p><u>Weather Cancellations</u></p>
                        					
                        <ul>
                           						
                           <li>In the event of a hurricane or other inclement weather, you can listen to local radio
                              and television stations or college closing advisories.&nbsp; You can also call Valencia's
                              emergency number at:&nbsp; <i class="far fa-phone fa-fw"></i>407-582-1691 or 1692 or visit the college's website at <a href="https://valenciacollege.edu">valenciacollege.edu</a> or <u>www.valenciacollege.info</u>.
                           </li>
                           						
                           <li>Make up time for classes missed will be scheduled when you return to class.</li>
                           					
                        </ul>
                        
                        					
                        <p><u>Disaster Preparedness</u></p>
                        					
                        <ul>
                           						
                           <li>In the event of a major emergency, monitor local radio and television stations and
                              follow the advice of local emergency officials.
                           </li>
                           						
                           <li>If you must evacuate to a new location, make sure you take important documents with
                              you, such as: 
                           </li>
                           						
                           <ul>
                              							
                              <li>Passport </li>
                              							
                              <li>Visa </li>
                              							
                              <li>SEVIS-issued DS 2019 </li>
                              							
                              <li>Social Security card </li>
                              							
                              <li>Financial records </li>
                              							
                              <li>Plane tickets</li>
                              							
                              <li>Health insurance card </li>
                              							
                              <li>Checks and credit cards </li>
                              							
                              <li>Contact numbers or email addresses to keep in touch with your designated school officials:
                                 Responsible Officer (RO) or Alternate Responsible Officer (ARO)
                              </li>
                              							
                              <li>SEVP contact information: &nbsp;email SEVP@dhs.gov </li>
                              							
                              <li>or 1-800-961-5294 </li>
                              						
                           </ul>
                           					
                        </ul>
                        
                        					
                        <h3>Valencia Benefits &amp; Resources<a name="valenciabenefits" id="valenciabenefits"></a></h3>
                        					
                        <ul>
                           						
                           <li>As a JEV student, you will receive a Valencia Student ID card.&nbsp; This identification
                              card will give you access to all Valencia events as well as libraries, computer labs,
                              and writing centers at any college campus.
                           </li>
                           						
                           <li>You can also get discounts on transportation, restaurants, and movie theaters.</li>
                           					
                        </ul>
                        
                        					
                        <h4>Libraries and Other Learning Resources</h4>
                        					
                        <h5><a href="/locations/west/" target="_blank">West Campus</a></h5>
                        					
                        <h6>Computer Access Lab</h6>
                        					
                        <p><i class="far fa-phone fa-fw"></i>407-582-1646<br>
                           						<i class="far fa-building fa-fw"></i>6-101
                        </p>
                        					
                        <p>The Computer Access Lab is available to any Valencia student with an active Atlas
                           account.&nbsp; The Computer Access Lab offers a wide variety of software that can help
                           you complete your assignments.
                        </p>
                        					
                        <h6>Library</h6>
                        					
                        <p><i class="far fa-phone fa-fw"></i>407-582-1432<br>
                           						<i class="far fa-building fa-fw"></i>6-201
                        </p>
                        					
                        <p>Instructional support, research assistance, research computers, computer access lab,
                           and wireless access.
                        </p> 
                        					
                        <h6>Writing Center</h6>
                        					
                        <p><i class="far fa-phone fa-fw"></i>407-582-1812 <br>
                           						<i class="far fa-building fa-fw"></i>5-155A
                        </p>
                        					
                        <p>The <a href="/students/learning-support/">Writing Center</a> is staffed with faculty-level English, Reading, and EAP instructors. The instructors
                           go over essays and other writing assignments with students. Open to all students.
                           Appointments are required.
                        </p> 
                        
                        					
                        <h5><a href="/locations/osceola/" target="_blank">Osceola Campus</a></h5>
                        					
                        <h6>Learning Center</h6>
                        					
                        <p><i class="far fa-phone fa-fw"></i>407-582-4112<br>
                           						<i class="far fa-building fa-fw"></i>3-100
                        </p>
                        					
                        <p> The Learning Center is the primary site for nonclassroom based learning at the Osceola
                           Campus and includes peer tutoring, communication labs, math labs, IT support, and
                           open computer lab. It is a one-stop center where students can access technology and
                           receive personal assistance.
                        </p>
                        				
                        <h6>Library</h6>
                        				
                        <p><i class="far fa-phone fa-fw"></i>407-582-4154<br>
                           					<i class="far fa-building fa-fw"></i>1-104
                        </p>
                        				
                        <p>Open to all students. Internet access and online databases available for research
                           purposes. Online databases can be accessed by remote access.
                        </p> 
                        				
                        <h6>Language Lab</h6>
                        				
                        <p><i class="far fa-phone fa-fw"></i>407-582-4903/4830<br>
                           					<i class="far fa-building fa-fw"></i>2-244
                        </p>
                        				
                        <p> This lab provides students with ESL tools as well as assistance to help them succeed.
                           
                        </p>
                        
                        			
                     </div>
                     		
                  </div>
                  		
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/student-life/index.pcf">©</a>
      </div>
   </body>
</html>