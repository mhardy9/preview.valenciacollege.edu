<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/description.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li>Program Description</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div id="google_translate_element"></div>        
                        
                        
                        
                        <h2>Valencia's International College Program with Academic Training at the <em>Walt Disney World</em><em>© </em>  Resort
                        </h2>
                        
                        <h2>Program Description</h2>
                        
                        <p><a href="#academic_courses_training">Academic Courses and Academic Training Practicum</a><br>
                           <a href="#cultural_immersion">Culture Immersion Activities</a> <br>
                           <a href="#scheduling">Scheduling</a><br>
                           <a href="#program_fees_income">Program Fees and Income</a></p>
                        
                        <h3>Academic Courses and Academic Training Practicum</h3>
                        
                        <p>As part of the program, participants are enrolled as full-time students at Valencia
                           and will receive a total of 12 credits in the following courses:&nbsp;&nbsp; 
                        </p>
                        
                        <ul>
                           
                           <li>
                              Business Management Course (3 credit hours):&nbsp;&nbsp; <img alt="Business Management" src="/students/exchange/images/DisneyManagementClass107.jpg" class="img-responsive">
                              
                           </li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>This course teaches students the skills they will need as managers in today’s global
                              economy.&nbsp; Course topics include customer service, professional image, teambuilding,
                              diversity and culture, presentation skills, decision making, communication skills,
                              management skills, and attitude.&nbsp; 
                           </p>
                           
                        </blockquote>
                        
                        
                        <ul>
                           
                           <li>
                              Academic Training Practicum at the <em>Walt Disney World </em>Resort (3 credit hours):&nbsp;&nbsp; 
                           </li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>This internship course focuses on specific <a href="https://www.disneyinternationalprograms.com/j1/experience_intro.html">training roles </a>at the <em>Walt Disney World</em> Resort including on-the-job learning activities.&nbsp; Students will earn an hourly rate
                              of pay. &nbsp; 
                           </p>
                           
                        </blockquote>            
                        
                        
                        <ul>
                           
                           <li>
                              Academic Training Online Course (3 credit hours):&nbsp;&nbsp;&nbsp; 
                           </li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>This professional development course guides students through their internship course
                              via reflective assignments in constructing a personal career development plan.&nbsp; Once
                              the academic training practicum has begun, the faculty member meets with students
                              to discuss program goals and assignments.&nbsp; Assignments are monitored via email and
                              chat rooms for the duration of the program.&nbsp;&nbsp; 
                              
                           </p>
                           
                        </blockquote>
                        
                        <ul>
                           
                           <li>
                              <a href="https://www.disneyinternationalprograms.com/j1/education_collegiate.html" target="_blank">Disney Collegiate Course (3 credit hours):</a>&nbsp;&nbsp; <img alt="Restaurant Seaters" src="/students/exchange/images/FBseaters.jpg" class="img-responsive">
                              
                           </li>
                           
                        </ul>
                        
                        <blockquote>
                           
                           <p>Students can choose from any one of nine Disney Collegiate Courses:&nbsp;&nbsp; Advanced Studies
                              in Hospitality Management, Corporate Analysis, Corporate Communication, Creativity
                              and Innovation, Human Resource Management, Interactive Learning Program, Experiential
                              Learning, Marketing You, and Organizational Leadership. Disney Collegiate Courses
                              are recommended for credit by the American Council on Education.&nbsp; &nbsp; &nbsp; 
                           </p>
                           
                           <p>Students receive a certificate from Valencia upon successful completion of the program,
                              and a transcript with letter grades is provided to their home institution.
                           </p>
                           
                        </blockquote>
                        
                        
                        <h3>Cultural Immersion Activities<a name="cultural_immersion" id="cultural_immersion"></a>
                           
                        </h3>
                        
                        <p>A primary objective of the J Exchange Visa is to provide students with cultural experiences
                           which include the opportunity to meet American students, learn about college life
                           in the United States, and learn about our region’s culture and history.&nbsp; &nbsp;As part
                           of these activities, we look forward to learning about your culture and benefiting
                           from an exchange of ideas and knowledge.
                        </p>
                        
                        <p>Examples of scheduled activities include such things as:</p>
                        
                        <ul>
                           
                           <li>Campus tour and meeting with Valencia faculty and students</li>
                           
                           <li>Visit to the <a href="http://www.thehistorycenter.org/">Orange County Regional History Center </a>
                              
                           </li>
                           
                           <li><a href="http://www.scenicboattours.com/home.htm">Winter Park Boat Tour</a></li>
                           
                           <li><a href="http://www.seaworld.org/education-programs/swf/tour/index.htm">SeaWorld backstage tour</a></li>
                           
                        </ul>            
                        <h3>
                           <img alt="SeaWorld Manatee Sign Student Group" src="/students/exchange/images/SeaWorld-Manateesign-group_000.JPG" class="img-responsive"><img alt="Winter Park Boat Tour-Student Group" src="/students/exchange/images/WinterParkBoatTour-group_000.JPG" class="img-responsive"><br>
                           
                        </h3>
                        
                        <h3>Scheduling<a name="scheduling" id="scheduling"></a></h3>
                        
                        <p>Students will spend approximately six to eight hours in class per week, plus approximately
                           four hours per week for homework. Courses will not<img alt="Bus to work" src="/students/exchange/images/BustoWork_000.jpg" class="img-responsive"> be scheduled during major U.S. holiday weeks.&nbsp; During the academic training practicum,
                           students will be scheduled at the <em>Walt Disney World </em>Resort between 30 and 37.5 hours per week except during peak seasons when they will
                           be scheduled more than 37.5 hours.&nbsp; 
                        </p>
                        
                        <h3>&nbsp;</h3>
                        
                        <h3>Program Fees and Income<a name="program_fees_income" id="program_fees_income"></a> 
                        </h3>
                        
                        <p>Fees</p>            
                        
                        <p>Application Fee: $50 </p>
                        
                        <p>Tuition and Fees: $2450 </p>
                        
                        <p>Injury and Sickness Insurance: $563</p>
                        
                        <p>SEVIS Fee: $180 </p>
                        
                        <p>Disney Program Assessment Fee: $154.50 payable online after acceptance.</p>
                        
                        <p>Living Expenses (approximate):  
                           
                        </p>            
                        
                        <ul>
                           
                           <li>
                              <em>You must have enough money when you start the program for initial expenses and to
                                 support yourself until you receive your first paycheck in the second week.  We recommend
                                 that you have at least $400 for this purpose. </em>Expect to pay for such initial expenses as bedding and other apartment items ($100-$200),
                              shoes and  other clothing expenses ($50-$100), food, and personal items.
                           </li>
                           
                           <li>Housing and transportation costs: 
                              
                              
                              Disney will deduct the cost of your housing and bus transportation (approximately
                              $100/week) from your weekly paychecks. 
                              
                              
                              The exact amount will depend on the size of your apartment.&nbsp; 
                           </li>
                           
                           <li>Food: $75 per week </li>
                           
                           <li>Personal expenses: $50 per week </li>
                           
                           <li>Course materials: $6-$120 depending on the course </li>
                           
                        </ul>            
                        
                        <p><span>Income</span> 
                        </p>
                        
                        <p> You will earn an hourly wage of at least minimum wage (currently $7.67).&nbsp; You will
                           be scheduled a minimum of 30 hours per week and a maximum of 37.5 hours per week when
                           classes are in session.&nbsp; During holiday periods, classes are not in session and you
                           can expect to work more than 37.5 hours per week.&nbsp; Thus, a student in a six-month
                           program will gross a minimum of $5263 ($7.67/hour x 30 hours/week x 24 weeks.)&nbsp; Be
                           aware that U.S. income taxes and your housing fees will be deducted from each paycheck,
                           which will reduce the total amount of money that is available to you for spending.
                           
                        </p>
                        
                        <p>The <em>Walt Disney World</em> Resort is required by law to withhold income taxes from your paychecks.&nbsp; By April
                           15 of the next year  you must submit a federal income tax return to determine whether
                           you owe more taxes or are owed a refund by the U. S. government.&nbsp; Valencia and the
                           <em>Walt Disney World</em> Resort will provide you with information on how to file your U. S. income tax return.
                           &nbsp;&nbsp;&nbsp;
                        </p>
                        
                        
                        
                        <h3>&nbsp;</h3>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/description.pcf">©</a>
      </div>
   </body>
</html>