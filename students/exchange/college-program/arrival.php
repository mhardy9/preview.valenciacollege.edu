<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Visitor Exchange Program | Valencia College | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/exchange/college-program/arrival.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/exchange/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Exchange Visitor Program</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/exchange/">Exchange</a></li>
               <li><a href="/students/exchange/college-program/">College Program</a></li>
               <li>Arrival Information</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               	
               		
               <div class="container margin-30" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-12">
                        
                        <div id="google_translate_element"></div>
                        
                        
                        
                        
                        <h2>Valencia's International College Program with Academic Training <br>
                           at the <em>Walt Disney World </em>  Resort
                        </h2>
                        
                        <h2>Arrival Information</h2>
                        
                        <h3>First Week</h3>
                        
                        <p>During your first few days in Orlando, you will be very busy getting settled into
                           your apartment, attending orientations, and getting prepared for your courses and
                           your academic training. Below is a list of activities you will undertake during your
                           first week. The order of these events may change but they are listed here to give
                           you an approximate idea of what to expect. 
                        </p>
                        
                        <h4>Sunday</h4>
                        
                        <ul>
                           
                           <li>Check into your housing complex and receive an infomation packet</li>
                           
                           <li>Move into your apartment </li>
                           
                           <li>Go grocery shopping</li>
                           
                        </ul>  
                        		
                        <h4>Monday</h4>
                        		
                        <ul>
                           
                           <li>Attend all day welcome/information sessions</li>
                           
                           <li>Bring the following documents for verification:
                              
                              <ul>
                                 
                                 <li>Passport with J-1 visa</li>
                                 
                                 <li>I-94 card</li>
                                 
                                 <li>DS-2019</li>
                                 
                              </ul>
                              
                           </li>
                           
                           <li>Complete the I-9 Form (verifying employment eligibility) </li>
                           
                           <li>Complete the W-4 Form (for taxes)</li>
                           
                           <li>Receive information on policies</li>
                           
                           <li>Housing overview </li>
                           
                           <li>Placement paperwork and payroll deduction</li>
                           
                           <li>Get Disney IDs</li>
                           
                           <li>Disney Look</li>
                           
                           <li>Get academic training schedule</li>
                           
                           <li>Benefits paperwork</li>
                           
                        </ul>
                        		
                        <h4>Tuesday</h4>
                        
                        <ul>
                           
                           <li><a href="/students/exchange/orientation/index.html">Valencia College J Exchange Visitor Orientation</a></li>
                           
                        </ul>
                        		
                        <h4>Wednesday</h4>
                        		
                        <ul>
                           
                           <li>Attend Disney Traditions</li>
                           
                           <li>Collect costume</li>
                           
                        </ul>
                        		
                        <h4>Thursday</h4>
                        		
                        <ul>
                           
                           <li>Start academic training </li>
                           
                        </ul>
                        
                     </div>
                     
                  </div>
                  
               </div>
               	
               <hr class="styled_2">
            </div>
         </div>
      </div>
      			<script type="text/javascript" src="/_resources/js/google-translate.js"></script>
      			<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?><div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/exchange/college-program/arrival.pcf">©</a>
      </div>
   </body>
</html>