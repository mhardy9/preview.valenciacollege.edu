<!DOCTYPE HTML>
<html lang="en-US">
   <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content=", college, school, educational">
      <meta name="description" content=" | Valencia College">
      <title>Safety, Security &amp; Risk Management | Valencia College</title>
      <meta name="Description" content="Change this to match title">
      <meta name="Keywords" content="college, school, educational">
      <meta name="Author" content="Valencia College"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/headcode.inc"); ?><script defer src="/_resources/css/fontawesome/js/fontawesome-all.js"></script><script>
					var page_url="https://preview.valenciacollege.edu/students/security/compliance-and-safety.php";
				</script></head>
   <body><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/ie-shim.inc"); ?>
      <div id="preloader">
         <div class="pulse"></div>
      </div>
      <!-- Pulse Preloader -->
      <!-- Return to Top --><a href="javascript:" id="return-to-top" aria-label="Return to top"><i class="fas fa-chevron-up" aria-label="Return to top" aria-hidden="true"></i></a>
      <!-- <header class="valencia-alert"><iframe></header> -->
      <!-- ChromeNav Header================================================== --><div class="header header-chrome desktop-only">
         <div class="container">
            <nav>
               <div class="row"><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/header-links.inc"); ?></div>
            </nav>
         </div>
      </div>
      <div class="header header-site">
         <div class="container">
            <div class="row">
               <nav class="col-xs-12" role="navigation" aria-label="Subsite Navigation"><a class="cmn-toggle-switch cmn-toggle-switch__htx open_close" href="javascript:void(0);"><span>Menu mobile</span></a><div class="site-menu"><a href="#" class="open_close" id="close_in" aria-label="close"><i class="icon_close"></i></a><?php include($_SERVER['DOCUMENT_ROOT'] . "/students/security/_menu.inc"); ?></div>
               </nav>
            </div>
         </div>
      </div>
      <div class="sub_header bg_interior">
         <div id="intro_txt">
            <h1>Safety, Security &amp; Risk Management</h1>
            <p>
               		
               		
            </p>
         </div>
      </div>
      <div id="route" role="navigation" aria-label="breadcrumb">
         <div class="container">
            <ul>
               <li><a href="/">Valencia College</a></li>
               <li><a href="/students/">Students</a></li>
               <li><a href="/students/security/">Security</a></li>
               <li>Safety, Security &amp; Risk Management</li>
            </ul>
         </div>
      </div>
      <div>
         <div class="container-fluid margin-60">
            <div class="row">
               <div class="container margin-60" role="main">
                  
                  <div class="row">
                     
                     <div class="col-md-9">
                        
                        <h2>Compliance and Safety</h2>
                        
                        <p><strong>Our Mission</strong></p>
                        Our mission is to ensure the safety of the Valencia College students, employees and
                        visitors through the interpretation of regulations, codes and standards.
                        
                        <div>
                           
                           <h3><strong>Compliance &amp; Safety Department</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><strong>Compliance &amp; Safety Department</strong></p>
                                 
                                 <p>The Compliance &amp; Safety Department is comprised of a Compliance Inspector, Sr. and
                                    a Compliance Inspector. The Compliance Inspectors are responsible for regulatory compliance
                                    at the federal, state, and local levels.
                                 </p>
                                 
                                 <p>The Compliance Inspectors are responsible for Valencia College's SREF Compliance,
                                    Fire Safety Program, OSHA Safety Program, Lab Safety Program, Chemical Safety Program,
                                    as well as the Management of all Hazardous Waste.
                                 </p>
                                 
                                 <p>The Compliance &amp; Safety Department is responsible for twelve locations in Orange and
                                    Osceola counties.
                                 </p>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>SREF Compliance</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="http://www.fldoe.org/core/fileparse.php/7738/urlt/srefrule14.pdf" target="_blank">Florida DOE-State Requirements for Educational Facilities (SREF)</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>Workplace Safety</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><a href="https://www.osha.gov/law-regs.html" target="_blank">Occupational Safety &amp; Health Administration (OSHA)</a></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>Fire Safety</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>Fire Safety Information</li>
                                    
                                    <ul>
                                       
                                       <li><a href="/students/security/documents/How%20Fire%20Extinguishers%20Work.pdf" target="_blank">How Fire Extinguishers Work</a></li>
                                       
                                       <li><a href="/students/security/documents/Building%20Fire%20Safety%20Features.pdf" target="_blank">Building Fire Safety Features</a></li>
                                       
                                       <li><a href="/students/security/documents/Office%20Fire%20Safety%20Tips.pdf" target="_blank">Office Fire Safety Tips</a></li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>Lab Safety</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>Lab Inspections/Audits</li>
                                    
                                    <ul>
                                       
                                       <li>Forms</li>
                                       
                                       <ul>
                                          
                                          <li><a href="/students/security/documents/Weekly%20Inspection%20Log%20Form.doc" target="_blank">Weekly Inspection Log</a></li>
                                          
                                          <li><a href="/students/security/documents/HWDAuditForm.xlsx" target="_blank">Internal Hazardous Waste Audits</a></li>
                                          
                                       </ul>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>Hazardous Waste Disposal</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li>HWD Manuals</li>
                                    
                                    <ul>
                                       
                                       <li><a href="/students/security/documents/Valencia%20CC%20HWD%20Manua-Sciences.pdf" target="_blank">Science Department Hazardous Waste Disposal Manual</a></li>
                                       
                                       <li><a href="/students/security/documents/Valencia%20CC%20HWD%20Manua-PhotoChem.pdf" target="_blank">Dark Room and Photography Hazardous Waste Disposal Manual</a></li>
                                       
                                       <li><a href="/students/security/documents/Valencia%20CC%20HWD%20Manua-PlantOps.pdf" target="_blank">Plant Ops Department Hazardous Waste Disposal Manual</a></li>
                                       
                                    </ul>
                                    
                                    <li>Forms</li>
                                    
                                    <ul>
                                       
                                       <li><a href="/students/security/documents/Hazwastedisposalform.pdf" target="_blank">Chemical Waste Inventory Forms</a></li>
                                       
                                       <li><a href="/students/security/documents/Hazwastedisposalform-WCDentHyg.pdf" target="_blank">Photo Chemical or Dental Hygiene Waste Inventory Form</a></li>
                                       
                                       <li><a href="/students/security/documents/UnivUsedOildisposalform.pdf" target="_blank">Universal Waste &amp; Used Oil Inventory Forms</a></li>
                                       
                                       <li><a href="/students/security/documents/Universalwastedisposalform.pdf" target="_blank">Used Lamps Inventory Forms</a></li>
                                       
                                    </ul>
                                    
                                    <li>FAQs</li>
                                    
                                    <ul>
                                       
                                       <li><strong>When are my completed Hazardous Waste Inventories due to the Compliance &amp; Safety Office?</strong></li>
                                       
                                       <ul>
                                          
                                          <li>All completed Hazardous Waste Inventory Forms are due to the Compliance &amp; Safety Office
                                             by the 10th of the month prior to your department’s scheduled pick-up month. For example,
                                             if your department’s scheduled pickup month is August, all completed forms are due
                                             by July 10th.
                                          </li>
                                          
                                       </ul>
                                       
                                       <li><strong>How do I know my department’s scheduled Hazardous Waste Disposal pick-up date/time?</strong></li>
                                       
                                       <ul>
                                          
                                          <li>Once all inventories have been submitted and quoted by the disposal company, a date/time
                                             will be scheduled. The Compliance &amp; Safety Office will notify each department (lab
                                             manager, supervisor, etc.) of the scheduled date/time via e-mail.
                                          </li>
                                          
                                       </ul>
                                       
                                       <li><strong>How do I obtain labels for my hazardous waste containers?</strong></li>
                                       
                                       <ul>
                                          
                                          <li>Contact the Compliance &amp; Safety Office to request the appropriate hazardous waste
                                             labels. Please specify what type and how many waste container(s) you are requesting
                                             labels for.
                                          </li>
                                          
                                       </ul>
                                       
                                       <li><strong>When is Bio-medical waste picked up at my campus?</strong></li>
                                       
                                       <ul>
                                          
                                          <li>East &amp; West campuses have an annual schedule that is followed by the disposal company.
                                             Osceola &amp; Lake Nona campuses are picked up on an “as needed” basis. Please advise
                                             the Compliance &amp; Safety Office if you need a copy of your Campus Pick-up Schedule.
                                          </li>
                                          
                                       </ul>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>Employee Safety Training</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><strong>Does your department need in-person training?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>Golf Cart Safety</li>
                                       
                                       <li>Fire Extinguisher Training</li>
                                       
                                       <li>Chemical Hazardous Waste</li>
                                       
                                       <li>Hazardous Communication Standard</li>
                                       
                                       <li>Blood Borne Pathogens Standard</li>
                                       
                                       <li>Personal Protective Equipment</li>
                                       
                                    </ul>
                                    
                                    <li><strong>Any other Safety Training your department would be interested in? We will do our best
                                          to customize the training to your department's needs.</strong></li>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>FAQs</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <ul>
                                    
                                    <li><strong>Is the Compliance &amp; Safety Office ultimately responsible for ensuring my compliance?
                                          </strong></li>
                                    
                                    <ul>
                                       
                                       <li>The Compliance &amp; Safety Office is responsible for <strong>assisting</strong> in compliance, including notifying the college community of changes, providing tools
                                          to help implement the changes and auditing facilities as needed. However, each department
                                          head is responsible for their own compliance.
                                       </li>
                                       
                                    </ul>
                                    
                                    <li><strong>What should I do if I find something wrong during an inspection of my area?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>If you find an area needing assistance during an inspection, please try to correct
                                          the issue. If additional assistance is needed, contact the Compliance &amp; Safety Office.
                                       </li>
                                       
                                    </ul>
                                    
                                    <li><strong>How do I report a safety concern?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>If you have an <strong>immediate</strong> safety concern, please contact your Security Office.
                                       </li>
                                       
                                    </ul>
                                    
                                    <li><strong>Does the Compliance &amp; Safety Office provide required Personal Protective Equipment
                                          (PPE) to my department?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>No, the Compliance &amp; Safety Office does not provide Personal Protective Equipment
                                          (PPE) to each department. Please check with your department head on the appropriate
                                          procedure for obtaining PPE for your department.
                                       </li>
                                       
                                    </ul>
                                    
                                    <li><strong>How often do the compliance inspectors inspect each campus?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>Typically, the Compliance Inspectors will inspect each campus at least once a month.
                                          Feel free to ask them questions as they tour your area.
                                       </li>
                                       
                                    </ul>
                                    
                                    <li><strong>When a deficiency is found in my area, what happens next?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>If the Compliance Inspector finds a deficiency your area, they will work with you
                                          to correct it on the spot. If that is not possible, an action plan can be developed
                                          to ensure the deficiency is corrected in a reasonable amount of time.
                                       </li>
                                       
                                    </ul>
                                    
                                    <li><strong>I corrected a deficiency that was identified in my area. Who do I notify?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>Please contact the Compliance &amp; Safety Office, so that the area may be re-inspected.</li>
                                       
                                    </ul>
                                    
                                    <li><strong>How was a deficiency identified in my office when I was not even present?</strong></li>
                                    
                                    <ul>
                                       
                                       <li>At times, Compliance Inspectors may inspect your work area when you are not present.
                                          Due to the various schedules that employees work, it is not possible to schedule an
                                          inspection with each employee before entering their work area.
                                       </li>
                                       
                                    </ul>
                                    
                                 </ul>
                                 
                              </div>
                              
                           </div>
                           
                           <h3><strong>Contact Us</strong></h3>
                           
                           <div>
                              
                              <div>
                                 
                                 <p><a href="mailto:compliance@valenciacollege.edu">compliance@valenciacollege.edu</a></p>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </div>
                     
                     <aside class="col-md-3">
                        <center>Safety, Security &amp; Risk Management is dedicated to providing a safe, secure and welcoming
                           environment for our Students, Faculty, Staff and Visitors. We strive to provide excellent
                           customer service while implementing the latest technologies, insuring that our working
                           and learning environment is the safest it can be.
                        </center><br><center>
                           
                           <h3>"See Something, Say Something"</h3>
                           
                        </center>
                        <center></center>
                        
                        <div>
                           
                           <div data-old-tag="table">
                              
                              <div data-old-tag="tbody">
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Criminal Justice Institute </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-8000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> District Office </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-3000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> East Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">East Campus Bldg 5 Room 220</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-2000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Lake Nona Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-7000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Osceola Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">Building 2, Room 109</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-4000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:osc_security@valenciacollege.edu">osc_security@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Poinciana Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">1-103</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6500</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> West Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">SSB Room 170</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-1000</div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><a href="mailto:drodriguez225@valenciacollege.edu">drodriguez225@valenciacollege.edu</a></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td"><strong> Winter Park Campus </strong></div>
                                    
                                 </div>
                                 
                                 <div data-old-tag="tr">
                                    
                                    <div data-old-tag="td">407-582-6000</div>
                                    
                                 </div>
                                 
                              </div>
                              
                           </div>
                           
                        </div>
                        
                     </aside>
                     
                  </div>
                  
               </div>
               <hr class="styled_2">
            </div>
         </div>
      </div><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footer.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/footcode.inc"); ?><?php include($_SERVER['DOCUMENT_ROOT'] . "/_resources/includes/analytics.inc"); ?>
      <div id="hidden" style="display:none;">
         <a id="de" rel="nofollow" href="http://a.cms.omniupdate.com/10?skin=oucampus&amp;account=valenciacollege&amp;site=www&amp;action=de&amp;path=/students/security/compliance-and-safety.pcf">©</a>
      </div>
   </body>
</html>